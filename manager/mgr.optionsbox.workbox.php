<?php
	require_once('../assets/includes/session.php');							# INCLUDE THE SESSION START FILE
	
	//sleep(1);
	
	# KEEP THE PAGE FROM CACHING
	header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
	header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past	

	require_once('mgr.security.php');								# INCLUDE SECURITY CHECK FILE		
	require_once('mgr.config.php');									# INCLUDE MANAGER CONFIG FILE
	require_once('../assets/includes/db.config.php');						# INCLUDE DATABASE CONFIG FILE
	require_once('../assets/includes/shared.functions.php');					# INCLUDE SHARED FUNCTIONS FILE
	require_once('mgr.functions.php');								# INCLUDE MANAGER FUNCTIONS FILE		
	error_reporting(0);												# TURN ERROR REPORTING OFF TEMPORARILY TO USE SCRIPT ERROR REPORTING
	require_once('../assets/includes/tweak.php');							# INCLUDE THE TWEAK FILE
	require_once('../assets/includes/db.conn.php');							# INCLUDE DATABASE CONNECTION FILE
	require_once('mgr.select.settings.php');						# SELECT THE SETTINGS DATABASE
	include_lang();													# INCLUDE THE LANGUAGE FILE	
	require_once('../assets/includes/addons.php');									# INCLUDE MANAGER ADDONS FILE
	require_once('mgr.error.check.php');							# INCLUDE THE ERROR CHECKING FILE	
		
	error_reporting(E_ALL & ~E_NOTICE);								# TURN ERROR REPORTING BACK ON
	
	
	switch($_GET['box']){
		default:
		case "optionswb":
		
			$og_id = $_GET['og_id'];
		
			# SELECT ITEMS
			$options_result = mysqli_query($db,"SELECT op_id FROM {$dbinfo[pre]}options WHERE deleted = '0' AND parent_id = '$og_id' ORDER BY sortorder");
			$options_rows = mysqli_num_rows($options_result);
			
			$optiongrp_result = mysqli_query($db,"SELECT name,parent_type FROM {$dbinfo[pre]}option_grp WHERE og_id = '{$og_id}'");
			$optiongrp = mysqli_fetch_object($optiongrp_result);

			echo "<form id='option_edit_form' name='option_edit_form' action='mgr.optionsbox.actions.php' method='post'>";
			echo "<input type='hidden' name='og_id' value='$og_id' />";
			echo "<div id='wbheader'><p>$optiongrp->name &nbsp;</p></div>";
   				echo "<div id='wbbody'>";
					echo "<div style='overflow: auto; position: relative' id='options_button_row'>";
						echo "<div class='subsubon' id='option_list' onclick=\"show_options_list();load_options_list_win('$og_id');\" style='border-left: 1px solid #d8d7d7'>$mgrlang[gen_listoption]</div>";
						echo "<div class='subsuboff' id='option_edit' onclick=\"edit_option('new','{$optiongrp->parent_type}');\" style='border-right: 1px solid #d8d7d7;'>$mgrlang[gen_newoption]</div>";
					echo "</div>";
					echo "<div class='more_options' style='background-image:none; width: 735px; padding: 0' id='options_list_win'></div>";
					echo "<div class='more_options' style='background-position:top; width: 735px; padding: 0; display: none;' id='options_edit_win'></div>";
				echo "</div>";
			echo "</div>";
			echo "<div id='wbfooter' style='padding: 0 13px 20px 20px; margin: 0;'>";
				echo "<p style='float: right;' id='options_list_win_buttons'><input type='button' value='{$mgrlang[gen_b_close]}' class='small_button' onclick='close_workbox();' /></p>";
				echo "<p style='float: right; display: none;' id='options_edit_win_buttons'><input type='button' value='$mgrlang[gen_b_cancel]' onclick=\"show_options_list();load_options_list_win('$og_id');\" /><input type='button' value='$mgrlang[gen_b_save]' onclick=\"submit_option_form('$og_id');\" /></p>";
			echo "</div>";
			echo "</form>";
			
			/*echo "<script>alert($options_rows);</script>";*/
			
			if($options_rows)
			{
				echo "<script>load_options_list_win('".$_GET['og_id']."');</script>";
			}
			else
			{
				echo "<script>edit_option('new','{$optiongrp->parent_type}');</script>";
			}
		break;		
	}	
?>
