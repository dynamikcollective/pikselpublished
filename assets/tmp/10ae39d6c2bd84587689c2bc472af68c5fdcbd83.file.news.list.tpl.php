<?php /* Smarty version Smarty-3.1.8, created on 2017-01-19 12:20:26
         compiled from "/home/dynamikd/public_html/clients/piksel/assets/themes/default/news.list.tpl" */ ?>
<?php /*%%SmartyHeaderCode:17925554605880af0ac7a958-71825379%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '10ae39d6c2bd84587689c2bc472af68c5fdcbd83' => 
    array (
      0 => '/home/dynamikd/public_html/clients/piksel/assets/themes/default/news.list.tpl',
      1 => 1345559894,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '17925554605880af0ac7a958-71825379',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'lang' => 0,
    'config' => 0,
    'imgPath' => 0,
    'news' => 0,
    'newsArticle' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.8',
  'unifunc' => 'content_5880af0acd7982_88992531',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5880af0acd7982_88992531')) {function content_5880af0acd7982_88992531($_smarty_tpl) {?><!DOCTYPE HTML>
<html>
<head>
	<?php echo $_smarty_tpl->getSubTemplate ('head.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

	<script type="text/javascript">
		<!--
			$(function(){ $('#navNews').addClass('selectedNav'); });
		-->
	</script>
</head>
<body>
	<?php echo $_smarty_tpl->getSubTemplate ('overlays.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

	<div id="container">
		<?php echo $_smarty_tpl->getSubTemplate ('header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

		<div class="divTable contentContainer">
			<div class="divTableRow">
				<div class="divTableCell contentLeftColumn">
					<?php echo $_smarty_tpl->getSubTemplate ('subnav.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

				</div>
				<div class="divTableCell contentRightColumn">
					<div class="content">
						<h1><?php echo $_smarty_tpl->tpl_vars['lang']->value['news'];?>
<?php if ($_smarty_tpl->tpl_vars['config']->value['settings']['rss_news']){?><a href="<?php echo linkto(array('page'=>'rss.php?mode=news'),$_smarty_tpl);?>
"><img src="<?php echo $_smarty_tpl->tpl_vars['imgPath']->value;?>
/rss.icon.small.png" class="rssH1Icon rssPageH1Icon"></a><?php }?></h1>
						<?php if ($_smarty_tpl->tpl_vars['news']->value){?>
							<?php  $_smarty_tpl->tpl_vars['newsArticle'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['newsArticle']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['news']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['newsArticle']->key => $_smarty_tpl->tpl_vars['newsArticle']->value){
$_smarty_tpl->tpl_vars['newsArticle']->_loop = true;
?>
								<h2 class="newsDate"><?php echo $_smarty_tpl->tpl_vars['newsArticle']->value['display_date'];?>
</h2>
								<div class="newsArticle">
									<p class="newsTitle"><a href="<?php echo $_smarty_tpl->tpl_vars['newsArticle']->value['linkto'];?>
"><?php echo $_smarty_tpl->tpl_vars['newsArticle']->value['title'];?>
</a></p>
									<p class="newsShort"><?php echo $_smarty_tpl->tpl_vars['newsArticle']->value['short'];?>
</p>
									<?php if ($_smarty_tpl->tpl_vars['newsArticle']->value['article']!=''){?><p class="newsMore"><a href="<?php echo $_smarty_tpl->tpl_vars['newsArticle']->value['linkto'];?>
" class="colorLink"><?php echo $_smarty_tpl->tpl_vars['lang']->value['more'];?>
</a></p><?php }?>
								</div>
							<?php } ?>
						<?php }else{ ?>
							<p class="notice"><?php echo $_smarty_tpl->tpl_vars['lang']->value['noNews'];?>
</p>
						<?php }?>
					</div>
				</div>
			</div>
		</div>
		<?php echo $_smarty_tpl->getSubTemplate ('footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

    </div>
</body>
</html><?php }} ?>