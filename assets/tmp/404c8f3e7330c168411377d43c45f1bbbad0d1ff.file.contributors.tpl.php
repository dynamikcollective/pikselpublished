<?php /* Smarty version Smarty-3.1.8, created on 2016-11-28 18:58:21
         compiled from "/home/dynamikd/public_html/clients/piksel/assets/themes/modern/contributors.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1517821447583c7e4d501181-74321553%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '404c8f3e7330c168411377d43c45f1bbbad0d1ff' => 
    array (
      0 => '/home/dynamikd/public_html/clients/piksel/assets/themes/modern/contributors.tpl',
      1 => 1404922998,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1517821447583c7e4d501181-74321553',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'featuredContributors' => 0,
    'lang' => 0,
    'contributor' => 0,
    'imgPath' => 0,
    'contributorsList' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.8',
  'unifunc' => 'content_583c7e4d59a888_03656871',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_583c7e4d59a888_03656871')) {function content_583c7e4d59a888_03656871($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_truncate')) include '/home/dynamikd/public_html/clients/piksel/assets/smarty/plugins/modifier.truncate.php';
?><!DOCTYPE HTML>
<html>
<head>
	<?php echo $_smarty_tpl->getSubTemplate ('head.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

	<script>
		$(function()
		{	
			$('.featuredContributor img').load(function()
			{
				setEquals('.featuredContributor'); // After the images all load do a final height resize
			});
			
			setEquals('.featuredContributor'); // Backup for IE
		});
	</script>
</head>
<body>
	<?php echo $_smarty_tpl->getSubTemplate ('overlays.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

	<div id="container">
		<?php echo $_smarty_tpl->getSubTemplate ('header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

		<?php echo $_smarty_tpl->getSubTemplate ('header2.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>
		
		<div class="container" style="min-height: 600px;">					
			<?php if ($_smarty_tpl->tpl_vars['featuredContributors']->value){?>
			<h1><?php echo $_smarty_tpl->tpl_vars['lang']->value['contributors'];?>
 <a href="<?php echo linkto(array('page'=>'create.account.php?jumpTo=members'),$_smarty_tpl);?>
" class="btn btn-xs btn-success" style="float: right;"><?php echo $_smarty_tpl->tpl_vars['lang']->value['signUpNow'];?>
</a></h1>
			<hr>
			<div id="featuredContributorsList">				
				<?php  $_smarty_tpl->tpl_vars['contributor'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['contributor']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['featuredContributors']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['contributor']->key => $_smarty_tpl->tpl_vars['contributor']->value){
$_smarty_tpl->tpl_vars['contributor']->_loop = true;
?>
					<div class="featuredContributor">
						<a href="<?php echo $_smarty_tpl->tpl_vars['contributor']->value['profileLinkto'];?>
"><?php if ($_smarty_tpl->tpl_vars['contributor']->value['avatar']){?><img src="<?php echo memberAvatar(array('memID'=>$_smarty_tpl->tpl_vars['contributor']->value['mem_id'],'size'=>100,'crop'=>100),$_smarty_tpl);?>
"><?php }else{ ?><img src="<?php echo $_smarty_tpl->tpl_vars['imgPath']->value;?>
/avatar.png" style="width: 80px;"><?php }?></a><strong><a href="<?php echo $_smarty_tpl->tpl_vars['contributor']->value['profileLinkto'];?>
"><?php echo $_smarty_tpl->tpl_vars['contributor']->value['display_name'];?>
</a></strong>
						<?php if ($_smarty_tpl->tpl_vars['contributor']->value['bio_content']){?><p class="bio"><?php echo smarty_modifier_truncate($_smarty_tpl->tpl_vars['contributor']->value['bio_content'],120);?>
</p><?php }?>
						<p><br><a href="<?php echo $_smarty_tpl->tpl_vars['contributor']->value['profileLinkto'];?>
" class="btn btn-xs btn-primary"><?php echo $_smarty_tpl->tpl_vars['lang']->value['profile'];?>
</a> <a href="<?php echo $_smarty_tpl->tpl_vars['contributor']->value['allMediaLinkto'];?>
" class="btn btn-xs btn-primary"><?php echo $_smarty_tpl->tpl_vars['lang']->value['media'];?>
</a></p>
					</div>
				<?php } ?>
			</div>
		<?php }?>				
		<ul id="contributorsList">
		<?php  $_smarty_tpl->tpl_vars['contributor'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['contributor']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['contributorsList']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['contributor']->key => $_smarty_tpl->tpl_vars['contributor']->value){
$_smarty_tpl->tpl_vars['contributor']->_loop = true;
?>
			<li><a href="<?php echo $_smarty_tpl->tpl_vars['contributor']->value['profileLinkto'];?>
"><img src="<?php echo memberAvatar(array('memID'=>$_smarty_tpl->tpl_vars['contributor']->value['mem_id'],'size'=>30,'crop'=>30),$_smarty_tpl);?>
"></a> <a href="<?php echo $_smarty_tpl->tpl_vars['contributor']->value['profileLinkto'];?>
"><?php echo $_smarty_tpl->tpl_vars['contributor']->value['display_name'];?>
</a></li>
		<?php } ?>
		</ul>
		</div>
		<?php echo $_smarty_tpl->getSubTemplate ('footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

    </div>
</body>
</html>
<?php }} ?>