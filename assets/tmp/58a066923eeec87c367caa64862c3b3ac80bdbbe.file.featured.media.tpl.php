<?php /* Smarty version Smarty-3.1.8, created on 2016-11-28 16:53:00
         compiled from "/home/dynamikd/public_html/clients/piksel/assets/themes/modern/featured.media.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1734470911583c60ec0cb5e8-07968355%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '58a066923eeec87c367caa64862c3b3ac80bdbbe' => 
    array (
      0 => '/home/dynamikd/public_html/clients/piksel/assets/themes/modern/featured.media.tpl',
      1 => 1403182478,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1734470911583c60ec0cb5e8-07968355',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'lang' => 0,
    'config' => 0,
    'mediaRows' => 0,
    'mediaPaging' => 0,
    'mediaArray' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.8',
  'unifunc' => 'content_583c60ec10d629_75824639',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_583c60ec10d629_75824639')) {function content_583c60ec10d629_75824639($_smarty_tpl) {?><!DOCTYPE HTML>
<html>
<head>
	<?php echo $_smarty_tpl->getSubTemplate ('head.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

</head>
<body>
	<?php echo $_smarty_tpl->getSubTemplate ('overlays.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

	<div id="container">
		<?php echo $_smarty_tpl->getSubTemplate ('header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

		<?php echo $_smarty_tpl->getSubTemplate ('header2.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>
		
		<div class="container">					
			<h1><?php echo $_smarty_tpl->tpl_vars['lang']->value['featuredMedia'];?>
<?php if ($_smarty_tpl->tpl_vars['config']->value['settings']['rss_featured_media']){?>&nbsp;<a href="<?php echo linkto(array('page'=>'rss.php?mode=featuredMedia'),$_smarty_tpl);?>
" class="btn btn-xxs btn-warning"><?php echo $_smarty_tpl->tpl_vars['lang']->value['rss'];?>
</a><?php }?></h1>
			<hr>
			<?php if ($_smarty_tpl->tpl_vars['mediaRows']->value){?>
				<?php echo $_smarty_tpl->getSubTemplate ("paging.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('paging'=>$_smarty_tpl->tpl_vars['mediaPaging']->value), 0);?>

				<div id="mediaListContainer">
					<?php  $_smarty_tpl->tpl_vars['media'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['media']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['mediaArray']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['media']->key => $_smarty_tpl->tpl_vars['media']->value){
$_smarty_tpl->tpl_vars['media']->_loop = true;
?>
						<?php echo $_smarty_tpl->getSubTemplate ('media.container.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

					<?php } ?>
				</div>
				<?php echo $_smarty_tpl->getSubTemplate ("paging.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('paging'=>$_smarty_tpl->tpl_vars['mediaPaging']->value), 0);?>

			<?php }else{ ?>
				<p class="notice"><?php echo $_smarty_tpl->tpl_vars['lang']->value['noMedia'];?>
</p>
			<?php }?>
		</div>
		<?php echo $_smarty_tpl->getSubTemplate ('footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

    </div>
</body>
</html><?php }} ?>