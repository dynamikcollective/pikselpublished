<?php /* Smarty version Smarty-3.1.8, created on 2016-11-28 16:52:56
         compiled from "/home/dynamikd/public_html/clients/piksel/assets/themes/modern/gallery.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1421872868583c60e8e66333-34939851%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'fc2fa2cbbbfd8807f0e91f59875638fd4fec9998' => 
    array (
      0 => '/home/dynamikd/public_html/clients/piksel/assets/themes/modern/gallery.tpl',
      1 => 1404996046,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1421872868583c60e8e66333-34939851',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'baseURL' => 0,
    'mediaPaging' => 0,
    'id' => 0,
    'contributor' => 0,
    'crumbs' => 0,
    'key' => 0,
    'galleriesData' => 0,
    'config' => 0,
    'currentGallery' => 0,
    'galleryID' => 0,
    'lang' => 0,
    'subGalleriesData' => 0,
    'galleryPaging' => 0,
    'subGallery' => 0,
    'imgPath' => 0,
    'mediaRows' => 0,
    'printRows' => 0,
    'prints' => 0,
    'print' => 0,
    'productRows' => 0,
    'products' => 0,
    'product' => 0,
    'packageRows' => 0,
    'packages' => 0,
    'package' => 0,
    'collectionRows' => 0,
    'collections' => 0,
    'collection' => 0,
    'gallerySortByOptions' => 0,
    'selectedGallerySortBy' => 0,
    'gallerySortByTypeOptions' => 0,
    'selectedGallerySortType' => 0,
    'mediaArray' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.8',
  'unifunc' => 'content_583c60e916ee23_65893151',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_583c60e916ee23_65893151')) {function content_583c60e916ee23_65893151($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_truncate')) include '/home/dynamikd/public_html/clients/piksel/assets/smarty/plugins/modifier.truncate.php';
if (!is_callable('smarty_function_html_options')) include '/home/dynamikd/public_html/clients/piksel/assets/smarty/plugins/function.html_options.php';
?><!DOCTYPE HTML>
<html>
<head>
	<?php echo $_smarty_tpl->getSubTemplate ('head.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

	<script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['baseURL']->value;?>
/assets/javascript/gallery.js"></script>
</head>
<body>
	<?php echo $_smarty_tpl->getSubTemplate ('overlays.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

	<div id="container">
		<?php echo $_smarty_tpl->getSubTemplate ('header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

		<?php echo $_smarty_tpl->getSubTemplate ('header2.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>
		
		<div class="container">					
			<form action="<?php echo linkto(array('page'=>($_smarty_tpl->tpl_vars['mediaPaging']->value['pageName'])."&id=".($_smarty_tpl->tpl_vars['id']->value)."&".($_smarty_tpl->tpl_vars['mediaPaging']->value['pageVar'])."=1"),$_smarty_tpl);?>
" method="post" id="galleryForm">
			<input type="hidden" name="postGalleryForm" id="postGalleryForm" value="1">
			
			<h1><?php if ($_smarty_tpl->tpl_vars['contributor']->value['avatar']){?><a href="<?php echo $_smarty_tpl->tpl_vars['contributor']->value['linkto'];?>
"><img src="<?php echo memberAvatar(array('memID'=>$_smarty_tpl->tpl_vars['contributor']->value['mem_id'],'size'=>40,'crop'=>40,'hcrop'=>40),$_smarty_tpl);?>
" class="h1PhotoHeader"></a><?php }?> <?php  $_smarty_tpl->tpl_vars['crumb'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['crumb']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['crumbs']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['crumb']->total= $_smarty_tpl->_count($_from);
 $_smarty_tpl->tpl_vars['crumb']->iteration=0;
foreach ($_from as $_smarty_tpl->tpl_vars['crumb']->key => $_smarty_tpl->tpl_vars['crumb']->value){
$_smarty_tpl->tpl_vars['crumb']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['crumb']->key;
 $_smarty_tpl->tpl_vars['crumb']->iteration++;
 $_smarty_tpl->tpl_vars['crumb']->last = $_smarty_tpl->tpl_vars['crumb']->iteration === $_smarty_tpl->tpl_vars['crumb']->total;
?><a href="<?php echo $_smarty_tpl->tpl_vars['galleriesData']->value[$_smarty_tpl->tpl_vars['key']->value]['linkto'];?>
"><?php echo $_smarty_tpl->tpl_vars['galleriesData']->value[$_smarty_tpl->tpl_vars['key']->value]['name'];?>
</a> <?php if (!$_smarty_tpl->tpl_vars['crumb']->last){?> &raquo; <?php }?><?php } ?> <?php if ($_smarty_tpl->tpl_vars['config']->value['settings']['rss_galleries']&&$_smarty_tpl->tpl_vars['currentGallery']->value['gallery_id']!=0){?> <a href="<?php echo linkto(array('page'=>"rss.php?mode=gallery&id=".($_smarty_tpl->tpl_vars['galleryID']->value)),$_smarty_tpl);?>
" class="btn btn-xxs btn-warning"><?php echo $_smarty_tpl->tpl_vars['lang']->value['rss'];?>
</a><?php }?></h1>
			<hr>
			<?php if ($_smarty_tpl->tpl_vars['currentGallery']->value['description']){?><p <?php if ($_smarty_tpl->tpl_vars['currentGallery']->value['event_details']){?>style="margin-bottom: 10px;"<?php }?>><?php echo $_smarty_tpl->tpl_vars['currentGallery']->value['description'];?>
</p><?php }?>

			<?php if ($_smarty_tpl->tpl_vars['currentGallery']->value['event_details']){?>
				<ul class="galleryEventDetails">
					<?php if ($_smarty_tpl->tpl_vars['currentGallery']->value['client_name']){?><li class="eventDetailsTitle"><?php echo $_smarty_tpl->tpl_vars['lang']->value['clientName'];?>
:</li><li><?php echo $_smarty_tpl->tpl_vars['currentGallery']->value['client_name'];?>
</li><?php }?>
					<?php if ($_smarty_tpl->tpl_vars['currentGallery']->value['event_code']){?><li class="eventDetailsTitle"><?php echo $_smarty_tpl->tpl_vars['lang']->value['eventCode'];?>
:</li><li><?php echo $_smarty_tpl->tpl_vars['currentGallery']->value['event_code'];?>
</li><?php }?>
					<?php if ($_smarty_tpl->tpl_vars['currentGallery']->value['event_date']){?><li class="eventDetailsTitle"><?php echo $_smarty_tpl->tpl_vars['lang']->value['eventDate'];?>
:</li><li><?php echo $_smarty_tpl->tpl_vars['currentGallery']->value['event_date_display'];?>
</li><?php }?>
					<?php if ($_smarty_tpl->tpl_vars['currentGallery']->value['event_location']){?><li class="eventDetailsTitle"><?php echo $_smarty_tpl->tpl_vars['lang']->value['eventLocation'];?>
:</li><li><?php echo $_smarty_tpl->tpl_vars['currentGallery']->value['event_location'];?>
</li><?php }?>
				</ul>
			<?php }?>
			
			<?php if ($_smarty_tpl->tpl_vars['subGalleriesData']->value){?>
				<?php if ($_smarty_tpl->tpl_vars['galleryPaging']->value['totalResults']>$_smarty_tpl->tpl_vars['config']->value['settings']['gallery_perpage']){?><?php echo $_smarty_tpl->getSubTemplate ("paging.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('paging'=>$_smarty_tpl->tpl_vars['galleryPaging']->value), 0);?>
<?php }?>
				<div id="galleryListContainer">
					<?php  $_smarty_tpl->tpl_vars['subGallery'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['subGallery']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['subGalleriesData']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['subGallery']->key => $_smarty_tpl->tpl_vars['subGallery']->value){
$_smarty_tpl->tpl_vars['subGallery']->_loop = true;
?>
						<div class="galleryContainer" style="width: <?php echo $_smarty_tpl->tpl_vars['config']->value['settings']['gallery_thumb_size'];?>
px">
							<div class="galleryDetailsContainer" style="width: <?php echo $_smarty_tpl->tpl_vars['config']->value['settings']['gallery_thumb_size'];?>
px; <?php if ($_smarty_tpl->tpl_vars['galleriesData']->value[$_smarty_tpl->tpl_vars['subGallery']->value]['galleryIcon']){?>vertical-align: top<?php }?>">
								<?php if ($_smarty_tpl->tpl_vars['galleriesData']->value[$_smarty_tpl->tpl_vars['subGallery']->value]['galleryIcon']){?><p class="galleryIconContainer" style="min-width: <?php echo $_smarty_tpl->tpl_vars['galleriesData']->value[$_smarty_tpl->tpl_vars['subGallery']->value]['galleryIcon']['width'];?>
px; min-height: <?php echo $_smarty_tpl->tpl_vars['galleriesData']->value[$_smarty_tpl->tpl_vars['subGallery']->value]['galleryIcon']['height']+5;?>
px;"><a href="<?php echo $_smarty_tpl->tpl_vars['galleriesData']->value[$_smarty_tpl->tpl_vars['subGallery']->value]['linkto'];?>
"><img src="<?php echo $_smarty_tpl->tpl_vars['baseURL']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['galleriesData']->value[$_smarty_tpl->tpl_vars['subGallery']->value]['galleryIcon']['imgSrc'];?>
"></a></p><?php }?>
								<p class="galleryDetails"><?php if ($_smarty_tpl->tpl_vars['galleriesData']->value[$_smarty_tpl->tpl_vars['subGallery']->value]['password']){?><img src="<?php echo $_smarty_tpl->tpl_vars['imgPath']->value;?>
/lock.png" class="lock"><?php }?><a href="<?php echo $_smarty_tpl->tpl_vars['galleriesData']->value[$_smarty_tpl->tpl_vars['subGallery']->value]['linkto'];?>
"><?php echo $_smarty_tpl->tpl_vars['galleriesData']->value[$_smarty_tpl->tpl_vars['subGallery']->value]['name'];?>
</a><?php if ($_smarty_tpl->tpl_vars['config']->value['settings']['gallery_count']){?><?php if ($_smarty_tpl->tpl_vars['galleriesData']->value[$_smarty_tpl->tpl_vars['subGallery']->value]['gallery_count']>0||$_smarty_tpl->tpl_vars['config']->value['ShowZeroCounts']){?>&nbsp;<span class="galleryMediaCount">(<?php echo $_smarty_tpl->tpl_vars['galleriesData']->value[$_smarty_tpl->tpl_vars['subGallery']->value]['gallery_count'];?>
)</span><?php }?><?php }?></p>
							</div>
							<!--gi: <?php echo $_smarty_tpl->tpl_vars['galleriesData']->value[$_smarty_tpl->tpl_vars['subGallery']->value]['galleryIcon']['imgSrc'];?>
-->
						</div>
					<?php } ?>
				</div>
				<?php if ($_smarty_tpl->tpl_vars['galleryPaging']->value['totalResults']>$_smarty_tpl->tpl_vars['config']->value['settings']['gallery_perpage']){?>
					<?php echo $_smarty_tpl->getSubTemplate ("paging.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('paging'=>$_smarty_tpl->tpl_vars['galleryPaging']->value), 0);?>

					<?php if ($_smarty_tpl->tpl_vars['mediaRows']->value){?><hr style="margin-top: 15px;"><?php }?>
				<?php }?>
			<?php }?>
			
			<?php if ($_smarty_tpl->tpl_vars['printRows']->value){?>
				<div class="galleryFeaturedItemsContainer">
					<h3><?php echo $_smarty_tpl->tpl_vars['lang']->value['prints'];?>
</h3>
					<?php  $_smarty_tpl->tpl_vars['print'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['print']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['prints']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['print']->key => $_smarty_tpl->tpl_vars['print']->value){
$_smarty_tpl->tpl_vars['print']->_loop = true;
?>
						<div class="featuredPageItem galleryFeaturedPrints workboxLinkAttach">
							<h2><a href="<?php echo $_smarty_tpl->tpl_vars['print']->value['linkto'];?>
" class="workboxLink"><?php echo $_smarty_tpl->tpl_vars['print']->value['name'];?>
</a></h2>
							<p class="description"><?php if ($_smarty_tpl->tpl_vars['print']->value['photo']){?><img src="<?php echo productShot(array('itemID'=>$_smarty_tpl->tpl_vars['print']->value['print_id'],'itemType'=>'print','photoID'=>$_smarty_tpl->tpl_vars['print']->value['photo']['id'],'size'=>70),$_smarty_tpl);?>
"><?php }?><?php echo smarty_modifier_truncate($_smarty_tpl->tpl_vars['print']->value['description'],360);?>
</p>
							<p class="moreInfo"><?php if ($_smarty_tpl->tpl_vars['print']->value['price']){?><span class="price"><?php echo $_smarty_tpl->tpl_vars['print']->value['price']['display'];?>
</span><?php }?><?php if ($_smarty_tpl->tpl_vars['config']->value['settings']['cart']==3){?> <?php echo $_smarty_tpl->tpl_vars['lang']->value['priceCreditSep'];?>
 <?php }?><?php if ($_smarty_tpl->tpl_vars['print']->value['credits']){?><span class="price"><?php echo $_smarty_tpl->tpl_vars['print']->value['credits'];?>
 <sup><?php echo $_smarty_tpl->tpl_vars['lang']->value['mediaLabelCredits'];?>
</sup></span><?php }?></p>
						</div>
					<?php } ?>
				</div>
			<?php }?>
			
			<?php if ($_smarty_tpl->tpl_vars['productRows']->value){?>
				<div class="galleryFeaturedItemsContainer">
					<h3><?php echo $_smarty_tpl->tpl_vars['lang']->value['products'];?>
</h3>
					<?php  $_smarty_tpl->tpl_vars['product'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['product']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['products']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['product']->key => $_smarty_tpl->tpl_vars['product']->value){
$_smarty_tpl->tpl_vars['product']->_loop = true;
?>
						<div class="featuredPageItem galleryFeaturedProducts workboxLinkAttach">
							<h2><a href="<?php echo $_smarty_tpl->tpl_vars['product']->value['linkto'];?>
" class="workboxLink"><?php echo $_smarty_tpl->tpl_vars['product']->value['name'];?>
</a></h2>
							<p class="description"><?php if ($_smarty_tpl->tpl_vars['product']->value['photo']){?><img src="<?php echo productShot(array('itemID'=>$_smarty_tpl->tpl_vars['product']->value['prod_id'],'itemType'=>'prod','photoID'=>$_smarty_tpl->tpl_vars['product']->value['photo']['id'],'size'=>70),$_smarty_tpl);?>
"><?php }?><?php echo smarty_modifier_truncate($_smarty_tpl->tpl_vars['product']->value['description'],360);?>
</p>
							<p class="moreInfo"><?php if ($_smarty_tpl->tpl_vars['product']->value['price']){?><span class="price"><?php echo $_smarty_tpl->tpl_vars['product']->value['price']['display'];?>
</span><?php }?><?php if ($_smarty_tpl->tpl_vars['config']->value['settings']['cart']==3){?> <?php echo $_smarty_tpl->tpl_vars['lang']->value['priceCreditSep'];?>
 <?php }?><?php if ($_smarty_tpl->tpl_vars['product']->value['credits']){?><span class="price"><?php echo $_smarty_tpl->tpl_vars['product']->value['credits'];?>
 <sup><?php echo $_smarty_tpl->tpl_vars['lang']->value['mediaLabelCredits'];?>
</sup></span><?php }?></p>
						</div>
					<?php } ?>
				</div>
			<?php }?>
			
			<?php if ($_smarty_tpl->tpl_vars['packageRows']->value){?>
				<div class="galleryFeaturedItemsContainer">
					<h3><?php echo $_smarty_tpl->tpl_vars['lang']->value['packages'];?>
</h3>
					<?php  $_smarty_tpl->tpl_vars['package'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['package']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['packages']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['package']->key => $_smarty_tpl->tpl_vars['package']->value){
$_smarty_tpl->tpl_vars['package']->_loop = true;
?>
						<div class="featuredPageItem galleryFeaturedPackages workboxLinkAttach">
							<h2><a href="<?php echo $_smarty_tpl->tpl_vars['package']->value['linkto'];?>
" class="workboxLink"><?php echo $_smarty_tpl->tpl_vars['package']->value['name'];?>
</a></h2>
							<p class="description"><?php if ($_smarty_tpl->tpl_vars['package']->value['photo']){?><img src="<?php echo productShot(array('itemID'=>$_smarty_tpl->tpl_vars['package']->value['pack_id'],'itemType'=>'pack','photoID'=>$_smarty_tpl->tpl_vars['package']->value['photo']['id'],'size'=>125),$_smarty_tpl);?>
"><?php }?><?php echo smarty_modifier_truncate($_smarty_tpl->tpl_vars['package']->value['description'],360);?>
</p>
							<p class="moreInfo"><?php if ($_smarty_tpl->tpl_vars['package']->value['price']){?><span class="price"><?php echo $_smarty_tpl->tpl_vars['package']->value['price']['display'];?>
</span><?php }?><?php if ($_smarty_tpl->tpl_vars['config']->value['settings']['cart']==3){?> <?php echo $_smarty_tpl->tpl_vars['lang']->value['priceCreditSep'];?>
 <?php }?><?php if ($_smarty_tpl->tpl_vars['package']->value['credits']){?><span class="price"><?php echo $_smarty_tpl->tpl_vars['package']->value['credits'];?>
 <sup><?php echo $_smarty_tpl->tpl_vars['lang']->value['mediaLabelCredits'];?>
</sup></span><?php }?></p>
						</div>
					<?php } ?>
				</div>
			<?php }?>
			
			<?php if ($_smarty_tpl->tpl_vars['collectionRows']->value){?>
				<div class="galleryFeaturedItemsContainer">
					<h3><?php echo $_smarty_tpl->tpl_vars['lang']->value['collections'];?>
</h3>
					<?php  $_smarty_tpl->tpl_vars['collection'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['collection']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['collections']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['collection']->key => $_smarty_tpl->tpl_vars['collection']->value){
$_smarty_tpl->tpl_vars['collection']->_loop = true;
?>
						<div class="featuredPageItem galleryFeaturedCollections workboxLinkAttach">
							<h2><a href="<?php echo $_smarty_tpl->tpl_vars['collection']->value['linkto'];?>
" class="workboxLink"><?php echo $_smarty_tpl->tpl_vars['collection']->value['name'];?>
</a></h2>
							<p class="description"><?php if ($_smarty_tpl->tpl_vars['collection']->value['photo']){?><img src="<?php echo productShot(array('itemID'=>$_smarty_tpl->tpl_vars['collection']->value['coll_id'],'itemType'=>'coll','photoID'=>$_smarty_tpl->tpl_vars['collection']->value['photo']['id'],'size'=>125),$_smarty_tpl);?>
"><?php }?><?php echo smarty_modifier_truncate($_smarty_tpl->tpl_vars['collection']->value['description'],360);?>
</p>
							<p class="moreInfo"><?php if ($_smarty_tpl->tpl_vars['collection']->value['price']){?><span class="price"><?php echo $_smarty_tpl->tpl_vars['collection']->value['price']['display'];?>
</span><?php }?><?php if ($_smarty_tpl->tpl_vars['config']->value['settings']['cart']==3){?> <?php echo $_smarty_tpl->tpl_vars['lang']->value['priceCreditSep'];?>
 <?php }?><?php if ($_smarty_tpl->tpl_vars['collection']->value['credits']){?><span class="price"><?php echo $_smarty_tpl->tpl_vars['collection']->value['credits'];?>
 <sup><?php echo $_smarty_tpl->tpl_vars['lang']->value['mediaLabelCredits'];?>
</sup></span><?php }?></p>
						</div>
					<?php } ?>
				</div>
			<?php }?>
				
			<?php if ($_smarty_tpl->tpl_vars['mediaRows']->value){?>
				<div>
					<p class="sortByContainer">
						<?php echo $_smarty_tpl->tpl_vars['lang']->value['sortBy'];?>

						<select name="gallerySortBy" id="gallerySortBy">
							<?php echo smarty_function_html_options(array('options'=>$_smarty_tpl->tpl_vars['gallerySortByOptions']->value,'selected'=>$_smarty_tpl->tpl_vars['selectedGallerySortBy']->value),$_smarty_tpl);?>

						</select>
						<select name="gallerySortType" id="gallerySortType">
							<?php echo smarty_function_html_options(array('options'=>$_smarty_tpl->tpl_vars['gallerySortByTypeOptions']->value,'selected'=>$_smarty_tpl->tpl_vars['selectedGallerySortType']->value),$_smarty_tpl);?>

						</select>
					</p>
					
					<?php echo $_smarty_tpl->getSubTemplate ("paging.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('paging'=>$_smarty_tpl->tpl_vars['mediaPaging']->value), 0);?>

					<div id="mediaListContainer">
						<?php  $_smarty_tpl->tpl_vars['media'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['media']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['mediaArray']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['media']->key => $_smarty_tpl->tpl_vars['media']->value){
$_smarty_tpl->tpl_vars['media']->_loop = true;
?>
							<?php echo $_smarty_tpl->getSubTemplate ('media.container.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

						<?php } ?>
					</div>
					<?php echo $_smarty_tpl->getSubTemplate ("paging.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('paging'=>$_smarty_tpl->tpl_vars['mediaPaging']->value), 0);?>

				</div>
			<?php }?>
			
			<?php if ($_smarty_tpl->tpl_vars['currentGallery']->value['gallery_id']!=0){?>
				<?php if (!$_smarty_tpl->tpl_vars['subGalleriesData']->value&&!$_smarty_tpl->tpl_vars['mediaRows']->value){?><br><p class="notice"><?php echo $_smarty_tpl->tpl_vars['lang']->value['noMedia'];?>
</p><?php }?>
			<?php }?>
			
			</form>
			
		</div>
		<?php echo $_smarty_tpl->getSubTemplate ('footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

    </div>
</body>
</html>
<?php }} ?>