<?php /* Smarty version Smarty-3.1.8, created on 2017-01-19 12:21:23
         compiled from "/home/dynamikd/public_html/clients/piksel/assets/themes/sleek/index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:3886303535880af43522950-26382533%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '5f14ccc30c58d9d631b5ad2082129c3f450b3106' => 
    array (
      0 => '/home/dynamikd/public_html/clients/piksel/assets/themes/sleek/index.tpl',
      1 => 1428936996,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '3886303535880af43522950-26382533',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'config' => 0,
    'baseURL' => 0,
    'featuredMedia' => 0,
    'media' => 0,
    'newestMediaRows' => 0,
    'lang' => 0,
    'imgPath' => 0,
    'newestMedia' => 0,
    'popularMediaRows' => 0,
    'popularMedia' => 0,
    'randomMediaRows' => 0,
    'randomMedia' => 0,
    'subGalleriesData' => 0,
    'subGallery' => 0,
    'galleriesData' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.8',
  'unifunc' => 'content_5880af4368b171_97394517',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5880af4368b171_97394517')) {function content_5880af4368b171_97394517($_smarty_tpl) {?><!DOCTYPE HTML>
<html>
<head>
	<?php echo $_smarty_tpl->getSubTemplate ('head.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

	<script type="text/javascript">
		var featuredVideoOverVol = '<?php echo $_smarty_tpl->tpl_vars['config']->value['featuredVideoOverVol'];?>
';
		var featuredVideoVolume = '<?php echo $_smarty_tpl->tpl_vars['config']->value['featuredVideoVolume'];?>
';
		
		var featuredMedia = 
		{
			'fadeSpeed'		: <?php echo $_smarty_tpl->tpl_vars['config']->value['settings']['hpf_fade_speed'];?>
,
			'interval'		: <?php echo $_smarty_tpl->tpl_vars['config']->value['settings']['hpf_inverval'];?>
,
			'detailsDelay'	: <?php echo $_smarty_tpl->tpl_vars['config']->value['settings']['hpf_details_delay'];?>
,
			'detailsDisTime': <?php echo $_smarty_tpl->tpl_vars['config']->value['settings']['hpf_details_distime'];?>

		};
		
		function featuredVideoPlayer(mediaID,container)
		{
			//alert(container);
			
			//$("#featuredVideoPlayerContainer").css({ 'width':'250px','height':'250px' });
			
			jwplayer(container).setup(
			{
				'flashplayer': "<?php echo $_smarty_tpl->tpl_vars['baseURL']->value;?>
/assets/jwplayer/player.swf",
				'file': "<?php echo $_smarty_tpl->tpl_vars['baseURL']->value;?>
/video.php?mediaID="+mediaID,
				'autostart': '<?php echo $_smarty_tpl->tpl_vars['config']->value['autoPlayFeaturedVid'];?>
',
				'type': 'video',
				'repeat': '<?php echo $_smarty_tpl->tpl_vars['config']->value['settings']['video_autorepeat'];?>
',
				'controlbar.position': '<?php echo $_smarty_tpl->tpl_vars['config']->value['settings']['video_controls'];?>
',
				'logo.file': '<?php echo $_smarty_tpl->tpl_vars['baseURL']->value;?>
/assets/watermarks/<?php echo $_smarty_tpl->tpl_vars['config']->value['settings']['vidpreview_wm'];?>
',
				'logo.hide': false,
				'logo.position': '<?php echo $_smarty_tpl->tpl_vars['config']->value['settings']['video_wmpos'];?>
',
				'stretching': 'uniform',
				'width': '100%',
				'height': '100%',
				'skin': '<?php echo $_smarty_tpl->tpl_vars['baseURL']->value;?>
/assets/jwplayer/skins/<?php echo $_smarty_tpl->tpl_vars['config']->value['settings']['video_skin'];?>
/<?php echo $_smarty_tpl->tpl_vars['config']->value['settings']['video_skin'];?>
.zip',
				'screencolor': '<?php echo $_smarty_tpl->tpl_vars['config']->value['settings']['video_bg_color'];?>
',
				'volume': '<?php echo $_smarty_tpl->tpl_vars['config']->value['featuredVideoVolume'];?>
',
				'modes': [
					{ 'type': 'flash', src: '<?php echo $_smarty_tpl->tpl_vars['baseURL']->value;?>
/assets/jwplayer/player.swf' },
					{ 'type': 'html5' },
					{ 'type': 'download' }
				]
			});
		}
	</script>
	<script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['baseURL']->value;?>
/assets/javascript/index.js"></script>
	<script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['baseURL']->value;?>
/assets/javascript/gallery.js"></script>
	<style>
		#featuredVideoPlayerContainer{
			min-width: <?php echo $_smarty_tpl->tpl_vars['config']->value['settings']['hpf_width'];?>
px;
			min-height: <?php echo $_smarty_tpl->tpl_vars['config']->value['settings']['hpf_crop_to'];?>
px;
		}
	</style>
</head>
<body>
	<?php echo $_smarty_tpl->getSubTemplate ('overlays.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

	<div id="container">
		<?php echo $_smarty_tpl->getSubTemplate ('header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

		<?php if ($_smarty_tpl->tpl_vars['config']->value['settings']['hpfeaturedmedia']&&$_smarty_tpl->tpl_vars['featuredMedia']->value){?>
		<div id="featuredRowContainer">
			<div class="center">
			
								
				<div id="featuredOneCell" style="width: <?php echo $_smarty_tpl->tpl_vars['config']->value['settings']['hpf_width'];?>
px; height: <?php echo $_smarty_tpl->tpl_vars['config']->value['settings']['hpf_crop_to'];?>
px;">
					<p id="featuredNext" class="opac50" style="z-index: 1000" onclick="featuredMediaRotator();">&nbsp;&raquo;&nbsp;</p>
					<div id="featuredOneContainer"></div>
					<ul id="featuredMediaList" class="opac60" style="width: <?php echo $_smarty_tpl->tpl_vars['config']->value['settings']['hpf_width']-20;?>
px;">
						<?php  $_smarty_tpl->tpl_vars['media'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['media']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['featuredMedia']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['media']->key => $_smarty_tpl->tpl_vars['media']->value){
$_smarty_tpl->tpl_vars['media']->_loop = true;
?>
							<li mediaType="<?php if ($_smarty_tpl->tpl_vars['media']->value['sampleVideo']){?>video<?php }else{ ?>image<?php }?>" image="image.php?mediaID=<?php echo $_smarty_tpl->tpl_vars['media']->value['encryptedID'];?>
=&type=featured&folderID=<?php echo $_smarty_tpl->tpl_vars['media']->value['encryptedFID'];?>
&size=<?php echo $_smarty_tpl->tpl_vars['config']->value['settings']['hpf_width'];?>
&crop=<?php echo $_smarty_tpl->tpl_vars['config']->value['settings']['hpf_crop_to'];?>
" encMediaID="<?php echo $_smarty_tpl->tpl_vars['media']->value['encryptedID'];?>
" href="<?php echo $_smarty_tpl->tpl_vars['media']->value['linkto'];?>
"><?php if ($_smarty_tpl->tpl_vars['media']->value['title']['value']||$_smarty_tpl->tpl_vars['media']->value['description']['value']){?><span class="title"><a href="<?php echo $_smarty_tpl->tpl_vars['media']->value['linkto'];?>
"><?php echo $_smarty_tpl->tpl_vars['media']->value['title']['value'];?>
</a></span><br><span class="description"><?php echo $_smarty_tpl->tpl_vars['media']->value['description']['value'];?>
</span><?php }?></li>
						<?php } ?>
					</ul>
				</div>
								
			</div>
		</div>
		<?php }?>
		<div id="contentContainer" class="center">
			<?php echo $_smarty_tpl->getSubTemplate ('search.row.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

			<div id="contentLeftContainer">
				<div>
					<?php echo $_smarty_tpl->getSubTemplate ('subnav.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

				</div>
			</div>
			<div id="contentRightContainer">
				<div>
					
					
					<?php if ($_smarty_tpl->tpl_vars['newestMediaRows']->value){?>
						<div class="homepageMediaList">
							<h1><?php if ($_smarty_tpl->tpl_vars['config']->value['settings']['newestpage']){?><a href="<?php echo linkto(array('page'=>'gallery.php?mode=newest-media&page=1'),$_smarty_tpl);?>
"><?php echo $_smarty_tpl->tpl_vars['lang']->value['newestMedia'];?>
</a><?php }else{ ?><?php echo $_smarty_tpl->tpl_vars['lang']->value['newestMedia'];?>
<?php }?><?php if ($_smarty_tpl->tpl_vars['config']->value['settings']['rss_newest']){?><a href="<?php echo linkto(array('page'=>'rss.php?mode=newestMedia'),$_smarty_tpl);?>
"><img src="<?php echo $_smarty_tpl->tpl_vars['imgPath']->value;?>
/rss.icon.small.png" class="rssH1Icon"></a><?php }?></h1>
							<?php  $_smarty_tpl->tpl_vars['media'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['media']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['newestMedia']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['media']->key => $_smarty_tpl->tpl_vars['media']->value){
$_smarty_tpl->tpl_vars['media']->_loop = true;
?>
								<?php echo $_smarty_tpl->getSubTemplate ('media.container.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

							<?php } ?>
						</div>
					<?php }?>
					
					
					<?php if ($_smarty_tpl->tpl_vars['popularMediaRows']->value){?>
						<div class="homepageMediaList">
							<h1><?php if ($_smarty_tpl->tpl_vars['config']->value['settings']['popularpage']){?><a href="<?php echo linkto(array('page'=>'gallery.php?mode=popular-media&page=1'),$_smarty_tpl);?>
"><?php echo $_smarty_tpl->tpl_vars['lang']->value['popularMedia'];?>
</a><?php }else{ ?><?php echo $_smarty_tpl->tpl_vars['lang']->value['popularMedia'];?>
<?php }?><?php if ($_smarty_tpl->tpl_vars['config']->value['settings']['rss_popular']){?><a href="<?php echo linkto(array('page'=>'rss.php?mode=popularMedia'),$_smarty_tpl);?>
"><img src="<?php echo $_smarty_tpl->tpl_vars['imgPath']->value;?>
/rss.icon.small.png" class="rssH1Icon"></a><?php }?></h1>
							<?php  $_smarty_tpl->tpl_vars['media'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['media']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['popularMedia']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['media']->key => $_smarty_tpl->tpl_vars['media']->value){
$_smarty_tpl->tpl_vars['media']->_loop = true;
?>
								<?php echo $_smarty_tpl->getSubTemplate ('media.container.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

							<?php } ?>
						</div>
					<?php }?>
					
					
					<?php if ($_smarty_tpl->tpl_vars['randomMediaRows']->value){?>
						<div class="homepageMediaList">
							<h1><?php echo $_smarty_tpl->tpl_vars['lang']->value['randomMedia'];?>
</h1>
							<?php  $_smarty_tpl->tpl_vars['media'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['media']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['randomMedia']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['media']->key => $_smarty_tpl->tpl_vars['media']->value){
$_smarty_tpl->tpl_vars['media']->_loop = true;
?>
								<?php echo $_smarty_tpl->getSubTemplate ('media.container.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

							<?php } ?>
						</div>
					<?php }?>
					
					<?php if ($_smarty_tpl->tpl_vars['subGalleriesData']->value){?>
						<h1><?php echo $_smarty_tpl->tpl_vars['lang']->value['featuredGalleries'];?>
</h1>						
						<div id="galleryListContainer">
							<?php  $_smarty_tpl->tpl_vars['subGallery'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['subGallery']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['subGalleriesData']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['subGallery']->key => $_smarty_tpl->tpl_vars['subGallery']->value){
$_smarty_tpl->tpl_vars['subGallery']->_loop = true;
?>
								<div class="galleryContainer" style="width: <?php echo $_smarty_tpl->tpl_vars['config']->value['settings']['gallery_thumb_size'];?>
px">
									<div class="galleryDetailsContainer" style="width: <?php echo $_smarty_tpl->tpl_vars['config']->value['settings']['gallery_thumb_size'];?>
px; <?php if ($_smarty_tpl->tpl_vars['galleriesData']->value[$_smarty_tpl->tpl_vars['subGallery']->value]['galleryIcon']){?>vertical-align: top<?php }?>">
										<?php if ($_smarty_tpl->tpl_vars['galleriesData']->value[$_smarty_tpl->tpl_vars['subGallery']->value]['galleryIcon']){?><p class="galleryIconContainer" style="min-width: <?php echo $_smarty_tpl->tpl_vars['galleriesData']->value[$_smarty_tpl->tpl_vars['subGallery']->value]['galleryIcon']['width'];?>
px; min-height: <?php echo $_smarty_tpl->tpl_vars['galleriesData']->value[$_smarty_tpl->tpl_vars['subGallery']->value]['galleryIcon']['height']+5;?>
px;"><a href="<?php echo $_smarty_tpl->tpl_vars['galleriesData']->value[$_smarty_tpl->tpl_vars['subGallery']->value]['linkto'];?>
"><img src="<?php echo $_smarty_tpl->tpl_vars['baseURL']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['galleriesData']->value[$_smarty_tpl->tpl_vars['subGallery']->value]['galleryIcon']['imgSrc'];?>
"></a></p><?php }?>
										<p class="galleryDetails"><?php if ($_smarty_tpl->tpl_vars['galleriesData']->value[$_smarty_tpl->tpl_vars['subGallery']->value]['password']){?><img src="<?php echo $_smarty_tpl->tpl_vars['imgPath']->value;?>
/lock.png" class="lock"><?php }?><a href="<?php echo $_smarty_tpl->tpl_vars['galleriesData']->value[$_smarty_tpl->tpl_vars['subGallery']->value]['linkto'];?>
"><?php echo $_smarty_tpl->tpl_vars['galleriesData']->value[$_smarty_tpl->tpl_vars['subGallery']->value]['name'];?>
</a><?php if ($_smarty_tpl->tpl_vars['config']->value['settings']['gallery_count']){?><?php if ($_smarty_tpl->tpl_vars['galleriesData']->value[$_smarty_tpl->tpl_vars['subGallery']->value]['gallery_count']>0||$_smarty_tpl->tpl_vars['config']->value['ShowZeroCounts']){?>&nbsp;<span class="galleryMediaCount">(<?php echo $_smarty_tpl->tpl_vars['galleriesData']->value[$_smarty_tpl->tpl_vars['subGallery']->value]['gallery_count'];?>
)</span><?php }?><?php }?></p>
									</div>
									<!--gi: <?php echo $_smarty_tpl->tpl_vars['galleriesData']->value[$_smarty_tpl->tpl_vars['subGallery']->value]['galleryIcon']['imgSrc'];?>
-->
								</div>
							<?php } ?>
						</div>
					<?php }?>
				</div>		
			</div>
		</div>
		<?php echo $_smarty_tpl->getSubTemplate ('footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

	</div>
</body>
</html><?php }} ?>