<?php /* Smarty version Smarty-3.1.8, created on 2017-01-11 19:08:01
         compiled from "/home/dynamikd/public_html/clients/piksel/assets/themes/modern/minicart.tpl" */ ?>
<?php /*%%SmartyHeaderCode:181642104058768291c872f5-76658486%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '962846673b9aaaa9145669644a4dad47d7f64c3b' => 
    array (
      0 => '/home/dynamikd/public_html/clients/piksel/assets/themes/modern/minicart.tpl',
      1 => 1412262654,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '181642104058768291c872f5-76658486',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'imgPath' => 0,
    'onlyLastAdded' => 0,
    'lang' => 0,
    'cartItems' => 0,
    'cartItem' => 0,
    'baseURL' => 0,
    'config' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.8',
  'unifunc' => 'content_58768291d9ca60_42852497',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_58768291d9ca60_42852497')) {function content_58768291d9ca60_42852497($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_truncate')) include '/home/dynamikd/public_html/clients/piksel/assets/smarty/plugins/modifier.truncate.php';
?><script>
	$(function()
	{
		$('#closeMiniCart').click(function(event){
			closeMiniCart();
		});
	});
</script>
<img src="<?php echo $_smarty_tpl->tpl_vars['imgPath']->value;?>
/close.button.png" id="closeMiniCart">
<?php if ($_smarty_tpl->tpl_vars['onlyLastAdded']->value){?>
	<p style="white-space: pre-wrap"><?php echo $_smarty_tpl->tpl_vars['lang']->value['cartItemAdded'];?>
</p><br><br>
<?php }?>
<?php if ($_smarty_tpl->tpl_vars['cartItems']->value){?>
	<div id="miniCartItemsList">
	<?php  $_smarty_tpl->tpl_vars['cartItem'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['cartItem']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['cartItems']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['cartItem']->key => $_smarty_tpl->tpl_vars['cartItem']->value){
$_smarty_tpl->tpl_vars['cartItem']->_loop = true;
?>
		<div class="divTable cartItemsList">
			<div class="divTableRow" id"cartItemRow<?php echo $_smarty_tpl->tpl_vars['cartItem']->value['oi_id'];?>
">
				<div class="divTableCell thumbRow">
					<?php echo $_smarty_tpl->tpl_vars['cartItem']->value['itemDetails']['numOf'];?>

					<?php if ($_smarty_tpl->tpl_vars['cartItem']->value['itemDetails']['media']){?>
						<a href="<?php echo linkto(array('page'=>"media.details.php?mediaID=".($_smarty_tpl->tpl_vars['cartItem']->value['itemDetails']['media']['useMediaID'])),$_smarty_tpl);?>
"><img src="<?php echo $_smarty_tpl->tpl_vars['baseURL']->value;?>
/image.php?mediaID=<?php echo $_smarty_tpl->tpl_vars['cartItem']->value['itemDetails']['media']['encryptedID'];?>
=&type=icon&folderID=<?php echo $_smarty_tpl->tpl_vars['cartItem']->value['itemDetails']['media']['encryptedFID'];?>
&size=60" class="thumb"></a>
					<?php }elseif($_smarty_tpl->tpl_vars['cartItem']->value['itemDetails']['photo']){?>
						<img src="<?php echo productShot(array('itemID'=>$_smarty_tpl->tpl_vars['cartItem']->value['item_id'],'itemType'=>$_smarty_tpl->tpl_vars['cartItem']->value['itemTypeShort'],'photoID'=>$_smarty_tpl->tpl_vars['cartItem']->value['itemDetails']['photo']['id'],'size'=>60),$_smarty_tpl);?>
" class="thumb">
					<?php }else{ ?>
						<img src="<?php echo $_smarty_tpl->tpl_vars['imgPath']->value;?>
/blank.cart.item.png">
					<?php }?>
				</div>
				<div class="divTableCell itemRow" style="position: relative">
					<h2 style="white-space: pre-wrap"><a href="<?php echo $_smarty_tpl->tpl_vars['baseURL']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['cartItem']->value['itemDetails']['cartEditLink'];?>
" class="cartItemEditLink"><?php echo $_smarty_tpl->tpl_vars['cartItem']->value['itemDetails']['name'];?>
</a></h2>
					
					<?php if ($_smarty_tpl->tpl_vars['cartItem']->value['item_type']=='digital'){?>
						<p class="cartItemDescription" style="white-space: normal">
							<?php if ($_smarty_tpl->tpl_vars['config']->value['settings']['display_license']){?><?php echo $_smarty_tpl->tpl_vars['lang']->value['license'];?>
: <?php echo $_smarty_tpl->tpl_vars['cartItem']->value['itemDetails']['licenseLang'];?>
<br><?php }?>
							<?php if ($_smarty_tpl->tpl_vars['cartItem']->value['itemDetails']['width']||$_smarty_tpl->tpl_vars['cartItem']->value['itemDetails']['height']){?><?php echo $_smarty_tpl->tpl_vars['cartItem']->value['itemDetails']['width'];?>
 x <?php echo $_smarty_tpl->tpl_vars['cartItem']->value['itemDetails']['height'];?>
 px <!--<?php if ($_smarty_tpl->tpl_vars['cartItem']->value['itemDetails']['widthIC']||$_smarty_tpl->tpl_vars['cartItem']->value['itemDetails']['heightIC']){?><em>( <?php echo $_smarty_tpl->tpl_vars['cartItem']->value['itemDetails']['widthIC'];?>
 x <?php echo $_smarty_tpl->tpl_vars['cartItem']->value['itemDetails']['heightIC'];?>
 @ <?php echo $_smarty_tpl->tpl_vars['config']->value['dpiCalc'];?>
 <?php echo $_smarty_tpl->tpl_vars['lang']->value['dpi'];?>
 )</em><?php }?>--><br><?php }?>
							<?php if ($_smarty_tpl->tpl_vars['cartItem']->value['itemDetails']['format']){?><?php echo $_smarty_tpl->tpl_vars['lang']->value['mediaLabelFormat'];?>
: <?php echo $_smarty_tpl->tpl_vars['cartItem']->value['itemDetails']['format'];?>
<br><?php }?>
							<?php if ($_smarty_tpl->tpl_vars['cartItem']->value['itemDetails']['dsp_type']=='video'){?>
								<?php if ($_smarty_tpl->tpl_vars['cartItem']->value['itemDetails']['fps']){?><?php echo $_smarty_tpl->tpl_vars['lang']->value['mediaLabelFPS'];?>
: <?php echo $_smarty_tpl->tpl_vars['cartItem']->value['itemDetails']['fps'];?>
<br><?php }?>
								<?php if ($_smarty_tpl->tpl_vars['cartItem']->value['itemDetails']['running_time']){?><?php echo $_smarty_tpl->tpl_vars['lang']->value['mediaLabelRunningTime'];?>
: <?php echo $_smarty_tpl->tpl_vars['cartItem']->value['itemDetails']['running_time'];?>
 <?php echo $_smarty_tpl->tpl_vars['lang']->value['seconds'];?>
<br><?php }?>
							<?php }?>
						</p>
					<?php }else{ ?>
						<p class="cartItemDescription" style="white-space: normal"><?php echo smarty_modifier_truncate($_smarty_tpl->tpl_vars['cartItem']->value['itemDetails']['description'],200);?>
</p>
					<?php }?>
				</div>
			</div>
		</div>
	<?php } ?>
	</div>
	<input type="button" value="<?php echo $_smarty_tpl->tpl_vars['lang']->value['viewCartUpper'];?>
" class="btn btn-xs btn-primary viewCartButton">
<?php }else{ ?>
	<?php echo $_smarty_tpl->tpl_vars['lang']->value['cartNoItems'];?>

<?php }?>
<?php }} ?>