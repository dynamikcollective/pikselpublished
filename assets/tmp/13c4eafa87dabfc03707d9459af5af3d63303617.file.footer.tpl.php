<?php /* Smarty version Smarty-3.1.8, created on 2017-01-19 12:20:27
         compiled from "/home/dynamikd/public_html/clients/piksel/assets/themes/default/footer.tpl" */ ?>
<?php /*%%SmartyHeaderCode:15643457945880af0b1c00f0-14723110%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '13c4eafa87dabfc03707d9459af5af3d63303617' => 
    array (
      0 => '/home/dynamikd/public_html/clients/piksel/assets/themes/default/footer.tpl',
      1 => 1428068340,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '15643457945880af0b1c00f0-14723110',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'contentBlocks' => 0,
    'config' => 0,
    'lang' => 0,
    'baseURL' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.8',
  'unifunc' => 'content_5880af0b21b766_70939189',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5880af0b21b766_70939189')) {function content_5880af0b21b766_70939189($_smarty_tpl) {?><?php if ($_smarty_tpl->tpl_vars['contentBlocks']->value['customBlockFooter']){?>
	<div class="cbFooter">
		<h1><?php echo $_smarty_tpl->tpl_vars['contentBlocks']->value['customBlockFooter']['name'];?>
</h1>
		<div><?php echo $_smarty_tpl->tpl_vars['contentBlocks']->value['customBlockFooter']['content'];?>
</div>
	</div>
<?php }?>
<div id="footer">	
	<?php if (!addon('unbrand')){?>
		<!-- Powered By PhotoStore | Sell Your Photos Online -->
		<p id="poweredBy">Powered By <a href="http://www.ktools.net/photostore/" target="_blank" class="photostoreLink" title="Powered By PhotoStore | Sell Your Photos Online">PhotoStore</a><br><a href="http://www.ktools.net/photostore/" target="_blank" class="sellPhotos">Sell Photos Online</a></p>
	<?php }?>
	
	<?php if ($_smarty_tpl->tpl_vars['config']->value['settings']['tospage']){?><a href="<?php echo linkto(array('page'=>'terms.of.use.php'),$_smarty_tpl);?>
"><?php echo $_smarty_tpl->tpl_vars['lang']->value['termsOfUse'];?>
</a> &nbsp;|&nbsp; <?php }?>
	<?php if ($_smarty_tpl->tpl_vars['config']->value['settings']['pppage']){?><a href="<?php echo linkto(array('page'=>'privacy.policy.php'),$_smarty_tpl);?>
"><?php echo $_smarty_tpl->tpl_vars['lang']->value['privacyPolicy'];?>
</a> &nbsp;|&nbsp; <?php }?>
	<?php if ($_smarty_tpl->tpl_vars['config']->value['settings']['papage']){?><a href="<?php echo linkto(array('page'=>'purchase.agreement.php'),$_smarty_tpl);?>
"><?php echo $_smarty_tpl->tpl_vars['lang']->value['purchaseAgreement'];?>
</a> <?php }?>
	<br>
	<?php echo $_smarty_tpl->tpl_vars['lang']->value['copyright'];?>
 <a href="<?php echo $_smarty_tpl->tpl_vars['baseURL']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['config']->value['settings']['business_name'];?>
</a>, <?php echo $_smarty_tpl->tpl_vars['lang']->value['reserved'];?>

</div>
<div id="statsCode"><?php echo $_smarty_tpl->tpl_vars['config']->value['settings']['stats_html'];?>
</div>

<?php if ($_smarty_tpl->tpl_vars['config']->value['settings']['fotomoto']){?><script type="text/javascript" src="//widget.fotomoto.com/stores/script/<?php echo $_smarty_tpl->tpl_vars['config']->value['settings']['fotomoto'];?>
.js"></script><?php }?><?php }} ?>