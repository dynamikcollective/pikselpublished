<?php /* Smarty version Smarty-3.1.8, created on 2016-11-28 16:53:12
         compiled from "/home/dynamikd/public_html/clients/piksel/assets/themes/modern/news.list.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1214399097583c60f81ceda7-67687520%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '88fda025beb9cdc74081678e5c72c9c37347e2cc' => 
    array (
      0 => '/home/dynamikd/public_html/clients/piksel/assets/themes/modern/news.list.tpl',
      1 => 1403182430,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1214399097583c60f81ceda7-67687520',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'lang' => 0,
    'config' => 0,
    'news' => 0,
    'newsArticle' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.8',
  'unifunc' => 'content_583c60f821c086_13847809',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_583c60f821c086_13847809')) {function content_583c60f821c086_13847809($_smarty_tpl) {?><!DOCTYPE HTML>
<html>
<head>
	<?php echo $_smarty_tpl->getSubTemplate ('head.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

</head>
<body>
	<?php echo $_smarty_tpl->getSubTemplate ('overlays.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

	<div id="container">
		<?php echo $_smarty_tpl->getSubTemplate ('header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

		<?php echo $_smarty_tpl->getSubTemplate ('header2.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>
		
		
		<div class="container">
			<div class="row">
				<?php echo $_smarty_tpl->getSubTemplate ('subnav.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>
		
				<div class="col-md-9">
					
					<h1><?php echo $_smarty_tpl->tpl_vars['lang']->value['news'];?>
<?php if ($_smarty_tpl->tpl_vars['config']->value['settings']['rss_news']){?>&nbsp;<a href="<?php echo linkto(array('page'=>'rss.php?mode=news'),$_smarty_tpl);?>
" class="btn btn-xxs btn-warning"><?php echo $_smarty_tpl->tpl_vars['lang']->value['rss'];?>
</a><?php }?></h1>
					<hr>
					<?php if ($_smarty_tpl->tpl_vars['news']->value){?>
						<?php  $_smarty_tpl->tpl_vars['newsArticle'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['newsArticle']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['news']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['newsArticle']->key => $_smarty_tpl->tpl_vars['newsArticle']->value){
$_smarty_tpl->tpl_vars['newsArticle']->_loop = true;
?>
							<h2 class="newsDate"><?php echo $_smarty_tpl->tpl_vars['newsArticle']->value['display_date'];?>
</h2>
							<div class="newsArticle">
								<p class="newsTitle"><a href="<?php echo $_smarty_tpl->tpl_vars['newsArticle']->value['linkto'];?>
"><?php echo $_smarty_tpl->tpl_vars['newsArticle']->value['title'];?>
</a></p>
								<p class="newsShort"><?php echo $_smarty_tpl->tpl_vars['newsArticle']->value['short'];?>
</p>
								<?php if ($_smarty_tpl->tpl_vars['newsArticle']->value['article']!=''){?><p class="text-right"><a href="<?php echo $_smarty_tpl->tpl_vars['newsArticle']->value['linkto'];?>
" class="btn btn-xs btn-primary"><?php echo $_smarty_tpl->tpl_vars['lang']->value['more'];?>
</a></p><?php }?>
							</div>
						<?php } ?>
					<?php }else{ ?>
						<p class="notice"><?php echo $_smarty_tpl->tpl_vars['lang']->value['noNews'];?>
</p>
					<?php }?>
					
				</div>
			</div>
		</div>
		<?php echo $_smarty_tpl->getSubTemplate ('footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

    </div>
</body>
</html><?php }} ?>