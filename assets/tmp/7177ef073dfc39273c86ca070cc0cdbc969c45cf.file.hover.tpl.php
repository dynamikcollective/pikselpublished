<?php /* Smarty version Smarty-3.1.8, created on 2016-11-28 19:04:04
         compiled from "/home/dynamikd/public_html/clients/piksel/assets/themes/modern/hover.tpl" */ ?>
<?php /*%%SmartyHeaderCode:701401497583c7fa4e4ae62-89869013%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '7177ef073dfc39273c86ca070cc0cdbc969c45cf' => 
    array (
      0 => '/home/dynamikd/public_html/clients/piksel/assets/themes/modern/hover.tpl',
      1 => 1428314958,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '701401497583c7fa4e4ae62-89869013',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'media' => 0,
    'config' => 0,
    'baseURL' => 0,
    'detail' => 0,
    'imgPath' => 0,
    'stars' => 0,
    'lang' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.8',
  'unifunc' => 'content_583c7fa4ef1cd9_47660002',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_583c7fa4ef1cd9_47660002')) {function content_583c7fa4ef1cd9_47660002($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_truncate')) include '/home/dynamikd/public_html/clients/piksel/assets/smarty/plugins/modifier.truncate.php';
?><?php if ($_smarty_tpl->tpl_vars['media']->value['videoStatus']){?>
	<div id="hoverMediaContainer" class="hoverMediaContainer" style="width: <?php echo $_smarty_tpl->tpl_vars['config']->value['settings']['video_rollover_width'];?>
px; height: <?php echo $_smarty_tpl->tpl_vars['config']->value['settings']['video_rollover_height'];?>
px; background-image: none;"><p id="vidContainer"></p></div>
	<script type="text/javascript">
		$(function()
		{	
			setTimeout(function()
			{
				//./assets/library/2011-11-30/samples/video_cpx-jd3.mp4
				//'flashplayer': "./assets/jwplayer/player.swf",
				//'screencolor': '<?php echo $_smarty_tpl->tpl_vars['config']->value['settings']['video_bg_color'];?>
',
				//video.php?mediaID=<?php echo $_smarty_tpl->tpl_vars['media']->value['encryptedID'];?>

				//$('#vidContainer').text('test');
				
				jwplayer("vidContainer").setup(
				{
					'file': "<?php echo $_smarty_tpl->tpl_vars['media']->value['videoInfo']['url'];?>
",
					'autostart': false,
					'type': 'video',
					'repeat': 'always',
					'controlbar.position': 'none',
					'stretching': 'uniform',
					'logo.file': '<?php echo $_smarty_tpl->tpl_vars['baseURL']->value;?>
/assets/watermarks/<?php echo $_smarty_tpl->tpl_vars['config']->value['settings']['vidrollover_wm'];?>
',
					'logo.hide': false,
					'logo.position': '<?php echo $_smarty_tpl->tpl_vars['config']->value['settings']['video_wmpos'];?>
',
					'width': '100%',
					'height': '100%',
					'volume': 100,
					'modes': [
						{ 'type': <?php if ($_smarty_tpl->tpl_vars['media']->value['videoInfo']['vidsample_extension']=='flv'||$_smarty_tpl->tpl_vars['config']->value['forceFlashVideoPlayer']){?>'flash', src: '<?php echo $_smarty_tpl->tpl_vars['baseURL']->value;?>
/assets/jwplayer/player.swf'<?php }else{ ?>'html5'<?php }?> },
						{ 'type': 'download' }
					]
				});
				
				jwplayer("vidContainer").play();
				
				//$('#vidContainer').text('test');
				
				<?php if ($_smarty_tpl->tpl_vars['config']->value['settings']['video_autoresize']){?>
					jwplayer("vidContainer").onMeta(function()
					{
						vidWindowResize("vidContainer");
					});
				<?php }?>
				
			},100); // Added a short delay to allow for the hover window to fade in first
			//alert('done');
		});
	</script>
<?php }else{ ?>
	<p id="hoverMediaContainer" class="hoverMediaContainer"><img src="<?php if ($_smarty_tpl->tpl_vars['media']->value['rolloverCachedLink']){?><?php echo $_smarty_tpl->tpl_vars['media']->value['rolloverCachedLink'];?>
<?php }else{ ?><?php echo mediaImage(array('mediaID'=>$_smarty_tpl->tpl_vars['media']->value['encryptedID'],'type'=>'rollover','folderID'=>$_smarty_tpl->tpl_vars['media']->value['encryptedFID']),$_smarty_tpl);?>
<?php }?>" class="hoverThumb" style="width: <?php echo $_smarty_tpl->tpl_vars['media']->value['width'];?>
px; height: <?php echo $_smarty_tpl->tpl_vars['media']->value['height'];?>
px;"></p>
<?php }?>
<?php if ($_smarty_tpl->tpl_vars['media']->value['details']||$_smarty_tpl->tpl_vars['media']->value['showRating']){?>
	<div>
		<ul class="mediaContent">
		<?php  $_smarty_tpl->tpl_vars['detail'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['detail']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['media']->value['details']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['detail']->key => $_smarty_tpl->tpl_vars['detail']->value){
$_smarty_tpl->tpl_vars['detail']->_loop = true;
?>
			<?php if ($_smarty_tpl->tpl_vars['detail']->value['value']!=''){?>			
				<li>
				<span class="mediaDetailLabel mediaDetailLabel<?php echo $_smarty_tpl->tpl_vars['detail']->key;?>
"><?php echo $_smarty_tpl->tpl_vars['detail']->value['lang'];?>
</span>: <span class="mediaDetailValue mediaDetailValue<?php echo $_smarty_tpl->tpl_vars['detail']->key;?>
">
					<?php if ($_smarty_tpl->tpl_vars['detail']->key=='owner'){?>
						<?php echo $_smarty_tpl->tpl_vars['detail']->value['value']['displayName'];?>

					<?php }else{ ?>				
						<?php echo smarty_modifier_truncate($_smarty_tpl->tpl_vars['detail']->value['value'],40);?>

					<?php }?>
				</span>
				</li>
				
			<?php }?>
		<?php } ?>
		<?php if ($_smarty_tpl->tpl_vars['media']->value['showRating']){?>
			<li>
				<p class="ratingStarsContainer" id="ratingStarsContainer<?php echo $_smarty_tpl->tpl_vars['media']->key;?>
">
					<?php  $_smarty_tpl->tpl_vars['stars'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['stars']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['media']->value['rating']['stars']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['stars']->key => $_smarty_tpl->tpl_vars['stars']->value){
$_smarty_tpl->tpl_vars['stars']->_loop = true;
?><img src="<?php echo $_smarty_tpl->tpl_vars['imgPath']->value;?>
/star.<?php echo $_smarty_tpl->tpl_vars['stars']->value;?>
.png" class="ratingStar"><?php } ?>
					&nbsp;<span class="mediaDetailValue"><strong><?php echo $_smarty_tpl->tpl_vars['media']->value['rating']['average'];?>
</strong>/<?php echo $_smarty_tpl->tpl_vars['config']->value['RatingStars'];?>
 (<?php echo $_smarty_tpl->tpl_vars['media']->value['rating']['votes'];?>
 <?php echo $_smarty_tpl->tpl_vars['lang']->value['votes'];?>
)</span><br>
				</p>
			</li>
		<?php }else{ ?>
			<li style="display: none;"></li><!-- Added so that last-child doesn't float right unless it is ratings-->
		<?php }?>
		</ul>
	</div>
<?php }?><?php }} ?>