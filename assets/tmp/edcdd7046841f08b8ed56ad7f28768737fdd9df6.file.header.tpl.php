<?php /* Smarty version Smarty-3.1.8, created on 2017-01-19 12:20:26
         compiled from "/home/dynamikd/public_html/clients/piksel/assets/themes/default/header.tpl" */ ?>
<?php /*%%SmartyHeaderCode:20344412895880af0adbc1f0-16053542%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'edcdd7046841f08b8ed56ad7f28768737fdd9df6' => 
    array (
      0 => '/home/dynamikd/public_html/clients/piksel/assets/themes/default/header.tpl',
      1 => 1436259368,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '20344412895880af0adbc1f0-16053542',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'mainLogo' => 1,
    'displayLanguages' => 1,
    'lang' => 1,
    'selectedLanguage' => 1,
    'activeLanguages' => 1,
    'baseURL' => 1,
    'language' => 1,
    'config' => 1,
    'loggedIn' => 1,
    'member' => 1,
    'lightboxSystem' => 1,
    'featuredTab' => 1,
    'contribLink' => 1,
    'imgPath' => 1,
    'currentGallery' => 1,
    'cartStatus' => 1,
    'displayCurrencies' => 1,
    'selectedCurrency' => 1,
    'currency' => 1,
    'activeCurrencies' => 1,
    'cartTotals' => 1,
    'currencySystem' => 1,
    'creditSystem' => 1,
    'message' => 1,
    'messageLang' => 1,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.8',
  'unifunc' => 'content_5880af0b0dd1c4_69673909',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5880af0b0dd1c4_69673909')) {function content_5880af0b0dd1c4_69673909($_smarty_tpl) {?>

<div id="header">
	<div id="logoContainer"><a href="<?php echo linkto(array('page'=>"index.php"),$_smarty_tpl);?>
"><img src="<?php echo $_smarty_tpl->tpl_vars['mainLogo']->value;?>
" id="mainLogo"></a></div>
	<div id="headerMemberDetailsArea">
		
		<?php if (count($_smarty_tpl->tpl_vars['displayLanguages']->value)>1){?>
			<div id="languageSelector">
				&nbsp;&nbsp;|&nbsp;&nbsp;<?php echo $_smarty_tpl->tpl_vars['lang']->value['language'];?>
<?php if ($_smarty_tpl->tpl_vars['activeLanguages']->value[$_smarty_tpl->tpl_vars['selectedLanguage']->value]['flag']){?><img src="<?php echo $_smarty_tpl->tpl_vars['baseURL']->value;?>
/assets/languages/<?php echo $_smarty_tpl->tpl_vars['selectedLanguage']->value;?>
/flag.png"><?php }?>
				<ul class="dropshadowdark">
					<?php  $_smarty_tpl->tpl_vars['language'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['language']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['displayLanguages']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['language']->key => $_smarty_tpl->tpl_vars['language']->value){
$_smarty_tpl->tpl_vars['language']->_loop = true;
?>
						<li <?php if ($_smarty_tpl->tpl_vars['language']->key==$_smarty_tpl->tpl_vars['selectedLanguage']->value){?>id="selectedLanguage"<?php }?>><?php if ($_smarty_tpl->tpl_vars['activeLanguages']->value[$_smarty_tpl->tpl_vars['language']->key]['flag']){?><img src="<?php echo $_smarty_tpl->tpl_vars['baseURL']->value;?>
/assets/languages/<?php echo $_smarty_tpl->tpl_vars['language']->key;?>
/flag.png"><?php }?><a href="<?php echo linkto(array('page'=>"actions.php?action=changeLanguage&setLanguage=".($_smarty_tpl->tpl_vars['language']->key)),$_smarty_tpl);?>
"><?php echo $_smarty_tpl->tpl_vars['language']->value;?>
</a></li>
					<?php } ?>
				</ul>
			</div>
			
		<?php }?>
		
		
		<?php if ($_smarty_tpl->tpl_vars['config']->value['settings']['display_login']){?>
			<?php if ($_smarty_tpl->tpl_vars['loggedIn']->value){?>
				<strong><a href="<?php echo linkto(array('page'=>"members.php"),$_smarty_tpl);?>
"><?php echo $_smarty_tpl->tpl_vars['member']->value['f_name'];?>
 <?php echo $_smarty_tpl->tpl_vars['member']->value['l_name'];?>
</a></strong>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="<?php echo linkto(array('page'=>"login.php?cmd=logout"),$_smarty_tpl);?>
"><?php echo $_smarty_tpl->tpl_vars['lang']->value['logout'];?>
</a>
			<?php }else{ ?>
				<a href="<?php echo linkto(array('page'=>"login.php?jumpTo=members"),$_smarty_tpl);?>
"><?php echo $_smarty_tpl->tpl_vars['lang']->value['login'];?>
</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="<?php echo linkto(array('page'=>"create.account.php?jumpTo=members"),$_smarty_tpl);?>
"><?php echo $_smarty_tpl->tpl_vars['lang']->value['createAccount'];?>
</a>
			<?php }?>
			
		<?php }?>
		
		
		<?php if ($_smarty_tpl->tpl_vars['lightboxSystem']->value){?>
			&nbsp;&nbsp;|&nbsp;&nbsp;<a href="<?php echo linkto(array('page'=>"lightboxes.php"),$_smarty_tpl);?>
"><?php echo $_smarty_tpl->tpl_vars['lang']->value['lightboxes'];?>
</a>
		<?php }?>
	</div>
</div>


<div id="topNav" style="overflow: auto;">
	<ul style="float: left">
		<?php if ($_smarty_tpl->tpl_vars['config']->value['settings']['news']){?><li id="navNews"><a href="<?php echo linkto(array('page'=>"news.php"),$_smarty_tpl);?>
"><?php echo $_smarty_tpl->tpl_vars['lang']->value['news'];?>
</a></li><?php }?>
		<?php if ($_smarty_tpl->tpl_vars['featuredTab']->value){?>
			<li id="featuredNavButton">
				<a href="#"><?php echo $_smarty_tpl->tpl_vars['lang']->value['featuredItems'];?>
</a>
				<ul>
					<?php if ($_smarty_tpl->tpl_vars['config']->value['settings']['featuredpage']){?><li id="featuredSubnavMedia"><a href="<?php echo linkto(array('page'=>"gallery.php?mode=featured-media&page=1"),$_smarty_tpl);?>
"><?php echo $_smarty_tpl->tpl_vars['lang']->value['mediaNav'];?>
</a></li><?php }?>
					<?php if ($_smarty_tpl->tpl_vars['config']->value['settings']['printpage']){?><li id="featuredSubnavPrints"><a href="<?php echo linkto(array('page'=>"featured.php?mode=prints"),$_smarty_tpl);?>
"><?php echo $_smarty_tpl->tpl_vars['lang']->value['prints'];?>
</a></li><?php }?>
					<?php if ($_smarty_tpl->tpl_vars['config']->value['settings']['prodpage']){?><li id="featuredSubnavProducts"><a href="<?php echo linkto(array('page'=>"featured.php?mode=products"),$_smarty_tpl);?>
"><?php echo $_smarty_tpl->tpl_vars['lang']->value['products'];?>
</a></li><?php }?>
					<?php if ($_smarty_tpl->tpl_vars['config']->value['settings']['packpage']){?><li id="featuredSubnavPackages"><a href="<?php echo linkto(array('page'=>"featured.php?mode=packages"),$_smarty_tpl);?>
"><?php echo $_smarty_tpl->tpl_vars['lang']->value['packages'];?>
</a></li><?php }?>
					<?php if ($_smarty_tpl->tpl_vars['config']->value['settings']['collpage']){?><li id="featuredSubnavCollections"><a href="<?php echo linkto(array('page'=>"featured.php?mode=collections"),$_smarty_tpl);?>
"><?php echo $_smarty_tpl->tpl_vars['lang']->value['collections'];?>
</a></li><?php }?>
					<?php if ($_smarty_tpl->tpl_vars['config']->value['settings']['subpage']&&$_smarty_tpl->tpl_vars['config']->value['settings']['subscriptions']){?><li id="featuredSubnavSubscriptions"><a href="<?php echo linkto(array('page'=>"featured.php?mode=subscriptions"),$_smarty_tpl);?>
"><?php echo $_smarty_tpl->tpl_vars['lang']->value['subscriptions'];?>
</a></li><?php }?>
					<?php if ($_smarty_tpl->tpl_vars['config']->value['settings']['creditpage']){?><li id="featuredSubnavCredits"><a href="<?php echo linkto(array('page'=>"featured.php?mode=credits"),$_smarty_tpl);?>
"><?php echo $_smarty_tpl->tpl_vars['lang']->value['credits'];?>
</a></li><?php }?>
				</ul>
			</li>
		<?php }?>
		<li id="navGalleries"><a href="<?php echo linkto(array('page'=>"gallery.php?mode=gallery"),$_smarty_tpl);?>
"><?php echo $_smarty_tpl->tpl_vars['lang']->value['galleries'];?>
</a></li>
		<?php if ($_smarty_tpl->tpl_vars['config']->value['settings']['newestpage']){?><li id="navNewestMedia"><a href="<?php echo linkto(array('page'=>"gallery.php?mode=newest-media&page=1"),$_smarty_tpl);?>
"><?php echo $_smarty_tpl->tpl_vars['lang']->value['newestMedia'];?>
</a></li><?php }?>
		<?php if ($_smarty_tpl->tpl_vars['config']->value['settings']['popularpage']){?><li id="navPopularMedia"><a href="<?php echo linkto(array('page'=>"gallery.php?mode=popular-media&page=1"),$_smarty_tpl);?>
"><?php echo $_smarty_tpl->tpl_vars['lang']->value['popularMedia'];?>
</a></li><?php }?>
		<?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['contribLink']->value;?>
<?php $_tmp1=ob_get_clean();?><?php if (addon('contr')&&$_tmp1==1){?><li id="navContributors"><a href="<?php echo linkto(array('page'=>"contributors.php"),$_smarty_tpl);?>
"><?php echo $_smarty_tpl->tpl_vars['lang']->value['contributors'];?>
</a></li><?php }?>
		<?php if ($_smarty_tpl->tpl_vars['config']->value['settings']['promopage']){?><li id="navPromotions"><a href="<?php echo linkto(array('page'=>"promotions.php"),$_smarty_tpl);?>
"><?php echo $_smarty_tpl->tpl_vars['lang']->value['promotions'];?>
</a></li><?php }?>
		<?php if ($_smarty_tpl->tpl_vars['config']->value['settings']['aboutpage']){?><li id="navAboutUs"><a href="<?php echo linkto(array('page'=>"about.php"),$_smarty_tpl);?>
"><?php echo $_smarty_tpl->tpl_vars['lang']->value['aboutUs'];?>
</a></li><?php }?>
		<?php if ($_smarty_tpl->tpl_vars['config']->value['settings']['contact']){?><li id="navContactUs"><a href="<?php echo linkto(array('page'=>"contact.php"),$_smarty_tpl);?>
"><?php echo $_smarty_tpl->tpl_vars['lang']->value['contactUs'];?>
</a></li><?php }?>
		<?php if ($_smarty_tpl->tpl_vars['config']->value['settings']['forum_link']){?><li id="navForum"><a href="<?php echo $_smarty_tpl->tpl_vars['config']->value['settings']['forum_link'];?>
"><?php echo $_smarty_tpl->tpl_vars['lang']->value['forum'];?>
</a></li><?php }?>
	</ul>
	<div id="social" style="float: right; text-align: right; margin-right: 10px; margin-top: 4px;"><?php if ($_smarty_tpl->tpl_vars['config']->value['settings']['facebook_link']){?><a href="<?php echo $_smarty_tpl->tpl_vars['config']->value['settings']['facebook_link'];?>
" target="_blank"><img src="<?php echo $_smarty_tpl->tpl_vars['imgPath']->value;?>
/facebook.icon.png" width="20" title="Facebook" style="padding-top: 6px;"></a><?php }?>&nbsp;<?php if ($_smarty_tpl->tpl_vars['config']->value['settings']['twitter_link']){?><a href="<?php echo $_smarty_tpl->tpl_vars['config']->value['settings']['twitter_link'];?>
" target="_blank"><img src="<?php echo $_smarty_tpl->tpl_vars['imgPath']->value;?>
/twitter.icon.png" width="20" title="Twitter"></a><?php }?></div>
</div>
<div id="searchBar">
	
	<?php if ($_smarty_tpl->tpl_vars['config']->value['settings']['search']){?>
		<form action="<?php echo linkto(array('page'=>"search.php"),$_smarty_tpl);?>
" method="get" id="searchFormTest">
		<input type="hidden" name="clearSearch" value="true">
		<div class="headerSearchBox"><input type="text" id="searchPhrase" name="searchPhrase" class="searchInputBox" value="<?php echo $_smarty_tpl->tpl_vars['lang']->value['enterKeywords'];?>
"></div>
		<?php if ($_smarty_tpl->tpl_vars['currentGallery']->value['gallery_id']){?><div class="headerSearchBox headerSearchBoxCG"><input type="checkbox" name="galleries" id="searchCurrentGallery" value="<?php echo $_smarty_tpl->tpl_vars['currentGallery']->value['gallery_id'];?>
" checked="checked"> <label for="searchCurrentGallery"><?php echo $_smarty_tpl->tpl_vars['lang']->value['curGalleryOnly'];?>
</label></p></div><?php }?>
		<div class="eyeGlass"></div>
		<div class="headerSearchBox headerSearchBoxOption"><a href="<?php echo linkto(array('page'=>'search.php'),$_smarty_tpl);?>
"><?php echo $_smarty_tpl->tpl_vars['lang']->value['advancedSearch'];?>
</a></div>
		</form>
	<?php }?>
	
	
	<?php if ($_smarty_tpl->tpl_vars['config']->value['settings']['esearch']){?>
		<div class="headerSearchBox"><a href="<?php echo linkto(array('page'=>"esearch.php"),$_smarty_tpl);?>
"><?php echo $_smarty_tpl->tpl_vars['lang']->value['eventSearch'];?>
</a></div>
	<?php }?>
	
	
	<?php if ($_smarty_tpl->tpl_vars['cartStatus']->value){?>
		<div id="headerCartBox">
			<?php if (count($_smarty_tpl->tpl_vars['displayCurrencies']->value)>1){?>
				<div id="currencySelector">
					<span id="currentCurrency"><?php echo $_smarty_tpl->tpl_vars['lang']->value['currency'];?>
</span>
					<ul class="dropshadowdark">
						<?php  $_smarty_tpl->tpl_vars['currency'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['currency']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['displayCurrencies']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['currency']->key => $_smarty_tpl->tpl_vars['currency']->value){
$_smarty_tpl->tpl_vars['currency']->_loop = true;
?>
							<li <?php if ($_smarty_tpl->tpl_vars['currency']->key==$_smarty_tpl->tpl_vars['selectedCurrency']->value){?>id="selectedCurrency"<?php }?>><a href="<?php echo linkto(array('page'=>"actions.php?action=changeCurrency&setCurrency=".($_smarty_tpl->tpl_vars['currency']->key)),$_smarty_tpl);?>
"><?php echo $_smarty_tpl->tpl_vars['currency']->value;?>
 (<?php echo $_smarty_tpl->tpl_vars['activeCurrencies']->value[$_smarty_tpl->tpl_vars['currency']->key]['code'];?>
)</a></li><!--onclick="changeCurrency('<?php echo $_smarty_tpl->tpl_vars['currency']->key;?>
');"-->
						<?php } ?>
					</ul>
				</div>
				
			<?php }?>
			<div id="cartPreviewContainer">
				<div id="miniCartContainer">LOADING</div>
				<div style="float: left; position: relative;" class="viewCartLink"><p id="cartItemsCount"><?php echo $_smarty_tpl->tpl_vars['cartTotals']->value['itemsInCart'];?>
</p><a href="<?php echo linkto(array('page'=>"cart.php"),$_smarty_tpl);?>
"><img src="<?php echo $_smarty_tpl->tpl_vars['imgPath']->value;?>
/cart.icon.png" alt="<?php echo $_smarty_tpl->tpl_vars['lang']->value['cart'];?>
"></a></div>
				<div style="float: left; display:<?php if ($_smarty_tpl->tpl_vars['cartTotals']->value['priceSubTotal']||$_smarty_tpl->tpl_vars['cartTotals']->value['creditsSubTotalPreview']){?>block<?php }else{ ?>none<?php }?>;" id="cartPreview">
					<a href="<?php echo linkto(array('page'=>"cart.php"),$_smarty_tpl);?>
" class="viewCartLink">
					<span id="cartPreviewPrice" style="<?php if (!$_smarty_tpl->tpl_vars['currencySystem']->value){?>display: none;<?php }?>"><?php echo $_smarty_tpl->tpl_vars['cartTotals']->value['priceSubTotalPreview']['display'];?>
</span><!-- with tax <?php echo $_smarty_tpl->tpl_vars['cartTotals']->value['totalLocal']['display'];?>
-->
					<?php if ($_smarty_tpl->tpl_vars['creditSystem']->value&&$_smarty_tpl->tpl_vars['currencySystem']->value){?> + <?php }?>
					<span id="cartPreviewCredits" style="<?php if (!$_smarty_tpl->tpl_vars['creditSystem']->value){?>display: none;<?php }?>"><?php echo $_smarty_tpl->tpl_vars['cartTotals']->value['creditsSubTotalPreview'];?>
 </span> <?php if ($_smarty_tpl->tpl_vars['creditSystem']->value){?><?php echo $_smarty_tpl->tpl_vars['lang']->value['credits'];?>
<?php }?>
					</a>
				</div>
			</div>
		</div>
	<?php }?>
</div>

<?php if ($_smarty_tpl->tpl_vars['message']->value){?>
	<?php  $_smarty_tpl->tpl_vars['messageLang'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['messageLang']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['message']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['messageLang']->key => $_smarty_tpl->tpl_vars['messageLang']->value){
$_smarty_tpl->tpl_vars['messageLang']->_loop = true;
?>
		<div class="messageBar"><img src="<?php echo $_smarty_tpl->tpl_vars['imgPath']->value;?>
/notice.icon.png"><?php echo $_smarty_tpl->tpl_vars['lang']->value[$_smarty_tpl->tpl_vars['messageLang']->value];?>
 <a href="#" class="buttonLink">X</a></div>
	<?php } ?>
<?php }?>
<?php }} ?>