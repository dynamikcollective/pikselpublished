<?php /* Smarty version Smarty-3.1.8, created on 2017-01-12 17:51:04
         compiled from "/home/dynamikd/public_html/clients/piksel/assets/themes/modern/featured.products.tpl" */ ?>
<?php /*%%SmartyHeaderCode:17763023235877c208b80a81-63444794%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'f23b6425dd92c8ca73a04745f09a49e7eee41baf' => 
    array (
      0 => '/home/dynamikd/public_html/clients/piksel/assets/themes/modern/featured.products.tpl',
      1 => 1404911258,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '17763023235877c208b80a81-63444794',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'baseURL' => 0,
    'lang' => 0,
    'featuredProductsRows' => 0,
    'featuredProducts' => 0,
    'product' => 0,
    'config' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.8',
  'unifunc' => 'content_5877c208bfc9f8_61547575',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5877c208bfc9f8_61547575')) {function content_5877c208bfc9f8_61547575($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_truncate')) include '/home/dynamikd/public_html/clients/piksel/assets/smarty/plugins/modifier.truncate.php';
?><!DOCTYPE HTML>
<html>
<head>
	<?php echo $_smarty_tpl->getSubTemplate ('head.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

	<script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['baseURL']->value;?>
/assets/javascript/featured.page.js"></script>
</head>
<body>
	<?php echo $_smarty_tpl->getSubTemplate ('overlays.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

	<div id="container">
		<?php echo $_smarty_tpl->getSubTemplate ('header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

		<?php echo $_smarty_tpl->getSubTemplate ('header2.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>
		
		
		<div class="container">
			<div class="row">
				<?php echo $_smarty_tpl->getSubTemplate ('subnav.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>
		
				<div class="col-md-9">
					
					<h1><?php echo $_smarty_tpl->tpl_vars['lang']->value['featuredProducts'];?>
</h1>
					<hr>
					<?php if ($_smarty_tpl->tpl_vars['featuredProductsRows']->value){?>
						<?php  $_smarty_tpl->tpl_vars['product'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['product']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['featuredProducts']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['product']->key => $_smarty_tpl->tpl_vars['product']->value){
$_smarty_tpl->tpl_vars['product']->_loop = true;
?>
							<div class="featuredPageItem workboxLinkAttach">
								<h2><a href="<?php echo $_smarty_tpl->tpl_vars['product']->value['linkto'];?>
" class="workboxLink"><?php echo $_smarty_tpl->tpl_vars['product']->value['name'];?>
</a></h2>
								<p class="description"><?php if ($_smarty_tpl->tpl_vars['product']->value['photo']){?><img src="<?php echo productShot(array('itemID'=>$_smarty_tpl->tpl_vars['product']->value['prod_id'],'itemType'=>'prod','photoID'=>$_smarty_tpl->tpl_vars['product']->value['photo']['id'],'size'=>125),$_smarty_tpl);?>
"><br><br><?php }?><?php echo smarty_modifier_truncate($_smarty_tpl->tpl_vars['product']->value['description'],360);?>
</p>
								<p class="moreInfo"><!--<input type="button" value="<?php echo $_smarty_tpl->tpl_vars['lang']->value['more'];?>
">--><?php if ($_smarty_tpl->tpl_vars['product']->value['price']){?><span class="price"><?php echo $_smarty_tpl->tpl_vars['product']->value['price']['display'];?>
</span><?php if ($_smarty_tpl->tpl_vars['product']->value['price']['taxInc']){?> <span class="taxIncMessage">(<?php echo $_smarty_tpl->tpl_vars['lang']->value['taxIncMessage'];?>
)</span><?php }?><?php }?><?php if ($_smarty_tpl->tpl_vars['config']->value['settings']['cart']==3&&$_smarty_tpl->tpl_vars['config']->value['settings']['credits_prod']){?> <?php echo $_smarty_tpl->tpl_vars['lang']->value['priceCreditSep'];?>
 <?php }?><?php if ($_smarty_tpl->tpl_vars['product']->value['credits']){?><span class="price"><?php echo $_smarty_tpl->tpl_vars['product']->value['credits'];?>
 <?php echo $_smarty_tpl->tpl_vars['lang']->value['mediaLabelCredits'];?>
</span><?php }?></p>
							</div>
						<?php } ?>
					<?php }else{ ?>
						<p class="notice"><?php echo $_smarty_tpl->tpl_vars['lang']->value['noFeatured'];?>
</p>
					<?php }?>
					
				</div>
			</div>
		</div>
		<?php echo $_smarty_tpl->getSubTemplate ('footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

    </div>
</body>
</html><?php }} ?>