<?php /* Smarty version Smarty-3.1.8, created on 2017-01-11 19:07:57
         compiled from "/home/dynamikd/public_html/clients/piksel/assets/themes/modern/digital.tpl" */ ?>
<?php /*%%SmartyHeaderCode:5138842395876828d790363-21864976%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '1aaf7b49f5da7906290325876ca97e8d86c216e7' => 
    array (
      0 => '/home/dynamikd/public_html/clients/piksel/assets/themes/modern/digital.tpl',
      1 => 1429527296,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '5138842395876828d790363-21864976',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'baseURL' => 0,
    'digital' => 0,
    'imgPath' => 0,
    'useID' => 0,
    'edit' => 0,
    'useMediaID' => 0,
    'loggedIn' => 0,
    'noAccess' => 0,
    'lang' => 0,
    'media' => 0,
    'id' => 0,
    'mediaID' => 0,
    'customizeID' => 0,
    'config' => 0,
    'rmBasePrice' => 0,
    'rmBaseCredits' => 0,
    'topLevelRM' => 0,
    'rmGroup' => 0,
    'option' => 0,
    'subRows' => 0,
    'downloadRows' => 0,
    'subsAvailable' => 0,
    'subsArray' => 0,
    'subscription' => 0,
    'cartStatus' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.8',
  'unifunc' => 'content_5876828da251c5_21493666',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5876828da251c5_21493666')) {function content_5876828da251c5_21493666($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_truncate')) include '/home/dynamikd/public_html/clients/piksel/assets/smarty/plugins/modifier.truncate.php';
?><script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['baseURL']->value;?>
/assets/javascript/digital.js"></script>
<script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['baseURL']->value;?>
/assets/javascript/workbox.js"></script>
<?php if ($_smarty_tpl->tpl_vars['digital']->value['lic_purchase_type']=='rm'){?><script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['baseURL']->value;?>
/assets/javascript/rights.managed.js"></script><?php }?>

<form class="cleanForm form-group" action="<?php echo linkto(array('page'=>'cart.php'),$_smarty_tpl);?>
" id="workboxItemForm" method="post">
<img src="<?php echo $_smarty_tpl->tpl_vars['imgPath']->value;?>
/close.button.png" id="closeWorkbox">
<!-- For cart -->
<input type="hidden" name="mode" value="add" id="mode"> 
<input type="hidden" name="type" value="digital" id="type">
<input type="hidden" name="profileID" id="profileID" value="<?php echo $_smarty_tpl->tpl_vars['useID']->value;?>
">
<input type="hidden" name="id" id="id" value="<?php echo $_smarty_tpl->tpl_vars['useID']->value;?>
">
<?php if ($_smarty_tpl->tpl_vars['edit']->value){?><input type="hidden" name="edit" value="<?php echo $_smarty_tpl->tpl_vars['edit']->value;?>
"><?php }?>
<!--<input type="hidden" name="customizeID" id="customizeID" value="<?php echo $_smarty_tpl->tpl_vars['digital']->value['useCustomizeID'];?>
">-->
<input type="hidden" name="mediaID" id="mediaID" value="<?php echo $_smarty_tpl->tpl_vars['useMediaID']->value;?>
">
<input type="hidden" id="instantDownloadAvailable" name="instantDownloadAvailable" value="<?php if ($_smarty_tpl->tpl_vars['digital']->value['fileCheck']['status']||$_smarty_tpl->tpl_vars['digital']->value['autoCreate']){?>1<?php }?>">
<input type="hidden" id="loggedIn" name="loggedIn" value="<?php echo $_smarty_tpl->tpl_vars['loggedIn']->value;?>
">
<?php if (!$_smarty_tpl->tpl_vars['digital']->value['fileCheck']['status']&&!$_smarty_tpl->tpl_vars['digital']->value['autoCreate']){?><input type="hidden" name="action" value="emailForFile"><?php }?>
<input type="hidden" name="licenseType" id="licenseType" value="<?php echo $_smarty_tpl->tpl_vars['digital']->value['lic_purchase_type'];?>
">

<?php if ($_smarty_tpl->tpl_vars['noAccess']->value){?>
	<p class="notice"><?php echo $_smarty_tpl->tpl_vars['lang']->value['noAccess'];?>
</p>
<?php }else{ ?>
	<!--<?php echo $_smarty_tpl->tpl_vars['digital']->value['downloadKey'];?>
-->
	<div class="container">
		<div class="row">
			<div class="col-md-5">
				<?php if ($_smarty_tpl->tpl_vars['media']->value){?>
					<div class="workboxLeftColumn">					
						<p id="mainShotContainer"><img src="<?php echo mediaImage(array('mediaID'=>$_smarty_tpl->tpl_vars['media']->value['encryptedID'],'type'=>'rollover','folderID'=>$_smarty_tpl->tpl_vars['media']->value['encryptedFID'],'size'=>300),$_smarty_tpl);?>
"></p>
						<ul class="purchaseListDescription">
							<!--
							<li>Profile: <?php echo $_smarty_tpl->tpl_vars['id']->value;?>
</li>
							<li>Media: <?php echo $_smarty_tpl->tpl_vars['mediaID']->value;?>
</li>
							<li>Customize: <?php echo $_smarty_tpl->tpl_vars['customizeID']->value;?>
</li>
							-->
							<?php if ($_smarty_tpl->tpl_vars['media']->value['copyright']){?><li><strong><?php echo $_smarty_tpl->tpl_vars['lang']->value['mediaLabelCopyright'];?>
:</strong> <?php echo $_smarty_tpl->tpl_vars['media']->value['copyright'];?>
</li><?php }?>
							<?php if ($_smarty_tpl->tpl_vars['media']->value['usage_restrictions']){?><li><strong><?php echo $_smarty_tpl->tpl_vars['lang']->value['mediaLabelRestrictions'];?>
:</strong> <?php echo $_smarty_tpl->tpl_vars['media']->value['usage_restrictions'];?>
</li><?php }?>
							<?php if ($_smarty_tpl->tpl_vars['media']->value['model_release_status']){?><li><strong><?php echo $_smarty_tpl->tpl_vars['lang']->value['mediaLabelRelease'];?>
:</strong> <?php echo $_smarty_tpl->tpl_vars['lang']->value['yes'];?>
</li><?php }?>
							<li><strong><?php echo $_smarty_tpl->tpl_vars['lang']->value['instantDownload'];?>
:</strong> 
							<?php if ($_smarty_tpl->tpl_vars['digital']->value['fileCheck']['status']||$_smarty_tpl->tpl_vars['digital']->value['autoCreate']){?>
								<img src="<?php echo $_smarty_tpl->tpl_vars['imgPath']->value;?>
/small.check.1.png" class="fileCheckIcon">
							<?php }else{ ?>
								<span class="notice" style="margin: 0; font-weight: normal; font-style: italic"><?php echo $_smarty_tpl->tpl_vars['lang']->value['noInstantDownload'];?>
</span>
							<?php }?>
							</li>
						</ul>
					</div>
				<?php }?>
			</div>
			<div class="col-md-7">			
				<?php if ($_smarty_tpl->tpl_vars['media']->value['title']){?><h1><?php echo $_smarty_tpl->tpl_vars['media']->value['title'];?>
</h1><?php }?>
				
				<p class="purchaseListDescription" style="margin-top: 16px; font-weight: bold;"><?php echo $_smarty_tpl->tpl_vars['digital']->value['name'];?>
<?php if ($_smarty_tpl->tpl_vars['digital']->value['dsp_type']=='video'&&$_smarty_tpl->tpl_vars['digital']->value['hd']){?><img src="<?php echo $_smarty_tpl->tpl_vars['imgPath']->value;?>
/hd.png" style="vertical-align:top;"><?php }?></p>
				
				<?php if ($_smarty_tpl->tpl_vars['config']->value['settings']['display_license']){?><p class="purchaseListDescription"><?php echo $_smarty_tpl->tpl_vars['lang']->value['license'];?>
: <strong><?php echo $_smarty_tpl->tpl_vars['digital']->value['licenseLang'];?>
</strong><br><span class="licenseDescriptionWB"><?php echo $_smarty_tpl->tpl_vars['digital']->value['licenseDescLang'];?>
</span></p><?php }?>
				<?php if ($_smarty_tpl->tpl_vars['digital']->value['width']||$_smarty_tpl->tpl_vars['digital']->value['height']){?><p class="purchaseListDescription"><?php echo $_smarty_tpl->tpl_vars['lang']->value['resolution'];?>
: <strong><?php echo $_smarty_tpl->tpl_vars['digital']->value['width'];?>
 x <?php echo $_smarty_tpl->tpl_vars['digital']->value['height'];?>
 px</strong> <?php if ($_smarty_tpl->tpl_vars['digital']->value['widthIC']||$_smarty_tpl->tpl_vars['digital']->value['heightIC']){?><em>( <?php echo $_smarty_tpl->tpl_vars['digital']->value['widthIC'];?>
 x <?php echo $_smarty_tpl->tpl_vars['digital']->value['heightIC'];?>
 @ <?php echo $_smarty_tpl->tpl_vars['config']->value['dpiCalc'];?>
 <?php echo $_smarty_tpl->tpl_vars['lang']->value['dpi'];?>
 )</em><?php }?></p><?php }?>
				<?php if ($_smarty_tpl->tpl_vars['digital']->value['format']){?><p class="purchaseListDescription"><?php echo $_smarty_tpl->tpl_vars['lang']->value['mediaLabelFormat'];?>
: <strong><?php echo $_smarty_tpl->tpl_vars['digital']->value['format'];?>
</strong></p><?php }?>
				<?php if ($_smarty_tpl->tpl_vars['digital']->value['dsp_type']=='video'){?>
					<p class="purchaseListDescription">
						<?php if ($_smarty_tpl->tpl_vars['digital']->value['fps']){?><?php echo $_smarty_tpl->tpl_vars['lang']->value['mediaLabelFPS'];?>
: <strong><?php echo $_smarty_tpl->tpl_vars['digital']->value['fps'];?>
</strong><br><?php }?>
						<?php if ($_smarty_tpl->tpl_vars['digital']->value['running_time']){?><?php echo $_smarty_tpl->tpl_vars['lang']->value['mediaLabelRunningTime'];?>
: <strong><?php echo $_smarty_tpl->tpl_vars['digital']->value['running_time'];?>
</strong> <?php echo $_smarty_tpl->tpl_vars['lang']->value['seconds'];?>
<br><?php }?>
					</p>
				<?php }?>
				<p class="purchaseListDescription"><?php echo smarty_modifier_truncate($_smarty_tpl->tpl_vars['digital']->value['description'],200);?>
</p>
				<p class="purchaseListDescription"><?php echo $_smarty_tpl->tpl_vars['lang']->value['quantityAvailable'];?>
: <strong><?php echo $_smarty_tpl->tpl_vars['digital']->value['quantityText'];?>
</strong></p>
				
				<?php if ($_smarty_tpl->tpl_vars['digital']->value['lic_purchase_type']=='rm'){?>
				<input type="hidden" name="rmBasePrice" id="rmBasePrice" value="<?php echo $_smarty_tpl->tpl_vars['rmBasePrice']->value;?>
">
				<input type="hidden" name="rmBaseCredits" id="rmBaseCredits" value="<?php echo $_smarty_tpl->tpl_vars['rmBaseCredits']->value;?>
">				
				<input type="hidden" name="rmCreditsEnc" id="rmCreditsEnc" value="">
				<input type="hidden" name="rmPriceEnc" id="rmPriceEnc" value="">
				
				<div id="rmPricingCalculator">
					<h1><?php echo $_smarty_tpl->tpl_vars['lang']->value['pricingCalculator'];?>
</h1><br>
					<?php  $_smarty_tpl->tpl_vars['rmGroup'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['rmGroup']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['topLevelRM']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['rmGroup']->key => $_smarty_tpl->tpl_vars['rmGroup']->value){
$_smarty_tpl->tpl_vars['rmGroup']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['rmGroup']->key;
?>
						<div id="rmContainer<?php echo $_smarty_tpl->tpl_vars['rmGroup']->value['og_id'];?>
" class="rmContainer">
							<table>
								<tr>
									<td><strong><?php echo $_smarty_tpl->tpl_vars['rmGroup']->value['og_name'];?>
</strong></td>
									<td>
										<select name="rmGroup[<?php echo $_smarty_tpl->tpl_vars['rmGroup']->value['og_id'];?>
]" licID="<?php echo $_smarty_tpl->tpl_vars['digital']->value['license_id'];?>
" class="form-control">
											<option value="0"></option>
											<?php  $_smarty_tpl->tpl_vars['option'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['option']->_loop = false;
 $_smarty_tpl->tpl_vars['optionKey'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['rmGroup']->value['options']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['option']->key => $_smarty_tpl->tpl_vars['option']->value){
$_smarty_tpl->tpl_vars['option']->_loop = true;
 $_smarty_tpl->tpl_vars['optionKey']->value = $_smarty_tpl->tpl_vars['option']->key;
?>
												<option value="<?php echo $_smarty_tpl->tpl_vars['option']->value['op_id'];?>
" price="<?php echo $_smarty_tpl->tpl_vars['option']->value['price'];?>
" credits="<?php echo $_smarty_tpl->tpl_vars['option']->value['credits'];?>
" priceMod="<?php echo $_smarty_tpl->tpl_vars['option']->value['price_mod'];?>
"  displayPrice="<?php echo $_smarty_tpl->tpl_vars['option']->value['price'];?>
"><?php echo $_smarty_tpl->tpl_vars['option']->value['op_name'];?>
</option>
											<?php } ?>
										</select>
									</td>
								</tr>
							</table>
						</div>
					<?php } ?>
				</div>
				<?php }?>
				
				
				<!--
				SUB ROWS: <?php echo $_smarty_tpl->tpl_vars['subRows']->value;?>
<br>
				DOWNLOADS: <?php echo $_smarty_tpl->tpl_vars['downloadRows']->value;?>
<br>
				-->
	
				<?php if ($_smarty_tpl->tpl_vars['digital']->value['license']=='cu'&&!$_smarty_tpl->tpl_vars['downloadRows']->value&&(!$_smarty_tpl->tpl_vars['subRows']->value||!$_smarty_tpl->tpl_vars['subsAvailable']->value)){?>
					<div><p class="notice" style="margin-left: 0; display: none;" id="contectForPricingSuccess"><?php echo $_smarty_tpl->tpl_vars['lang']->value['thankRequest'];?>
</p></div>
					<div style="margin-top: 20px;" id="contactForPricing">
						<?php echo $_smarty_tpl->tpl_vars['lang']->value['pleaseContactForm'];?>
:
						<input type="hidden" name="action" value="emailForQuote">
						<br><br>
						<div class="divTable">
							<div class="divTableRow">
								<div class="divTableCell formFieldLabel"><?php echo $_smarty_tpl->tpl_vars['lang']->value['name'];?>
:</div>
								<div class="divTableCell"><input type="text" name="contactForm[name]" id="contactName" require="require" errorMessage="<?php echo $_smarty_tpl->tpl_vars['lang']->value['required'];?>
" style="min-width: 250px;" class="form-control"></div>
							</div>
							<div class="divTableRow">
								<div class="divTableCell formFieldLabel"><?php echo $_smarty_tpl->tpl_vars['lang']->value['email'];?>
:</div>
								<div class="divTableCell"><input type="text" name="contactForm[email]" id="contactEmail" require="require" errorMessage="<?php echo $_smarty_tpl->tpl_vars['lang']->value['required'];?>
" style="min-width: 250px;" class="form-control"></div>
							</div>
							<div class="divTableRow">
								<div class="divTableCell formFieldLabel"><?php echo $_smarty_tpl->tpl_vars['lang']->value['message'];?>
:</div>
								<div class="divTableCell"><textarea style="height: 50px; min-width: 250px;" name="contactForm[message]" id="contactMessage" class="form-control"></textarea></div>
							</div>
						</div>
					</div>
				<?php }?>
								
				<?php if (!$_smarty_tpl->tpl_vars['downloadRows']->value&&$_smarty_tpl->tpl_vars['subRows']->value&&$_smarty_tpl->tpl_vars['subsAvailable']->value&&$_smarty_tpl->tpl_vars['digital']->value['license']!='fr'&&$_smarty_tpl->tpl_vars['digital']->value['lic_purchase_type']!='rm'){?>
					<input type="hidden" name="downloadType" id="downloadType" value="sub">
					<div class="downloadSubs">
						<span style=""><?php echo $_smarty_tpl->tpl_vars['lang']->value['downWithSub'];?>
:</span><br>
						<ul>
						<?php  $_smarty_tpl->tpl_vars['subscription'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['subscription']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['subsArray']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['subscription']->index=-1;
foreach ($_from as $_smarty_tpl->tpl_vars['subscription']->key => $_smarty_tpl->tpl_vars['subscription']->value){
$_smarty_tpl->tpl_vars['subscription']->_loop = true;
 $_smarty_tpl->tpl_vars['subscription']->index++;
 $_smarty_tpl->tpl_vars['subscription']->first = $_smarty_tpl->tpl_vars['subscription']->index === 0;
?>
							<?php if ($_smarty_tpl->tpl_vars['subscription']->value['available']){?>
								<li class="subOptions" msub_id="<?php echo $_smarty_tpl->tpl_vars['subscription']->value['msub_id'];?>
">
									<input type="radio" name="subscription" value="<?php echo $_smarty_tpl->tpl_vars['subscription']->value['msub_id'];?>
" id="sub<?php echo $_smarty_tpl->tpl_vars['subscription']->value['msub_id'];?>
" key="<?php echo $_smarty_tpl->tpl_vars['subscription']->value['downloadKey'];?>
" > 
									<p>
										<label for="sub<?php echo $_smarty_tpl->tpl_vars['subscription']->value['msub_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['subscription']->value['item_name'];?>
</label><br>
										<?php echo $_smarty_tpl->tpl_vars['lang']->value['expires'];?>
: <strong><?php echo $_smarty_tpl->tpl_vars['subscription']->value['expireDate'];?>
</strong><br>
										<?php echo $_smarty_tpl->tpl_vars['lang']->value['downloadsRemainingB'];?>
: 
										<strong>
											<?php if ($_smarty_tpl->tpl_vars['subscription']->value['total_downloads']){?>
												<?php echo $_smarty_tpl->tpl_vars['subscription']->value['totalRemaining'];?>

											<?php }else{ ?>
												<?php echo $_smarty_tpl->tpl_vars['lang']->value['unlimited'];?>

											<?php }?>
											<?php if ($_smarty_tpl->tpl_vars['subscription']->value['perday']){?> (<?php echo $_smarty_tpl->tpl_vars['subscription']->value['todayRemaining'];?>
 <?php echo $_smarty_tpl->tpl_vars['lang']->value['today'];?>
)<?php }?>
										</strong>
									</p>
								</li>
							<?php }?>
						<?php } ?>
						</ul>
						<div id="requestDownloadSuccess" class="notice" style="display: none; margin: 0; float: left;"><?php echo $_smarty_tpl->tpl_vars['lang']->value['requestDownloadSuccess'];?>
</div>
						<input type="button" value="<?php if ($_smarty_tpl->tpl_vars['digital']->value['fileCheck']['status']||$_smarty_tpl->tpl_vars['digital']->value['autoCreate']){?><?php echo $_smarty_tpl->tpl_vars['lang']->value['downloadUpper'];?>
<?php }else{ ?><?php echo $_smarty_tpl->tpl_vars['lang']->value['requestDownload'];?>
<?php }?>" disabled="disabled" style="float: right; margin-top: 10px;" id="workboxDownload" class="btn btn-xs btn-primary">
					</div>
				<?php }?>
				
				<?php if ($_smarty_tpl->tpl_vars['digital']->value['license']=='rf'){?><?php }?>

			</div>
		</div>
	</div>
<?php }?>
<div class="workboxActionButtons">
	<?php if ($_smarty_tpl->tpl_vars['downloadRows']->value){?>
		<span class="notice" style="margin: 0" id="prevDownloadedMessage"><?php echo $_smarty_tpl->tpl_vars['lang']->value['prevDownloaded'];?>
</span>
		<input type="hidden" name="downloadType" id="downloadType" value="prevDown">
		<input type="button" value="<?php echo $_smarty_tpl->tpl_vars['lang']->value['downloadUpper'];?>
" id="workboxDownload" key="<?php echo $_smarty_tpl->tpl_vars['digital']->value['downloadKey'];?>
">
	<?php }else{ ?>
		<?php if (!$_smarty_tpl->tpl_vars['downloadRows']->value&&$_smarty_tpl->tpl_vars['subRows']->value&&$_smarty_tpl->tpl_vars['subsAvailable']->value&&$_smarty_tpl->tpl_vars['digital']->value['license']!='fr'&&$_smarty_tpl->tpl_vars['digital']->value['lic_purchase_type']!='rm'){?>
			
		<?php }else{ ?>
			<?php if (($_smarty_tpl->tpl_vars['digital']->value['license']=='rf'||$_smarty_tpl->tpl_vars['digital']->value['license']=='rm'||$_smarty_tpl->tpl_vars['digital']->value['license']=='ex'||$_smarty_tpl->tpl_vars['digital']->value['license']=='eu')&&$_smarty_tpl->tpl_vars['cartStatus']->value){?>
				<?php if ($_smarty_tpl->tpl_vars['media']->value&&$_smarty_tpl->tpl_vars['digital']->value['price']){?><?php echo $_smarty_tpl->tpl_vars['lang']->value['mediaLabelPrice'];?>
: <span class="price" id="workboxItemPrice"><?php echo $_smarty_tpl->tpl_vars['digital']->value['price']['display'];?>
</span><?php if ($_smarty_tpl->tpl_vars['digital']->value['price']['taxInc']){?> <span class="taxIncMessage">(<?php echo $_smarty_tpl->tpl_vars['lang']->value['taxIncMessage'];?>
)</span><?php }?><?php }?>
				<?php if ($_smarty_tpl->tpl_vars['media']->value&&$_smarty_tpl->tpl_vars['digital']->value['credits']){?>&nbsp;&nbsp;<?php echo $_smarty_tpl->tpl_vars['lang']->value['mediaLabelCredits'];?>
: <span class="price" id="workboxItemCredits"><?php echo $_smarty_tpl->tpl_vars['digital']->value['credits'];?>
</span><?php }?>
				<?php if (!$_smarty_tpl->tpl_vars['noAccess']->value&&!$_smarty_tpl->tpl_vars['edit']->value){?><br><input type="button" value="<?php echo $_smarty_tpl->tpl_vars['lang']->value['addToCart'];?>
" id="workboxAddToCart" class="btn btn-xs btn-primary"><?php }?>
			<?php }?>
			
			<?php if ($_smarty_tpl->tpl_vars['digital']->value['license']=='fr'){?>
				<input type="hidden" name="downloadType" id="downloadType" value="free">
				<?php if ($_smarty_tpl->tpl_vars['digital']->value['fileCheck']['status']||$_smarty_tpl->tpl_vars['digital']->value['autoCreate']){?>
					<input type="button" value="<?php echo $_smarty_tpl->tpl_vars['lang']->value['downloadUpper'];?>
" id="workboxDownload" key="<?php echo $_smarty_tpl->tpl_vars['digital']->value['downloadKey'];?>
" class="btn btn-xs btn-primary">
				<?php }else{ ?>
					<div id="requestDownloadSuccess" class="notice" style="display: none; margin: 0; float: left;"><?php echo $_smarty_tpl->tpl_vars['lang']->value['requestDownloadSuccess'];?>
</div>
					<div style="float: left;" id="requestDownloadContainer"><input type="text" name="requestDownloadEmail" id="requestDownloadEmail" style="display: none;" value="<?php echo $_smarty_tpl->tpl_vars['lang']->value['enterEmail'];?>
" class="form-control"> <input type="button" value="<?php echo $_smarty_tpl->tpl_vars['lang']->value['requestDownload'];?>
" altText="<?php echo $_smarty_tpl->tpl_vars['lang']->value['submit'];?>
" id="workboxRequestDownload" class="btn btn-xs btn-primary"></div>
				<?php }?>
			<?php }?>
		
			<?php if ($_smarty_tpl->tpl_vars['digital']->value['license']=='cu'){?><?php if (!$_smarty_tpl->tpl_vars['noAccess']->value){?><br><input type="button" value="<?php echo $_smarty_tpl->tpl_vars['lang']->value['submit'];?>
" id="workboxSubmit" class="btn btn-xs btn-primary"><?php }?><?php }?>			
		<?php }?>
	<?php }?>	
</div>
</form><?php }} ?>