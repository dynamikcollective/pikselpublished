<?php /* Smarty version Smarty-3.1.8, created on 2016-11-28 19:04:00
         compiled from "/home/dynamikd/public_html/clients/piksel/assets/themes/modern/paging.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1675905802583c7fa086d114-22351240%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '53f2044c3c61fa062663a74fe8df4c619797d3a4' => 
    array (
      0 => '/home/dynamikd/public_html/clients/piksel/assets/themes/modern/paging.tpl',
      1 => 1402069378,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1675905802583c7fa086d114-22351240',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'lang' => 0,
    'paging' => 0,
    'id' => 0,
    'page' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.8',
  'unifunc' => 'content_583c7fa08dad75_97954243',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_583c7fa08dad75_97954243')) {function content_583c7fa08dad75_97954243($_smarty_tpl) {?><div class="paging">
	<?php echo $_smarty_tpl->tpl_vars['lang']->value['page'];?>

	<select class="pagingPageNumber">
		<?php  $_smarty_tpl->tpl_vars['page'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['page']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['paging']->value['pageNumbers']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['page']->key => $_smarty_tpl->tpl_vars['page']->value){
$_smarty_tpl->tpl_vars['page']->_loop = true;
?>
			<option value="<?php echo linkto(array('page'=>($_smarty_tpl->tpl_vars['paging']->value['pageName'])."&id=".($_smarty_tpl->tpl_vars['id']->value)."&".($_smarty_tpl->tpl_vars['paging']->value['pageVar'])."=".($_smarty_tpl->tpl_vars['page']->key)),$_smarty_tpl);?>
" <?php if ($_smarty_tpl->tpl_vars['page']->key==$_smarty_tpl->tpl_vars['paging']->value['currentPage']){?>selected="selected"<?php }?>><?php echo $_smarty_tpl->tpl_vars['page']->value;?>
</option>
		<?php } ?>
	</select> 
	<?php echo $_smarty_tpl->tpl_vars['lang']->value['of'];?>
 <strong><?php echo $_smarty_tpl->tpl_vars['paging']->value['totalPages'];?>
</strong> <span class="totalResults">(<?php echo $_smarty_tpl->tpl_vars['paging']->value['totalResults'];?>
 <?php echo $_smarty_tpl->tpl_vars['lang']->value['itemsTotal'];?>
)</span> &nbsp;
	
	<?php if ($_smarty_tpl->tpl_vars['paging']->value['previousPage']){?><a href="<?php echo linkto(array('page'=>($_smarty_tpl->tpl_vars['paging']->value['pageName'])."&id=".($_smarty_tpl->tpl_vars['id']->value)."&".($_smarty_tpl->tpl_vars['paging']->value['pageVar'])."=".($_smarty_tpl->tpl_vars['paging']->value['previousPage'])),$_smarty_tpl);?>
" class="btn btn-xs btn-primary">&laquo; <?php echo $_smarty_tpl->tpl_vars['lang']->value['prevUpper'];?>
</a><?php }?>

	<?php if ($_smarty_tpl->tpl_vars['paging']->value['nextPage']){?><a href="<?php echo linkto(array('page'=>($_smarty_tpl->tpl_vars['paging']->value['pageName'])."&id=".($_smarty_tpl->tpl_vars['id']->value)."&".($_smarty_tpl->tpl_vars['paging']->value['pageVar'])."=".($_smarty_tpl->tpl_vars['paging']->value['nextPage'])),$_smarty_tpl);?>
" class="btn btn-xs btn-primary"><?php echo $_smarty_tpl->tpl_vars['lang']->value['nextUpper'];?>
 &raquo;</a><?php }?><!--<?php echo linkto(array('page'=>($_smarty_tpl->tpl_vars['paging']->value['pageName'])."&id=".($_smarty_tpl->tpl_vars['id']->value)."&".($_smarty_tpl->tpl_vars['paging']->value['pageVar'])."=".($_smarty_tpl->tpl_vars['paging']->value['nextPage'])),$_smarty_tpl);?>
-->
</div><?php }} ?>