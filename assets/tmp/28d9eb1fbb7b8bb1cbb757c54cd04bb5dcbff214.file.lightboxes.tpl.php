<?php /* Smarty version Smarty-3.1.8, created on 2017-01-11 19:00:45
         compiled from "/home/dynamikd/public_html/clients/piksel/assets/themes/modern/lightboxes.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1044343278587680dd8f11c7-43380645%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '28d9eb1fbb7b8bb1cbb757c54cd04bb5dcbff214' => 
    array (
      0 => '/home/dynamikd/public_html/clients/piksel/assets/themes/modern/lightboxes.tpl',
      1 => 1403520520,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1044343278587680dd8f11c7-43380645',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'loggedIn' => 0,
    'lang' => 0,
    'notice' => 0,
    'lightboxRows' => 0,
    'lightboxArray' => 0,
    'lightbox' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.8',
  'unifunc' => 'content_587680dd95d281_39775304',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_587680dd95d281_39775304')) {function content_587680dd95d281_39775304($_smarty_tpl) {?><!DOCTYPE HTML>
<html>
<head>
	<?php echo $_smarty_tpl->getSubTemplate ('head.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

</head>
<body>
	<?php echo $_smarty_tpl->getSubTemplate ('overlays.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

	<div id="container">
		<?php echo $_smarty_tpl->getSubTemplate ('header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

		<?php echo $_smarty_tpl->getSubTemplate ('header2.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>
		
		
		<div class="container">
			<div class="row">
				<?php if ($_smarty_tpl->tpl_vars['loggedIn']->value){?>
					<?php echo $_smarty_tpl->getSubTemplate ('memnav.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

				<?php }else{ ?>
					<?php echo $_smarty_tpl->getSubTemplate ('subnav.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

				<?php }?>	
				<div class="col-md-9">
					
					<h1><?php echo $_smarty_tpl->tpl_vars['lang']->value['lightboxes'];?>
</h1>
					<hr>
					<input type="button" value="<?php echo $_smarty_tpl->tpl_vars['lang']->value['newLightbox'];?>
" style="float: right; margin-bottom: 10px;" id="newLightbox" class="btn btn-xs btn-success">
					
					<?php if ($_smarty_tpl->tpl_vars['notice']->value){?>
						<p class="notice" style="margin-bottom: 14px;"><?php echo $_smarty_tpl->tpl_vars['lang']->value[$_smarty_tpl->tpl_vars['notice']->value];?>
</p>
					<?php }?>
					
					<?php if ($_smarty_tpl->tpl_vars['lightboxRows']->value){?>
						<table class="dataTable">
							<tr>
								<th><?php echo $_smarty_tpl->tpl_vars['lang']->value['lightboxUpper'];?>
</th>
								<th class="hidden-xs" style="text-align: center"><?php echo $_smarty_tpl->tpl_vars['lang']->value['itemsUpper'];?>
</th>
								<th class="hidden-xs"><?php echo $_smarty_tpl->tpl_vars['lang']->value['createdUpper'];?>
</th>
								<th></th>
							</tr>
							<?php  $_smarty_tpl->tpl_vars['lightbox'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['lightbox']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['lightboxArray']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['lightbox']->key => $_smarty_tpl->tpl_vars['lightbox']->value){
$_smarty_tpl->tpl_vars['lightbox']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['lightbox']->key;
?>
								<tr>
									<td><a href="<?php echo $_smarty_tpl->tpl_vars['lightbox']->value['linkto'];?>
" class="colorLink"><?php echo $_smarty_tpl->tpl_vars['lightbox']->value['name'];?>
</a></td>
									<td class="hidden-xs" style="text-align: center"><?php echo $_smarty_tpl->tpl_vars['lightbox']->value['items'];?>
</td>
									<td class="hidden-xs"><?php echo $_smarty_tpl->tpl_vars['lightbox']->value['create_date_display'];?>
</td>
									<td style="text-align: right; white-space: nowrap"><a href="<?php echo $_smarty_tpl->tpl_vars['lightbox']->value['linkto'];?>
" class="btn btn-xs btn-primary"><?php echo $_smarty_tpl->tpl_vars['lang']->value['view'];?>
</a> <a href="<?php echo $_smarty_tpl->tpl_vars['lightbox']->value['ulightbox_id'];?>
" class="btn btn-xs btn-primary lightboxEdit"><?php echo $_smarty_tpl->tpl_vars['lang']->value['edit'];?>
</a> <a href="<?php echo $_smarty_tpl->tpl_vars['lightbox']->value['ulightbox_id'];?>
" class="btn btn-xs btn-danger lightboxDelete"><?php echo $_smarty_tpl->tpl_vars['lang']->value['delete'];?>
</a></td>
								</tr>
							<?php } ?>
						</table>
					<?php }else{ ?>
						<p class="notice"><?php echo $_smarty_tpl->tpl_vars['lang']->value['noLightboxes'];?>
</p>
					<?php }?>
					
				</div>
			</div>
		</div>
		<?php echo $_smarty_tpl->getSubTemplate ('footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

    </div>
</body>
</html><?php }} ?>