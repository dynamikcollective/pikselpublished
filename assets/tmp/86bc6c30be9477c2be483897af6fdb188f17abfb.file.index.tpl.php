<?php /* Smarty version Smarty-3.1.8, created on 2017-01-19 12:20:33
         compiled from "/home/dynamikd/public_html/clients/piksel/assets/themes/default/index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:13251478535880af11b4e6c3-45183311%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '86bc6c30be9477c2be483897af6fdb188f17abfb' => 
    array (
      0 => '/home/dynamikd/public_html/clients/piksel/assets/themes/default/index.tpl',
      1 => 1428936928,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '13251478535880af11b4e6c3-45183311',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'config' => 0,
    'baseURL' => 0,
    'featuredMedia' => 0,
    'media' => 0,
    'featuredContributors' => 0,
    'siteStats' => 0,
    'imgPath' => 0,
    'lang' => 0,
    'membersOnline' => 0,
    'member' => 0,
    'contributor' => 0,
    'featuredPrintsRows' => 0,
    'featuredPrints' => 0,
    'print' => 0,
    'featuredProductsRows' => 0,
    'featuredProducts' => 0,
    'product' => 0,
    'featuredPackagesRows' => 0,
    'featuredPackages' => 0,
    'package' => 0,
    'featuredCollectionsRows' => 0,
    'featuredCollections' => 0,
    'collection' => 0,
    'featuredPromotionsRows' => 0,
    'featuredPromotions' => 0,
    'promotion' => 0,
    'featuredSubscriptionsRows' => 0,
    'featuredSubscriptions' => 0,
    'subscription' => 0,
    'featuredCreditsRows' => 0,
    'featuredCredits' => 0,
    'credits' => 0,
    'featuredNewsRows' => 0,
    'featuredNews' => 0,
    'news' => 0,
    'newestMediaRows' => 0,
    'newestMedia' => 0,
    'popularMediaRows' => 0,
    'popularMedia' => 0,
    'randomMediaRows' => 0,
    'randomMedia' => 0,
    'subGalleriesData' => 0,
    'subGallery' => 0,
    'galleriesData' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.8',
  'unifunc' => 'content_5880af120db4e8_30403308',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5880af120db4e8_30403308')) {function content_5880af120db4e8_30403308($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_truncate')) include '/home/dynamikd/public_html/clients/piksel/assets/smarty/plugins/modifier.truncate.php';
?><!DOCTYPE HTML>
<html>
<head>
	<?php echo $_smarty_tpl->getSubTemplate ('head.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

	<script type="text/javascript">
		var featuredVideoOverVol = '<?php echo $_smarty_tpl->tpl_vars['config']->value['featuredVideoOverVol'];?>
';
		var featuredVideoVolume = '<?php echo $_smarty_tpl->tpl_vars['config']->value['featuredVideoVolume'];?>
';
		
		var featuredMedia = 
		{
			'fadeSpeed'		: <?php echo $_smarty_tpl->tpl_vars['config']->value['settings']['hpf_fade_speed'];?>
,
			'interval'		: <?php echo $_smarty_tpl->tpl_vars['config']->value['settings']['hpf_inverval'];?>
,
			'detailsDelay'	: <?php echo $_smarty_tpl->tpl_vars['config']->value['settings']['hpf_details_delay'];?>
,
			'detailsDisTime': <?php echo $_smarty_tpl->tpl_vars['config']->value['settings']['hpf_details_distime'];?>

		};
		
		function featuredVideoPlayer(mediaID,container)
		{
			//alert(container);
			
			//$("#featuredVideoPlayerContainer").css({ 'width':'250px','height':'250px' });
			
			jwplayer(container).setup(
			{
				'flashplayer': "<?php echo $_smarty_tpl->tpl_vars['baseURL']->value;?>
/assets/jwplayer/player.swf",
				'file': "<?php echo $_smarty_tpl->tpl_vars['baseURL']->value;?>
/video.php?mediaID="+mediaID,
				'autostart': '<?php echo $_smarty_tpl->tpl_vars['config']->value['autoPlayFeaturedVid'];?>
',
				'type': 'video',
				'repeat': '<?php echo $_smarty_tpl->tpl_vars['config']->value['settings']['video_autorepeat'];?>
',
				'controlbar.position': '<?php echo $_smarty_tpl->tpl_vars['config']->value['settings']['video_controls'];?>
',
				'logo.file': '<?php echo $_smarty_tpl->tpl_vars['baseURL']->value;?>
/assets/watermarks/<?php echo $_smarty_tpl->tpl_vars['config']->value['settings']['vidpreview_wm'];?>
',
				'logo.hide': false,
				'logo.position': '<?php echo $_smarty_tpl->tpl_vars['config']->value['settings']['video_wmpos'];?>
',
				'stretching': 'uniform',
				'width': '100%',
				'height': '100%',
				'skin': '<?php echo $_smarty_tpl->tpl_vars['baseURL']->value;?>
/assets/jwplayer/skins/<?php echo $_smarty_tpl->tpl_vars['config']->value['settings']['video_skin'];?>
/<?php echo $_smarty_tpl->tpl_vars['config']->value['settings']['video_skin'];?>
.zip',
				'screencolor': '<?php echo $_smarty_tpl->tpl_vars['config']->value['settings']['video_bg_color'];?>
',
				'volume': '<?php echo $_smarty_tpl->tpl_vars['config']->value['featuredVideoVolume'];?>
',
				'modes': [
					{ 'type': 'flash', src: '<?php echo $_smarty_tpl->tpl_vars['baseURL']->value;?>
/assets/jwplayer/player.swf' },
					{ 'type': 'html5' },
					{ 'type': 'download' }
				]
			});
		}
	</script>
	<script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['baseURL']->value;?>
/assets/javascript/index.js"></script>
	<script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['baseURL']->value;?>
/assets/javascript/gallery.js"></script>
	<style>
		#featuredVideoPlayerContainer{
			min-width: <?php echo $_smarty_tpl->tpl_vars['config']->value['settings']['hpf_width'];?>
px;
			min-height: <?php echo $_smarty_tpl->tpl_vars['config']->value['settings']['hpf_crop_to'];?>
px;
		}
	</style>
</head>
<body>
	<?php echo $_smarty_tpl->getSubTemplate ('overlays.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

	<div id="container">
		<?php echo $_smarty_tpl->getSubTemplate ('header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>
		
		
		
		<div id="featuredMedia">
			<div class="divTable">
				<div class="divTableRow">
					<div class="divTableCell hpWelcomeMessage"><?php echo content(array('id'=>'homeWelcome'),$_smarty_tpl);?>
</div>
					<?php if ($_smarty_tpl->tpl_vars['config']->value['settings']['hpfeaturedmedia']&&$_smarty_tpl->tpl_vars['featuredMedia']->value){?>
						<div class="divTableCell" id="featuredOneCell" style="width: <?php echo $_smarty_tpl->tpl_vars['config']->value['settings']['hpf_width'];?>
px; height: <?php echo $_smarty_tpl->tpl_vars['config']->value['settings']['hpf_crop_to'];?>
px;">
							<p id="featuredNext" class="opac50" style="z-index: 1000" onclick="featuredMediaRotator();">&nbsp;&raquo;&nbsp;</p>
							<div id="featuredOneContainer"></div>
							<ul id="featuredMediaList" class="opac60" style="width: <?php echo $_smarty_tpl->tpl_vars['config']->value['settings']['hpf_width']-20;?>
px;">
								<?php  $_smarty_tpl->tpl_vars['media'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['media']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['featuredMedia']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['media']->key => $_smarty_tpl->tpl_vars['media']->value){
$_smarty_tpl->tpl_vars['media']->_loop = true;
?>
									<li mediaType="<?php if ($_smarty_tpl->tpl_vars['media']->value['sampleVideo']){?>video<?php }else{ ?>image<?php }?>" image="image.php?mediaID=<?php echo $_smarty_tpl->tpl_vars['media']->value['encryptedID'];?>
=&type=featured&folderID=<?php echo $_smarty_tpl->tpl_vars['media']->value['encryptedFID'];?>
&size=<?php echo $_smarty_tpl->tpl_vars['config']->value['settings']['hpf_width'];?>
&crop=<?php echo $_smarty_tpl->tpl_vars['config']->value['settings']['hpf_crop_to'];?>
" encMediaID="<?php echo $_smarty_tpl->tpl_vars['media']->value['encryptedID'];?>
" href="<?php echo $_smarty_tpl->tpl_vars['media']->value['linkto'];?>
"><?php if ($_smarty_tpl->tpl_vars['media']->value['title']['value']||$_smarty_tpl->tpl_vars['media']->value['description']['value']){?><span class="title"><a href="<?php echo $_smarty_tpl->tpl_vars['media']->value['linkto'];?>
"><?php echo $_smarty_tpl->tpl_vars['media']->value['title']['value'];?>
</a></span><br><span class="description"><?php echo $_smarty_tpl->tpl_vars['media']->value['description']['value'];?>
</span><?php }?></li>
								<?php } ?>
							</ul>
						</div>
					<?php }?>
				</div>
			</div>
		</div>
		
		<div id="featureBoxes" class="divTable">
			<div class="divTableRow">
				
				
				<div class="divTableCell hpFeatureBox" <?php if ((!$_smarty_tpl->tpl_vars['featuredContributors']->value&&!$_smarty_tpl->tpl_vars['config']->value['settings']['members_online'])&&$_smarty_tpl->tpl_vars['siteStats']->value){?>style="width: 100%;"<?php }elseif((!$_smarty_tpl->tpl_vars['featuredContributors']->value||!$_smarty_tpl->tpl_vars['config']->value['settings']['members_online'])&&$_smarty_tpl->tpl_vars['siteStats']->value){?>style="width: 50%;"<?php }elseif($_smarty_tpl->tpl_vars['siteStats']->value){?>style="width: 33%;"<?php }?>>
					<?php if ($_smarty_tpl->tpl_vars['siteStats']->value){?>
						<img src="<?php echo $_smarty_tpl->tpl_vars['imgPath']->value;?>
/feature.box.a.icon.png">
						<h1><?php echo $_smarty_tpl->tpl_vars['lang']->value['siteStats'];?>
</h1>
						<div class="divTable">
							<div class="divTableRow">
								<div class="divTableCell"><?php echo $_smarty_tpl->tpl_vars['lang']->value['members'];?>
:</div>
								<div class="divTableCell"><strong><?php echo $_smarty_tpl->tpl_vars['siteStats']->value['members'];?>
</strong></div>
							</div>
							<div class="divTableRow">
								<div class="divTableCell"><?php echo $_smarty_tpl->tpl_vars['lang']->value['media'];?>
:</div>
								<div class="divTableCell"><strong><?php echo $_smarty_tpl->tpl_vars['siteStats']->value['media'];?>
</strong></div>
							</div>
							
							<div class="divTableRow">
								<div class="divTableCell"><?php echo $_smarty_tpl->tpl_vars['lang']->value['visits'];?>
:</div>
								<div class="divTableCell"><strong><?php echo $_smarty_tpl->tpl_vars['siteStats']->value['visits'];?>
</strong></div>
							</div>
						</div>
					<?php }?>&nbsp;
				</div>
				
				
				<div class="divTableCell hpFeatureBox" <?php if ((!$_smarty_tpl->tpl_vars['featuredContributors']->value&&!$_smarty_tpl->tpl_vars['siteStats']->value)&&$_smarty_tpl->tpl_vars['config']->value['settings']['members_online']){?>style="width: 100%;"<?php }elseif((!$_smarty_tpl->tpl_vars['featuredContributors']->value||!$_smarty_tpl->tpl_vars['siteStats']->value)&&$_smarty_tpl->tpl_vars['config']->value['settings']['members_online']){?>style="width: 50%;"<?php }elseif($_smarty_tpl->tpl_vars['config']->value['settings']['members_online']){?>style="width: 33%;"<?php }?>>
					<?php if ($_smarty_tpl->tpl_vars['config']->value['settings']['members_online']){?>
						<img src="<?php echo $_smarty_tpl->tpl_vars['imgPath']->value;?>
/feature.box.b.icon.png">
						<h1><?php echo $_smarty_tpl->tpl_vars['lang']->value['membersOnline'];?>
</h1>
						<div><?php if ($_smarty_tpl->tpl_vars['membersOnline']->value){?><?php  $_smarty_tpl->tpl_vars['member'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['member']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['membersOnline']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['member']->total= $_smarty_tpl->_count($_from);
 $_smarty_tpl->tpl_vars['member']->iteration=0;
foreach ($_from as $_smarty_tpl->tpl_vars['member']->key => $_smarty_tpl->tpl_vars['member']->value){
$_smarty_tpl->tpl_vars['member']->_loop = true;
 $_smarty_tpl->tpl_vars['member']->iteration++;
 $_smarty_tpl->tpl_vars['member']->last = $_smarty_tpl->tpl_vars['member']->iteration === $_smarty_tpl->tpl_vars['member']->total;
?><?php echo $_smarty_tpl->tpl_vars['member']->value['f_name'];?>
 <?php echo $_smarty_tpl->tpl_vars['member']->value['l_name'];?>
 <span class="time">(<?php echo $_smarty_tpl->tpl_vars['member']->value['lastSeen'];?>
 <?php echo $_smarty_tpl->tpl_vars['lang']->value['minutesAgo'];?>
)</span><?php if (!$_smarty_tpl->tpl_vars['member']->last){?>,<?php }?> <?php } ?><?php }else{ ?><?php echo $_smarty_tpl->tpl_vars['lang']->value['none'];?>
<?php }?></div>
					<?php }?>&nbsp;
				</div>
				
				
				<div class="divTableCell hpFeatureBox" style="<?php if ((!$_smarty_tpl->tpl_vars['config']->value['settings']['members_online']&&!$_smarty_tpl->tpl_vars['siteStats']->value)&&$_smarty_tpl->tpl_vars['featuredContributors']->value){?>width: 100%;<?php }elseif((!$_smarty_tpl->tpl_vars['config']->value['settings']['members_online']||!$_smarty_tpl->tpl_vars['siteStats']->value)&&$_smarty_tpl->tpl_vars['featuredContributors']->value){?>width: 50%;<?php }elseif($_smarty_tpl->tpl_vars['featuredContributors']->value){?>width: 33%;<?php }?>">
					<?php if ($_smarty_tpl->tpl_vars['featuredContributors']->value){?>
						<div style="min-height: 100px;">
							<div class="divTable">
								<div class="divTableRow">
									<div class="divTableCell" style="vertical-align:top;">
										<div id="featuredIcon"><img src="<?php echo $_smarty_tpl->tpl_vars['imgPath']->value;?>
/feature.box.c.icon.png" id="contributorAvatar"></div>
									</div>
									<div class="divTableCell" style="vertical-align:top; padding-left: 10px;">		
										<h1><?php echo $_smarty_tpl->tpl_vars['lang']->value['contributors'];?>
</h1>
										<?php  $_smarty_tpl->tpl_vars['contributor'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['contributor']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['featuredContributors']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['contributor']->total= $_smarty_tpl->_count($_from);
 $_smarty_tpl->tpl_vars['contributor']->iteration=0;
foreach ($_from as $_smarty_tpl->tpl_vars['contributor']->key => $_smarty_tpl->tpl_vars['contributor']->value){
$_smarty_tpl->tpl_vars['contributor']->_loop = true;
 $_smarty_tpl->tpl_vars['contributor']->iteration++;
 $_smarty_tpl->tpl_vars['contributor']->last = $_smarty_tpl->tpl_vars['contributor']->iteration === $_smarty_tpl->tpl_vars['contributor']->total;
?>
											<!--<?php if ($_smarty_tpl->tpl_vars['contributor']->value['avatar']){?><img src="<?php echo memberAvatar(array('memID'=>$_smarty_tpl->tpl_vars['contributor']->value['mem_id'],'size'=>70,'crop'=>70),$_smarty_tpl);?>
"><?php }?>--><a href="<?php echo linkto(array('page'=>"contributors.php?id=".($_smarty_tpl->tpl_vars['contributor']->value['useID'])."&seoName=".($_smarty_tpl->tpl_vars['contributor']->value['seoName'])),$_smarty_tpl);?>
" <?php if ($_smarty_tpl->tpl_vars['contributor']->value['avatar']){?>memID="<?php echo $_smarty_tpl->tpl_vars['contributor']->value['mem_id'];?>
" class="hpContributorLink"<?php }?>><?php echo $_smarty_tpl->tpl_vars['contributor']->value['display_name'];?>
</a><?php if (!$_smarty_tpl->tpl_vars['contributor']->last){?>,<?php }?>
										<?php } ?>
										<p class="more"><a href="<?php echo linkto(array('page'=>"contributors.php"),$_smarty_tpl);?>
"><?php echo $_smarty_tpl->tpl_vars['lang']->value['more'];?>
</a> &raquo;</p>
									</div>
								</div>
							</div>
						</div>
					<?php }?>&nbsp;
				</div>
				
			</div>
		</div>
		<div class="divTable contentContainer">
			<div class="divTableRow">
				<div class="divTableCell contentLeftColumn">
					<?php echo $_smarty_tpl->getSubTemplate ('subnav.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

					
																		
					<?php if ($_smarty_tpl->tpl_vars['featuredPrintsRows']->value){?>
						<div class="subNavFeaturedBox">
							<h1><?php echo $_smarty_tpl->tpl_vars['lang']->value['featuredPrints'];?>
</h1>
							<div class="divTable">
								<?php  $_smarty_tpl->tpl_vars['print'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['print']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['featuredPrints']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['print']->key => $_smarty_tpl->tpl_vars['print']->value){
$_smarty_tpl->tpl_vars['print']->_loop = true;
?>
									<div class="divTableRow workboxLinkAttach">
										<div class="divTableCell"><?php if ($_smarty_tpl->tpl_vars['print']->value['photo']){?><img src="<?php echo productShot(array('itemID'=>$_smarty_tpl->tpl_vars['print']->value['print_id'],'itemType'=>'print','photoID'=>$_smarty_tpl->tpl_vars['print']->value['photo']['id'],'size'=>50,'crop'=>40),$_smarty_tpl);?>
"><?php }?></div>
										<div class="divTableCell">
											<h2><a href="<?php echo $_smarty_tpl->tpl_vars['print']->value['linkto'];?>
" class="workboxLink"><?php echo $_smarty_tpl->tpl_vars['print']->value['name'];?>
</a></h2>
											<p class="featuredDescription"><?php echo smarty_modifier_truncate($_smarty_tpl->tpl_vars['print']->value['description'],60);?>
</p>
											<p class="featuredPrice"><?php if ($_smarty_tpl->tpl_vars['print']->value['price']){?><span class="price"><?php echo $_smarty_tpl->tpl_vars['print']->value['price']['display'];?>
</span><?php if ($_smarty_tpl->tpl_vars['print']->value['price']['taxInc']){?> <span class="taxIncMessage">(<?php echo $_smarty_tpl->tpl_vars['lang']->value['taxIncMessage'];?>
)</span><?php }?><?php }?><?php if ($_smarty_tpl->tpl_vars['config']->value['settings']['cart']==3&&$_smarty_tpl->tpl_vars['config']->value['settings']['credits_print']){?> <?php echo $_smarty_tpl->tpl_vars['lang']->value['priceCreditSep'];?>
 <?php }?><?php if ($_smarty_tpl->tpl_vars['print']->value['credits']){?><span class="price"><?php echo $_smarty_tpl->tpl_vars['print']->value['credits'];?>
 <sup><?php echo $_smarty_tpl->tpl_vars['lang']->value['mediaLabelCredits'];?>
</sup></span><?php }?></p>
										</div>
									</div>
								<?php } ?>
							</div>						
							<?php if ($_smarty_tpl->tpl_vars['config']->value['settings']['printpage']){?><p class="featuredBoxMore"><a href="<?php echo linkto(array('page'=>'featured.php?mode=prints'),$_smarty_tpl);?>
"><?php echo $_smarty_tpl->tpl_vars['lang']->value['more'];?>
</a></p><?php }?>
						</div>
					<?php }?>
							
											
					<?php if ($_smarty_tpl->tpl_vars['featuredProductsRows']->value){?>
						<div class="subNavFeaturedBox">
							<h1><?php echo $_smarty_tpl->tpl_vars['lang']->value['featuredProducts'];?>
</h1>
							<div class="divTable">
								<?php  $_smarty_tpl->tpl_vars['product'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['product']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['featuredProducts']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['product']->key => $_smarty_tpl->tpl_vars['product']->value){
$_smarty_tpl->tpl_vars['product']->_loop = true;
?>
									<div class="divTableRow workboxLinkAttach">
										<div class="divTableCell"><?php if ($_smarty_tpl->tpl_vars['product']->value['photo']){?><img src="<?php echo productShot(array('itemID'=>$_smarty_tpl->tpl_vars['product']->value['prod_id'],'itemType'=>'prod','photoID'=>$_smarty_tpl->tpl_vars['product']->value['photo']['id'],'size'=>50,'crop'=>40),$_smarty_tpl);?>
"><?php }?></div>
										<div class="divTableCell">
											<h2><a href="<?php echo $_smarty_tpl->tpl_vars['product']->value['linkto'];?>
" class="workboxLink"><?php echo $_smarty_tpl->tpl_vars['product']->value['name'];?>
</a></h2>
											<p class="featuredDescription"><?php echo smarty_modifier_truncate($_smarty_tpl->tpl_vars['product']->value['description'],60);?>
</p>
											<p class="featuredPrice"><?php if ($_smarty_tpl->tpl_vars['product']->value['price']){?><span class="price"><?php echo $_smarty_tpl->tpl_vars['product']->value['price']['display'];?>
</span><?php if ($_smarty_tpl->tpl_vars['product']->value['price']['taxInc']){?> <span class="taxIncMessage">(<?php echo $_smarty_tpl->tpl_vars['lang']->value['taxIncMessage'];?>
)</span><?php }?><?php }?><?php if ($_smarty_tpl->tpl_vars['config']->value['settings']['cart']==3&&$_smarty_tpl->tpl_vars['config']->value['settings']['credits_prod']){?> <?php echo $_smarty_tpl->tpl_vars['lang']->value['priceCreditSep'];?>
 <?php }?><?php if ($_smarty_tpl->tpl_vars['product']->value['credits']){?><span class="price"><?php echo $_smarty_tpl->tpl_vars['product']->value['credits'];?>
 <sup><?php echo $_smarty_tpl->tpl_vars['lang']->value['mediaLabelCredits'];?>
</sup></span><?php }?></p>
										</div>
									</div>
								<?php } ?>
							</div>						
							<?php if ($_smarty_tpl->tpl_vars['config']->value['settings']['prodpage']){?><p class="featuredBoxMore"><a href="<?php echo linkto(array('page'=>'featured.php?mode=products'),$_smarty_tpl);?>
"><?php echo $_smarty_tpl->tpl_vars['lang']->value['more'];?>
</a></p><?php }?>
						</div>
					<?php }?>
						
													
					<?php if ($_smarty_tpl->tpl_vars['featuredPackagesRows']->value){?>
						<div class="subNavFeaturedBox">
							<h1><?php echo $_smarty_tpl->tpl_vars['lang']->value['featuredPackages'];?>
</h1>
							<div class="divTable">
								<?php  $_smarty_tpl->tpl_vars['package'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['package']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['featuredPackages']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['package']->key => $_smarty_tpl->tpl_vars['package']->value){
$_smarty_tpl->tpl_vars['package']->_loop = true;
?>
									<div class="divTableRow workboxLinkAttach">
										<div class="divTableCell"><?php if ($_smarty_tpl->tpl_vars['package']->value['photo']){?><img src="<?php echo productShot(array('itemID'=>$_smarty_tpl->tpl_vars['package']->value['pack_id'],'itemType'=>'pack','photoID'=>$_smarty_tpl->tpl_vars['package']->value['photo']['id'],'size'=>50,'crop'=>40),$_smarty_tpl);?>
"><?php }?></div>
										<div class="divTableCell">
											<h2><a href="<?php echo $_smarty_tpl->tpl_vars['package']->value['linkto'];?>
" class="workboxLink"><?php echo $_smarty_tpl->tpl_vars['package']->value['name'];?>
</a></h2>
											<p class="featuredDescription"><?php echo smarty_modifier_truncate($_smarty_tpl->tpl_vars['package']->value['description'],60);?>
</p>
											<p class="featuredPrice"><?php if ($_smarty_tpl->tpl_vars['package']->value['price']){?><span class="price"><?php echo $_smarty_tpl->tpl_vars['package']->value['price']['display'];?>
</span><?php if ($_smarty_tpl->tpl_vars['package']->value['price']['taxInc']){?> <span class="taxIncMessage">(<?php echo $_smarty_tpl->tpl_vars['lang']->value['taxIncMessage'];?>
)</span><?php }?><?php }?><?php if ($_smarty_tpl->tpl_vars['config']->value['settings']['cart']==3&&$_smarty_tpl->tpl_vars['config']->value['settings']['credits_pack']){?> <?php echo $_smarty_tpl->tpl_vars['lang']->value['priceCreditSep'];?>
 <?php }?><?php if ($_smarty_tpl->tpl_vars['package']->value['credits']){?><span class="price"><?php echo $_smarty_tpl->tpl_vars['package']->value['credits'];?>
 <sup><?php echo $_smarty_tpl->tpl_vars['lang']->value['mediaLabelCredits'];?>
</sup></span><?php }?></p>
										</div>
									</div>
								<?php } ?>
							</div>					
							<?php if ($_smarty_tpl->tpl_vars['config']->value['settings']['packpage']){?><p class="featuredBoxMore"><a href="<?php echo linkto(array('page'=>'featured.php?mode=packages'),$_smarty_tpl);?>
"><?php echo $_smarty_tpl->tpl_vars['lang']->value['more'];?>
</a></p><?php }?>
						</div>
					<?php }?>
					
													
					<?php if ($_smarty_tpl->tpl_vars['featuredCollectionsRows']->value){?>
						<div class="subNavFeaturedBox">
							<h1><?php echo $_smarty_tpl->tpl_vars['lang']->value['featuredCollections'];?>
</h1>
							<div class="divTable">
								<?php  $_smarty_tpl->tpl_vars['collection'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['collection']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['featuredCollections']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['collection']->key => $_smarty_tpl->tpl_vars['collection']->value){
$_smarty_tpl->tpl_vars['collection']->_loop = true;
?>
									<div class="divTableRow workboxLinkAttach">
										<div class="divTableCell"><?php if ($_smarty_tpl->tpl_vars['collection']->value['photo']){?><img src="<?php echo productShot(array('itemID'=>$_smarty_tpl->tpl_vars['collection']->value['coll_id'],'itemType'=>'coll','photoID'=>$_smarty_tpl->tpl_vars['collection']->value['photo']['id'],'size'=>50,'crop'=>40),$_smarty_tpl);?>
"><?php }?></div>
										<div class="divTableCell">
											<h2><a href="<?php echo $_smarty_tpl->tpl_vars['collection']->value['linkto'];?>
" class="workboxLink"><?php echo $_smarty_tpl->tpl_vars['collection']->value['name'];?>
</a></h2>
											<p class="featuredDescription"><?php echo smarty_modifier_truncate($_smarty_tpl->tpl_vars['collection']->value['description'],60);?>
</p>
											<p class="featuredPrice"><?php if ($_smarty_tpl->tpl_vars['collection']->value['price']){?><span class="price"><?php echo $_smarty_tpl->tpl_vars['collection']->value['price']['display'];?>
</span><?php if ($_smarty_tpl->tpl_vars['collection']->value['price']['taxInc']){?> <span class="taxIncMessage">(<?php echo $_smarty_tpl->tpl_vars['lang']->value['taxIncMessage'];?>
)</span><?php }?><?php }?><?php if ($_smarty_tpl->tpl_vars['config']->value['settings']['cart']==3&&$_smarty_tpl->tpl_vars['config']->value['settings']['credits_coll']){?> <?php echo $_smarty_tpl->tpl_vars['lang']->value['priceCreditSep'];?>
 <?php }?><?php if ($_smarty_tpl->tpl_vars['collection']->value['credits']){?><span class="price"><?php echo $_smarty_tpl->tpl_vars['collection']->value['credits'];?>
 <sup><?php echo $_smarty_tpl->tpl_vars['lang']->value['mediaLabelCredits'];?>
</sup></span><?php }?></p>
										</div>
									</div>
								<?php } ?>
							</div>					
							<?php if ($_smarty_tpl->tpl_vars['config']->value['settings']['collpage']){?><p class="featuredBoxMore"><a href="<?php echo linkto(array('page'=>'featured.php?mode=collections'),$_smarty_tpl);?>
"><?php echo $_smarty_tpl->tpl_vars['lang']->value['more'];?>
</a></p><?php }?>
						</div>
					<?php }?>
					
													
					<?php if ($_smarty_tpl->tpl_vars['featuredPromotionsRows']->value){?>
						<div class="subNavFeaturedBox">
							<h1><?php echo $_smarty_tpl->tpl_vars['lang']->value['promotions'];?>
</h1>
							<div class="divTable">
								<?php  $_smarty_tpl->tpl_vars['promotion'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['promotion']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['featuredPromotions']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['promotion']->key => $_smarty_tpl->tpl_vars['promotion']->value){
$_smarty_tpl->tpl_vars['promotion']->_loop = true;
?>
									<div class="divTableRow workboxLinkAttach">
										<div class="divTableCell"><?php if ($_smarty_tpl->tpl_vars['promotion']->value['photo']){?><img src="<?php echo productShot(array('itemID'=>$_smarty_tpl->tpl_vars['promotion']->value['promo_id'],'itemType'=>'promo','photoID'=>$_smarty_tpl->tpl_vars['promotion']->value['photo']['id'],'size'=>50,'crop'=>40),$_smarty_tpl);?>
"><?php }?></div>
										<div class="divTableCell">
											<h2><a href="<?php echo $_smarty_tpl->tpl_vars['promotion']->value['linkto'];?>
" class="workboxLink"><?php echo $_smarty_tpl->tpl_vars['promotion']->value['name'];?>
</a></h2>
											<p class="featuredDescription"><?php echo smarty_modifier_truncate($_smarty_tpl->tpl_vars['promotion']->value['description'],60);?>
</p>
											<p class="featuredPrice"><?php if ($_smarty_tpl->tpl_vars['promotion']->value['price']){?><span class="price"><?php echo $_smarty_tpl->tpl_vars['promotion']->value['price']['display'];?>
</span><?php }?></p>
										</div>
									</div>
								<?php } ?>
							</div>					
							<?php if ($_smarty_tpl->tpl_vars['config']->value['settings']['promopage']){?><p class="featuredBoxMore"><a href="<?php echo linkto(array('page'=>'promotions.php'),$_smarty_tpl);?>
"><?php echo $_smarty_tpl->tpl_vars['lang']->value['more'];?>
</a></p><?php }?>
						</div>
					<?php }?>
					
													
					<?php if ($_smarty_tpl->tpl_vars['featuredSubscriptionsRows']->value){?>
						<div class="subNavFeaturedBox">
							<h1><?php echo $_smarty_tpl->tpl_vars['lang']->value['featuredSubscriptions'];?>
</h1>
							<div class="divTable">
								<?php  $_smarty_tpl->tpl_vars['subscription'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['subscription']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['featuredSubscriptions']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['subscription']->key => $_smarty_tpl->tpl_vars['subscription']->value){
$_smarty_tpl->tpl_vars['subscription']->_loop = true;
?>
									<div class="divTableRow workboxLinkAttach">
										<div class="divTableCell"><?php if ($_smarty_tpl->tpl_vars['subscription']->value['photo']){?><img src="<?php echo productShot(array('itemID'=>$_smarty_tpl->tpl_vars['subscription']->value['sub_id'],'itemType'=>'sub','photoID'=>$_smarty_tpl->tpl_vars['subscription']->value['photo']['id'],'size'=>50,'crop'=>40),$_smarty_tpl);?>
"><?php }?></div>
										<div class="divTableCell">
											<h2><a href="<?php echo $_smarty_tpl->tpl_vars['subscription']->value['linkto'];?>
" class="workboxLink"><?php echo $_smarty_tpl->tpl_vars['subscription']->value['name'];?>
</a></h2>
											<p class="featuredDescription"><?php echo smarty_modifier_truncate($_smarty_tpl->tpl_vars['subscription']->value['description'],60);?>
</p>
											<p class="featuredPrice"><?php if ($_smarty_tpl->tpl_vars['subscription']->value['price']){?><span class="price"><?php echo $_smarty_tpl->tpl_vars['subscription']->value['price']['display'];?>
</span><?php if ($_smarty_tpl->tpl_vars['subscription']->value['price']['taxInc']){?> <span class="taxIncMessage">(<?php echo $_smarty_tpl->tpl_vars['lang']->value['taxIncMessage'];?>
)</span><?php }?><?php }?><?php if ($_smarty_tpl->tpl_vars['config']->value['settings']['cart']==3&&$_smarty_tpl->tpl_vars['config']->value['settings']['credits_sub']){?> <?php echo $_smarty_tpl->tpl_vars['lang']->value['priceCreditSep'];?>
 <?php }?><?php if ($_smarty_tpl->tpl_vars['subscription']->value['credits']){?><span class="price"><?php echo $_smarty_tpl->tpl_vars['subscription']->value['credits'];?>
 <sup><?php echo $_smarty_tpl->tpl_vars['lang']->value['mediaLabelCredits'];?>
</sup></span><?php }?></p>
										</div>
									</div>
								<?php } ?>
							</div>					
							<?php if ($_smarty_tpl->tpl_vars['config']->value['settings']['subpage']){?><p class="featuredBoxMore"><a href="<?php echo linkto(array('page'=>'featured.php?mode=subscriptions'),$_smarty_tpl);?>
"><?php echo $_smarty_tpl->tpl_vars['lang']->value['more'];?>
</a></p><?php }?>
						</div>
					<?php }?>
					
													
					<?php if ($_smarty_tpl->tpl_vars['featuredCreditsRows']->value){?>
						<div class="subNavFeaturedBox">
							<h1><?php echo $_smarty_tpl->tpl_vars['lang']->value['featuredCredits'];?>
</h1>
							<div class="divTable">
								<?php  $_smarty_tpl->tpl_vars['credits'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['credits']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['featuredCredits']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['credits']->key => $_smarty_tpl->tpl_vars['credits']->value){
$_smarty_tpl->tpl_vars['credits']->_loop = true;
?>
									<div class="divTableRow workboxLinkAttach">
										<div class="divTableCell"><?php if ($_smarty_tpl->tpl_vars['credits']->value['photo']){?><img src="<?php echo productShot(array('itemID'=>$_smarty_tpl->tpl_vars['credits']->value['credit_id'],'itemType'=>'credit','photoID'=>$_smarty_tpl->tpl_vars['credits']->value['photo']['id'],'size'=>50,'crop'=>40),$_smarty_tpl);?>
"><?php }?></div>
										<div class="divTableCell">
											<h2><a href="<?php echo $_smarty_tpl->tpl_vars['credits']->value['linkto'];?>
" class="workboxLink"><?php echo $_smarty_tpl->tpl_vars['credits']->value['name'];?>
</a></h2>
											<p class="featuredDescription"><?php echo smarty_modifier_truncate($_smarty_tpl->tpl_vars['credits']->value['description'],60);?>
</p>
											<p class="featuredPrice"><?php if ($_smarty_tpl->tpl_vars['credits']->value['price']){?><span class="price"><?php echo $_smarty_tpl->tpl_vars['credits']->value['price']['display'];?>
</span><?php if ($_smarty_tpl->tpl_vars['credits']->value['price']['taxInc']){?> <span class="taxIncMessage">(<?php echo $_smarty_tpl->tpl_vars['lang']->value['taxIncMessage'];?>
)</span><?php }?><?php }?></p>
										</div>
									</div>
								<?php } ?>
							</div>					
							<?php if ($_smarty_tpl->tpl_vars['config']->value['settings']['creditpage']){?><p class="featuredBoxMore"><a href="<?php echo linkto(array('page'=>'featured.php?mode=credits'),$_smarty_tpl);?>
"><?php echo $_smarty_tpl->tpl_vars['lang']->value['more'];?>
</a></p><?php }?>
						</div>
					<?php }?>	
													
				</div>
				<div class="divTableCell contentRightColumn">					
					
					
					<?php if ($_smarty_tpl->tpl_vars['featuredNewsRows']->value){?>
						<div id="featuredNews">
							<?php if ($_smarty_tpl->tpl_vars['config']->value['settings']['rss_news']){?><a href="<?php echo linkto(array('page'=>'rss.php?mode=news'),$_smarty_tpl);?>
"><img src="<?php echo $_smarty_tpl->tpl_vars['imgPath']->value;?>
/rss.icon.large.png" id="homepageNewsRSS"></a><?php }?>
							<?php if ($_smarty_tpl->tpl_vars['config']->value['settings']['news']){?><p class="moreNews"><a href="<?php echo linkto(array('page'=>'news.php'),$_smarty_tpl);?>
"><?php echo $_smarty_tpl->tpl_vars['lang']->value['moreNews'];?>
 <img src="<?php echo $_smarty_tpl->tpl_vars['imgPath']->value;?>
/more.news.arrow.png"></a></p><?php }?>
							<div class="divTable">
							<?php  $_smarty_tpl->tpl_vars['news'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['news']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['featuredNews']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['news']->key => $_smarty_tpl->tpl_vars['news']->value){
$_smarty_tpl->tpl_vars['news']->_loop = true;
?>
								<div class="divTableRow">
									<div class="divTableCell"><?php echo $_smarty_tpl->tpl_vars['news']->value['display_date'];?>
</div>
									<div class="divTableCell">|</div>
									<div class="divTableCell"><a href="<?php echo $_smarty_tpl->tpl_vars['news']->value['linkto'];?>
"><?php echo $_smarty_tpl->tpl_vars['news']->value['title'];?>
</a></div>
								</div>
							<?php } ?>
							</div>
						</div>
					<?php }?>
					
					
					<?php if ($_smarty_tpl->tpl_vars['newestMediaRows']->value){?>
						<h1><?php if ($_smarty_tpl->tpl_vars['config']->value['settings']['newestpage']){?><a href="<?php echo linkto(array('page'=>'gallery.php?mode=newest-media&page=1'),$_smarty_tpl);?>
"><?php echo $_smarty_tpl->tpl_vars['lang']->value['newestMedia'];?>
</a><?php }else{ ?><?php echo $_smarty_tpl->tpl_vars['lang']->value['newestMedia'];?>
<?php }?><?php if ($_smarty_tpl->tpl_vars['config']->value['settings']['rss_newest']){?><a href="<?php echo linkto(array('page'=>'rss.php?mode=newestMedia'),$_smarty_tpl);?>
"><img src="<?php echo $_smarty_tpl->tpl_vars['imgPath']->value;?>
/rss.icon.small.png" class="rssH1Icon"></a><?php }?></h1>
						<div class="homepageMediaList">
							<?php  $_smarty_tpl->tpl_vars['media'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['media']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['newestMedia']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['media']->key => $_smarty_tpl->tpl_vars['media']->value){
$_smarty_tpl->tpl_vars['media']->_loop = true;
?>
								<?php echo $_smarty_tpl->getSubTemplate ('media.container.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

							<?php } ?>
						</div>
					<?php }?>
					
					
					<?php if ($_smarty_tpl->tpl_vars['popularMediaRows']->value){?>
						<h1><?php if ($_smarty_tpl->tpl_vars['config']->value['settings']['popularpage']){?><a href="<?php echo linkto(array('page'=>'gallery.php?mode=popular-media&page=1'),$_smarty_tpl);?>
"><?php echo $_smarty_tpl->tpl_vars['lang']->value['popularMedia'];?>
</a><?php }else{ ?><?php echo $_smarty_tpl->tpl_vars['lang']->value['popularMedia'];?>
<?php }?><?php if ($_smarty_tpl->tpl_vars['config']->value['settings']['rss_popular']){?><a href="<?php echo linkto(array('page'=>'rss.php?mode=popularMedia'),$_smarty_tpl);?>
"><img src="<?php echo $_smarty_tpl->tpl_vars['imgPath']->value;?>
/rss.icon.small.png" class="rssH1Icon"></a><?php }?></h1>
						<div class="homepageMediaList">
							<?php  $_smarty_tpl->tpl_vars['media'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['media']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['popularMedia']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['media']->key => $_smarty_tpl->tpl_vars['media']->value){
$_smarty_tpl->tpl_vars['media']->_loop = true;
?>
								<?php echo $_smarty_tpl->getSubTemplate ('media.container.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

							<?php } ?>
						</div>
					<?php }?>
					
					
					<?php if ($_smarty_tpl->tpl_vars['randomMediaRows']->value){?>
						<h1><?php echo $_smarty_tpl->tpl_vars['lang']->value['randomMedia'];?>
</h1>
						<div class="homepageMediaList">
							<?php  $_smarty_tpl->tpl_vars['media'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['media']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['randomMedia']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['media']->key => $_smarty_tpl->tpl_vars['media']->value){
$_smarty_tpl->tpl_vars['media']->_loop = true;
?>
								<?php echo $_smarty_tpl->getSubTemplate ('media.container.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

							<?php } ?>
						</div>
					<?php }?>
					
					<?php if ($_smarty_tpl->tpl_vars['subGalleriesData']->value){?>
						<h1><?php echo $_smarty_tpl->tpl_vars['lang']->value['featuredGalleries'];?>
</h1>						
						<div id="galleryListContainer">
							<?php  $_smarty_tpl->tpl_vars['subGallery'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['subGallery']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['subGalleriesData']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['subGallery']->key => $_smarty_tpl->tpl_vars['subGallery']->value){
$_smarty_tpl->tpl_vars['subGallery']->_loop = true;
?>
								<div class="galleryContainer" style="width: <?php echo $_smarty_tpl->tpl_vars['config']->value['settings']['gallery_thumb_size'];?>
px">
									<div class="galleryDetailsContainer" style="width: <?php echo $_smarty_tpl->tpl_vars['config']->value['settings']['gallery_thumb_size'];?>
px; <?php if ($_smarty_tpl->tpl_vars['galleriesData']->value[$_smarty_tpl->tpl_vars['subGallery']->value]['galleryIcon']){?>vertical-align: top<?php }?>">
										<?php if ($_smarty_tpl->tpl_vars['galleriesData']->value[$_smarty_tpl->tpl_vars['subGallery']->value]['galleryIcon']){?><p class="galleryIconContainer" style="min-width: <?php echo $_smarty_tpl->tpl_vars['galleriesData']->value[$_smarty_tpl->tpl_vars['subGallery']->value]['galleryIcon']['width'];?>
px; min-height: <?php echo $_smarty_tpl->tpl_vars['galleriesData']->value[$_smarty_tpl->tpl_vars['subGallery']->value]['galleryIcon']['height']+5;?>
px;"><a href="<?php echo $_smarty_tpl->tpl_vars['galleriesData']->value[$_smarty_tpl->tpl_vars['subGallery']->value]['linkto'];?>
"><img src="<?php echo $_smarty_tpl->tpl_vars['baseURL']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['galleriesData']->value[$_smarty_tpl->tpl_vars['subGallery']->value]['galleryIcon']['imgSrc'];?>
"></a></p><?php }?>
										<p class="galleryDetails"><?php if ($_smarty_tpl->tpl_vars['galleriesData']->value[$_smarty_tpl->tpl_vars['subGallery']->value]['password']){?><img src="<?php echo $_smarty_tpl->tpl_vars['imgPath']->value;?>
/lock.png" class="lock"><?php }?><a href="<?php echo $_smarty_tpl->tpl_vars['galleriesData']->value[$_smarty_tpl->tpl_vars['subGallery']->value]['linkto'];?>
"><?php echo $_smarty_tpl->tpl_vars['galleriesData']->value[$_smarty_tpl->tpl_vars['subGallery']->value]['name'];?>
</a><?php if ($_smarty_tpl->tpl_vars['config']->value['settings']['gallery_count']){?><?php if ($_smarty_tpl->tpl_vars['galleriesData']->value[$_smarty_tpl->tpl_vars['subGallery']->value]['gallery_count']>0||$_smarty_tpl->tpl_vars['config']->value['ShowZeroCounts']){?>&nbsp;<span class="galleryMediaCount">(<?php echo $_smarty_tpl->tpl_vars['galleriesData']->value[$_smarty_tpl->tpl_vars['subGallery']->value]['gallery_count'];?>
)</span><?php }?><?php }?></p>
									</div>
									<!--gi: <?php echo $_smarty_tpl->tpl_vars['galleriesData']->value[$_smarty_tpl->tpl_vars['subGallery']->value]['galleryIcon']['imgSrc'];?>
-->
								</div>
							<?php } ?>
						</div>
					<?php }?>
										
				</div>
			</div>
		</div>		
		<?php echo $_smarty_tpl->getSubTemplate ('footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>
		
    </div>
</body>
</html><?php }} ?>