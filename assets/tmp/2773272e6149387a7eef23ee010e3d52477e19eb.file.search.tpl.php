<?php /* Smarty version Smarty-3.1.8, created on 2017-01-11 18:40:46
         compiled from "/home/dynamikd/public_html/clients/piksel/assets/themes/modern/search.tpl" */ ?>
<?php /*%%SmartyHeaderCode:97247767258767c2ea64133-38200834%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '2773272e6149387a7eef23ee010e3d52477e19eb' => 
    array (
      0 => '/home/dynamikd/public_html/clients/piksel/assets/themes/modern/search.tpl',
      1 => 1403709658,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '97247767258767c2ea64133-38200834',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'baseURL' => 0,
    'lang' => 0,
    'config' => 0,
    'searchForm' => 0,
    'imgPath' => 0,
    'value' => 0,
    'keyword' => 0,
    'mediaTypesRows' => 0,
    'mediaTypeID' => 0,
    'mediaTypes' => 0,
    'name' => 0,
    'fromDateText' => 0,
    'toDateText' => 0,
    'licenseID' => 0,
    'licensesList' => 0,
    'galleryID' => 0,
    'galleriesData' => 0,
    'mediaRows' => 0,
    'mediaType' => 0,
    'fromYear' => 0,
    'fromMonth' => 0,
    'fromDay' => 0,
    'toYear' => 0,
    'toMonth' => 0,
    'toDay' => 0,
    'license' => 0,
    'galleries' => 0,
    'key' => 0,
    'gallery' => 0,
    'searchSortByOptions' => 0,
    'searchSortByTypeOptions' => 0,
    'mediaPaging' => 0,
    'mediaArray' => 0,
    'tags' => 0,
    'tag' => 0,
    'debugMode' => 0,
    'sql' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.8',
  'unifunc' => 'content_58767c2ee13156_96738136',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_58767c2ee13156_96738136')) {function content_58767c2ee13156_96738136($_smarty_tpl) {?><?php if (!is_callable('smarty_function_html_options')) include '/home/dynamikd/public_html/clients/piksel/assets/smarty/plugins/function.html_options.php';
?><!DOCTYPE HTML>
<html>
<head>
	<?php echo $_smarty_tpl->getSubTemplate ('head.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

	<script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['baseURL']->value;?>
/assets/javascript/colorpicker/js/colorpicker.js"></script>
	<script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['baseURL']->value;?>
/assets/javascript/search.js"></script>
	<script>
		var enterKeywords = '<?php echo $_smarty_tpl->tpl_vars['lang']->value['enterKeywords'];?>
';
		var requireSearchKeyword = '<?php echo $_smarty_tpl->tpl_vars['config']->value['requireSearchKeyword'];?>
';
		var searchFormHex = '<?php echo $_smarty_tpl->tpl_vars['searchForm']->value['hex'];?>
';
		
		$(function()
		{
			//$('.headerSearchBox').hide();
		});
	</script>
</head>
<body>
	<?php echo $_smarty_tpl->getSubTemplate ('overlays.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

	<div id="container">
		<?php echo $_smarty_tpl->getSubTemplate ('header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

		<?php echo $_smarty_tpl->getSubTemplate ('header2.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>
		
		
		<form action="search.php" method="get" id="searchForm" class="form-group">
		<input type="hidden" name="postSearchForm" id="postSearchForm" value="1">
		<input type="hidden" name="keywordsExist" id="keywordsExist" value="<?php if ($_smarty_tpl->tpl_vars['searchForm']->value['keywords']){?>1<?php }else{ ?>0<?php }?>">
		
		<div class="container">
			<div class="row">
				
				<div class="col-md-3">
					<h2><?php echo $_smarty_tpl->tpl_vars['lang']->value['currentSearch'];?>
</h2>						
					<div style="margin-bottom: 20px;">
						<div class="currentSearchBox">
						<?php if ($_smarty_tpl->tpl_vars['searchForm']->value['inSearch']){?>
							<ul>
								<li>
									<strong><?php echo $_smarty_tpl->tpl_vars['lang']->value['searchKeywords'];?>
:</strong>
									<?php if ($_smarty_tpl->tpl_vars['searchForm']->value['keywords']){?>
										<?php  $_smarty_tpl->tpl_vars['value'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['value']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['searchForm']->value['keywords']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['value']->index=-1;
foreach ($_from as $_smarty_tpl->tpl_vars['value']->key => $_smarty_tpl->tpl_vars['value']->value){
$_smarty_tpl->tpl_vars['value']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['value']->key;
 $_smarty_tpl->tpl_vars['value']->index++;
 $_smarty_tpl->tpl_vars['value']->first = $_smarty_tpl->tpl_vars['value']->index === 0;
?>
											<ul class="keywordQueries">
												<li>
													<?php if (!$_smarty_tpl->tpl_vars['value']->first){?><a href="<?php echo linkto(array('page'=>"search.php?clear=searchPhrase&batch=".($_smarty_tpl->tpl_vars['key']->value)),$_smarty_tpl);?>
"><img src="<?php echo $_smarty_tpl->tpl_vars['imgPath']->value;?>
/close.button.png" class="searchCloseButton"></a><?php }?>
													<?php if ($_smarty_tpl->tpl_vars['value']->value['connector']){?><span class="searchConnector"><?php echo $_smarty_tpl->tpl_vars['lang']->value[$_smarty_tpl->tpl_vars['value']->value['connector']];?>
 ( </span><?php }?>
													<?php  $_smarty_tpl->tpl_vars['keyword'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['keyword']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['value']->value['displayWords']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['keyword']->total= $_smarty_tpl->_count($_from);
 $_smarty_tpl->tpl_vars['keyword']->iteration=0;
foreach ($_from as $_smarty_tpl->tpl_vars['keyword']->key => $_smarty_tpl->tpl_vars['keyword']->value){
$_smarty_tpl->tpl_vars['keyword']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['keyword']->key;
 $_smarty_tpl->tpl_vars['keyword']->iteration++;
 $_smarty_tpl->tpl_vars['keyword']->last = $_smarty_tpl->tpl_vars['keyword']->iteration === $_smarty_tpl->tpl_vars['keyword']->total;
?>
														<span><?php echo $_smarty_tpl->tpl_vars['keyword']->value;?>
<?php if (!$_smarty_tpl->tpl_vars['keyword']->last){?> + <?php }?></span>
													<?php } ?>
													<?php if ($_smarty_tpl->tpl_vars['value']->value['connector']){?><span class="searchConnector">)</span><?php }?>
												</li>
											</ul>
										<?php } ?>
									<?php }else{ ?>
										<span><?php echo $_smarty_tpl->tpl_vars['lang']->value['none'];?>
</span>
									<?php }?>
								</li>
								<?php if ($_smarty_tpl->tpl_vars['mediaTypesRows']->value&&$_smarty_tpl->tpl_vars['config']->value['settings']['search_media_types']){?><li><strong><?php echo $_smarty_tpl->tpl_vars['lang']->value['searchHeaderType'];?>
:</strong> <span><?php if ($_smarty_tpl->tpl_vars['searchForm']->value['mediaTypes']){?><a href="<?php echo linkto(array('page'=>"search.php?clear=mediaTypes"),$_smarty_tpl);?>
"><img src="<?php echo $_smarty_tpl->tpl_vars['imgPath']->value;?>
/close.button.png" class="searchCloseButton"></a><?php  $_smarty_tpl->tpl_vars['mediaTypeID'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['mediaTypeID']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['searchForm']->value['mediaTypes']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['mediaTypeID']->total= $_smarty_tpl->_count($_from);
 $_smarty_tpl->tpl_vars['mediaTypeID']->iteration=0;
foreach ($_from as $_smarty_tpl->tpl_vars['mediaTypeID']->key => $_smarty_tpl->tpl_vars['mediaTypeID']->value){
$_smarty_tpl->tpl_vars['mediaTypeID']->_loop = true;
 $_smarty_tpl->tpl_vars['mediaTypeID']->iteration++;
 $_smarty_tpl->tpl_vars['mediaTypeID']->last = $_smarty_tpl->tpl_vars['mediaTypeID']->iteration === $_smarty_tpl->tpl_vars['mediaTypeID']->total;
?><?php echo $_smarty_tpl->tpl_vars['mediaTypes']->value[$_smarty_tpl->tpl_vars['mediaTypeID']->value]['name'];?>
<?php if (!$_smarty_tpl->tpl_vars['mediaTypeID']->last){?> + <?php }?><?php } ?><?php }else{ ?><?php echo $_smarty_tpl->tpl_vars['lang']->value['searchAll'];?>
<?php }?></span></li><?php }?>
								<?php if ($_smarty_tpl->tpl_vars['config']->value['settings']['search_fields']){?><li><strong><?php echo $_smarty_tpl->tpl_vars['lang']->value['searchHeaderFields'];?>
:</strong> <span><?php if ($_smarty_tpl->tpl_vars['searchForm']->value['fields']){?><a href="<?php echo linkto(array('page'=>"search.php?clear=fields"),$_smarty_tpl);?>
"><img src="<?php echo $_smarty_tpl->tpl_vars['imgPath']->value;?>
/close.button.png" class="searchCloseButton"></a><?php  $_smarty_tpl->tpl_vars['name'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['name']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['searchForm']->value['fields']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['name']->total= $_smarty_tpl->_count($_from);
 $_smarty_tpl->tpl_vars['name']->iteration=0;
foreach ($_from as $_smarty_tpl->tpl_vars['name']->key => $_smarty_tpl->tpl_vars['name']->value){
$_smarty_tpl->tpl_vars['name']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['name']->key;
 $_smarty_tpl->tpl_vars['name']->iteration++;
 $_smarty_tpl->tpl_vars['name']->last = $_smarty_tpl->tpl_vars['name']->iteration === $_smarty_tpl->tpl_vars['name']->total;
?><?php echo $_smarty_tpl->tpl_vars['name']->value;?>
<?php if (!$_smarty_tpl->tpl_vars['name']->last){?> + <?php }?><?php } ?><?php }else{ ?><?php echo $_smarty_tpl->tpl_vars['lang']->value['searchAll'];?>
<?php }?></span></li><?php }?>
								<?php if ($_smarty_tpl->tpl_vars['config']->value['settings']['search_orientation']){?><li><strong><?php echo $_smarty_tpl->tpl_vars['lang']->value['searchHeaderOrientation'];?>
:</strong> <span><?php if ($_smarty_tpl->tpl_vars['searchForm']->value['orientations']){?><a href="<?php echo linkto(array('page'=>"search.php?clear=orientations"),$_smarty_tpl);?>
"><img src="<?php echo $_smarty_tpl->tpl_vars['imgPath']->value;?>
/close.button.png" class="searchCloseButton"></a><?php  $_smarty_tpl->tpl_vars['name'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['name']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['searchForm']->value['orientations']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['name']->total= $_smarty_tpl->_count($_from);
 $_smarty_tpl->tpl_vars['name']->iteration=0;
foreach ($_from as $_smarty_tpl->tpl_vars['name']->key => $_smarty_tpl->tpl_vars['name']->value){
$_smarty_tpl->tpl_vars['name']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['name']->key;
 $_smarty_tpl->tpl_vars['name']->iteration++;
 $_smarty_tpl->tpl_vars['name']->last = $_smarty_tpl->tpl_vars['name']->iteration === $_smarty_tpl->tpl_vars['name']->total;
?><?php echo $_smarty_tpl->tpl_vars['name']->value;?>
<?php if (!$_smarty_tpl->tpl_vars['name']->last){?> + <?php }?><?php } ?><?php }else{ ?><?php echo $_smarty_tpl->tpl_vars['lang']->value['searchAll'];?>
<?php }?></span></li><?php }?>
								<?php if ($_smarty_tpl->tpl_vars['config']->value['settings']['search_dates']){?><li><strong><?php echo $_smarty_tpl->tpl_vars['lang']->value['dateRange'];?>
:</strong> <span><?php if ($_smarty_tpl->tpl_vars['searchForm']->value['searchDate']['dateRangeSearch']){?><a href="<?php echo linkto(array('page'=>"search.php?clear=dates"),$_smarty_tpl);?>
"><img src="<?php echo $_smarty_tpl->tpl_vars['imgPath']->value;?>
/close.button.png" class="searchCloseButton"></a><br><?php echo $_smarty_tpl->tpl_vars['fromDateText']->value;?>
 - <?php echo $_smarty_tpl->tpl_vars['toDateText']->value;?>
<?php }else{ ?><?php echo $_smarty_tpl->tpl_vars['lang']->value['searchAll'];?>
<?php }?></span></li><?php }?>
								<?php if ($_smarty_tpl->tpl_vars['config']->value['settings']['search_license_type']){?><li><strong><?php echo $_smarty_tpl->tpl_vars['lang']->value['license'];?>
:</strong> <span><?php if ($_smarty_tpl->tpl_vars['searchForm']->value['licenses']){?><a href="<?php echo linkto(array('page'=>"search.php?clear=licenses"),$_smarty_tpl);?>
"><img src="<?php echo $_smarty_tpl->tpl_vars['imgPath']->value;?>
/close.button.png" class="searchCloseButton"></a><?php  $_smarty_tpl->tpl_vars['licenseID'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['licenseID']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['searchForm']->value['licenses']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['licenseID']->total= $_smarty_tpl->_count($_from);
 $_smarty_tpl->tpl_vars['licenseID']->iteration=0;
foreach ($_from as $_smarty_tpl->tpl_vars['licenseID']->key => $_smarty_tpl->tpl_vars['licenseID']->value){
$_smarty_tpl->tpl_vars['licenseID']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['licenseID']->key;
 $_smarty_tpl->tpl_vars['licenseID']->iteration++;
 $_smarty_tpl->tpl_vars['licenseID']->last = $_smarty_tpl->tpl_vars['licenseID']->iteration === $_smarty_tpl->tpl_vars['licenseID']->total;
?><?php echo $_smarty_tpl->tpl_vars['licensesList']->value[$_smarty_tpl->tpl_vars['licenseID']->value]['licenseLang'];?>
<?php if (!$_smarty_tpl->tpl_vars['licenseID']->last){?> + <?php }?><?php } ?><?php }else{ ?><?php echo $_smarty_tpl->tpl_vars['lang']->value['searchAll'];?>
<?php }?></span></li><?php }?>
								<?php if ($_smarty_tpl->tpl_vars['config']->value['settings']['search_color']){?><li><strong><?php echo $_smarty_tpl->tpl_vars['lang']->value['searchHeaderColor'];?>
:</strong> <?php if ($_smarty_tpl->tpl_vars['searchForm']->value['hex']){?><a href="<?php echo linkto(array('page'=>"search.php?clear=colors"),$_smarty_tpl);?>
"><img src="<?php echo $_smarty_tpl->tpl_vars['imgPath']->value;?>
/close.button.png" class="searchCloseButton"></a><span class="colorSearchSpan" style="background-color: #<?php echo $_smarty_tpl->tpl_vars['searchForm']->value['hex'];?>
">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><?php }else{ ?><span><?php echo $_smarty_tpl->tpl_vars['lang']->value['searchAll'];?>
</span><?php }?></li><?php }?>
								<?php if ($_smarty_tpl->tpl_vars['config']->value['settings']['search_galleries']){?><li><strong><?php echo $_smarty_tpl->tpl_vars['lang']->value['searchHeaderGalleries'];?>
:</strong> <span><?php if ($_smarty_tpl->tpl_vars['searchForm']->value['galleries']){?><a href="<?php echo linkto(array('page'=>"search.php?clear=galleries"),$_smarty_tpl);?>
"><img src="<?php echo $_smarty_tpl->tpl_vars['imgPath']->value;?>
/close.button.png" class="searchCloseButton"></a><?php  $_smarty_tpl->tpl_vars['galleryID'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['galleryID']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['searchForm']->value['galleries']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['galleryID']->total= $_smarty_tpl->_count($_from);
 $_smarty_tpl->tpl_vars['galleryID']->iteration=0;
foreach ($_from as $_smarty_tpl->tpl_vars['galleryID']->key => $_smarty_tpl->tpl_vars['galleryID']->value){
$_smarty_tpl->tpl_vars['galleryID']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['galleryID']->key;
 $_smarty_tpl->tpl_vars['galleryID']->iteration++;
 $_smarty_tpl->tpl_vars['galleryID']->last = $_smarty_tpl->tpl_vars['galleryID']->iteration === $_smarty_tpl->tpl_vars['galleryID']->total;
?><?php echo $_smarty_tpl->tpl_vars['galleriesData']->value[$_smarty_tpl->tpl_vars['galleryID']->value]['name'];?>
<?php if (!$_smarty_tpl->tpl_vars['galleryID']->last){?> + <?php }?><?php } ?><?php }else{ ?><?php echo $_smarty_tpl->tpl_vars['lang']->value['searchAll'];?>
<?php }?></span></li><?php }?>
								<li><a href="<?php echo linkto(array('page'=>"search.php?clearSearch=true"),$_smarty_tpl);?>
"><?php echo $_smarty_tpl->tpl_vars['lang']->value['searchClear'];?>
</a></li>
							</ul>
						<?php }else{ ?>
							<?php echo $_smarty_tpl->tpl_vars['lang']->value['searchStart'];?>

						<?php }?>
						</div>
					</div>
					
					<?php if (!$_smarty_tpl->tpl_vars['searchForm']->value['inSearch']||$_smarty_tpl->tpl_vars['mediaRows']->value){?>
					
						<?php if ($_smarty_tpl->tpl_vars['searchForm']->value['inSearch']){?><h2><?php echo $_smarty_tpl->tpl_vars['lang']->value['searchNarrow'];?>
</h2><?php }?>
						
						<div class="searchOptionsColumn">
							<h2 style="margin-top: 0;"><?php echo $_smarty_tpl->tpl_vars['lang']->value['searchHeaderKeywords'];?>
</h2>
							<div style="position: relative">
								<input type="text" name="searchPhrase" id="searchPhrase2" value="" class="searchInputBox form-control">									
								<p class="searchMatchTerms"><label for="exactMatch"><?php echo $_smarty_tpl->tpl_vars['lang']->value['exactMatch'];?>
</label> <input type="checkbox" name="exactMatch" value="1" id="exactMatch"></p>
							</div>
						
							<?php if ($_smarty_tpl->tpl_vars['config']->value['settings']['search_fields']){?>
								<h2><?php echo $_smarty_tpl->tpl_vars['lang']->value['searchHeaderFields'];?>
</h2>								
								<ul>
									<li style="margin-bottom: 6px;"><input type="checkbox" name="fields[mediaID]" value="<?php echo $_smarty_tpl->tpl_vars['lang']->value['id'];?>
" id="fieldMediaID" <?php if ($_smarty_tpl->tpl_vars['searchForm']->value['fields']['mediaID']){?>checked="checked"<?php }?>> <label for="fieldMediaID"><?php echo $_smarty_tpl->tpl_vars['lang']->value['id'];?>
</label></li>
									<li style="margin-bottom: 6px;"><input type="checkbox" name="fields[keywords]" value="<?php echo $_smarty_tpl->tpl_vars['lang']->value['searchKeywords'];?>
" id="fieldKeywords" <?php if ($_smarty_tpl->tpl_vars['searchForm']->value['fields']['keywords']){?>checked="checked"<?php }?>> <label for="fieldKeywords"><?php echo $_smarty_tpl->tpl_vars['lang']->value['searchKeywords'];?>
</label></li>
									<li style="margin-bottom: 6px;"><input type="checkbox" name="fields[title]" value="<?php echo $_smarty_tpl->tpl_vars['lang']->value['searchTitle'];?>
" id="fieldTitle" <?php if ($_smarty_tpl->tpl_vars['searchForm']->value['fields']['title']){?>checked="checked"<?php }?>> <label for="fieldTitle"><?php echo $_smarty_tpl->tpl_vars['lang']->value['searchTitle'];?>
</label></li>
									<li style="margin-bottom: 6px;"><input type="checkbox" name="fields[description]" value="<?php echo $_smarty_tpl->tpl_vars['lang']->value['searchDescription'];?>
" id="fieldDescription" <?php if ($_smarty_tpl->tpl_vars['searchForm']->value['fields']['description']){?>checked="checked"<?php }?>> <label for="fieldDescription"><?php echo $_smarty_tpl->tpl_vars['lang']->value['searchDescription'];?>
</label></li>
									<li style="margin-bottom: 6px;"><input type="checkbox" name="fields[filename]" value="<?php echo $_smarty_tpl->tpl_vars['lang']->value['searchFilename'];?>
" id="fieldFilename" <?php if ($_smarty_tpl->tpl_vars['searchForm']->value['fields']['filename']){?>checked="checked"<?php }?>> <label for="fieldFilename"><?php echo $_smarty_tpl->tpl_vars['lang']->value['searchFilename'];?>
</label></li>
								</ul>
							<?php }?>
							
							<?php if ($_smarty_tpl->tpl_vars['mediaTypesRows']->value&&$_smarty_tpl->tpl_vars['config']->value['settings']['search_media_types']){?>
								<h2><?php echo $_smarty_tpl->tpl_vars['lang']->value['searchHeaderType'];?>
</h2>								
								<ul>
									<?php  $_smarty_tpl->tpl_vars['mediaType'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['mediaType']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['mediaTypes']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['mediaType']->key => $_smarty_tpl->tpl_vars['mediaType']->value){
$_smarty_tpl->tpl_vars['mediaType']->_loop = true;
?>
										<li style="margin-bottom: 6px;"><input type="checkbox" name="mediaTypes[<?php echo $_smarty_tpl->tpl_vars['mediaType']->value['mt_id'];?>
]" id="mediaType<?php echo $_smarty_tpl->tpl_vars['mediaType']->value['mt_id'];?>
" value="<?php echo $_smarty_tpl->tpl_vars['mediaType']->value['mt_id'];?>
" <?php if ($_smarty_tpl->tpl_vars['mediaType']->value['selected']){?>checked="checked"<?php }?>> <label for="mediaType<?php echo $_smarty_tpl->tpl_vars['mediaType']->value['mt_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['mediaType']->value['name'];?>
</label></li>
									<?php } ?>
								</ul>
							<?php }?>
							
							<div style="overflow: auto; clear: both;">
								<?php if ($_smarty_tpl->tpl_vars['config']->value['settings']['search_orientation']){?>									
									<div style="float: left; width: 50%">
										<h2><?php echo $_smarty_tpl->tpl_vars['lang']->value['searchHeaderOrientation'];?>
</h2>
										<ul>
											<li style="margin-bottom: 6px;"><input type="checkbox" name="orientations[portrait]" value="<?php echo $_smarty_tpl->tpl_vars['lang']->value['searchPortrait'];?>
" id="orientationPortrait" <?php if ($_smarty_tpl->tpl_vars['searchForm']->value['orientations']['portrait']){?>checked="checked"<?php }?>> <label for="orientationPortrait"><?php echo $_smarty_tpl->tpl_vars['lang']->value['searchPortrait'];?>
</label></li>
											<li style="margin-bottom: 6px;"><input type="checkbox" name="orientations[landscape]" value="<?php echo $_smarty_tpl->tpl_vars['lang']->value['searchLandscape'];?>
" id="orientationLandscape" <?php if ($_smarty_tpl->tpl_vars['searchForm']->value['orientations']['landscape']){?>checked="checked"<?php }?> > <label for="orientationLandscape"><?php echo $_smarty_tpl->tpl_vars['lang']->value['searchLandscape'];?>
</label></li>
											<li style="margin-bottom: 6px;"><input type="checkbox" name="orientations[square]" value="<?php echo $_smarty_tpl->tpl_vars['lang']->value['searchSquare'];?>
" id="orientationSquare" <?php if ($_smarty_tpl->tpl_vars['searchForm']->value['orientations']['square']){?>checked="checked"<?php }?>> <label for="orientationSquare"><?php echo $_smarty_tpl->tpl_vars['lang']->value['searchSquare'];?>
</label></li>
										</ul>									
									</div>
								<?php }?>
								<?php if ($_smarty_tpl->tpl_vars['config']->value['settings']['search_color']){?>
									<div style="float: left; width: 50%">
										<h2><?php echo $_smarty_tpl->tpl_vars['lang']->value['searchHeaderColor'];?>
</h2>	
										<input type="hidden" name="red" id="red" value="<?php echo $_smarty_tpl->tpl_vars['searchForm']->value['red'];?>
">
										<input type="hidden" name="green" id="green" value="<?php echo $_smarty_tpl->tpl_vars['searchForm']->value['green'];?>
">
										<input type="hidden" name="blue" id="blue" value="<?php echo $_smarty_tpl->tpl_vars['searchForm']->value['blue'];?>
">	
										<input type="hidden" name="hex" id="hex" value="<?php echo $_smarty_tpl->tpl_vars['searchForm']->value['hex'];?>
">								
										<div id="colorpickerHolder">
											<div></div>
										</div>
									</div>
								<?php }?>
							</div>
					
							<?php if ($_smarty_tpl->tpl_vars['config']->value['settings']['search_dates']){?>
								<div>
									<h2><?php echo $_smarty_tpl->tpl_vars['lang']->value['dates'];?>
</h2>								
									<input type="hidden" name="searchDate[dateRangeSearch]" id="dateRangeSearch" value="<?php if ($_smarty_tpl->tpl_vars['searchForm']->value['searchDate']['dateRangeSearch']){?>on<?php }else{ ?>off<?php }?>">
									<p style="padding-bottom: 4px;"><input type="checkbox" name="searchDate[dateRangeSearchCB]" id="dateRangeSearchCB" value="1" <?php if ($_smarty_tpl->tpl_vars['searchForm']->value['searchDate']['dateRangeSearch']){?>checked="checked"<?php }?>> <label for="dateRangeSearchCB"><?php echo $_smarty_tpl->tpl_vars['lang']->value['searchDateRange'];?>
</label></p>
									<div class="searchDate" <?php if (!$_smarty_tpl->tpl_vars['searchForm']->value['searchDate']['dateRangeSearch']){?>style="display: none;"<?php }?>>
										<p><?php echo $_smarty_tpl->tpl_vars['lang']->value['from'];?>
</p>
										<select name="searchDate[fromYear]">
											<?php echo smarty_function_html_options(array('options'=>$_smarty_tpl->tpl_vars['fromYear']->value,'selected'=>$_smarty_tpl->tpl_vars['searchForm']->value['searchDate']['fromYear']),$_smarty_tpl);?>

										</select>
										<select name="searchDate[fromMonth]">
											<?php echo smarty_function_html_options(array('options'=>$_smarty_tpl->tpl_vars['fromMonth']->value,'selected'=>$_smarty_tpl->tpl_vars['searchForm']->value['searchDate']['fromMonth']),$_smarty_tpl);?>

										</select>
										<select name="searchDate[fromDay]">
											<?php echo smarty_function_html_options(array('options'=>$_smarty_tpl->tpl_vars['fromDay']->value,'selected'=>$_smarty_tpl->tpl_vars['searchForm']->value['searchDate']['fromDay']),$_smarty_tpl);?>

										</select>
										<p><?php echo $_smarty_tpl->tpl_vars['lang']->value['to'];?>
</p>
										<select name="searchDate[toYear]">
											<?php echo smarty_function_html_options(array('options'=>$_smarty_tpl->tpl_vars['toYear']->value,'selected'=>$_smarty_tpl->tpl_vars['searchForm']->value['searchDate']['toYear']),$_smarty_tpl);?>

										</select>
										<select name="searchDate[toMonth]">
											<?php echo smarty_function_html_options(array('options'=>$_smarty_tpl->tpl_vars['toMonth']->value,'selected'=>$_smarty_tpl->tpl_vars['searchForm']->value['searchDate']['toMonth']),$_smarty_tpl);?>

										</select>
										<select name="searchDate[toDay]">
											<?php echo smarty_function_html_options(array('options'=>$_smarty_tpl->tpl_vars['toDay']->value,'selected'=>$_smarty_tpl->tpl_vars['searchForm']->value['searchDate']['toDay']),$_smarty_tpl);?>

										</select>
									</div>
								</div>
							<?php }?>
							
							<?php if ($_smarty_tpl->tpl_vars['config']->value['settings']['search_license_type']){?>
								<h2><?php echo $_smarty_tpl->tpl_vars['lang']->value['licenseType'];?>
</h2>							
								<!--
								<ul>
									<li style="margin-bottom: 6px;"><input type="checkbox" name="licenses[royaltyFree]" value="<?php echo $_smarty_tpl->tpl_vars['lang']->value['searchRoyaltyFree'];?>
" id="licenseRoyaltyFree" <?php if ($_smarty_tpl->tpl_vars['searchForm']->value['licenses']['royaltyFree']){?>checked="checked"<?php }?>> <label for="licenseRoyaltyFree"><?php echo $_smarty_tpl->tpl_vars['lang']->value['searchRoyaltyFree'];?>
</label></li>
									<li style="margin-bottom: 6px;"><input type="checkbox" name="licenses[free]" value="<?php echo $_smarty_tpl->tpl_vars['lang']->value['searchFree'];?>
" id="licenseFree" <?php if ($_smarty_tpl->tpl_vars['searchForm']->value['licenses']['free']){?>checked="checked"<?php }?>> <label for="licenseFree"><?php echo $_smarty_tpl->tpl_vars['lang']->value['searchFree'];?>
</label></li>
									<li style="margin-bottom: 6px;"><input type="checkbox" name="licenses[contactUs]" value="<?php echo $_smarty_tpl->tpl_vars['lang']->value['searchContactUs'];?>
" id="licenseContactUs" <?php if ($_smarty_tpl->tpl_vars['searchForm']->value['licenses']['contactUs']){?>checked="checked"<?php }?>> <label for="licenseContactUs"><?php echo $_smarty_tpl->tpl_vars['lang']->value['searchContactUs'];?>
</label></li>										
									<li style="margin-bottom: 6px;"><input type="checkbox" name="licenses[extended]" value="<?php echo $_smarty_tpl->tpl_vars['lang']->value['mediaLicenseEX'];?>
" id="licenseExtended" <?php if ($_smarty_tpl->tpl_vars['searchForm']->value['licenses']['extended']){?>checked="checked"<?php }?>> <label for="licenseExtended"><?php echo $_smarty_tpl->tpl_vars['lang']->value['mediaLicenseEX'];?>
</label></li>
									<li style="margin-bottom: 6px;"><input type="checkbox" name="licenses[editorial]" value="<?php echo $_smarty_tpl->tpl_vars['lang']->value['mediaLicenseEU'];?>
" id="licenseEditorial" <?php if ($_smarty_tpl->tpl_vars['searchForm']->value['licenses']['editorial']){?>checked="checked"<?php }?>> <label for="licenseEditorial"><?php echo $_smarty_tpl->tpl_vars['lang']->value['mediaLicenseEU'];?>
</label></li>
								</ul>
								-->
								
								<ul>
									<?php  $_smarty_tpl->tpl_vars['license'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['license']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['licensesList']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['license']->key => $_smarty_tpl->tpl_vars['license']->value){
$_smarty_tpl->tpl_vars['license']->_loop = true;
?>
										<li style="margin-bottom: 6px;"><input type="checkbox" name="licenses[<?php echo $_smarty_tpl->tpl_vars['license']->value['license_id'];?>
]" value="<?php echo $_smarty_tpl->tpl_vars['license']->value['license_id'];?>
" id="lic<?php echo $_smarty_tpl->tpl_vars['license']->value['license_id'];?>
" <?php if ($_smarty_tpl->tpl_vars['license']->value['selected']){?>checked="checked"<?php }?>> <label for="lic<?php echo $_smarty_tpl->tpl_vars['license']->value['license_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['license']->value['licenseLang'];?>
</label></li>	
									<?php } ?>
								</ul>
								
							<?php }?>
							
							<?php if ($_smarty_tpl->tpl_vars['config']->value['settings']['search_galleries']){?>
								<h2><?php echo $_smarty_tpl->tpl_vars['lang']->value['searchHeaderGalleries'];?>
</h2>
								<ul>
									<?php  $_smarty_tpl->tpl_vars['gallery'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['gallery']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['galleries']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['gallery']->key => $_smarty_tpl->tpl_vars['gallery']->value){
$_smarty_tpl->tpl_vars['gallery']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['gallery']->key;
?>
										<li style="margin-bottom: 6px;"><input type="checkbox" name="galleries[]" id="gallery<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
" value="<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
" <?php if ($_smarty_tpl->tpl_vars['gallery']->value['selected']){?>checked="checked"<?php }?>> <label for="gallery<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['gallery']->value['name'];?>
</label></li>
									<?php } ?>
								</ul>
							<?php }?>
							
							<br><br>						
							<input type="button" value="<?php echo $_smarty_tpl->tpl_vars['lang']->value['searchUpper'];?>
" style="width: 100%;" class="btn btn-xs btn-primary" id="searchButton">							
							<br><br>
						
						</div>
					<?php }?>
				</div>
				
					
				<div class="col-md-9">
					
					<h1><?php echo $_smarty_tpl->tpl_vars['lang']->value['search'];?>
</h1>
					<hr>						
					<?php if ($_smarty_tpl->tpl_vars['searchForm']->value['inSearch']){?>
						<?php if ($_smarty_tpl->tpl_vars['mediaRows']->value){?>
							<p class="sortByContainer">
								<?php echo $_smarty_tpl->tpl_vars['lang']->value['sortBy'];?>
  
								<select name="searchSortBy" id="searchSortBy">
									<?php echo smarty_function_html_options(array('options'=>$_smarty_tpl->tpl_vars['searchSortByOptions']->value,'selected'=>$_smarty_tpl->tpl_vars['searchForm']->value['searchSortBy']),$_smarty_tpl);?>

								</select>
								<!--
								<select name="searchSortType" id="searchSortType">
									<?php echo smarty_function_html_options(array('options'=>$_smarty_tpl->tpl_vars['searchSortByTypeOptions']->value,'selected'=>$_smarty_tpl->tpl_vars['searchForm']->value['searchSortType']),$_smarty_tpl);?>

								</select>
								-->
							</p>
							
							<?php echo $_smarty_tpl->getSubTemplate ("paging.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('paging'=>$_smarty_tpl->tpl_vars['mediaPaging']->value), 0);?>

							<div id="mediaListContainer">
								<?php  $_smarty_tpl->tpl_vars['media'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['media']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['mediaArray']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['media']->key => $_smarty_tpl->tpl_vars['media']->value){
$_smarty_tpl->tpl_vars['media']->_loop = true;
?>
									<?php echo $_smarty_tpl->getSubTemplate ('media.container.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

								<?php } ?>
							</div>
							<?php echo $_smarty_tpl->getSubTemplate ("paging.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('paging'=>$_smarty_tpl->tpl_vars['mediaPaging']->value), 0);?>

						<?php }else{ ?>
							<p class="notice"><?php echo $_smarty_tpl->tpl_vars['lang']->value['searchNoResults'];?>
</p><br>
						<?php }?>
					<?php }else{ ?>
						<?php echo $_smarty_tpl->tpl_vars['lang']->value['searchEnterKeyMessage'];?>

					<?php }?>
					
					<?php if ($_smarty_tpl->tpl_vars['tags']->value){?>
						<ul class="searchTagList">
						<?php  $_smarty_tpl->tpl_vars['tag'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['tag']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['tags']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['tag']->total= $_smarty_tpl->_count($_from);
 $_smarty_tpl->tpl_vars['tag']->iteration=0;
foreach ($_from as $_smarty_tpl->tpl_vars['tag']->key => $_smarty_tpl->tpl_vars['tag']->value){
$_smarty_tpl->tpl_vars['tag']->_loop = true;
 $_smarty_tpl->tpl_vars['tag']->iteration++;
 $_smarty_tpl->tpl_vars['tag']->last = $_smarty_tpl->tpl_vars['tag']->iteration === $_smarty_tpl->tpl_vars['tag']->total;
?>
							<li><a href="search.php?clearSearch=true&searchPhrase=<?php echo $_smarty_tpl->tpl_vars['tag']->value['keyword'];?>
"><?php echo $_smarty_tpl->tpl_vars['tag']->value['keyword'];?>
</a><!-- <span class="searchTagCount">(<?php echo $_smarty_tpl->tpl_vars['tag']->value['count'];?>
)</span>--></li>
						<?php } ?>
						</ul>
						<!-- Tag cloud
						<div style="clear: both; margin-top: 20px;">
						<?php  $_smarty_tpl->tpl_vars['tag'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['tag']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['tags']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['tag']->total= $_smarty_tpl->_count($_from);
 $_smarty_tpl->tpl_vars['tag']->iteration=0;
foreach ($_from as $_smarty_tpl->tpl_vars['tag']->key => $_smarty_tpl->tpl_vars['tag']->value){
$_smarty_tpl->tpl_vars['tag']->_loop = true;
 $_smarty_tpl->tpl_vars['tag']->iteration++;
 $_smarty_tpl->tpl_vars['tag']->last = $_smarty_tpl->tpl_vars['tag']->iteration === $_smarty_tpl->tpl_vars['tag']->total;
?>
							<a href="search.php?clearSearch=true&searchPhrase=<?php echo $_smarty_tpl->tpl_vars['tag']->value['keyword'];?>
" style="font-size: <?php echo $_smarty_tpl->tpl_vars['tag']->value['fontSize'];?>
px"><?php echo $_smarty_tpl->tpl_vars['tag']->value['keyword'];?>
</a> <?php if (!$_smarty_tpl->tpl_vars['tag']->last){?>,<?php }?>
						<?php } ?>
						</div							-->
					<?php }?>
					
				</div>
			</div>
		</div>
		</form>
		
		<?php if ($_smarty_tpl->tpl_vars['debugMode']->value){?>
			<?php echo debugOutput(array('value'=>$_smarty_tpl->tpl_vars['searchForm']->value,'title'=>'searchForm'),$_smarty_tpl);?>

			<?php echo debugOutput(array('value'=>$_smarty_tpl->tpl_vars['sql']->value,'title'=>'sql'),$_smarty_tpl);?>

		<?php }?>
		<?php echo $_smarty_tpl->getSubTemplate ('footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

    </div>
</body>
</html><?php }} ?>