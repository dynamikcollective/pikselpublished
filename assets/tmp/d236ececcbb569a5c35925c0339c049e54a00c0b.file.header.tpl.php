<?php /* Smarty version Smarty-3.1.8, created on 2017-01-19 12:21:23
         compiled from "/home/dynamikd/public_html/clients/piksel/assets/themes/sleek/header.tpl" */ ?>
<?php /*%%SmartyHeaderCode:14082489745880af4376ca70-36670985%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'd236ececcbb569a5c35925c0339c049e54a00c0b' => 
    array (
      0 => '/home/dynamikd/public_html/clients/piksel/assets/themes/sleek/header.tpl',
      1 => 1411995270,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '14082489745880af4376ca70-36670985',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'config' => 0,
    'imgPath' => 0,
    'cartStatus' => 0,
    'lang' => 0,
    'cartTotals' => 0,
    'currencySystem' => 0,
    'creditSystem' => 0,
    'featuredTab' => 0,
    'contribLink' => 0,
    'mainLogo' => 0,
    'displayLanguages' => 0,
    'selectedLanguage' => 0,
    'language' => 0,
    'displayCurrencies' => 0,
    'selectedCurrency' => 0,
    'activeCurrencies' => 0,
    'currency' => 0,
    'loggedIn' => 0,
    'member' => 0,
    'lightboxSystem' => 0,
    'message' => 0,
    'messageLang' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.8',
  'unifunc' => 'content_5880af4392abd1_80102186',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5880af4392abd1_80102186')) {function content_5880af4392abd1_80102186($_smarty_tpl) {?>	<div id="topNavContainer">
		<div class="center">			
			<?php if ($_smarty_tpl->tpl_vars['config']->value['settings']['facebook_link']||$_smarty_tpl->tpl_vars['config']->value['settings']['twitter_link']){?><div id="social" style="float: right; margin-top: 8px; margin-left: 20px;"><?php if ($_smarty_tpl->tpl_vars['config']->value['settings']['facebook_link']){?><a href="<?php echo $_smarty_tpl->tpl_vars['config']->value['settings']['facebook_link'];?>
" target="_blank"><img src="<?php echo $_smarty_tpl->tpl_vars['imgPath']->value;?>
/facebook.icon.png" width="20" title="Facebook"></a><?php }?>&nbsp;<?php if ($_smarty_tpl->tpl_vars['config']->value['settings']['twitter_link']){?><a href="<?php echo $_smarty_tpl->tpl_vars['config']->value['settings']['twitter_link'];?>
" target="_blank"><img src="<?php echo $_smarty_tpl->tpl_vars['imgPath']->value;?>
/twitter.icon.png" width="20" title="Twitter"></a><?php }?></div><?php }?>
			
			<?php if ($_smarty_tpl->tpl_vars['cartStatus']->value){?>
				<div id="cartPreviewContainer">
					<div id="miniCartContainer">...</div>
					<div>
						<div style="float: left;"><?php echo $_smarty_tpl->tpl_vars['lang']->value['cart'];?>
:&nbsp;&nbsp; <a href="<?php echo linkto(array('page'=>"cart.php"),$_smarty_tpl);?>
" class="viewCartLink"><span id="cartItemsCount"><?php echo $_smarty_tpl->tpl_vars['cartTotals']->value['itemsInCart'];?>
</span>&nbsp;<?php echo $_smarty_tpl->tpl_vars['lang']->value['items'];?>
 </a> &nbsp; </div>
						<div style="float: left; display:<?php if ($_smarty_tpl->tpl_vars['cartTotals']->value['priceSubTotal']||$_smarty_tpl->tpl_vars['cartTotals']->value['creditsSubTotalPreview']){?>block<?php }else{ ?>none<?php }?>;" id="cartPreview">
							<a href="<?php echo linkto(array('page'=>"cart.php"),$_smarty_tpl);?>
" class="viewCartLink">
							<span id="cartPreviewPrice" style="<?php if (!$_smarty_tpl->tpl_vars['currencySystem']->value){?>display: none;<?php }?>"><?php echo $_smarty_tpl->tpl_vars['cartTotals']->value['priceSubTotalPreview']['display'];?>
</span>
							<?php if ($_smarty_tpl->tpl_vars['creditSystem']->value&&$_smarty_tpl->tpl_vars['currencySystem']->value){?> + <?php }?>
							<span id="cartPreviewCredits" style="<?php if (!$_smarty_tpl->tpl_vars['creditSystem']->value){?>display: none;<?php }?>"><?php echo $_smarty_tpl->tpl_vars['cartTotals']->value['creditsSubTotalPreview'];?>
 </span> <?php if ($_smarty_tpl->tpl_vars['creditSystem']->value){?><?php echo $_smarty_tpl->tpl_vars['lang']->value['credits'];?>
<?php }?>
							</a>
						</div>
					</div>
				</div>
			<?php }?>
			<ul id="topNav">
				<?php if ($_smarty_tpl->tpl_vars['config']->value['settings']['news']){?><li id="navNews"><a href="<?php echo linkto(array('page'=>"news.php"),$_smarty_tpl);?>
"><?php echo $_smarty_tpl->tpl_vars['lang']->value['news'];?>
</a></li><?php }?>
				<?php if ($_smarty_tpl->tpl_vars['featuredTab']->value){?>
					<li id="featuredNavButton">
						<a href="#"><?php echo $_smarty_tpl->tpl_vars['lang']->value['featuredItems'];?>
</a>
						<ul>
							<?php if ($_smarty_tpl->tpl_vars['config']->value['settings']['featuredpage']){?><li id="featuredSubnavMedia"><a href="<?php echo linkto(array('page'=>"gallery.php?mode=featured-media&page=1"),$_smarty_tpl);?>
"><?php echo $_smarty_tpl->tpl_vars['lang']->value['mediaNav'];?>
</a></li><?php }?>
							<?php if ($_smarty_tpl->tpl_vars['config']->value['settings']['printpage']){?><li id="featuredSubnavPrints"><a href="<?php echo linkto(array('page'=>"featured.php?mode=prints"),$_smarty_tpl);?>
"><?php echo $_smarty_tpl->tpl_vars['lang']->value['prints'];?>
</a></li><?php }?>
							<?php if ($_smarty_tpl->tpl_vars['config']->value['settings']['prodpage']){?><li id="featuredSubnavProducts"><a href="<?php echo linkto(array('page'=>"featured.php?mode=products"),$_smarty_tpl);?>
"><?php echo $_smarty_tpl->tpl_vars['lang']->value['products'];?>
</a></li><?php }?>
							<?php if ($_smarty_tpl->tpl_vars['config']->value['settings']['packpage']){?><li id="featuredSubnavPackages"><a href="<?php echo linkto(array('page'=>"featured.php?mode=packages"),$_smarty_tpl);?>
"><?php echo $_smarty_tpl->tpl_vars['lang']->value['packages'];?>
</a></li><?php }?>
							<?php if ($_smarty_tpl->tpl_vars['config']->value['settings']['collpage']){?><li id="featuredSubnavCollections"><a href="<?php echo linkto(array('page'=>"featured.php?mode=collections"),$_smarty_tpl);?>
"><?php echo $_smarty_tpl->tpl_vars['lang']->value['collections'];?>
</a></li><?php }?>
							<?php if ($_smarty_tpl->tpl_vars['config']->value['settings']['subpage']&&$_smarty_tpl->tpl_vars['config']->value['settings']['subscriptions']){?><li id="featuredSubnavSubscriptions"><a href="<?php echo linkto(array('page'=>"featured.php?mode=subscriptions"),$_smarty_tpl);?>
"><?php echo $_smarty_tpl->tpl_vars['lang']->value['subscriptions'];?>
</a></li><?php }?>
							<?php if ($_smarty_tpl->tpl_vars['config']->value['settings']['creditpage']){?><li id="featuredSubnavCredits"><a href="<?php echo linkto(array('page'=>"featured.php?mode=credits"),$_smarty_tpl);?>
"><?php echo $_smarty_tpl->tpl_vars['lang']->value['credits'];?>
</a></li><?php }?>
						</ul>
					</li>
				<?php }?>
				<li id="navGalleries"><a href="<?php echo linkto(array('page'=>"gallery.php?mode=gallery"),$_smarty_tpl);?>
"><?php echo $_smarty_tpl->tpl_vars['lang']->value['galleries'];?>
</a></li>
				<?php if ($_smarty_tpl->tpl_vars['config']->value['settings']['newestpage']){?><li id="navNewestMedia"><a href="<?php echo linkto(array('page'=>"gallery.php?mode=newest-media&page=1"),$_smarty_tpl);?>
"><?php echo $_smarty_tpl->tpl_vars['lang']->value['newestMedia'];?>
</a></li><?php }?>
				<?php if ($_smarty_tpl->tpl_vars['config']->value['settings']['popularpage']){?><li id="navPopularMedia"><a href="<?php echo linkto(array('page'=>"gallery.php?mode=popular-media&page=1"),$_smarty_tpl);?>
"><?php echo $_smarty_tpl->tpl_vars['lang']->value['popularMedia'];?>
</a></li><?php }?>
				<?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['contribLink']->value;?>
<?php $_tmp1=ob_get_clean();?><?php if (addon('contr')&&$_tmp1==1){?><li id="navContributors"><a href="<?php echo linkto(array('page'=>"contributors.php"),$_smarty_tpl);?>
"><?php echo $_smarty_tpl->tpl_vars['lang']->value['contributors'];?>
</a></li><?php }?>
				<?php if ($_smarty_tpl->tpl_vars['config']->value['settings']['promopage']){?><li id="navPromotions"><a href="<?php echo linkto(array('page'=>"promotions.php"),$_smarty_tpl);?>
"><?php echo $_smarty_tpl->tpl_vars['lang']->value['promotions'];?>
</a></li><?php }?>
				<?php if ($_smarty_tpl->tpl_vars['config']->value['settings']['aboutpage']){?><li id="navAboutUs"><a href="<?php echo linkto(array('page'=>"about.php"),$_smarty_tpl);?>
"><?php echo $_smarty_tpl->tpl_vars['lang']->value['aboutUs'];?>
</a></li><?php }?>
				<?php if ($_smarty_tpl->tpl_vars['config']->value['settings']['contact']){?><li id="navContactUs"><a href="<?php echo linkto(array('page'=>"contact.php"),$_smarty_tpl);?>
"><?php echo $_smarty_tpl->tpl_vars['lang']->value['contactUs'];?>
</a></li><?php }?>
				<?php if ($_smarty_tpl->tpl_vars['config']->value['settings']['forum_link']){?><li id="navForum"><a href="<?php echo $_smarty_tpl->tpl_vars['config']->value['settings']['forum_link'];?>
"><?php echo $_smarty_tpl->tpl_vars['lang']->value['forum'];?>
</a></li><?php }?>				
			</ul>
			
		</div>
	</div>
	<div id="secondRowContainer">
		<div class="center">
			<a href="<?php echo linkto(array('page'=>"index.php"),$_smarty_tpl);?>
"><img src="<?php echo $_smarty_tpl->tpl_vars['mainLogo']->value;?>
" id="mainLogo"></a>
			<ul id="secondRowNav">
				<?php if (count($_smarty_tpl->tpl_vars['displayLanguages']->value)>1){?>
				<li>
					<div id="languageSelector" class="clSelector">
						<div class="currentDDSelection"><?php echo $_smarty_tpl->tpl_vars['displayLanguages']->value[$_smarty_tpl->tpl_vars['selectedLanguage']->value];?>
 <img src="<?php echo $_smarty_tpl->tpl_vars['imgPath']->value;?>
/dropdown.arrow.png"></div>
						<ul class="clDropDown">
							<?php  $_smarty_tpl->tpl_vars['language'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['language']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['displayLanguages']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['language']->key => $_smarty_tpl->tpl_vars['language']->value){
$_smarty_tpl->tpl_vars['language']->_loop = true;
?>
								<li <?php if ($_smarty_tpl->tpl_vars['language']->key==$_smarty_tpl->tpl_vars['selectedLanguage']->value){?>id="selectedLanguage"<?php }?>><a href="<?php echo linkto(array('page'=>"actions.php?action=changeLanguage&setLanguage=".($_smarty_tpl->tpl_vars['language']->key)),$_smarty_tpl);?>
"><?php echo $_smarty_tpl->tpl_vars['language']->value;?>
</a></li>
							<?php } ?>
						</ul>
						
					</div>
					<div style="float: left"><?php echo $_smarty_tpl->tpl_vars['lang']->value['language'];?>
: </div>			
				</li>
				<?php }?>
				<?php if (count($_smarty_tpl->tpl_vars['displayCurrencies']->value)>1){?>
				<li>
					<div id="currencySelector" class="clSelector">
						<div class="currentDDSelection"><?php echo $_smarty_tpl->tpl_vars['activeCurrencies']->value[$_smarty_tpl->tpl_vars['selectedCurrency']->value]['name'];?>
 (<?php echo $_smarty_tpl->tpl_vars['activeCurrencies']->value[$_smarty_tpl->tpl_vars['selectedCurrency']->value]['code'];?>
) <img src="<?php echo $_smarty_tpl->tpl_vars['imgPath']->value;?>
/dropdown.arrow.png"></div>
						<ul class="clDropDown">
							<?php  $_smarty_tpl->tpl_vars['currency'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['currency']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['displayCurrencies']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['currency']->key => $_smarty_tpl->tpl_vars['currency']->value){
$_smarty_tpl->tpl_vars['currency']->_loop = true;
?>
								<li <?php if ($_smarty_tpl->tpl_vars['currency']->key==$_smarty_tpl->tpl_vars['selectedCurrency']->value){?>id="selectedCurrency"<?php }?>><a href="<?php echo linkto(array('page'=>"actions.php?action=changeCurrency&setCurrency=".($_smarty_tpl->tpl_vars['currency']->key)),$_smarty_tpl);?>
"><?php echo $_smarty_tpl->tpl_vars['currency']->value;?>
 (<?php echo $_smarty_tpl->tpl_vars['activeCurrencies']->value[$_smarty_tpl->tpl_vars['currency']->key]['code'];?>
)</a></li><!--onclick="changeCurrency('<?php echo $_smarty_tpl->tpl_vars['currency']->key;?>
');"-->
							<?php } ?>
						</ul>
					</div>				
					<div style="float: left"><?php echo $_smarty_tpl->tpl_vars['lang']->value['currency'];?>
: </div>
				</li>
				<?php }?>
				
				
				<li>
					<ul id="tnLoginArea">
						<?php if ($_smarty_tpl->tpl_vars['config']->value['settings']['display_login']){?>
							<?php if ($_smarty_tpl->tpl_vars['loggedIn']->value){?>
								<li><a href="<?php echo linkto(array('page'=>"members.php"),$_smarty_tpl);?>
"><?php echo $_smarty_tpl->tpl_vars['member']->value['f_name'];?>
 <?php echo $_smarty_tpl->tpl_vars['member']->value['l_name'];?>
</a></li>
								<li><a href="<?php echo linkto(array('page'=>"login.php?cmd=logout"),$_smarty_tpl);?>
"><?php echo $_smarty_tpl->tpl_vars['lang']->value['logout'];?>
</a></li>
							<?php }else{ ?>
								<li><a href=""><a href="<?php echo linkto(array('page'=>"login.php?jumpTo=members"),$_smarty_tpl);?>
"><?php echo $_smarty_tpl->tpl_vars['lang']->value['login'];?>
</a></a></li>
								<li><a href="<?php echo linkto(array('page'=>"create.account.php?jumpTo=members"),$_smarty_tpl);?>
"><?php echo $_smarty_tpl->tpl_vars['lang']->value['createAccount'];?>
</a></li>
							<?php }?>
						<?php }?>
						
						<?php if ($_smarty_tpl->tpl_vars['lightboxSystem']->value){?>
							<li><a href="<?php echo linkto(array('page'=>"lightboxes.php"),$_smarty_tpl);?>
"><?php echo $_smarty_tpl->tpl_vars['lang']->value['lightboxes'];?>
</a></li>
						<?php }?>						
					</ul>
				</li>
			</ul>
		</div>
	</div>
	<?php if ($_smarty_tpl->tpl_vars['message']->value){?>
		<?php  $_smarty_tpl->tpl_vars['messageLang'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['messageLang']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['message']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['messageLang']->key => $_smarty_tpl->tpl_vars['messageLang']->value){
$_smarty_tpl->tpl_vars['messageLang']->_loop = true;
?>
			<div class="messageBar"><img src="<?php echo $_smarty_tpl->tpl_vars['imgPath']->value;?>
/notice.icon.png"><?php echo $_smarty_tpl->tpl_vars['lang']->value[$_smarty_tpl->tpl_vars['messageLang']->value];?>
 <a href="#" class="buttonLink">X</a></div>
		<?php } ?>
	<?php }?><?php }} ?>