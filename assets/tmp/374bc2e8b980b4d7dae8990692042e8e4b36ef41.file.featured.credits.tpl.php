<?php /* Smarty version Smarty-3.1.8, created on 2017-01-11 19:02:59
         compiled from "/home/dynamikd/public_html/clients/piksel/assets/themes/modern/featured.credits.tpl" */ ?>
<?php /*%%SmartyHeaderCode:57317130758768163b28d25-18830237%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '374bc2e8b980b4d7dae8990692042e8e4b36ef41' => 
    array (
      0 => '/home/dynamikd/public_html/clients/piksel/assets/themes/modern/featured.credits.tpl',
      1 => 1404911418,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '57317130758768163b28d25-18830237',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'baseURL' => 0,
    'lang' => 0,
    'featuredCreditsRows' => 0,
    'featuredCredits' => 0,
    'credit' => 0,
    'cartStatus' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.8',
  'unifunc' => 'content_58768163bc9392_84714217',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_58768163bc9392_84714217')) {function content_58768163bc9392_84714217($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_truncate')) include '/home/dynamikd/public_html/clients/piksel/assets/smarty/plugins/modifier.truncate.php';
?><!DOCTYPE HTML>
<html>
<head>
	<?php echo $_smarty_tpl->getSubTemplate ('head.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

	<script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['baseURL']->value;?>
/assets/javascript/featured.page.js"></script>
</head>
<body>
	<?php echo $_smarty_tpl->getSubTemplate ('overlays.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

	<div id="container">
		<?php echo $_smarty_tpl->getSubTemplate ('header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

		<?php echo $_smarty_tpl->getSubTemplate ('header2.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>
		
		
		<div class="container">
			<div class="row">
				<?php echo $_smarty_tpl->getSubTemplate ('subnav.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>
		
				<div class="col-md-9">
					
					<h1><?php echo $_smarty_tpl->tpl_vars['lang']->value['featuredCredits'];?>
</h1>
					<hr>
					<?php if ($_smarty_tpl->tpl_vars['featuredCreditsRows']->value){?>
						<?php  $_smarty_tpl->tpl_vars['credit'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['credit']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['featuredCredits']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['credit']->key => $_smarty_tpl->tpl_vars['credit']->value){
$_smarty_tpl->tpl_vars['credit']->_loop = true;
?>
							<div class="featuredPageItem workboxLinkAttach">
								<h2><a href="<?php echo $_smarty_tpl->tpl_vars['credit']->value['linkto'];?>
" class="workboxLink"><?php echo $_smarty_tpl->tpl_vars['credit']->value['name'];?>
</a></h2>
								<p class="description"><?php if ($_smarty_tpl->tpl_vars['credit']->value['photo']){?><img src="<?php echo productShot(array('itemID'=>$_smarty_tpl->tpl_vars['credit']->value['credit_id'],'itemType'=>'credit','photoID'=>$_smarty_tpl->tpl_vars['credit']->value['photo']['id'],'size'=>125),$_smarty_tpl);?>
"><br><br><?php }?><?php echo smarty_modifier_truncate($_smarty_tpl->tpl_vars['credit']->value['description'],360);?>
</p>
								<?php if ($_smarty_tpl->tpl_vars['cartStatus']->value){?><p class="moreInfo"><?php if ($_smarty_tpl->tpl_vars['credit']->value['price']){?><span class="price"><?php echo $_smarty_tpl->tpl_vars['credit']->value['price']['display'];?>
</span><?php if ($_smarty_tpl->tpl_vars['credit']->value['price']['taxInc']){?> <span class="taxIncMessage">(<?php echo $_smarty_tpl->tpl_vars['lang']->value['taxIncMessage'];?>
)</span><?php }?><?php }?></p><?php }?>
							</div>
						<?php } ?>
					<?php }else{ ?>
						<p class="notice"><?php echo $_smarty_tpl->tpl_vars['lang']->value['noFeatured'];?>
</p>
					<?php }?>
					
				</div>
			</div>
		</div>
		<?php echo $_smarty_tpl->getSubTemplate ('footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

    </div>
</body>
</html><?php }} ?>