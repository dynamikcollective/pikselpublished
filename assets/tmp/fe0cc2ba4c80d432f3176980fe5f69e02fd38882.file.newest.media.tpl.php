<?php /* Smarty version Smarty-3.1.8, created on 2016-11-28 18:57:12
         compiled from "/home/dynamikd/public_html/clients/piksel/assets/themes/modern/newest.media.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1384399417583c7e08e484b6-99586454%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'fe0cc2ba4c80d432f3176980fe5f69e02fd38882' => 
    array (
      0 => '/home/dynamikd/public_html/clients/piksel/assets/themes/modern/newest.media.tpl',
      1 => 1403182766,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1384399417583c7e08e484b6-99586454',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'config' => 0,
    'lang' => 0,
    'mediaRows' => 0,
    'mediaPaging' => 0,
    'mediaArray' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.8',
  'unifunc' => 'content_583c7e08eb9be9_99593902',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_583c7e08eb9be9_99593902')) {function content_583c7e08eb9be9_99593902($_smarty_tpl) {?><!DOCTYPE HTML>
<html>
<head>
	<?php echo $_smarty_tpl->getSubTemplate ('head.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

</head>
<body>
	<?php echo $_smarty_tpl->getSubTemplate ('overlays.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

	<div id="container">
		<?php echo $_smarty_tpl->getSubTemplate ('header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

		<?php echo $_smarty_tpl->getSubTemplate ('header2.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>
		
		<div class="container">					
			<h1>Newest Media<?php if ($_smarty_tpl->tpl_vars['config']->value['settings']['rss_newest']){?> <a href="<?php echo linkto(array('page'=>'rss.php?mode=newestMedia'),$_smarty_tpl);?>
" class="btn btn-xxs btn-warning"><?php echo $_smarty_tpl->tpl_vars['lang']->value['rss'];?>
</a><?php }?></h1>
			<hr>
			<?php if ($_smarty_tpl->tpl_vars['mediaRows']->value){?>
				<?php echo $_smarty_tpl->getSubTemplate ("paging.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('paging'=>$_smarty_tpl->tpl_vars['mediaPaging']->value), 0);?>

				<div id="mediaListContainer">
					<?php  $_smarty_tpl->tpl_vars['media'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['media']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['mediaArray']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['media']->key => $_smarty_tpl->tpl_vars['media']->value){
$_smarty_tpl->tpl_vars['media']->_loop = true;
?>
						<?php echo $_smarty_tpl->getSubTemplate ('media.container.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

					<?php } ?>
				</div>
				<?php echo $_smarty_tpl->getSubTemplate ("paging.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('paging'=>$_smarty_tpl->tpl_vars['mediaPaging']->value), 0);?>

			<?php }else{ ?>
				<p class="notice"><?php echo $_smarty_tpl->tpl_vars['lang']->value['noMedia'];?>
</p>
			<?php }?>
		</div>
		<?php echo $_smarty_tpl->getSubTemplate ('footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

    </div>
</body>
</html>
<?php }} ?>