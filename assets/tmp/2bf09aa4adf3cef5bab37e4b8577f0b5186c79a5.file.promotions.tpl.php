<?php /* Smarty version Smarty-3.1.8, created on 2016-11-28 16:53:05
         compiled from "/home/dynamikd/public_html/clients/piksel/assets/themes/modern/promotions.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1159198651583c60f186d1f2-90160731%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '2bf09aa4adf3cef5bab37e4b8577f0b5186c79a5' => 
    array (
      0 => '/home/dynamikd/public_html/clients/piksel/assets/themes/modern/promotions.tpl',
      1 => 1404920470,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1159198651583c60f186d1f2-90160731',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'baseURL' => 0,
    'lang' => 0,
    'promotions' => 0,
    'promo' => 0,
    'loggedIn' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.8',
  'unifunc' => 'content_583c60f18d0cc0_66661433',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_583c60f18d0cc0_66661433')) {function content_583c60f18d0cc0_66661433($_smarty_tpl) {?><!DOCTYPE HTML>
<html>
<head>
	<?php echo $_smarty_tpl->getSubTemplate ('head.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

	<script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['baseURL']->value;?>
/assets/javascript/featured.page.js"></script>
</head>
<body>
	<?php echo $_smarty_tpl->getSubTemplate ('overlays.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

	<div id="container">
		<?php echo $_smarty_tpl->getSubTemplate ('header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

		<?php echo $_smarty_tpl->getSubTemplate ('header2.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>
		
		
		<div class="container">
			<div class="row">
				<?php echo $_smarty_tpl->getSubTemplate ('subnav.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>
		
				<div class="col-md-9">
					
					<h1><?php echo $_smarty_tpl->tpl_vars['lang']->value['promotions'];?>
</h1>
					<hr>
					<?php if ($_smarty_tpl->tpl_vars['promotions']->value){?>
						<?php  $_smarty_tpl->tpl_vars['promo'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['promo']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['promotions']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['promo']->key => $_smarty_tpl->tpl_vars['promo']->value){
$_smarty_tpl->tpl_vars['promo']->_loop = true;
?>
							<div class="featuredPageItem workboxLinkAttach">
								<h2><a href="<?php echo $_smarty_tpl->tpl_vars['promo']->value['linkto'];?>
" class="workboxLink"><?php echo $_smarty_tpl->tpl_vars['promo']->value['name'];?>
</a></h2>
								<p class="description">
									<?php if ($_smarty_tpl->tpl_vars['promo']->value['photo']){?><img src="<?php echo productShot(array('itemID'=>$_smarty_tpl->tpl_vars['promo']->value['promo_id'],'itemType'=>'promo','photoID'=>$_smarty_tpl->tpl_vars['promo']->value['photo']['id'],'size'=>125),$_smarty_tpl);?>
"><br><br><?php }?>
									<?php echo $_smarty_tpl->tpl_vars['promo']->value['description'];?>

									<?php if ($_smarty_tpl->tpl_vars['promo']->value['autoapply']){?><br><br><span class="promoUse">*<?php echo $_smarty_tpl->tpl_vars['lang']->value['autoApply'];?>
</span><?php }elseif($_smarty_tpl->tpl_vars['promo']->value['promo_code']){?><br><br><span class="promoUse">*<?php echo $_smarty_tpl->tpl_vars['lang']->value['useCoupon'];?>
<strong>: <?php echo $_smarty_tpl->tpl_vars['promo']->value['promo_code'];?>
</strong></span><?php }?>
									<?php if ($_smarty_tpl->tpl_vars['promo']->value['oneuse']&&!$_smarty_tpl->tpl_vars['loggedIn']->value){?><br><br><span class="promoUse">*<?php echo $_smarty_tpl->tpl_vars['lang']->value['couponLoginWarn'];?>
</span><?php }?>
								</p>
							</div>
						<?php } ?>							
						
					<?php }else{ ?>
						<p class="notice"><?php echo $_smarty_tpl->tpl_vars['lang']->value['noPromotions'];?>
</p>
					<?php }?>
					
				</div>
			</div>
		</div>
		<?php echo $_smarty_tpl->getSubTemplate ('footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

    </div>
</body>
</html><?php }} ?>