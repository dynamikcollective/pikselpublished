<?php /* Smarty version Smarty-3.1.8, created on 2016-11-28 16:52:49
         compiled from "/home/dynamikd/public_html/clients/piksel/assets/themes/modern/index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:940399612583c60e15a18c7-50958041%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '729978131f566c5546a9e64c15a3fdb74c2112cf' => 
    array (
      0 => '/home/dynamikd/public_html/clients/piksel/assets/themes/modern/index.tpl',
      1 => 1430991098,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '940399612583c60e15a18c7-50958041',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'config' => 0,
    'baseURL' => 0,
    'featuredMedia' => 0,
    'media' => 0,
    'lang' => 0,
    'subGalleriesData' => 0,
    'subGallery' => 0,
    'galleriesData' => 0,
    'imgPath' => 0,
    'newestMediaRows' => 0,
    'newestMedia' => 0,
    'popularMediaRows' => 0,
    'popularMedia' => 0,
    'randomMediaRows' => 0,
    'randomMedia' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.8',
  'unifunc' => 'content_583c60e16c2db9_16696756',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_583c60e16c2db9_16696756')) {function content_583c60e16c2db9_16696756($_smarty_tpl) {?><!-- 
Based on this tutorial
http://www.sitepoint.com/understanding-twitter-bootstrap-3/
-->
<!DOCTYPE HTML>
<html>
	<head>
		<?php echo $_smarty_tpl->getSubTemplate ('head.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

		
		<script>
			$(function()
			{
				$('#myCarousel').carousel({
					interval: <?php echo $_smarty_tpl->tpl_vars['config']->value['settings']['hpf_inverval'];?>

				});
			});
		</script>
		<script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['baseURL']->value;?>
/assets/javascript/gallery.js"></script>
	</head>
	<body>
		<?php echo $_smarty_tpl->getSubTemplate ('overlays.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>
		
		<?php echo $_smarty_tpl->getSubTemplate ('header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

		
		<?php if ($_smarty_tpl->tpl_vars['config']->value['settings']['hpfeaturedmedia']&&$_smarty_tpl->tpl_vars['featuredMedia']->value){?>
		<!-- Carousel
		================================================== -->
		<div id="myCarousel" class="carousel slide">
			<!-- Indicators -->
			<ol class="carousel-indicators">
				<?php  $_smarty_tpl->tpl_vars['media'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['media']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['featuredMedia']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['media']->iteration=0;
 $_smarty_tpl->tpl_vars['media']->index=-1;
foreach ($_from as $_smarty_tpl->tpl_vars['media']->key => $_smarty_tpl->tpl_vars['media']->value){
$_smarty_tpl->tpl_vars['media']->_loop = true;
 $_smarty_tpl->tpl_vars['media']->iteration++;
 $_smarty_tpl->tpl_vars['media']->index++;
 $_smarty_tpl->tpl_vars['media']->first = $_smarty_tpl->tpl_vars['media']->index === 0;
?>
					<li data-target="#myCarousel" data-slide-to="<?php echo $_smarty_tpl->tpl_vars['media']->iteration-1;?>
" class="<?php if ($_smarty_tpl->tpl_vars['media']->first){?>active<?php }?>"></li>
				<?php } ?>
			</ol>
			<div class="carousel-inner">
				<?php  $_smarty_tpl->tpl_vars['media'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['media']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['featuredMedia']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['media']->iteration=0;
 $_smarty_tpl->tpl_vars['media']->index=-1;
foreach ($_from as $_smarty_tpl->tpl_vars['media']->key => $_smarty_tpl->tpl_vars['media']->value){
$_smarty_tpl->tpl_vars['media']->_loop = true;
 $_smarty_tpl->tpl_vars['media']->iteration++;
 $_smarty_tpl->tpl_vars['media']->index++;
 $_smarty_tpl->tpl_vars['media']->first = $_smarty_tpl->tpl_vars['media']->index === 0;
?>
				<div class="item <?php if ($_smarty_tpl->tpl_vars['media']->first){?>active<?php }?>">
					<img src="image.php?mediaID=<?php echo $_smarty_tpl->tpl_vars['media']->value['encryptedID'];?>
=&type=featured&folderID=<?php echo $_smarty_tpl->tpl_vars['media']->value['encryptedFID'];?>
&size=<?php echo $_smarty_tpl->tpl_vars['config']->value['settings']['hpf_width'];?>
&crop=<?php echo $_smarty_tpl->tpl_vars['config']->value['settings']['hpf_crop_to'];?>
" encMediaID="<?php echo $_smarty_tpl->tpl_vars['media']->value['encryptedID'];?>
" href="<?php echo $_smarty_tpl->tpl_vars['media']->value['linkto'];?>
" alt="<?php echo $_smarty_tpl->tpl_vars['media']->value['title']['value'];?>
">
					<div class="container">
						<div class="carousel-caption">
							<h1><?php echo $_smarty_tpl->tpl_vars['media']->value['title']['value'];?>
</h1>
							<p class="hidden-xs"><?php echo $_smarty_tpl->tpl_vars['media']->value['description']['value'];?>
</p>
							<p><a class="btn btn-large btn-primary" href="<?php echo $_smarty_tpl->tpl_vars['media']->value['linkto'];?>
"><?php echo $_smarty_tpl->tpl_vars['lang']->value['details'];?>
</a></p>
						</div>
					</div>
				</div>
				<?php } ?>
			</div>
			<a class="left carousel-control" href="#myCarousel" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
			<a class="right carousel-control" href="#myCarousel" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
		</div><!-- /.carousel -->
		<?php }?>
	

		<?php echo $_smarty_tpl->getSubTemplate ('header2.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

	
		<div class="container">
			<div class="row">
				<?php echo $_smarty_tpl->getSubTemplate ('subnav.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>
		
				<div class="col-md-9">
					<div id="homepageJumbotron">
						<div>
							<?php echo content(array('id'=>'homeWelcome'),$_smarty_tpl);?>

						</div>
					</div>				
					
					
					<?php if ($_smarty_tpl->tpl_vars['subGalleriesData']->value){?>
						<hr>
						<h3><?php echo $_smarty_tpl->tpl_vars['lang']->value['featuredGalleries'];?>
</h3>						
						<div id="galleryListContainer">
							<?php  $_smarty_tpl->tpl_vars['subGallery'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['subGallery']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['subGalleriesData']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['subGallery']->key => $_smarty_tpl->tpl_vars['subGallery']->value){
$_smarty_tpl->tpl_vars['subGallery']->_loop = true;
?>
								<div class="galleryContainer" style="width: <?php echo $_smarty_tpl->tpl_vars['config']->value['settings']['gallery_thumb_size'];?>
px">
									<div class="galleryDetailsContainer" style="width: <?php echo $_smarty_tpl->tpl_vars['config']->value['settings']['gallery_thumb_size'];?>
px; <?php if ($_smarty_tpl->tpl_vars['galleriesData']->value[$_smarty_tpl->tpl_vars['subGallery']->value]['galleryIcon']){?>vertical-align: top<?php }?>">
										<?php if ($_smarty_tpl->tpl_vars['galleriesData']->value[$_smarty_tpl->tpl_vars['subGallery']->value]['galleryIcon']){?><p class="galleryIconContainer" style="min-width: <?php echo $_smarty_tpl->tpl_vars['galleriesData']->value[$_smarty_tpl->tpl_vars['subGallery']->value]['galleryIcon']['width'];?>
px; min-height: <?php echo $_smarty_tpl->tpl_vars['galleriesData']->value[$_smarty_tpl->tpl_vars['subGallery']->value]['galleryIcon']['height']+5;?>
px;"><a href="<?php echo $_smarty_tpl->tpl_vars['galleriesData']->value[$_smarty_tpl->tpl_vars['subGallery']->value]['linkto'];?>
"><img src="<?php echo $_smarty_tpl->tpl_vars['baseURL']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['galleriesData']->value[$_smarty_tpl->tpl_vars['subGallery']->value]['galleryIcon']['imgSrc'];?>
"></a></p><?php }?>
										<p class="galleryDetails"><?php if ($_smarty_tpl->tpl_vars['galleriesData']->value[$_smarty_tpl->tpl_vars['subGallery']->value]['password']){?><img src="<?php echo $_smarty_tpl->tpl_vars['imgPath']->value;?>
/lock.png" class="lock"><?php }?><a href="<?php echo $_smarty_tpl->tpl_vars['galleriesData']->value[$_smarty_tpl->tpl_vars['subGallery']->value]['linkto'];?>
"><?php echo $_smarty_tpl->tpl_vars['galleriesData']->value[$_smarty_tpl->tpl_vars['subGallery']->value]['name'];?>
</a><?php if ($_smarty_tpl->tpl_vars['config']->value['settings']['gallery_count']){?><?php if ($_smarty_tpl->tpl_vars['galleriesData']->value[$_smarty_tpl->tpl_vars['subGallery']->value]['gallery_count']>0||$_smarty_tpl->tpl_vars['config']->value['ShowZeroCounts']){?>&nbsp;<span class="galleryMediaCount">(<?php echo $_smarty_tpl->tpl_vars['galleriesData']->value[$_smarty_tpl->tpl_vars['subGallery']->value]['gallery_count'];?>
)</span><?php }?><?php }?></p>
									</div>
									<!--gi: <?php echo $_smarty_tpl->tpl_vars['galleriesData']->value[$_smarty_tpl->tpl_vars['subGallery']->value]['galleryIcon']['imgSrc'];?>
-->
								</div>
							<?php } ?>
						</div>
					<?php }?>
					
					
					<?php if ($_smarty_tpl->tpl_vars['newestMediaRows']->value){?>				
					<div class="clearfix">
						<hr>
						<h3><?php if ($_smarty_tpl->tpl_vars['config']->value['settings']['newestpage']){?><a href="<?php echo linkto(array('page'=>'gallery.php?mode=newest-media&page=1'),$_smarty_tpl);?>
"><?php echo $_smarty_tpl->tpl_vars['lang']->value['newestMedia'];?>
</a><?php }else{ ?><?php echo $_smarty_tpl->tpl_vars['lang']->value['newestMedia'];?>
<?php }?><?php if ($_smarty_tpl->tpl_vars['config']->value['settings']['rss_newest']){?> <a href="<?php echo linkto(array('page'=>'rss.php?mode=newestMedia'),$_smarty_tpl);?>
" class="btn btn-xxs btn-warning"><?php echo $_smarty_tpl->tpl_vars['lang']->value['rss'];?>
</a><?php }?></h3>
						<div>
							<?php  $_smarty_tpl->tpl_vars['media'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['media']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['newestMedia']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['media']->iteration=0;
 $_smarty_tpl->tpl_vars['media']->index=-1;
foreach ($_from as $_smarty_tpl->tpl_vars['media']->key => $_smarty_tpl->tpl_vars['media']->value){
$_smarty_tpl->tpl_vars['media']->_loop = true;
 $_smarty_tpl->tpl_vars['media']->iteration++;
 $_smarty_tpl->tpl_vars['media']->index++;
 $_smarty_tpl->tpl_vars['media']->first = $_smarty_tpl->tpl_vars['media']->index === 0;
?>
								<?php echo $_smarty_tpl->getSubTemplate ('media.container.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

							<?php } ?>
						</div>
					</div>
					<?php }?>
					
					
					
					<?php if ($_smarty_tpl->tpl_vars['popularMediaRows']->value){?>
					<div class="clearfix">
						<hr>
						<h3><?php if ($_smarty_tpl->tpl_vars['config']->value['settings']['popularpage']){?><a href="<?php echo linkto(array('page'=>'gallery.php?mode=popular-media&page=1'),$_smarty_tpl);?>
"><?php echo $_smarty_tpl->tpl_vars['lang']->value['popularMedia'];?>
</a><?php }else{ ?><?php echo $_smarty_tpl->tpl_vars['lang']->value['popularMedia'];?>
<?php }?><?php if ($_smarty_tpl->tpl_vars['config']->value['settings']['rss_popular']){?> <a href="<?php echo linkto(array('page'=>'rss.php?mode=popularMedia'),$_smarty_tpl);?>
" class="btn btn-xxs btn-warning"><?php echo $_smarty_tpl->tpl_vars['lang']->value['rss'];?>
</a><?php }?></h3>
						<div>
							<?php  $_smarty_tpl->tpl_vars['media'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['media']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['popularMedia']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['media']->iteration=0;
 $_smarty_tpl->tpl_vars['media']->index=-1;
foreach ($_from as $_smarty_tpl->tpl_vars['media']->key => $_smarty_tpl->tpl_vars['media']->value){
$_smarty_tpl->tpl_vars['media']->_loop = true;
 $_smarty_tpl->tpl_vars['media']->iteration++;
 $_smarty_tpl->tpl_vars['media']->index++;
 $_smarty_tpl->tpl_vars['media']->first = $_smarty_tpl->tpl_vars['media']->index === 0;
?>
								<?php echo $_smarty_tpl->getSubTemplate ('media.container.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

							<?php } ?>
						</div>
					</div>
					<?php }?>
					
					
					<?php if ($_smarty_tpl->tpl_vars['randomMediaRows']->value){?>
					<div class="clearfix">
						<hr>
						<h3><?php echo $_smarty_tpl->tpl_vars['lang']->value['randomMedia'];?>
</h3>						
						<div>
							<?php  $_smarty_tpl->tpl_vars['media'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['media']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['randomMedia']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['media']->iteration=0;
 $_smarty_tpl->tpl_vars['media']->index=-1;
foreach ($_from as $_smarty_tpl->tpl_vars['media']->key => $_smarty_tpl->tpl_vars['media']->value){
$_smarty_tpl->tpl_vars['media']->_loop = true;
 $_smarty_tpl->tpl_vars['media']->iteration++;
 $_smarty_tpl->tpl_vars['media']->index++;
 $_smarty_tpl->tpl_vars['media']->first = $_smarty_tpl->tpl_vars['media']->index === 0;
?>
								<?php echo $_smarty_tpl->getSubTemplate ('media.container.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

							<?php } ?>
						</div>
					</div>
					<?php }?>
					
				</div>
			</div>
		</div>
		<?php echo $_smarty_tpl->getSubTemplate ('footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

	</body>
</html>
<?php }} ?>