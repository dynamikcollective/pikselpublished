<?php /* Smarty version Smarty-3.1.8, created on 2017-01-19 12:20:27
         compiled from "/home/dynamikd/public_html/clients/piksel/assets/themes/default/subnav.tpl" */ ?>
<?php /*%%SmartyHeaderCode:16942224485880af0b0e5bd6-81251153%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '409a3c56e59c61e6fa22590d6a46781cc7acdb75' => 
    array (
      0 => '/home/dynamikd/public_html/clients/piksel/assets/themes/default/subnav.tpl',
      1 => 1386525096,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '16942224485880af0b0e5bd6-81251153',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'galleriesData' => 0,
    'lang' => 0,
    'imgPath' => 0,
    'contentPages' => 0,
    'content' => 0,
    'contentBlocks' => 0,
    'pageID' => 0,
    'featuredContributors' => 0,
    'contributor' => 0,
    'config' => 0,
    'membersOnline' => 0,
    'member' => 0,
    'siteStats' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.8',
  'unifunc' => 'content_5880af0b1baa06_49658650',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5880af0b1baa06_49658650')) {function content_5880af0b1baa06_49658650($_smarty_tpl) {?><div id="galleryList">
		
	<div id="treeMenu"></div>
</div>
<!--
{
    "data" : "node_title", // omit `attr` if not needed; the `attr` object gets passed to the jQuery `attr` function
    "attr" : { "id" : "node_identificator", "some-other-attribute" : "attribute_value" },
    "state" : "closed", // or "open", defaults to "closed"
    "children" : [ /* an array of child nodes objects */ ]
}
-->


<div class="subNavFeaturedBox">
	
	<?php if ($_smarty_tpl->tpl_vars['galleriesData']->value){?>
		<div id="galleryPicker">
			<a href="<?php echo linkto(array('page'=>"gallery.php?mode=gallery&id=0"),$_smarty_tpl);?>
"><?php echo $_smarty_tpl->tpl_vars['lang']->value['galleries'];?>
</a> <img src="<?php echo $_smarty_tpl->tpl_vars['imgPath']->value;?>
/gallery.arrow.png" id="galleryListToggle">
		</div>
	<?php }?>
	
	<?php if (count($_smarty_tpl->tpl_vars['contentPages']->value)>0){?>
		<ul id="customPages">
			<?php  $_smarty_tpl->tpl_vars['content'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['content']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['contentPages']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['content']->key => $_smarty_tpl->tpl_vars['content']->value){
$_smarty_tpl->tpl_vars['content']->_loop = true;
?>
				<li>
				<?php if ($_smarty_tpl->tpl_vars['content']->value['linked']){?>
				<a href="<?php echo $_smarty_tpl->tpl_vars['content']->value['linked'];?>
" target="_blank"><?php echo $_smarty_tpl->tpl_vars['content']->value['name'];?>
</a>
				<?php }else{ ?>
				<a href="<?php echo linkto(array('page'=>"content.php?id=".($_smarty_tpl->tpl_vars['content']->value['content_id'])),$_smarty_tpl);?>
"><?php echo $_smarty_tpl->tpl_vars['content']->value['name'];?>
</a>
				<?php }?>
				</li>
			<?php } ?>
		</ul>
	<?php }?>
	
</div>

<?php  $_smarty_tpl->tpl_vars['content'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['content']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['contentBlocks']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['content']->key => $_smarty_tpl->tpl_vars['content']->value){
$_smarty_tpl->tpl_vars['content']->_loop = true;
?>
	<?php if ($_smarty_tpl->tpl_vars['content']->value['specType']=='sncb'){?>
		<div class="subNavFeaturedBox cbSubnav">
			<h1><?php echo $_smarty_tpl->tpl_vars['content']->value['name'];?>
</h1>
			<div><?php echo $_smarty_tpl->tpl_vars['content']->value['content'];?>
</div>
		</div>
	<?php }?>
<?php } ?>

<?php if ($_smarty_tpl->tpl_vars['pageID']->value!='homepage'){?>
	
	<?php if ($_smarty_tpl->tpl_vars['featuredContributors']->value){?>
		<div class="subNavFeaturedBox" id="subNavContributors">
			<img src="<?php echo $_smarty_tpl->tpl_vars['imgPath']->value;?>
/feature.box.c.icon.png" class="fbHero opac20">
			<h1><?php echo $_smarty_tpl->tpl_vars['lang']->value['showcasedContributors'];?>
</h1>
			<div class="divTable" style="margin-bottom: 20px;">
			<?php  $_smarty_tpl->tpl_vars['contributor'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['contributor']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['featuredContributors']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['contributor']->key => $_smarty_tpl->tpl_vars['contributor']->value){
$_smarty_tpl->tpl_vars['contributor']->_loop = true;
?>
				<div class="divTableRow">
					<div class="divTableCell"><img src="<?php echo memberAvatar(array('memID'=>$_smarty_tpl->tpl_vars['contributor']->value['mem_id'],'size'=>30,'crop'=>30),$_smarty_tpl);?>
"></div>
					<div class="divTableCell"><a href="<?php echo linkto(array('page'=>"contributors.php?id=".($_smarty_tpl->tpl_vars['contributor']->value['useID'])."&seoName=".($_smarty_tpl->tpl_vars['contributor']->value['seoName'])),$_smarty_tpl);?>
"><?php echo $_smarty_tpl->tpl_vars['contributor']->value['display_name'];?>
</a></div>
				</div>
			<?php } ?>
			</div>
		</div>						
	<?php }?>
	
	
	<?php if ($_smarty_tpl->tpl_vars['config']->value['settings']['members_online']){?>
		<div class="subNavFeaturedBox" id="subNavOnlineMembers">
			<img src="<?php echo $_smarty_tpl->tpl_vars['imgPath']->value;?>
/feature.box.b.icon.png" class="fbHero opac20">
			<h1><?php echo $_smarty_tpl->tpl_vars['lang']->value['membersOnline'];?>
</h1>
			<ul>
				<?php if ($_smarty_tpl->tpl_vars['membersOnline']->value){?>
					<?php  $_smarty_tpl->tpl_vars['member'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['member']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['membersOnline']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['member']->key => $_smarty_tpl->tpl_vars['member']->value){
$_smarty_tpl->tpl_vars['member']->_loop = true;
?>
						<li><?php echo $_smarty_tpl->tpl_vars['member']->value['display_name'];?>
 <span class="time">(<?php echo $_smarty_tpl->tpl_vars['member']->value['lastSeen'];?>
 <?php echo $_smarty_tpl->tpl_vars['lang']->value['minutesAgo'];?>
)</span></li>
					<?php } ?>
				<?php }else{ ?>
					<li><?php echo $_smarty_tpl->tpl_vars['lang']->value['none'];?>
</li>
				<?php }?>
			</ul>
		</div>
	<?php }?>
	
	
	<?php if ($_smarty_tpl->tpl_vars['siteStats']->value){?>
		<div class="subNavStatsBox" id="subNavStats">
			<img src="<?php echo $_smarty_tpl->tpl_vars['imgPath']->value;?>
/feature.box.a.icon.png" class="fbHero opac20">
			<h1><?php echo $_smarty_tpl->tpl_vars['lang']->value['siteStats'];?>
</h1>
			<div class="divTable">
				<div class="divTableRow">
					<div class="divTableCell"><?php echo $_smarty_tpl->tpl_vars['lang']->value['members'];?>
:</div>
					<div class="divTableCell"><strong><?php echo $_smarty_tpl->tpl_vars['siteStats']->value['members'];?>
</strong></div>
				</div>
				<div class="divTableRow">
					<div class="divTableCell"><?php echo $_smarty_tpl->tpl_vars['lang']->value['media'];?>
:</div>
					<div class="divTableCell"><strong><?php echo $_smarty_tpl->tpl_vars['siteStats']->value['media'];?>
</strong></div>
				</div>
				
				<div class="divTableRow">
					<div class="divTableCell"><?php echo $_smarty_tpl->tpl_vars['lang']->value['visits'];?>
:</div>
					<div class="divTableCell"><strong><?php echo $_smarty_tpl->tpl_vars['siteStats']->value['visits'];?>
</strong></div>
				</div>
			</div>
		</div>
	<?php }?>
	
<?php }?><?php }} ?>