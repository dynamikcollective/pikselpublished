<?php /* Smarty version Smarty-3.1.8, created on 2017-01-12 18:12:08
         compiled from "/home/dynamikd/public_html/clients/piksel/assets/themes/modern/about.tpl" */ ?>
<?php /*%%SmartyHeaderCode:14111867075877c6f873f1c6-31146127%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '520df77fd15fbf99b4c48eaf92e5abf15359fc11' => 
    array (
      0 => '/home/dynamikd/public_html/clients/piksel/assets/themes/modern/about.tpl',
      1 => 1402068702,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '14111867075877c6f873f1c6-31146127',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'lang' => 0,
    'content' => 0,
    'config' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.8',
  'unifunc' => 'content_5877c6f8791a63_15150313',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5877c6f8791a63_15150313')) {function content_5877c6f8791a63_15150313($_smarty_tpl) {?><!DOCTYPE HTML>
<html>
<head>
	<?php echo $_smarty_tpl->getSubTemplate ('head.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

</head>
<body>
	<?php echo $_smarty_tpl->getSubTemplate ('overlays.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

	<div id="container">
		<?php echo $_smarty_tpl->getSubTemplate ('header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

		<?php echo $_smarty_tpl->getSubTemplate ('header2.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>
		
		
		<div class="container">
			<div class="row">
				<?php echo $_smarty_tpl->getSubTemplate ('subnav.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>
		
				<div class="col-md-9">
					
					<h1><?php echo $_smarty_tpl->tpl_vars['lang']->value['aboutUs'];?>
</h1>
					<div class="container" style="padding: 0;">
						<div class="row">
							
							<div class="col-md-9">
								<?php echo $_smarty_tpl->tpl_vars['content']->value['body'];?>

							</div>
							
							<div class="col-md-3">
								<p>
									<strong><?php echo $_smarty_tpl->tpl_vars['config']->value['settings']['business_name'];?>
</strong><br>
									<?php echo $_smarty_tpl->tpl_vars['config']->value['settings']['business_address'];?>
<br>
									<?php if ($_smarty_tpl->tpl_vars['config']->value['settings']['business_address2']){?><?php echo $_smarty_tpl->tpl_vars['config']->value['settings']['business_address2'];?>
<br><?php }?>
									<?php echo $_smarty_tpl->tpl_vars['config']->value['settings']['business_city'];?>
, <?php echo $_smarty_tpl->tpl_vars['config']->value['settings']['business_state'];?>
 <?php echo $_smarty_tpl->tpl_vars['config']->value['settings']['business_zip'];?>
<br>
									<?php echo $_smarty_tpl->tpl_vars['config']->value['settings']['business_country'];?>

									<?php if ($_smarty_tpl->tpl_vars['config']->value['settings']['contact']){?>
										<br><br><a href="<?php echo linkto(array('page'=>"contact.php"),$_smarty_tpl);?>
" class="colorLink"><?php echo $_smarty_tpl->tpl_vars['lang']->value['contactUs'];?>
</a>
									<?php }?>
								</p>
							</div>
						</div>
					</div>
					
				</div>
			</div>
		</div>
		<?php echo $_smarty_tpl->getSubTemplate ('footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

    </div>
</body>
</html><?php }} ?>