<?php /* Smarty version Smarty-3.1.8, created on 2016-11-28 19:04:00
         compiled from "/home/dynamikd/public_html/clients/piksel/assets/themes/modern/media.container.tpl" */ ?>
<?php /*%%SmartyHeaderCode:604126460583c7fa08e0b01-88801003%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '164d762cce92cadd05c07fde1c540b01c118c607' => 
    array (
      0 => '/home/dynamikd/public_html/clients/piksel/assets/themes/modern/media.container.tpl',
      1 => 1425384996,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '604126460583c7fa08e0b01-88801003',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'config' => 0,
    'media' => 0,
    'imgPath' => 0,
    'thumbMediaDetails' => 0,
    'detail' => 0,
    'stars' => 0,
    'lang' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.8',
  'unifunc' => 'content_583c7fa09c7529_06724545',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_583c7fa09c7529_06724545')) {function content_583c7fa09c7529_06724545($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_truncate')) include '/home/dynamikd/public_html/clients/piksel/assets/smarty/plugins/modifier.truncate.php';
?><div class="mediaContainer" style="width: <?php echo $_smarty_tpl->tpl_vars['config']->value['settings']['thumb_size']+25;?>
px" id="mediaContainer<?php echo $_smarty_tpl->tpl_vars['media']->value['media_id'];?>
">
	<div class="mcPadding">
		<div class="mediaThumbContainer loader1Center" id="thumb<?php echo $_smarty_tpl->tpl_vars['media']->value['media_id'];?>
" style="height: <?php echo $_smarty_tpl->tpl_vars['config']->value['settings']['thumb_size']+20;?>
px; width: <?php echo $_smarty_tpl->tpl_vars['config']->value['settings']['thumb_size'];?>
px;"><a href="<?php echo $_smarty_tpl->tpl_vars['media']->value['linkto'];?>
"><?php if ($_smarty_tpl->tpl_vars['media']->value['dsp_type']=='video'){?><img src="<?php echo $_smarty_tpl->tpl_vars['imgPath']->value;?>
/dtype.video.png" class="dtypeIcon"><?php }?><img src="<?php if ($_smarty_tpl->tpl_vars['media']->value['thumbCachedLink']){?><?php echo $_smarty_tpl->tpl_vars['media']->value['thumbCachedLink'];?>
<?php }else{ ?><?php echo mediaImage(array('mediaID'=>$_smarty_tpl->tpl_vars['media']->value['encryptedID'],'type'=>'thumb','folderID'=>$_smarty_tpl->tpl_vars['media']->value['encryptedFID'],'seo'=>$_smarty_tpl->tpl_vars['media']->value['seoName']),$_smarty_tpl);?>
<?php }?>" class="img-thumbnail img-responsive mediaThumb <?php if ($_smarty_tpl->tpl_vars['config']->value['settings']['rollover_status']){?>showHoverWindow<?php }?>" mediaID="<?php echo $_smarty_tpl->tpl_vars['media']->value['encryptedID'];?>
" alt="<?php echo $_smarty_tpl->tpl_vars['media']->value['title'];?>
"></a></div>
		<ul class="mediaContent">
			<?php  $_smarty_tpl->tpl_vars['detail'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['detail']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['thumbMediaDetails']->value[$_smarty_tpl->tpl_vars['media']->value['media_id']]; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['detail']->key => $_smarty_tpl->tpl_vars['detail']->value){
$_smarty_tpl->tpl_vars['detail']->_loop = true;
?>
				<li>
					<?php if ($_smarty_tpl->tpl_vars['detail']->value['value']!=''){?>
						<span class="mediaDetailLabel mediaDetailLabel<?php echo $_smarty_tpl->tpl_vars['detail']->key;?>
"><?php echo $_smarty_tpl->tpl_vars['detail']->value['lang'];?>
</span>: <span class="mediaDetailValue mediaDetailValue<?php echo $_smarty_tpl->tpl_vars['detail']->key;?>
">
						<?php if ($_smarty_tpl->tpl_vars['detail']->key=='owner'){?>
							<?php if ($_smarty_tpl->tpl_vars['detail']->value['value']['useID']){?>
								<a href="<?php echo linkto(array('page'=>"contributors.php?id=".($_smarty_tpl->tpl_vars['detail']->value['value']['useID'])."&seoName=".($_smarty_tpl->tpl_vars['detail']->value['value']['seoName'])),$_smarty_tpl);?>
" class="colorLink"><?php echo $_smarty_tpl->tpl_vars['detail']->value['value']['displayName'];?>
</a>
							<?php }else{ ?>
								<?php echo $_smarty_tpl->tpl_vars['detail']->value['value']['displayName'];?>

							<?php }?>
						<?php }else{ ?>						
							<?php echo smarty_modifier_truncate($_smarty_tpl->tpl_vars['detail']->value['value'],40);?>

						<?php }?>
						</span>
					<?php }?>
				</li>
			<?php } ?>
			<?php if ($_smarty_tpl->tpl_vars['media']->value['showRating']){?>
				<li>
					<p class="ratingStarsContainer <?php if ($_smarty_tpl->tpl_vars['media']->value['allowRating']){?>starRating<?php }?>" mediaID="<?php echo $_smarty_tpl->tpl_vars['media']->key;?>
">
						<?php  $_smarty_tpl->tpl_vars['stars'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['stars']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['media']->value['rating']['stars']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['stars']->key => $_smarty_tpl->tpl_vars['stars']->value){
$_smarty_tpl->tpl_vars['stars']->_loop = true;
?><img src="<?php echo $_smarty_tpl->tpl_vars['imgPath']->value;?>
/star.<?php echo $_smarty_tpl->tpl_vars['stars']->value;?>
.png" class="ratingStar" originalStatus="<?php echo $_smarty_tpl->tpl_vars['stars']->value;?>
"><?php } ?>
						&nbsp;<span class="mediaDetailValue"><strong><?php echo $_smarty_tpl->tpl_vars['media']->value['rating']['average'];?>
</strong>/<?php echo $_smarty_tpl->tpl_vars['config']->value['RatingStars'];?>
 (<?php echo $_smarty_tpl->tpl_vars['media']->value['rating']['votes'];?>
 <?php echo $_smarty_tpl->tpl_vars['lang']->value['votes'];?>
)</span><br>
					</p>
				</li>
			<?php }?>
			<li class="mediaContainerIcons">
				<?php if ($_smarty_tpl->tpl_vars['media']->value['showLightbox']){?><div><img src="<?php echo $_smarty_tpl->tpl_vars['imgPath']->value;?>
/lightbox.icon.<?php echo $_smarty_tpl->tpl_vars['media']->value['inLightbox'];?>
.png" inLightbox="<?php echo $_smarty_tpl->tpl_vars['media']->value['inLightbox'];?>
" lightboxItemID="<?php echo $_smarty_tpl->tpl_vars['media']->value['lightboxItemID'];?>
" mediaID="<?php echo $_smarty_tpl->tpl_vars['media']->value['media_id'];?>
" id="addToLightboxButton<?php echo $_smarty_tpl->tpl_vars['media']->value['media_id'];?>
" class="mediaContainerIcon addToLightboxButton" title="<?php echo $_smarty_tpl->tpl_vars['lang']->value['lightbox'];?>
"></div><?php }?>
				<?php if ($_smarty_tpl->tpl_vars['config']->value['settings']['email_friend']&&$_smarty_tpl->tpl_vars['config']->value['settings']['thumbDetailsEmail']){?><div><img src="<?php echo $_smarty_tpl->tpl_vars['imgPath']->value;?>
/email.icon.0.png" class="mediaContainerIcon emailToFriend" mediaID="<?php echo $_smarty_tpl->tpl_vars['media']->value['useMediaID'];?>
" title="<?php echo $_smarty_tpl->tpl_vars['lang']->value['email'];?>
"></div><?php }?>
				<?php if ($_smarty_tpl->tpl_vars['config']->value['settings']['thumbDetailsPackage']){?><div><img src="<?php echo $_smarty_tpl->tpl_vars['imgPath']->value;?>
/package.icon.0.png" class="mediaContainerIcon assignToPackageButton" mediaID="<?php echo $_smarty_tpl->tpl_vars['media']->value['media_id'];?>
" title="<?php echo $_smarty_tpl->tpl_vars['lang']->value['assignToPackage'];?>
"></div><?php }?>
				<?php if ($_smarty_tpl->tpl_vars['config']->value['settings']['thumbDetailsDownloads']){?>
				<div>
					<div class="thumbDownloadContainer" id="thumbDownloadContainer<?php echo $_smarty_tpl->tpl_vars['media']->value['media_id'];?>
"></div>
					<img src="<?php echo $_smarty_tpl->tpl_vars['imgPath']->value;?>
/download.icon.0.png" mediaID="<?php echo $_smarty_tpl->tpl_vars['media']->value['media_id'];?>
" id="downloadMediaButton<?php echo $_smarty_tpl->tpl_vars['media']->value['media_id'];?>
" class="mediaContainerIcon downloadMediaButton" title="<?php echo $_smarty_tpl->tpl_vars['lang']->value['download'];?>
">
				</div>
				<?php }?>
			</li>
		</ul>
	</div>
</div><?php }} ?>