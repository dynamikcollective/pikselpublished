<?php /* Smarty version Smarty-3.1.8, created on 2016-11-28 16:52:49
         compiled from "/home/dynamikd/public_html/clients/piksel/assets/themes/modern/footer.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1363945327583c60e1c42aa6-09314850%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'bea21be369613181091e3b0376ecaccd49f46962' => 
    array (
      0 => '/home/dynamikd/public_html/clients/piksel/assets/themes/modern/footer.tpl',
      1 => 1428072962,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1363945327583c60e1c42aa6-09314850',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'contentBlocks' => 0,
    'lang' => 0,
    'baseURL' => 0,
    'config' => 0,
    'imgPath' => 0,
    'theme' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.8',
  'unifunc' => 'content_583c60e1cefa86_67055060',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_583c60e1cefa86_67055060')) {function content_583c60e1cefa86_67055060($_smarty_tpl) {?><footer>
	<?php if ($_smarty_tpl->tpl_vars['contentBlocks']->value['customBlockFooter']){?>
		<div><?php echo $_smarty_tpl->tpl_vars['contentBlocks']->value['customBlockFooter']['content'];?>
</div>
	<?php }?>
	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<?php echo $_smarty_tpl->tpl_vars['lang']->value['copyright'];?>
 <a href="<?php echo $_smarty_tpl->tpl_vars['baseURL']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['config']->value['settings']['business_name'];?>
</a><br><?php echo $_smarty_tpl->tpl_vars['lang']->value['reserved'];?>

			</div>
			<div class="col-md-3">
				<?php if (addon('rss')){?>
				<ul>
					<li><strong><?php echo $_smarty_tpl->tpl_vars['lang']->value['rss'];?>
</strong></li>
					<?php if ($_smarty_tpl->tpl_vars['config']->value['settings']['rss_newest']){?><li><a href="<?php echo linkto(array('page'=>'rss.php?mode=newestMedia'),$_smarty_tpl);?>
"><?php echo $_smarty_tpl->tpl_vars['lang']->value['newestMedia'];?>
</a></li><?php }?>
					<?php if ($_smarty_tpl->tpl_vars['config']->value['settings']['rss_newest']){?><li><a href="<?php echo linkto(array('page'=>'rss.php?mode=popularMedia'),$_smarty_tpl);?>
"><?php echo $_smarty_tpl->tpl_vars['lang']->value['popularMedia'];?>
</a></li><?php }?>
					<?php if ($_smarty_tpl->tpl_vars['config']->value['settings']['rss_featured_media']){?><li><a href="<?php echo linkto(array('page'=>'rss.php?mode=featuredMedia'),$_smarty_tpl);?>
"><?php echo $_smarty_tpl->tpl_vars['lang']->value['featuredMedia'];?>
</a></li><?php }?>
				</ul>
				<?php }?>
			</div>
			<div class="col-md-3">
				<ul style="margin-bottom: 10px;">
					<?php if ($_smarty_tpl->tpl_vars['config']->value['settings']['contact']){?><li><a href="<?php echo linkto(array('page'=>"contact.php"),$_smarty_tpl);?>
"><?php echo $_smarty_tpl->tpl_vars['lang']->value['contactUs'];?>
</a></li><?php }?>
					<?php if ($_smarty_tpl->tpl_vars['config']->value['settings']['aboutpage']){?><li><a href="<?php echo linkto(array('page'=>"about.php"),$_smarty_tpl);?>
"><?php echo $_smarty_tpl->tpl_vars['lang']->value['aboutUs'];?>
</a></li><?php }?>
					<?php if ($_smarty_tpl->tpl_vars['config']->value['settings']['forum_link']){?><li><a href="<?php echo $_smarty_tpl->tpl_vars['config']->value['settings']['forum_link'];?>
"><?php echo $_smarty_tpl->tpl_vars['lang']->value['forum'];?>
</a></li><?php }?>					
					<?php if ($_smarty_tpl->tpl_vars['config']->value['settings']['tospage']){?><li><a href="<?php echo linkto(array('page'=>'terms.of.use.php'),$_smarty_tpl);?>
"><?php echo $_smarty_tpl->tpl_vars['lang']->value['termsOfUse'];?>
</a></li><?php }?>
					<?php if ($_smarty_tpl->tpl_vars['config']->value['settings']['pppage']){?><li><a href="<?php echo linkto(array('page'=>'privacy.policy.php'),$_smarty_tpl);?>
"><?php echo $_smarty_tpl->tpl_vars['lang']->value['privacyPolicy'];?>
</a></li><?php }?>
					<?php if ($_smarty_tpl->tpl_vars['config']->value['settings']['papage']){?><li><a href="<?php echo linkto(array('page'=>'purchase.agreement.php'),$_smarty_tpl);?>
"><?php echo $_smarty_tpl->tpl_vars['lang']->value['purchaseAgreement'];?>
</a></li><?php }?>
				</ul>
				<?php if ($_smarty_tpl->tpl_vars['config']->value['settings']['facebook_link']){?><a href="<?php echo $_smarty_tpl->tpl_vars['config']->value['settings']['facebook_link'];?>
" target="_blank"><img src="<?php echo $_smarty_tpl->tpl_vars['imgPath']->value;?>
/facebook.icon.png" width="20" title="Facebook"></a><?php }?>&nbsp;<?php if ($_smarty_tpl->tpl_vars['config']->value['settings']['twitter_link']){?><a href="<?php echo $_smarty_tpl->tpl_vars['config']->value['settings']['twitter_link'];?>
" target="_blank"><img src="<?php echo $_smarty_tpl->tpl_vars['imgPath']->value;?>
/twitter.icon.png" width="20" title="Twitter"></a><?php }?>
			</div>
			<div class="col-md-3 text-right">
				<?php if (!addon('unbrand')){?>
					<!-- Powered By PhotoStore | Sell Your Photos Online -->
					<p id="poweredBy">Powered By <a href="http://www.ktools.net/photostore/" target="_blank" class="photostoreLink" title="Powered By PhotoStore | Sell Your Photos Online">PhotoStore</a><br><a href="http://www.ktools.net/photostore/" target="_blank" class="sellPhotos">Sell Photos Online</a></p>
				<?php }?>
			</div>
		</div>
	</div>
	<div id="statsCode"><?php echo $_smarty_tpl->tpl_vars['config']->value['settings']['stats_html'];?>
</div>
</footer>
<script src="<?php echo $_smarty_tpl->tpl_vars['baseURL']->value;?>
/assets/themes/<?php echo $_smarty_tpl->tpl_vars['theme']->value;?>
/js/bootstrap.min.js"></script>

<?php if ($_smarty_tpl->tpl_vars['config']->value['settings']['fotomoto']){?><script type="text/javascript" src="//widget.fotomoto.com/stores/script/<?php echo $_smarty_tpl->tpl_vars['config']->value['settings']['fotomoto'];?>
.js"></script><?php }?><?php }} ?>