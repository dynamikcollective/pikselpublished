<!DOCTYPE HTML>
<html>
	<head>
		{include file='head.tpl'}
		
		<script>
			$(function()
			{
				$('#myCarousel').carousel({
					interval: {$config.settings.hpf_inverval}
				});
			});
		</script>
		<script type="text/javascript" src="{$baseURL}/assets/javascript/gallery.js"></script>
	</head>
	<body>
		{include file='overlays.tpl'}		
		{include file='header-home.tpl'}
		
		{if $config.settings.hpfeaturedmedia and $featuredMedia}
		<!-- Carousel
		================================================== -->
		<div id="myCarousel" class="carousel slide">
			<!-- Indicators -->
			<ol class="carousel-indicators">
				{foreach $featuredMedia as $media}
					<li data-target="#myCarousel" data-slide-to="{$media@iteration - 1}" class="{if $media@first}active{/if}"></li>
				{/foreach}
			</ol>
			<div class="carousel-inner">
				{foreach $featuredMedia as $media}
				<div class="item {if $media@first}active{/if}">
					<img src="image.php?mediaID={$media.encryptedID}=&type=featured&folderID={$media.encryptedFID}&size={$config.settings.hpf_width}&crop={$config.settings.hpf_crop_to}" encMediaID="{$media.encryptedID}" href="{$media.linkto}" alt="{$media.title.value}">
					<div class="container">
						<div class="carousel-caption">
							<h1>{$media.title.value}</h1>
							<p class="hidden-xs">{$media.description.value}</p>
							<p><a class="btn btn-large btn-primary" href="{$media.linkto}">{$lang.details}</a></p>
						</div>
					</div>
				</div>
				{/foreach}
			</div>
			<a class="left carousel-control" href="#myCarousel" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
			<a class="right carousel-control" href="#myCarousel" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
		</div><!-- /.carousel -->
		{/if}
		
		{include file='header2.tpl'}	
		
		<div class="">
			<div class="">
				<!-- include file='subnav.tpl' -->
				<div id="wrapper-categorias"class="col-md-12">
					<!--
					<div id="homepageJumbotron">
						<div>
							<center><h4>Contenido</h4></center>
						</div>
					</div>				
					-->
					
					{* Popular Media Area / DESTACADOS *}
					{if $popularMediaRows}
					<section id="destacados" class="clearfix home">
						<div class="container">
						<h3>{if $config.settings.popularpage}<a href="{linkto page='gallery.php?mode=popular-media&page=1'}">Lo más buscado</a>{else}{$lang.popularMedia}{/if}{if $config.settings.rss_popular} <a href="{linkto page='rss.php?mode=popularMedia'}" class="btn btn-xxs btn-warning">{$lang.rss}</a>{/if}</h3>
						<!--<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>-->
						</div>
						<div class="imgs-wrapper container">
							{foreach $popularMedia as $media}
								{include file='media.container.tpl'}
							{/foreach}
						</div>
					</section>
					{/if}					
					
					{* Featured Galleries Area  / CATEGORIAS *}
					{if $subGalleriesData}
					<section id="categorias" class="clearfix home">
						<div class="container">
						<h3><span>{$lang.featuredGalleries}</span></h3>	
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>					
						<div id="galleryListContainer">
							{foreach $subGalleriesData as $subGallery}
								<figure class="galleryContainer col-xs-12 col-sm-2">
									{if $galleriesData.$subGallery.galleryIcon}
									<a href="{$galleriesData.$subGallery.linkto}"><img class="img-responsive" src="{$baseURL}/{$galleriesData.$subGallery.galleryIcon.imgSrc}"></a>
									{/if}{*old {productShot itemID=$subGallery itemType=gallery photoID=$galleriesData.$subGallery.galleryIcon.ip_id size=$config.settings.gallery_thumb_size} *}
									<figcaption class="galleryDetails">
										{if $galleriesData.$subGallery.password}
											<img src="{$imgPath}/lock.png" class="lock">
										{/if}
										<a href="{$galleriesData.$subGallery.linkto}">{$galleriesData.$subGallery.name}</a>
										{if $config.settings.gallery_count}{if $galleriesData.$subGallery.gallery_count > 0 or $config.ShowZeroCounts}&nbsp;<span class="galleryMediaCount">({$galleriesData.$subGallery.gallery_count})</span>{/if}{/if}
									</figcaption>
									<!--gi: {$galleriesData.$subGallery.galleryIcon.imgSrc}-->
								</figure>
							{/foreach}
						</div>
						</div>
					</section>
					{/if}
					
					{* Newest Media Area / NUEVOS *}
					{if $newestMediaRows}				
					<section id="nuevos" class="clearfix home">
						<div class="container">
						<h3>{if $config.settings.newestpage}<a href="{linkto page='gallery.php?mode=newest-media&page=1'}">{$lang.newestMedia}</a>{else}{$lang.newestMedia}{/if}{if $config.settings.rss_newest} <a href="{linkto page='rss.php?mode=newestMedia'}" class="btn btn-xxs btn-warning">{$lang.rss}</a>{/if}</h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
						<div class="imgs-wrapper">
							{foreach $newestMedia as $media}
								{include file='media.container.tpl'}
							{/foreach}
						</div>
						</div>
					</section>
					{/if}
					
					
					{* Random Media Area *}
					{if $randomMediaRows}
					<div class="clearfix">
						<hr>
						<h3>{$lang.randomMedia}</h3>						
						<div>
							{foreach $randomMedia as $media}
								{include file='media.container.tpl'}
							{/foreach}
						</div>
					</div>
					{/if}
					
				</div>
			</div>
		</div>
		{include file='footer.tpl'}
	</body>
</html>
