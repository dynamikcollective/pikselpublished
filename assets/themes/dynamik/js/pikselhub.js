$(document).ready(function() {

    $( window ).scroll(function() {
        var positionW = 0;
        positionW = $(this).scrollTop();
        if(positionW > 0){
            $("nav").addClass("scrolled");
        }
        else{
            $("nav").removeClass("scrolled");
        }
        //console.log($(this).scrollTop());
    });

    $('button.navbar-toggle').click(function() {
        $('button.navbar-toggle').toggleClass('opened');
    });

    if($("section").is("#destacados")){
        var contador = 1;
        var modContador = 0;
        $('#destacados .mediaContainer').each(function() {
            modContador = contador % 6;
            console.log(modContador +'--------->medio numero' + contador);
            $(this).children('div').removeClass('mcPadding');
            $(this).children('div').children('div').children('a').children('img').removeClass('img-thumbnail');
            if(modContador == 0 || modContador == 1){
                $(this).addClass('col-xs-12 col-sm-6');
            }
            else{
                $(this).addClass('col-xs-12 col-sm-3');
            }
            contador++;
        });
    }
    if($("section").is("#nuevos")){
        var contador = 1;
        var modContador = 0;
        $('#nuevos .mediaContainer').each(function() {
            modContador = contador % 6;
            console.log(modContador +'--------->medio numero' + contador);
            $(this).children('div').removeClass('mcPadding');
            $(this).children('div').children('div').children('a').children('img').removeClass('img-thumbnail');
            if(modContador == 0 || modContador == 1){
                $(this).addClass('col-xs-12 col-sm-6');
            }
            else{
                $(this).addClass('col-xs-12 col-sm-3');
            }
            contador++;
        });
    }
    if($("section").is("#results-search-wrapper")){
        console.log('#results-search-wrapper');
        $('#results-search-wrapper .mediaContainer').each(function() {
            console.log('ajustado');
            $(this).children('div').removeClass('mcPadding');
            $(this).children('div').children('div').children('a').children('img').removeClass('img-thumbnail');
            $(this).addClass('col-xs-12 col-sm-4');
        });
    }
    if($("section").is("#wrapper-gallery")){
        console.log('#wrapper-gallery');
        $('#wrapper-gallery .mediaContainer').each(function() {
            console.log('ajustado');
            $(this).children('div').removeClass('mcPadding');
            $(this).children('div').children('div').children('a').children('img').removeClass('img-thumbnail');
            $(this).addClass('col-xs-12 col-sm-4');
        });
    }



});