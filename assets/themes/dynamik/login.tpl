<!DOCTYPE HTML>
<html>
<head>
	{include file='head.tpl'}
	<script type="text/javascript" src="{$baseURL}/assets/javascript/login.js"></script>
</head>
<body>
	{include file='overlays.tpl'}
	<div id="container">
		{include file='header.tpl'}
		
		<section id="login-wrapper" class="container">
			<div class="row">
				<div class="col-xs-12"> 
					<div class="title-container"><h1>{$lang.login}</h1></div>
				</div>
				<div class="col-xs-12 col-md-4 col-md-offset-4">
					
					<div class="divTableCell contentRightColumn">
						<div class="content">
							{if $logNotice}<p class="notice">{$lang.{$logNotice}}</p><br>{/if}
							{$lang.loginMessage}
							<form id="loginForm" class="cleanForm form-group" action="login.php" method="post">
							<div class="divTable">
								<div class="divTableRow">
									<div class="divTableCell"><input type="text" id="memberEmail" name="memberEmail" style="min-width: 220px" class="form-control" placeholder="{$lang.email}"></div>
								</div>
								<div class="divTableRow">
									<div class="divTableCell"><input type="password" id="memberPassword" name="memberPassword" style="min-width: 220px" class="form-control" placeholder="{$lang.password}"></div>
								</div>
								<div class="divTableRow">
									<div id="submit-contact" class="divTableCell" style="text-align: right;"><a href="workbox.php?mode=forgotPassword" id="forgotPassword">{$lang.forgotPassword}</a> &nbsp; <input type="submit" value="{$lang.loginCaps}" class="btn btn-xs btn-primary"></div>
								</div>
							</div>
							</form>
						</div>
					</div>
					<div class="new-account">
						<hr>
						<h2>{$lang.createAccount}</h2>				
						<a href="create.account.php?jumpTo=members" class="btn btn-xs btn-primary">&nbsp;{$lang.createAccount}&nbsp;</a>
					</div>	
				</div>		
			</div>
		</section>
		{include file='footer.tpl'}
    </div>
</body>
</html>