<div id="header">
    <nav class="navbar navbar-piksel navbar-default navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
                <button tupe="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" arial-expanded="false" aria-controls="navbar">
                    <span class="sr-only"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="logo" href="{linkto page="index.php"}">
                    <img src="{$mainLogo}" id="mainLogo"/>
                </a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <div class="top-header cart-currency">

                    {if $cartStatus}			
                        <div class="nav navbar-right">
                            <div id="headerCartBox">
                                <div id="cartPreviewContainer">
                                    <div id="miniCartContainer"></div>
                                    <div style="float: left; position: relative;" class="viewCartLink"><p id="cartItemsCount">{$cartTotals.itemsInCart}</p><a href="{linkto page="cart.php"}"><img src="{$imgPath}/cart.icon.png" alt="{$lang.cart}"></a></div>
                                    <div style="float: left; display:{if $cartTotals.priceSubTotal or $cartTotals.creditsSubTotalPreview}block{else}none{/if};" id="cartPreview">
                                        <a href="{linkto page="cart.php"}" class="viewCartLink">
                                        <span id="cartPreviewPrice" style="{if !$currencySystem}display: none;{/if}">{$cartTotals.priceSubTotalPreview.display}</span><!-- with tax {$cartTotals.totalLocal.display}-->
                                        {if $creditSystem and $currencySystem} + {/if}
                                        <span id="cartPreviewCredits" style="{if !$creditSystem}display: none;{/if}">{$cartTotals.creditsSubTotalPreview} </span> {if $creditSystem}{$lang.credits}{/if}
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    {/if}

                    <ul class="nav navbar-nav navbar-right">
                        {if $displayLanguages|@count > 1}
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">{$displayLanguages.$selectedLanguage}<b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                {foreach $displayLanguages as $language}
                                    <li><a href="{linkto page="actions.php?action=changeLanguage&setLanguage={$language@key}"}">{$language}</a></li>
                                {/foreach}
                            </ul>
                        </li>
                        {/if}
                        {if $displayCurrencies|@count > 1}
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">{$activeCurrencies.$selectedCurrency.name} ({$activeCurrencies.$selectedCurrency.code})<b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                {foreach $displayCurrencies as $currency}
                                    <li><a href="{linkto page="actions.php?action=changeCurrency&setCurrency={$currency@key}"}">{$currency} ({$activeCurrencies.{$currency@key}.code})</a></li>
                                {/foreach}
                            </ul>
                        </li>
                        {/if}
                    </ul>

                </div>
                <div class="row">
                    <ul id="main-menu" class="nav navbar-nav main-menu navbar-right">
                        <li><a href="{linkto page="search.php?postSearchForm=1&keywordsExist=0&searchPhrase=&mediaTypes%5B4%5D=4&red=&green=&blue=&hex=&searchDate%5BdateRangeSearch%5D=off&searchDate%5BfromYear%5D=2000&searchDate%5BfromMonth%5D=01&searchDate%5BfromDay%5D=01&searchDate%5BtoYear%5D=2017&searchDate%5BtoMonth%5D=01&searchDate%5BtoDay%5D=27"}">Fotografía</a></li>
					    <li><a href="{linkto page="search.php?postSearchForm=1&keywordsExist=0&searchPhrase=&mediaTypes%5B5%5D=5&red=&green=&blue=&hex=&searchDate%5BdateRangeSearch%5D=off&searchDate%5BfromYear%5D=2000&searchDate%5BfromMonth%5D=01&searchDate%5BfromDay%5D=01&searchDate%5BtoYear%5D=2017&searchDate%5BtoMonth%5D=01&searchDate%5BtoDay%5D=27&searchSortBy=relevancy"}">Ilustración</a></li>
                        <li><a href="{linkto page="content.php?id=4"}">Precios</a></li>
                        <li><a href="{linkto page="content.php?id=16"}">Acerca de Piksel HUB</a></li>
                        <li><a href="{linkto page="contact.php"}">Contáctanos</a></li>
                        <li><a href="{linkto page="login.php?jumpTo=members"}">{$lang.login}</a></li>
                    </ul>
                </div>
            </div><!--/.nav-collapse -->
        </div>
    </nav>

    {if $message}
        {foreach $message as $messageLang}
            <div class="container messageBar alert alert-danger">{$lang.{$messageLang}} <p><a href="#" class="buttonLink btn btn-xs btn-danger">X</a></p></div>
        {/foreach}
    {/if}	
    {* Header Search Box Area *}
        
    <div class="wrapper-cart-interna container">	


        <div class="search-wrapper col-xs-12 col-md-5">
            <form role="search" action="{linkto page="search.php"}" method="get" id="searchFormTest" class="navbar-form">			
            <input type="hidden" name="clearSearch" value="true">
                <div class="input-wrapper">					
                    <input type="text" class="form-control" placeholder="{$lang.enterKeywords}" name="searchPhrase" id="searchPhrase">			
                </div>
                <div style="margin-top: 6px;">
                    {if $currentGallery.gallery_id}<input type="checkbox" name="galleries" id="searchCurrentGallery" value="{$currentGallery.gallery_id}" checked="checked"><label for="searchCurrentGallery">{$lang.curGalleryOnly}</label>&nbsp;&nbsp;{/if}
                    <!--<a href="{linkto page='search.php'}">{$lang.advancedSearch}</a>-->
                    {* Event Search Link *}
                    {if $config.settings.esearch}
                        <a href="{linkto page="esearch.php"}">{$lang.eventSearch}</a>
                    {/if}
                </div>
            </form>		
        </div>

        <div class="col-md-7 cart-currency">
            <!--
            {if $cartStatus}			
                <div class="nav navbar-right">
                    <div id="headerCartBox">
                        <div id="cartPreviewContainer">
                            <div id="miniCartContainer"></div>
                            <div style="float: left; position: relative;" class="viewCartLink"><p id="cartItemsCount">{$cartTotals.itemsInCart}</p><a href="{linkto page="cart.php"}"><img src="{$imgPath}/cart.icon.png" alt="{$lang.cart}"></a></div>
                            <div style="float: left; display:{if $cartTotals.priceSubTotal or $cartTotals.creditsSubTotalPreview}block{else}none{/if};" id="cartPreview">
                                <a href="{linkto page="cart.php"}" class="viewCartLink">
                                <span id="cartPreviewPrice" style="{if !$currencySystem}display: none;{/if}">{$cartTotals.priceSubTotalPreview.display}</span>
                                {if $creditSystem and $currencySystem} + {/if}
                                <span id="cartPreviewCredits" style="{if !$creditSystem}display: none;{/if}">{$cartTotals.creditsSubTotalPreview} </span> {if $creditSystem}{$lang.credits}{/if}
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            {/if}

            <ul class="nav navbar-nav navbar-right">
                {if $displayLanguages|@count > 1}
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">{$displayLanguages.$selectedLanguage}<b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        {foreach $displayLanguages as $language}
                            <li><a href="{linkto page="actions.php?action=changeLanguage&setLanguage={$language@key}"}">{$language}</a></li>
                        {/foreach}
                    </ul>
                </li>
                {/if}
                {if $displayCurrencies|@count > 1}
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">{$activeCurrencies.$selectedCurrency.name} ({$activeCurrencies.$selectedCurrency.code})<b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        {foreach $displayCurrencies as $currency}
                            <li><a href="{linkto page="actions.php?action=changeCurrency&setCurrency={$currency@key}"}">{$currency} ({$activeCurrencies.{$currency@key}.code})</a></li>
                        {/foreach}
                    </ul>
                </li>
                {/if}
            </ul>
            -->
        </div>

    </div>


</div>
