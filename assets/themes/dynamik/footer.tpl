<footer>
	<section class="top">
		<div class="container">
			<div class="wrapper-logo-footer col-xs-12">
				<img src="{$baseURL}/assets/themes/{$theme}/img/ico-footer.png" alt="">
				<div class="line"></div>
			</div>
			<div class="wrapper-menu col-xs-12">
				<ul class="list-inline">
					<li><a href="{linkto page="search.php?postSearchForm=1&keywordsExist=0&searchPhrase=&mediaTypes%5B4%5D=4&red=&green=&blue=&hex=&searchDate%5BdateRangeSearch%5D=off&searchDate%5BfromYear%5D=2000&searchDate%5BfromMonth%5D=01&searchDate%5BfromDay%5D=01&searchDate%5BtoYear%5D=2017&searchDate%5BtoMonth%5D=01&searchDate%5BtoDay%5D=27"}">Fotografía</a></li>
					<li><a href="{linkto page="search.php?postSearchForm=1&keywordsExist=0&searchPhrase=&mediaTypes%5B5%5D=5&red=&green=&blue=&hex=&searchDate%5BdateRangeSearch%5D=off&searchDate%5BfromYear%5D=2000&searchDate%5BfromMonth%5D=01&searchDate%5BfromDay%5D=01&searchDate%5BtoYear%5D=2017&searchDate%5BtoMonth%5D=01&searchDate%5BtoDay%5D=27&searchSortBy=relevancy"}">Ilustración</a></li>
					<li><a href="{linkto page="content.php?id=4"}">Precios</a></li>
					<li><a href="{linkto page="content.php?id=16"}">Acerca de Piksel HUB</a></li>
					<li><a href="{linkto page="contact.php"}">Contáctanos</a></li>
				</ul>
			</div>
		</div>
	</section>
	<section class="bottom">
		<div class="container">
			<div class="wrapper col-xs-12">
				<span id="site-date"><span>Piksel 2017 -</span> Todos los derechos reservados |</span>
				<span id="dynamik">
					<span>Powered by: </span>
					<a href="http://www.dnmk.co/" target="_blank"><img src="{$baseURL}/assets/themes/{$theme}/img/dinamyk-logo.png" alt="logo-dynamik" title="Dynamik"/></a>
				</span>
			</div>
		</div>
	</section>
</footer>


<!--
<footer>
	{if $contentBlocks.customBlockFooter}
		<div>{$contentBlocks.customBlockFooter.content}</div>
	{/if}
	<div class="container">
		<div class="row">
			<div class="col-md-3">
				{$lang.copyright} <a href="{$baseURL}">{$config.settings.business_name}</a><br>{$lang.reserved}
			</div>
			<div class="col-md-3">
				{if addon('rss')}
				<ul>
					<li><strong>{$lang.rss}</strong></li>
					{if $config.settings.rss_newest}<li><a href="{linkto page='rss.php?mode=newestMedia'}">{$lang.newestMedia}</a></li>{/if}
					{if $config.settings.rss_newest}<li><a href="{linkto page='rss.php?mode=popularMedia'}">{$lang.popularMedia}</a></li>{/if}
					{if $config.settings.rss_featured_media}<li><a href="{linkto page='rss.php?mode=featuredMedia'}">{$lang.featuredMedia}</a></li>{/if}
				</ul>
				{/if}
			</div>
			<div class="col-md-3">
				<ul style="margin-bottom: 10px;">
					{if $config.settings.contact}<li><a href="{linkto page="contact.php"}">{$lang.contactUs}</a></li>{/if}
					{if $config.settings.aboutpage}<li><a href="{linkto page="about.php"}">{$lang.aboutUs}</a></li>{/if}
					{if $config.settings.forum_link}<li><a href="{$config.settings.forum_link}">{$lang.forum}</a></li>{/if}					
					{if $config.settings.tospage}<li><a href="{linkto page='terms.of.use.php'}">{$lang.termsOfUse}</a></li>{/if}
					{if $config.settings.pppage}<li><a href="{linkto page='privacy.policy.php'}">{$lang.privacyPolicy}</a></li>{/if}
					{if $config.settings.papage}<li><a href="{linkto page='purchase.agreement.php'}">{$lang.purchaseAgreement}</a></li>{/if}
				</ul>
				{if $config.settings.facebook_link}<a href="{$config.settings.facebook_link}" target="_blank"><img src="{$imgPath}/facebook.icon.png" width="20" title="Facebook"></a>{/if}&nbsp;{if $config.settings.twitter_link}<a href="{$config.settings.twitter_link}" target="_blank"><img src="{$imgPath}/twitter.icon.png" width="20" title="Twitter"></a>{/if}
			</div>
			<div class="col-md-3 text-right">
				{if !addon('unbrand')}-->
					<!-- Powered By PhotoStore | Sell Your Photos Online -->
					<!--<p id="poweredBy">Powered By <a href="http://www.ktools.net/photostore/" target="_blank" class="photostoreLink" title="Powered By PhotoStore | Sell Your Photos Online">PhotoStore</a><br><a href="http://www.ktools.net/photostore/" target="_blank" class="sellPhotos">Sell Photos Online</a></p>
				{/if}
			</div>
		</div>
	</div>
	<div id="statsCode">{$config.settings.stats_html}</div>
</footer>
-->
<script src="{$baseURL}/assets/themes/{$theme}/js/bootstrap.min.js"></script>

{if $config.settings.fotomoto}<script type="text/javascript" src="//widget.fotomoto.com/stores/script/{$config.settings.fotomoto}.js"></script>{/if}