<!DOCTYPE HTML>
<html>
<head>
	{include file='head.tpl'}
	<script type="text/javascript">
		<!--
			$(function(){ $('#navPopularMedia').addClass('selectedNav'); });
		-->		
	</script>
</head>
<body>
	{include file='overlays.tpl'}
	<div id="container">
		{include file='header.tpl'}
		<div id="contentContainer" class="center">
			{include file='search.row.tpl'}
			<div class="content">
				<h1>{$lang.popularMedia}{if $config.settings.rss_popular} <a href="{linkto page='rss.php?mode=popularMedia'}"><img src="{$imgPath}/rss.icon.small.png" class="rssH1Icon rssPageH1Icon"></a>{/if}</h1>
				{if $mediaRows}
					{include file="paging.tpl" paging=$mediaPaging}
					<div id="mediaListContainer">
						{foreach $mediaArray as $media}
							{include file='media.container.tpl'}
						{/foreach}
					</div>
					{include file="paging.tpl" paging=$mediaPaging}
				{else}
					<p class="notice">{$lang.noMedia}</p>
				{/if}
			</div>
		</div>
		{include file='footer.tpl'}
    </div>
</body>
</html>