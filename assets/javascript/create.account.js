$(function()
{	
	$('#email').blur(function()
	{
		$('.formErrorMessage').remove(); // Remove the error messages from previous submits
		$('*').removeClass('formError'); // Remove the formError class from any errors previously
		checkEmail();
	});
	
	$('#createAccountForm').submit(function(event)
	{
		//event.preventDefault();
		submitAccountForm();
		return false;
	});
});

/*
* Check and submit the create new account form
*/
function submitAccountForm()
{
	$('.formErrorMessage').remove(); // Remove the error messages from previous submits
	$('*').removeClass('formError'); // Remove the formError class from any errors previously
	
	var numOfStates = $('#state option').size() - 1; // Find the number of states retruned
	if(numOfStates == 0)	
		$('#state').removeAttr('require'); // If no states then remove state requirement
	
	var error = false;
	error = checkRequired();
	
	if(!error) // No other errors so far so continue checking
	{
		if($('#password').val() != $('#vpassword').val()) // Make sure the pass and verify pass match
		{
			displayFormError('#password',2);
			error = true;
			return false;
		}
	}
	
	if(!error) // No other errors so far so continue checking
	{
		if($('#password').val().length < 6) // Make sure the length of the password is at least 6 characters
		{	
			displayFormError('#password',3);
			error = true;
			return false;	
		}
	}
	
	if(!error)
		$('#createAccountForm').submit(); // No errors - submit the form
	else
		return false;
}

/*
* Check if email already exists
*/
function checkEmail()
{
	$.ajax({
		type: 'POST',
		url: 'actions.php',
		data: { "action": "checkEmail","email" : $('#email').val() },
		dataType: 'json',
		success: function(data)
		{	
			if(data.emailsReturned > 0)
				displayFormError('#email',2);
		}
	});	
}