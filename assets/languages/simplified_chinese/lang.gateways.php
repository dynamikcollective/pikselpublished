<?php
	# GATEWAYS LANG
	# 4.6.3
	# STRIPE
	$lang['stripe_displayName']			= "Stripe";
	$lang['stripe_publicDescription']	= "Credit Card or Debit Card";
	$lang['stripe_pkey']				= "Publishable Key";
	$lang['stripe_pkey_d']				= "Your publishable key for your stripe account.";
	$lang['stripe_skey']				= "Secret Key";
	$lang['stripe_skey_d']				= "Your secret key for your stripe account.";
	
	# 4.1.4
	# MOLLIE IDEAL
	$lang['mollieideal_displayName']		= "iDeal (Mollie)";
	$lang['mollieideal_f_partnerid']		= "伙伴编号";
	$lang['mollieideal_f_partnerid_d']		= "你的iDeal 伙伴ID.";
	$lang['mollieideal_f_profilekey']		= "简介关键";
	$lang['mollieideal_f_profilekey_d']		= "你的 Mollie iDeal 理想的关键.";
	$lang['mollieideal_f_testmode']			= "测试模式";
	$lang['mollieideal_f_testmode_d']		= "转成你的Mollie iDeal测试模式.";
	$lang['mollieideal_instructions']		= "确保TESTMODE对应的设置在你的 Mollie.nl帐户.";
	$lang['mollieideal_publicDescription']	= "支付安全和方便使用 iDeal (仅在荷兰).";
	
	# 4.1
	# ONEBIP 
	$lang['onebip_displayName']			= "OneBip"; 
	$lang['onebip_merchantid']			= "电子邮件";	
	$lang['onebip_merchantid_d']		= "输入为您的onebip帐户所使用的电子邮件."; 
	$lang['onebip_merchantkey']			= "API密钥"; 
	$lang['onebip_merchantkey_d']		= "输入为您的onebip帐户的API密钥.";
	$lang['onebip_testmode']			= "测试模式";
	$lang['onebip_testmode_d']			= "进入测试模式，用你的onebip支付.";
	$lang['onebip_publicDescription']	= "流动付款";
	
	# PAYFAST 
	$lang['payfast_displayName']		= "快速付款"; 
	$lang['payfast_merchantid']		= "编号";	
	$lang['payfast_merchantid_d']		= "输入编号"; 
	$lang['payfast_merchantkey']		= "编号"; 
	$lang['payfast_merchantkey_d']		= "将您的NOCHEX付款,进入测试模式";
	$lang['payfast_testmode']		= "测试模式"; 
	$lang['payfast_testmode_d']		= "将您的快速付款进入测试模式";
	$lang['payfast_publicDescription']	= "信用卡或银行"; 
	
	# NOCHEX
	$lang['nochex_displayName']		= "Nochex";
	$lang['nochex_f_accountid']		= "帐户编号";
	$lang['nochex_f_accountid_d']		= "您的NOCHEX的帐户编号";
	$lang['nochex_f_testmode']		= "测试模式";
	$lang['nochex_f_testmode_d']		= "将您的NOCHEX付款,进入测试模式";
	$lang['nochex_publicDescription']	= "信用卡或借记卡";
	
	# WORLDPAY
	$lang['worldpay_displayName']		= "WorldPay";
	$lang['worldpay_f_installid']		= "安装编号";
	$lang['worldpay_f_installid_d']		= "为您的WorldPay帐户安装编号";
	$lang['worldpay_f_testmode']		= "测试模式";
	$lang['worldpay_f_testmode_d']		= "将您的WorldPay付款,进入测试模式";
	$lang['worldpay_publicDescription']	= "信用卡或借记卡";
	
	# ROBOKASSA
	$lang['robokassa_displayName']		= "RoboKassa";
	$lang['robokassa_f_merchantid']		= "商家编号";
	$lang['robokassa_f_merchantid_d']	= "商家编号为您的RoboKassa的帐户.";
	$lang['robokassa_f_merchantpass']	= "商户编号商户通";
	$lang['robokassa_f_merchantpass_d']	= "商家编号为您的RoboKassa商人通ID.";
	$lang['robokassa_publicDescription']= "信用卡或借记卡";
	
	# PAYGATE
	$lang['paygate_displayName']		= "PayGate";
	$lang['paygate_f_accountid']		= "PayGate 编号";
	$lang['paygate_f_accountid_d']		= "D为您关闸的帐户.";
	$lang['paygate_f_accountkey']		= "PayGate 重点";
	$lang['paygate_f_accountkey_d']		= "您的PayGate编号.";
	$lang['paygate_f_testmode']			= "测试模式";
	$lang['paygate_f_testmode_d']		= "将您的的PayGate支付到测试模式.";	
	$lang['paygate_f_testid']			= "测试你的PayGateID";
	$lang['paygate_f_testid_d']			= "你的测试关闸ID";
	$lang['paygate_f_testkey']			= "测试PayGate关键";
	$lang['paygate_f_testkey_d']		= "你的PayGate测试重点";
	$lang['paygate_publicDescription']	= "信用卡或借记卡";
	
	# PAYSTATION
	$lang['paystation_displayName']		= "PayStation";
	$lang['paystation_f_accountid']		= "PayStation 编号";
	$lang['paystation_f_accountid_d']	= "您的PayStation 编号.";
	$lang['paystation_f_testmode']		= "测试模式";
	$lang['paystation_f_testmode_d']	= "进入测试模式，你的PayStation.";	
	$lang['paystation_f_gatewayid']		= "网关编号";
	$lang['paystation_f_gatewayid_d']	= "你的PayStation 网关编号.";
	$lang['paystation_publicDescription']= "信用卡或借记卡";
	
	# SKRILL
	$lang['skrill_displayName']			= "Skrill (moneybookers)";
	$lang['skrill_f_email']				= "电邮地址";
	$lang['skrill_f_email_d']			= "您的Skrill帐户的电子邮件地址.";
	$lang['skrill_f_testmode']			= "测试模式";
	$lang['skrill_f_testmode_d']		= "进入测试模式，你的Skrill";	
	$lang['skrill_f_testemail']			= "测试电子邮件";
	$lang['skrill_f_testemail_d']		= "你的Skrill测试电子邮件地址.";
	$lang['skrill_publicDescription']	= "信用卡或借记卡";
	$lang['skrill_f_completeOrder']		= "点击这里填写您的订单!";
	
	# CHRONOPAY
	$lang['chronopay_displayName']		= "ChronoPay";
	$lang['chronopay_f_clientid']		= "客户端ID";
	$lang['chronopay_f_clientid_d']		= "你的ChronoPay客户端编号.";
	$lang['chronopay_f_siteid']			= "网站ID";
	$lang['chronopay_f_siteid_d']		= "你的ChronoPay网站编号.";
	$lang['chronopay_f_productid']		= "产品编号";
	$lang['chronopay_f_productid_d']	= "你的ChronoPay产品编号.";
	$lang['chronopay_publicDescription']= "信用卡或借记卡";
	
	# IDEAL
	$lang['ideal_displayName']			= "iDeal (ING)";
	$lang['ideal_f_accountid']			= "帐户编号";
	$lang['ideal_f_accountid_d']		= "你的iDeal帐户编号.";
	$lang['ideal_f_transkey']			= "秘密钥匙";
	$lang['ideal_f_transkey_d']			= "你的iDeal秘密钥匙.";
	$lang['ideal_f_testmode']			= "测试模式";
	$lang['ideal_f_testmode_d']			= "把你的iDeal付款测试模式.";
	$lang['ideal_publicDescription']	= "银行户头";
	
	# PAYPAL
	$lang['paypal_displayName']			= "贝宝(PayPal)";
	$lang['paypal_f_email']				= "电子邮件";
	$lang['paypal_f_email_d']			= "您的PayPal帐户的电子邮件地址.";
	$lang['paypal_f_testmode']			= "贡献者媒体";
	$lang['paypal_f_testmode_d']		= "将您的贝宝（Paypal）支付进入测试模式.";	
	$lang['paypal_f_testemail']			= "Testing Email";
	$lang['paypal_f_testemail_d']		= "您的贝宝（PayPal）的沙箱测试电子邮件地址.";
	$lang['paypal_publicDescription']	= "贝宝(PayPal), 信用卡或银行帐户";
	
	# 2CHECKOUT
	$lang['2checkout_displayName']		= "2Checkout.com";
	$lang['2checkout_f_accountid']		= "户口编号";
	$lang['2checkout_f_accountid_d']	= "你的2Checkout帐户编号.";
	$lang['2checkout_f_testmode']		= "测试模式";
	$lang['2checkout_f_testmode_d']		= "进入测试模式，把你的2Checkout支付.";
	$lang['2checkout_instructions']		= "In 2Checkout.com set direct return to 'Header Redirect (Your URL)' and set approved URL to http://www.YOUR_DOMAIN_NAME.com/assets/gateways/2checkout/ipn.php";
	$lang['2checkout_publicDescription']= "信用卡或美国银行账户";

	
	# Plug n' Pay
	$lang['plugnpay_displayName']		= "随插即支付";
	$lang['plugnpay_f_accountid']		= "户口编号";
	$lang['plugnpay_f_accountid_d']		= "你的插件与付费帐户编号.";
	$lang['plugnpay_publicDescription']	= "";
	
	# AUTHORIZE.NET
	$lang['authorize_displayName']		= "Authorize.net";
	$lang['authorize_f_apiid']			= "API 登录编号";
	$lang['authorize_f_apiid_d']		= "你的Authorize.net API 登录编号.";
	$lang['authorize_f_transkey']		= "交易键";
	$lang['authorize_f_transkey_d']		= "你的Authorize.net交易键.";
	$lang['authorize_f_testmode']		= "测试模式";
	$lang['authorize_f_testmode_d']		= "进入测试模式，用你的Authorize.net支付.";
	$lang['authorize_publicDescription']= "";
	$lang['authorize_f_completeOrder'] = "点击这里填写您的订单!";
	
	# MYGATE.CO.ZA
	$lang['mygate_displayName']			= "MyGate.co.za";
	$lang['mygate_f_merchantid']		= "商家编号";
	$lang['mygate_f_merchantid_d']		= "你的MyGate.co.za 商家编号.";
	$lang['mygate_f_appid']				= "申请ID";
	$lang['mygate_f_appid_d']			= "你的MyGate.co.za 应用程序编号.";
	$lang['mygate_publicDescription']	= "公众说明";
	$lang['mygate_f_testmode']			= "测试模式";
	$lang['mygate_f_testmode_d']		= "进入测试模式，你的Mygate付款.";	
	
	# MAIL IN PAYMENT
	$lang['mailin_displayName']			= "在付款邮件";
	$lang['mailin_f_instructions']		= "邮件付款说明";
	$lang['mailin_f_instructions_d']	= "说明客户发送邮件，如支票或汇票付款。";
	$lang['mailin_publicDescription']	= "邮寄您的付款予下页所提供的地址.";
?>