<?php
	# 4.4.6
	$wplang['stats_visitors']		= "Approximate Site Visits";
	
	# 4.0.7	
	$wplang['sitehealth_exif']	= "PHP EXIF支援 (exif_read_data) PHP EXIF Support (exif_read_data)";
	
	# GENERAL WIDGET LANGUAGE
	$wplang['widget_close']		= "关";
	$wplang['widget_save']		= "储存";
	$wplang['load_failed'] 		= "装载板失败!";
	$wplang['widget_title']		= "标题";
	$wplang['widget_note']		= "记录";
	$wplang['widget_for']		= "为";
	
	# NOTES WIDGET
	$wplang['notes_title']		= "记录";
	$wplang['notes_newnote']	= "新记录"; 
	$wplang['notes_postedby']	= "贴出由"; 
	$wplang['notes_lastupdate']	= "最后更新"; 
	$wplang['notes_nonotes']	= "这里没有任何的记录显示"; 
	
	# EXTRAS WIDGET
	$wplang['extras_title']		= "额外的照片储存库"; 
	
	# KTOOLS ACCOUNT WIDGET
	$wplang['kaccount_title']	= "K工具 帐户"; 
	$wplang['kaccount_support']	= "支援/升级 每天提醒"; 
	$wplang['kaccount_messages']	= "未读留言"; 
	$wplang['kaccount_affil']	= "加上自上次登录的销售"; 
	
	# KNEWS WIDGET
	$wplang['knews_title']		= "Ktools.net 消息";
	
	# QUICK STATS WIDGET - NEW AND PENDING
	$wplang['qstats_title']		= "新和未审核."; 
	$wplang['qstats_logmem']	= "自上次登录的会员"; 
	$wplang['qstats_penmem']	= "未审核会员"; 
	$wplang['qstats_logorders']	= "自上次登录的订单"; 
	$wplang['qstats_penorders']	= "未审核订单"; 
	$wplang['qstats_logcomm']	= "自上次登录的意见"; 
	$wplang['qstats_pencomm']	= "未审核的意见"; 
	$wplang['qstats_logtags']	= "自上次登录的标签"; 
	$wplang['qstats_pentags']	= "未审核标签"; 
	$wplang['qstats_lograte']	= "自上次登录的评级"; 
	$wplang['qstats_penrate']	= "未审核评级"; 
	$wplang['qstats_penbios']	= "未审核的会员动态时报"; 
	$wplang['qstats_penavatars']	= "未审核的头像"; 
	$wplang['qstats_pensupport']	= "待支持门票";
	
	# STATS WIDGET
	$wplang['stats_title']		= "统计资料"; 
	$wplang['stats_op1']		= "销售"; 
	$wplang['stats_op2']		= "会员"; 
	$wplang['stats_op_7days']	= "最近7天"; 
	$wplang['stats_op_6mon']	= "最近6个月"; 
	$wplang['stats_op_5year']	= "最近5年"; 
	$wplang['stats_tdsales']	= "总销售"; 
	$wplang['stats_tdorders']	= "总订单"; 
	$wplang['stats_sales']		= "销售"; 
	$wplang['stats_orders']		= "订购"; 
	$wplang['stats_atsales']	= "全天销售"; 
	$wplang['stats_atorders']	= "全天订单"; 
	$wplang['stats_tdmems']		= "今天新会员"; 
	$wplang['stats_mems']		= "新会员"; 
	$wplang['stats_tamems']		= "总活跃会员"; 
	$wplang['stats_timems']		= "总不活跃会员"; 
	
	
	# SITE HEALTH WIDGET
	$wplang['sitehealth_title']	= "网站服务器健康"; 
	$wplang['sitehealth_ok']	= "OK";
	$wplang['sitehealth_low']	= "低"; 
	$wplang['sitehealth_high']	= "高"; 
	$wplang['sitehealth_failed']	= "失败"; 
	$wplang['sitehealth_unava'] 	= "不可用"; 
	$wplang['sitehealth_off']	= "关"; 
	$wplang['sitehealth_on']	= "开"; 
	$wplang['sitehealth_inst']	= "安装"; 
	$wplang['sitehealth_none']	= "无"; 
	$wplang['sitehealth_exists']	= "存在"; 
	$wplang['sitehealth_write']	= "Writable";
	$wplang['sitehealth_nonwri']	= "Not Writable";
	$wplang['sitehealth_php']	= "PHP 版本";  
	$wplang['sitehealth_gd']	= "GD 图书馆";  
	$wplang['sitehealth_mem']	= "PHP内存限制"; 
	$wplang['sitehealth_exe']	= "PHP 最大_执行_时间";  
	$wplang['sitehealth_time']	= "PHP 最大_输入_时间";  
	$wplang['sitehealth_file']	= "PHP 上载_最大_档案尺寸"; ;
	$wplang['sitehealth_post']	= "PHP post_max_size";
	$wplang['sitehealth_safe']	= "PHP安全_模式"; 
	$wplang['sitehealth_dbv']	= "数据库/产品版本检查"; 
	$wplang['sitehealth_mysqlv']	= "MySQL 版本";  
	$wplang['sitehealth_load']	= "服务器负载"; 
	$wplang['sitehealth_upti']	= "服务器正常运行时间"; 

	# UPDATE CHECK WIDGET
	$wplang['updater_title']	= "检查更新"; 
	$wplang['updater_newest']	= "你有新版本"; 
	$wplang['updater_update']	= "更新可用"; 
	$wplang['updater_newestis']	= "新版本是"; 
	$wplang['updater_getnew']	= "最新版本请登入 <a href='http://www.ktools.net/members/' target='_blank'>Ktools.net account</a>";
	$wplang['updater_yourv']	= "您正在执行的版本"; 
	
	# BLANK WIDGET
	$wplang['blank_title']		= "空白方格"; 
	
	# CALENDAR WIDGET
	$wplang['calendar_title']	= "日历"; 
	
	# TIPS CHECK WIDGET
	//$wplang['tips_title']		= "提示"; 
	//$wplang['tips_next']		= "下一个提示"; 
	//$welcometip[] 				= "您可以使用<strong>Ctrl+Shift+S</strong>来开及关的捷径程序命名表?";   
	//$welcometip[] 				= "您可以将这些的欢迎面板的重新安排他们的方式，你想。只要点击标题，然后拖动。然后删除该面板的顺序."; 
	//$welcometip[] 				= "为了快速访问到您的陈列室和许多其他领域，单击该选项卡的左上角的箭头。这将打开快捷菜单。再次单击该选项卡关闭.";    
	
?>