<?php

	# PORTUGUES
	$langset['version'] = "4.5";		// Version number this language file is translated for
	$langset['active'] = "1";			// Allow the language to be selected in the management area
	$langset['translatedBy'] = 'Ktools Member - welderalves';			// Method or person who translated the language
	$langset['mgmtAreaTrans'] = "1";			// Management area also translated
	$langset['lang_charset'] = "utf-8";		// The required character set for your language text. //iso-8859-1
	$langset['id'] = "portugues_brasil"; 	// Language ID - MUST MATCH THE DIRECORY NAME EXACTLY - NO SPACES - ALL LOWERCASE
	$langset['locale'] = "pt_BR"; 		// Language Locale
	$langset['xmlLangCode'] = "pt"; 		// Language Locale
	$langset['name'] = "Portugues Brasil"; 	// Language Name That Gets Displayed
	$langset['date_format'] = "PT"; 		// PT (PT Date 08/17/2012), EURO (European Date 17/08/2012), INT (World Date 2012/08/17)
	$langset['clock_format'] = "24"; 		// 12 or 24 (hours)
?>