<?php
/*Portugues*/
	# 4.4.6
	$wplang['stats_visitors']		= "Approximate Site Visits";
	
	# 4.1.3
	$wplang['qstats_pending_media']	= "Pending Contributor Media";
	
	# 4.0.7
	$wplang['sitehealth_exif'] = "Supporte PHP EXIF  (exif_read_data)";
	# GENERAL WIDGET LANGUAGE
	$wplang['widget_close'] = "Fechar";
	$wplang['widget_save'] = "Salvar";
	$wplang['load_failed'] = "Carregar o painel falhou!";
	$wplang['widget_title'] = "Título";
	$wplang['widget_note'] = "Nota";
	$wplang['widget_for'] = "Para";
	# NOTES WIDGET
	$wplang['notes_title'] = "Notas";
	$wplang['notes_newnote'] = "Nova Nota";
	$wplang['notes_postedby'] = "Colocado por";
	$wplang['notes_lastupdate'] = "Última atualização";
	$wplang['notes_nonotes'] = "Não existem notas para mostrar";
	# EXTRAS WIDGET
	$wplang['extras_title'] = "Extras ktools";
	# KTOOLS ACCOUNT WIDGET
	$wplang['kaccount_title'] = "Conta Ktools";
	$wplang['kaccount_support'] = "Supporte/Atualizar - Dias em falta";
	$wplang['kaccount_messages'] = "Mensagens não lidas";
	$wplang['kaccount_affil'] = "Vendas de afiliados desde a última entrada";
	# KNEWS WIDGET
	$wplang['knews_title'] = "Noticias ktools";
	# QUICK STATS WIDGET - NEW AND PENDING
	$wplang['qstats_title']	= "Novos & Pendentes";
	$wplang['qstats_logmem'] = "Usuários desde a ultima entrada";
	$wplang['qstats_penmem'] = "Usuários pendentes";
	$wplang['qstats_logorders'] = "Encomendas desde a última entrada";
	$wplang['qstats_penorders'] = "Encomendas pendentes";
	$wplang['qstats_logcomm'] = "Comentários desde a última entrada";
	$wplang['qstats_pencomm'] = "Comentários pendentes";
	$wplang['qstats_logtags'] = "Etiquetas desde a última entrada";
	$wplang['qstats_pentags'] = "Etiquetas pendentes";
	$wplang['qstats_lograte'] = "Avaliações desde a última entrada";
	$wplang['qstats_penrate'] = "Avaliações pendentes";
	$wplang['qstats_penbios'] = "Usuários Bios pendentes";
	$wplang['qstats_penavatars']= "Avatars pendentes";
	$wplang['qstats_pensupport']= "Senhas de suporte pendentes";
	# STATS WIDGET
	$wplang['stats_title'] = "Stats";
	$wplang['stats_op1'] = "Vendas";
	$wplang['stats_op2'] = "Usuários";
	$wplang['stats_op_7days'] = "Últimos 7 Dias";
	$wplang['stats_op_6mon'] = "Últimos 6 Meses";
	$wplang['stats_op_5year'] = "Últimos 5 Anos";
	$wplang['stats_tdsales'] = "Vendas hoje";
	$wplang['stats_tdorders'] = "Encomendas hoje";
	$wplang['stats_sales'] = "Vendas";
	$wplang['stats_orders'] = "Encomendas";
	$wplang['stats_atsales'] = "Vendas até hoje";
	$wplang['stats_atorders'] = "Encomendas até hoje";
	$wplang['stats_tdmems'] = "Novos Usuários hoje";
	$wplang['stats_mems'] = "Novos Usuários";
	$wplang['stats_tamems'] = "Total de Usuários ativos";
	$wplang['stats_timems'] = "Total de Usuários inativos";
	# SITE HEALTH WIDGET
	$wplang['sitehealth_title'] = "Status do Site & Servidor";
	$wplang['sitehealth_ok'] = "OK";
	$wplang['sitehealth_low'] = "BAIXO";
	$wplang['sitehealth_high'] = "ALTO";
	$wplang['sitehealth_failed'] = "FALHOU";
	$wplang['sitehealth_unava'] = "Não existente";
	$wplang['sitehealth_off'] = "OFF";
	$wplang['sitehealth_on'] = "ON";
	$wplang['sitehealth_inst'] = "Instalado";
	$wplang['sitehealth_none'] = "Não";
	$wplang['sitehealth_exists']= "Existentes";
	$wplang['sitehealth_write'] = "Tem permissão de escrita";
	$wplang['sitehealth_nonwri'] = "Sem permissão de escrita";
	$wplang['sitehealth_php'] = "Versão PHP";
	$wplang['sitehealth_gd'] = "Livraria GD";
	$wplang['sitehealth_mem'] = "limite de memória PHP";
	$wplang['sitehealth_exe'] = "PHP max_execution_time";
	$wplang['sitehealth_time'] = "PHP max_input_time";
	$wplang['sitehealth_file'] = "PHP upload_max_filesize";
	$wplang['sitehealth_post'] = "PHP post_max_size";
	$wplang['sitehealth_safe'] = "PHP safe_mode";
	$wplang['sitehealth_dbv'] = "Base de Dados/Verificar versão do produto";
	$wplang['sitehealth_mysqlv'] = "MySQL Version";
	$wplang['sitehealth_load'] = "Server Load";
	$wplang['sitehealth_upti'] = "Tempo do servidor";
	# UPDATE CHECK WIDGET
	$wplang['updater_title'] = "Procurar atualizações";
	$wplang['updater_newest'] = "Tem a nova versão";
	$wplang['updater_update'] = "Existe atualização";
	$wplang['updater_newestis'] = "A nova versão é";
	$wplang['updater_getnew'] = "Para obter uma nova versão, entre na sua conta <a href='http://www.ktools.net/members/' target='_blank'>Ktools.net account</a>";
	$wplang['updater_yourv'] = "Possui a nova versão";
	# BLANK WIDGET
	$wplang['blank_title'] = "Painel Branco";
	# CALENDAR WIDGET
	$wplang['calendar_title'] = "Calendário";
	# TIPS CHECK WIDGET
	//$wplang['tips_title']		= "Tipos";
	//$wplang['tips_next']		= "próxima dica";
	//$welcometip[] 				= "Sabia que você pode usar <strong> Ctrl + Shift + S </ strong> para abrir e fechar o menu de atalhos?";
	//$welcometip[] 				= "Você pode arrastar os painéis de boas-vindas para reorganizá-las da maneira que você goste. Basta clicar no título e arrastar. Em seguida, solte o painel quando é na ordem que você gostaria.";
	//$welcometip[] 				= "Para acesso rápido às suas galerias e muitas outras áreas, clique na guia com a seta no canto superior esquerdo. Isto irá abrir o menu de atalhos. Clique na guia novamente para fechá-lo.";



?>