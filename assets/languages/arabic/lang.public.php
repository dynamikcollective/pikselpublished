<?php
	/******************************************************************
	*  ARABIC
	******************************************************************/
	
	# 4.7
	$lang['featuredGalleries']		= "Featured Galleries";
	
	# 4.6.3	
	$lang['ccMessage']				= "Please enter your credit card information below.";
	$lang['ccNumber']				= "Card Number";
	$lang['ccCVC']					= "CVC";
	$lang['ccExpiration']			= "Expiration (MM/YYYY)";
	
	# 4.6.1
	$lang['passwordLeaveBlank']		= "Leave blank for no password";
	$lang['myAlbums']				= "My Albums";
	
	# 4.5
	$lang['dateDownloadUpper']		= "DATE DOWNLOADED";
	$lang['downloadTypeUpper']		= "DOWNLOAD TYPE";
	$lang['rss']					= "RSS";
	
	# 4.4.8
	$lang['uploadFile']				= "Upload File";
	
	# 4.4.7
	$lang['contrSmallFileSize']		= "The file size is too small. The file must be at least";
	# plupload
	$lang['plupSelectFiles']		=  "Select files";
	$lang['plupAddFilesToQueue']	=  "Add files to the upload queue and click the start button.";
	$lang['plupFilename']			=  "Filename";
	$lang['plupStatus']				=  "Status";
	$lang['plupSize']				=  "Size";
	$lang['plupAddFiles']			=  "Add files";
	$lang['plupStartUplaod']		=  "Start upload";
	$lang['plupStopUpload']			=  "Stop current upload";
	$lang['plupStartQueue']			=  "Start uploading queue";
	$lang['plupDragFilesHere']		=  "Drag files here.";
	
	# 4.4.6
	$lang['batchUploader']			= "Batch Uploader";
	$lang['uploader'][1]			= "Java Based";
	$lang['uploader'][2]			= "HTML5/Flash Based";
	$lang['change']					= "Change";
	
	# 4.4.5
	$lang['moreInfoPlus']				= "[More Info]";
	
	# 4.4.3
	$lang['mediaLabelPropRelease']	= "Property Release";
	
	# 4.4.2
	$lang['go']						= "GO";
	
	# 4.4.0
	$lang['share'] 					= "Share";
	$lang['bbcode'] 				= "BBCode";
	$lang['html'] 					= "HTML";
	$lang['link']					= "Link";
	$lang['pricingCalculator']		= "Pricing Calculator";
	$lang['noOptionsAvailable']		= "No options available";
	$lang['viewCartUpper']			= "VIEW CART";
		
	# 4.3
	$lang['vatIDNumber']			= "VAT ID Number";
	
	# 4.2.1
	$lang['captchaError']			= "The captcha entered was not correct. Your message was not sent.";
	
	# 4.1.7
	$lang['mediaLicenseEU'] 		= "Editorial Use";
	
	# 4.1.6
	$lang['yourBill'] 				= "Your Bill";
	$lang['invoiceNumber'] 			= "Invoice Number";
	$lang['paymentThanks'] 			= "Thank you for your payment.";
	$lang['msActive'] 				= "Your membership is now active.";
	$lang['today'] 					= "Today";
	$lang['downloadsRemainingB']	= "Downloads Remaining";
	
	# 4.1.4
	$lang['link'] 					= "Link";
	$lang['optional'] 				= "Optional";
	
	# 4.1.3
	$lang['keywordRelevancy'] 		= "Keyword Relevancy";
	$lang['mediaLicenseEX'] 		= "Extended License";
	
	# 4.1.1
	$lang['editMediaDetails'] 		= "Edit Media Details";
	$lang['searchResults'] 			= "Search Results";
	$lang['width'] 					= "Width";
	$lang['height'] 				= "Height";
	$lang['hd'] 					= "HD";
	$lang['mediaProfile'] 			= "Profile";
	$lang['photo'] 					= "Photo";
	$lang['video'] 					= "Video";
	$lang['other'] 					= "Other";
	$lang['thumbnail'] 				= "Thumbnail";
	$lang['videoSample'] 			= "Video Sample";
	$lang['attachFile'] 			= "Attach File";
	$lang['fileAttached'] 			= "File Attached";
	$lang['attachMessage'] 			= "Please select a file to attach for this profile";
	$lang['browse'] 				= "Browse";
	$lang['uploadThumb'] 			= "Upload Thumbnail";
	$lang['uploadVideo'] 			= "Upload Video Preview";
	
	# 4.1
	$lang['importSuccessMes'] 		= "Imported successfully!";
	$lang['forgotPassword'] 		= "Forgot Password?";
	$lang['passwordSent'] 			= "Email was sent, please check your email inbox or spam folder for the message.";
	$lang['passwordFailed'] 		= "Sorry but that membership does not exists, please try to enter your email again.";
	$lang['photoProfiles']			= "Photo Profiles";
	$lang['videoProfiles']			= "Video Profiles";
	$lang['otherProfiles']			= "Other Profiles";
	$lang['saving']					= "Saving";
	$lang['myAccount']				= "My Account";
	$lang['myGalleries']			= "My Galleries";
	$lang['noMediaAlbum']			= "There is no meida to display.";
	$lang['editDetails']			= "Edit Details";
	$lang['approvalStatus0']		= "Pending Approval";
	$lang['approvalStatus1']		= "Approved";
	$lang['approvalStatus2']		= "Failed Approval";
	$lang['noDetailsMes']			= "No further details were provided!";	
	$lang['orphanedMedia']			= "Orphaned Media";
	$lang['lastBatch']				= "Last Batch Added";
	$lang['deleteAlbumMes']			= "Delete this album and all media it contains?";
	$lang['mailInMedia']			= "Mail on CD/DVD";
	$lang['deleteAlbum']			= "Delete Album";
	$lang['editAlbum']				= "Edit Album";
	$lang['albumName']				= "Album Name";
	$lang['makePublic']				= "Make Public";
	$lang['deleteMedia']			= "Delete Media";
	$lang['deleteMediaMes']			= "Delete the selected media files?";
	$lang['selectAll']				= "Select All";
	$lang['selectNone']				= "Select None";
	$lang['noImportFilesMessage']	= "There are no files to import!";
	$lang['selectAlbumMes']			= "Select the album you wish to add this media to.";
	$lang['selectGalleriesMes']		= "Select the galleries you wish to add this media to.";
	$lang['chooseItemsMes']			= "Choose the items to sell from the list below.";
	$lang['ablum']					= "Album";
	$lang['pricing']				= "Pricing";
	$lang['px']						= "px";	
	$lang['noSales']				= "You have no sales.";
	$lang['itemUpper']				= "ITEM";
	$lang['commissionUpper']		= "COMMISSION";
	$lang['addUpper']				= "ADD";
	$lang['cmDeleteVerify']			= "Are you sure that you would like to delete this media?";
	$lang['waitingForImport']		= "Media Waiting For Import";
	$lang['importSelectedUpper']	= "IMPORT SELECTED";
	$lang['addMediaDetails']		= "Add Media Details";
	$lang['uploadUpper']			= "UPLOAD";
	$lang['noBioMessage']			= "Currently there is no bio for this member.";
	$lang['collections']			= "Collections";
	$lang['uploadMediaUpper']		= "UPLOAD NEW MEDIA";
	$lang['startUpper']				= "START";
	
	# 4.0.9
	$lang['displayName']			= "العنوان";
	$lang['newAlbum']				= "البوم جديد";
	$lang['albums']					= "الالبومات";
	$lang['viewAllMedia']			= "عرض جميع المرئيات";
	$lang['signUpNow']				= "تسجيل";
	
	# 4.0.8		
	$lang['exactMatch']				= "مطابقة تامة";
	$lang['mediaLabelMediaTypes']	= "انواع المرئيات";
	
	# 4.0.6
	$lang['orderNumUpper']			= "رقم الطلب";
	$lang['orderDateUpper']			= "تاريخ الطلب";
	$lang['paymentUpper']			= "القيمة";	
	
	# 4.0.5
	$lang['dateRange']				= "الفترة الزمنية";
	$lang['resolution']				= "Resolution\ الدقة";
	$lang['continueShopUpper']		= "استمر بعملية التسوق";
	$lang['votes']					= "اصوات";
	$lang['moreNews']				= "المزيد من الاخبار";
	$lang['currentSearch']			= "البحث الحالي";
	$lang['dates']					= "التواريخ";
	$lang['licenseType']			= "نوع الترخيص";
	$lang['searchUpper']			= "بحث";
	$lang['from']					= "من";
	$lang['to']						= "الى";
	$lang['lightboxUpper']			= "المفضلة";
	$lang['itemsUpper']				= "المشتريات";
	$lang['createdUpper']			= "تم الانشاء";
	$lang['na']						= "لا ينطبق"; 
	$lang['galSortCDate']			= "تاريخ الانشاء";
	
	# 4.0.4
	$lang['digitalDownloads']		= "تحميل رقمي للملفات";

	$lang['copyright']				= "حقوق النشر &copy; ".date("Y");
	$lang['reserved']				= "محفوظة";
	
	$lang['days']					= "ايام";
	$lang['weeks']					= "اسابيع";
	$lang['months']					= "اشهر";
	$lang['years']					= "سنوات";
	$lang['weekly']					= "اسبوعيا";
	$lang['monthly']				= "شهريا";
	$lang['quarterly']				= "كل 3 اشهر";
	$lang['semi-annually']			= "كل 6 اشهر";
	$lang['annually']				= "سنوي";	
	$lang['guest']					= "زائر";	
	$lang['login'] 					= "تسجيل الدخول";	
	$lang['loginCaps'] 				= "تسجيل الدخول";
	$lang['loginMessage']			= "الرجاء ادخال البريد الالكتروني وكلمة السر في الاسفل";
	$lang['loggedOutMessage']		= "تمت عملية الخروج من حسابك بنجاح";
	$lang['loginFailedMessage']		= "فشل بعملية الدخول تأكد من كلمة السر او اسم الدخول";
	$lang['accountActivated']		= "تم تفعيل حسابك بنجاح";
	$lang['activationEmail']		= "تم ارسال وصلة التفعيل لبريدك الالكتروني الرجاء الضغط عليها لتفعيل حسابك معنا";
	$lang['loginAccountClosed']		= "هذا الحساب مغلق او لم يتم تفعيله حتى الان";
	$lang['loginPending']			= "هذا الحساب لم يتم تدقيقه بعد ، يجب أن يتحقق مدير الموقع منه قبل ان تتمكن من الدخول";
	$lang['yesLogin']				= "نعم ، اريد الدخول";
	$lang['noCreateAccount']		= "لا ، اريد تسجيل حساب جديد";
	$lang['haveAccountQuestion']	= "هل تملك حساب معنا؟";	
	$lang['collections']			= "مجموعات خاصة";
	$lang['featuredCollections']	= "مجموعات خاصة مختارة";	
	$lang['similarMedia']			= "مواد اخرى مشابهة";	
	$lang['paypal']					= "PayPal";
	$lang['checkMO']				= "Check/Money Order";
	$lang['paypalEmail']			= "PayPal ايميل ال";	
	$lang['paid']					= "تم الدقع";
	$lang['processing']				= "تحت المراجعة";
	$lang['unpaid']					= "غير مدفوعة";
	$lang['refunded']				= "المبلغ المسترجع";
	$lang['failed']					= "فشل";
	$lang['cancelled']				= "ملغي";
	$lang['approved']				= "تمت الموافقة";
	$lang['incomplete']				= "غير مكتمل";
	$lang['billLater']				= "ارسل الفاتورة لاحقا";
	$lang['expired']				= "منتهي الصلاحية";
	$lang['unlimited']				= "غير محدد او الى مالانهاية";
	$lang['quantityAvailable']		= "الكمية المتوفرة";	
	$lang['shipped']				= "تم الشحن او الارسال";
	$lang['notShipped']				= "لم ترسل بعد";
	$lang['backordered']			= "طلب متأخر \ باك اوردر";	
	$lang['taxIncMessage']			= "الضريبة مشمولة";	
	$lang['addTag']					= "اضافة سمة";
	$lang['memberTags']				= "سمات المستخدم";
	$lang['comments']				= "تعليقات";
	$lang['showMoreComments']		= "اظهار جميع التعليقات";
	$lang['noComments']				= "لا يوجد تعليقات منشورة";
	$lang['noTags']					= "لا توجد سمات منشورة";
	$lang['addNewComment']			= "اضف تعليق";
	$lang['commentPosted']			= "تم ارسال التعليق بنجاح";
	$lang['commentPending']			= "تم ارسال تعليقك الجديد بنجاح وستتم مراجعته قريبا";
	$lang['commentError']			= "حدث خطأ بالتالي لم يتم ارسال التعليق للموقع";
	$lang['tagPosted']				= "تم ارسال السمة بنجاح";
	$lang['tagPending']				= "تم ارسال السمة بنجاح وسيتم مراجعتها قريبا";
	$lang['tagError']				= "حدث خطأ بالتالي لم يتم ارسال السمة للموقع";
	$lang['tagDuplicate']			= "هذه السمة مضافة مسبقا";
	$lang['tagNotAccepted']			= "تم رفض هذه السمة من قبل موقعنا";	
	$lang['preferredLang']			= "اللغة المفضلة";
	$lang['dateTime']				= "التاريخ\الوقت";
	$lang['preferredCurrency']		= "العملة المفضلة";	
	$lang['longDate']				= "تاريخ سرياني";
	$lang['shortDate']				= "تاريخ جريجوري";
	$lang['numbDate']				= "عرض رقمي للتاريخ";	
	$lang['daylightSavings']		= "التوقيت الصيفي";
	$lang['dateFormat']				= "طريقة عرض التاريخ";
	$lang['timeZone']				= "منطقة زمنية";
	$lang['dateDisplay']			= "عرض التاريخ";
	$lang['clockFormat']			= "طريقة عرض الزمن";
	$lang['numberDateSep']			= "الفاصل المستخدم بين ارقام التاريخ";	
	$lang['renew']					= "تجديد";	
	$lang['noOrders']				= "لا يوجد طلبات لهذا الاشتراك";
	$lang['noDownloads']			= "هذا الاشتراك لم يقم بتحميل اي ملف";
	$lang['noFeatured'] 			= "لا يوجد عينات منتخبة ضمن هذا القسم";	
	$lang['promotions']				= "عروض";
	$lang['noPromotions']			= "لا يوجد عروض او كوبونات شراء حاليا راجعنا فيما بعد";	
	$lang['autoApply']				= "اضافة تلقائية عند عملية الدفع";
	$lang['useCoupon']				= "استخدم الرمز او الشفرة التالية عند الدفع او اضغط على زر الاضافة";	
	$lang['couponApplied']			= "تم اضافة الخصم او العرض الى سلة مشترياتك بنجاح";
	$lang['couponFailed']			= "العرض او الخصم الذي استخدمته غير متوفر";	
	$lang['couponNeedsLogin']		= "يجب عليك الدخول اولا لأستخدام هذا العرض او الخصم";
	$lang['couponMinumumWarn']		= "اجمالي مبلغ الشراء حتى الان لا يؤهلك لأستخدام هذا العرض او الخصم";
	$lang['couponAlreadyUsed']		= "تستطيع استخدام هذا العرض او الخصم مرة واحدة فقط";
	$lang['couponLoginWarn']		= "يجب عليك الدخول اولا لأستخدام هذا العرض او الخصم";	
	$lang['checkout']				= "الدفع";
	$lang['continue']				= "التالي";	
	$lang['shipTo']					= "البضاعة الى";
	$lang['billTo']					= "الفاتورة الى";
	//$lang['mailTo']					= "ارسل الفاتورة الى البريد الالكتروني التالي";	
	$lang['galleries']				= "البومات";
	$lang['chooseGallery']			= "اختر احدى الالبومات في الاسفل لعرض محتواها";
	$lang['galleryLogin']			= "الرجاء ادخال كلمة السر الخاصة بالالبوم لعرض محتوياته";
	$lang['galleryWrongPass']		= "كلمة السر التي ادخلتها لهذا الالبوم غير صحيحة";	
	$lang['newestMedia']			= "اخر ما نشرنا";
	$lang['popularMedia']			= "مواد وصور مشهورة بين زوارنا";	
	$lang['contributors']			= "المساهمين";
	$lang['contUploadNewMedia']		= "اضف مادة او صورة جديدة";
	$lang['contViewSales']			= "عرض المبيعات";
	$lang['contPortfolio']			= "المحفظة الشخصية";
	$lang['contGalleries']			= "البوماتي";
	$lang['contMedia']				= "المواد والصور الخاصة بي";	
	$lang['aboutUs']				= "عن الموقع";	
	$lang['news']					= "اخبار";
	$lang['noNews']					= "لا يوجد تقارير خبرية حاليا";	
	$lang['termsOfUse']				= "شروط الاستخدام";
	$lang['privacyPolicy']			= "خصوصية المستخدم";
	$lang['purchaseAgreement']		= "عقد الشراء";	
	$lang['iAgree']					= "اوافق على";	
	$lang['createAccount']			= "تسجيل حساب جديد";
	$lang['createAccountMessage']	= "الرجاء مليء المعلومات المطلوبة في الاسفل لعمل اشتراك جديد";	
	$lang['contactUs']				= "اتصل بنا";
	$lang['contactMessage']			= "شكرا على اتصالك بنا ، سنقوم بالرد قريبا";
	$lang['contactError']			= "البيانات لم تمليء بشكل صحيح وكامل بالتالي لم يتم ارسال الرسالة";
	$lang['contactIntro']			= "الرجاء استخدام الحقول في الاسفل للاتصال بنا وسنرد قريبا";
	$lang['contactEmailSubject']	= "السؤال من حقل الاتصال";
	$lang['contactFromName']		= "حقل الاتصال";	
	$lang['prints']					= "مطبوعات";
	$lang['featuredPrints']			= "مطبوعات مختارة";	
	$lang['newBill']				= "تم اعداد فاتورة جديدة لهذا الاشتراك. هذا الاشتراك لم يكون مفعلا حتى تسديدها علما أن كل الفواتير المؤجلة السابقة -ان وجدت- قد تم الغائها";
	$lang['accountInfo']			= "بيانات الاشتراك";
	$lang['accountUpdated']			= "تم تحديث بيانات الاشتراك الخاص بك";
	$lang['editAccountInfo']		= "تعديل بيانات الاشتراك";
	$lang['editAccountInfoMes']		= "عدل بيانات اشتراكك ثم اضغط حفظ";	
	$lang['accountInfoError1']		= "كلمتا السر غير متطابقتين";
	$lang['accountInfoError5']		= "كلمة السر التي ادخلتها غير صحيحة";
	$lang['accountInfoError2']		= "يجب أن لا تقل كلمة السر عن 6 خانات";
	$lang['accountInfoError12']		= "هذا الايميل مسجل معنا سابقا";
	$lang['accountInfoError13']		= "لم يتم قبول بريدك الالكتروني";
	$lang['accountInfoError3']		= "ادخل الكلمات في الاعلى";
	$lang['accountInfoError4']		= "كلمات الاختبار ادخلت بشكل خطأ";
	$lang['readAgree']				= "قمت بقراءة";	
	$lang['agreements']				= "الاتفاق";	
	$lang['poweredBy']				= "بدعم من";
	$lang['captchaAudio']			= "ادخل الارقام التي تسمعها";
	$lang['captchaIncorrect']		= "خطأ ، الرجاء المحاولة مرة اخرى";
	$lang['captchaPlayAgain']		= "شغل مرة اخرى";
	$lang['captchaDownloadMP3']		= "تحميل على شكل ام بي ثري";
	$lang['captcha']				= "اختبار عقلي بسيط";
	$lang['subHeaderID']			= "ID";
	$lang['subHeaderSubscript']		= "الاشتراك";
	$lang['subHeaderExpires']		= "الانتهاء";
	$lang['subHeaderDPD']			= "تحميل في كل يوم\ المتبقي";
	$lang['subHeaderStatus']		= "الحالة";
	$lang['downloads']				= "تحميل";	
	$lang['thankRequest']			= "شكرا لطلبك ، سنتصل بك قريبا";
	$lang['pleaseContactForm']		= "نرجوا الاتصال بنا للحصول على الاسعار والعروض الخاصة بهذه المادة عن طريق مليء البيانات التالية";
	$lang['downWithSubscription']	= "تحميل الملف اعتمادا على نوع اشتراكك";	
	$lang['noInstantDownload'] 		= "هذا الملف غير متوفر للتحميل المباشر ، سيتم ارساله عن طريق بريد الكتروني من قبل مدير الموقع عند الطلب او الشراء";
	
	// Tickets
	$lang['newTicketsMessage']		= "رسائل طلب دعم جديدة او محدثة";
	$lang['emptyTicket']			= "رسالة طلب الدعم هذه فارغة";
	$lang['messageID']				= "الخاص بطلب الدعم IDال";
	$lang['ticketNoReplies']		= "تم اغلاق رسالة الدعم هذه ، الردود الجديدة لن تقبل الان";
	$lang['ticketUpdated']			= "تم اضافة تحديث على طلب الدعم هذا";
	$lang['ticketClosed']			= "تم اغلاق طلب الدعم هذا";
	$lang['closeTicket']			= "اغلق طلب الدعم";
	$lang['newTicket']				= "رسالة طلب دعم جديدة";
	$lang['newTicketButton']		= "رسالة طلب دعم جديدة";
	$lang['ticketUnreadMes']		= "رسالة طلب الدعم تحوي ردود جديدة او غير مقروءة";
	$lang['ticketUnderAccount']		= "لا يوجد رسائل طلب دعم لهذا الاشتراك";	
	$lang['ticketSubmitted']		= "تم استلام رسالة طلب الدعم وسنرد عليك قريبا";
	
	$lang['mediaNotElidgiblePack']	= "هذه المادة غير مناسبة للاضافة مع اي مجموعة";
	$lang['noBills']				= "لا يوجد فواتير لهذا الاشتراك";	
	$lang['lightbox']				= "القوائم المفضلة";
	$lang['noLightboxes']			= "لا يوجد قوائم مفضلة للعرض";
	$lang['lightboxDeleted']		= "تم حذف القائمة";
	$lang['lbDeleteVerify']			= "هل انت متأكد من حذف القائمة المفضلة هذه؟";
	$lang['newLightbox']			= "مفضلة جديدة";
	$lang['lightboxCreated']		= "تم انشاء قائمة مفضلة جديدة";
	$lang['addToLightbox']			= "اضف الى المفضلة";
	$lang['createNewLightbox']		= "انشاء قائمة مفضلة جديدة";
	$lang['editLightbox']			= "تعديل المفضلة";
	$lang['addNotes']				= "اضافة ملاحظة";
	$lang['editLightboxItem']		= "تعديل عنصر في المفضلة";
	$lang['removeFromLightbox']		= "حذف من المفضلة";	
	$lang['savedChangesMessage']	= "تم حفظ التغييرات";	
	$lang['noSubs']					= "لا يوجد مشتركين لهذا الحساب";	
	$lang['unpaidBills']			= "فواتير غير مدفوعة";	
	$lang['notices']				= "ملاحظات";	
	$lang['subscription']			= "اشتراك";	
	$lang['assignToPackage']		= "اضف الى حزمة";
	$lang['startNewPackage']		= "ابدأ حزمة جديدة";
	$lang['packagesInCart']			= "الحزم في سلة التسوق";	
	$lang['id']						= "ID";
	$lang['summary']				= "خلاصة";
	$lang['status']					= "الحالة";
	$lang['lastUpdated']			= "اخر تحديث";
	$lang['opened']					= "فتح";
	$lang['reply']					= "رد";	
	$lang['required']				= "مطلوب";	
	$lang['bills']					= "فواتير";
	$lang['orders']					= "طلبات";
	$lang['downloadHistory']		= "تحميلات سابقة";
	$lang['supportTickets']			= "طلبات الدعم التقني";
	$lang['order']					= "طلب";	
	$lang['packages']				= "حزم";
	$lang['featuredPackages']		= "حزم مختارة";	
	$lang['products']				= "منتج";
	$lang['featuredProducts']		= "منتجات مختارة";	
	$lang['subscriptions']			= "اشتراكات";
	$lang['featuredSubscriptions']	= "اشتراكات مختارة";	
	$lang['yourCredits']			= "رصيدك<br />الائتماني";
	$lang['featuredCredits']		= "حزم ائتمان مختارة";	
	$lang['media'] 					= "مادة";
	$lang['mediaNav'] 				= "مادة";
	$lang['featuredMedia']			= "مادة مختارة";	
	$lang['featuredItems'] 			= "مختارات";	
	$lang['showcasedContributors'] 	= "نماذج من المساهمين";	
	$lang['galleryLogin']			= "دخول للمعرض";	
	$lang['forum'] 					= "منتدى";
	$lang['randomMedia'] 			= "مادة عشوائية";
	$lang['language'] 				= "اللغة";	
	$lang['priceCreditSep']			= "او";	
	$lang['viewCollection']			= "عرض المجموعة";	
	$lang['loggedInAs']				= "تم تسجيل الدخول باسم";	
	$lang['editProfile']			= "تعديل الحساب";	
	$lang['noItemCartWarning']		= "يجب أن تختار صورة قبل ان تتمكن من الاضافة الى سلة التسوق";
	$lang['cartTotalListWarning']	= "لاحظ ان المبلغ الاجمالي مقدر وقد يتغير بشكل طفيف عند الدفع بحالة وجود تغيير عملة";
	$lang['applyCouponCode']		= "اضافة خصم";
	$lang['billMeLater']			= "ارسل الفاتورة لاحقا";
	$lang['billMeLaterDescription']	= "سنرسل لك فواتير شهرية لمشترياتك";
	$lang['shippingOptions']		= "خيارات الشحن او الارسال";
	$lang['paymentOptions']			= "خيارات الدفع";
	$lang['chooseCountryFirst']		= "اختر الدولة اولا";
	$lang['paymentCancelled']		= "تم الغاء محاولة الدفع لكن يمكنك المحاولة مجددا"; 
	$lang['paymentDeclined']		= "تم رفض محاولة الدفع لكن يمكنك المحاولة مجددا"; 		
	$lang['generalInfo']			= "معلومات شخصية";
	$lang['membership']				= "الاشتراك";
	$lang['preferences']			= "الخصائص المتاحة";
	$lang['address']				= "العنوان";
	$lang['actions']				= "الخصائص المتاحة";
	$lang['changePass']				= "تغيير كلمة السر";
	$lang['changeAvatar']			= "تغيير الصورة الشخصية";
	$lang['bio']					= "السيرة الذاتية";
	$lang['contributorSettings']	= "خيارات المساهمين";
	$lang['edit']					= "تعديل";	
	$lang['leftToFill']				= "خانات لازالت بحاجة للمليء";	
	$lang['commissionMethod']		= "ادواة دفع العمولة"; 
	$lang['commission']				= "العمولة";	
	$lang['signupDate']				= "عضو منذ";
	$lang['lastLogin']				= "اخر دخول";	
	$lang['clientName']				= "اسم الزبون";
	$lang['eventCode']				= "شفرة المناسبة";
	$lang['eventDate']				= "تاريخ المناسبة";
	$lang['eventLocation']			= "مكان المناسبة";	
	$lang['viewPackOptions']		= "عرض محتويات وخيارات الحزمة";
	$lang['viewOptions']			= "عرض الخيارات";	
	$lang['remove']					= "حذف";
	$lang['productShots']			= "صور المواد المباعة";
	$lang['currentPass']			= "كلمة السر الحالية";
	$lang['newPass']				= "كلمة سر جديدة";
	$lang['vNewPass']				= "تاكيد كلمة السر الجديدة";
	$lang['verifyPass']				= "تاكيد كلمة السر";
	$lang['firstName']				= "الاسم الاول";
	$lang['lastName']				= "اسم العائلة";
	$lang['location']				= "المكان";
	$lang['memberSince']			= "عضو منذ";
	$lang['portfolio']				= "نماذج من اعمال سابقة";
	$lang['profile']				= "الملف الشخصي";
	$lang['address']				= "العنوان";
	$lang['city']					= "المدينة";
	$lang['state']					= "المقاطعة او الولاية";
	$lang['zip']					= "الرمز البريدي";
	$lang['country']				= "الدولة";
	$lang['save'] 					= "حفظ";
	$lang['add'] 					= "اضف";
	$lang['companyName']			= "اسم الشركة";
	$lang['accountStatus']			= "حالة الحساب";
	$lang['website']				= "الموقع الالكتروني";
	$lang['phone']					= "التلفون";
	$lang['email'] 					= "البريد الالكتروني";
	$lang['name'] 					= "العنوان";
	$lang['submit'] 				= "ارسل";	
	$lang['message'] 				= "الرسالة";	
	$lang['question']				= "سبب الاتصال";
	$lang['password'] 				= "كلمة السر";	
	$lang['members'] 				= "اعضاء";
	$lang['visits'] 				= "عدد الزوار";	
	$lang['logout'] 				= "تسجيل خروج";
	$lang['lightboxes']				= "القوائم المفضلة";
	$lang['cart']					= "سلة المشتريات";
	$lang['cartItemAdded']			= "تمت الاضافة الى سلة المشتريات";
	$lang['includesTax']			= "السعر يشمل الضرائب";
	$lang['addToCart']				= "اضف الى سلة المشتريات";
	$lang['items']					= "المشتريات";
	$lang['item']					= "المشتريات";
	$lang['qty']					= "العدد";
	$lang['price']					= "السعر";
	$lang['more']					= "المزيد";
	$lang['moreInfo']				= "قراءة المزيد";
	$lang['back']					= "رجوع";
	$lang['none']					= "لا يوجد";
	$lang['details']				= "التفاصيل";
	$lang['options']				= "الخيارات";
	$lang['page']					= "الصفحة";
	$lang['ratingSubmitted']		= "تم ارسال التصويت";
	$lang['noMedia']				= "فارغة";
	$lang['itemsTotal']				= "عدد المشتريات الكلي";
	$lang['of']						= "ل";
	$lang['view']					= "عرض";
	$lang['apply']					= "ارسل";
	$lang['currency']				= "تغيير العملة";
	$lang['active']					= "مفعل";
	$lang['pending']				= "بانتظار المراجعة";
	$lang['freeTrial']				= "تجربة مجانية";
	$lang['setupFee']				= "سعر خدمة";
	$lang['free']					= "مجاني";
	$lang['open']					= "افتح";
	$lang['close']					= "اغلق";
	$lang['closed']					= "مغلق";
	$lang['by']						= "بواسطة";
	$lang['download']				= "تحميل";
	$lang['downloadUpper']			= "تحميل";
	$lang['KB']						= "K";
	$lang['MB']						= "MB";
	$lang['files']					= "ملفات";
	$lang['unknown']				= "غير معروف";
	$lang['freeDownload']			= "تحميل مجاني";
	$lang['prevDown']				= "تحميل سابق";
	$lang['pay']					= "دفع";
	$lang['purchaseCredits']		= "شراء رصيد ائتماني";
	$lang['purchaseSub']			= "شراء اشتراك";
	$lang['hour'] 					= "ساعة";
	$lang['slash'] 					= "خط مائل";
	$lang['period'] 				= "مدة";
	$lang['dash'] 					= "خط افقي فاصل";
	$lang['gmt'] 					= "GMT";
	$lang['avatar'] 				= "الصورة الشخصية";
	$lang['delete'] 				= "حذف";
	$lang['uploadAvatar']			= "تحميل صورة شخصية";
	$lang['welcome']				= "مرحبا";
	$lang['expires']				= "تنتهي";
	$lang['msExpired']				= "الاشتراك منتهي الصلاحية";
	$lang['newSales']				= "مبيعات جديدة منذ اخر دخول";
	$lang['never']					= "مطلقا";
	$lang['yes']					= "نعم";
	$lang['no']						= "لا";
	$lang['create'] 				= "جديد";
	$lang['cancel'] 				= "الغاء";
	$lang['notes'] 					= "ملاحظات";
	$lang['update'] 				= "تحديث";
	$lang['each'] 					= "كل";
	$lang['enterKeywords']			= "اضف كلمات البحث";
	$lang['send']					= "ارسل";
	$lang['emailTo']				= "بريد الكتروني الى";
	$lang['yourName']				= "اسم المرسل";
	$lang['yourEmail']				= "البريد الالكتروني للمرسل";
	$lang['emailToFriend']			= "ارسل وصلة لهذه المادة";
	$lang['emailToFriendSent']		= "تم ارسال بريد الكتروني ، تستطيع ارسال بريد اخر او اغلاق هذه النافذة";
	$lang['linkSentBy']				= "تم ارسال وصلة الى صورة او فيديو من موقعنا عن طريق";
	$lang['newPackageMessage']		= "ابدأ حزمة جديدة او اختر واحدة موجودة فعلا في سلة التسوق";
	$lang['warning']				= "تنبيه";
	$lang['estimated']				= "مقدرة";
	$lang['doneUpper']				= "تم";
	$lang['returnToSiteUpper']		= "الرجوع للموقع";
	$lang['chooseCountryFirst']		= "اختر دولة اولا";
	$lang['advancedSearch']			= "بحث متطور";
	$lang['eventSearch']			= "بحث عن مناسبة";	
	$lang['AND']					= "و\ AND";
	$lang['OR']						= "\أو OR";
	$lang['NOT']					= "استثناء\ NOT";
	$lang['noAccess']				= "ليس لديك صلاحية لدخول هذا القسم";	
	$lang['siteStats']				= "احصائيات الموقع";
	$lang['membersOnline']			= "عدد الاعضاء المتواجدين الان";
	$lang['minutesAgo']				= "قبل دقائق";
	$lang['seconds']				= "ثواني";	
	$lang['megabytesAbv']			= "ميجا بايت";	
	$lang['downWithSub']			= "تحميل باستخدام الاشتراك";
	$lang['downloadsRemaining']		= "عدد التحميل المسموح به اليوم";
	$lang['requestDownloadSuccess']	= "وصلنا طلبك لهذا الملف وسنتصل بك قريبا";	
	$lang['mediaLicenseNFS']		= "ليس للبيع";
	$lang['mediaLicenseRF']			= "تملك غير مشروط";
	$lang['mediaLicenseRM']			= "تملك مشروط";
	$lang['mediaLicenseFR']			= "مجاني";
	$lang['mediaLicenseCU']			= "اتصل بنا";	
	$lang['original']				= "اصلية";	
	$lang['prevDownloaded']			= "قمت بتحميل هذا الملف سابقا ، لا توجد تكلفة لتحميله مرة اخرى";
	$lang['instantDownload']		= "تحميل فوري";
	$lang['requestDownload']		= "طلب تحميل";
	$lang['request']				= "طلب";
	$lang['enterEmail']				= "ادخل بريدك الالكتروني";	
	$lang['license']				= "الرخصة";
	$lang['inches']					= "بوصة";
	$lang['centimeters']			= "سنتيميتر";
	$lang['dpi']					= "dpi";	
	$lang['noSimilarMediaMessage']	= "لم نجد مواد مشابهة عند البحث";
	$lang['backUpper']				= "رجوع";
	$lang['nextUpper']				= "التالي";
	$lang['prevUpper']				= "السابق";
	$lang['curGalleryOnly']			= "ضمن المعرض الحالي فقط";
	$lang['sortBy']					= "اسلوب الترتيب";
	$lang['galSortColor']			= "اللون";
	$lang['galSortDate']			= "تاريخ الاضافة";
	$lang['galSortID']				= "ID";
	$lang['galSortTitle']			= "العنوان";
	$lang['galSortFilename']		= "اسم الملف";
	$lang['galSortFilesize']		= "حجم الملف";
	$lang['galSortSortNum']			= "رقم الترتيب";
	$lang['galSortBatchID']			= "Batch ID";
	$lang['galSortFeatured']		= "مختارة";
	$lang['galSortWidth']			= "العرض";
	$lang['galSortHeight']			= "الطول";
	$lang['galSortViews']			= "عدد المشاهدات";
	$lang['galSortAsc']				= "تصاعدي";
	$lang['galSortDesc']			= "تنازلي";
	$lang['mediaIncludedInColl']	= "هذا الملف تم تضمينه في المجموعات التالية";	
	$lang['billHeaderInvoice']		= "فاتورة";
	$lang['billHeaderDate']			= "تاريخ الفاتورة";
	$lang['billHeaderDueDate']		= "تاريح الدفع";
	$lang['billHeaderTotal']		= "المجموع";
	$lang['billHeaderStatus']		= "الحالة";
	
	// Cart
	$lang['creditsWarning']			= "ليس لديك رصيد ائتماني كافي للدفع ، الرجاء تسجيل الدخول او اضافة رصيد";
	$lang['subtotalWarning']		= "المبلغ الادنى للشراء وقبول الفاتورة هو";
	$lang['pleaseWait']				= "الرجاء الانتظار بينما نراجع طلبك";
	$lang['billMailinThanks']		= "الرجاء ارسال شيك او حوالة مالية الى العنوان التالي ، وعند استلامه سنحدث هذه الفاتورة ونعتبرها مدفوعة";
	$lang['mailinRef']				= "الرجاء تسجيل الرقم التالي للاشارة الى عملية الدفع هذه";
	$lang['subtotal']				= "المجموع الفرعي";
	$lang['shipping']				= "الشحن";
	$lang['discounts']				= "تخفيضات\عروض";
	$lang['total']					= "المجموع الكلي";
	$lang['creditsSubtotal']		= "المجموع الفرعي للمبلغ الائتماني";
	$lang['creditsDiscounts']		= "تخفيض المبلغ الائتماني";
	$lang['credits']				= "المبلغ الائتماني";
	$lang['reviewOrder']			= "مراجعة الطلب";
	$lang['payment']				= "دفع";
	$lang['cartNoItems']			= "ليس لديك مواد في سلة التسوق";
	$lang['enterShipInfo']			= "الرجاء ادخال معلومات الدفع والشحن في الاسفل";
	$lang['sameAsShipping']			= "نفس عنوان الشحن";
	$lang['differentAddress']		= "ادخل عنوان مختلف";
	$lang['noShipMethod']			= "لايوجد ادوات شحن وتوصيل لهذه المواد";	
	$lang['choosePaymentMethod']	= "الرجاء اختيار اسلوب الدفع";	
	$lang['discountCode']			= "رمز عرض التخفيض";
	$lang['use']					= "استعمل";
	$lang['continueNoAccount']		= "او استمر بدون تسجيل حساب جديد";
	
	
	// Order
	$lang['yourOrder']				= "طلب الشراء الخاص بك";
	$lang['viewInvoice']			= "عرض الطلب";
	$lang['totalsShown']			= "المجاميع الظاهرة في";
	$lang['downloadExpired']		= "انتهاء مدة التحميل";
	$lang['downloadExpiredMes']		= "انتهت مدة التحميل ، اتصل بنا لأعادة التفعيل";
	$lang['downloadMax']			= "تم تجاوز عدد مرات التحميل المسموحة";
	$lang['downloadMaxMes']			= "تم تجاوز عدد مرات التحميل المسموحة لهذا الملف ، اتصل بنا لأعادة التفعيل";
	$lang['downloadNotApproved']	= "طلب لم تتم الموافقة عليه حتى الان";
	$lang['downloadNotApprovedMes']	= "لا تستطيع تحميل هذا الملف حتى يتم الموافقة على طلب الشراء ، امهلنا بعض الوقت";	
	$lang['downloadNotAvail']		= "التحميل الفوري غير متوفر";
	$lang['downloadNotAvailMes']	= "التحميل الفوري غير متوفر لهذا الملف ، استخدم زر الطلب لنرساله لك على البريد الالكتروني";
	$lang['orderNumber']			= "رقم الطلب";
	$lang['orderPlaced']			= "تم ارسال الطلب"; 
	$lang['orderStatus']			= "حالة الطلب";
	$lang['paymentStatus']			= "حالة الدفع";
	
	
	// IPTC
	$lang['iptc']					= "IPTC";
	$lang['iptc_title']				= "العنوان";
	$lang['iptc_description']		= "الوصف";
	$lang['iptc_instructions']		= "التعليمات";
	$lang['iptc_date_created']		= "تاريخ الانشاء";
	$lang['iptc_author']			= "اسم الشخص";
	$lang['iptc_city']				= "المدينة";
	$lang['iptc_state']				= "المقاطعة";
	$lang['iptc_country']			= "الدولة";
	$lang['iptc_job_identifier']	= "مؤشر المهنة";
	$lang['iptc_headline']			= "عنوان";
	$lang['iptc_provider']			= "Creditline"; //Provider
	$lang['iptc_source']			= "المصدر";
	$lang['iptc_description_writer']= "Description Writer";
	$lang['iptc_urgency']			= "مستعجل";
	$lang['iptc_copyright_notice']	= "حقوق النشر";
	
	// EXIF
	$lang['exif']							= "EXIF";
	$lang['exif_FileName']					= "اسم الملف";
	$lang['exif_FileDateTime']				= "File Date/Time";
	$lang['exif_FileSize']					= "حجم الملف";
	$lang['exif_FileType']					= "نوع الملف";
	$lang['exif_MimeType']					= "Mime Type";
	$lang['exif_SectionsFound']				= "الاقسام التي وجدت";
	$lang['exif_ImageDescription']			= "وصف الصورة";
	$lang['exif_Make']						= "الصنع";
	$lang['exif_Model']						= "الموديل";
	$lang['exif_Orientation']				= "Orientation";
	$lang['exif_XResolution']				= "XResolution";
	$lang['exif_YResolution']				= "YResolution";
	$lang['exif_ResolutionUnit']			= "Resolution Unit";
	$lang['exif_Software']					= "Software";
	$lang['exif_DateTime']					= "Date/Time";
	$lang['exif_YCbCrPositioning']			= "YCbCr Positioning";
	$lang['exif_Exif_IFD_Pointer']			= "Exif IFD Pointer";
	$lang['exif_GPS_IFD_Pointer']			= "GPS IFD Pointer";
	$lang['exif_ExposureTime']				= "Exposure Time";
	$lang['exif_FNumber']					= "FNumber";
	$lang['exif_ExposureProgram']			= "Exposure Program";
	$lang['exif_ISOSpeedRatings']			= "ISO Speed Ratings";
	$lang['exif_ExifVersion']				= "Exif Version";
	$lang['exif_DateTimeOriginal']			= "Date/Time Original";
	$lang['exif_DateTimeDigitized']			= "Date/Time Digitized";
	$lang['exif_ComponentsConfiguration']	= "Components Configuration";
	$lang['exif_ShutterSpeedValue']			= "Shutter Speed Value";
	$lang['exif_ApertureValue']				= "Aperture Value";
	$lang['exif_MeteringMode']				= "Metering Mode";
	$lang['exif_Flash']						= "فلاش";
	$lang['exif_FocalLength']				= "Focal Length";
	$lang['exif_FlashPixVersion']			= "Flash Pix Version";
	$lang['exif_ColorSpace']				= "مساحة اللون";
	$lang['exif_ExifImageWidth']			= "Exif عرض الصورة";
	$lang['exif_ExifImageLength']			= "Exif طول الصورة";
	$lang['exif_SensingMethod']				= "اداة الاستشعار";
	$lang['exif_ExposureMode']				= "Exposure Mode";
	$lang['exif_WhiteBalance']				= "White Balance";
	$lang['exif_SceneCaptureType']			= "Scene Capture Type";
	$lang['exif_Sharpness']					= "حدة الصورة";
	$lang['exif_GPSLatitudeRef']			= "GPS Latitude Ref";
	$lang['exif_GPSLatitude_0']				= "GPSLatitude 0";
	$lang['exif_GPSLatitude_1']				= "GPSLatitude 1";
	$lang['exif_GPSLatitude_2']				= "GPSLatitude 2";
	$lang['exif_GPSLongitudeRef']			= "GPS Longitude Ref";
	$lang['exif_GPSLongitude_0']			= "GPS Longitude 0";
	$lang['exif_GPSLongitude_1']			= "GPS Longitude 1";
	$lang['exif_GPSLongitude_2']			= "GPS Longitude 2";
	$lang['exif_GPSTimeStamp_0']			= "GPS Timestamp 0";
	$lang['exif_GPSTimeStamp_1']			= "GPS Timestamp 1";
	$lang['exif_GPSTimeStamp_2']			= "GPS Timestamp 2";
	$lang['exif_GPSImgDirectionRef']		= "GPS Imgage Direction Reference";
	$lang['exif_GPSImgDirection']			= "GPS اتجاه الصورة";
	
	// Media Labels
	$lang['mediaLabelTitle']		= "العنوان";
	$lang['mediaLabelDesc']			= "الوصف";
	$lang['mediaLabelCopyright']	= "الحقوق";
	$lang['mediaLabelRestrictions']	= "ضوابط الاستخدام";
	$lang['mediaLabelRelease']		= "الاصدار";
	$lang['mediaLabelKeys']			= "كلمات البحث";
	$lang['mediaLabelFilename']		= "اسم الملف";
	$lang['mediaLabelID']			= "ID اسم المعرف";
	$lang['mediaLabelDate']			= "تاريخ الاضافة";
	$lang['mediaLabelDateC']		= "تاريخ اول عرض";
	$lang['mediaLabelDownloads']	= "تحميل";
	$lang['mediaLabelViews']		= "مشاهدات";
	$lang['mediaLabelPurchases']	= "المشترين";
	$lang['mediaLabelColors']		= "الالوان";
	$lang['mediaLabelPrice']		= "السعر";
	$lang['mediaLabelCredits']		= "القيمة بنظامنا الخاص بالائتمان او الكريدت";
	$lang['mediaLabelOwner']		= "المالك";
	$lang['mediaLabelUnknown']		= "غير معروف";
	$lang['mediaLabelResolution']	= "بكسل";
	$lang['mediaLabelFilesize']		= "حجم الملف";
	$lang['mediaLabelRunningTime']	= "الوقت";
	$lang['mediaLabelFPS']			= "FPS";
	$lang['mediaLabelFormat']		= "نوع الملف";
	
	// Search
	$lang['search']					= "بحث";
	$lang['searchEnterKeyMessage']	= "الرجاء ادخال كلمات البحث في المكان المخصص";
	$lang['searchNoResults']		= "لا يوجد نتائج مطابقة";
	$lang['searchDateRange']		= "بحث ضمن نطاق زمني معين";
	$lang['searchNarrow']			= "احصر نطاق البحث اكثر";
	$lang['searchStart']			= "ابدأ بحثك بالاسفل";
	$lang['searchAll']				= "الكل";
	$lang['searchClear']			= "مسح";
	$lang['searchKeywords']			= "كلمات البحث";
	$lang['searchTitle']			= "العنوان";
	$lang['searchDescription']		= "الوصف";
	$lang['searchFilename']			= "اسم الملف";
	$lang['searchPortrait']			= "لوحة بورتريه";
	$lang['searchLandscape']		= "منظر عرضي لاندسكيب";
	$lang['searchSquare']			= "مربع";
	$lang['searchRoyaltyFree']		= "تملك غير مشروط";
	$lang['searchRightsManaged']	= "تملك مشروط";
	$lang['searchFree']				= "مجانية";
	$lang['searchContactUs']		= "اتصل بنا";
	$lang['searchHeaderKeywords']	= "كلمات البحث";
	$lang['searchHeaderFields']		= "حقول";
	$lang['searchHeaderType']		= "النوع";
	$lang['searchHeaderOrientation']= "النوع";
	$lang['searchHeaderColor']		= "اللون";
	$lang['searchHeaderGalleries']	= "البومات";	
?>