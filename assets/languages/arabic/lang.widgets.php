<?php
	# 4.4.6
	$wplang['stats_visitors']		= "Approximate Site Visits";
	
	# 4.1.3
	$wplang['qstats_pending_media']	= "Pending Contributor Media";
	
	# 4.0.7	
	$wplang['sitehealth_exif']	= "PHP EXIF Support (exif_read_data)";
	
	# GENERAL WIDGET LANGUAGE
	$wplang['widget_close']		= "اغلق";
	$wplang['widget_save']		= "حفظ";
	$wplang['load_failed'] 		= "فشل في تحميل لوحة التحكم";
	$wplang['widget_title']		= "العنوان";
	$wplang['widget_note']		= "ملاحظة";
	$wplang['widget_for']		= "ل";
	
	# NOTES WIDGET
	$wplang['notes_title']		= "ملاحظات";
	$wplang['notes_newnote']	= "ملاحظة جديدة";
	$wplang['notes_postedby']	= "اسم الكاتب";
	$wplang['notes_lastupdate']	= "اخر تحديث";
	$wplang['notes_nonotes']	= "لانوجد اي ملاحظة للعرض";
	
	# EXTRAS WIDGET
	$wplang['extras_title']		= "برمجيات ملحقة";
	
	# KTOOLS ACCOUNT WIDGET
	$wplang['kaccount_title']	= "Ktools حساب ال";
	$wplang['kaccount_support']	= "الوقت المتبقي لخدمات الدعم الفني والترقية";
	$wplang['kaccount_messages']= "رسائل غير مقروءة";
	$wplang['kaccount_affil']	= "المبيعات المشتركة منذ اخر دخول";
	
	# KNEWS WIDGET
	$wplang['knews_title']		= "Ktools.net اخبار";
	
	# QUICK STATS WIDGET - NEW AND PENDING
	$wplang['qstats_title']		= "جديد وبانتظار المراجعة";
	$wplang['qstats_logmem']	= "عدد الاعضاء منذ اخر دخول";
	$wplang['qstats_penmem']	= "اعضاء جدد بانتظار موافقة";
	$wplang['qstats_logorders']	= "طلبات شراء منذ اخر دخول";
	$wplang['qstats_penorders']	= "طلبات شراء تنتظر مراجعة";
	$wplang['qstats_logcomm']	= "التعليقات منذ اخر دخول";
	$wplang['qstats_pencomm']	= "تعليقات تنتظر مراجعة";
	$wplang['qstats_logtags']	= "السمات منذ اخر دخول";
	$wplang['qstats_pentags']	= "سمات تنتظر المراجعة";
	$wplang['qstats_lograte']	= "تصويت منذ اخر دخول";
	$wplang['qstats_penrate']	= "تصويتات تنتظر مراجعة";
	$wplang['qstats_penbios']	= "سير شخصية تنتظر مراجعة";
	$wplang['qstats_penavatars']= "صور ملفات شخصية تنتظر مراجعة";
	$wplang['qstats_pensupport']= "طلبات دعم تقني تنتظر مراجعة";
	
	# STATS WIDGET
	$wplang['stats_title']		= "احصائيات";
	$wplang['stats_op1']		= "مبيعات";
	$wplang['stats_op2']		= "اعضاء";
	$wplang['stats_op_7days']	= "اخر 7 ايام";
	$wplang['stats_op_6mon']	= "اخر 6 اشهر";
	$wplang['stats_op_5year']	= "اخر 5 سنوات";
	$wplang['stats_tdsales']	= "مبيعات اليوم";
	$wplang['stats_tdorders']	= "طلبيات اليوم";
	$wplang['stats_sales']		= "مبيعات";
	$wplang['stats_orders']		= "طلبيات";	
	$wplang['stats_atsales']	= "جملة المبيعات حتى الان";
	$wplang['stats_atorders']	= "جملة الطلبيات حتى الان";
	$wplang['stats_tdmems']		= "الاعضاء الجدد اليوم";
	$wplang['stats_mems']		= "اعضاء جدد";
	$wplang['stats_tamems']		= "جميع الاعضاء المفعلين";
	$wplang['stats_timems']		= "جميع الاعضاء غير المفعلين";
	
	
	# SITE HEALTH WIDGET
	$wplang['sitehealth_title']	= "معلومات عن الموقع وخدمة الاستضافة";
	$wplang['sitehealth_ok']	= "جيد";
	$wplang['sitehealth_low']	= "ضعيف";
	$wplang['sitehealth_high']	= "ممتاز";
	$wplang['sitehealth_failed']= "فشل";
	$wplang['sitehealth_unava'] = "غير متوفر";
	$wplang['sitehealth_off']	= "متوقف";
	$wplang['sitehealth_on']	= "مفعل";
	$wplang['sitehealth_inst']	= "تمت اضافته";
	$wplang['sitehealth_none']	= "لا شيء";
	$wplang['sitehealth_exists']= "موجود";
	$wplang['sitehealth_write']	= "قابل للتعديل عليه";
	$wplang['sitehealth_nonwri']= "غير قابل للتعديل عليه";
	$wplang['sitehealth_php']	= "PHP نسخة";
	$wplang['sitehealth_gd']	= "GD مكتبة ال";
	$wplang['sitehealth_mem']	= "PHP Memory Limit";
	$wplang['sitehealth_exe']	= "PHP max_execution_time";
	$wplang['sitehealth_time']	= "PHP max_input_time";
	$wplang['sitehealth_file']	= "PHP upload_max_filesize";
	$wplang['sitehealth_post']	= "PHP post_max_size";
	$wplang['sitehealth_safe']	= "PHP safe_mode";
	$wplang['sitehealth_dbv']	= "قاعدة البيانات\التأكد من نسخة الاصدار";
	$wplang['sitehealth_mysqlv']= "MySQL Version";
	$wplang['sitehealth_load']	= "الحمل على السيرفر المستضيف";
	$wplang['sitehealth_upti']	= "الزمن منذ اخر توقف للسيرفر المستضيف";
	
	# UPDATE CHECK WIDGET
	$wplang['updater_title']	= "التحقق من التحديثات الجديدة";
	$wplang['updater_newest']	= "لديك اخر نسخة من النظام";
	$wplang['updater_update']	= "تحديث جديد متوفر";
	$wplang['updater_newestis']	= "اخر تحديث متوفر هو"; 
	$wplang['updater_getnew']	= "<a href='http://www.ktools.net/members/' target='_blank'>Ktools.net حساب</a> للحصول على اخر نسخة من النظام قم بزيارة الرابط التالي";
	$wplang['updater_yourv']	= "نسخة النظام الحالية لديك هي";
	
	# BLANK WIDGET
	$wplang['blank_title']		= "لوحة تحكم فارغة";
	
	# CALENDAR WIDGET
	$wplang['calendar_title']	= "رزنامة";
	
	# TIPS CHECK WIDGET
	//$wplang['tips_title']		= "نصائح";
	//$wplang['tips_next']		= "النصيحة التالية";
	//$welcometip[] 				= "لفتح واغلاق قائمة الاختصارات <strong>Ctrl+Shift+S</strong> هل كنت تعلم أنه بامكانك الضغط على"; 
	//$welcometip[] 				= "تستطيع الضغط على الاماكن الترحيبية بالماوس ومن ثم سحبها وترتيب مكانها كما تشاء"; 
	//$welcometip[] 				= "من اجل الدخول السريع لملفات الصور واماكن اخرى قم بالضغط على الخانة الموجودة في الجزء الاعلى على اليسار ، هذا سيفتح نافذة بها وصلات خاصة للتنقل السريع. اضغط على الخانة مرة اخرى لكي تختفي مرة اخرى"; 
	
?>