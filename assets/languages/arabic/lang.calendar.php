<?php
	# ARABIC
	$calendar = array();
	$calendar['long_month']		= array(1 => "كانون الثاني","شباط","آذار","نيسان","أيار","حزيران","تموز","آب","أيلول","تشرين الأول","تشرين الثاني","كانون الأول");
	
	/** Arabic months has no short or long names. As such, names declaired above in the long month are the ones officially used in Iraq, Lebanaon, Syria, palestine and Jordan.
		Naming adopted in other countries such as Egypt, Oman ... are declared bellow in the short month variable.
	*/

	$calendar['short_month']	= array(1 => "يناير","فبراير","مارس","ابريل","مايو","يونيو","يوليو","اغسطس","سبتمبر","اكتوبر","نوفمبر","ديسمبر");
	$calendar['full_days']		= array(1 => "الأحد","الأثنين","الثلاثاء","الأربعاء","الخميس","الجمعة","السبت");
	$calendar['short_days']		= array(1 => "الأحد","الأثنين","الثلاثاء","الأربعاء","الخميس","الجمعة","السبت");
?>