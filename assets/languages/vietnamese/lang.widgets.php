<?php
	# 4.4.6
	$wplang['stats_visitors']		= "Approximate Site Visits";
	
	# 4.1.3
	$wplang['qstats_pending_media']	= "Nội dung cộng tác viên đang chờ";
	
	# 4.0.7	
	$wplang['sitehealth_exif']	= "PHP EXIF Support (exif_read_data)";
	
	# GENERAL WIDGET LANGUAGE
	$wplang['widget_close']		= "Đóng";
	$wplang['widget_save']		= "Lưu";
	$wplang['load_failed'] 		= "Không thể tải bảng điều khiển!";
	$wplang['widget_title']		= "Tiêu đề";
	$wplang['widget_note']		= "Ghi chú";
	$wplang['widget_for']		= "Cho";
	
	# NOTES WIDGET
	$wplang['notes_title']		= "Ghi chú";
	$wplang['notes_newnote']	= "Ghi chú mới";
	$wplang['notes_postedby']	= "Người gửi";
	$wplang['notes_lastupdate']	= "Cập nhật lần cuối";
	$wplang['notes_nonotes']	= "Không có ghi chú để hiển thị";
	
	# EXTRAS WIDGET
	$wplang['extras_title']		= "PhotoStore Extras";
	
	# KTOOLS ACCOUNT WIDGET
	$wplang['kaccount_title']	= "Tài khoản Ktools";
	$wplang['kaccount_support']	= "Hỗ trợ / Nâng cấp ngày còn lại";
	$wplang['kaccount_messages']= "Tin nhắn chưa đọc";
	$wplang['kaccount_affil']	= "Liên kết bán hàng kể từ lần đăng nhập cuối";
	
	# KNEWS WIDGET
	$wplang['knews_title']		= "Tin tức Ktools.net";
	
	# QUICK STATS WIDGET - NEW AND PENDING
	$wplang['qstats_title']		= "Mới & Đang chờ";
	$wplang['qstats_logmem']	= "Thành viên từ lần đăng nhập cuối";
	$wplang['qstats_penmem']	= "Thành viên đang chờ";
	$wplang['qstats_logorders']	= "Đơn hàng kể từ lần đăng nhập cuối";
	$wplang['qstats_penorders']	= "Đơn hàng đang chờ";
	$wplang['qstats_logcomm']	= "Bình luận kể từ lần đăng nhập cuối";
	$wplang['qstats_pencomm']	= "Bình luận đang chờ";
	$wplang['qstats_logtags']	= "Thông tin kể từ lần đăng nhập cuối";
	$wplang['qstats_pentags']	= "Thông tin đang chờ";
	$wplang['qstats_lograte']	= "Đánh giá kể từ lần đăng nhập cuối";
	$wplang['qstats_penrate']	= "Đánh giá đang chờ";
	$wplang['qstats_penbios']	= "Tiểu sử thành viên đang chờ";
	$wplang['qstats_penavatars']= "Avatars đang chờ";
	$wplang['qstats_pensupport']= "Tin nhắn hỗ trợ đang chờ";
	
	# STATS WIDGET
	$wplang['stats_title']		= "Số liệu thống kê";
	$wplang['stats_op1']		= "Bán hàng";
	$wplang['stats_op2']		= "Thành viên";
	$wplang['stats_op_7days']	= "7 ngày cuối";
	$wplang['stats_op_6mon']	= "6 tháng cuối";
	$wplang['stats_op_5year']	= "5 năm cuối";
	$wplang['stats_tdsales']	= "Giảm giá hôm nay";
	$wplang['stats_tdorders']	= "Đặt hàng hôm nay";
	$wplang['stats_sales']		= "Bán hàng";
	$wplang['stats_orders']		= "Đơn đặt hàng";	
	$wplang['stats_atsales']	= "Tất cả sản phẩm bán ra";
	$wplang['stats_atorders']	= "Tất cả đơn đặt hàng";
	$wplang['stats_tdmems']		= "Thành viên mới hôm nay";
	$wplang['stats_mems']		= "Thành viên mới";
	$wplang['stats_tamems']		= "Tổng cộng thành viên hoạt động";
	$wplang['stats_timems']		= "Tổng cộng thành viên không hoạt động";
	
	
	# SITE HEALTH WIDGET
	$wplang['sitehealth_title']	= "Tình trạng trang Web & Máy chủ";
	$wplang['sitehealth_ok']	= "OK";
	$wplang['sitehealth_low']	= "THẤP";
	$wplang['sitehealth_high']	= "CAO";
	$wplang['sitehealth_failed']= "KHÔNG THÀNH CÔNG";
	$wplang['sitehealth_unava'] = "Không có sẵn";
	$wplang['sitehealth_off']	= "TẮT";
	$wplang['sitehealth_on']	= "BẬT";
	$wplang['sitehealth_inst']	= "Cài đặt";
	$wplang['sitehealth_none']	= "Không";
	$wplang['sitehealth_exists']= "Tồn tại";
	$wplang['sitehealth_write']	= "Có thể sửa đổi";
	$wplang['sitehealth_nonwri']= "Không thể sửa đổi";
	$wplang['sitehealth_php']	= "Phiên bản PHP";
	$wplang['sitehealth_gd']	= "Thư viện GD";
	$wplang['sitehealth_mem']	= "Giới hạn bộ nhớ PHP";
	$wplang['sitehealth_exe']	= "PHP max_execution_time";
	$wplang['sitehealth_time']	= "PHP max_input_time";
	$wplang['sitehealth_file']	= "PHP upload_max_filesize";
	$wplang['sitehealth_post']	= "PHP post_max_size";
	$wplang['sitehealth_safe']	= "PHP safe_mode";
	$wplang['sitehealth_dbv']	= "Database/Product Version Check";
	$wplang['sitehealth_mysqlv']= "MySQL Version";
	$wplang['sitehealth_load']	= "Server Load";
	$wplang['sitehealth_upti']	= "Server Uptime";
	
	# UPDATE CHECK WIDGET
	$wplang['updater_title']	= "Kiểm tra thông tin cập nhật";
	$wplang['updater_newest']	= "Bạn có phiên bản mới nhất";
	$wplang['updater_update']	= "Cập nhật có sẵn";
	$wplang['updater_newestis']	= "Phiên bản mới nhất là"; 
	$wplang['updater_getnew']	= "Để có được phiên bản mới nhất xin vui lòng đăng nhập vào <a href='http://www.ktools.net/members/' target='_blank'>tài khoản Ktools.net</a>";
	$wplang['updater_yourv']	= "Bạn đang chạy phiên bản";
	
	# BLANK WIDGET
	$wplang['blank_title']		= "Bảng điều chỉnh trống";
	
	# CALENDAR WIDGET
	$wplang['calendar_title']	= "Lịch";
	
	# TIPS CHECK WIDGET
	//$wplang['tips_title']		= "Tư vấn";
	//$wplang['tips_next']		= "Tư vấn kế tiếp";
	//$welcometip[] 				= "Bạn có biết bạn có thể sử dụng <strong>Ctrl+Shift+S</strong> để mở và đóng danh sách các phím tắt?"; 
	//$welcometip[] 				= "Bạn có thể kéo các bảng chào đón để sắp xếp lại chúng theo cách bạn muốn. Chỉ cần nhấp vào tiêu đề và kéo. Sau đó thả bảng khi nó theo thứ tự mà bạn muốn."; 
	//$welcometip[] 				= "Để truy cập nhanh đến bộ sưu tập và nhiều lĩnh vực khác của bạn bấm vào tab với mũi tên ở phía trên bên trái. Điều này sẽ mở danh sách các phiếm phím tắt. Nhấp vào tab để đóng nó."; 
	
?>