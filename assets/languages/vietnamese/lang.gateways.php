<?php
	# GATEWAYS LANG
	# 4.6.3
	# STRIPE
	$lang['stripe_displayName']			= "Stripe";
	$lang['stripe_publicDescription']	= "Credit Card or Debit Card";
	$lang['stripe_pkey']				= "Publishable Key";
	$lang['stripe_pkey_d']				= "Your publishable key for your stripe account.";
	$lang['stripe_skey']				= "Secret Key";
	$lang['stripe_skey_d']				= "Your secret key for your stripe account.";
	
	# 4.1.4
	# MOLLIE IDEAL
	$lang['mollieideal_displayName']		= "iDeal (Mollie)";
	$lang['mollieideal_f_partnerid']		= "ID đối tác";
	$lang['mollieideal_f_partnerid_d']		= "ID đối tác iDeal của bạn.";
	$lang['mollieideal_f_profilekey']		= "Mã số hồ sơ";
	$lang['mollieideal_f_profilekey_d']		= "Mã số hồ sơ Mollie iDeal của bạn.";
	$lang['mollieideal_f_testmode']			= "Chế độ kiểm tra";
	$lang['mollieideal_f_testmode_d']		= "Chuyển Mollie iDeal của bạn vào chế độ kiểm tra.";
	$lang['mollieideal_instructions']		= "Hãy chắc chắn rằng chế độ kiểm tra tương ứng với các thiết lập trong tài khoản Mollie.nl của bạn.";
	$lang['mollieideal_publicDescription']	= "Thanh toán an toàn và dễ sử dụng iDeal (chỉ có ở Hà Lan).";
	
	# 4.1
	# ONEBIP 
	$lang['onebip_displayName']			= "OneBip"; 
	$lang['onebip_merchantid']			= "Email";	
	$lang['onebip_merchantid_d']		= "Nhập email được sử dụng cho tài khoản onebip của bạn."; 
	$lang['onebip_merchantkey']			= "API mã số"; 
	$lang['onebip_merchantkey_d']		= "Nhập mã số API cho tào khoản onebip của bạn.";
	$lang['onebip_testmode']			= "Chế độ kiểm tra";
	$lang['onebip_testmode_d']			= "Đặt thanh toán onebip của bạn vào chế độ kiểm tra.";
	$lang['onebip_publicDescription']	= "Thanh toán bằng điện thoại di động";
	
	# PAYFAST 
	$lang['payfast_displayName']		= "PayFast"; 
	$lang['payfast_merchantid']			= "ID thương mại";	
	$lang['payfast_merchantid_d']		= "Nhập ID thương mại cho tài khoản PayFast của bạn."; 
	$lang['payfast_merchantkey']		= "Mã số thương mại"; 
	$lang['payfast_merchantkey_d']		= "Nhập mã số thương mại cho tài khoản PayFast của bạn.";
	$lang['payfast_testmode']			= "Chế độ kiểm tra"; 
	$lang['payfast_testmode_d']			= "Đặt PayFast của bạn vào chế độ kiểm tra.";
	$lang['payfast_publicDescription']	= "Thẻ tín dụng hoặc tài khoản ngân hàng"; 
	
	# NOCHEX
	$lang['nochex_displayName']			= "Nochex";
	$lang['nochex_f_accountid']			= "ID tài khoản";
	$lang['nochex_f_accountid_d']		= "ID tài khoản Nochex của bạn.";
	$lang['nochex_f_testmode']			= "Chế độ kiểm tra";
	$lang['nochex_f_testmode_d']		= "Đặt thanh toán Nochex của bạn vào chế độ kiểm tra.";
	$lang['nochex_publicDescription']	= "Thẻ tín dụng hoặc thẻ ghi nợ";
	
	# WORLDPAY
	$lang['worldpay_displayName']		= "WorldPay";
	$lang['worldpay_f_installid']		= "ID cài đặt";
	$lang['worldpay_f_installid_d']		= "ID cài đặt cho tài khoản WorldPay của bạn.";
	$lang['worldpay_f_testmode']		= "Chế độ kiểm tra";
	$lang['worldpay_f_testmode_d']		= "Đặt thanh toán WorldPay của bạn vào chế độ kiểm tra.";
	$lang['worldpay_publicDescription']= "Thẻ tín dụng hoặc thẻ ghi nợ";
	
	# ROBOKASSA
	$lang['robokassa_displayName']		= "RoboKassa";
	$lang['robokassa_f_merchantid']		= "ID thương mại";
	$lang['robokassa_f_merchantid_d']	= "ID thương mại cho tài khoản RoboKassa của bạn.";
	$lang['robokassa_f_merchantpass']	= "Mật khẩu thương mại";
	$lang['robokassa_f_merchantpass_d']	= "ID mật khẩu thương mại RoboKassa của bạn.";
	$lang['robokassa_publicDescription']= "Thẻ tín dụng hoặc thẻ ghi nợ";
	
	# PAYGATE
	$lang['paygate_displayName']		= "PayGate";
	$lang['paygate_f_accountid']		= "PayGate ID";
	$lang['paygate_f_accountid_d']		= "ID cho tài khoản PayGate của bạn.";
	$lang['paygate_f_accountkey']		= "Mã số PayGate";
	$lang['paygate_f_accountkey_d']		= "Mã số ID PayGate của bạn.";
	$lang['paygate_f_testmode']			= "Chế độ kiểm tra";
	$lang['paygate_f_testmode_d']		= "Đặt thanh toán PayGate của bạn vào chế độ kiểm tra.";	
	$lang['paygate_f_testid']			= "Kiểm tra ID PayGate";
	$lang['paygate_f_testid_d']			= "Kiểm tra ID PayGate của bạn";
	$lang['paygate_f_testkey']			= "Kiểm tra mã số PayGate";
	$lang['paygate_f_testkey_d']		= "Kiểm tra mã số PayGate của bạn";
	$lang['paygate_publicDescription']	= "Thẻ tín dụng hoặc thẻ ghi nợ";
	
	# PAYSTATION
	$lang['paystation_displayName']		= "PayStation";
	$lang['paystation_f_accountid']		= "PayStation ID";
	$lang['paystation_f_accountid_d']	= "ID cho tài khoản PayStation của bạn.";
	$lang['paystation_f_testmode']		= "Chế độ kiểm tra";
	$lang['paystation_f_testmode_d']	= "Đặt thanh toán PayStation của bạn vào chế độ kiểm tra.";	
	$lang['paystation_f_gatewayid']		= "Cổng ID";
	$lang['paystation_f_gatewayid_d']	= "Cổng ID PayStation của bạn.";
	$lang['paystation_publicDescription']= "Thẻ tín dụng hoặc thẻ ghi nợ";
	
	# SKRILL
	$lang['skrill_displayName']			= "Skrill (moneybookers)";
	$lang['skrill_f_email']				= "Địa chỉ Email";
	$lang['skrill_f_email_d']			= "Địa chỉ Email cho tài khoản Skrill của bạn.";
	$lang['skrill_f_testmode']			= "Chế độ kiểm tra";
	$lang['skrill_f_testmode_d']		= "Đặt thanh toán Skrill của bạn vào chế độ kiểm tra.";	
	$lang['skrill_f_testemail']			= "Kiểm tra Email";
	$lang['skrill_f_testemail_d']		= "Kiểm tra địa chỉ email Skrill của bạn.";
	$lang['skrill_publicDescription']	= "Thẻ tín dụng hoặc thẻ ghi nợ";
	$lang['skrill_f_completeOrder']		= "Bấm vào đây để hoàn thành đơn đặt hàng của bạn!";
	
	# CHRONOPAY
	$lang['chronopay_displayName']		= "ChronoPay";
	$lang['chronopay_f_clientid']		= "ID khách hàng";
	$lang['chronopay_f_clientid_d']		= "ID khách hàng ChronoPay của bạn.";
	$lang['chronopay_f_siteid']			= "ID ChronoPay ";
	$lang['chronopay_f_siteid_d']		= "ID ChronoPay của bạn.";
	$lang['chronopay_f_productid']		= "ID sản phẩm.";
	$lang['chronopay_f_productid_d']	= "ID sản phẩm ChronoPay của bạn.";
	$lang['chronopay_publicDescription']= "Thẻ tín dụng hoặc thẻ ghi nợ";
	
	# IDEAL
	$lang['ideal_displayName']			= "iDeal (ING)";
	$lang['ideal_f_accountid']			= "ID tài khoản";
	$lang['ideal_f_accountid_d']		= "ID tài khoản iDeal của bạn.";
	$lang['ideal_f_transkey']			= "Mã số bí mật";
	$lang['ideal_f_transkey_d']			= "Mã số bí mật iDeal của bạn.";
	$lang['ideal_f_testmode']			= "Chế độ kiểm tra";
	$lang['ideal_f_testmode_d']			= "Đặt thanh toán iDeal của bạn vào chế độ kiểm tra.";
	$lang['ideal_publicDescription']	= "Tài khoản ngân hàng";
	
	# PAYPAL
	$lang['paypal_displayName']			= "PayPal";
	$lang['paypal_f_email']				= "Địa chỉ Email";
	$lang['paypal_f_email_d']			= "Địa chỉ Email cho tài khoản PayPal của bạn.";
	$lang['paypal_f_testmode']			= "Chế độ kiểm tra";
	$lang['paypal_f_testmode_d']		= "Đặt thanh toán PayPal của bạn vào chế độ kiểm tra.";	
	$lang['paypal_f_testemail']			= "Kiểm tra Email";
	$lang['paypal_f_testemail_d']		= "Kiểm tra địa chỉ email PayPal sandbox của bạn.";
	$lang['paypal_publicDescription']	= "PayPal, Thẻ tín dụng hoặc tài khoản ngân hàng";
	
	# 2CHECKOUT
	$lang['2checkout_displayName']		= "2Checkout.com";
	$lang['2checkout_f_accountid']		= "ID tài khoản";
	$lang['2checkout_f_accountid_d']	= "ID tài khoản 2Checkout của bạn.";
	$lang['2checkout_f_testmode']		= "Chế độ kiểm tra";
	$lang['2checkout_f_testmode_d']		= "Đặt thanh toán 2Checkout của bạn vào chế độ kiểm tra.";
	$lang['2checkout_instructions']		= "In 2Checkout.com set direct return to 'Header Redirect (Your URL)' and set approved URL to http://www.YOUR_DOMAIN_NAME.com/assets/gateways/2checkout/ipn.php";
	$lang['2checkout_publicDescription']= "Thẻ tín dụng hoa85i tài khoản ngân hàng USA";

	
	# Plug n' Pay
	$lang['plugnpay_displayName']		= "Plug n' Pay";
	$lang['plugnpay_f_accountid']		= "ID tài khoản";
	$lang['plugnpay_f_accountid_d']		= "ID tài khoản Plug n' Pay của bạn.";
	$lang['plugnpay_publicDescription']	= "";
	
	# AUTHORIZE.NET
	$lang['authorize_displayName']		= "Authorize.net";
	$lang['authorize_f_apiid']			= "ID Đăng nhập API";
	$lang['authorize_f_apiid_d']		= "ID Đăng nhập Authorize.net API của bạn.";
	$lang['authorize_f_transkey']		= "Mã số giao dịch";
	$lang['authorize_f_transkey_d']		= "Mã số giao dịch Authorize.net của bạn.";
	$lang['authorize_f_testmode']		= "Chế độ kiểm tra";
	$lang['authorize_f_testmode_d']		= "Đặt thanh toán Authorize.net của bạn vào chế độ kiểm tra.";
	$lang['authorize_publicDescription']= "";
	$lang['authorize_f_completeOrder'] = "Bấm vào đây để hoàn thành đơn đặt hàng của bạn!";
	
	# MYGATE.CO.ZA
	$lang['mygate_displayName']			= "MyGate.co.za";
	$lang['mygate_f_merchantid']		= "ID thương mại";
	$lang['mygate_f_merchantid_d']		= "ID thương mại MyGate.co.za của bạn.";
	$lang['mygate_f_appid']				= "ID ứng dụng";
	$lang['mygate_f_appid_d']			= "ID ứng dụng MyGate.co.za của bạn.";
	$lang['mygate_publicDescription']	= "";
	$lang['mygate_f_testmode']			= "Chế độ kiểm tra";
	$lang['mygate_f_testmode_d']		= "Đặt thanh toán Mygate của bạn vào chế độ kiểm tra.";	
	
	# MAIL IN PAYMENT
	$lang['mailin_displayName']			= "Mail In Payment";
	$lang['mailin_f_instructions']		= "Hướng dẫn Mail In Payment";
	$lang['mailin_f_instructions_d']	= "Hướng dẫn cho khách hàng để gửi email trong thanh toán như kiểm tra hoặc chuyển tiền.";
	$lang['mailin_publicDescription']	= "Gửi thanh toán của bạn đến địa chỉ được cung cấp trên trang tiếp theo.";
?>