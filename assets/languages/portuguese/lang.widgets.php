<?php
/*Portugues*/
	# 4.4.6
	$wplang['stats_visitors']		= "Approximate Site Visits";
	
	# 4.1.3
	$wplang['qstats_pending_media']	= "Pending Contributor Media";
	
	# 4.0.7
	$wplang['sitehealth_exif'] = "Supporte PHP EXIF  (exif_read_data)";
	# GENERAL WIDGET LANGUAGE
	$wplang['widget_close'] = "Fechar";
	$wplang['widget_save'] = "Salvar";
	$wplang['load_failed'] = "Carregar o painel falhou!";
	$wplang['widget_title'] = "T&#237;tulo";
	$wplang['widget_note'] = "Nota";
	$wplang['widget_for'] = "Para";
	# NOTES WIDGET
	$wplang['notes_title'] = "Notas";
	$wplang['notes_newnote'] = "Nova Nota";
	$wplang['notes_postedby'] = "Colocado por";
	$wplang['notes_lastupdate'] = "&#218;ltima atualiza&#231;&#227;o";
	$wplang['notes_nonotes'] = "N&#227;o existem notas para mostrar";
	# EXTRAS WIDGET
	$wplang['extras_title'] = "Extras ktools";
	# KTOOLS ACCOUNT WIDGET
	$wplang['kaccount_title'] = "Conta Ktools";
	$wplang['kaccount_support'] = "Supporte/Atualizar - Dias em falta";
	$wplang['kaccount_messages'] = "Mensagens n&#227;o lidas";
	$wplang['kaccount_affil'] = "Vendas de afiliados desde a &#250;ltima entrada";
	# KNEWS WIDGET
	$wplang['knews_title'] = "Noticias ktools";
	# QUICK STATS WIDGET - NEW AND PENDING
	$wplang['qstats_title']	= "Novos & Pendentes";
	$wplang['qstats_logmem'] = "Utilizadores desde a ultima entrada";
	$wplang['qstats_penmem'] = "Utilizadores pendentes";
	$wplang['qstats_logorders'] = "Encomendas desde a &#250;ltima entrada";
	$wplang['qstats_penorders'] = "Encomendas pendentes";
	$wplang['qstats_logcomm'] = "Coment&#225;rios desde a &#250;ltima entrada";
	$wplang['qstats_pencomm'] = "Coment&#225;rios pendentes";
	$wplang['qstats_logtags'] = "Etiquetas desde a &#250;ltima entrada";
	$wplang['qstats_pentags'] = "Etiquetas pendentes";
	$wplang['qstats_lograte'] = "Avalia&#231;&#245;es desde a &#250;ltima entrada";
	$wplang['qstats_penrate'] = "Avalia&#231;&#245;es pendentes";
	$wplang['qstats_penbios'] = "Utilizadores Bios pendentes";
	$wplang['qstats_penavatars']= "Avatars pendentes";
	$wplang['qstats_pensupport']= "Senhas de suporte pendentes";
	# STATS WIDGET
	$wplang['stats_title'] = "Stats";
	$wplang['stats_op1'] = "Vendas";
	$wplang['stats_op2'] = "Utilizadores";
	$wplang['stats_op_7days'] = "&#218;ltimos 7 Dias";
	$wplang['stats_op_6mon'] = "&#218;ltimos 6 Meses";
	$wplang['stats_op_5year'] = "&#218;ltimos 5 Anos";
	$wplang['stats_tdsales'] = "Vendas hoje";
	$wplang['stats_tdorders'] = "Encomendas hoje";
	$wplang['stats_sales'] = "Vendas";
	$wplang['stats_orders'] = "Encomendas";
	$wplang['stats_atsales'] = "Vendas at&#233; hoje";
	$wplang['stats_atorders'] = "Encomendas at&#233; hoje";
	$wplang['stats_tdmems'] = "Novos utilizadores hoje";
	$wplang['stats_mems'] = "Novos utilizadores";
	$wplang['stats_tamems'] = "Total de utilizadores ativos";
	$wplang['stats_timems'] = "Total de utilizadores inativos";
	# SITE HEALTH WIDGET
	$wplang['sitehealth_title'] = "Sa&#250;de do Sitio & Servidor";
	$wplang['sitehealth_ok'] = "OK";
	$wplang['sitehealth_low'] = "BAIXO";
	$wplang['sitehealth_high'] = "ALTO";
	$wplang['sitehealth_failed'] = "FALHOU";
	$wplang['sitehealth_unava'] = "N&#227;o existente";
	$wplang['sitehealth_off'] = "OFF";
	$wplang['sitehealth_on'] = "ON";
	$wplang['sitehealth_inst'] = "Instalado";
	$wplang['sitehealth_none'] = "N&#227;o";
	$wplang['sitehealth_exists']= "Existentes";
	$wplang['sitehealth_write'] = "Tem permiss�o de escrita";
	$wplang['sitehealth_nonwri'] = "Sem permiss&#227;o de escrita";
	$wplang['sitehealth_php'] = "Vers&#227;o PHP";
	$wplang['sitehealth_gd'] = "Livraria GD";
	$wplang['sitehealth_mem'] = "limite de mem&#243;ria PHP";
	$wplang['sitehealth_exe'] = "PHP max_execution_time";
	$wplang['sitehealth_time'] = "PHP max_input_time";
	$wplang['sitehealth_file'] = "PHP upload_max_filesize";
	$wplang['sitehealth_post'] = "PHP post_max_size";
	$wplang['sitehealth_safe'] = "PHP safe_mode";
	$wplang['sitehealth_dbv'] = "Base de Dados/Verificar vers�o do produto";
	$wplang['sitehealth_mysqlv'] = "MySQL Version";
	$wplang['sitehealth_load'] = "Server Load";
	$wplang['sitehealth_upti'] = "Tempo do servidor";
	# UPDATE CHECK WIDGET
	$wplang['updater_title'] = "Procurar atualiza&#231;&#245;es";
	$wplang['updater_newest'] = "Tem a nova vers&#227;o";
	$wplang['updater_update'] = "Existe atualiza&#231;&#227;o";
	$wplang['updater_newestis'] = "A nova vers&#227;o &#233;";
	$wplang['updater_getnew'] = "Para obter uma nova vers&#227;o, entre na sua conta <a href='http://www.ktools.net/members/' target='_blank'>Ktools.net account</a>";
	$wplang['updater_yourv'] = "Possui a nova vers&#227;o";
	# BLANK WIDGET
	$wplang['blank_title'] = "Painel Branco";
	# CALENDAR WIDGET
	$wplang['calendar_title'] = "Calendario";
	# TIPS CHECK WIDGET
	//$wplang['tips_title']		= "Tipos";
	//$wplang['tips_next']		= "pr&#243;xima dica";
	//$welcometip[] 				= "Sabia que voc&#234; pode usar <strong> Ctrl + Shift + S </ strong> para abrir e fechar o menu de atalhos?";
	//$welcometip[] 				= "Voc&#234; pode arrastar os pain&#233;is de boas-vindas para reorganiz&#225;-las da maneira que voc&#234; goste. Basta clicar no t&#237;tulo e arrastar. Em seguida, solte o painel quando &#233; na ordem que voc&#234; gostaria.";
	//$welcometip[] 				= "Para acesso r&#225;pido &#224;s suas galerias e muitas outras &#225;reas, clique na guia com a seta no canto superior esquerdo. Isto ir&#225; abrir o menu de atalhos. Clique na guia novamente para fech&#225;-lo.";



?>