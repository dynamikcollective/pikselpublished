<?php
	
	# 4.7.3
	$mgrlang['del_linked_file']					= "Deliver Linked File";
	$mgrlang['file_linked']						= "File Linked";
	$mgrlang['other_digital_sizes']				= "Other Digital Sizes";
	$mgrlang['media_f_el']						= "External Link";
	$mgrlang['media_f_el_d']					= "Link to file if not stored locally.";
	
	# 4.7
	$mgrlang['webset_f_fotomoto']				= "Fotomoto Store ID";
	$mgrlang['webset_f_fotomoto_d']				= "To enable Fotomoto support enter your store ID.";
	# plupload
	$mgrlang['plupSelectFiles']					=  "Select files";
	$mgrlang['plupAddFilesToQueue']				=  "Add files to the upload queue and click the start button.";
	$mgrlang['plupFilename']					=  "Filename";
	$mgrlang['plupStatus']						=  "Status";
	$mgrlang['plupSize']						=  "Size";
	$mgrlang['plupAddFiles']					=  "Add files";
	$mgrlang['plupStartUplaod']					=  "Start upload";
	$mgrlang['plupStopUpload']					=  "Stop current upload";
	$mgrlang['plupStartQueue']					=  "Start uploading queue";
	$mgrlang['plupDragFilesHere']				=  "Drag files here.";
	$mgrlang['feature_gallery']					=  "Feature Gallery On Home Page";
	$mgrlang['feature_gallery_d']				=  "Feature this gallery on the home page.";
	
	# 4.6.1
	$mgrlang['image_zoom_warning']				= "The Image Zoom feature prevents sample photos on the details page from scaling for mobile devices.";
	
	# 4.6
	$mgrlang['webset_f_facebook_link']			= "Link To Facebook";
	$mgrlang['webset_f_facebook_link_d']		= "Add a link to your Facebook page.";
	$mgrlang['webset_f_twitter_link']			= "Link To Twitter";
	$mgrlang['webset_f_twitter_link_d']			= "Add a link to your Twitter page.";
	$mgrlang['webset_social_set']				= "Social";

	# 4.4.6
	$mgrlang['mem_del_bill']				= "Are you sure you would like to delete this bill?";
	
	# 4.4.5
	$mgrlang['setup_f_pubuploader']			= "Default Public Batch Uploader";
	$mgrlang['setup_f_pubuploader_d']		= "Default batch uploader for members.";
	$mgrlang['setup_f_uploader_op4']		= "Plupload (HTML5, Flash, Gears, Silverlight, HTML4)";
	
	# 4.4.4
	$mgrlang['gen_code_link_d']						= "HTML code to insert into your template for a link to this agreement.";
	$mgrlang['gen_email_template']					= "Email Template";
	$mgrlang['util_f_sql'] 							= "SQL Execute";
	$mgrlang['util_f_sql_d'] 						= "Insert a query to execute. WARNING! Do not use this unless you know what you are doing!";
	$mgrlang['util_f_exec']							= "Execute";
	$mgrlang['util_mes_07']							= "Your query ran successfully.";
	$mgrlang['util_mes_08']							= "Your query failed to run.";
	
	# 4.4.3
	$mgrlang['media_f_pr']							= "Property Release";
	$mgrlang['media_f_pr_d']						= "Does this media have a property release form.";
	$mgrlang['landf_f_gallerySorting']				= "Sorting Gallery List";
	$mgrlang['landf_f_gallerySorting_d']			= "Set how you wish for your galleries to be sorted in a list. (IMPORTANT! you will not see the change right away)";
	$mgrlang['landf_f_gallerySorting_ascending']	= "Ascending A-Z";
	$mgrlang['landf_f_gallerySorting_descending']	= "Descending Z-A";
	$mgrlang['landf_f_gallerySortBy_name']			= "Gallery Name";
	$mgrlang['landf_f_gallerySortBy_sortnumber']	= "Gallery Sort Number";
	$mgrlang['landf_f_gallerySortBy_created']		= "Date Created";
	$mgrlang['landf_f_gallerySortBy_edited']		= "Date Edited";
	$mgrlang['landf_f_gallerySortBy_activedate']	= "Active Date";
	$mgrlang['landf_f_gallerySortBy_expiredate']	= "Expire Date";
	$mgrlang['landf_f_gallerySortBy_eventdate']		= "Event Date";
	$mgrlang['landf_f_gallerySortBy_eventlocation']	= "Event Location";
	$mgrlang['landf_f_gallerySortBy_eventcode']		= "Event Code";
	$mgrlang['cp_linked']							= "Link";
	$mgrlang['cp_linked_d']							= "Specify a link to where users who try to view this content will be redirected to. Example (http://www.--domain--.com).";
	
	# 4.4.2
	$mgrlang['landf_f_tagCloudSort_keyword']			= "Alpha-Numeric";
	$mgrlang['landf_f_tagCloudSort_default']			= "Popularity";
	$mgrlang['landf_f_tagCloudSort_d']					= "Use the drop down box to set how you would like the tag cloud keywords sorted.";
	$mgrlang['landf_f_tagCloudSort']					= "Tag Cloud Sort";
	$mgrlang['landf_f_tagCloudOn_d']					= "Show the tag cloud on the advanced search or no results returned during search.";
	$mgrlang['landf_f_tagCloudOn']						= "Tag Could";
	$mgrlang['gen_dig_sub_dl']							= "Digital Subscription Download";
	
	
	# 4.4.0

	$mgrlang['approve_media']				= "אשר מדיה";

	$mgrlang['rm_pricing']					= "תמחור Rights Managed";

	$mgrlang['rm_selections']				= "בחירות תמחור Rights Managed";

	$mgrlang['landf_f_share'] 				= "שתף";

	$mgrlang['landf_f_share_d'] 			= "הצג את איזור המדיה המשותפת בדף הפרטים, איפה שהמבקרים יכולים להשיג fetch BBcode, HTML, וכו'.";

	$mgrlang['landf_f_contri_link'] 		= "הצג קישור דף תורמים";

	$mgrlang['landf_f_contri_link_d'] 		= "הצג את קישור דף התורמים בראש התפריט.";	

	$mgrlang['rm_price_mod'] 				= "שינויי מחיר";

	$mgrlang['rm_price_mod_d'] 				= "איך המחיר ישתנה כאשר בוחרים אפשרות זו.";

	$mgrlang['rm_price_d'] 					= "המחיר שנוסף, הופחת או הוכפל מהסה\"כ.";

	$mgrlang['rm_credits_d'] 				= "הקרדיטים שנוספו, הופחתו או הוכפלו מהסה\"כ.";

	$mgrlang['rm_option_name'] 				= "שם אפשרות";

	$mgrlang['rm_option_name_d'] 			= "שם אפשרות זו.";

	$mgrlang['rm_option_group'] 			= "קבוצת אפשרויות";

	$mgrlang['rm_option_group_d'] 			= "בחר את קבוצת הפשרויות שאפשרות זו היא עבורה.";

	$mgrlang['rm_message_1'] 				= "אתה צריך ליצור קבוצות אפשרויות לפני שתיצור אפשרויות.";

	$mgrlang['rm_message_2'] 				= "לתוכנית RM זו אין קבוצות אפשרויות. לחץ 'קבוצת אפשרויות חדשה' כדי להתחיל.";	

	$mgrlang['rm_og_name'] 					= "שם קבוצת אפשרויות";

	$mgrlang['rm_og_name_d'] 				= "שם קבוצת אפשרויות זו.";	

	$mgrlang['rm_link_to'] 					= "קשר ל-";

	$mgrlang['rm_link_to_d'] 				= "בחר את האפשרויות שקבוצה זו מקושרת אליהן. קבוצה זו (רשימה נגללת) תוצג כאשר אפשרות זו תיבחר.";	

	$mgrlang['gen_base_price'] 				= "מחיר בסיס";

	$mgrlang['gen_base_credits'] 			= "מספר קרדיטים בסיסי";	

	$mgrlang['rm_custom_bc'] 				= "קבע עלות בסיסית מותאמת";

	$mgrlang['rm_custom_mp'] 				= "השתמש במחיר/קרדיטים של המדיה כעלות בסיסית";		

	$mgrlang['rm_cps'] 						= "הגדר תוכניות תמחור";

	$mgrlang['gen_pricing_scheme']			= "תוכנית תמחור";

	$mgrlang['gen_new_op_grp']				= "קבוצת אפשרויות חדשה";

	$mgrlang['gen_new_option']				= "אפשרות חדשה";

	$mgrlang['gen_edit_op_grp']				= "ערוך קבוצת אפשרויות";

	$mgrlang['gen_edit_option']				= "ערוך אפשרות";

	$mgrlang['webset_f_minicart']			= "עגלה מיני";

	$mgrlang['webset_f_minicart_d']			= "תכונה דינמית להוספה לעגלה שתשמור על הלקוח בעמוד הנוכחי בזמן הוספת פריט לעגלה.";

	# 4.3

	$mgrlang['mem_mes_10']					= "לא קיימים חיובים בחשבון זה.";

	$mgrlang['taxvat_id']					= "מס' מע\"מ/מס";

	$mgrlang['landf_f_license']				= "רשיון תצוגה";

	$mgrlang['landf_f_license_d']			= "הצג את סוג הרשיון עבור מדיה דיגיטלית. הסרת הסימון תסתיר את סוג הרישיון בכל מקום באתר הציבורי.";

	$mgrlang['webset_f_taxid']				= "בקש מס' מע\"מ";

	$mgrlang['webset_f_taxid_d']			= "בקש מס' מע\"מ מהלקוחות, בסוף החשבון.";	

	$mgrlang['webset_f_onotes']				= "הערות הלקוח";

	$mgrlang['webset_f_onotes_d']			= " אפשר ללקוחות להוסיף הערות להזמנה.";

	$mgrlang['order_onotes_d']				= "הערות שהלקוח הזין עם ההזמנה.";	

	$mgrlang['subnav_licenses']				= "רשיונות";

	$mgrlang['subnav_licenses_d']			= "צור וערוך סוגי רשיונות עבור המדיה שלך.";	

	$mgrlang['licenses_new_header']			= "הוסף רשיון";

	$mgrlang['licenses_edit_header']		= "ערוך רשיון";

	$mgrlang['licenses_new_message']		= "בעזרת הטופס הבא תוכל להוסיף סוג רשיון חדש.";

	$mgrlang['licenses_edit_message']		= "ערוך את סוג הרשיון ולחץ על כפתור 'שמור שינויים'.";

	$mgrlang['licenses_tab1']				= "פרטים";

	$mgrlang['licenses_tab2']				= "קבוצות";

	$mgrlang['licenses_f_name']				= "שם הרשיון";

	$mgrlang['licenses_f_name_d']			= "שם סוג הרשיון. לדוגמא: Royalty Free.";	

	$mgrlang['licenses_f_groups']			= "קבוצות רשיון";

	$mgrlang['licenses_f_groups_d']			= "בחר את הקבוצות שסוג הרשיון שייך להם.";

	$mgrlang['licenses_f_description_d']	= "תיאור קצר של סוג הרשיון הזה.";	

	$mgrlang['licenses_f_handle_d']			= "בחר איך רשיון זה יטופל באתר.";

	$mgrlang['gen_reg_license']				= "רשיון רגיל";

	

	

	# 4.2.2

	$mgrlang['tax_dit']						= "מיסי פריט דיגיטלי";

	$mgrlang['tax_dit_d']					= "(מדיה, קרדיטים, מנויים, הרשמות)";

	$mgrlang['tax_pit']						= "מיסי פריט פיזי";

	$mgrlang['tax_pit_d']					= "(הדפסות, מוצרים, חבילות)";	

	$mgrlang['tax_f_taxms_d']				= "החל מס על תוכניות הרשמה";

	$mgrlang['tax_f_taxprints_d']			= "החל מס על הדפסות ומוצרים.";

	$mgrlang['tax_f_taxshipping_d']			= "החל מס על עלויות משלוח.";

	$mgrlang['tax_f_taxdigital_d']			= "החל מס על הורדות מדיה דיגיטלית.";	

	$mgrlang['tax_f_taxsubs_d']				= "החל מס על מנויים דיגיטליים.";

	$mgrlang['tax_f_taxcredits_d']			= "החל מס על קרדיטים דיגיטליים.";

	$mgrlang['country_f_tax_ms_d']			= "החל מס כאשר לקוח מבצע הרשמה במדינה זו.";

	$mgrlang['states_f_tax_ms_d']			= "החל מס כאשר לקוח רוכש תוכנית הרשמה במדינה/פרובינציה זו.";

	$mgrlang['zipcodes_f_tax_ms_d']			= "החל מס כאשר לקוח מבצע הרשמה במיקוד זה.";

	$mgrlang['landf_watermark'] 			= "סימני מים (Watermarks)";

	$mgrlang['landf_f_uploadwatermark'] 	= " העלה סימן מים (Watermark)";

	$mgrlang['landf_f_deletewatermark'] 	= " מחק סימן מים (Watermark)";

	$mgrlang['landf_f_uploadwatermark_d'] 	= "הסימן מים (watermark) חייב להיות בפורמט PNG, כל דבר אחר לא יעבוד!";

	$mgrlang['landf_f_deletewatermark_d'] 	= "סמן את התיבות למטה בשביל סימני המים (watermarks) שברצונך למחוק, ולחץ על כפתור 'שמור שינויים'.";

	

	# 4.2.1

	$mgrlang['contact_captcha']				= "Captcha עבור טופס 'צור קשר'";

	$mgrlang['contact_captcha_d']			= "הדלק captcha בטופס 'צור קשר' בחנות, כך שהמבקרים חייבים להכניס קוד ייחודי בשליחת הטופס. זה ימנע מספאמ-בוטים להציף את המערכת.";

	$mgrlang['digital_downloads_upper']		= "הורדות דיגיטליות";

	$mgrlang['products_upper']				= "מוצרים";

	

	# 4.1.7

	$mgrlang['dsp_op_eu']					= "שימוש לצורך עריכה";

	$mgrlang['mem_f_msexpires']				= "תאריך תוקף";

	$mgrlang['mem_f_msexpires_d']			= "כאשר תוכנית הרשמה נוכחית מסתיימת עבור משתמש זה. הוא יעבור לתוכנית ברירת המחדל";

	$mgrlang['add_selected']				= "הוסף את מה שנבחר";

	$mgrlang['remove_selected']				= "הסר את מה שנבחר";

	

	

	# 4.1.6

	$mgrlang['batch_edit']					= "ערוך מצבור";

	$mgrlang['saved']						= "נשמר";

	$mgrlang['subscription_f_dtotal']		= "סה\"כ הורדות";

	$mgrlang['subscription_f_dtotal_d']		= " מספר הפעמים שמשתמש יכול להוריד עם מנוי זה. השאר ריק בשביל 'ללא הגבלה'.";

	$mgrlang['gen_t_dlrem_caps']			= "סה\"כ הורדות / נותרו";

	$mgrlang['no_limit']					= "ללא הגבלה";

		

	# 4.1.4

	$mgrlang['pay_commission']				= "שלם עמלה";

	

	# 4.1.3

	$mgrlang['landf_f_watermark_d']			= "החל סימן מים (watermark) לתמונות שמוצגות בדף הבית. אותו סימן מים שמוגדר עבור תצוגה מקדימה.";

	$mgrlang['mem_del_sub']					= "האם אתה בטוח שברצונך למחוק מנוי זה?";

	$mgrlang['dsp_op_ex']					= "רשיון מורחב";

	

	# 4.1.2

	$mgrlang['contributors_media']			= "מדיית תורמים";

	

	# 4.1

	$mgrlang['dp_watermark']				= "סימן מים (Watermark)";

	$mgrlang['dp_watermark_d']				= "בחר סימן מים להציג על גירסא זו כשתורד. הגדר כ'כלום' (none) אם אתה לא רוצה סימן מים.";

	$mgrlang['order_item_id'] 				= "מזהה";

	$mgrlang['order_item_file'] 			= "קובץ";

	$mgrlang['order_item_ofile'] 			= "קובץ מקורי";

	$mgrlang['order_item_afile'] 			= "קובץ מצורף";

	$mgrlang['geo_location_area']			= "איזור מיקום גיאוגרפי";

	$mgrlang['geo_on_off']					= "מידע על מיקום גיאוגרפי on/off";

	$mgrlang['geo_on_off_d']				= "הדלק/כבה את תכונות 'מידע על מיקום גיאוגרפי'.";

	$mgrlang['geo_width']					= "רוחב מפה";

	$mgrlang['geo_width_d'] 				= "רוחב המפה שברצונך להציג.";

	$mgrlang['geo_height'] 					= "גובה מפה";

	$mgrlang['geo_height_d'] 				= "גובה המפה שברצונך להציג.";

	$mgrlang['geo_zoom'] 					= "זום מפה";

	$mgrlang['geo_zoom_d'] 					= "קבע את רמת זום המפה, ככל שהמספר גבוה יותר כך הוא קרוב יותר לפני הקרקע.";

	$mgrlang['geo_pin_color'] 				= "צבע סיכה";

	$mgrlang['geo_pin_color_d'] 			= "קבע את צבע הסיכה/מארקר (לדוגמא: שחור, לבן, אדום, וכו..).";

	$mgrlang['geo_map_type'] 				= "סוג מפה";

	$mgrlang['geo_map_type_d']				= "הגדר את סוג המפה שברצונך להציג.";

	$mgrlang['geo_map_type_roadmap'] 		= "מפת כבישים";

	$mgrlang['geo_map_type_satellite']		 = "לווין";

	$mgrlang['geo_map_type_terrain']		= "שטח";

	$mgrlang['geo_map_type_hybrid']			= "שילוב (מפת כבישים + לווין)";

	$mgrlang['zoom_location_area']			= "איזור זום תמונה";

	$mgrlang['zoom_on_off']					= "זום תמונה דלוק/כבוי";

	$mgrlang['zoom_on_off_d']				= "הדלק או כבה את תכונת זום לתמונה.";

	$mgrlang['zoom_lens_size']				= "גודל עדשות זום התמונה";

	$mgrlang['zoom_lens_size_d'] 			= "הכנס גודל עבור עדשות הזום.";

	$mgrlang['zoom_border_size'] 			= "גודל גבולות זום התמונה";

	$mgrlang['zoom_border_size_d'] 			= "הכנס גודל גבול עבור עדשות הזום.";

	$mgrlang['zoom_border_color'] 			= "צבע גבולות זום התמונה";

	$mgrlang['zoom_border_color_d'] 		= "הכנס צבע עבור צבע גבול זום התמונה.";

	$mgrlang['gen_del_manually']			= "שלח ידנית";

	$mgrlang['gen_del_orig']				= "שלח קובץ מקורי";

	$mgrlang['gen_create_auto_mes']			= "'צור אוטומטית' יעבוד רק כאשר הקובץ המקורי שהועלה הוא JPG. ייתכן ולשרת המארח יש מגבלות זיכרון שיימנעו את 'צור אוטומטית' מלעבוד על תמונות גדולות. אם אתה נתקל בבעיות זיכרון בקש מIf you run into memory problems you will need to have your host increase your memory limit.";

	$mgrlang['dsp_f_delivery'] 				= "שיטת משלוח";

	$mgrlang['dsp_f_delivery_d'] 			= "בחר איך הקובץ יישלח ללקוח.";

	$mgrlang['setup_measurement'] 			= "יחידת מדידה";

	$mgrlang['setup_measurement_d'] 		= "בחר את יחידת המדידה שתוצג.";

	$mgrlang['setup_measurement_i'] 		= "אינצ'ים";

	$mgrlang['setup_measurement_c'] 		= "סנטימטרים";

	$mgrlang['fileupload_title'] 			= "העלאת קובץ";

	$mgrlang['fileupload_details'] 			= "העלה תמונות שיכולות לשמש באיזור התוכן שלמעלה.";

	$mgrlang['fileupload_source'] 			= "מקור: (סמן כדי למחוק פריט)";

	$mgrlang['approvalStatus0']				= "ממתין לאישור";

	$mgrlang['approvalStatus1']				= "מאושר";

	$mgrlang['approvalStatus2']				= "נכשל באישור";

	$mgrlang['media_f_astatus']				= "סטטוס אישור";

	$mgrlang['media_f_astatus_d']			= "סטטוס אישור עבור מדיה זו.";

	$mgrlang['media_f_astatus_mes']			= "הודעה/סיבה";

	$mgrlang['subnav_contr_sales']			= "מכירות תורמים";

	$mgrlang['subnav_contr_sales_d']		= "מכירות ועמלות שחייבים לתורמים.";

	$mgrlang['mem_tab17']					= "מכירות";

	$mgrlang['no_sales']					= "אין מכירות";

	$mgrlang['mem_f_profviews']				= "צפיות בפרופיל";

	$mgrlang['mem_f_profviews_d']			= "מספר הפעמים שהפרופיל הציבורי של משתמשים אלו נצפה.";	

	$mgrlang['webset_f_cos']				= "עמלה על הורדות מינויים";

	$mgrlang['webset_f_cos_d']				= "עמלת תורמים עבור כל הורדה של לקוח עם מינוי.";

	$mgrlang['one_download']				= "1 הורדה";

	$mgrlang['gen_pay']						= "שלם";

	$mgrlang['mark_as_paid']				= "סמן כשולם";

	$mgrlang['mark_as_unpaid']				= "סמן כלא שולם";

	$mgrlang['no_member']					= "משתמש לא קיים!";

	$mgrlang['send_cmo']					= "שלח חשבון/כסף הזמנה";

	$mgrlang['pay_paypal']					= "שלם דרך PayPal";

	$mgrlang['com_pay_from']				= "תשלום עמלה";

	$mgrlang['commission_display']			= "הצג עמלות עם סטטוסים אלו";

	$mgrlang['contributor_sale']			= "מכירות תורם";

	$mgrlang['contributor_sale_d']			= "פרטים על מכירות ועמלות תורם.";

	$mgrlang['contrsales_f_ps_d']			= "סטטוס התשלום לתורם עבור עמלה זו.";

	$mgrlang['contrsales_f_on_d']			= "ההזמנה בה קרתה העמלה.";

	$mgrlang['contrsales_f_com']			= "עמלה";

	$mgrlang['contrsales_f_com_d']			= "סכום רווח העמלה שנעשתה במכירה זו.";	

	$mgrlang['contrsales_f_sd']				= "תאריך מכירה";

	$mgrlang['contrsales_f_sd_d']			= "תאריך בה קרתה ההזמנה.";

	$mgrlang['contrsales_f_dp']				= "תאריך סומן ששולם";

	$mgrlang['contrsales_f_dp_d']			= "התאריך בו עמלה זו סומנה כמשולמת.";	

	$mgrlang['contrsales_f_mem_d']			= "תורם שהוא הבעלים של המדיה שנמכרה.";	

	$mgrlang['contrsales_f_io']				= "פריט שהוזמן";

	$mgrlang['contrsales_f_io_d']			= "פריט שהוזמן והרוויח עמלה.";	

	$mgrlang['media_f_albums']				= "אלבומים";

	$mgrlang['media_f_albums_d']			= "אלבומי משתמשים שמדיה זו תוצג בהם.";

	

	# 4.0.9

	$mgrlang['mem_f_disname']				= "שם תצוגה";

	$mgrlang['mem_f_disname_d']				= "השם שיוצג באיזורים ציבוריים. אם שדה זה נשאר ריק, השם האמיתי יוצג.";	

	$mgrlang['mem_f_showcase']				= "תורם לתצוגה";

	$mgrlang['mem_f_showcase_d']			= "אפשר למשתמש זה להיות תורם לתצוגה. משתמשים כאלו יהיו מודגשים בדף התורמים ובאיזורי התורמים לתצוגה האחרים.";

	$mgrlang['membership_f_album']			= "צור אלבומים";

	$mgrlang['membership_f_album_d']		= "אפשר למשתמשים בתוכנית הרשמה זו ליצור אלבומים משלהם בחשבון.";	

	$mgrlang['webset_featured_media'] 		= "איזור מדיה נבחרת";

	$mgrlang['gen_milliseconds'] 			= "מילישניות";

	$mgrlang['landf_f_hpf_width']			= "רוחב";

	$mgrlang['landf_f_hpf_width_d']			= "רוחב דף בית של תמונות נבחרות.";	

	$mgrlang['landf_f_hpf_crop']			= "חתוך ל";

	$mgrlang['landf_f_hpf_crop_d']			= "חתוך תמונות מקודמות בדף הבית לגובה זה.";	

	$mgrlang['landf_f_hpf_fadespeed']		= "מהירות דהייה (fade)";

	$mgrlang['landf_f_hpf_fadespeed_d']		= "הזמן במילישניות שלוקח לתמונה להיעלם או להופיע.";	

	$mgrlang['landf_f_hpf_inverval']		= "פרק זמן";

	$mgrlang['landf_f_hpf_inverval_d']		= "זמן במילישניות שתמונה מוצגת לפני שעוברים לתמונה הבאה.";

	$mgrlang['landf_f_hpf_ddelay']			= "השהיית פרטים";

	$mgrlang['landf_f_hpf_ddelay_d']		= "זמן במילישניות שפופ-אפ עם פרטים משתהה לפני הצגתם.";	

	$mgrlang['landf_f_hpf_ddis']			= "זמן תצוגת פרטים";

	$mgrlang['landf_f_hpf_ddis_d']			= "זמן במילישניות שפופ-אפ פרטים מוצג לפני שהוא מוסתר חזרה.";

	

	# 4.0.8

	$mgrlang['media_filemissing']			= "קובץ חסר";

	

	# 4.0.7

	$mgrlang['setup_f_iptcutf']				= "קידוד UTF8 IPTC";

	$mgrlang['setup_f_iptcutf_d']			= "השתמש ב- UTF8 encoding ב- מידע IPTC שנקרא מהתמונות שלך. אם ישנה בעיה בקריאת תווים מיוחדים, בטל אפשרות זו.";

	

	# 4.0.6

	$mgrlang['files_processed']				= "קבצים שעובדו";

	$mgrlang['elapsed']						= "עברו";

	$mgrlang['remaining']					= "נותרו";

	$mgrlang['createBillScratch']			= "צור חשבון מחדש";

	$mgrlang['createBillLater']				= "צור חשבון מהזמנות 'חייב אותי מאוחר יותר' של משתמשים";

	$mgrlang['landf_f_tospage']				= "אפשר דף 'תנאי שימוש'";

	$mgrlang['landf_f_pppage']				= "אפשר דף 'מדיניות פרטיות'";

	$mgrlang['landf_f_papage']				= "אפשר דף 'הסכם רכישה'";

	

	# 4.0.5

	$mgrlang['gen_imp_media']				= "בחר קבצים לייבא או";

	$mgrlang['gen_files2']					= "קובץ/ים";

	$mgrlang['gen_folders']					= "תיקיה/יות";

	$mgrlang['gen_importmes']				= "בחר קבצים שברצונך לייבא במצבור זה או לחץ על 'העלה מדיה חדשה' כדי להוסיף קבצים חדשים.";	

	$mgrlang['all_media']					= "כל המדיה";

	$mgrlang['orph_media']					= "מדיה מיותמת";

	$mgrlang['last_batch']					= "מצבור אחרון שיובא";

	$mgrlang['added_today']					= "התווסף היום";

	$mgrlang['added_week']					= "התווסף בשבוע האחרון";

	$mgrlang['added_month']					= "התווסף בחודש האחרון";

	$mgrlang['featured_media']				= "מדיה נבחרת";

	$mgrlang['create_group']				= "צור קבוצה";

	$mgrlang['gen_other']					= "אחר";

	$mgrlang['gen_photo']					= "תמונה";

	$mgrlang['gen_video']					= "וידאו";

	$mgrlang['choose_thumb_mes']			= "לחץ על כפתור 'העלה' כדי לבחור קובץ עבור תמונה מוקטנת";

	$mgrlang['min_size_mes']				= "מומלץ גודל מינימלי של 800x600px";

	$mgrlang['add_range']					= "הוסף טווח";

	$mgrlang['gen_sharpen']					= "חדד";

	$mgrlang['gen_na']						= "N/A";  // Needs translation

	$mgrlang['landf_f_login']				= "הצג התחבר/צור חשבון";  // Needs translation

	$mgrlang['landf_f_login_d']				= "הצג קישור או כפתור של התחברות או יצירת חשבון.";  // Needs translation

	

	# 4.0.4

	$mgrlang['gen_import_done_mes']			= "המדיה שלך יובאה לתוך האתר.";

	$mgrlang['gen_import_done_mes2']		= "אנא בדוק אזהרות באדום.";

	$mgrlang['gen_manage_media']			= "נהל את המדיה שלי";

	

	# 4.0.1

	$mgrlang['galleries_f_sort_d']			= "מיון ברירת-מחדל בגלריה זו.";

	$mgrlang['recommended']					= "מומלץ";

	

		

	# COPYRIGHT

	$mgrlang['powered_by']					= "Powered by";

	$mgrlang['copyright']					= "Copyright &copy; 2013 Ktools.net LLC, All Rights Reserved."; // COPYRIGHT MUST STAY AND CANNOT BE REMOVED



	# LOGIN AREA

	$mgrlang['login_title'] 				= "התחבר";

	$mgrlang['login_box_header']			= "איזור ניהול";

	$mgrlang['login_username']				= "שם משתמש";

	$mgrlang['login_password']				= "סיסמא";

	$mgrlang['login_recover_email_body']	= "מתחת מופיע מידע ההתחברות לאיזור הניהול עבור";

	$mgrlang['login_mes_01']				= "אימייל זה אינו קיים בבסיס הנתונים.";

	$mgrlang['login_mes_02']				= "מידע עבור התחברות נשלח אליך במייל";

	$mgrlang['login_mes_03']				= "שחזר את המידע הנחוץ להתחברות";

	$mgrlang['login_email_address']			= "כתובת מייל";

	$mgrlang['login_mes_04']				= "אם לא קיבלת את המידע הנחוץ להתחברות בתוך 5 דקות, אנא בדוק את 'חוסם הספאם' שלך, וודא שהוא לא חוסם אי-מיילים מאת";

	$mgrlang['login_back'] 					= "חזור לדף ההתחברות";

	$mgrlang['login_mes_05']				= "ההתחברות נכשלה. אנא נסה שנית.";

	$mgrlang['login_mes_06']				= "התנתקת.";

	$mgrlang['login_mes_07']				= "אתה חייב להיות מחובר כדי לצפות בדף זה.";

	$mgrlang['login_mes_08']				= "פג תוקפו של ה- session שלך.";

	$mgrlang['login_mes_09']				= "אנא התחבר";

	$mgrlang['login_remember']				= "זכור אותי";

	$mgrlang['login_recover']				= "שחזר מידע התחברות";

	$mgrlang['login_enter']					= "התחבר";

	$mgrlang['login_send']					= "שלח";

	$mgrlang['login_check']					= "בודק שם משתמש/סיסמא...";

	$mgrlang['login_loggedin']				= "התחבר";

	$mgrlang['login_loggedout']				= "התנתק";

	$mgrlang['login_tab1']					= "התחבר";

	$mgrlang['login_tab2']					= "שחזר סיסמא";

	$mgrlang['login_echeck']				= "בודק כתובת מייל...";

	

	# GENERAL AREAS

	$mgrlang['support_ticket']				= "כרטיס תמיכה";

	$mgrlang['ticket']						= "כרטיס";

	$mgrlang['email']						= "אימייל";

	$mgrlang['start']						= "התחל";

	$mgrlang['enc_decry']					= "קודד/פענח";

	$mgrlang['start_importing']				= "התחל יבוא";

	$mgrlang['start_upload']				= "התחל העלאה"; 

	$mgrlang['gen_preview']					= "תצוגה מקדימה";

	$mgrlang['gen_attach_file']				= "צרף קובץ";

	$mgrlang['gen_each']					= "כל אחד";

	$mgrlang['gen_downloads']				= "הורדות";

	$mgrlang['gen_dl_caps']					= "הורדות";

	$mgrlang['gen_bills']					= "חשבונות";

	$mgrlang['gen_lu_caps']					= "עודכן לאחרונה";

	$mgrlang['gen_notice']					= "הודעה";

	$mgrlang['gen_language']				= "שפה";

	$mgrlang['gen_tips']					= "טיפ";

	$mgrlang['gen_debug']					= "פאנל דיבאג";

	$mgrlang['gen_pageload']				= "דף נטען ב-";

	$mgrlang['gen_seconds']					= "שניות";

	$mgrlang['gen_off']						= "כבוי";

	$mgrlang['gen_server']					= "שרת";

	$mgrlang['change_batch']				= "תוכל לשנות את מעלה המצבור שבשימוש ב-";

	$mgrlang['gen_fileex']					= "סיומות קבצים מקובלות";

	$mgrlang['gen_cud']						= "פרטי משתמשים נוכחיים";

	$mgrlang['gen_addon']					= "תוספים מותקנים";

	$mgrlang['gen_settings']				= "הגדרות";

	$mgrlang['gen_curpage']					= "דף נוכחי";

	$mgrlang['gen_logout']					= "התנתק";

	$mgrlang['gen_logged_as']				= "מחובר כ- ";

	$mgrlang['gen_open_pub']				= "אתר ציבורי";

	$mgrlang['gen_edit_pub']				= "ערוך מצב";

	$mgrlang['gen_help']					= "עזרה & תמיכה";

	$mgrlang['gen_mgr_title']				= "איזור ניהול";

	$mgrlang['gen_info']					= "הצג מידע נוסף.";

	$mgrlang['gen_demo_mode']				= "תוכנה זו רצה במצב דמו. ייתכן ותכונות מסויימות מנוטרלות.";

	$mgrlang['gen_b_search']				= "חפש";

	$mgrlang['gen_search']					= "חפש";

	$mgrlang['gen_search_results']			= "חפש תוצאות עבור";

	$mgrlang['gen_b_cancel']				= "בטל";

	$mgrlang['gen_b_save']					= "שמור";

	$mgrlang['gen_b_help']					= "עזרה";

	$mgrlang['gen_b_update']				= "עדכן";

	$mgrlang['gen_b_clear_sort']			= "נקה כל המיונים";

	$mgrlang['gen_add_new']					= "הוסף חדש";

	$mgrlang['gen_sel_all']					= "בחר הכל";

	$mgrlang['gen_desel_all']				= "אל תבחר הכל";

	$mgrlang['gen_print_this']				= "דף זה בלבד";

	$mgrlang['gen_print_all']				= "כל הדפים";

	$mgrlang['gen_b_print']					= "הדפס";

	$mgrlang['gen_print']					= "הדפס";

	$mgrlang['gen_prints']					= "הדפסות";

	$mgrlang['gen_subs']					= "מנויים";

	$mgrlang['gen_prod']					= "מוצר";

	$mgrlang['gen_prods']					= "מוצרים";

	$mgrlang['gen_pack']					= "חבילה";

	$mgrlang['gen_packs']					= "חבילות";

	$mgrlang['gen_digital']					= "דיגיטלי";

	$mgrlang['gen_coll']					= "אוסף";

	$mgrlang['gen_colls']					= "אוספים";

	$mgrlang['gen_media_types']				= "סוגי מדיה";

	$mgrlang['gen_sub']						= "מנוי";

	$mgrlang['gen_b_prepare']				= "מכין";

	$mgrlang['gen_delete_sel']				= "מחק שנבחרו";

	$mgrlang['gen_edit']					= "ערוך פריט זה";

	$mgrlang['gen_assets']					= "צפה במדיה";

	$mgrlang['gen_default']					= "ברירת מחדל";

	$mgrlang['gen_short_view']				= "צפה";

	$mgrlang['gen_short_upload']			= "העלה";

	$mgrlang['gen_short_edit']				= "ערוך";

	$mgrlang['gen_thumbnail']				= "תמונה מוקטנת";

	$mgrlang['gen_vidsample']				= "דגימת וידאו";

	$mgrlang['gen_short_delete']			= "מחק";

	$mgrlang['gen_activate']				= "הפעל";

	$mgrlang['gen_deactivate']				= "בטל";

	$mgrlang['gen_upload']					= "העלה לגלריה זו";

	$mgrlang['gen_delete']					= "מחק פריט זה";

	$mgrlang['gen_b_cancel2']				= "בטל";

	$mgrlang['gen_b_next']					= "הבא";

	$mgrlang['gen_b_previous']				= "קודם";

	$mgrlang['gen_b_purge_al']				= "נקה לפני";

	$mgrlang['gen_b_print']					= "הדפסה";

	$mgrlang['gen_b_approve']				= "אשר";

	$mgrlang['gen_b_approved']				= "מאושר";

	$mgrlang['gen_b_napproved']				= "לא מאושר";

	$mgrlang['gen_b_display']				= "תצוגה";

	$mgrlang['gen_b_download']				= "הורד (CVS)";

	$mgrlang['gen_add_lang']				= "שפות נוספות";

	$mgrlang['gen_sys_name']				= "מערכת";

	$mgrlang['gen_loading']					= "טוען...";

	$mgrlang['gen_addonerror']				= "תוסף זה לא מותקן";

	$mgrlang['gen_mes_locked']				= "נעול";

	$mgrlang['gen_shortcuts']				= "קיצורי דרך ספרייה";

	$mgrlang['gen_details']					= "פרטים";

	$mgrlang['gen_missing']					= "חסר";

	$mgrlang['gen_langfiles']				= "קבצי שפה";

	$mgrlang['gen_alerts']					= "התראות";

	$mgrlang['gen_noalerts']				= "אין התראות כרגע";

	$mgrlang['gen_dbbackup']				= "בסיס הנתונים ביצע גיבוי:";

	$mgrlang['gen_dbbackup2']				= "גיבוי בסיס נתונים";

	$mgrlang['gen_langerr1']				= "נכשל בטעינת ספריית שפה";

	$mgrlang['gen_langerr2']				= "ספריית שפה או קובץ הגדרות חסרים";

	$mgrlang['gen_saving']					= "שומר";

	$mgrlang['gen_mylinks']					= "הקישורים שלי";

	$mgrlang['gen_editlinks']				= "ערוך קישורים שלי...";

	$mgrlang['gen_meswin']					= "חלון הודעה";

	$mgrlang['gen_t_none']					= "כלום";

	$mgrlang['gen_t_all']					= "הכל";

	$mgrlang['gen_t_sub_caps']				= "מינוי";

	$mgrlang['gen_t_exp_caps']				= "פג תוקף";

	$mgrlang['gen_t_dpdr_caps']				= "ליום / נשאר";

	$mgrlang['gen_t_show_all']				= "הצג הכל";

	$mgrlang['gen_t_show_sel']				= "הצג שנבחרו";

	$mgrlang['gen_t_ae_grps']				= "ערוך קבוצות";

	$mgrlang['gen_t_select']				= "בחר";

	$mgrlang['gen_t_actions']				= "פעולות";

	$mgrlang['gen_empty']					= "אין פריטים לתצוגה. לחץ על כפתור 'הוסף חדש' כדי להוסיף פריט.";

	$mgrlang['gen_empty_short']				= "אין פריטים לתצוגה.";

	$mgrlang['gen_b_new']					= "צור חדש";

	$mgrlang['gen_b_ed']					= "ערוך";

	$mgrlang['gen_b_sa']					= "בחר הכל";

	$mgrlang['gen_b_sn']					= "בחר כלום";

	$mgrlang['gen_b_remove_sel']			= "הסר מה שנבחר";

	$mgrlang['gen_refresh']					= "רענן";

	$mgrlang['gen_b_grps']					= "קבוצות";

	$mgrlang['gen_group']					= "קבוצה";

	$mgrlang['gen_qty']						= "Qty";

	$mgrlang['gen_b_panels']				= "יישומונים";

	$mgrlang['gen_b_dup']					= "שכפל";

	$mgrlang['gen_b_grps_alt']				= "קבוצות";

	$mgrlang['gen_b_back']					= "חזור";

	$mgrlang['gen_b_back_alt']				= "חזור ל-";

	$mgrlang['gen_b_del']					= "מחק";

	$mgrlang['gen_b_headers']				= "מידע";

	$mgrlang['gen_b_filters']				= "מסננים";

	$mgrlang['gen_b_memberships']			= "תוכניות הרשמה";

	$mgrlang['gen_b_sav']					= "שמור";

	$mgrlang['gen_b_submit']				= "שלח";

	$mgrlang['gen_b_close']					= "סגור";

	$mgrlang['mes_to_mem']					= "הודעה למשתמש";

	$mgrlang['gen_b_upload']				= "העלה";

	$mgrlang['gen_b_assign']				= "צרף";

	$mgrlang['gen_b_unassign']				= "בטל צירוף";

	$mgrlang['gen_b_try_again']				= "נסה שוב";

	$mgrlang['gen_mes_delsuc']				= "פריט/ים נמחק/ו בהצלחה!";

	$mgrlang['gen_mes_dupsuc']				= "פריט שוכפל בהצלחה!";

	$mgrlang['gen_mes_noitem']				= "לא נבחרו פריטים למחיקה!";

	$mgrlang['gen_mes_noitem2']				= "לא נמחקו פריטים, כי לא ניתן למחוק את רשומת ברירת המחדל!";

	$mgrlang['gen_mes_newsave']				= "הפריט החדש נוסף!";

	$mgrlang['gen_mes_changesave']			= "השינויים נשמרו!";

	$mgrlang['gen_mes_sortclear']			= "כל המיונים אופסו!";

	$mgrlang['gen_pleaseenter']				= "אנא הכנס";

	$mgrlang['gen_pleaseselect']			= "אנא בחר";

	$mgrlang['gen_beforesave']				= "לפני שמירה";

	$mgrlang['gen_passgen']					= "מייצר סיסמאות";

	$mgrlang['gen_page']					= "דף";

	$mgrlang['gen_of']						= "מתוך";

	$mgrlang['gen_items']					= "פריטים בסה\"כ";

	$mgrlang['gen_showing']					= "מציג";

	$mgrlang['gen_group_mult']				= "מספר פריטי קבוצה";

	$mgrlang['gen_perpage']					= "לעמוד";

	$mgrlang['gen_error_reported']			= "תקלה דווחה: ";

	$mgrlang['gen_error_01']				= "נתיב ספריה זו אינו קיים. אנא עדכן את הנתיב לספריה ב- הגדרות->התקנת תוכנה:";

	$mgrlang['gen_error_02']				= "התיקיה הבאה חייבת להיות ניתנת לכתיבה כדי להשתמש ב- PhotoStore:";

	$mgrlang['gen_error_03']				= "לתוכנה זו אין מספר סידורי פעיל.";

	$mgrlang['gen_error_04']				= "התוכנה לא הופעלה כשורה ו/או בעלת מפתחות שגויים. אנא צור קשר <a href='http://www.ktools.net/' target='_blank'>Ktools.net</a> בשביל מידע נוסף.";

	$mgrlang['gen_error_05']				= "לא ניתן לבחור את הטבלאות בבסיס הנתונים. וודא כי בסיס הנתונים מותקן כראוי.";

	$mgrlang['gen_error_06']				= "תיקיית הנכסים (assets) חייבת להיות עם הרשאות כתיבה.";

	$mgrlang['gen_error_07']				= "תיקיית ההתקנה עדיין קיימת. היא צריכה להימחק מטעמי אבטחה.";

	$mgrlang['gen_error_07b']				= "קבצי ההתקנה photostore.zip עדיין קיימים. זה צריך להימחק מטעמי אבטחה.";

	$mgrlang['gen_error_08']				= "תיקיית הנכסים לא קיימת.";

	$mgrlang['gen_error_09']				= "PHP 4.2 ומעלה חייב להיות מותקן.";

	$mgrlang['gen_error_10']				= "GD Library 2 לא מותקן.";

	$mgrlang['gen_error_11']				= "התוכנה נתקלה בתקלה חמורה: ";

	$mgrlang['gen_error_12']				= "איזור 'עורך גלריה' דורש את פונקציית rename() של PHP.";

	$mgrlang['gen_error_13']				= "איזור 'עורך גלריה' דורש את פונקציית mkdir() של PHP.";

	$mgrlang['gen_error_14']				= "איזור 'עורך גלריה' דורש את פונקציית rmdir() של PHP.";

	$mgrlang['gen_error_15']				= "התוכנה דורשת את פונקציית realpath() של PHP.";

	$mgrlang['gen_error_16']				= "התוכנה דורשת את פונקציית opendir() של PHP.";

	$mgrlang['gen_error_17']				= "התוכנה דורשת את פונקציית readdir() של PHP.";

	$mgrlang['gen_error_18']				= "תיקיית ה- incoming לא קיימת ב:";

	$mgrlang['gen_error_19']				= "תיקיית ה- incoming לא ניתנת לכתיבה:";

	$mgrlang['gen_error_20']				= "השרת אינו מקצה מספיק מקום כדי ליצור תצוגה מקדימה לתמונה זו. התהליך צריך בערך";

	$mgrlang['gen_error_21']				= "התוכנה לא רשומה לשרת זה.";

	$mgrlang['gen_error_22']				= "אין לך הרשאת גישה לאיזור זה.";

	$mgrlang['gen_error_23']				= "השרת אינו מקצה מספיק מקום עבור עיבוד תמונה זו. שנה את גודל התמונה ונסה להעלות אותה שוב.";

	$mgrlang['gen_error_24']				= "התרחשה תקלה";

	$mgrlang['gen_error_25']				= "קובץ גדול מדי. גודל מקסימלי";

	$mgrlang['gen_error_26']				= "לא זוהה Adobe Flash Player. הוא חייב להיות מותקן בשביל תכונה זו.";

	$mgrlang['gen_error_27']				= "תיקיית הדמויות (avatars) לא קיימת או שלא ניתנת לכתיבה. לא תוכל להעלות או לערוך דמויות עד שזה ייפתר.";

	$mgrlang['gen_error_28']				= "מצטערים. לא ניתן לטעון יישומון כי allow_url_fopen כבוי ב- PHP בשרת.";

	$mgrlang['gen_error_29']				= "נעשו שינויים שמצריכים לעדכן את ספירת מדיית הגלריה.";

	$mgrlang['gen_error_29b']				= "לחץ כאן כדי לעשות כך עכשיו.";

	$mgrlang['gen_error_29c']				= "ספירת מדיית הגלריה עודכנה. ניתן לסגור את החלון.";

	$mgrlang['gen_search_start']			= "התחל להקליד כדי לחפש.";

	$mgrlang['gen_hidden']					= "הסתר במצב דמו";

	$mgrlang['gen_disabled']				= "תכונה זו מבוטלת במצב דמו.";

	$mgrlang['gen_suredelete']				= "האם אתה בטוח שברצונך למחוק פריט/ים זה?";

	$mgrlang['gen_suredeact']				= "האם אתה בטוח שברצונך להפסיק פעילות פריט/ים זה?";

	$mgrlang['gen_mes_leaverow']			= "אתה חייב להשאיר לפחות שורה אחת.";

	$mgrlang['gen_wb_everyone']				= "כולם";

	$mgrlang['gen_wb_limited']				= "מוגבל";

	$mgrlang['gen_wb_specific']				= "רק משתמשים/קבוצות משתמשים/תוכניות הרשמה ספציפיים";

	$mgrlang['gen_wb_members']				= "משתמשים רשומים";

	$mgrlang['gen_mem_gals']				= "גלריות משתמשים רשומים";

	$mgrlang['gen_wb_groups']				= "קבוצות משתמשים רשומים";

	$mgrlang['gen_wb_memberships']			= "תוכניות הרשמה";

	$mgrlang['gen_item_for']				= "פריט זה זמין עבור";

	$mgrlang['gen_medianame_photos']		= "תמונות";

	$mgrlang['gen_medianame_videos']		= "קבצי וידאו";

	$mgrlang['gen_medianame_music']			= "מוזיקה";

	$mgrlang['gen_medianame_files']			= "קבצים";

	$mgrlang['gen_medianame_media']			= "מדיה";

	$mgrlang['gen_media_caps']				= "מדיה";

	$mgrlang['gen_dd_caps']					= "תאריך הורדה";

	$mgrlang['gen_ds_caps']					= "גודל הורדה";

	$mgrlang['gen_dat_caps']				= "סוג גישת הורדה";

	$mgrlang['gen_gmes_1']					= "בחר מהרשימה את הקבוצות שברצונך להציג.";

	$mgrlang['gen_gmes_2']					= "כרגע לא נוצרו קבוצות. לחץ על כפתור ערוך קבוצות כדי לנהל קבוצות.";

	$mgrlang['gen_gmes_3']					= "חייבים להיות לפחות 2 פריטים פעילים כדי להשתמש בתכונת המיון!";

	$mgrlang['gen_h_dd']					= "גרור פריטים כדי לשנות את הסדר";

	$mgrlang['gen_to']						= "ל";

	$mgrlang['gen_from']					= "מ";

	$mgrlang['gen_tfg']						= "הקבוצות הבאות";

	$mgrlang['gen_tfr']						= "האיזורים הבאים";

	$mgrlang['gen_clear_groups']			= "נקה כל הצירופים הקודמים לקבוצות";

	$mgrlang['gen_au_itg']					= "צרף לקבוצות";

	$mgrlang['gen_sort']					= "מיון";

	$mgrlang['gen_actions']					= "פעולות";

	$mgrlang['gen_related_actions']			= "אפשרויות קשורות";

	$mgrlang['gen_width']					= "רוחב";

	$mgrlang['gen_height']					= "גובה";

	$mgrlang['gen_wb_noneavail']			= "דבר לא זמין";

	$mgrlang['gen_block_ip']				= "IP חסום";

	$mgrlang['gen_block_ip2']				= "כדי לבטל חסימת IP זה, לך ל-";

	$mgrlang['gen_block_domain']			= "Domain חסום";

	$mgrlang['gen_block_domain2']			= "כדי לבטל חסימת ה- domain לך ל-";

	$mgrlang['gen_block_email']				= "כתובת אימייל חסומה";

	$mgrlang['gen_block_email2']			= "כדי לבטל את חסימת אימייל זה לך ל-";

	$mgrlang['gen_active']					= "הגדר כפעיל";

	$mgrlang['gen_inactive']				= "הגדר כלא פעיל";

	$mgrlang['gen_approved']				= "הגדר כמאושר";

	$mgrlang['gen_unapproved']				= "הגדר כלא מאושר";

	$mgrlang['gen_selitems']				= "פריטים שנבחרו";

	$mgrlang['gen_allitems']				= "כל הפריטים";

	$mgrlang['gen_example']					= "דוגמא";

	$mgrlang['gen_or']						= "או";

	$mgrlang['gen_chr_period']				= "נקודה";

	$mgrlang['gen_chr_comma']				= "פסיק";

	$mgrlang['gen_chr_dash']				= "מקו";

	$mgrlang['gen_chr_equals']				= "שווה";

	$mgrlang['gen_chr_slash']				= "לוכסן";

	$mgrlang['gen_chr_semicolon']			= "נקודה פסיק";

	$mgrlang['gen_chr_colon']				= "נקודותיים";

	$mgrlang['gen_chr_apostrophe']			= "גרש";

	$mgrlang['gen_copy']					= "העתק";

	$mgrlang['gen_togroups']				= "צרף/בטל צירוף לקבוצות";

	$mgrlang['gen_toregions']				= "צרף/בטל צירוף לאיזורים";

	$mgrlang['gen_tostatus']				= "הגדר פעיל/לא פעיל";

	$mgrlang['gen_toapproved']				= "הגדר מאושר/לא מאושר";

	$mgrlang['gen_toregions']				= "צרף לאיזורים";

	$mgrlang['gen_files']					= "קבצים";

	$mgrlang['gen_total']					= "סה\"כ";

	$mgrlang['gen_mb']						= "MB";

	$mgrlang['gen_kb']						= "KB";

	$mgrlang['gen_gb']						= "GB";

	$mgrlang['gen_ilg']						= "כלול תיקייה מקומית";

	$mgrlang['gen_contrap']					= "תורם קובע מחירים";

	$mgrlang['gen_adminap']					= "השתמש במחירי מנהל";

	$mgrlang['gen_adminap2']				= "מנהל קובע מחירים";

	$mgrlang['gen_base_price']				= "מקורי";

	$mgrlang['gen_base_photoprice']			= "מחיר מדיה";

	$mgrlang['gen_base_photocredits']		= "פרטי מדיה";

	$mgrlang['gen_base_credits']			= "מקורי";

	$mgrlang['gen_closed']					= "סגור";

	$mgrlang['gen_open']					= "פתוח";

	$mgrlang['gen_pending']					= "ממתין";

	$mgrlang['gen_processing']				= "מעבד";

	$mgrlang['gen_refunded']				= "החזר כספי";

	$mgrlang['gen_paid']					= "שולם";

	$mgrlang['gen_visitor']					= "אורח";

	$mgrlang['get_never']					= "לעולם לא";

	$mgrlang['get_set_date']				= "הגדר תאריך";

	$mgrlang['get_direct_link']				= "קישור ישיר";	

	$mgrlang['gen_active']					= "פעיל";

	$mgrlang['gen_inactive']				= "לא פעיל";

	$mgrlang['gen_active_d']				= "הפוך פריט זה לפעיל. פריטים לא פעילים לא יופיעו באתר הציבורי.";

	$mgrlang['gen_approved']				= "מאושר";

	$mgrlang['gen_failed']					= "נכשל";

	$mgrlang['gen_cancelled']				= "בוטל";

	$mgrlang['gen_bill']					= "חייב מאוחר יותר";

	$mgrlang['gen_incomplete']				= "לא הושלם";

	$mgrlang['gen_none']					= "ללא";

	$mgrlang['gen_unpaid']					= "לא שולם";

	$mgrlang['gen_shipped']					= "נשלח";

	$mgrlang['gen_pshipped']				= "נשלח חלקית";

	$mgrlang['gen_backordered']				= "לא במלאי";

	$mgrlang['gen_shipnone']				= "N/A";

	$mgrlang['gen_shipparent']				= "השתמש בסטטוס משלוח של ההזמנה";

	$mgrlang['gen_notshipped']				= "לא נשלח";

	$mgrlang['gen_notap']					= "N/A";

	$mgrlang['gen_reset']					= "אפס";

	$mgrlang['gen_remove']					= "הסר";

	$mgrlang['gen_f_discount']				= "מחיר הנחה";

	$mgrlang['gen_f_discount_d']			= "מחיר עבור כל פריט אחרי הראשון. השאר ריק כדי להשאיר את המחיר אותו דבר כמו הפריט הראשון.";

	$mgrlang['gen_f_discountc']				= "הנחת קרדיטים";

	$mgrlang['gen_f_discountc_d']			= "קרדיטים עבור כל פריט אחרי הראשון. השאר ריק כדי להשאיר את אותו מספר קרדיטים כמו הפריט הראשון.";

	$mgrlang['gen_f_weight']				= "משקל פריט";

	$mgrlang['gen_f_weight_d']				= "משקל פריט זה. משמש כדי לחשב עלויות משלוח, במידת הצורך.";

	$mgrlang['gen_f_lab_code']				= "Lab Code";

	$mgrlang['gen_f_lab_code_d']			= "An item code the print lab may use to identify this product.";

	$mgrlang['gen_f_lab']					= "Notify Lab";

	$mgrlang['gen_f_lab_d']					= "If any orders are placed for this item send a copy of the order email to the selected lab.";

	$mgrlang['gen_f_perm']					= "הרשאות";

	$mgrlang['gen_f_perm_d']				= "בחר את המשתמשים, קבוצות משתמשים או תוכניות הרשמה שיכולים לראות פריט זה.";

	$mgrlang['gen_f_itemcode']				= "קוד פריט";

	$mgrlang['gen_f_itemcode_d']			= "הכנס קוד פריט (אופציונלי) עבור שימוש פנימי.";

	$mgrlang['gen_t_sortorder']				= "הזמנה";

	$mgrlang['gen_t_subject']				= "נושא";

	$mgrlang['gen_t_id']					= "מזהה";

	$mgrlang['gen_t_lb_name']				= "שם תיבה אישית";

	$mgrlang['gen_t_name']					= "שם";

	$mgrlang['gen_t_active']				= "פעיל";

	$mgrlang['gen_t_status']				= "סטטוס";

	$mgrlang['gen_t_submitted']				= "הועלה";

	$mgrlang['gen_t_member']				= "משתמש";

	$mgrlang['gen_member']					= "משתמש";

	$mgrlang['gen_t_media']					= "מדיה";

	$mgrlang['gen_t_quantity']				= "כמות";

	$mgrlang['gen_t_contributor']			= "תורם";

	$mgrlang['gen_t_commission']			= "עמלה";

	$mgrlang['gen_t_downexp']				= "פג תוקף הורדה";

	$mgrlang['gen_t_ship_status']			= "סטטוס משלוח פריט";

	$mgrlang['gen_t_cost']					= "עלות";

	$mgrlang['gen_t_type']					= "סוג";

	$mgrlang['gen_t_item']					= "פריט";

	$mgrlang['gen_t_updated']				= "עודכן";

	$mgrlang['gen_t_date']					= "תאריך";

	$mgrlang['gen_t_customer']				= "לקוח";

	$mgrlang['gen_t_title']					= "כותרת";

	$mgrlang['gen_t_code']					= "קוד";

	$mgrlang['gen_t_remaining']				= "נותר";

	$mgrlang['gen_t_region']				= "איזור";

	$mgrlang['gen_t_gname']					= "שם קבוצה";	

	$mgrlang['gen_t_homepage']				= "דף בית";

	$mgrlang['gen_group_new_header']		= "הוסף קבוצה חדשה";

	$mgrlang['gen_group_edit_header']		= "ערוך קבוצה";

	$mgrlang['gen_group_new_message']		= "בעזרת הטופס הבא תוכל להוסיף קבוצות חדשות. קבוצות עוזרות לך לארגן תאריכים באיזור הניהול.";

	$mgrlang['gen_group_edit_message']		= "ערוך קבוצה זו ולחץ על כפתור 'שמור שינויים'.";

	$mgrlang['gen_group_f_name']			= "שם קבוצה";

	$mgrlang['gen_group_f_name_d']			= "שם קבוצה זו.";

	$mgrlang['gen_group_f_flag']			= "אייקון";

	$mgrlang['gen_group_f_flag_d']			= "אייקון בשביל לזהות קבוצה זו באיזור הניהול.";

	$mgrlang['gen_each']					= "ea.";

	$mgrlang['gen_b_exit_grps']				= "צא מקבוצות";

	$mgrlang['gen_b_exit_msps']				= "צא מתוכניות הרשמה";

	$mgrlang['gen_groups']					= "קבוצות";

	$mgrlang['gen_new_group']				= "הוסף קבוצה חדשה";

	$mgrlang['gen_edit_group']				= "ערוך קבוצה";

	$mgrlang['gen_group_list']				= "רשימת קבוצות";	

	$mgrlang['gen_empty_groups']			= "אין קבוצות עבור איזור זה. לחץ על הטאב 'הוסף קבוצה חדשה' כדי להוסיף קבוצה.";

	$mgrlang['gen_group_header']			= "הצג פריטים שבקבוצות אלו";

	$mgrlang['gen_opgroup']					= "קבוצת אפשרויות";

	$mgrlang['gen_option']					= "אפשרות";

	$mgrlang['gen_empty_options']			= "אין אפשרויות בקבוצה זו. לחץ על טאב 'אפשרות חדשה' כדי להוסיף אפשרות.";

	$mgrlang['gen_newoption']				= "אפשרות חדשה";

	$mgrlang['gen_editoption']				= "ערוך אפשרות";

	$mgrlang['gen_listoption']				= "רשימת אפשרויות";

	$mgrlang['gen_option_f_name']			= "שם אפשרות";

	$mgrlang['gen_option_f_name_d']			= "שם לאפשרות זו.";

	$mgrlang['gen_price']					= "מחיר";

	$mgrlang['gen_option_f_price_d']		= " עלות להוסיף או להוריד ממחיר הפריט המקורי כאשר האפשרות נבחרת. השאר ריק בשביל 'בלי שינויים'.";

	$mgrlang['gen_credits']					= "קרדיטים";

	$mgrlang['gen_option_f_credits_d']		= "עלות קרדיטים כדי להוסיף או להוריד ממחיר הקרדיטים של הפריט המקורי כאשר אפשרות זו נבחרה. השאר ריק כדי לא לעשות שינוי.";

	$mgrlang['gen_icon']					= "אייקון";

	$mgrlang['gen_no_orders']				= "אין הזמנות לתצוגה.";

	$mgrlang['gen_add_ship']				= "משלוח נוסף";

	$mgrlang['gen_add_ship_d']				= "כל משלוח נוסף עבור פריט שתרצה להוסיף. זה יתווסף אחרי שמשלוח כבר חושב אבל לפני אחוזי איזור ומיסים.";

	

	$mgrlang['gen_option_type1']			= "רשימה נפתחת";

	$mgrlang['gen_option_type2']			= "תיבות סימון";

	$mgrlang['gen_option_type3']			= "כפתורי רדיו";

	$mgrlang['gen_option_h_sort']			= "מיין";

	$mgrlang['gen_option_h_name']			= "שם קבוצת אפשויות";

	$mgrlang['gen_option_h_type']			= "סוג";

	$mgrlang['gen_option_h_active']			= "פעיל";

	$mgrlang['gen_option_h_required']		= "נצרך";

	$mgrlang['gen_tab_prod_shots']			= "צילומי מוצר";

	$mgrlang['gen_tab_pricing']				= "תמחור";

	$mgrlang['gen_tab_attach']				= "צרף";

	$mgrlang['gen_tab_options']				= "אפשרויות";

	$mgrlang['gen_tab_groups']				= "קבוצות";

	$mgrlang['gen_tab_discounts']			= "הנחות";

	$mgrlang['gen_tab_details']				= "פרטים";

	$mgrlang['gen_tab_options']				= "אפשרויות";

	$mgrlang['gen_tab_parent']				= "אב";

	$mgrlang['gen_tab_event_details']		= "פרטי אירוע";

	$mgrlang['gen_tab_permissions']			= "הרשאות";

	$mgrlang['gen_tab_advanced']			= "מתקדם";

	$mgrlang['gen_tab_shipping']			= "משלוח";

	$mgrlang['gen_tab_contributors']		= "תורמים";

	$mgrlang['gen_tab_galleries']			= "גלריות";

	$mgrlang['gen_bill_item']				= "חייב פריט";

	$mgrlang['gen_dig_ver']					= "גירסאות דיגיטליות";

	$mgrlang['gen_del_logo']				= "האם אתה בטוח כי ברצונך למחוק לוגו זה?";

	$mgrlang['gen_expired']					= "פג תוקף";

	$mgrlang['gen_not_active']				= "עדיין לא פעיל";

	$mgrlang['gen_publish']					= "פרסם";

	$mgrlang['gen_featurepage']				= "דף נבחר זה כבוי ב- הגדרות > מראה והרגשה > דפים";

	$mgrlang['gen_hpfeaturearea']			= "דף בית נבחר זה כבוי ב- הגדרות > מראה והרגשה > דף בית";

	$mgrlang['gen_import']					= "ייבא מצבור";

	$mgrlang['gen_leave_blank_mes']			= "השאר ריק כדי להשתמש בברירת מחדל";

	$mgrlang['leave_blank_quan']			= "השאר ריק בשביל כמות בלתי מוגבלת";

	$mgrlang['gen_unlimited']				= "לא מוגבל";

	$mgrlang['gen_nfs']						= "לא למכירה";

	$mgrlang['gen_rm']						= "Rights Managed";

	$mgrlang['gen_free']					= "חינם";

	$mgrlang['gen_cu']						= "צור קשר";

	$mgrlang['quick_create_gal']			= "צור במהירות גלריה חדשה";

	$mgrlang['not_enough_mem']				= "תמונות מוקטנות לא נוצרו. השרת לא מקצה מספיק מקום כדי ליצור תמונות מוקטנות לגודל תמונה שכזה. ניתן ליצור תמונה מוקטנת ידנית ב- ספריה->מדיה. אם ברצונך שתמונות מוקטנות יווצרו אוטומטית אנא צור קשר עם חברת האכסון, כדי שיגדילו את מגבלת זכרון ה- PHP.";

	$mgrlang['no_auto_thumbs']				= "לא ניתן ליצור אוטומטית תמונות מוקטנות עבור סוג קובץ זה";

	$mgrlang['manual_add_thumb']			= "אתה יכול להוסיף ידנית תמונה מוקטנת עבור קובץ, ב- ספריה->מדיה.";

	$mgrlang['no_folder_import']			= "תיקיה לא קיימת בשרת. תהליך הייבוא נכשל.";

	$mgrlang['folder_not_writable']			= "לא ניתן לכתוב בתיקיה. תהליך הייבוא נכשל.";

	$mgrlang['error_moving_file']			= "נכשל. לא ניתן להעביר את הקובץ לתיקיית הספרייה.";

	$mgrlang['file_too_large']				= "הקובץ גדול מכדי להעביר למקום חיצוני. מדלג על הקובץ.";

	$mgrlang['error_move_ext']				= "לא ניתן להעביר את הקובץ למקום האיכסון";

	$mgrlang['no_curl']						= "תוסף CURL לא נטען.";

	$mgrlang['failed_as3_move']				= "נכשל בהעברת קובץ ל- AmazonS3.";

	$mgrlang['failed_cf_move']				= "נכשל בהעברת קובץ ל CloudFiles. תהליך הוידוא נכשל.";

	$mgrlang['failed_icon_copy']			= "לא ניתן להעתיק אייקון לתיקיית הספרייה.";

	$mgrlang['failed_thumb_copy']			= "לא ניתן להעתיק תמונה מוקטנת לתיקיית הספרייה.";

	$mgrlang['failed_sample_copy']			= "לא ניתן להעתיק דגימה לתיקיית הספרייה.";

	$mgrlang['gen_local']					= "מקומי";

	$mgrlang['folder_exists']				= "תיקיה זו קיימת כבר במקום איכסון אחר";

	$mgrlang['folder_exists2']				= "אנא שנה את מקום האיכסון או צור תיקיה חדשה.";

	$mgrlang['rmps']						= "Rights Managed Pricing Scheme";

	$mgrlang['original_prof_set']			= "השאר ריק כדי להשתמש בהגדרות של הפרופיל המקומי.";

	$mgrlang['remove_customization']		= "הסר התאמה אישית";

	$mgrlang['quan_available']				= "כמות זמינה";

	$mgrlang['no_preview']					= "תצוגה מקדימה לא יכולה להיווצר מ-";

	$mgrlang['select_files_import']			= "לפני שתוכל להמשיך עליך לבחור את הקבצים שברצונך לייבא";

	$mgrlang['gen_error_occ']				= "אירעה תקלה!";

	$mgrlang['starting_import']				= "מתחיל בתהליך ייבוא";

	$mgrlang['gen_stop']					= "עצור";

	$mgrlang['add_more_to_batch']			= "הוסף עוד למצבור זה";

	$mgrlang['import_new_batch']			= "ייבא מצבור חדש";

	$mgrlang['files_uploaded']				= "הקבצים הועלו. ניתן לייבא אותם לתוך האתר.";

	$mgrlang['upload_new_media']			= "העלה מדיה חדשה";

	$mgrlang['gen_continue']				= "המשך";

	$mgrlang['choose_save_loc']				= "בחר היכן לשמור את הקבצים";

	$mgrlang['import_folder']				= "ייבא מדיה לתיקיה הזו";

	$mgrlang['create_folder']				= "צור תיקיה חדשה";

	$mgrlang['enc_folder']					= "הצפן תיקיה";

	$mgrlang['enc_folder_d']				= "הצפן את שם התיקיה והקבצים שבתוכה.";

	$mgrlang['ext_store_limit']				= "כדי לאכסן קבצים מחוץ לשרת המקומי הם חייבים להיות מתחת ל-";

	$mgrlang['large_files_skipped']			= "קבצים גדולים יותר ידולגו.";

	$mgrlang['local_lib']					= "תיקיה מקומית";	

	$mgrlang['f_clips']						= "קודי קיצור (Clips)";

	$mgrlang['f_clips_d']					= "השתמש בקודים אלו בטקסט שלך והם יוחלפו בערכים האמיתיים.";	

	$mgrlang['f_content']					= "תוכן";

	$mgrlang['f_content_d']					= "התוכן שיופיע בדף זה או במקטע התוכן.";

	$mgrlang['gen_desc_caps']				= "תיאור";

	$mgrlang['gen_cost_caps']				= "מחיר";

	$mgrlang['gen_order_num_caps']			= "מספר הזמנה";

	$mgrlang['gen_order_date_caps']			= "תאריך הזמנה";

	$mgrlang['gen_order_date']				= "תאריך הזמנה";

	$mgrlang['gen_in_bill_caps']			= "כלול בחיוב";

	$mgrlang['error']						= "תקלה";

	$mgrlang['gen_invoice_number']			= "מספר חשבונית";

	$mgrlang['gen_due_date']				= "תאריך פירעון";

	$mgrlang['gen_due_date_caps']			= "תאריך פירעון";

	$mgrlang['gen_subtotal']				= "סיכום ביניים";

	$mgrlang['gen_total']					= "סה\"כ";

	$mgrlang['gen_total_caps']				= "סה\"כ";

	$mgrlang['gen_tax']						= "מס";

	$mgrlang['gen_shipping']				= "משלוח";

	$mgrlang['gen_discounts']				= "הנחות";

	$mgrlang['gen_cred_discounts']			= "הנחות קרדיט";

	$mgrlang['gen_cred_total']				= "סה\"כ קרדיטים";

	$mgrlang['gen_payment_status']			= "סטטוס תשלום";

	$mgrlang['gen_payment_status_caps']		= "סטטוס תשלום";

	$mgrlang['please_choose_mem']			= "אנא בחר משתמש";

	$mgrlang['display_data']				= "הצג מידע זה בטבלה";

	$mgrlang['customer']					= "לקוח";

	$mgrlang['gen_invoice_date']			= "תאריך חשבונית";

	$mgrlang['gen_payment_date']			= "תאריך תשלום";

	$mgrlang['gen_pd_caps']					= "תאריך תשלום";

	$mgrlang['gen_shipping_status']			= "סטטוס משלוח";

	$mgrlang['gen_exit_search']				= "צא מהחיפוש";

	$mgrlang['gen_notes']					= "הערות";

	$mgrlang['gen_notes_d']					= "הערות לשימוש עצמי בלבד, ולא יוצגו באתר הציבורי.";

	$mgrlang['gen_description']				= "תיאור";

	$mgrlang['gen_description_d']			= "תיאור פריט זה.";

	$mgrlang['gen_no_notes']				= "לפריט זה אין הערות";

	$mgrlang['max_prod_shots']				= "הגעת למקסימום הצילומים האפשריים לצרף למוצר זה.";	

	$mgrlang['tab_pricing']					= "תמחור";

	$mgrlang['tab_contents']				= "תכנים";

	$mgrlang['tab_prod_shots']				= "צילומי מוצר";

	$mgrlang['tab_details']					= "פרטים";

	$mgrlang['tab_groups']					= "קבוצות";

	$mgrlang['tab_advanced']				= "מתקדם";

	$mgrlang['tab_advertise']				= "פרסום";

	$mgrlang['gen_quantity']				= "כמות";

	$mgrlang['shipping_methods']			= "דרכי משלוח";

	$mgrlang['shipping_methods_d']			= "דרכי משלוח זמינות עבור איזור זה.";

	$mgrlang['shipping_adj']				= "התאמות משלוח";

	$mgrlang['shipping_adj_d']				= "אחוז מסכום המשלוח לצורך גביה, במשלוח למדינה זו. ברירת מחדל היא 100%. מעל 100% מוסיף עלות ומתחת 100% מוריד עלות.";

	$mgrlang['gen_taxable']					= "חייב במס";

	$mgrlang['gen_taxable_d']				= "הגדר פריט זה חייב במס.";

	$mgrlang['all_galleries']				= "כל הגלריות";

	$mgrlang['percentage']					= "אחוז";

	$mgrlang['dollar_value']				= "ערך הדולר";

	$mgrlang['min']							= "מינימום";

	$mgrlang['max']							= "מקסימום";

	$mgrlang['attachment']					= "קובץ מצורף";

	$mgrlang['attachment_mwsg']				= "צרף למדיה בתוך הגלריות הנבחרות";

	$mgrlang['galleries']					= "גלריות";

	$mgrlang['galleries_attach']			= "בחר לאלו גלריות יש גישה להציג פריט זה, ואז לבחור את סוג הקובץ המצורף.";

	$mgrlang['body']						= "תוכן";

	$mgrlang['body_email_d']				= "תוכן האימייל.";	

	$mgrlang['enc_name']					= "שם מוצפן";	

	$mgrlang['gen_active_date']				= "תאריך פעילות";

	$mgrlang['gen_expire_date']				= "תאריך תפוגה";

	$mgrlang['gen_owner']					= "בעלים";

	$mgrlang['gen_more']					= "עוד";

	$mgrlang['allow_contr']					= "אפשר תרומות";

	$mgrlang['access']						= "גישה";

	$mgrlang['gen_now']						= "עכשיו";

	$mgrlang['gen_never']					= "מעולם לא";

	$mgrlang['max_icons']					= "הגעת למקסימום האייקונים שניתן לצרף לפריט זה.";

	$mgrlang['exit_members']				= "יציאת משתמשים";

	$mgrlang['changes_saved']				= "השינויים נשמרו.";

	$mgrlang['gen_size']					= "גודל";

	$mgrlang['gen_quality']					= "איכות";

	$mgrlang['gen_watermark']				= "סימן מים";

	$mgrlang['gen_top']						= "למעלה";

	$mgrlang['gen_bottom']					= "תחתית";

	$mgrlang['gen_over']					= "מעל (over)";

	$mgrlang['gen_hidden']					= "מוסתר";

	$mgrlang['gen_unknown']					= "לא ידוע";

	$mgrlang['gen_feature']					= "קדם";

	$mgrlang['gen_unfeature']				= "לא לקדם";	

	$mgrlang['gen_set_galicon']				= "קבע אייקון לגלריה";

	$mgrlang['gen_unset_galicon']			= "בטל אייקון לגלריה";	

	$mgrlang['gen_gallery']					= "גלריה";	

	$mgrlang['add_media']					= "הוסף מדיה";	

	$mgrlang['active_inactive']				= "פעיל/לא פעיל";	

	$mgrlang['gen_featured']				= "מקודם";

	$mgrlang['set_gal_icon']				= "קבע כאייקון לגלריה";

	$mgrlang['edit_details']				= "ערוך פרטים";

	$mgrlang['not_available']				= "לא זמין יותר";

	$mgrlang['digital_collections']			= "אוספים דיגיטליים";

	$mgrlang['gen_code']					= "קוד";

	$mgrlang['gen_code_d']					= "קוד לשים בטמפלייט כדי להראות את מקטע התוכן הזה.";

	$mgrlang['gen_content_name']			= "שם איזור תוכן";

	$mgrlang['gen_content_name_d']			= "שם איזור תוכן זה. ישמש גם ככותרת באתר הציבורי, במקרים מסויימים.";

	$mgrlang['gen_content_type']			= "סוג תוכן";

	$mgrlang['gen_content_type_d']			= "בחר אם זה מקטע תוכן או דף שלם.";

	$mgrlang['gen_content_block']			= "מקטע תוכן";

	$mgrlang['gen_content_page']			= "דף חדש";

	$mgrlang['gen_contr_sell']				= "מכירות תורם";

	$mgrlang['gen_contr_sell_d']			= "אפשר לתורמים למכור פריט זה.";	

	$mgrlang['gen_com_type']				= "סוג עמלה";

	$mgrlang['gen_com_type_d']				= "בחר אחוז או ערך דולרי עבור עמלה לפריט זה.";

	$mgrlang['gen_com_value']				= "ערך עמלה";

	$mgrlang['gen_com_value_d']				= "ערך הדולר עבור עמלה על פריט זה.";

	$mgrlang['gen_com_level']				= "רמת עמלה";

	$mgrlang['gen_com_level_d']				= "אחוז מהסה\"כ שהעמלה תשולם עליו ברמת עמלה של תורמים.";	

	$mgrlang['gen_mmprice']					= "מחיר מינימום/מקסימום";

	$mgrlang['gen_mmprice_d']				= "מחיר מינימום שתורם יכול לקבל עבור פריט זה. השאר ריק או 0 עבור בלי מחיר מינימום.";

	$mgrlang['gen_mmcredits']				= "מינימום/מקסימום קרדיטים";

	$mgrlang['gen_mmcredits_d']				= "מינימום קרדיטים שתורם יכול לקבל על פריט זה. השאר ריק או הכנס 0 עבור כלום.";

	$mgrlang['attach_sel_gal']				= "צרף לגלריות נבחרות";

	$mgrlang['attach_media_in_gal']			= "צרף למדיה שבתוך גלריות נבחרות";

	$mgrlang['attach_both']					= "צרף לגלריות ולמדיה שבתוכן";

	$mgrlang['gen_orders']					= "הזמנות";

	$mgrlang['gen_dis_in_all']				= "הצג בהכל";

	$mgrlang['gen_days']					= "ימים";

	$mgrlang['gen_weeks']					= "שבועות";

	$mgrlang['gen_months']					= "חודשים";

	$mgrlang['gen_years']					= "שנים";

	$mgrlang['gen_adv_hp']					= "פרסום בעמוד הבית";

	$mgrlang['gen_adv_hp_d']				= "פרסם פריט זה בעמוד הבית של האתר.";

	$mgrlang['gen_adv_fp']					= "פרסם בדף המקודם";

	$mgrlang['gen_originals']				= "מקוריים";

	$mgrlang['gen_not_enough_mem']			= "אין מספיק זיכרון ליצור אייקון עבור תמונה זו.";	

	$mgrlang['manually_approve']			= "אישור ידני";

	$mgrlang['auto_approve']				= "אישור אוטומטי";	

	$mgrlang['one_credit']					= "קרדיט 1";	

	$mgrlang['mem_selector']				= "מיון משתמש(member selector)";	

	$mgrlang['add_ship_mod']				= "הוסף יחידת משלוח";

	$mgrlang['add_sub']						= "הוסף מינוי";

	$mgrlang['select_sub_mes']				= "בחר מינוי להוספה לחשבון משתמשים זה ולחץ על כפתור 'שמור שינויים'.";

	$mgrlang['clear_regions']				= "נקה כל איזורים מצורפים קודמים";

	$mgrlang['batch_upload']				= "העלה מצבור";

	$mgrlang['upload_photos']				= "העלה תמונות";

	$mgrlang['gen_message']					= "הודעה";

	$mgrlang['create_new_ticket']			= "צור כרטיס חדש";

	$mgrlang['gen_items2']					= "פריטים";

	$mgrlang['bill_date_caps']				= "תאריך חיוב";

	$mgrlang['int_notes']					= "הערות פנימיות";

	$mgrlang['int_notes_d']					= "הערות לשימוש פנימי שלא יוצגו באתר הציבורי.";	

	$mgrlang['edit_prof']					= "ערוך פרופיל";

	$mgrlang['flag']						= "דגל";

	

	$mgrlang['photographer_caps']			= "צלם";

	$mgrlang['email_caps']					= "אימייל";

	$mgrlang['compname_caps']				= "שם חברה";

	$mgrlang['website_caps']				= "אתר";

	$mgrlang['signup_caps']					= "נרשם";

	$mgrlang['lastlogin_caps']				= "התחבר לאחרונה";

	$mgrlang['membership_caps']				= "תוכנית הרשמה";

	$mgrlang['credits_caps']				= "קרדיטיםS";

	$mgrlang['groups_caps']					= "קבוצות";

	$mgrlang['notes_caps']					= "הערות";

	$mgrlang['address_caps']				= "כתובות";

	$mgrlang['create_automat']				= "צור אוטומטית";

	

	

	

	

	

	# WEBSITE SETTINGS

	$mgrlang['webset_tab1']					= "הגדרות כללי";

	$mgrlang['webset_tab2']					= "חנות/עגלה";

	$mgrlang['webset_tab3']					= "מידע ליצירת קשר";

	$mgrlang['webset_tab4']					= "התראות";

	$mgrlang['webset_tab5']					= "חפש בהתקנה";

	$mgrlang['webset_tab9']					= "משתמשים רשומים";

	$mgrlang['webset_tab7']					= "הגדרות מתקדמות";

	$mgrlang['webset_tab8']					= "הרשמה";

	$mgrlang['webset_tab10']				= "הגדרות חשבונית";

	$mgrlang['webset_f_status']				= "סטטוס האתר";

	$mgrlang['webset_f_status_d']			= "הדלק/כבה את האתר. כשהוא כבוי תוכל להציג הודעה למבקרים.";

	$mgrlang['webset_on']					= "דלוק";

	$mgrlang['webset_off']					= "כבוי";

	$mgrlang['webset_message']				= "הודעה";

	$mgrlang['webset_f_title']				= "כותרת האתר";

	$mgrlang['webset_f_title_d']			= "הכותרת של האתר.";

	$mgrlang['webset_f_meta_desc']			= "תיאור האתר";

	$mgrlang['webset_f_meta_desc_d']		= "תיאור metatag של האתר.";

	$mgrlang['webset_f_meta_keys']			= "מילות מפתח של האתר";

	$mgrlang['webset_f_meta_keys_d']		= "מילות מפתח metatag של האתר.";

	$mgrlang['webset_f_use_gpi']			= "השתמש במידע של המדיה בשביל ה- Metatags";

	$mgrlang['webset_f_use_gpi_d']			= "החלף את ה- metatags של האתר עם מידע מתוך גלריה/תמונה/מדיה בדפי פרטים של גלריות.";

	$mgrlang['webset_f_signup_req']			= "הרשמה חובה";

	$mgrlang['webset_f_signup_req_d']		= "חייב לקוחות ליצור חשבון לפני רכישה מהאתר.";

	$mgrlang['webset_h_rating_system']		= "מערכת דירוג";

	$mgrlang['webset_f_rating_system']		= "מערכת דירוג";

	$mgrlang['webset_f_rating_system_d']	= "הדלק מערכת דירוג ואפשר למשתמשים לדרג תמונות ומדיה אחרת.";

	$mgrlang['webset_f_rating_system_lr']	= "דירוגי אורחים";

	$mgrlang['webset_f_rating_system_lr_d']	= "אפשר לאורחים לדרג תמונות ומדיה אחרת בלי שיהיה להם חשבון או שהם יהיו מחוברים.";

	$mgrlang['webset_h_comment_system']		= "מערכת הערות";

	$mgrlang['webset_f_comment_system']		= "מערכת הערות";

	$mgrlang['webset_f_comment_system_d']	= "הדלק את מערכת ההערות ואפשר למשתמשים לכתוב הערות על תמונות ומדיה אחרת.";

	$mgrlang['webset_f_comment_system_lr']	= "הערות אורחים";

	$mgrlang['webset_f_comment_system_lr_d']= "אפשר לאורחים לפרסם הודעות על תמונות ומדיה אחרת בלי שיהיה להם חשבון או שהם יהיו מחוברים.";

	$mgrlang['webset_h_tagging_system']		= "תיוג קהילתי";

	$mgrlang['webset_f_tagging_system']		= "תיוג קהילתי";

	$mgrlang['webset_f_tagging_system_d']	= "אפשר הוספת תגים/מילות מפתח לתמונות ומדיה אחרת שלך.";	

	$mgrlang['webset_f_tagging_system_lr']	= "תיוג אורח";

	$mgrlang['webset_f_tagging_system_lr_d']= "אפשר לאורחים להוסיף תיוגים לתמונה ומדיה אחרת בלי שיהיה להם חשבון, או בלי להיות מחוברים.";

	$mgrlang['webset_f_request_photo']		= "בקשת מדיה";

	$mgrlang['webset_f_request_photo_d']	= "אפשר לאורחים (אינם מחוברים) לבקש תמונה או מדיה אחרת ממך.";

	$mgrlang['webset_f_email_friend']		= "שלח מייל לחבר";

	$mgrlang['webset_f_email_friend_d']		= " אפשר למשתמשים לשלוח לחבר באימייל קישור לדף פרטי קובץ.";

	$mgrlang['webset_f_news']				= "דף חדשות";

	$mgrlang['webset_f_news_d']				= "הדלק את דף החדשות שבאתר.";

	$mgrlang['webset_f_search']				= "חיפוש";

	$mgrlang['webset_f_search_d']			= "הדלק את תכונת החיפוש ואפשר למבקרים לחפש תמונות או מדיה אחרת.";

	$mgrlang['webset_f_esearch']			= "חיפוש אירוע";

	$mgrlang['webset_f_esearch_d']			= "הצג את איזור החיפוש של האירוע.";

	$mgrlang['webset_f_contact']			= "דף צור קשר";

	$mgrlang['webset_f_contact_d']			= "הדלק את דף צור קשר ואפשר למבקרים ליצור קשר באמצעות הטופס.";	

	$mgrlang['webset_f_watch_lists']		= "רשימו צפייה";

	$mgrlang['webset_f_watch_lists_d']		= "אפשר למשתמשים ליצור רשימות צפייה כדי לצפות בגלריות עבור תמונות חדשות.";

	$mgrlang['webset_h_lightbox']			= "תיבות אישיות";

	$mgrlang['webset_f_lightbox']			= "תיבות אישיות";

	$mgrlang['webset_f_lightbox_d']			= "אפשר למשתמשים ליצור תיבות אישיות.";

	$mgrlang['webset_f_glightbox']			= "תיבות אישיות אורחים";

	$mgrlang['webset_f_glightbox_d']		= "אפשר לאורחים ליצור תיבות אישיות בלי ליצור חשבון.";	

	$mgrlang['webset_f_cart']				= "עגלת קנייה";

	$mgrlang['webset_f_cart_d']				= "עגלת קניות וסוג מערכת תשלומים.";

	$mgrlang['webset_f_credits']			= "עגלה מבוססת קרדיט";

	$mgrlang['webset_f_credits_d']			= "אפשר למשתמשים לרכוש קרדיטים שישמשו כדי לקנות מדיה או מוצרים.";

	$mgrlang['webset_f_delete_carts']		= "מחק עגלות נטושות";

	$mgrlang['webset_f_delete_carts_d']		= "מתי תרצה שעגלות נטושות יימחקו מבסיס הנתונים.";

	$mgrlang['webset_f_download_expire']	= "פקיעת תוקף הורדה";

	$mgrlang['webset_f_download_expire_d']	= "קישורי הורדה יפוג תוקפם זמן זה אחרי הרכישה.";

	$mgrlang['webset_f_dl_attempts']		= "נסיונות הורדה";

	$mgrlang['webset_f_dl_attempts_d']		= "מספר הפעמים שלקוח יכול להוריד את הפריט שרכש. עבור כל פריט.";

	$mgrlang['webset_f_dl_unlimited']		= "לא מוגבל";

	$mgrlang['webset_time_never']			= "לעולם לא";

	$mgrlang['webset_time_1d']				= "אחרי 1 יום";

	$mgrlang['webset_time_3d']				= "אחרי 3 ימים";

	$mgrlang['webset_time_5d']				= "אחרי 5 ימים";

	$mgrlang['webset_time_1w']				= "אחרי 1 שבוע";

	$mgrlang['webset_time_2w']				= "אחרי 2 שבועות";

	$mgrlang['webset_time_1m']				= "אחרי 1 חודש";

	$mgrlang['webset_time_2m']				= "אחרי 2 חודשים";

	$mgrlang['webset_time_3m']				= "אחרי 3 חודשים";

	$mgrlang['webset_time_6m']				= "אחרי 6 חודשים";

	$mgrlang['webset_time_1y']				= "אחרי 1 שנה";

	$mgrlang['webset_f_dle']				= "תוספות זמן לקישור הורדה";

	$mgrlang['webset_f_dle_d']				= "אפשר למשתמשים לחדש את קישורי ההורדה על-ידי וידוא הרכישה דרך מייל.";

	$mgrlang['webset_f_default_price']		= "מחיר ברירת מחדל";

	$mgrlang['webset_f_default_price_d']	= "אם לא הוגדר מחיר עבור פריט, זה יהיה המחיר.";

	$mgrlang['webset_f_default_credits']	= "קרדיטים ברירת מחדל";

	$mgrlang['webset_f_default_credits_d']	= "אם לא מצויין מספר קרדיטים עבור קובץ, זה יהיה המספר הזה.";

	$mgrlang['webset_f_auto_orders']		= "אישור אוטומטי של הזמנות";

	$mgrlang['webset_f_auto_orders_d']		= "אשר הזמנות אוטומטית כאשר הן משולמות. בטל כדי לאשר הזמנות ידנית.";

	$mgrlang['webset_f_min_total']			= "סיכום ביניים מינימלי";

	$mgrlang['webset_f_min_total_d']		= "סיכום ביניים של הזמנה חייב להיות לפחות הסכום הזה (לפני מס או משלוח) לפני שמישהו יכול להתחיל בתהליך תשלום.";

	$mgrlang['webset_f_support_email']		= "אימייל תמיכה";

	$mgrlang['webset_f_support_email_d']	= "מיילים שקשורים לנושאי תמיכה יישלחו לכתובת זו.";

	$mgrlang['webset_f_sales_email']		= "אימייל מכירות";

	$mgrlang['webset_f_sales_email_d']		= "מיילים שקשורים לנושאי מכירות יישלחו לכתובת זו.";

	$mgrlang['webset_f_address']			= "כתובת העסק";

	$mgrlang['webset_f_address_d']			= "משמש בשביל מידע ליצירת קשר ושליחת חשבון/כסף לתשלומי הזמנה.";

	$mgrlang['webset_f_business_country']	= "מדינת העסק";

	$mgrlang['webset_f_business_country_d']	= "שם המדינה בה נמצא העסק.";	

	$mgrlang['webset_f_business_name']		= "שם העסק";

	$mgrlang['webset_f_business_name_d']	= "שם העסק או האתר.";

	$mgrlang['webset_f_business_city']		= "עיר העסק";

	$mgrlang['webset_f_business_city_d']	= "העיר בה נמצא העסק.";

	$mgrlang['webset_f_business_state']		= "מדינה/פרובינצית העסק";

	$mgrlang['webset_f_business_state_d']	= "המדינה או הפרובינציה בה נמצא העסק.";

	$mgrlang['webset_f_business_zip']		= "מיקוד העסק";

	$mgrlang['webset_f_business_zip_d']		= "מיקוד העסק.";	

	$mgrlang['webset_f_notify_sale']		= "הזמנות חדשות";

	$mgrlang['webset_f_notify_sale_d']		= "עדכן אותי במייל כאשר יש הזמנה חדשה. אימיילים אלו ילכו לכתובת מייל המכירות.";

	$mgrlang['webset_f_notify_account']		= "חשבונות חדשים";

	$mgrlang['webset_f_notify_account_d']	= "עדכן אותי במייל כאשר נוצר משתמש חדש. אימיילים אלו ילכו לכתובת מייל התמיכה.";

	$mgrlang['webset_f_notify_profile']		= "עדכוני מידע משתמש";

	$mgrlang['webset_f_notify_profile_d']	= "עדכן אותי במייל כאשר משתמש שינה את הפרופיל שלו. אימיילים אלו ילכו לכתובת מייל התמיכה.";

	$mgrlang['webset_f_notify_rating']		= "דירוגים חדשים";

	$mgrlang['webset_f_notify_rating_d']	= "עדכן אותי במייל כאשר מישהו דירג תמונה או מדיה אחרת. אימיילים אלו ילכו לכתובת מייל התמיכה.";

	$mgrlang['webset_f_notify_comment']		= "הערות חדשות";

	$mgrlang['webset_f_notify_comment_d']	= "עדכן אותי במייל כאשר מישהו פרסם הערה חדשה על תמונה או מדיה אחרת. אימיילים אלו ילכו לכתובת מייל התמיכה.";

	$mgrlang['webset_f_notify_lightbox']	= "תיבה אישית חדשה";

	$mgrlang['webset_f_notify_lightbox_d']	= "עדכן אותי דרך האימייל כאשר מישהו יוצר תיבה חדשה. אימיילים אלו ילכו לכתובת מייל התמיכה.";

	$mgrlang['webset_f_grp_login']			= "קבוצות התחברות";

	$mgrlang['webset_f_grp_login_d']		= "משתמשים מצטרפים אוטומטית לקבוצות אלו רק כאשר הם מחוברים.";

	$mgrlang['webset_f_grp_signup']			= "קבוצות הרשמה גלובליות";

	$mgrlang['webset_f_grp_signup_d']		= "משתמשים מצטרפים אוטומטית לקבוצות הבאות כאשר הם נרשמים לחשבון בלי קשר באיזו תוכנית הרשמה הם בחרו.";

	$mgrlang['webset_f_forum_link']			= "קישור לפורום";

	$mgrlang['webset_f_forum_link_d']		= "קישור לפורום חיצוני או לוח bulletin.";	

	$mgrlang['webset_f_member_avatars']		= "דמויות משתמשים";

	$mgrlang['webset_f_member_avatars_d']	= "אפשר למשתמשים להעלות דמות, לוגו או תמונה כדי לייצג את עצמם.";	

	$mgrlang['webset_f_avatar_feature1']	= "אישור דמות";

	$mgrlang['webset_f_avatar_feature1_d']	= "נותן לך את האפשרות לראות ולאשר את הדמות לפני שזה מוצג ציבורית.";

	$mgrlang['webset_f_avatar_feature2']	= "גודל דמות מקסימלי";

	$mgrlang['webset_f_avatar_feature2_d']	= "רוחב או גובה מקסימלי של דמות שניתן להעלות.";

	$mgrlang['webset_f_avatar_feature3']	= "סוגי קבצי דמות";

	$mgrlang['webset_f_avatar_feature3_d']	= "סוג קובץ דמות שמשתמשים יכולים להעלות.";

	$mgrlang['webset_f_avatar_feature4']	= "גודל קובץ דמות מקסימלי";

	$mgrlang['webset_f_avatar_feature4_d']	= "גודל קובץ מקסימלי של דמות שמשתמש יכול להעלות.";		

	$mgrlang['webset_h_contr']				= "תורמים";

	$mgrlang['webset_f_contr_portfolios']	= "פרופילי תורמים";

	$mgrlang['webset_f_contr_portfolios_d']	= "אפשר לתורמים ליצור פורטפוליו של העבודה הטובה ביותר שלהם, ולהוסיף ביוגרפיה.";

	$mgrlang['webset_f_contr_showcase']		= "תורמים לתצוגה";

	$mgrlang['webset_f_contr_showcase_d']	= "תורמים לתצוגה (לראווה) הם אלו שהוגדרו כך בחשבון המשתמש שלהם.";	

	$mgrlang['webset_f_contr_showcase1']	= "כמה";

	$mgrlang['webset_f_contr_showcase1_d']	= "כמה תורמים תרצה שיהיו בתצוגה.";

	$mgrlang['webset_f_contr_showcase3']	= "בחר תורמים";

	$mgrlang['webset_f_contr_showcase3_d']	= "איך תורמים ייבחרו לתצוגה.";

	$mgrlang['webset_f_contr_showcase3op1']	= "אקראית (כל תורם)";

	$mgrlang['webset_f_contr_showcase3op2']	= "אקראית (רק אלו עם מדיה למכירה)";

	$mgrlang['webset_f_contr_showcase3op3']	= "אקראית (רק אלו עם ביוגרפיה)";

	$mgrlang['webset_f_contr_showcase3op4']	= "אקראית (רק אלו עם ביוגרפיה ומדיה למכירה)";

	$mgrlang['webset_f_contr_showcase3op5']	= "אני אבחר אותם ידנית";	

	$mgrlang['webset_f_contr_metatags']		= "השתמש במידע המשתמש ב- Metatags";

	$mgrlang['webset_f_contr_metatags_d']	= "השתמש במידע המשתמש עבור metatags בדפי פרופילי המשתמש.";

	$mgrlang['webset_f_contr_cd']			= "תורם CD/DVD";

	$mgrlang['webset_f_contr_cd_d']			= "אפשר לתורמים לשלוח תמונות ומדיה אחרת אל הלקוח על-גבי CD/DVD במקום הורדה מיידית.";

	$mgrlang['webset_f_contr_samples']		= "אפשר לתורם דוגמיות בלבד";

	$mgrlang['webset_f_contr_samples_d']	= "אפשר לתורמים להעלות רק דוגמיות של המדיה שלהם, ולשלוח את המקוריים אחרי הרכישה.";

	$mgrlang['webset_f_contr_cd2']			= "הגשה על-ידי CD/DVD";

	$mgrlang['webset_f_contr_cd2_d']		= "אפשר לתורמים לשלוח את התמונות והמדיה שלהם אליך על CD/DVD במקום להעלות אותם לאתר.";

	$mgrlang['webset_f_contr_cd2_mes']		= "הודעת הגשה על-ידי CD/DVD";

	$mgrlang['webset_f_contr_cd2_mes_d']	= "הודעה לתצוגה עבור תורמים ששולחים את המדיה שלהם על CD/DVD.";

	$mgrlang['webset_f_com_calc']			= "עמלות תורמים";

	$mgrlang['webset_f_com_calc_d']			= "עמלות תורמים מבוססות על רווח או על סך כל המכירה.";

	$mgrlang['webset_com_calc_op1']			= "רווח";

	$mgrlang['webset_com_calc_op2']			= "סה\"כ";

	$mgrlang['webset_f_notify_contrup']		= "העלאות תורמים";

	$mgrlang['webset_f_notify_contrup_d']	= "הודע לי במייל כאשר משתמש מעלה מדיה חדשה. מיילים אלו יגיעו לכתובת של מייל התמיכה.";

	$mgrlang['webset_f_poe']				= "Print Lab Contacts";

	$mgrlang['webset_f_poe_d']				= "Print lab names and email address. Allows you to send order emails for certain prints/products to a print lab or contact person automatically.";

	$mgrlang['webset_b_lab']				= "Add Lab Contact";

	$mgrlang['webset_lab_name']				= "שם";

	$mgrlang['webset_lab_email']			= "אימייל";

	$mgrlang['webset_f_invoice_prefix']		= "קידומת מספר חשבונית";

	$mgrlang['webset_f_invoice_prefix_d']	= "קוד לשים לפני מספר החשבונית.";

	$mgrlang['webset_f_invoice_next']		= "מספר החשבונית הבאה";

	$mgrlang['webset_f_invoice_next_d']		= "מספר סדרתי של החשבונית/הזמנה הבאה.";

	$mgrlang['webset_f_invoice_suffix']		= "בסוף מספר חשבונית";

	$mgrlang['webset_f_invoice_suffix_d']	= "קוד לשים בסוף מספר חשבונית.";

	$mgrlang['webset_f_invoice_preview']	= "תצוגה מקדימה למספר חשבונית";

	$mgrlang['webset_mes_invoice_warn']		= "מספר החשבונית הבא עשוי להיות נמוך יותר ממספר החשבונית הקודם שניתן.";

	$mgrlang['webset_mes_cart_warn']		= "אם עגלת הקניות מאופשרת עליך לבחור לפחות סוג אחד של עגלה.";

	$mgrlang['webset_f_compay']				= "תשלומי עמלה";

	$mgrlang['webset_f_compay_d']			= "אפשרויות זמינות למשתמשים כדי לקבל כל סוג של תשלומי עמלה.";

	$mgrlang['webset_f_compay_op1']			= "PayPal";

	$mgrlang['webset_f_compay_op2']			= "חשבון/כסף של הזמנה";

	$mgrlang['webset_f_compay_op3']			= "אחר";

	$mgrlang['webset_f_compay_op3b']		= "תוית (label)";

	$mgrlang['webset_f_contr_dvp']			= "תמחור גירסא מקורית";

	$mgrlang['webset_f_contr_dvp_d']		= "מי קובע את מחיר גירסאות דיגיטליות מקוריות שתורמים מעלים.";

	$mgrlang['webset_f_contr_col']			= "תמחור אוסף";

	$mgrlang['webset_f_contr_col_d']		= "מי מגדיר את המחירים על אוספים שתורמים יוצרים.";

	$mgrlang['webset_rss_news']				= "RSS חדשות";

	$mgrlang['webset_rss_news_d']			= "אפשר למבקרים להירשם לפיד RSS של חדשות האתר.";

	$mgrlang['webset_rss_galleries']		= "RSS גלריה";

	$mgrlang['webset_rss_galleries_d']		= "אפשר למבקרים להירשם לפיד RSS של גלריות.";

	$mgrlang['webset_rss_fmedia']			= "RSS מדיה מקודמת";

	$mgrlang['webset_rss_fmedia_d']			= "אפשר למבקרים להירשם לפיד RSS של מדיה מקודמת.";

	$mgrlang['webset_rss_search']			= "RSS חיפוש";

	$mgrlang['webset_rss_search_d']			= "אפשר למבקרים להירשם לפיד RSS של חיפושים שלהם.";

	$mgrlang['webset_rss_newest']			= "RSS פריטים חדשים";

	$mgrlang['webset_rss_newest_d']			= "אפשר למבקרים להירשם לפיד RSS של תמונות/קבצים החדשים ביותר.";

	$mgrlang['webset_rss_popular']			= "RSS פריטים פופולריים";

	$mgrlang['webset_rss_popular_d']		= "אפשר למבקרים להירשם לפיד RSS של תמונות/קבצים הפופולריים ביותר.";

	$mgrlang['webset_rss_records']			= "רשומות";

	$mgrlang['webset_rss_records_d']		= "מספר הרשומות לכלול בכל פיד RSS.";

	$mgrlang['webset_ticket_sys']			= "מערכת כרטיסי תמיכה";

	$mgrlang['webset_f_enable_ts']			= "אפשר מערכת כרטיסי תמיכה";

	$mgrlang['webset_f_enable_ts_d']		= "הפעל את מערכת כרטיסי תמיכה עבור משתמשי האתר.";

	$mgrlang['webset_f_coc']				= "עמלה על קרדיטים";

	$mgrlang['webset_f_coc_d']				= "עמלה לתורמים עבור כל קרדיט ששולם על-ידי לקוח.";

	$mgrlang['webset_pur_wcred']			= "אפשר רכישה עם קרדיטים על";	

	$mgrlang['webset_f_ont']				= "סוג מספר הזמנה";

	$mgrlang['webset_f_ont_d']				= "בחר אם מספרי הזמנה סידוריים או רנדומאליים.";	

	$mgrlang['webset_use_seq']				= "השתמש במספרי הזמנה אקראיים";

	$mgrlang['webset_use_rand']				= "השתמש במספרי הזמנה אקראיים";	

	$mgrlang['webset_f_non']				= "מספר ההזמנה הבא";

	$mgrlang['webset_f_non_d']				= "מספר ההזמנה הבא שייצא.";	

	$mgrlang['webset_f_puragree']			= "הסכם רכישה";

	$mgrlang['webset_f_puragree_d']			= "נצרך כי הלקוח יסכים להסכם הרכישה לפני ביצוע התשלום.";	

	$mgrlang['webset_f_credsys']			= "מערכת קרדיטים";	

	$mgrlang['webset_f_skipship']			= "דלג על משלוח";

	$mgrlang['webset_f_skipship_d']			= "דלג על עלויות משלוח עבור פריטים שנרכשו בקרדיטים.";	

	$mgrlang['webset_f_mships']				= "תוכניות הרשמה";

	$mgrlang['webset_f_mships_d']			= " הצג תוכניות הרשמה זמינות בטופס יצירת חשבון.";	

	$mgrlang['webset_h_info']				= "מידע";

	$mgrlang['webset_h_disabled']			= "מבוטל";

	$mgrlang['webset_h_request']			= "רשות";

	$mgrlang['webset_h_require']			= "חובה";

	$mgrlang['webset_h_info']				= "מידע";

	$mgrlang['webset_signup_argee']			= "הסכם הרשמה";	

	$mgrlang['webset_cur_based']			= "מבוסס מטבע";

	$mgrlang['webset_cred_based']			= "מבוסס קרדיטים";

	$mgrlang['webset_both']					= "שניהם";

	$mgrlang['webset_enabled']				= "מופעל";

	$mgrlang['webset_enable_subs']			= "אפשר מינויים";

	$mgrlang['webset_f_new_tags']			= "תגים חדשים";

	$mgrlang['webset_f_new_tags_d']			= "עדכן אותי דרך המייל אם מישהו פרסם תג חדש. המיילים האלו יגיעו לכתובת של מייל התמיכה.";	

	$mgrlang['webset_f_fileds']				= "שדות";

	$mgrlang['webset_f_fileds_d']			= "אפשר למבקרים לצמצם את החיפוש ע\"י בחירת שדות לחיפוש.";	

	$mgrlang['webset_f_types']				= "סוגים";

	$mgrlang['webset_f_types_d']			= "אפשר למבקרים לצמצם את החיפוש ע\"פ סוגי מדיה.";	

	$mgrlang['webset_f_orien']				= "אוריינטציה";

	$mgrlang['webset_f_orien_d']			= "אפשר למבקרים לצמצם את החיפוש ע\"פ אוריינטציה.";	

	$mgrlang['webset_f_color']				= "צבע";

	$mgrlang['webset_f_color_d']			= "אפשר למבקרים לצמצם את החיפוש ע\"פ צבע.";	

	$mgrlang['webset_f_dates']				= "תאריכים";

	$mgrlang['webset_f_dates_d']			= "אפשר למבקרים לצמצם את החיפוש ע\"פ טווח תאריכים.";	

	$mgrlang['webset_f_lictype']			= "סוג רשיון";

	$mgrlang['webset_f_lictype_d']			= "אפשר למבקרים לצמצם את החיפוש ע\"פ סוג רשיון.";	

	$mgrlang['webset_f_galleries']			= "גלריות";

	$mgrlang['webset_f_galleries_d']		= "אפשר למבקרים לצמצם את החיפוש ע\"פ גלריות בדף החיפוש.";	

	$mgrlang['webset_f_regfield']			= "שדות הרשמה";

	$mgrlang['webset_f_regfield_d']			= "איזה מידע תרצה ללכוד כשמשתמש נרשם?";

	

	

	# MEDIA

	$mgrlang['media_f_grp_d']				= "הוסף מדיה לקבוצות הבאות.";

	$mgrlang['media_f_keeporg']				= "שמור מקוריים";

	$mgrlang['media_f_keeporg_d']			= "בחר אם לשמור או למחוק מקוריים. אם לא תשמור מקוריים תצטרך להעלות אותם כאשר הם יירכשו.";

	$mgrlang['media_det_for_batch']			= "פרטים עבור מצבור";

	$mgrlang['media_f_sn']					= "מספר מיון";

	$mgrlang['media_f_sn_d']				= "מספר שנקבע בשביל מיון ידני.";

	$mgrlang['media_f_mr']					= "מודל שחרור";

	$mgrlang['media_f_mr_d']				= "האם לקובץ זה יש טופס מודל שחרור.";

	$mgrlang['media_f_copyright']			= "זכויות יוצרים";

	$mgrlang['media_f_copyright_d']			= "מידע זכויות יוצרים עבור קובץ זה.";	

	$mgrlang['media_f_usageres']			= "הגבלות שימוש";

	$mgrlang['media_f_usageres_d']			= "הגבלות שימוש כלשהן על הקובץ.";

	$mgrlang['media_f_groups']				= "קבוצות";

	$mgrlang['media_f_dc_d']				= "תאריך בו תמונה, וידאו או קובץ אלו נוצרו.";

	$mgrlang['media_f_da_d']				= "תאריך בו תמונה, וידאו או קובץ אלו נוספו לספרייה.";

	$mgrlang['media_f_mt_d']				= "בחר סוגי מדיה שיתאימו לקבצים אלו.";

	$mgrlang['media_save_first']			= "שמור תחילה כדי לראות שם קובץ ונתיב!";

	$mgrlang['media_mem_media']				= "מדיה של המשתמש";

	$mgrlang['media_exit_mem']				= "יציאת משתמשים";	

	$mgrlang['media_f_title']				= "כותרת";

	$mgrlang['media_f_title_d']				= "כותרת לקובץ מדיה זה.";	

	$mgrlang['media_f_titleimp_d']			= "קבע כותרת שתרצה להחיל על כל המדיה המיובאת במצבור זה.";		

	$mgrlang['media_f_description']			= "תיאור";

	$mgrlang['media_f_description_d']		= "תיאור עבור קובץ מדיה זה.";

	$mgrlang['media_f_descriptionimp_d']	= "קבע תיאור שתרצה להחיל על כל המדיה המיובאת במצבור זה.";

	$mgrlang['media_f_keywords']			= "מילות מפתח";

	$mgrlang['media_f_keywords_d']			= "מילות מפתח בשביל מדיה זו.";

	$mgrlang['media_f_keywordsimp_d']		= "בחר מילות מפתח שתרצה להוסיף לכל המדיה שתייבא במצבור זה.";

	$mgrlang['media_f_galls']				= "גלריות/אירועים";

	$mgrlang['media_f_galls_d']				= "הגדר גלריות או אירועים שהמדיה במצבור זה תהיה שייכת אליהם.";

	$mgrlang['media_f_format']				= "פורמט/סוג קובץ";

	$mgrlang['media_f_format_d']			= "פורמט או סוג קובץ זה.";

	$mgrlang['media_f_width']				= "רוחב";

	$mgrlang['media_f_width_d']				= "רוחב קובץ זה.";

	$mgrlang['media_f_height']				= "גובה";

	$mgrlang['media_f_height_d']			= "גובה קובץ זה.";

	$mgrlang['media_f_hd']					= "HD";

	$mgrlang['media_f_hd_d']				= "בדוק אם זהו HD וידאו.";

	$mgrlang['media_f_fps']					= "FPS";

	$mgrlang['media_f_fps_d']				= "מספר פריימים לשניה בוידאו זה.";	

	$mgrlang['media_f_runtime']				= "זמן ריצה/אורך";

	$mgrlang['media_f_runtime_d']			= "אורך וידאו זה.";	

	$mgrlang['media_f_orgcopy']				= "עותק מקורי";

	$mgrlang['media_f_orgcopy_d']			= "בחר איך גירסא זו של הקבצים תהיה רשומה.";

	$mgrlang['media_f_hidden']				= "מוסתר (לא זמין להורדה)";

	$mgrlang['media_f_rmps']				= "Rights Managed תוכנית תמחור";

	$mgrlang['media_f_price']				= "מחיר";

	$mgrlang['media_f_price_d']				= "מחיר הגירסא המקורית של פריט זה.";

	$mgrlang['media_f_credits']				= "קרדיטים";

	$mgrlang['media_f_credits_d']			= "כמות הקרדיטים עבור הגירסא המקורית של פריט זה.";

	$mgrlang['media_click_upbut']			= "לחץ על כפתור 'העלה' כדי לצרף קובץ";	

	$mgrlang['media_f_digsizes']			= "גדלים דיגיטליים";

	$mgrlang['media_f_digsizes_d']			= "וריאציות נוספות שתרצה למכור עם פריט זה. תוכל להתאים פרופילים אלו ולהוסיף רשיונות ותמחור ספציפיים עבור פריט זה.";

	$mgrlang['media_no_mem']				= "'צור אוטומטית' מכיוון שאין מספיק זיכרון זמין להקצות עבור התהליך! השרת המארח מגביל את השימוש בזיכרון על-ידי תכונת memory_limit. אנא צור איתם קשר כדי להעלות את המגבלה.";

	$mgrlang['media_f_print_d']				= "רשום את הדפסות אלו למכירה עם המדיה במצבור זה. כל קבוצות שנבחרו יוסיפו תמיד כל הדפסות בקבוצה זו.";

	$mgrlang['media_f_prod_d']				= "רשום מוצרים אלו למכירה עם המדיה במצבור זה.";

	$mgrlang['media_f_pack_d']				= "בחר את החבילות שיוצגו עם התמונות במצבור זה.";

	$mgrlang['media_size_limit']			= "כמה מהקבצים שאתה עומד לייבא הם מעל מגבלת גודל הקובץ ולא ניתן להעתיקה למיקום אחסון חיצוני. קבצים אלו ידולגו במהלך תהליך הייבוא. מגבלת גודל קובץ היא";

	$mgrlang['media_preimport']				= "לפני שתייבא את הקבצים שלך אתה יכול להוסיף כמה פרטים ותמחור, אם תרצה. כשתהיה מוכן להתחיל בייבוא לחץ על כפתור 'התחל ייבוא'.";

	$mgrlang['media_customize']				= "התאם";

	$mgrlang['media_customized']			= "מותאם";

	$mgrlang['media_fileattached']			= "קובץ מצורף";

	$mgrlang['media_pd_customized']			= "תמחור או פרטים הותאמו";

	$mgrlang['media_f_coll']				= "הוסף מדיה זו לאוספים הבאים. רק אוספים מהסוג 'צור אוסף ממדיה אינדיבידואלית' ירשמו כאן.";



	

	# LOOK & FEEL	

	$mgrlang['landf_f_detpage']				= "דף פרטים";

	$mgrlang['landf_f_detpage_d']			= "הצג את הפריטים הבאים בדף הפרטים.";	

	$mgrlang['landf_f_crop_thumb']			= "חתוך תמונות מוקטנות";

	$mgrlang['landf_f_crop_thumb_d']		= "חתוך תמונות מוקטנות כך שהרוחב והאורך של התמונה יהיו באותו גודל.";

	$mgrlang['landf_f_crop_ro']				= "חיתוך מעברים (Rollovers)";

	$mgrlang['landf_f_crop_ro_d']			= "חתוך מעברים כך שתצוגת התמונות האופקית והאנכית יהיו באותו גודל.";	

	$mgrlang['landf_f_vid_skin']			= "'כיסוי' בקרי וידאו";

	$mgrlang['landf_f_vid_skin_d']			= "'כיסוי' (סקין) עבור בקרי נגן הוידאו.";	

	$mgrlang['landf_f_vid_ap']				= "נגן אוטומטית וידאו";

	$mgrlang['landf_f_vid_ap_d']			= "התחל לנגן אוטומטית דגימות וידאו שבדף פרטי המדיה. יתכן ולא יעבוד ב- iOS, Android או דפדפני מובייל.";	

	$mgrlang['landf_f_vid_loop']			= "חזור על וידאו";

	$mgrlang['landf_f_vid_loop_d']			= "חזור על וידאו אוטומטית כשהוא מסתיים.";	

	$mgrlang['landf_f_vid_bgcolor']			= "צבע רקע של נגן וידאו";

	$mgrlang['landf_f_vid_bgcolor_d']		= "צבע הרקע של נגן הוידאו בזמן שהוידאו עדיין לא נטען. ייתכן ולא יעבוד עבור דפדפני מובייל.";	

	$mgrlang['landf_f_vid_rosize']			= "גודל מעבר וידאו";

	$mgrlang['landf_f_vid_rosize_d']		= "גודל הוידאו שמנוגן במעבר זה.";		

	$mgrlang['landf_f_vid_prsize']			= "גודל תצוגה מקדימה של הוידאו";

	$mgrlang['landf_f_vid_prsize_d']		= "גודל התצוגה המקדימה של הוידאו בדף פרטי המדיה.";	

	$mgrlang['landf_f_vid_autors']			= "התאמת גודל אוטומטית";

	$mgrlang['landf_f_vid_autors_d']		= "אוטומטית שנה את גודל נגן הוידאו כך שיתאים לגודל הוידאו כאשר יתחיל להתנגן. יתכן ולא יעבוד ב- iOS, Android או דפדפני מובייל.";

	$mgrlang['landf_f_vid_cont']			= "בקרי וידאו";

	$mgrlang['landf_f_vid_cont_d']			= "מיקום בקרי הוידאו בדף פרטי המדיה.";

	$mgrlang['landf_f_vid_prwm']			= "תצוגה מקדימה של סימן המים של הוידאו";

	$mgrlang['landf_f_vid_prwm_d']			= "סימן מים בשביל דוגמיות הוידאו בדף פרטי מדיה.";

	$mgrlang['landf_f_vid_rowm']			= "סימן מים לוידאו מעבר";

	$mgrlang['landf_f_vid_rowm_d']			= "סימן מים עבור הוידאו לדוגמא בפופ-אפ של מעבר.";

	$mgrlang['landf_f_vid_wmpos']			= "מיקום סימן מים";

	$mgrlang['landf_f_vid_wmpos_d']			= "כיסוי עבור בקרי נגן הוידאו.";

	$mgrlang['landf_f_vid_wmpos1']			= "תחתית-שמאל";

	$mgrlang['landf_f_vid_wmpos2']			= "תחתית-ימין";

	$mgrlang['landf_f_vid_wmpos3']			= "למעלה-שמאל";

	$mgrlang['landf_f_vid_wmpos4']			= "למעלה-ימין";

	$mgrlang['landf_crop_height']			= "חתוך גובה";

	$mgrlang['landf_preimg_01']				= "פרח";

	$mgrlang['landf_preimg_02']				= "כחול";

	$mgrlang['landf_preimg_03']				= "תמונת צב";

	$mgrlang['landf_preimg_04']				= "פרחים";

	$mgrlang['landf_preimg_05']				= "רציף";

	$mgrlang['landf_preimg_06']				= "בריכה וכדור";

	$mgrlang['landf_preimg_07']				= "כלב";

	$mgrlang['landf_preimg_08']				= "ענף";

	$mgrlang['landf_preimg_09']				= "דבורה ועלים";

	$mgrlang['landf_olderver']				= "מסמן שערכה זו היא עבור גירסא ישנה יותר של התוכנה ויתכן שלא תתאים לגירסא הנוכחית.";

	$mgrlang['mediadet_mem_fname']			= "שם פרטי של המשתמש";

	$mgrlang['mediadet_mem_email']			= "אימייל של המשתמש";

	$mgrlang['mediadet_mem_id']				= "מזהה משתמש";

	$mgrlang['mediadet_mem_lname']			= "שם משפחה של המשתמש";

	$mgrlang['mediadet_id']					= "מזהה";

	$mgrlang['mediadet_title']				= "כותרת";

	$mgrlang['mediadet_filename']			= "שם קובץ";

	$mgrlang['mediadet_owner']				= "בעלים";

	$mgrlang['mediadet_priceorg']			= "מחיר (מקורי)";

	$mgrlang['mediadet_creditsorg']			= "קרדיטים (מקורי)";

	$mgrlang['mediadet_dateadded']			= "התווסף בתאריך";

	$mgrlang['mediadet_datecreated']		= "תאריך יצירה";

	$mgrlang['mediadet_description']		= "תיאור";

	$mgrlang['mediadet_foldfile']			= "תיקיה/שם קובץ";

	$mgrlang['mediadet_views']				= "צפיות";

	$mgrlang['mediadet_keywords']			= "מילות מפתח";

	$mgrlang['mediadet_colors']				= "צבעים";

	$mgrlang['mediadet_copyright']			= "זכויות יוצרים";

	$mgrlang['mediadet_usage_rest']			= "הגבלות שימוש";

	$mgrlang['mediadet_mod_release']		= "מודל שחרור";

	$mgrlang['mediadet_res']				= "רזולוציה (מקורית)";

	$mgrlang['mediadet_filesize']			= "גודל קובץ (מקורי)";

	$mgrlang['mediadet_rating']				= "דירוג";

	$mgrlang['mediadet_add_lb']				= "הוסף לתיבה אישית";

	$mgrlang['mediadet_add_pack']			= "הוסף לחבילה";

	$mgrlang['mediadet_email']				= "אימייל";

	$mgrlang['mediadet_rating']				= "דירוג";	

	$mgrlang['landf_f_aupage']				= "אפשר דף 'אודותינו'";

	$mgrlang['landf_f_fppage']				= "אפשר דף קידומי מכירות מקודמים";

	$mgrlang['landf_f_fppage_d']			= "הצג את דף קידומי מכירות שמציג קופונים ופרסומות.";	

	$mgrlang['landf_f_nmpage']				= "אפשר דף מדיה חדשה";

	$mgrlang['landf_f_nmpage_d']			= "הצג איזור מדיה חדשה שיראה את המדיה החדשה ביותר שנוספה.";	

	$mgrlang['landf_f_fmpage']				= "אפשר דף מדיה מקודמת";

	$mgrlang['landf_f_fmpage_d']			= "הצג דף מדיה מקודמת באתר.";		

	$mgrlang['landf_f_pmpage']				= "אפשר דף מדיה פופולרית";

	$mgrlang['landf_f_pmpage_d']			= "הצג איזור מדיה פופולרית שיראה את קבצי המדיה הפופולריים ביותר באתר.";		

	$mgrlang['landf_f_fprintspage']			= "אפשר דף הדפסות מקודמות";

	$mgrlang['landf_f_fprintspage_d']		= "הצג את דף הדפסות מקודמות שיראה הדפסות שסימנת כמקודמות.";	

	$mgrlang['landf_f_fprodspage']			= "אפשר דף מוצרים מקודמים";

	$mgrlang['landf_f_fprodspage_d']		= "הצג את דף המוצרים המקודמים שיראה מוצגים שסימנת כמקודמים.";	

	$mgrlang['landf_f_fpackspage']			= "אפשר דף חבילות מקודמות";

	$mgrlang['landf_f_fpackspage_d']		= "הצג את דף החבילות המקודמות שיראה את כל החבילות שסימנת כמקודמות.";	

	$mgrlang['landf_f_fcollspage']			= "אפשר דף אוספים מקודמים";

	$mgrlang['landf_f_fcollspage_d']		= "הצג את דף האוספים המקודמים שיראה את כל האוספים שסימנת כמקודמים.";	

	$mgrlang['landf_f_fsubspage']			= "אפשר דף מינויים מקודמים";

	$mgrlang['landf_f_fsubspage_d']			= "הצג את דף המינויים המקודמים שיראה את כל המינויים שסימנת כמקודמים.";	

	$mgrlang['landf_f_fcredpage']			= "אפשר דף קרדיטים מקודמים";

	$mgrlang['landf_f_fcredpage_d']			= "הצג את דף חבילות הקרדיטים המקודמות שיראה את חבילות הקרדיטים שסימנת כמקודמות.";	

	$mgrlang['landf_f_medcount']			= "מוני מדיה";

	$mgrlang['landf_f_medcount_d']			= "הצג את מספר קבצי המדיה שבכל גלריה סמוך לשם הגלריה.";	

	$mgrlang['landf_f_stats']				= "הצג סטטיסטיקות";

	$mgrlang['landf_f_stats_d']				= "הצג את סטטיסטיקות האתר באתר המרכזי.";	

	$mgrlang['landf_f_memonline']			= "משתמשים מחוברים";

	$mgrlang['landf_f_memonline_d']			= "הצג משתמשים שנמצאים און-ליין באתר.";

	$mgrlang['landf_f_relatedmed']			= "מדיה קשורה";

	$mgrlang['landf_f_relatedmed_d']		= "הצג את איזור המדיה הקשורה בזמן צפיה בדף פרטי מדיה.";	

	$mgrlang['landf_f_iptc']				= "IPTC";

	$mgrlang['landf_f_iptc_d']				= "הצג מידע IPTC בדף פרטי מדיה אם הוא קיים.";	

	$mgrlang['landf_f_exif']				= "EXIF";

	$mgrlang['landf_f_exif_d']				= "הצג מידע EXIF בדף פרטי מדיה אם הוא קיים.";	

	$mgrlang['landf_f_socnet']				= "קוד רשתות חברתיות";

	$mgrlang['landf_f_socnet_d']			= "העתק והדבק כל קוד של רשתות חברתיות HTML/Javascript לכאן.";

	$mgrlang['landf_f_logo']				= "לוגו";

	$mgrlang['landf_f_logo_d']				= "העלה לוגו כדי להשתמש בו באתר, אימיילים וחשבוניות. רק PNG, GIF או JPEG.";	

	$mgrlang['landf_f_newsarea']			= "אפשר איזור חדשות";

	$mgrlang['landf_f_featuremed']			= "אפשר איזור מדיה מקודמת";

	$mgrlang['landf_f_newmed']				= "אפשר איזור מדיה חדשה";

	$mgrlang['landf_f_newmed_d']			= "הצג מבחר קטן של מדיה חדשה בדף הבית.";

	$mgrlang['landf_f_newmed2']				= "כמה פריטי מדיה חדשה יוצגו בדף הבית.";	

	$mgrlang['gen_how_many']				= "כמה";	

	$mgrlang['landf_f_popmed']				= "אפשר איזור מדיה פופולרית";

	$mgrlang['landf_f_popmed_d']			= "הצג מבחר קטן של מדיה פופולרית בדף הבית.";

	$mgrlang['landf_f_popmed2']				= "כמה פריטי מדיה פופולרית יוצגו בדף הבית.";

	$mgrlang['landf_f_randmed']				= "מדיה אקראית בדף הבית";

	$mgrlang['landf_f_randmed_d']			= "הצג מבחר קטן של מדיה אקראית בדף הבית.";

	$mgrlang['landf_f_randmed2']			= "כמה פריטי מדיה אקראית יוצגו בדף הבית.";	

	$mgrlang['landf_f_featpromo']			= "אפשר דף פרסומים מקודמים";

	$mgrlang['landf_f_featprint']			= "אפשר איזור הדפסות מקודמות";

	$mgrlang['landf_f_featprod']			= "אפשר איזור מוצרים מקודמים";

	$mgrlang['landf_f_featpacks']			= "אפשר איזור חבילות מקודמות";

	$mgrlang['landf_f_featcolls']			= "אפשר איזור אוספים מקודמים";

	$mgrlang['landf_f_featsubs']			= "אפשר איזור מינויים מקודמים";


	$mgrlang['landf_f_featcred']			= "אפשר איזור קרדיטים מקודמים";

	$mgrlang['landf_f_galthumbsize']		= "גודל תמונה מוקטנת לגלריה";

	$mgrlang['landf_f_galthumbsize_d']		= "בחר גודל עבור תמונות מוקטנות בגלריה. הגדרות איכות וחדות יהיו אותן הגדרות שהגדרת ב'הגדרות תמונה מוקטנת'.";	

	$mgrlang['landf_f_cropgalthumb']		= "חתוך אייקונים של גלריה";

	$mgrlang['landf_f_cropgalthumb_d']		= "חתוך אייקוני גלריה כך שגובה ורוחב התמונות יהיו באותו גודל.";	

	$mgrlang['landf_f_galperpage']			= "גלריה לעמוד";

	$mgrlang['landf_f_galperpage_d']		= "חתוך תמונות מוקטנות של הגלריה כך שגובה ורוחב התמונות יהיו באותו גודל.";	

	$mgrlang['landf_f_galdsort']			= "ברירת מחדל למיון גלריה";

	$mgrlang['landf_f_galdsort_d']			= "ברירת המחדל למיון בגלריות.";	

	$mgrlang['landf_theme']					= "ערכת נושא";	

	$mgrlang['landf_thumbnails']			= "תמונות מוקטנות";	

	$mgrlang['landf_rollovers']				= "מעברים (rollovers)";	

	$mgrlang['landf_previews']				= "תצוגות מקדימות";

	$mgrlang['landf_homepage']				= "דף בית";	

	$mgrlang['landf_other']					= "אחר";

	$mgrlang['landf_videos']				= "קבצי וידאו";

	$mgrlang['landf_pages']					= "דפים";

	$mgrlang['landf_galleries']				= "גלריות";

	$mgrlang['landf_logo']					= "לוגו";

	$mgrlang['landf_preimg']				= "תמונת תצוגה מקדימה";	

	$mgrlang['landf_f_details']				= "פרטים";

	$mgrlang['landf_f_details_d']			= "הצג את הפרטים הבאים מתחת לאייקון התמונה המוקטנת.";

	$mgrlang['landf_f_detro_d']				= "הצג את הפרטים הבאים מתחת לתמונה בחלונות מעברים.";

	$mgrlang['landf_f_actrollover']			= "הפעל מעבר (Rollover)";

	$mgrlang['landf_f_actrollover_d']		= "הצג את הפופ-אפ בזמן מעבר על התמונות המוקטנות.";

	

	# AGREEMENTS

	$mgrlang['agree_f_name']				= "שם רשיון/הסכם";

	$mgrlang['agree_f_name_d']				= "שם ההסכם או הרשיון. ייתכן וישמש ככותרת הדף באתר הציבורי, בחלק מהמקרים.";

	$mgrlang['agree_f_content_d']			= "התוכן שיופיע בהסכם או רישיון זה.";

	

	# PUBLIC AREAS

	$mgrlang['pubLightbox']					= "תיבה אישית";

	$mgrlang['pubRating']					= "דירוגי מדיה";

	$mgrlang['pubUpdateMembership']			= "עדכן הרשמה";

	$mgrlang['pubUpdate']					= "עדכן";

	$mgrlang['pubAccountInfo']				= "פרטי חשבון";

	$mgrlang['pubBio']						= "ביוגרפיה";

	$mgrlang['pubAddress']					= "כתובת";

	$mgrlang['pubPassword']					= "סיסמא";

	$mgrlang['pubDateTime']					= "תאריך/שעה";

	$mgrlang['pubAvatar']					= "דמות";

	$mgrlang['pubDelete']					= "מחק";

	$mgrlang['pubNew']						= "חדש";

	$mgrlang['pubCreate']					= "צור";

	$mgrlang['pubAddItem']					= "הוסף פריט";

	$mgrlang['pubMedia']					= "מדיה";

	$mgrlang['pubRemoveItem']				= "הסר פריט";

	$mgrlang['pubLogin']					= "התחבר";

	$mgrlang['pubLoggedOut']				= "התנתק";

	$mgrlang['pubLoggedIn']					= "התחבר";

	$mgrlang['pubNewComment']				= "הערה חדשה";

	$mgrlang['pubNewTag']					= "תווית חדשה";

	

	# WELCOME PAGE

	$mgrlang['welcome_wel']					= "ברוכים הבאים";

	$mgrlang['welcome_last_logged']			= "לאחרונה התחברת ב- ";

	$mgrlang['welcome_page_title']			= "לוח בקרה";

	$mgrlang['welcome_ftv_wizard']			= "נראה שזו הפעם הראשונה שאתה נכנס לאיזור הניהול. האם תרצה לטעון את אשף ההתקנה?";

	$mgrlang['welcome_launch_wiz']			= "האם תרצה לטעון את אשף ההתקנה?";

	

	# ADMINISTRATORS PAGE

	$mgrlang['admin_b_new']					= "הוסף חדש";

	$mgrlang['admin_b_sa']					= "בחר הכל";

	$mgrlang['admin_b_sn']					= "אל תבחר הכל";

	$mgrlang['admin_b_del']					= "מחק";

	$mgrlang['admin_mes_01']				= "מנהל חדש התווסף.";

	$mgrlang['admin_mes_02']				= "השינויים נשמרו.";

	$mgrlang['admin_mes_03']				= "חשבון מנהל נמחק בהצלחה.";

	$mgrlang['admin_mes_04']				= "מנהל נמחק בהצלחה.";

	$mgrlang['admin_mes_05']				= "לא נבחר אף מנהל למחיקה.";

	$mgrlang['admin_mes_06']				= "רק סופר-מנהל יכול למחוק מנהלים.";

	$mgrlang['admin_mes_07']				= "רק סופר-מנהל יכול להוסיף מנהלים חדשים.";

	$mgrlang['admin_mes_08']				= "אם אינך סופר-מנהל אתה יכול לערוך רק את הפרופיל שלך.";

	$mgrlang['admin_admin']					= "מנהל";

	$mgrlang['admin_superadmin']			= "סופר-מנהל";

	$mgrlang['admin_last_login']			= "התחבר לאחרונה";

	$mgrlang['admin_never']					= "מעולם לא";

	$mgrlang['admin_username']				= "שם משתמש";

	$mgrlang['admin_password']				= "סיסמא";

	$mgrlang['admin_email']					= "אימייל";

	$mgrlang['admin_mes_09']				= "אנא הכנס";

	$mgrlang['admin_mes_10']				= "לפני שמירה";

	$mgrlang['admin_edit_header']			= "ערוך מנהל";

	$mgrlang['admin_edit_message']			= "ערוך מנהל זה ולחץ על כפתור 'שמור שינויים'.";

	$mgrlang['admin_new_header']			= "הוסף מנהל חדש";

	$mgrlang['admin_new_message']			= "בעזרת הטופס הבא תוכל להוסיף מנהלים חדשים לאתר.";

	$mgrlang['admin_tab1']					= "פרטי מנהל";

	$mgrlang['admin_tab2']					= "הרשאות";

	$mgrlang['admin_tab3']					= "יומן פעילות";

	$mgrlang['admin_f_username']			= "שם משתמש";

	$mgrlang['admin_f_username_d']			= "מקסימום 17 תווים";

	$mgrlang['admin_f_password']			= "סיסמא";

	$mgrlang['admin_f_password_d']			= "מקסימום 50 תווים";

	$mgrlang['admin_hidden']				= "מוסתר במצב דמו";

	$mgrlang['admin_f_email']				= "אימייל";

	$mgrlang['admin_f_email_d']				= "מקסימום 250 תווים";

	$mgrlang['admin_f_active']				= "פעיל";

	$mgrlang['admin_f_active_d']			= "הפעל חשבון זה";

	$mgrlang['admin_f_perm']				= "הרשאות";

	$mgrlang['admin_f_perm_d']				= "בחר את האיזורים שברצונך שלמנהל זה תהיה גישה אליהם, בתוך איזור הניהול.";

	$mgrlang['admin_f_all']					= "הכל";

	$mgrlang['admin_f_none']				= "כלום";

	$mgrlang['admin_unencrypt']				= "פענח";

	$mgrlang['admin_mes_11']				= "כתובת המייל שהכנסת אינה תקינה.";

	$mgrlang['admin_mes_12']				= "שם המשתמש מכיל תווים לא חוקיים.";

	$mgrlang['admin_mes_13']				= "סיסמא לא יכולה להכיל רווחים.";

	$mgrlang['admin_mes_14']				= "שם המשתמש תפוס. אנא בחר אחר.";

	$mgrlang['admin_al_none']				= "לא נשמרה פעילות!";

	

	# DISCOUNTS BOX

	$mgrlang['discounts_buy']				= "קנה";

	$mgrlang['discounts_save']				= "או עוד ושמור";

	

	# GALLERIES

	$mgrlang['galleries_new_header']		= "הוסף גלריה חדשה";

	$mgrlang['galleries_edit_header']		= "ערוך גלריה";

	$mgrlang['galleries_new_message']		= "בעזרת הטופס הבא תוכל להוסיף גלריה לאתר.";

	$mgrlang['galleries_edit_message']		= "ערוך את הגלריה ולחץ על כפתור 'שמור שינויים'.";

	$mgrlang['galleries_f_name']			= "שם גלריה/אירוע";

	$mgrlang['galleries_f_name_d']			= "מקסימום 100 תווים";

	$mgrlang['galleries_f_description']		= "תיאור";

	$mgrlang['galleries_f_description_d']	= "תיאור גלריה זו. זה יוצג באתר הציבורי בגלריה הזו.";

	$mgrlang['galleries_f_parent']			= "גלריית אב";

	$mgrlang['galleries_f_parent_d']		= "האב של הגלרייה הנוכחית";

	$mgrlang['galleries_f_dirpath']			= "נתיב התיקיה";

	$mgrlang['galleries_f_dirpath_d']		= "מיקום הגלריה על השרת";	

	$mgrlang['galleries_none']				= "ללא";

	$mgrlang['galleries_mes_01']			= "הגלריה החדשה נוצרה.";

	$mgrlang['galleries_mes_02']			= "השינויים נשמרו.";

	$mgrlang['galleries_mes_03']			= "נכשל ביצירה: ";

	$mgrlang['galleries_mes_04']			= "נכשל במחיקה: ";

	$mgrlang['galleries_mes_05']			= "נכשל בהעברה: ";

	$mgrlang['galleries_mes_06']			= "תיקיה לא קיימת:";

	$mgrlang['galleries_mes_07']			= "תיקיה כבר קיימת:";

	$mgrlang['galleries_nogal']				= "כרגע לא נוצרו לך גלריות.";

	$mgrlang['galleries_b_list']			= "רשימה";

	$mgrlang['galleries_b_boxes']			= "תיבות";

	$mgrlang['galleries_f_icon_d']			= "בחר אייקון לייצג את הגלריה הזו.";

	$mgrlang['galleries_f_active_d']		= "התאריך בו גלריה זו תיהפך לפעילה.";

	$mgrlang['galleries_f_expire_d']		= "התאריך בו גלריה זו תפסיק להיות זמינה.";

	$mgrlang['galleries_f_dlink_d']			= "קישור ישיר לגלריה.";

	$mgrlang['galleries_f_owner_d']			= "האדם לו שייכת הגלריה.";

	$mgrlang['galleries_f_contr_d']			= "אפשר למשתמשים לתרום לגלריה זו. אפשר זאת רק אם תוכנית ההרשמה שלהם מאפשרת את זה.";

	$mgrlang['galleries_f_client']			= "שם לקוח";

	$mgrlang['galleries_f_client_d']		= "שם הלקוח שהאירוע בשבילו.";	

	$mgrlang['galleries_f_eventid']			= "קוד או מזהה האירוע";

	$mgrlang['galleries_f_eventid_d']		= "הקוד או המזהה הפנימי עבור אירוע זה.";	

	$mgrlang['galleries_f_eventdate']		= "תאריך האירוע";

	$mgrlang['galleries_f_eventdate_d']		= "התאריך בו התרחש האירוע. זה משמש לקוחות שרוצים לחפש את האירוע.";	

	$mgrlang['galleries_f_eventloc']		= "מיקום האירוע";

	$mgrlang['galleries_f_eventloc_d']		= "כאן תוכל להכניס את כתובת, עיר, מדינה, או כל מידע אחר שתרצה על המיקום.";	

	$mgrlang['galleries_f_access']			= "מיקום האירוע";

	$mgrlang['galleries_f_access_d']		= "בחר מי יכול לגשת לגלריה זו.";	

	$mgrlang['galleries_f_password']		= "סיסמא";

	$mgrlang['galleries_f_password_d']		= "כדי להגן על גלריה/אירוע זו בסיסמא, הכנס סיסמא. השאר ריק אם אתה לא מעוניין בסיסמא.";

	$mgrlang['galleries_f_assets']			= "נכסים";

	$mgrlang['galleries_f_assets_d']		= "מספר הנכסים באירוע/גלריה זו. כולל תמונות, סרטים וקבצים.";

	$mgrlang['galleries_f_views']			= "צפיות";

	$mgrlang['galleries_f_views_d']			= "מספר הפעמים שנכסים באירוע/גלריה זו נצפו. 'נקה מספר צפיות גלריה' מאפס את מספר הצפיות לגלריה ולכל הגלריות בנים.";

	

	$mgrlang['sort_default']				= "השתמש כברירת מחדל";

	$mgrlang['sort_added']					= "נוסף בתאריך";

	$mgrlang['sort_id']						= "מזהה";

	$mgrlang['sort_title']					= "כותרת";

	$mgrlang['sort_filename']				= "שם קובץ";

	$mgrlang['sort_filesize']				= "גודל קובץ";

	$mgrlang['sort_width']					= "רוחב";

	$mgrlang['sort_height']					= "גובה";

	$mgrlang['sort_snumber']				= "מספר מיון";

	$mgrlang['sort_bid']					= "מזהה מצבור";

	$mgrlang['sort_featured']				= "מקודם";

	$mgrlang['sort_views']					= "צפיות";

	$mgrlang['sort_asce']					= "סדר עולה";

	$mgrlang['sort_desc']					= "סדר יורד";

	$mgrlang['galleries_no_mem']			= "כרגע אין משתמשים לבחור מהם.";

	

	$mgrlang['galleries_f_ded']				= "הצג פרטי אירוע";

	$mgrlang['galleries_f_ded_d']			= "הצג פרטי אירוע בדף הגלריה הציבורי.";	

	

	

	

	

	

	# NEWS

	$mgrlang['news_new_header']				= "הוסף פריט חדשות חדש";

	$mgrlang['news_edit_header']			= "ערוך פריט חדשות";

	$mgrlang['news_new_message']			= "בעזרת הטופס הבא תוכל להוסיף פריט חדשות חדש לאתר.";

	$mgrlang['news_edit_message']			= "ערוך פריט חדשות זה ולחץ על כפתור 'שמור שינויים'.";

	$mgrlang['news_tab1']					= "פרטי חדשות";

	$mgrlang['news_tab2']					= "קבוצות";

	$mgrlang['news_f_date']					= "תאריך/שעת פרסום";

	$mgrlang['news_f_date_d']				= "פרסם פריט חדשות בתאריך ושעה זו (yyyy/mm/dd hh:mm). פריט חדשות לא יוצג עד לתאריך.";

	$mgrlang['news_f_title']				= "כותרת";

	$mgrlang['news_f_title_d']				= "מקסימום 100 תווים";

	$mgrlang['news_f_short']				= "תיאור קצר";

	$mgrlang['news_f_short_d']				= "תיאור התצוגה המקדימה.";

	$mgrlang['news_f_article']				= "מאמר";

	$mgrlang['news_f_acticle_d']			= "מאמרי חדשות שלמים";

	$mgrlang['news_f_active']				= "פעיל";

	$mgrlang['news_f_active_d']				= "הצג פריט חדשות זה בדף הבית.";

	$mgrlang['news_f_homepage']				= "דף בית";

	$mgrlang['news_f_homepage_d']			= "הצג פריט חדשות זה בדף הבית.";

	$mgrlang['news_title']					= "כותרת";

	$mgrlang['news_al_delete']				= "פריט(י) חדשות נמחק(ו) ";

	$mgrlang['news_f_groups']				= "קבוצות של חדשות";

	$mgrlang['news_f_groups_d']				= "בחר קבוצות של חדשות אליהן יהיה שייך פריט חדשות זה.";

	$mgrlang['news_f_expire_d']				= "תאריך התפוגה של פריט חדשות זה, בו הוא לא יוצג יותר.";

	

	

	# PAGE CONTENT AREAS

	$mgrlang['pc_new_header']				= "הוסף תוכן חדש";

	$mgrlang['pc_edit_header']				= "ערוך תוכן דף";

	$mgrlang['pc_new_message']				= "בעזרת הטופס הבא תוכל להוסיף לאתר תוכן חדש ודפים.";

	$mgrlang['pc_edit_message']				= "ערוך את התוכן ולחץ על כפתור 'שמור שינויים'.";

	$mgrlang['pc_content']					= "איזורי תוכן כללי";

	$mgrlang['pc_custom_page']				= "דפי התוכן שלך";

	$mgrlang['pc_custom_block']				= "מקטעי התוכן שלך";

	

	# EMAIL CONTENT AREAS

	$mgrlang['ec_new_header']				= "הוסף אימייל חדש";

	$mgrlang['ec_edit_header']				= "ערוך תוכן אימייל";

	$mgrlang['ec_new_message']				= "בעזרת הטופס הבא ניתן להוסיף הודעת מייל חדשה.";

	$mgrlang['ec_edit_message']				= "ערוך אימייל זה ולחץ על כפתור 'שמור שינויים'.";

	$mgrlang['ec_public']					= "אימיילים למשתמש";

	$mgrlang['ec_admin']					= "אימיילים למנהלים";

	$mgrlang['ec_custom']					= "אימיילים שלך";

	$mgrlang['ec_f_emailsub']				= "נושא אימייל";

	$mgrlang['ec_f_emailsub_d']				= "הנושא עבור הודעת מייל זה.";

	

	# AGREEMENTS AREAS

	$mgrlang['agree_new_header']			= "הוסף רשיון או הסכם חדש";

	$mgrlang['agree_edit_header']			= "ערוך רשיון או הסכם";

	$mgrlang['agree_new_message']			= "בעזרת הטופס הבא תוכל להוסיף רשיון או הסכם חדש.";

	$mgrlang['agree_edit_message']			= "ערוך את הרשיון או ההסכם ולץ על כפתור 'שמור שינויים'.";

	$mgrlang['agree_general']				= "הסכמים כללי";

	$mgrlang['agree_custom']				= "הסכמים שלך";

	

	# SOFTWARE SETUP

	$mgrlang['setup_tab1']					= "התקנה כללי";

	$mgrlang['setup_tab2']					= "תאריך & שעה";

	$mgrlang['setup_tab3']					= "מתקדם";

	$mgrlang['setup_tab4']					= "גיבויים";

	$mgrlang['setup_tab5']					= "מספרים";

	$mgrlang['setup_tab6']					= "אודות";

	$mgrlang['setup_tab7']					= "עיבוד תמונה";

	$mgrlang['setup_tab8']					= "התקנת מייל";

	$mgrlang['setup_tab9']					= "אבטחה";

	$mgrlang['setup_tab10']					= "פעילות מערכת";

	$mgrlang['setup_mes_01']				= "שעה מותאמת על-ידי";

	$mgrlang['setup_hours']					= "שעות";

	$mgrlang['setup_gmt']					= "GMT";

	$mgrlang['setup_timezone_01']			= "Enitwetok, Kwajalien";

	$mgrlang['setup_timezone_02']			= "Midway Island, Samoa";

	$mgrlang['setup_timezone_03']			= "Hawaii";

	$mgrlang['setup_timezone_04']			= "Alaska";

	$mgrlang['setup_timezone_05']			= "Pacific Time (US &amp; Canada)";

	$mgrlang['setup_timezone_06']			= "Mountain Time (US &amp; Canada)";

	$mgrlang['setup_timezone_07']			= "Central Time (US &amp; Canada), Mexico City";

	$mgrlang['setup_timezone_08']			= "Eastern Time (US &amp; Canada), Bogota, Lima";

	$mgrlang['setup_timezone_09']			= "Atlantic Time (Canada), Caracas, La Paz";

	$mgrlang['setup_timezone_10']			= "Newfoundland";

	$mgrlang['setup_timezone_11']			= "Brazil, Buenos Aires, Falkland Is.";

	$mgrlang['setup_timezone_12']			= "Mid-Atlantic, Ascention Is., St Helena";

	$mgrlang['setup_timezone_13']			= "Azores, Cape Verde Islands";

	$mgrlang['setup_timezone_14']			= "Casablanca, Dublin, London, Lisbon, Monrovia";

	$mgrlang['setup_timezone_15']			= "Brussels, Copenhagen, Madrid, Paris";

	$mgrlang['setup_timezone_16']			= "Kaliningrad, South Africa";

	$mgrlang['setup_timezone_17']			= "Baghdad, Riyadh, Moscow, Nairobi";

	$mgrlang['setup_timezone_18']			= "Tehran";

	$mgrlang['setup_timezone_19']			= "Abu Dhabi, Baku, Muscat, Tbilisi";

	$mgrlang['setup_timezone_20']			= "Kabul";

	$mgrlang['setup_timezone_21']			= "Ekaterinburg, Karachi, Tashkent";

	$mgrlang['setup_timezone_22']			= "Bombay, Calcutta, Madras, New Delhi";

	$mgrlang['setup_timezone_23']			= "Almaty, Colomba, Dhakra";

	$mgrlang['setup_timezone_24']			= "Bangkok, Hanoi, Jakarta";

	$mgrlang['setup_timezone_25']			= "Hong Kong, Perth, Singapore, Taipei";

	$mgrlang['setup_timezone_26']			= "Osaka, Sapporo, Seoul, Tokyo, Yakutsk";

	$mgrlang['setup_timezone_27']			= "Adelaide, Darwin";

	$mgrlang['setup_timezone_28']			= "Melbourne, Papua New Guinea, Sydney";

	$mgrlang['setup_timezone_29']			= "Magadan, New Caledonia, Solomon Is.";

	$mgrlang['setup_timezone_30']			= "Auckland, Fiji, Marshall Island";

	$mgrlang['setup_f_iptc']				= "קרא מידע IPTC";

	$mgrlang['setup_f_iptc_d']				= "קרא מידע IPTC מתמונות בזמן שהן מיובאות.";

	$mgrlang['setup_f_exif']				= "קרא מידע EXIF";

	$mgrlang['setup_f_exif_d']				= "קרא מידע EXIF מתמונות בזמן שהן מיובאות.";

	$mgrlang['setup_f_url']					= "כתובת URL מלאה";

	$mgrlang['setup_f_url_d']				= "בלי סלאש ( / ) בסוף הכתובת";	

	$mgrlang['setup_f_idn']					= "נתיב תיקיית Incoming";

	$mgrlang['setup_f_idn_d']				= "נתיב לתיקיית incoming בשרת. חייבת הרשאת כתיבה.";

	$mgrlang['setup_f_lp']					= "נתיב ספרייה";

	$mgrlang['setup_f_lp_d']				= "נתיב לתיקיית הספרייה בשרת. חייב להיות עם הרשאת כתיבה.";

	$mgrlang['setup_f_dv']					= "וידוא מחיקה";

	$mgrlang['setup_f_dv_d']				= "תמיד בקש וידוא לפני מחיקת רשומות בצד הניהול.";

	$mgrlang['setup_f_alerts']				= "אזהרות פופ-אפ";

	$mgrlang['setup_f_alerts_d']			= "הצג אזהרות פופ-אפ באיזור הניהול עבור משתמשים חדשים, הזמנות, גיבויים וכו'.";

	$mgrlang['setup_f_aka']					= "אפשר גישה לתמיכה";

	$mgrlang['setup_f_aka_d']				= "אפשר ל- Ktools.net גישה למטרות תמיכה בלבד. אנחנו נתחבר אך ורק אם תבקש עזרה.";

	$mgrlang['setup_f_is']					= "שיתוף מידע";

	$mgrlang['setup_f_is_d']				= "שתף מידע אנונימית עם Ktools.net.";	

	$mgrlang['setup_f_admin_ac']			= "פעילות מנהל";

	$mgrlang['setup_f_admin_ac_d']			= "תעד כל פעילות של מנהלים.";

	$mgrlang['setup_f_mem_ac']				= "פעילות משתמש";

	$mgrlang['setup_f_mem_ac_d']			= "תעד כל פעילות של משתמשים.";

	$mgrlang['setup_f_ceditor']				= "עורך תוכן";

	$mgrlang['setup_f_ceditor_d']			= "בחר בעורך תוכן לשימוש באיזורי הטקסט.";

	$mgrlang['setup_f_ceditor_op1']			= "InnovaEditor";

	$mgrlang['setup_f_ceditor_op2']			= "TinyMCE";

	$mgrlang['setup_f_ceditor_op0']			= "None";

	$mgrlang['setup_f_uploader']			= "Batch Uploader";

	$mgrlang['setup_f_uploader_d']			= "בחר את 'מעלה המצבור' שתרצה להשתמש בו כדי להוסיף מדיה לאתר.";

	$mgrlang['setup_f_uploader_op1']		= "Java Uploader (מומלץ)";

	$mgrlang['setup_f_uploader_op2']		= "Flash Uploader";

	$mgrlang['setup_f_uploader_op3']		= "HTML Uploader";

	$mgrlang['setup_f_weight']				= "סימון יחידת משקל";

	$mgrlang['setup_f_weight_d']			= "סימון שיוצג ליד יחידות משקל. לדוגמא פאונדים, קילוגרמים, lbs או kgs.";

	$mgrlang['setup_f_tz']					= "איזור זמן";

	$mgrlang['setup_f_tz_d']				= "בחר את איזור הזמן שלך.";

	$mgrlang['setup_f_dtb']					= "תצוגה מקדימה של תאריך/שעה";

	$mgrlang['setup_f_dtb_d']				= "תצוגה מקדימה של השעה עם שינויים.";

	$mgrlang['setup_f_ds']					= "שעון קיץ";

	$mgrlang['setup_f_ds_d']				= "שנה שעון אוטומטית לשעון קיץ.";

	$mgrlang['setup_f_df']					= "פורמט תאריך";

	$mgrlang['setup_f_df_d']				= "US, European או International/World Date.";

	$mgrlang['setup_f_dd']					= "תצוגת תאריך";

	$mgrlang['setup_f_dd_d']				= "איך תרצה שתאריכים יוצגו.";

	$mgrlang['setup_f_dd1_d']				= "תאריך ארוך";

	$mgrlang['setup_f_dd2_d']				= "תאריך קצר";

	$mgrlang['setup_f_dd3_d']				= "תאריך מספרי";

	$mgrlang['setup_f_df1_d']				= "mm/dd/yyyy";

	$mgrlang['setup_f_df2_d']				= "dd/mm/yyyy";

	$mgrlang['setup_f_df3_d']				= "yyyy/mm/dd";

	$mgrlang['setup_f_cf']					= "פורמט שעון";

	$mgrlang['setup_f_cf_d']				= "פורמט 12 או 24 שעות.";

	$mgrlang['setup_f_cf1_d']				= "12 שעות";

	$mgrlang['setup_f_cf2_d']				= "24 שעות";

	$mgrlang['setup_f_nds']					= "מפריד מספרי תאריך";

	$mgrlang['setup_f_nds_d']				= "הפרד חודש, יום ושנה עם התו הזה - כאשר יוצגו תאריכים בצורה מספרית.";

	$mgrlang['setup_f_lfo']					= "עקיפת קובץ שפה";

	$mgrlang['setup_f_lfo_d']				= "עקוף את הגדרות פורמט שעה ותאריך עם הגדרות מקובץ השפה שנבחר באתר הציבורי. זה נמצא בקובץ lang.settings.php עבור כל שפה.";

	$mgrlang['setup_f_mo']					= "עקיפת משתמש";

	$mgrlang['setup_f_mo_d']				= "אפשר למשתמשים להגדיר תאריך ושעה לעצמם בחשבון שלהם.";

	$mgrlang['setup_f_mwr']					= "קישורי SEO";

	$mgrlang['setup_f_mwr_d']				= "לחץ כדי לאפשר כתיבה מחדש של mod עבור כתובות URL ידידותיות למנועי חיפוש. דרוש קובץ .htaccess תקין.";	

	$mgrlang['setup_f_amfolders']			= "ניהול תיקיות אוטומטי";

	$mgrlang['setup_f_amfolders_d']			= "נהל תיקיות עבורי באופן אוטומטי. כאשר אפשרות זו אינה מופעלת תוכל לנהל את התיקיות בעצמך.";	

	$mgrlang['setup_f_debug']				= "דיבאג לפאנל ניהול";

	$mgrlang['setup_f_debug_d']				= "Ctrl+Shift+d לפתוח פאנל, כשהוא פעיל.";

	$mgrlang['setup_f_demo']				= "מצב דמו";

	$mgrlang['setup_f_demo_d']				= "אפשר גישת דמו לאיזור הניהול.";	

	$mgrlang['setup_f_purge_ac']			= "ניקוי רישומי פעילות";

	$mgrlang['setup_f_purge_ac_d']			= "מחק רישומי פעילות ישנים אחרי זמן זה.";

	$mgrlang['setup_time_never']			= "לעולם לא";

	$mgrlang['setup_time_1d']				= "אחרי 1 יום";

	$mgrlang['setup_time_3d']				= "אחרי 3 ימים";

	$mgrlang['setup_time_5d']				= "אחרי 5 ימים";

	$mgrlang['setup_time_1w']				= "אחרי 1 שבוע";

	$mgrlang['setup_time_2w']				= "אחרי 2 שבועות";

	$mgrlang['setup_time_1m']				= "אחרי 1 חודש";

	$mgrlang['setup_time_2m']				= "אחרי 2 חודשים";

	$mgrlang['setup_time_3m']				= "אחרי 3 חודשים";

	$mgrlang['setup_time_6m']				= "אחרי 6 חודשים";

	$mgrlang['setup_time_1y']				= "אחרי 1 שנה";

	$mgrlang['setup_f_captcha']				= "Captcha לטופס ההרשמה";

	$mgrlang['setup_f_captcha_d']			= "הדלק captcha עבור נרשמים חדשים, כך שהמבקרים חייבים להכניס קוד ייחודי בזמן ההרשמה.";

	$mgrlang['setup_f_email_conf']			= "נדרש אישור במייל";

	$mgrlang['setup_f_email_conf_d']		= "דרוש מהמשתמשים לאשר את ההרשמה דרך אימייל לפני שחשבונם יהפוך לפעיל.";

	$mgrlang['setup_f_drc']					= "בטל קליק ימני";

	$mgrlang['setup_f_drc_d']				= "נסה למנוע ממבקרים לעשות קליק ימני באתר. כמו כן, זה יבטל את IE image toolbar.";

	$mgrlang['setup_f_dcp']					= "בטל העתק-הדבק";

	$mgrlang['setup_f_dcp_d']				= "נסה למנוע ממבקרים לעשות העתק-הדבק על תמונות.";

	$mgrlang['setup_f_dpri']				= "בטל הדפסה";

	$mgrlang['setup_f_dpri_d']				= "נסה למנוע ממבקרים להדפיס תמונות מהאתר.";

	$mgrlang['setup_f_dpl']					= "בטל קישוריות";

	$mgrlang['setup_f_dpl_d']				= "נסה למנוע מאתרים חיצוניים קישורים לתמונות לדוגמא באתר.";

	$mgrlang['setup_f_dec_separator']		= "נקודה עשרונית";

	$mgrlang['setup_f_dec_separator_d']		= "תו לשימוש עבור הנקודה העשרונית.";

	$mgrlang['setup_f_thou_separator']		= "מפריד לאלפים";

	$mgrlang['setup_f_thou_separator_d']	= "תו לשימוש עבור הפרדת ספרות אלפים.";

	$mgrlang['setup_f_thou_sep_none']		= "ללא";

	$mgrlang['setup_f_thou_sep_space']		= "רווח";

	$mgrlang['setup_f_num_after']			= "מקומות עשרוניים";

	$mgrlang['setup_f_num_after_d']			= "מספר המקומות אחרי הנקודה העשרונית.";

	$mgrlang['setup_f_neg_format']			= "פורמט מספר שלילי";

	$mgrlang['setup_f_neg_format_d']		= "הפורמט שישמש למספרים שליליים.";

	$mgrlang['setup_f_lang_override']		= "מעקף קובץ שפה";

	$mgrlang['setup_f_lang_override_d']		= "עקוף הגדרות פורמט מספרים עם ההגדרות מקובץ השפה שנבחר באתר הציבורי.";

	$mgrlang['setup_f_mem_override']		= "מעקף משתמש";

	$mgrlang['setup_f_mem_override_d']		= "אפשר למשתמשים להגדיר את אפשרויות פורמט המספרים בחשבון שלהם.";

	$mgrlang['setup_f_num_preview']			= "מספר צפיות מוקדמות";

	$mgrlang['setup_f_num_pos']				= "חיובי";

	$mgrlang['setup_f_num_neg']				= "שלילי";		

	$mgrlang['setup_f_blockip']				= "חסום כתובות IP";

	$mgrlang['setup_f_blockip_d']			= "חסום כתובות IP אלו מצפייה באתר. אחת בשורה.";

	$mgrlang['setup_f_blockreferrer']		= "חסום Domains";

	$mgrlang['setup_f_blockreferrer_d']		= "חסום מבקרים מ- domains אלו. לדוגמא: google.com. אל תכלול www. אחד בשורה.";

	$mgrlang['setup_f_blockemail']			= "חסימת כתובות אי-מייל";

	$mgrlang['setup_f_blockemail_d']		= "חסום מבקרים שכתובת המייל שהזינו בזמן ההרשמה מכילה את הכתובות הבאות. לדוגמא: someone@somewhere.com או @yahoo.com. אחד לשורה.";

	$mgrlang['setup_f_blockwords']			= "מסנן מילים";

	$mgrlang['setup_f_blockwords_d']		= "המילים הבאות יסוננו מתוך תוכן שיוכנס על-ידי משתמשים. מילה אחת לשורה.";

	$mgrlang['setup_f_imageproc']			= "מעבד תמונה";

	$mgrlang['setup_f_imageproc_d']			= "באיזה מעבד תמונה להשתמש.";

	$mgrlang['setup_f_mailproc']			= "סוג מייל";

	$mgrlang['setup_f_mailproc_d']			= "בחר את תהליך השרת שתרצה שישמש כדי לשלוח מייל.";

	$mgrlang['setup_f_copy_messages']		= "העתק מיילים להודעות";

	$mgrlang['setup_f_copy_messages_d']		= "העתק את כל המיילים שנשלחים דרך המערכת אל איזור ההודעות של המשתמשים.";

	$mgrlang['setup_phpmail']				= "PHP Mail()";

	$mgrlang['setup_smtp']					= "SMTP";

	$mgrlang['setup_f_smtp_host']			= "SMTP Host";

	$mgrlang['setup_f_smtp_host_d']			= "SMTP שרת מארח.";

	$mgrlang['setup_f_smtp_port']			= "SMTP פתחה (port)";

	$mgrlang['setup_te_sent']				= "דואר בדיקה נשלח";

	$mgrlang['setup_f_smtp_port_d']			= "SMTP מספר פתחה עבור שרת זה. בדרך כלל 25.";	

	$mgrlang['setup_testemailat_mes']		= "אתה יכול לבדוק את הגדרות האימייל על-ידי שליחת מייל בדיקה ב- הגדרות > כלים > שלח אימייל בדיקה.";

	$mgrlang['setup_f_test_email']			= "שלח אימייל בדיקה";

	$mgrlang['setup_f_test_email_d']		= "שלח אימייל בדיקה לכתובת מייל תמיכה שמוגדר ב-הגדרות > הגדרות אתר > מידע ליצירת קשר.";	

	$mgrlang['setup_f_smtp_username']		= "SMTP שם משתמש";

	$mgrlang['setup_f_smtp_username_d']		= "SMTP שם משתמש שרת.";

	$mgrlang['setup_f_smtp_password']		= "SMTP סיסמא";

	$mgrlang['setup_f_smtp_password_d']		= "SMTP סיסמת שרת.";

	$mgrlang['setup_gd']					= "GD Library";

	$mgrlang['setup_imagemagick']			= "ImageMagick";

	$mgrlang['setup_imagemagickerr']		= "לא מותקן";

	$mgrlang['setup_f_flexpricing']			= "תמחור מתקדם";

	$mgrlang['setup_f_flexpricing_d']		= "הפעל אפשרויות תמחור מתקדם. לחץ כדי לקרוא עוד.";

	$mgrlang['setup_f_lbu']					= "גיבוי אחרון לבסיס הנתונים";

	$mgrlang['setup_f_lbu_d']				= "הזמן בו רץ הגיבוי האחרון לבסיס הנתונים.";

	$mgrlang['setup_f_srp']					= "נקודות שחזור עבור הגדרות";

	$mgrlang['setup_f_srp_d']				= "צור אוטומטית נקודת שחזור בכל פעם שהגדרות נשמרות. כולל איזורי 'הגדרות אתר', 'מראה והרגשה' ו- 'התקנת תוכנה'. אחרת נקודות שחזור יכולות להיווצר ידנית ב- הגדרות -> כלים.";

	$mgrlang['setup_f_nbu']					= "גיבוי הבא של בסיס הנתונים";

	$mgrlang['setup_f_nbu_d']				= "מתי ירוץ הגיבוי הבא.";

	$mgrlang['setup_f_dbbackup']			= "מרווח זמן לגיבוי בסיס הנתונים";

	$mgrlang['setup_f_dbbackup_d']			= "קבע את מרווחי הזמן בו בסיס הנתונים של האתר יגובה אל השרת אוטומטית.";

	$mgrlang['setup_f_dbbackup_o1']			= "פעם ביום";

	$mgrlang['setup_f_dbbackup_o2']			= "פעם בשבוע";

	$mgrlang['setup_f_dbbackup_o3']			= "פעם בחודש";

	$mgrlang['setup_f_dbbackup_o4']			= "לעולם לא";

	$mgrlang['setup_f_cron']				= "נתיב Cron Job";

	$mgrlang['setup_f_cron_d']				= "נתיב אל קובץ cron job כדי לקבוע cron jobs אופציונליים על השרת.";

	$mgrlang['setup_f_statshtml']			= "סטטיסטיקות HTML";

	$mgrlang['setup_f_statshtml_d']			= "הכנס HTML של מוני סטטיסטיקות חיצוניים לאתר.";

	$mgrlang['setup_f_savedbackup']			= "הורד גיבויים של בסיס הנתונים";

	$mgrlang['setup_f_savedbackup_d']		= "הורד עותק שמור של גיבוי בסיס הנתונים. בחר את תאריך הגיבוי ולחץ 'הורד'.";

	$mgrlang['setup_f_savedbackup_b']		= "הורד";

	$mgrlang['setup_f_nobackup']			= "לא נמצאו גיבויים";		

	$mgrlang['setup_f_serial']				= "מספר סידורי";

	$mgrlang['setup_f_serial_d']			= "המספר סידורי ששימש בהתקנת מוצר זה.";

	$mgrlang['setup_f_actkey']				= "מפתח הפעלה (Activation Key)";

	$mgrlang['setup_f_actkey_d']			= "מפתח הפעלה של התקנה זו.";

	$mgrlang['setup_f_valkey']				= "מפתח וידוא (Validation Key)";

	$mgrlang['setup_f_valkey_d']			= "מפתח וידוא של התקנה זו.";

	$mgrlang['setup_f_addons']				= "תוספים";

	$mgrlang['setup_f_addons_d']			= "תוספים מותקנים.";	

	$mgrlang['setup_f_version']				= "מידע גירסא";

	$mgrlang['setup_f_version_d']			= "שם מוצר וגירסא שמותקנים כרגע.";

	$mgrlang['setup_f_servinfo']			= "מידע שרת";

	$mgrlang['setup_f_servinfo_d']			= "הצג את phpinfo() עבור שרת זה.";

	$mgrlang['setup_f_opensi']				= "פתח מידע שרת";

	$mgrlang['setup_f_licagree']			= "הסכם רשיון התוכנה";

	$mgrlang['setup_f_licagree_d']			= "הצג את הסכם הרשיון לתוכנה זו.";

	$mgrlang['setup_f_api']					= "סיסמת API";

	$mgrlang['setup_f_api_d']				= "הסיסמא הדרושה כשמשתמשים ב- scripts API.";

	$mgrlang['setup_f_openla']				= "פתח הסכם רשיון";

	$mgrlang['setup_mes_01']				= "ה- URL שהכנסת לא מתאימה ל- URL של האתר.";

	$mgrlang['setup_mes_02']				= "נתיב ה- incoming שהכנסת לא קיים בשרת.";

	$mgrlang['setup_mes_03']				= "נתיב ה- incoming שהכנסת לא ניתן לכתיבה.";

	$mgrlang['setup_mes_05']				= "נתיב הגלריות שהכנסת אינו קיים באתר.";

	$mgrlang['setup_mes_06']				= "נתיב הגלריות שהכנסת אינו ניתן לכתיבה.";

	$mgrlang['setup_mes_07']				= "אפשרות החידוד דורשת שיהיה מותקן על השרת PHP 5 ומעלה.";

	$mgrlang['setup_tip_01']				= "כדי ליצור ידנית נקודת שחזור עבור ההגדרות לך ל- 'הגדרות > כלים'. ב- 'צור נקודת שחזור' לחץ 'צור'.";	

	$mgrlang['setup_image_caching']			= "זיכרון מטמון תמונה";

	$mgrlang['setup_image_caching_d']		= "שמור בזיכרון מטמון (Cache) תמונות כדי שייטענו מהר יותר. הגדרה זה יכולה להשתנות בקובץ tweak.php.";	

	$mgrlang['setup_ic_time']				= "זמן שמירת תמונה בזיכרון מטמון";

	$mgrlang['setup_ic_time_d']				= "פרק הזמן בשניות לשמירת תמונות בזיכרון מטמון (cache) לפני טעינת עותק חדש. הגדרה זו יכולה להשתנות בקובץ tweak.php.";

	$mgrlang['setup_seconds']				= "שניות";	

	$mgrlang['setup_en_cache']				= "אפשר זיכרון מטמון דף";

	$mgrlang['setup_en_cache_d']			= "שמור בזיכרון מטמון (Cache) דפים כדי שהם ייטענו יותר מהר. זה לא משפיע על תמונות.";	

	$mgrlang['setup_pc_period']				= "זמן שמירת דף בזיכרון מטמון";

	$mgrlang['setup_pc_period_d']			= "פרק הזמן בשניות של שמירת דף בזיכרון מטמון.";	

	$mgrlang['setup_customizer']			= "התאמה אישית";

	$mgrlang['setup_customizer_d']			= "התאם אישית מחירים ואפשרויות אחרות עבור הדפסות, מוצאים וגירסאות דיגיטליות עבור כל קובץ מדיה.";	

	$mgrlang['setup_htaccess']				= "תכונה זו דורשת קובץ .htaccess תקין בתיקיה ש- PhotoStore מותקן.";	

	$mgrlang['setup_f_autoenc']				= "קידוד תיקיה אוטומטי";

	$mgrlang['setup_f_autoenc_d']			= "ברירת המחדל האוטומטית עבור תיקיות ושמות קבצים שבתוכן שיהיו מקודדים, בלי שצריך לסמן אותם במיוחד.";

	$mgrlang['setup_f_libsize']				= "ספרייה";

	$mgrlang['setup_f_libsize_d']			= "גודל מוערך של ספריית המדיה שלך.";

	$mgrlang['setup_f_sfiletype']			= "סוגי קובץ נתמכים";

	$mgrlang['setup_f_sfiletype_d']			= "סוגי קובץ שנתמכים כרגע.";	

	

	# STORAGE

	$mgrlang['storage_t_alias']				= "כינוי";

	$mgrlang['storage_new_header']			= "הוסף פרופיל אחסון חדש";

	$mgrlang['storage_edit_header']			= "ערוך פרופיל אחסון";

	$mgrlang['storage_new_message']			= "בעזרת הטופס הבא תוכל להוסיף פרופיל אחסון חדש.";

	$mgrlang['storage_edit_message']		= "ערוך פרופיל אחסון זה ולחץ על כפתור 'שמור שינויים'.";	

	$mgrlang['storage_tab1']				= "פרטים";

	$mgrlang['storage_tab2']				= "קבוצות";

	$mgrlang['storage_tab3']				= "מתקדם";

	$mgrlang['storage_f_alias']				= "זהות";

	$mgrlang['storage_f_alias_d']			= "שם פרופיל אחסון זה.";

	$mgrlang['storage_f_groups']			= "קבוצות אחסון";

	$mgrlang['storage_f_groups_d']			= "בחר את קבוצות האחסון ששיטה זו שייכת אליהם.";

	$mgrlang['storage_f_st']				= "סוג אחסון";

	$mgrlang['storage_f_st_d']				= "סוג האחסון עבור פרופיל זה.";	

	$mgrlang['storage_f_stop1']				= "תיקיה בשרת";

	$mgrlang['storage_f_stop2']				= "FTP";

	$mgrlang['storage_f_stop3']				= "Amazon S3";

	$mgrlang['storage_f_stop4']				= "Rackspace Cloud Files";

	$mgrlang['storage_f_active']			= "פעיל";

	$mgrlang['storage_f_active_d']			= "הגדר פרופיל אחסון זה כפעיל.";	

	$mgrlang['storage_f_ftphost']			= "שרת מארח";

	$mgrlang['storage_f_ftphost_d']			= "FTP של שרת מארח או כתובת IP.";

	$mgrlang['storage_f_ftpport']			= "פתחה";

	$mgrlang['storage_f_ftpport_d']			= "מספר פתחה FTP לשימוש.";

	$mgrlang['storage_f_ftpuser']			= "שם משתמש";

	$mgrlang['storage_f_ftpuser_d']			= "שם משתמש חשבון FTP.";

	$mgrlang['storage_f_ftppass']			= "סיסמא";

	$mgrlang['storage_f_ftppass_d']			= "סיסמת חשבון FTP.";

	$mgrlang['storage_f_ftppath']			= "נתיב";

	$mgrlang['storage_f_ftppoath_d']		= "נתיב לתיקיה על שרת FTP.";

	$mgrlang['storage_f_localpath']			= "נתיב מוחלט";

	$mgrlang['storage_f_localpath_d']		= "הנתיב המוחלט למיקום האחסון.";

	$mgrlang['storage_b_test']				= "בדיקה";

	$mgrlang['storage_b_checking']			= "בודק...";

	$mgrlang['storage_f_media']				= "מדיה";

	$mgrlang['storage_f_media_d']			= "מספר מוערך של קבצי מדיה בתיקית אחסון זו.";

	$mgrlang['storage_f_space']				= "מקום בדיסק";

	$mgrlang['storage_f_space_d']			= "מקום בדיסק מוערך שהמדיה תופסת במיקום האחסון.";	

	$mgrlang['storage_f_as3key']			= "מפתח גישה";	

	$mgrlang['storage_f_as3key_d']			= "מפתח הגישה מחשבון AmazonS3 שלך.";

	$mgrlang['storage_f_as3skey']			= "מפתח סודי";	

	$mgrlang['storage_f_as3skey_d']			= "המפתח הסודי מחשבון AmazonS3 שלך.";	

	$mgrlang['storage_f_cf_username']		= "שם משתמש";	

	$mgrlang['storage_f_cf_username_d']		= "שם משתמש עבור חשבון Rackspace Cloud Files.";

	$mgrlang['storage_f_cf_apikey']			= "מפתח API";	

	$mgrlang['storage_f_cf_apikey_d']		= "מפתח ה- API עבור חשבון Rackspace Cloud Files.";	

	$mgrlang['storage_mes_ftp1']			= "FTP התחבר בהצלחה.";

	$mgrlang['storage_mes_ftp2']			= "FTP חיבור נכשל. כתובת השרת או פתחה (port) לא נכונים.";

	$mgrlang['storage_mes_ftp3']			= "FTP חיבור נכשל. שם משתמש או סיסמא לא נכונים.";

	$mgrlang['storage_mes_ftp4']			= "FTP חיבור נכשל. הנתיב לא נכון.";

	$mgrlang['storage_mes_ftp5']			= "לא ניתן ליצור חיבור FTP.";

	$mgrlang['storage_mes_ftp6a']			= "תכונות FTP לא נתמכות על-ידי שרת זה";

	$mgrlang['storage_mes_ftp6b']			= "שרת זה לא תומך בפונקציות הנדרשות כדי ליצור חיבורי FTP באמצעות PHP. צור קשר עם השרת המארח כדי להוסיף תמיכת FTP ב- PHP.";

	$mgrlang['storage_mes_local1']			= "נראה שהכנסת נתיב יחסי. אנא הכנס נתיב מוחלט.";

	$mgrlang['storage_mes_local2']			= "בדיקה הצליחה.";

	$mgrlang['storage_mes_local3']			= "בדיקה נכשלה. תיקיה לא ניתנת לכתיבה.";

	$mgrlang['storage_mes_local4']			= "תיקיה לא קיימת.";

	$mgrlang['storage_mes_as3_1']			= "חיבור ל- Amazon S3 הצליח.";

	$mgrlang['storage_mes_as3_2']			= "חיבור ל- Amazon S3 נכשל.";

	$mgrlang['storage_mes_as3_3a']			= "תכונות Amazon S3 לא נתמכות על-ידי שרת זה";

	$mgrlang['storage_mes_as3_3b']			= " שרת זה לא תומך בפונקציות הנדרשות כדי ליצור חיבור עם Amazon S3 על-ידי PHP. אנא צור קשר עם השרת המארח כדי להוסיף תמיכת cURL ב- PHP.";

	$mgrlang['storage_mes_cf_1']			= "חיבור ל- Rackspace Cloud Files הצליח.";

	$mgrlang['storage_mes_cf_2']			= "חיבור ל- Rackspace Cloud Files נכשל.";

	$mgrlang['storage_mes_cf_3a']			= "תכונת Rackspace Cloud Files לא נתמך על-ידי שרת זה";

	$mgrlang['storage_mes_cf_3b']			= "השרת לא תומך בפונקציות הנדרשות כדי ליצור חיבור עם Rackspace Cloud Files בעזרת PHP. אנא צור קשר עם השרת המארח כדי להוסיף תמיכת cURL ב- PHP.";

	

	# FOLDERS

	$mgrlang['folders_t_enc']				= "מקודד";

	$mgrlang['folders_tab1']				= "פרטים";

	$mgrlang['folders_tab2']				= "קבוצות";

	$mgrlang['folders_tab3']				= "מתקדם";

	$mgrlang['folders_f_name']				= "שם תיקיה";

	$mgrlang['folders_f_name_d']			= "שם התיקיה שבספריה. אותיות, מספרים, ו - _ בלבד. מקסימום 40 תווים.";

	$mgrlang['folders_f_storage']			= "אחסון";

	$mgrlang['folders_f_storage_d']			= "המיקום בו תיווצר תיקיה זו.";

	$mgrlang['folders_f_groups']			= "קבוצות תיקיות";

	$mgrlang['folders_f_groups_d']			= "בחר את קבוצות התיקיות להן תשוייך תיקיה זו.";

	$mgrlang['folders_f_media']				= "מדיה";

	$mgrlang['folders_f_media_d']			= "בערך מספר קבצי המדיה בתיקיה זו.";

	$mgrlang['folders_f_notes']				= "הערות";

	$mgrlang['folders_f_notes_d']			= "הערות על תיקיה זו לשימוש פנימי (אופציונלי).";

	$mgrlang['folders_f_space']				= "מקום בדיסק";

	$mgrlang['folders_f_space_d']			= "בערך המקום בדיסק שתופסת המדיה שבתיקיה זו.";	

	$mgrlang['folders_new_header']			= "הוסף תיקיה חדשה";

	$mgrlang['folders_edit_header']			= "ערוך תיקיה";

	$mgrlang['folders_new_message']			= "בעזרת הטופס הבא תוכל להוסיף תיקיה חדשה.";

	$mgrlang['folders_edit_message']		= "ערוך תיקיה זו ולחץ על כפתור 'שמור שינויים'.";

	$mgrlang['folders_f_sp']				= "הנתיב אל האחסון";

	$mgrlang['folders_f_sp_d']				= "הנתיב לתיקיה זו במיקום האיחסון שלך.";

	$mgrlang['folders_auto_manage_mes']		= "התיקיות מנוהלות עבורך באופן אוטומטי. כדי לשנות הגדרה זו:  הגדרות > התקנת תוכנה > מתקדם";

	

	# TAXES/VAT

	$mgrlang['tax_mes_01']					= "שיעור המס/מע\"מ מכיל תוים שאינם מספרים.";

	$mgrlang['tax_mes_02']					= "ערך המס/מע\"מ לא יכול לעלות על 99%";

	$mgrlang['tax_mes_03']					= "Using default Tax/VAT value";

	$mgrlang['tax_mes_04']					= "ערך המס/מע\"מ חייב לעלות על 1%";

	$mgrlang['tax_mes_05']					= "ערך ברירת המחדל של המס/מע\"מ מכיל תוים שאינם מספרים.";

	$mgrlang['tax_mes_06']					= "ערך ברירת מחדל המס/מע\"מ לא יכול לעלות על 99%";

	$mgrlang['tax_mes_07']					= "ערך המס/מע\"מ לא יכול לפחות מ 1%";

	$mgrlang['tax_tab1']					= "פרטים";

	$mgrlang['tax_tab2']					= "תצורה";

	$mgrlang['tax_tab3']					= "מתקדם";

	$mgrlang['tax_f_tdi']					= "החל מס על פריטים דיגיטליים";

	$mgrlang['tax_f_tdi_d']					= "לחץ כדי לאפשר. זה יעקוף כל הגדרות איזוריות.";

	$mgrlang['tax_f_tp']					= "החל מס על הדפסות/מוצרים";

	$mgrlang['tax_f_tp_d']					= "לחץ כדי לאפשר. זה יעקוף כל הגדרות איזוריות.";

	$mgrlang['tax_f_taxa']					= "שם מס A";

	$mgrlang['tax_f_taxa_d']				= "הצג שם עבור ערך מס A. דוגמאות: Tax, VAT(מע\"מ), GST, PST. השאר ריק בשביל אף מס A.";

	$mgrlang['tax_f_taxb']					= "שם מס B";

	$mgrlang['tax_f_taxb_d']				= "הצג שם עבור ערך מס B. השאר ריק בשביל אף מס B.";

	$mgrlang['tax_f_taxc']					= "שם מס C";

	$mgrlang['tax_f_taxc_d']				= "הצג שם עבור ערך מס C. השאר ריק בשביל אף מס C.";	

	$mgrlang['tax_f_dtr']					= "ערכי מס/מע\"מ ברירת מחדל";

	$mgrlang['tax_f_dtr_d']					= "אם לא נקבע שיעור מס/מע\"מ עבור איזור, ערכים אלו ישמשו במקום.";

	$mgrlang['tax_f_dtr_a']					= "מס A";

	$mgrlang['tax_f_dtr_b']					= "מס B";

	$mgrlang['tax_f_dtr_c']					= "מס C";

	$mgrlang['tax_f_inc']					= "כלול מס במחירים";

	$mgrlang['tax_f_inc_d']					= "הצג מחירים ללקוח כשהמס כבר כלול.";

	$mgrlang['tax_f_taxtype']				= "הגדרת מס";

	$mgrlang['tax_f_taxtype_d']				= "בחר להחיל את אותם מיסים על כולם או להחיל מיסים על-פי איזורים.";

	$mgrlang['tax_f_taxa_default']			= "מס A";

	$mgrlang['tax_f_taxa_default_d']		= "כמות המס לגביה עבור מס A.";

	$mgrlang['tax_f_taxb_default']			= "מס B";

	$mgrlang['tax_f_taxb_default_d']		= "כמות המס לגביה עבור מס B.";

	$mgrlang['tax_f_taxc_default']			= "מס C";

	$mgrlang['tax_f_taxc_default_d']		= "כמות המס לגביה עבור מס C.";

	$mgrlang['tax_f_tbr_d']					= "אפשרות זו מאפשרת לך להגדיר את החנות כך שתוכל להחיל מיסים על-פי מדינות, פרובינציות, ומיקודים. אחרי שמירה כשהאפשרות הזו דולקת, מיסים יוכלו להיקבע באיזורים אלו.";

	$mgrlang['tax_f_optout']				= "התחמקות";

	$mgrlang['tax_f_optout_d']				= "אפשר למשתמשים 'להתחמק' מלשלם מס (לא מומלץ).";

	$mgrlang['tax_f_stm']					= "הודעת מס מכירות";

	$mgrlang['tax_f_stm_d']					= "ייתכן ותצטרך על-פי החוק את מספר תיק המס שלך. השתמש בשדה זה כדי לספק את מספר תיק המס וסוג המס.";	

	$mgrlang['tax_globally']				= "מס גלובלי";

	$mgrlang['tax_by_region']				= "מס על-פי איזור";

	$mgrlang['tax_f_taxmem']				= "מס תוכניות הרשמה";

	

	# SOFTWARE UPGRADE

	$mgrlang['softup_mes_01']				= "זהו אינו קובץ התקנה תקין.";

	$mgrlang['softup_mes_02']				= "שדרוג הושלם! אתה אמור לראות את מספר הגירסא החדשה מתחת.";

	$mgrlang['softup_tab1']					= "פרטים";

	$mgrlang['softup_f_replace']			= "החלף קבצים";

	$mgrlang['softup_f_replace_d']			= "בסיס הנתונים עודכן. הקבצים הבאים השתנו ויצטרכו החלפה. כדי להשלים את השדרוג כל אחד מהקבצים ברשימה עם חדשים מהקובץ zip. בסיום לחץ 'done' ותילקח בחזרה לדף המשודרג.";

	$mgrlang['softup_f_current']			= "גירסא מותקנת";

	$mgrlang['softup_f_current_d']			= "הגירסא שכרגע מותקנת אצלך.";

	$mgrlang['softup_f_check']				= "הגירסא הזמינה החדשה ביותר";

	$mgrlang['softup_f_check_d']			= "הגירסא החדשה ביותר של התוכנה שזמינה כרגע.";

	$mgrlang['softup_f_upfile']				= "בחר שדרוג או קובץ תוסף";

	$mgrlang['softup_f_upfile_d']			= "בחר קובץ upgrade.php כדי לעבד. חפש את הקובץ עם הכפתור 'עיין' ואז לחץ 'עבד קובץ שדרוג'.";

	$mgrlang['softup_b_process']			= "עבד קובץ שדרוג";

	$mgrlang['softup_b_check_now']			= "בדוק כעת";

	$mgrlang['softup_error_01']				= "השדרוג לא הושלם בהצלחה.";

	

	# COUNTRIES

	$mgrlang['countries_t_name']			= "שם מדינה";

	$mgrlang['country_f_title']				= "שם מדינה";

	$mgrlang['country_f_title_d']			= "שם המדינה.";

	$mgrlang['country_tab1']				= "פרטים";

	$mgrlang['country_tab2']				= "משלוח";

	$mgrlang['country_tab3']				= "מיסים";

	$mgrlang['country_tab4']				= "מתקדם";

	$mgrlang['country_tab5']				= "קבוצות";

	$mgrlang['country_new_header']			= "הוסף מדינה חדשה";

	$mgrlang['country_edit_header']			= "ערוך מידע מדינה";

	$mgrlang['country_new_message']			= "בעזרת הטופס הבא ניתן להוסיף מדינה.";

	$mgrlang['country_edit_message']		= "ערוך מדינה זו ולחץ על כפתור 'שמור שינויים'.";

	$mgrlang['country_f_active']			= "פעיל";

	$mgrlang['country_f_active_d']			= "הגדר מדינה זו כפעילה ברשימה.";

	$mgrlang['country_f_code2']				= "קוד מדינה (2)";

	$mgrlang['country_f_code2_d']			= "קוד מדינה של 2 תוים.";

	$mgrlang['country_f_code3']				= "קוד מדינה (3)";

	$mgrlang['country_f_code3_d']			= "קוד מדינה של 3 תוים.";

	$mgrlang['country_f_numcode']			= "קוד מספרי";

	$mgrlang['country_f_numcode_d']			= "ה- ISO 3166-1 קוד מספרי שנקבע למדינה זו.";	

	$mgrlang['country_f_longitude']			= "קו אורך";

	$mgrlang['country_f_longitude_d']		= "קו אורך של המדינה.";

	$mgrlang['country_f_latitude']			= "קו רוחב";

	$mgrlang['country_f_latitude_d']		= "קו רוחב של המדינה.";

	$mgrlang['country_f_region']			= "איזור";

	$mgrlang['country_f_region_d']			= "איזור בו נמצאת המדינה. אם אין איזור השאר ריק, או NA.";	

	$mgrlang['country_f_tax_a']				= "מס A";

	$mgrlang['country_f_tax_a_d']			= "כמות המס לחיוב עבור מס A במדינה זו.";

	$mgrlang['country_f_tax_b']				= "מס B";

	$mgrlang['country_f_tax_b_d']			= "כמות המס לחיוב עבור מס B במדינה זו.";

	$mgrlang['country_f_tax_c']				= "מס C";

	$mgrlang['country_f_tax_c_d']			= "כמות המס לחיוב עבור מס C במדינה זו.";	

	$mgrlang['country_f_tax_prints']		= "מיסי הדפסות ומוצרים";

	$mgrlang['country_f_tax_prints_d']		= "החל מס על הדפסות ומוצרים, כולל חבילות, במדינה זו.";

	$mgrlang['country_f_tax_digital']		= "מיסי הורדות דיגיטליות";

	$mgrlang['country_f_tax_digital_d']		= "החל מס על הורדות דיגיטליות במדינה זו.";

	$mgrlang['country_f_tax_subs']			= "מיסי מנויים";

	$mgrlang['country_f_tax_subs_d']		= "החל מס על מנויים במדינה זו.";

	$mgrlang['country_f_tax_shipping']		= "מיסי משלוח";

	$mgrlang['country_f_tax_shipping_d']	= "החל מס לעלות משלוח הזמנה למדינה זו.";

	$mgrlang['country_f_tax_credits']		= "מיסי קרדיטים";

	$mgrlang['country_f_tax_credits_d']		= "החל מס כאשר לקוח רוכש קרדיטים במדינה זו.";

	$mgrlang['country_f_groups']			= "קבוצות מדינות";

	$mgrlang['country_f_groups_d']			= "בחר את קבוצות המדינות שמדינה זו שייכת אליהם.";

	

	# STATES

	$mgrlang['states_t_name']				= "שם מדינה/פרובינציה";

	$mgrlang['states_country']				= "מדינה";

	$mgrlang['states_edit_countries']		= "ערוך מדינות";	

	$mgrlang['states_f_title']				= "שם מדינה/פרובינציה";

	$mgrlang['states_f_title_d']			= "שם המדינה/פרובינציה.";

	$mgrlang['states_tab1']					= "פרטים";

	$mgrlang['states_tab2']					= "משלוח";

	$mgrlang['states_tab3']					= "מיסים";

	$mgrlang['states_tab4']					= "מתקדם";

	$mgrlang['states_new_header']			= "הוסף מדינה/פרובינציה";

	$mgrlang['states_edit_header']			= "ערוך מידע מדינה/פרובינציה";

	$mgrlang['states_new_message']			= "בעזרת הטופס הבא תוכל להוסיף מדינה/פרובינציה.";

	$mgrlang['states_edit_message']			= "ערוך מדינה/פרובינציה זו ולחץ על כפתור 'שמור שינויים'.";

	$mgrlang['states_f_country']			= "מדינה";

	$mgrlang['states_f_country_d']			= "המדינה בה נמצאת מדינה/פרובינציה זו.";

	$mgrlang['states_f_active']				= "פעיל";

	$mgrlang['states_f_active_d']			= "קבע מדינה/פרובינציה זו כפעילה ברשימה.";

	$mgrlang['states_f_scode']				= "קוד מדינה/פרובינציה";

	$mgrlang['states_f_scode_d']			= "ראשי תיבות או קוד של המדינה/פרובינציה.";

	$mgrlang['states_f_tax_a']				= "מס A";

	$mgrlang['states_f_tax_a_d']			= "כמות המס לחיוב עבור מס A במדינה/פרובינציה זו.";

	$mgrlang['states_f_tax_b']				= "מס B";

	$mgrlang['states_f_tax_b_d']			= "כמות המס לחיוב עבור מס B במדינה/פרובינציה זו.";

	$mgrlang['states_f_tax_c']				= "מס C";

	$mgrlang['states_f_tax_c_d']			= "כמות המס לחיוב עבור מס C במדינה/פרובינציה זו.";	

	$mgrlang['states_f_tax_prints']			= "מס הדפסות ומוצרים";

	$mgrlang['states_f_tax_prints_d']		= "החל מס על הדפסות ומוצרים כולל חבילות במדינה/פרובינציה זו.";

	$mgrlang['states_f_tax_digital']		= "מס הורדות דיגיטליות";

	$mgrlang['states_f_tax_digital_d']		= "החל מס על הורדות דיגיטליות במדינה/פרובינציה זו.";

	$mgrlang['states_f_tax_subs']			= "מס מנויים";

	$mgrlang['states_f_tax_subs_d']			= "החל מס על מנויים במדינה/פרובינציה זו.";

	$mgrlang['states_f_tax_shipping']		= "מס משלוח";

	$mgrlang['states_f_tax_shipping_d']		= "החל מס על עלות משלוח הזמנה במדינה/פרובינציה זו.";

	$mgrlang['states_f_tax_credits']		= "מס קרדיטים";

	$mgrlang['states_f_tax_credits_d']		= "החל מס כאשר לקוח רוכש קרדיטים במדינה/פרובינציה זו.";

	$mgrlang['states_f_tax_mems']			= "מס תוכניות הרשמה";

	$mgrlang['states_f_tax_mems_d']			= "החל מס כאשר לקוח רוכש תוכנית הרשמה במדינה/פרובינציה זו.";

	

	# ZIPCODES

	$mgrlang['zipcodes_t_name']				= "מיקוד";

	$mgrlang['zipcodes_region']				= "איזור";

	$mgrlang['zipcodes_edit_countries']		= "ערוך מדינות";

	$mgrlang['zipcodes_edit_states']		= "ערוך מדינות/פרובינציות";

	$mgrlang['zipcodes_tab1']				= "פרטים";

	$mgrlang['zipcodes_tab2']				= "משלוח";

	$mgrlang['zipcodes_tab3']				= "מיסים";

	$mgrlang['zipcodes_tab4']				= "מתקדם";

	$mgrlang['zipcodes_new_header']			= "הוסף מיקוד חדש";

	$mgrlang['zipcodes_edit_header']		= "ערוך מיקוד";

	$mgrlang['zipcodes_new_message']		= "בעזרת הטופס הבא תוכל להוסיף מיקוד.";

	$mgrlang['zipcodes_edit_message']		= "ערוך את המיקוד ולץ על כפתור 'שמור שינויים'.";

	$mgrlang['zipcodes_f_region']			= "איזור";

	$mgrlang['zipcodes_f_region_d']			= "האיזור שמיקוד זה שייך אליו.";

	$mgrlang['zipcodes_f_zipcode']			= "מיקוד";

	$mgrlang['zipcodes_f_active_d']			= "xxxx";

	$mgrlang['zipcodes_f_active']			= "פעיל";

	$mgrlang['zipcodes_f_active_d']			= "הגדר מיקוד זה כפעיל ברשימה.";

	$mgrlang['zipcodes_f_tax_a']			= "מס A";

	$mgrlang['zipcodes_f_tax_a_d']			= "כמות המס לחיוב עבור מס A במיקוד זה.";

	$mgrlang['zipcodes_f_tax_b']			= "מס B";

	$mgrlang['zipcodes_f_tax_b_d']			= "כמות המס לחיוב עבור מס B במיקוד זה.";

	$mgrlang['zipcodes_f_tax_c']			= "מס C";

	$mgrlang['zipcodes_f_tax_c_d']			= "כמות המס לחיוב עבור מס C במיקוד זה.";	

	$mgrlang['zipcodes_f_tax_prints']		= "מס הדפסות ומוצרים";

	$mgrlang['zipcodes_f_tax_prints_d']		= "החל מס על הדפסות ומוצרים כולל חבילות, במיקוד זה.";

	$mgrlang['zipcodes_f_tax_digital']		= "מס הורדות דיגיטליות";

	$mgrlang['zipcodes_f_tax_digital_d']	= "החל מס על הורדות דיגיטליות במיקוד זה.";

	$mgrlang['zipcodes_f_tax_subs']			= "מס מנויים";

	$mgrlang['zipcodes_f_tax_subs_d']		= "החל מס על מנויים במיקוד זה.";

	$mgrlang['zipcodes_f_tax_shipping']		= "מס משלוח";

	$mgrlang['zipcodes_f_tax_shipping_d']	= "החל מס על עלות משלוח ההזמנה במיקוד זה.";

	$mgrlang['zipcodes_f_tax_credits']		= "מס קרדיטים";

	$mgrlang['zipcodes_f_tax_credits_d']	= "החל מס כאשר לקוח רוכש קרדיטים במיקוד זה.";

		

	# SHIPPING

	$mgrlang['shipping_new_header']			= "הוסף שיטת משלוח חדשה";

	$mgrlang['shipping_edit_header']		= "ערוך שיטת משלוח";

	$mgrlang['shipping_new_message']		= "בעזרת הטופס הבא ניתן להוסיף שיטת משלוח חדשה.";

	$mgrlang['shipping_edit_message']		= "ערוך שיטת משלוח זו ולחץ על כפתור 'שמור שינויים'.";

	$mgrlang['shipping_tab1']				= "פרטים";

	$mgrlang['shipping_tab2']				= "תמחור";

	$mgrlang['shipping_tab3']				= "קבוצות";

	$mgrlang['shipping_tab4']				= "איזורים";

	$mgrlang['shipping_tab5']				= "מתקדם";

	$mgrlang['shipping_f_title']			= "שם שיטת משלוח";

	$mgrlang['shipping_f_title_d']			= "שם שיטת המשלוח. לדוגמא: משלוח של 2-ימים";

	$mgrlang['shipping_f_desc']				= "תיאור";

	$mgrlang['shipping_f_desc_d']			= "תיאור קצר של שיטת המשלוח.";

	$mgrlang['shipping_f_active']			= "פעיל";

	$mgrlang['shipping_f_active_d']			= "קבע שיטת משלוח זו כפעילה.";	

	$mgrlang['shipping_f_ship_days']		= "ימי משלוח";

	$mgrlang['shipping_f_ship_days_d']		= "טווח הימים שלוקח כדי לקבל את הפריט אחרי שהוא נשלח. הגדרה זו היא אופציונלית.";

	$mgrlang['shipping_f_ship_days1']		= "ימים";

	$mgrlang['shipping_f_taxable']			= "חייב במס";

	$mgrlang['shipping_f_taxable_d']		= "סמן אם שיטת משלוח זו חייבת במס.";

	$mgrlang['shipping_f_ship_notes']		= "הערות משלוח פנימיות";

	$mgrlang['shipping_f_ship_notes_d']		= "הערות שיעזרו לך לזכור פרטים נוספים אודות שיטת משלוח זו. זה לא יוצג באתר הציבורי.";	

	$mgrlang['shipping_f_module']			= "יחידת משלוח";

	$mgrlang['shipping_f_module_d']			= "השתמש בחברת משלוחים מוגדרת מראש ובחישובים שלהם, או צור משלך.";

	$mgrlang['shipping_f_module_op1']		= "מותאם אישית";

	$mgrlang['shipping_f_calc']				= "חשב משלוח על-ידי";

	$mgrlang['shipping_f_calc_d']			= "חשב משלוח על-פי סיכום ביניים, כמות או משקל של ההזמנה.";	

	$mgrlang['shipping_f_calc_op1']			= "משקל";

	$mgrlang['shipping_f_calc_op2']			= "סיכום ביניים של פריטים הניתנים למשלוח";

	$mgrlang['shipping_f_calc_op3']			= "מחיר אחיד";

	$mgrlang['shipping_f_calc_op4']			= "כמות";

	$mgrlang['shipping_f_ccalc']			= "חישוב עלות";

	$mgrlang['shipping_f_ccalc_d']			= "העלות מוגדרת בסכומים קבועים או באחוזים.";

	$mgrlang['shipping_f_ccalc_op1']		= "סכום קבוע";

	$mgrlang['shipping_f_ccalc_op2']		= "אחוז מסיכום הביניים";

	$mgrlang['shipping_f_calc_from']		= "מאת";

	$mgrlang['shipping_f_calc_to']			= "אל";

	$mgrlang['shipping_f_calc_cost']		= "עלות";

	$mgrlang['shipping_f_calc_inf']			= "ומעלה";

	$mgrlang['shipping_f_ranges']			= "טווחים";

	$mgrlang['shipping_f_ranges_d']			= "הכנס טווחים בהם נוספה עלות המשלוח.";

	$mgrlang['shipping_f_flatrate']			= "מחיר אחיד";

	$mgrlang['shipping_f_flatrate_d']		= "הכנס מחיר אחיד או אחוזים עבור שיטת משלוח זו.";

	$mgrlang['shipping_f_groups']			= "קבוצות משלוחים";

	$mgrlang['shipping_f_groups_d']			= "בחר את קבוצות המשלוחים ששיטת משלוח זו שייכת אליהם.";

	$mgrlang['shipping_f_regions']			= "איזורי משלוח";

	$mgrlang['shipping_f_regions_d']		= "בחר את האיזורים ששיטת משלוח זו זמינה עבורם.";

	$mgrlang['shipping_f_regions_op1']		= "בכל מקום";

	$mgrlang['shipping_f_regions_op2']		= "איזורים נבחרים";

	$mgrlang['shipping_f_regions_op1_d']	= "שיטת משלוח זו זמינה עבור כל המדינות.";

	$mgrlang['shipping_f_regions_op2_d']	= "שיטת משלוח זו זמינה רק עבור האיזורים שנבחרו ברשימה למטה";	

	$mgrlang['shipping_mes_01']				= "ערך הטווח 'מ-' הוא ריק או שאינו מספר.";

	$mgrlang['shipping_mes_02']				= "ערך הטווח 'עד-' הוא ריק או שאינו מספר.";

	$mgrlang['shipping_mes_03']				= "המחיר עבור טווח זה הוא ריק או שאינו מספר.";

	$mgrlang['shipping_mes_04']				= "ערך הטווח 'עד-' לא יכול להיות קטן או שווה לערך 'מ-'.";

	$mgrlang['shipping_mes_05']				= "הכנס מחיר אחיד למשלוח.";

	$mgrlang['shipping_mes_06']				= "אנא הכנס עלות עבור השורה האחרונה.";

	

	# CURRENCIES

	$mgrlang['currencies_t_defaultcur']		= "ראשי";

	$mgrlang['currencies_t_er']				= "שער חליפין";	

	$mgrlang['currencies_wb_upt']			= "עדכן שערי מטבע";

	$mgrlang['currencies_wb_upd']			= "כדי לעדכן את שערי המטבע הנוכחיים מ- Yahoo או Google לחץ על כפתור 'עדכן'. רק מטבעות פעילים יעודכנו.";

	$mgrlang['currencies_dd_ur']			= "עדכן שערים";

	$mgrlang['currencies_mes1']				= "שער עודן על-ידי";

	$mgrlang['currencies_mes2']				= "עדכונים הושלמו";

	$mgrlang['currencies_mes3']				= "מעדכן...";

	$mgrlang['currencies_error1']			= "עדכון נכשל - המטבע לא נתמך על-ידי Google או Yahoo";

	$mgrlang['currencies_error2']			= "עדכון נכשל";

	$mgrlang['currencies_error3']			= "לא ניתן לבטל את המטבע הראשי.";

	$mgrlang['currencies_tab1']				= "פרטים";

	$mgrlang['currencies_tab2']				= "שער חליפין";

	$mgrlang['currencies_new_header']		= "הוסף מטבע חדש";

	$mgrlang['currencies_edit_header']		= "ערוך מידע מטבע";

	$mgrlang['currencies_new_message']		= "בעזרת הטופס הבא תוכל להוסיף מטבע חדש.";

	$mgrlang['currencies_edit_message']		= "ערוך מטבע זה ולחץ על כפתור 'שמור שינויים'.";

	$mgrlang['currencies_f_name']			= "שם מטבע";

	$mgrlang['currencies_f_name_d']			= "שם המטבע שאתה מוסיף.";

	$mgrlang['currencies_f_code']			= "קוד מטבע";

	$mgrlang['currencies_f_code_d']			= "קוד 3 ספרות של המטבע שאתה מוסיף.";

	$mgrlang['currencies_f_active']			= "פעיל";

	$mgrlang['currencies_f_active_d']		= "הגדר מטבע כפעיל. אם יש יותר ממטבע אחד פעיל בו זמנית, המשתמש יוכל לבחור במטבע מתוך רשימה.";

	$mgrlang['currencies_f_defaultcur']		= "ברירת מחדל";

	$mgrlang['currencies_f_defaultcur_d']	= "הגדר מטבע זה כברירת מחדל או מטבע ראשי של האתר.";

	$mgrlang['currencies_f_den']			= "סמל או קיצור";

	$mgrlang['currencies_f_den_d']			= "סמל או קיצור של המטבע, שישמש בעת הצגת מחיר.";

	$mgrlang['currencies_f_dec_sep']		= "נקודה עשרונית";

	$mgrlang['currencies_f_dec_sep_d']		= "תו לשימוש עבור נקודה עשרונית.";

	$mgrlang['currencies_f_thou_sep']		= "מפריד אלפים";

	$mgrlang['currencies_f_thou_sep_d']		= "תו לשימוש עבור מפריד אלפים.";

	$mgrlang['currencies_f_thou_sep_none']	= "ללא";

	$mgrlang['currencies_f_thou_sep_space']	= "רווח";

	$mgrlang['currencies_f_num_after']		= "מקומות עשרוניים";

	$mgrlang['currencies_f_num_after_d']	= "מספר מקומות אחרי הנקודה העשרונית.";

	$mgrlang['currencies_f_neg_format']		= "פורמט מטבע שלילי";

	$mgrlang['currencies_f_neg_format_d']	= "הפורמט לשימוש עבור מטבע שלילי.";

	$mgrlang['currencies_f_pos_format']		= "פורמט מטבע חיובי";

	$mgrlang['currencies_f_pos_format_d']	= "הפורמט לשימוש עבור מטבע חיובי.";

	$mgrlang['currencies_f_num_neg']		= "שלילי";

	$mgrlang['currencies_f_num_pos']		= "חיובי";

	$mgrlang['currencies_f_num_preview']	= "תצוגה מקדימה של המטבעות";

	$mgrlang['currencies_f_er_preview']		= "תצוגה מקדימה של חליפין";	

	$mgrlang['currencies_f_er']				= "שער חליפין";

	$mgrlang['currencies_f_er_d']			= "שער החליפין מהמטבע הראשי או ברירת המחדל שלך אל מטבע זה.";

	$mgrlang['currencies_f_last_update']	= "עודכן לאחרונה";

	$mgrlang['currencies_f_last_update_d']	= "הפעם האחרונה שרישום מטבע זה עודכן.";

	$mgrlang['currencies_f_never_update']	= "לעולם לא";

	$mgrlang['currencies_f_er_ffg']			= "עדכן ערך מ- Google";

	$mgrlang['currencies_f_er_ffy']			= "עדכן ערך מ- Yahoo";

	$mgrlang['currencies_f_er_fetching']	= "מושך מידע";

	$mgrlang['currencies_f_auto_update']	= "עודכן לאחרונה";

	$mgrlang['currencies_f_auto_update_d']	= "משוך אוטומטית את הערכים העדכניים על בסיס יומי. דורש התחברות כמנהל או הגדרת cronjob.";

	$mgrlang['currencies_f_er_updater']		= "השתמש לעדכון";

	$mgrlang['currencies_f_er_updater_d']	= "בזמן עדכונים אוטומטיים השתמש בהבא כדי למצוא את הערך.";

	$mgrlang['currencies_updated_24hours']	= "שער החליפין עודכן ב- 24 שעות האחרונות.";

	

	# MEDIA TYPES

	$mgrlang['media_type_new_header']		= "הוסף סוג מדיה";

	$mgrlang['media_type_edit_header']		= "ערוך סוג מדיה";

	$mgrlang['media_type_new_message']		= "בעזרת הטופס הבא תוכל להוסיף סוג מדיה חדש.";

	$mgrlang['media_type_edit_message']		= "ערוך את סוג המדיה ולחץ על כפתור 'שמור שינויים'.";

	$mgrlang['media_type_tab1']				= "פרטים";

	$mgrlang['media_type_tab2']				= "קבוצות";

	$mgrlang['media_type_f_name']			= "שם סוג מדיה";

	$mgrlang['media_type_f_name_d']			= "שם סוג המדיה. לדוגמא: אילוסטרציות.";	

	$mgrlang['media_type_f_groups']			= "קבוצות סוגי מדיה";

	$mgrlang['media_type_f_groups_d']		= "בחר את הקבוצות להן שייך סוג מדיה זה.";

	$mgrlang['media_type_f_active_d']		= "הגדר סוג מדיה פעיל. סוגי מדיה לא פעילים לא יוצגו באתר. מדיה ששייכת לסוג שכזה בכל זאת תוצג.";

	

	# MEDIA COMMENT

	$mgrlang['media_comments_t_media']		= "מדיה";

	$mgrlang['media_comments_t_member']		= "משתמש רשום";

	$mgrlang['media_comments_t_comment']	= "הערה";	

	$mgrlang['media_comments_edit_header']	= "ערוך הערת מדיה";

	$mgrlang['media_comments_edit_message']	= "ערוך הערה זו שפורסמה על-ידי מבקר ולחץ על כפתור 'שמור שינויים'.";

	$mgrlang['media_comments_tab1']			= "פרטים";

	$mgrlang['media_comments_f_com']		= "הערה";

	$mgrlang['media_comments_f_com_d']		= "הערה שפורסמה על-ידי מבקר על המדיה הזו.";

	$mgrlang['media_comments_f_postd']		= "פורסם ב- תאריך/שעה";

	$mgrlang['media_comments_f_postd_d']	= "תאריך ושעה בהם פורסמה ההערה.";

	$mgrlang['media_comments_f_status']		= "סטטוס";

	$mgrlang['media_comments_f_status_d']	= "סטטוס הערה זו. רק הערות מאושרות יוצגו באתר הציבורי.";

	$mgrlang['media_comments_f_mem']		= "משתמש";

	$mgrlang['media_comments_f_mem_d']		= "המשתמש שכתב את ההערה. אם זה היה מבקר לא רשום זה יוצג כ- 'אורח' (Guest).";

	$mgrlang['media_comments_f_med']		= "מדיה";

	$mgrlang['media_comments_f_med_d']		= "מדיה שהערה זו הושארה עבורה.";

	

	# MEDIA RATINGS

	$mgrlang['media_ratings_t_rating']		= "דירוג";

	$mgrlang['media_ratings_edit_header']	= "ערוך דירוגי מדיה";

	$mgrlang['media_ratings_edit_message']	= "ערוך דירוג זה שנעשה על-ידי משתמש ולחץ על כפתור 'שמור שינויים'.";

	$mgrlang['media_ratings_f_rating']		= "דירוג";

	$mgrlang['media_ratings_f_rating_d']	= "דירוג שנעשה על-ידי מבקר או משתמש.";

	$mgrlang['media_ratings_tab1']			= "פרטים";

	$mgrlang['media_ratings_f_postd']		= "הועלה בתאריך/שעה";

	$mgrlang['media_ratings_f_postd_d']		= "תאריך ושעה שדירוג זה נעשה.";

	$mgrlang['media_ratings_f_status']		= "סטטוס";

	$mgrlang['media_ratings_f_status_d']	= "סטטוס דירוג זה. רק דירוגים מאושרים יוצגו באתר הציבורי.";

	$mgrlang['media_ratings_f_mem']			= "משתמש";

	$mgrlang['media_ratings_f_mem_d']		= "משתמש שהשאיר דירוג זה. אם זה היה מישהו ללא חשבון או לא מחובר, זהו אורח (Guest).";

	$mgrlang['media_ratings_f_med']			= "מדיה";

	$mgrlang['media_ratings_f_med_d']		= "מדיה עליה נכתבה ההערה.";

	

	# MEDIA TAGS

	$mgrlang['media_tags_t_tag']			= "תווית";

	$mgrlang['media_tags_edit_header']		= "ערוך תווית מדיה";

	$mgrlang['media_tags_edit_message']		= "ערוך תווית שנכתבה על-ידי מבקר ולחץ על כפתור 'שמור שינויים'.";

	$mgrlang['media_tags_tab1']				= "פרטים";

	$mgrlang['media_tags_f_tag']			= "תוית";

	$mgrlang['media_tags_f_tag_d']			= "תוית שפורסמה על-ידי מבקר על המדיה שמעל.";

	$mgrlang['media_tags_f_postd']			= "הועלה בתאריך/שעה";

	$mgrlang['media_tags_f_postd_d']		= "תאריך ושעה שתג זה נשלח.";

	$mgrlang['media_tags_f_status']			= "סטטוס";

	$mgrlang['media_tags_f_status_d']		= "סטטוס תג זה. רק תגים מאושרים יוצגו באתר הציבורי.";

	$mgrlang['media_tags_f_mem']			= "משתמש";

	$mgrlang['media_tags_f_mem_d']			= "משתמש שפרסם תג זה. אם זהו מישהו ללא חשבון או שהוא לא היה מחובר, זהו אורח.";

	$mgrlang['media_tags_f_med']			= "מדיה";

	$mgrlang['media_tags_f_med_d']			= "מדיה שהתווית נכתבה בשבילה.";

	

	# BILLINGS

	$mgrlang['billings_new_header']			= "צור חשבון חדש";

	$mgrlang['billings_edit_header']		= "ערוך חשבון";

	$mgrlang['billings_new_message']		= "השתמש בטופס הבא כדיי ליצור חשבון חדש עבור משתמש.";

	$mgrlang['billings_edit_message']		= "ערוך חשבון זה ולחץ על כפתור 'שמור שינויים'.";

	$mgrlang['billings_f_method']			= "שיטה";

	$mgrlang['billings_f_method_d']			= "איך תרצה ליצור חשבון זה.";

	$mgrlang['billings_f_member']			= "עבור משתמש";

	$mgrlang['billings_no_bill_later']		= "אין הזמנות 'חייב אותי מאוחר יותר' כרגע.";

	$mgrlang['billings_bill_date']			= "תאריך חשבון";

	$mgrlang['billings_display']			= "הצג חשבונות עם סטטוסים אלו";

	$mgrlang['billings_invnum_bill']		= "מספר החשבונית עבור חשבון זה.";

	$mgrlang['billings_for_mem']			= "עבור משתמש";

	$mgrlang['billings_f_items_d']			= "פריטים שכלולים בחשבון זה.";

	$mgrlang['billings_f_orders']			= "הזמנות";

	$mgrlang['billings_f_orders_d']			= "הזמנות 'חייב אותי מאוחר יותר' שכלולים בחשבון זה.";

	$mgrlang['billings_f_status']			= "סטטוס";

	$mgrlang['billings_f_status_d']			= "סטטוס תשלום בחשבון זה.";

	$mgrlang['billings_f_bill_date_d']		= "התאריך בו נוצר חשבון זה.";

	$mgrlang['billings_f_due_date']			= "מועד פירעון";

	$mgrlang['billings_f_due_date_d']		= "תאריך בו חשבון זה צריך להיפרע.";

	$mgrlang['billings_f_groups']			= "קבוצות חיובים";

	

	# PAYMENT GATEWAYS

	$mgrlang['pmntgate_edit_header']		= "הגדר דרכי תשלום בהם אפשר להשתמש באתר.";	

	$mgrlang['pmntgate_actgate']			= "הפעל אמצעי תשלום";

	

	# MEMBERSHIPS

	$mgrlang['membership']					= "תוכנית הרשמה";

	$mgrlang['membership_tab1']				= "פרטי תוכנית";

	$mgrlang['membership_tab2']				= "תמחור";

	$mgrlang['membership_tab3']				= "סטטיסטיקות";

	$mgrlang['membership_tab4']				= "מכסות";

	$mgrlang['membership_tab5']				= "אפשרויות";

	$mgrlang['membership_tab6']				= "קבוצות";

	$mgrlang['membership_t_sortorder']		= "הזמנה";

	$mgrlang['membership_new_header']		= "הוסף תוכנית הרשמה חדשה";

	$mgrlang['membership_edit_header']		= "ערוך תוכנית הרשמה";

	$mgrlang['membership_new_message']		= "השתמש בטופס הבא כדי ליצור תוכנית הרשמה חדשה.";

	$mgrlang['membership_edit_message']		= "ערוך תוכנית הרשמה ולחץ על כפתור 'שמור שינויים'.";

	$mgrlang['membership_f_name']			= "שם תוכנית";

	$mgrlang['membership_f_name_d']			= "שם תוכנית ההרשמה.";

	$mgrlang['membership_f_groups']			= "קבוצות תוכניות";

	$mgrlang['membership_f_groups_d']		= "בחר את קבוצות התוכניות אליהן שייכת תוכנית הרשמה זו.";

	$mgrlang['membership_f_flag']			= "אייקון";

	$mgrlang['membership_f_flag_d']			= "אייקון שמשמש לזיהוי תוכנית הרשמה זו באיזור הניהול.";

	$mgrlang['membership_f_directlink']		= "קישור ישיר";

	$mgrlang['membership_f_directlink_d']	= "קישור ישיר להרשמה לתוכנית הרשמה זו.";

	$mgrlang['membership_f_description']	= "תיאור";

	$mgrlang['membership_f_description_d']	= "תיאור תוכנית הרשמה זו.";

	$mgrlang['membership_default']			= "תוכנית הרשמה ברירת-מחדל";	

	$mgrlang['membership_f_active']			= "פעיל";

	$mgrlang['membership_f_active_d']		= "הגדר תוכנית הרשמה זו כפעילה או לא. הגדרת תוכנית זו כלא פעילה לא תאפשר למשתמשים חדשים להירשם אליה, אך לא תשפיע על מי שכבר נרשם אליה.";

	$mgrlang['membership_f_ms_type']		= "סוג תוכנית הרשמה";

	$mgrlang['membership_f_ms_type_d']		= "בחר את סוג תוכנית ההרשמה. חינם, חד פעמי או חוזרת.";

	$mgrlang['membership_f_ms_type_op1']	= "חינם";

	$mgrlang['membership_f_ms_type_op2']	= "חד-פעמי";

	$mgrlang['membership_f_ms_type_op3']	= "חוזרת";

	$mgrlang['membership_f_trial']			= "תקופת ניסיון";

	$mgrlang['membership_f_trial_d']		= "אפשר תקופת ניסיון עבור תוכנית הרשמה זו.";

	$mgrlang['membership_f_op1']			= "ללא";

	$mgrlang['membership_f_op2']			= "תקופת ניסיון";

	$mgrlang['membership_f_op3']			= "ימים";

	$mgrlang['membership_f_op4']			= "שבועות";

	$mgrlang['membership_f_op5']			= "חודשים";

	$mgrlang['membership_f_op6']			= "שנים";

	$mgrlang['membership_f_cost']			= "עלות";

	$mgrlang['membership_f_cost_d']			= "עלות הרשמה לתוכנית זו. השאר ריק או הכנס 0 עבור ללא עלות.";

	$mgrlang['membership_f_cost_h1']		= "מחיר";

	$mgrlang['membership_f_cost_h2']		= "תקופה";

	$mgrlang['membership_f_cost_op1']		= "שבועי";

	$mgrlang['membership_f_cost_op2']		= "חודשי";

	$mgrlang['membership_f_cost_op3']		= "רבעוני";

	$mgrlang['membership_f_cost_op4']		= "חצי-שנתי";

	$mgrlang['membership_f_cost_op5']		= "שנתי";

	$mgrlang['membership_f_ms_mems']		= "משתמשים בתוכנית הרשמה";

	$mgrlang['membership_f_ms_mems_d']		= "משתמשים בתוכנית ההרשמה הזו.";

	$mgrlang['membership_f_showmem']		= "הצג משתמשים";

	$mgrlang['membership_f_bio']			= "ביוגרפיה";

	$mgrlang['membership_f_bio_d']			= "אפשר למשתמשים בתוכנית הרשמה זו להוסיף ביוגרפיה קצרה על עצמם.";

	$mgrlang['membership_manual']			= "אישור ידני";

	$mgrlang['membership_auto']				= "אישור אוטומטי";

	$mgrlang['membership_f_avatar']			= "דמות";

	$mgrlang['membership_f_avatar_d']		= "אפשר למשתמשים בתוכנית הרשמה זו להעלות דמות שתייצג אותם. ואז, בחר האם הדמות צריכה אישור מנהל לפני שתוצג באתר.";

	$mgrlang['membership_f_lightboxes']		= "תיבות אישיות";

	$mgrlang['membership_f_lightboxes_d']	= "אפשר למשתמשים בתוכנית הרשמה זו ליצור תיבות אישיות.";

	$mgrlang['membership_f_comments']		= "הערות";

	$mgrlang['membership_f_comments_d']		= "אפשר למשתמשים בתוכנית הרשמה זו לפרסם הערות על תמונות ומדיה אחרת. ואז בחר האם הערה צריכה אישור ממנהל לפני שהיא מוצגת באתר.";

	$mgrlang['membership_f_rating']			= "דירוג מדיה";

	$mgrlang['membership_f_rating_d']		= "אפשר למשתמשים בתוכנית הרשמה זו לדרג תמונות ומדיה אחרת. ואז בחר אם דירוגים צריכים אישור ממנהל לפני שהם מוצגים באתר.";

	$mgrlang['membership_f_tagging']		= "תיוג קהילתי";

	$mgrlang['membership_f_tagging_d']		= "אפשר למשתמשים בתוכנית הרשמה זו להוסיף תגים לתמונות ומדיה אחרת. ואז בחר האם תגים אלו יצטרכו אישור ממנהל לפני שיוצגו באתר.";

	$mgrlang['membership_f_memgroups']		= "קבוצות משתמשים";

	$mgrlang['membership_f_memgroups_d']	= "בזמן התחברות לתוכנית הרשמה זו המשתמשים הופכים אוטומטית לחלק מקבוצות אלו.";

	$mgrlang['membership_f_coupons']		= "קופונים";

	$mgrlang['membership_f_coupons_d']		= "חלק למשתמשים אלו קופונים או הצעות הנחה בזמן התחברות לתוכנית הרשמה זו.";

	$mgrlang['membership_f_galsuggest']		= "אפשר הצעת גלריות";

	$mgrlang['membership_f_galsuggest_d']	= "אפשר למשתמשים ששייכים לתוכנית הרשמה זו להציע גלריות להוספה.";

	$mgrlang['membership_f_medrequest']		= "בקש תמונות/מדיה";

	$mgrlang['membership_f_medrequest_d']	= "אפשר למשתמשים ששייכים לתוכנית הרשמה זו לבקש תמונות או מדיה אחרת להוספה.";

	$mgrlang['membership_f_featured']		= "רשום כתורם";

	$mgrlang['membership_f_featured_d']		= "רשום משתמשים של תוכנית הרשמה זו כתורמים בכל דפי התורמים.";

	$mgrlang['membership_f_auploads']		= "אפשר העלאות";

	$mgrlang['membership_f_auploads_d']		= "אפשר למשתמשים בתוכנית הרשמה זו להעלות תמונות ומדיה אחרת.";

	$mgrlang['membership_f_diskspace']		= "מקום בדיסק";

	$mgrlang['membership_f_diskspace_d']	= "כמות המקום בדיסק לאפשר למשתמשים אלו להשתמש עם התמונות שלהם.";

	$mgrlang['membership_f_admingal']		= "גלריות מנהלים";

	$mgrlang['membership_f_admingal_d']		= "אפשר למשתמשים בתוכנית הרשמה זו להעלות תמונות לגלריות שאתה יצרת.";

	$mgrlang['membership_f_allowedit']		= "אפשר עריכה";

	$mgrlang['membership_f_allowedit_d']	= "אפשר למשתמשים בתוכנית הרשמה זו לערוך את פרטי התמונות או המדיה שלהם מאוחר יותר. ואז בחר האם העריכות צריכות אישור ממנהל לפני שיוצגו באתר.";

	$mgrlang['membership_f_allowdel']		= "אפשר מחיקה";

	$mgrlang['membership_f_allowdel_d']		= "אפשר למשתמשים בתוכנית הרשמה זו למחוק את התמונות או המדיה שלהם. ואז בחר האם מנהל צריך לצפות במחיקה של תמונה או מדיה.";

	$mgrlang['membership_f_acfilesizes']	= "גדלי קבצים מקובלים";

	$mgrlang['membership_f_acfilesizes_d']	= "גדלי קבצים של תמונות ומדיה אחרת שמשתמשים יכולים להעלות.";

	$mgrlang['membership_f_min']			= "מינימום";

	$mgrlang['membership_f_mb']				= "mb";

	$mgrlang['membership_f_px']				= "px";

	$mgrlang['membership_f_max']			= "מקסימום";

	$mgrlang['membership_f_res']			= "רזולוציה מקובלת";

	$mgrlang['membership_f_res_d']			= "The resolution of photos that members can upload.";

	$mgrlang['membership_f_aft']			= "סוגי קבצים מקובלים";

	$mgrlang['membership_f_aft_d']			= "סוגי קבצים שמשתמשים בתוכנית הרשמה זו יכולים להעלות.";

	$mgrlang['membership_f_portfolio']		= "פורטפוליו";

	$mgrlang['membership_f_portfolio_d']	= "אפשר למשתמשים בתוכנית הרשמה זו ליצור פורטפוליו לתצוגה של העבודה שלהם.";

	$mgrlang['membership_f_allowsell']		= "אפשר מכירה";

	$mgrlang['membership_f_allowsell_d']	= "אפשר למשתמשים בתוכנית הרשמה זו למכור תמונות ומדיה אחרת.";

	$mgrlang['membership_f_commission']		= "רמת עמלה";

	$mgrlang['membership_f_commission_d']	= "רמת אחוזי עמלה שמשתמשים בתוכנית הרשמה זו ירוויחו.";

	$mgrlang['membership_f_commission_op1']	= "ללא";

	$mgrlang['membership_f_commission_op2']	= "מחיר אחיד";

	$mgrlang['membership_f_commission_op3']	= "אחוז";

	$mgrlang['membership_f_aprod']			= "מוצרים";

	$mgrlang['membership_f_aprod_d']		= "אפשר למשתמשים בתוכנית הרשמה זו לבחור פריטים למכירה.";

	$mgrlang['membership_f_sdf']			= "מכירת גירסא דיגיטלית מקורית";

	$mgrlang['membership_f_sdf_d']			= "אפשר למשתמשים בתוכנית הרשמה זו למכור את הקבצים הדיגיטליים המקוריים שלהם";

	$mgrlang['membership_f_aprints']		= "הדפסות";

	$mgrlang['membership_f_aprints_d']		= "אפשר למשתמשים בתוכנית הרשמה זו לבחור הדפסות למכירה.";

	$mgrlang['membership_f_asizes']			= "פרופילים דיגיטליים";

	$mgrlang['membership_f_asizes_d']		= "אפשר למשתמשים בתוכנית הרשמה זו לבחור מתוך פרופילים דיגיטליים בשביל מכירה.";

	$mgrlang['membership_f_collect']		= "יצירת אוסף";

	$mgrlang['membership_f_collect_d']		= "אפשר למשתמשים בתוכנית הרשמה זו ליצור אוספים של תמונות ומדיה אחרת שלהם עבור מכירה.";

	$mgrlang['membership_f_setupfee']		= "הגדרת מחיר";

	$mgrlang['membership_f_approval']		= "אישור";

	$mgrlang['membership_f_approval_d']		= "בחר איך תרצה לאשר תמונות, מדיה, הגדרות שלהם ובחירות מוצרים כאשר תורם מעלה משהו חדש. זה יוחל גם על שינויים שהם יעשו מאוחר יותר.";

	$mgrlang['membership_f_iis']			= "כלול בחיפוש";

	$mgrlang['membership_f_iis_d']			= "כלול כל מדיה שהועלתה על-ידי משתמשים בתוכנית הרשמה זו בחיפוש שהתבצע על-ידי מבקרים.";	

	$mgrlang['membership_f_pricing']		= "תמחור";

	$mgrlang['membership_f_pricing_d']		= "בחר מי קובע מחירים?";

	

	

	

	# TOOLS LINKS

	$mgrlang['tl_new_header']				= "הוסף קישור חדש";

	$mgrlang['tl_edit_header']				= "ערוך קישור";

	$mgrlang['tl_new_message']				= "בעזרת הטופס הבא תוכל להוסיף קישור חדש לתפריט Tools & Links.";

	$mgrlang['tl_edit_message']				= "ערוך קישור זה ולחץ של כפתור 'שמור שינויים'.";


	$mgrlang['tl_tab1']						= "פרטי קישור";

	$mgrlang['tl_f_name']					= "שם קישור";

	$mgrlang['tl_f_name_d']					= "שם הקישור שיוצג בתפריט.";

	$mgrlang['tl_f_url']					= "קישור";

	$mgrlang['tl_f_url_d']					= "אתר או דף שתרצה שהקישור יוביל אליו. תכלול http:// אם זה קישור פנימי.";

	$mgrlang['tl_f_owner']					= "כולם";

	$mgrlang['tl_f_owner_d']				= "הצג קישור זה עבור מנהלים. אם התיבה לא מסומנת, הקישור יוצג רק כשמחוברים לחשבון זה.";	

	$mgrlang['tl_f_target']					= "חלון יעד";

	$mgrlang['tl_f_target_d']				= "באיזה חלון תרצה שהקישור ייפתח.";

	$mgrlang['tl_newwindow']				= "חלון חדש";

	$mgrlang['tl_thiswindow']				= "חלון נוכחי";



	# MEMBERS

	$mgrlang['members_new_header']			= "הוסף משתמש חדש";

	$mgrlang['members_edit_header']			= "ערוך משתמש";

	$mgrlang['members_new_message']			= "בעזרת הטופס הבא תוכל להוסיף משתמש חדש.";

	$mgrlang['members_edit_message']		= "ערוך משתמש זה ולחץ על כפתור 'שמור שינויים'.";

	$mgrlang['mem_tab1']					= "פרופיל";

	$mgrlang['mem_tab2']					= "תוכנית הרשמה";

	$mgrlang['mem_tab3']					= "הזמנות";

	$mgrlang['mem_tab4']					= "תיבות אישיות";

	$mgrlang['mem_tab5']					= "דירוגים";

	$mgrlang['mem_tab6']					= "כרטיסים";

	$mgrlang['mem_tab7']					= "הצעות מחיר";

	$mgrlang['mem_tab8']					= "העלאות";

	$mgrlang['mem_tab9']					= "רישומי פעילות";

	$mgrlang['mem_tab10']					= "קבוצות";

	$mgrlang['mem_tab11']					= "מתקדם";

	$mgrlang['mem_tab12']					= "סטטיסטיקות";

	$mgrlang['mem_tab13']					= "הערות";

	$mgrlang['mem_tab14']					= "כתובת";

	$mgrlang['mem_tab15']					= "תויות";

	$mgrlang['mem_tab16']					= "ביוגרפיה";

	$mgrlang['mem_f_groups']				= "קבוצות משתמשים";

	$mgrlang['mem_f_groups_d']				= "בחר את הקבוצות שמשתמש זה שייך אליהן. משתמש יכול להיות שייך למספר בלתי מוגבל של קבוצות.";

	$mgrlang['mem_never']					= "מעולם לא";

	$mgrlang['mem_last_login']				= "התחבר לאחרונה";

	$mgrlang['mem_member_num']				= "מזהה משתמש";

	$mgrlang['mem_signup_date']				= "תאריך הצטרפות";

	$mgrlang['mem_unique_id']				= "מזהה ייחודי";

	$mgrlang['mem_f_signupdate']			= "תאריך/שעת הרשמה";

	$mgrlang['mem_f_signupdate_d']			= "התאריך והשעה בה נוצר חשבון המשתמש (GMT).";

	$mgrlang['mem_f_fname']					= "שם פרטי";

	$mgrlang['mem_f_fname_d']				= "שם פרטי של המשתמש";

	$mgrlang['mem_f_lname']					= "שם משפחה";

	$mgrlang['mem_name']					= "שם";

	$mgrlang['mem_f_lname_d']				= "שם משפחה של המשתמש";

	$mgrlang['mem_f_email']					= "אימייל";

	$mgrlang['mem_f_email_d']				= "כתובת אימייל של המשתמש";

	$mgrlang['mem_f_email_error']			= "כתובת מייל כבר בשימוש.";

	$mgrlang['mem_f_password']				= "סיסמא";

	$mgrlang['mem_f_password_d']			= "סיסמת המשתמש";

	$mgrlang['mem_f_company_name']			= "שם חברה";

	$mgrlang['mem_f_company_name_d']		= "שם החברה של המשתמש.";

	$mgrlang['mem_f_website']				= "אתר";

	$mgrlang['mem_f_website_d']				= "כתובת האתר של המשתמש.";

	$mgrlang['mem_f_address']				= "כתובת";

	$mgrlang['mem_f_address_d']				= "כתובת המגורים של המשתמש.";

	$mgrlang['mem_f_city']					= "עיר";

	$mgrlang['mem_f_city_d']				= "עיר של המשתמש.";

	$mgrlang['mem_f_state']					= "מדינה/פרובינציה";

	$mgrlang['mem_f_state_d']				= "מדינה/פרובינציה של המשתמש.";

	$mgrlang['mem_f_country']				= "מדינה";

	$mgrlang['mem_f_country_d']				= "המדינה של המשתמש";

	$mgrlang['mem_f_zip']					= "מיקוד";

	$mgrlang['mem_f_zip_d']					= "מיקוד המשתמש.";

	$mgrlang['mem_f_status']				= "סטטוס";

	$mgrlang['mem_f_status_d']				= "הגדר משתמש כפעיל. משתמשים שלא יוגדרו כפעילים לא יוכלו להתחבר.";

	$mgrlang['mem_f_phone']					= "מספר טלפון";

	$mgrlang['mem_f_phone_d']				= "מספר טלפון של המשתמש.";

	$mgrlang['mem_f_notes']					= "הערות";

	$mgrlang['mem_f_notes_d']				= "הערות על המשתמש. יוצג רק בצד הניהול.";

	$mgrlang['mem_f_membership']			= "תוכנית הרשמה";

	$mgrlang['mem_f_membership_d']			= "תוכנית ההרשמה אליה רשום משתמש זה. משתמש יכול להיות רשום לתוכנית אחת בלבד בזמן נתון.";

	$mgrlang['mem_f_email_pass']			= "סיסמת אימייל";

	$mgrlang['mem_block_list']				= "הוסף לרשימת חסימה";

	$mgrlang['mem_f_visit']					= "בקר באתר";

	$mgrlang['mem_country_warn']			= "במדינה זו אין מדינות/פרובינציות. תוכל להוסיף כאלו ב: ";

	$mgrlang['mem_choose_country']			= "אנא בחר מדינה תחילה!";

	$mgrlang['mem_f_signup_ip']				= "כתובת IP בזמן ההרשמה";

	$mgrlang['mem_f_signup_ip_d']			= "כתובת IP שנשמרה כשהמשתמש נרשם.";

	$mgrlang['mem_f_login_ip']				= "כתובת IP בהתחברות האחרונה";

	$mgrlang['mem_f_login_ip_d']			= "כתובת IP שנשמרה כשהמשתמש התחבר בפעם האחרונה.";

	$mgrlang['mem_f_referrer']				= "המקור המפנה";

	$mgrlang['mem_f_referrer_d']			= "האתר שהפנה כפי שנרשם בזמן שהמשתמש התחבר.";

	$mgrlang['mem_f_avatar']				= "דמות";

	$mgrlang['mem_f_avatar_d']				= "דמות המשתמש או לוגו.";

	$mgrlang['mem_del_avatar']				= "האם אתה בטוח כי ברצונך למחוק דמות זו?";

	$mgrlang['mem_unknown']					= "אין/לא ידוע";

	$mgrlang['mem_f_paypal']				= "PayPal כתובת אימייל";

	$mgrlang['mem_f_paypal_d']				= "כתובת אימייל PayPal של משתמשים. משמש כדי לשלוח תשלומי עמלה.";

	$mgrlang['mem_f_compref']				= "שיטת תשלום עמלה";

	$mgrlang['mem_f_compref_d']				= "הגדרות משתמשים איך הם ירצו לשלם תשלומי עמלה.";

	$mgrlang['mem_f_paypal_email']			= "כתובת אימייל PayPal";

	$mgrlang['mem_mes_01']					= "כרגע לא מופעלות אפשרויות תשלום עמלה. להפעיל כאלו לך ל-";

	$mgrlang['mem_mes_02']					= "משתמש זה לא דירג אף מדיה.";

	$mgrlang['mem_mes_03']					= "משתמש זה לא תייג אף מדיה.";

	$mgrlang['mem_mes_04']					= "משתמש זה לא כתב הערות על אף מדיה.";

	$mgrlang['mem_mes_05']					= "אין כרטיסי תמיכה בחשבון משתמש זה.";

	$mgrlang['mem_orig']					= "מקורי";

	$mgrlang['mem_mes_06']					= "לא נעשו הורדות בחשבון משתמש זה.";

	$mgrlang['mem_mes_07']					= "אין מינויים בחשבון משתמש זה.";

	$mgrlang['mem_mes_08']					= "לא היו הזמנות בחשבון משתמש זה.";

	$mgrlang['mem_mes_09']					= "לא נוצרו תיבות אישיות בחשבון זה.";

	$mgrlang['mem_new_ticket']				= "כרטיס חדש";

	$mgrlang['mem_quotes']					= "הצעות מחיר";

	$mgrlang['mem_comlevel']				= "רמת עמלה";

	$mgrlang['mem_comlevelms']				= "השתמש ברמת עמלה של תוכנית הרשמה";

	$mgrlang['mem_comlevelcus']				= "השתמש ברמת עמלה מותאמת";

	$mgrlang['mem_f_billlater']				= "אפשר 'חייב אותי מאוחר יותר'";

	$mgrlang['mem_f_billlater_d']			= "אפשר למשתמש לבחור 'חייב אותי מאוחר יותר' בזמן התשלום.";

	$mgrlang['mem_f_crdits_d']				= "מספר הקרדיטים שיש למשתמש זה.";

	$mgrlang['mem_add_sub']					= "הוסף מינוי";

	$mgrlang['mem_f_bio_updated']			= "ביוגרפיה עודכנה";

	$mgrlang['mem_f_bio_updated_d']			= "התאריך בה עודכנה הביוגרפיה על-ידי המשתמש.";	

	$mgrlang['mem_f_bio']					= "ביוגרפיה";

	$mgrlang['mem_f_bio_d']					= "ביוגרפיה הוכנסה על-ידי משתמש זה.";

	$mgrlang['mem_f_status']				= "סטטוס";

	$mgrlang['mem_f_status_d']				= "סמן תיבה זו כדי לאשר את הביוגרפיה לתצוגה באתר.";

	$mgrlang['mem_new_bill']				= "חשבון חדש";

	$mgrlang['mem_show_only']				= "הצג רק משתמשים של תוכניות הרשמה אלו";

	$mgrlang['mem_edit_ms']					= "ערוך תוכניות הרשמה";

	$mgrlang['mem_table_headers']			= "הצג את כותרות הטבלה האלו";

	$mgrlang['mem_f_ftm']					= "קדם משתמש זה";

	$mgrlang['mem_f_ftm_d']					= "אפשר למשתמש זה להיות מקודם בדף הבית ובדף התורמים.";

	

	# DOWNLOAD HISTORY

	$mgrlang['mem_download_unknown']		= "לא ידוע";

	$mgrlang['mem_download_free']			= "הורדה חופשית";

	$mgrlang['mem_download_sub']			= "מינוי";

	$mgrlang['mem_download_order']			= "הזמנה";

	$mgrlang['mem_download_credits']		= "קרדיטים";

	$mgrlang['mem_download_prev']			= "הורד בעבר";

	

	

	# ORDERS

	$mgrlang['order_tab1']					= "פרטי הזמנה";

	$mgrlang['order_tab2']					= "פריטים";

	$mgrlang['order_tab3']					= "פרטי חשבונית";

	$mgrlang['order_tab4']					= "העברות";

	$mgrlang['order_t_ordernum']			= "מספר הזמנה";

	$mgrlang['order_t_payment']				= "תשלום";

	$mgrlang['order_t_shipping']			= "סטטוס משלוח";

	$mgrlang['order_t_status']				= "סטטוס הזמנה";

	$mgrlang['order_t_total']				= "סה\"כ";

	$mgrlang['order_t_invoicenum']			= "מספר חשבונית";

	$mgrlang['order_new_header']			= "הוסף הזמנה חדשה";

	$mgrlang['order_edit_header']			= "צפה בהזמנה";

	$mgrlang['order_new_message']			= "בעזרת הטופס הבא תוכל להוסיף הזמנה חדשה.";

	$mgrlang['order_edit_message']			= "צפה בפרטי הזמנה זו וערוך אם צריך.";

	$mgrlang['order_f_invoice']				= "מספר חשבונית";

	$mgrlang['order_f_invoice_d']			= "מספר החשבונית שמצורפת להזמנה זו.";

	$mgrlang['order_f_ordernum']			= "מספר הזמנה";

	$mgrlang['order_f_ordernum_d']			= "מספר הזמנה ייחודי עבור הזמנה זו.";

	$mgrlang['order_reset_dls']				= "אפס הורדות";

	$mgrlang['order_reset_exp']				= "אפס תאריך תפוגה";

	$mgrlang['order_del_oi']				= "מחק פריט הזמנה";

	$mgrlang['order_update_ss']				= "עדכן סטטוס משלוח פריט";

	$mgrlang['order_track_num']				= "מספר מעקב";

	$mgrlang['order_dis_statuses']			= "הצג הזמנות עם סטטוסים אלו";

	$mgrlang['order_dis_data']				= "הצג מידע זה בטבלה";

	$mgrlang['order_f_status']				= "סטטוס הזמנה";

	$mgrlang['order_f_status_d']			= "סטטוס נוכחי של הזמנה זו.";

	$mgrlang['order_f_datet']				= "תאריך/שעה של ההזמנה";

	$mgrlang['order_f_datet_d']				= "התאריך והשעה בהם נרשמה ההזמנה (GMT).";

	$mgrlang['order_f_customer']			= "לקוח";

	$mgrlang['order_f_adminnotes']			= "הערות מנהל";

	$mgrlang['order_f_adminnotes_d']		= "הערות פנימיות שתרצה לכתוב על הזמנה זו.";	

	$mgrlang['order_f_memnotes']			= "הערות למשתמש";

	$mgrlang['order_f_memnotes_d']			= "הערות אלו יופיעו בדף פרטי הזמנה עבור הזמנה זו.";	

	$mgrlang['order_f_ogroups']				= "קבוצות הזמנות";

	$mgrlang['order_f_paystat']				= "סטטוס תשלום";

	$mgrlang['order_f_paystat_d']			= "סטטוס התשלום להזמנה זו. מומלץ שלא תשלח הזמנות בלי שססטוס התשלום הוא 'מאושר'.";	

	$mgrlang['order_f_paydt']				= "תאריך/שעה של התשלום";

	$mgrlang['order_f_paydt_d']				= "התאריך והשעה שחשבונית זו שולמה (GMT).";

	$mgrlang['order_f_coupuse']				= "שימוש בקופונים";

	$mgrlang['order_f_coupuse_d']			= "מספרים מזהים של קופונים שנעשה בהם שימוש ברכישה זו.";

	$mgrlang['order_f_curuse']				= "מטבע שנעשה בו שימוש";

	$mgrlang['order_f_curuse_d']			= "מספר מזהה של המטבע שנבחר על-ידי המשתמש בעת ביצוע הרכישה.";

	$mgrlang['order_f_pubord']				= "לינק ציבורי לדף פרטי הזמנה";

	$mgrlang['order_f_pubilin']				= "לינק ציבורי לחשבונית";

	$mgrlang['order_f_oshipstat']			= "מצב משלוח ההזמנה";

	$mgrlang['order_f_oshipstat_d']			= "סטטוס כללי של משלוח כל ההזמנה.";

	$mgrlang['order_f_shipto']				= "שלח אל";

	$mgrlang['order_f_shipto_d']			= "הכתובת שסופקה על-ידי הלקוח עבור משלוח.";

	$mgrlang['order_f_shipmeth']			= "שיטת משלוח";

	$mgrlang['order_f_shipmeth_d']			= "שיטת המשלוח שנבחרה על-ידי הלקוח.";

	$mgrlang['order_f_pback']				= "משתני תשובה חוזרת";

	$mgrlang['order_f_pback_d']				= "משתנים שמתקבלים כתשובה מאמצעי התשלום.";

	

	# PRODUCTS

	$mgrlang['products_new_header']			= "הוסף מוצרים חדשים";

	$mgrlang['products_edit_header']		= "ערוך מוצרים";

	$mgrlang['products_new_message']		= "בעזרת הטופס הבא תוכל להוסיף מוצרים חדשים.";

	$mgrlang['products_edit_message']		= "ערוך מוצרים אלו ולחץ על כפתור 'שמור שינויים'.";

	$mgrlang['products_f_name']				= "שם מוצר";

	$mgrlang['products_f_name_d']			= "הכנס את שם מוצרים אלו.";

	$mgrlang['products_f_price']			= "מחיר מוצר";

	$mgrlang['products_f_price_d']			= "מחיר המוצר הזה.";

	$mgrlang['products_f_my_cost']			= "העלות שלי";

	$mgrlang['products_f_my_cost_d']		= "העלות שלי עבור מוצר זה. משמש לחישוב רווח.";

	$mgrlang['products_f_credits']			= "קרדיטים";

	$mgrlang['products_f_credits_d']		= "עלות קרדיטים לרכישת מוצר זה.";

	$mgrlang['products_f_perm']				= "הרשאות";

	$mgrlang['products_f_perm_d']			= "בחר משתמשים, קבוצות משתמשים או תוכניות הרשמה שיכולים לראות פריט זה.";

	$mgrlang['products_f_taxable']			= "חיוב במס";

	$mgrlang['products_f_taxable_d']		= "האם צריך לחייב פריט זה במס.";	

	$mgrlang['products_f_multiple']			= "כפולה";

	$mgrlang['products_f_multiple_d']		= "אפשר למשתמשים לרכוש יותר מפריט אחד.";

	$mgrlang['products_f_discount']			= "הנחה";

	$mgrlang['products_f_discount_d']		= "מחיר עבור פריט אחרי הראשון. השאר ריק כדי להשאיר את אותו מחיר כמו הפריט הראשון.";

	$mgrlang['products_f_groups']			= "קבוצות מוצרים";

	$mgrlang['products_f_groups_d']			= "בחר את קבוצות המוצרים שזה שייך אליהן.";

	$mgrlang['products_f_top']				= "סוג מוצר";

	$mgrlang['products_f_groups_d']			= "בחר האם זהו מוצר מבוסס מדיה או מוצר עצמאי.";

	$mgrlang['products_f_medbased']			= "מוצר מבוסס מדיה";

	$mgrlang['products_f_standalone']		= "מוצר עצמאי";

	$mgrlang['products_f_adv_d']			= "פרסם מוצר זה בדף המוצרים המקודמים.";

	$mgrlang['products_f_attach']			= "מצורף";

	$mgrlang['products_f_attach_d']			= "היכן להציע מוצר זה למכירה. בחירה זו עוקפת בחירות מדיה אינדיבידואליות אחרות .";

	

	# COLLECTIONS

	$mgrlang['collections_new_header']		= "הוסף אוסף חדש";

	$mgrlang['collections_edit_header']		= "ערוך אוסף";

	$mgrlang['collections_new_message']		= "בעזרת הטופס הבא תוכל להוסיף אוסף חדש.";

	$mgrlang['collections_edit_message']	= "ערוך אוסף זה ולחץ על כפתור 'שמור שינויים'.";

	$mgrlang['collections_f_name']			= "שם אוסף";

	$mgrlang['collections_f_name_d']		= "הכנס שם אוסף זה.";

	$mgrlang['collections_f_price']			= "מחיר אוסף";

	$mgrlang['collections_f_price_d']		= "מחירו של אוסף זה.";

	$mgrlang['collections_f_credits']		= "קרדיטים";

	$mgrlang['collections_f_credits_d']		= "עלות הקרדיטים לרכישת אוסף זה.";

	$mgrlang['collections_f_weight']		= "משקל פריט";

	$mgrlang['collections_f_weight_d']		= "משקל פריט זה. ישמש לחישוב עלויות משלוח, במידת הצורך.";

	$mgrlang['collections_f_perm']			= "הרשאות";

	$mgrlang['collections_f_perm_d']		= "בחר את המשתמשים, קבוצות משתמשים או תוכניות הרשמה שיוכלו לראות פריט זה.";

	$mgrlang['collections_f_taxable']		= "חיוב במס";

	$mgrlang['collections_f_taxable_d']		= "האם לחייב במס פריט זה.";	

	$mgrlang['collections_f_groups']		= "קבוצות אוספים";

	$mgrlang['collections_f_groups_d']		= "בחר את קבוצות האוספים שזה שייך אליהן.";

	$mgrlang['collections_f_active_d']		= "הגדר אוסף זה כפעיל. אוספים לא פעילים לא יוצגו באתר.";

	$mgrlang['collections_f_toc']			= "סוג האוסף";

	$mgrlang['collections_f_toc_d']			= "צור אוסף זה מגלריות קיימות או הגדר זאת כך שמדיה מסויימת תוכל להתווסף לאוסף מאוחר יותר.";

	$mgrlang['collections_cfg']				= "צור אוסף מגלריות";

	$mgrlang['collections_cfim']			= "צור אוסף ממדיה אינדיבידואלית";

	$mgrlang['collections_media_added']		= "ניתן להוסיף או להסיר מדיה מאוסף זה ב-  ספרייה > מדיה או כאשר מדיה מיובאת אל הספריה.";

	$mgrlang['collections_choose_gal']		= "בחר אלו גלריות לכלול באוסף זה. כל מדיה בתוך גלריות אלו יכללו באוסף בלי קשר להרשאות או הגדרות הגלריות. כמו כן כל מדיה עתידית שתתווסף לגלריות או האוספים האלו תהיה זמינה עבור מי שרכש אוסף זה.";

	$mgrlang['collections_adv_d']			= "פרסם אוסף זה בדף האוספים המקודמים.";

	

	

	# SUBSCRIPTIONS

	$mgrlang['subscription_new_header']		= "הוסף מינוי";

	$mgrlang['subscription_edit_header']	= "ערוך מינוי";

	$mgrlang['subscription_new_message']	= "בעזרת האוסף הבא תוכל להוסיף מינוי חדש.";

	$mgrlang['subscription_edit_message']	= "ערוך מינוי זה ולחץ על כפתור 'שמור שינויים'.";

	$mgrlang['subscription_tab1']			= "פרטים";

	$mgrlang['subscription_tab2']			= "קבוצות";

	$mgrlang['subscription_f_name']			= "שם מינוי";

	$mgrlang['subscription_f_name_d']		= "שם המינוי.";	

	$mgrlang['subscription_f_groups']		= "קבוצות מינויים";

	$mgrlang['subscription_f_groups_d']		= "בחר את הקבוצות שמינוי זה ישתייך אליהן.";	

	$mgrlang['subscription_f_durvalue']		= "תקופה";

	$mgrlang['subscription_f_durvalue_d']	= "כמות הזמן שמינוי יהיה תקף לרכישה אחת.";	

	$mgrlang['subscription_f_price']		= "מחיר";

	$mgrlang['subscription_f_price_d']		= "מחיר מינוי זה.";	

	$mgrlang['subscription_f_credits']		= "קרדיטים";

	$mgrlang['subscription_f_credits_d']	= "מחיר מינוי זה.";	

	$mgrlang['subscription_f_dpd']			= "הורדות ליום";

	$mgrlang['subscription_f_dpd_d']		= "מספר ההורדות שמשתמש יכול להוריד ביום עם מינוי זה. השאר ריק בשביל 'ללא הגבלה'.";

	$mgrlang['subscription_adv_fp_d']		= "פרסם מינוי זה בדף המינויים המקודמים.";

	

	$mgrlang['subscription_f_downi']		= "הורד פריטים";

	$mgrlang['subscription_f_downi_d']		= "פריטים שניתן להוריד במינוי זה.";

	

	# CREDITS

	$mgrlang['credits_new_header']			= "הוסף חבילת קרדיטים";

	$mgrlang['credits_edit_header']			= "ערוך חבילת קרדיטים";

	$mgrlang['credits_new_message']			= "בעזרת הטופס הבא תוכל להוסיף חבילת קרדיטים חדשה.";

	$mgrlang['credits_edit_message']		= "ערוך חבילת קרדיטים זו ולחץ על כפתור 'שמור שינויים'.";

	$mgrlang['credits_tab1']				= "פרטים";

	$mgrlang['credits_tab2']				= "קבוצות";

	$mgrlang['credits_f_name']				= "שם חבילת קרדיטים";

	$mgrlang['credits_f_name_d']			= "שם חבילת הקרדיטים.";	

	$mgrlang['credits_f_groups']			= "קבוצות של חבילות קרדיטים";

	$mgrlang['credits_f_groups_d']			= "בחר את הקבוצות שחבילת קרדיטים זו שייכת אליהן.";

	$mgrlang['credits_f_amount_d']			= "כמות הקרדיטים שיכללו בחבילה זו.";	

	$mgrlang['credits_f_price']				= "מחיר";

	$mgrlang['credits_f_price_d']			= "מחיר קרדיטים אלו.";

	$mgrlang['credits_f_advertise_d']			= "פרסם חבילת קרדיטים זו בדף הקרדיטים המקודמים.";

	

	# PROMOTIONS

	$mgrlang['promotions_new_header']		= "הוסף מקדם מכירות או קופון";

	$mgrlang['promotions_edit_header']		= "ערוך מקדם מכירות או קופון";

	$mgrlang['promotions_new_message']		= "בעזרת הטופס הבא תוכל ליצור מקדם מכירות/קופון חדש.";

	$mgrlang['promotions_edit_message']		= "ערוך את מקדם המכירות או הקופון ולחץ על הכפתור 'שמור שינויים'.";

	$mgrlang['promotions_tab1']				= "פרטים";

	$mgrlang['promotions_tab2']				= "קבוצות";	

	$mgrlang['promotions_f_name']			= "שם מקדם מכירות/קופון";

	$mgrlang['promotions_f_name_d']			= "השם של מקדם המכירות או הקופון.";

	$mgrlang['promotions_f_code']			= "קוד מקדם המכירות/קופון";

	$mgrlang['promotions_f_code_d']			= "הכנס קוד ייחודי עבור קופון או מקדם מכירות זה. זהו הקוד שהמשתמש יכניס בזמן התשלום. בלי רווחים. בזמן השמירה כל האותיות יהפכו לגדולות.";

	$mgrlang['promotions_f_groups']			= "קבוצות מקדמי מכירות/קופונים";

	$mgrlang['promotions_f_groups_d']		= "בחר את הקבוצות שפריט זה יהיה שייך אליהם.";

	$mgrlang['promotions_lowestprice']		= "הפריטים עם המחירים הנמוכים ביותר בעגלת הקניות של הלקוחות יהפכו לפריטים חינמיים.";

	$mgrlang['promotions_buy']				= "קנה";

	$mgrlang['promotions_get']				= "קבל";

	$mgrlang['promotions_free']				= "חינם";	

	$mgrlang['promotions_f_minpur']			= "רכישה מינימלית";

	$mgrlang['promotions_f_minpur_d']		= "קבע כמות רכישה מינימלית לפני שניתן יהיה להשתמש בקופון זה. לא כולל מס ומשלוח. השאר ריק בשביל בלי כמות מינימלית.";	

	$mgrlang['promotions_f_quanre']			= "כמות שנותרה";

	$mgrlang['promotions_f_oneuse']			= "שימוש אחד עבור כל משתמש";

	$mgrlang['promotions_f_oneuse_d']		= "זה יגביל משתמש מלהשתמש בקופון רק פעם אחת. אורחים לא יוכלו להשתמש בקופון בלי ליצור חשבון או להתחבר.";

	$mgrlang['promotions_f_autoap']			= "החל אוטומטית";

	$mgrlang['promotions_f_autoap_d']		= "הנחה זו תחול אוטומטית בלי להוסיף אותה בעזרת קישור או הקלדת קוד.";

	$mgrlang['promotions_f_type']			= "סוג פרומו/קופון";

	$mgrlang['promotions_f_type_d']			= "בחר את סוג מקדם המכירות/קופון שתרצה שזה יהיה.";	

	$mgrlang['promotions_type_op1']			= "הורדת אחוזים";

	$mgrlang['promotions_type_op2']			= "הורדת כמות דולרית";

	$mgrlang['promotions_type_op3']			= "ללא מס";

	$mgrlang['promotions_type_op4']			= "משלוח חינם";

	$mgrlang['promotions_type_op5']			= "הנחת כמות";

	$mgrlang['promotions_digitalfile']		= "קבצים דיגיטליים";

	$mgrlang['promotions_directlink']		= "קישור ישיר";

	$mgrlang['promotions_directlink_d']		= "קישור לקופון באתר הציבורי.";

	$mgrlang['promotions_advoncart']		= "פרסם בדף העגלה";

	$mgrlang['promotions_advoncart_d']		= "הצג מקדם מכירות/קופון זה בדף העגלה.";	

	$mgrlang['promotions_advonpp']			= "פרסם בדף פרומו";

	$mgrlang['promotions_advonpp_d']		= "הצג מקדם מכירות/קופון זה בדף קידום המכירות.";

	

	

	# RSS FEEDS

	$mgrlang['rss_type1']					= "של החדש ביותר";

	$mgrlang['rss_type2']					= "של הפופולרי ביותר";

	$mgrlang['rss_type3']					= "אקראי";

	$mgrlang['rss_type4']					= "פריטי חדשות אתר";

	$mgrlang['rss_type1b']					= "מדיה החדשה ביותר";

	$mgrlang['rss_type2b']					= "מדיה הפופולרית ביותר";

	$mgrlang['rss_type3b']					= "מדיה אקראית";

	$mgrlang['rss_type4b']					= "חדשות אתר";	

	$mgrlang['rss_mes_01']					= "מדיה מ- ";

	$mgrlang['rss_mes_02']					= "פידים של RSS נמחקו בהצלחה.";

	$mgrlang['rss_mes_03']					= "פיד(ים) של RSS נמחק(ו) בהצלחה.";

	$mgrlang['rss_mes_04']					= "לא נבחרו פידי RSS עבור מחיקה.";

	$mgrlang['rss_f_photos']				= "מספר פריטים";

	$mgrlang['rss_f_photos_d']				= "מספר הפריטים בתצוגת פיד RSS.";

	$mgrlang['rss_f_type']					= "סוג פיד RSS";

	$mgrlang['rss_f_type_d']				= "סוג הפיד לתצוגה.";	

	$mgrlang['rss_new_header']				= "הוסף פיד RSS חדש";

	$mgrlang['rss_edit_header']				= "ערוך פיד RSS";

	$mgrlang['rss_new_message']				= "בעזרת הטופס הבא תוכל להוסיף פיד RSS חדש לאתר.";

	$mgrlang['rss_edit_message']			= "ערוך את פיד RSS זה ולחץ על כפתור 'שמור שינויים'.";

	$mgrlang['rss_tab1']					= "פרטי הפיד";

	$mgrlang['rss_f_category']				= "גלריה";

	$mgrlang['rss_f_category_d']			= "גלריה להציג ממנה מדיה.";

	$mgrlang['rss_mes_05']					= "נוסף ה- RSS Feed שלך.";

	$mgrlang['rss_mes_06']					= "השינויים נשמרו.";

	$mgrlang['rss_views']					= "צפיות";

	$mgrlang['rss_reset']					= "אפס";

	$mgrlang['rss_allgal']					= "כל הגלריות הציבוריות";	

	

	# SERVERS

	$mgrlang['servers_mes_02']				= "פרופיל(י) שרת נמחקו(ו) בהצלחה.";

	$mgrlang['servers_mes_03']				= "לא נבחרו פרופילי שרת למחיקה.";

	$mgrlang['servers_new_header']			= "הוסף פרופיל סוג קובץ חדש";

	$mgrlang['servers_edit_header']			= "ערוך פרופיל שרת";

	$mgrlang['servers_new_message']			= "בעזרת הטופס הבא תוכל להוסיף פרופיל שרת חדש לאתר.";

	$mgrlang['servers_edit_message']		= "ערוך פרופיל שרת זה ולחץ על כפתור 'שמור שינויים'.";

	$mgrlang['servers_tab1']				= "פרטי שרת";

	$mgrlang['servers_f_name']				= "שם שרת";

	$mgrlang['servers_f_name_d']			= "שם שיתייחס לשרת. לדוגמא Server1 או MyServer.com";

	$mgrlang['servers_f_url']				= "כתובת URL מלאה";

	$mgrlang['servers_f_url_d']				= "כתובת URL לשרת. לדוגמא http://www.myserver.com או http://www.myserver.com/path. No trailing slash ( / ).";

	$mgrlang['servers_b_test']				= "בדיקה";

	$mgrlang['servers_f_pass']				= "סיסמא";

	$mgrlang['servers_f_pass_d']			= "סיסמא עבור השרת המרוחק לוידוא. 6-10 תווים, בלי רווחים.";

	$mgrlang['servers_mes_04']				= "אנא הכנס כתובת URL תקינה";

	$mgrlang['servers_mes_05']				= "נוסף פרופיל השרת החדש שלך.";

	$mgrlang['servers_mes_06']				= "השינויים נשמרו.";

	

	# DIGITAL PROFILES

	$mgrlang['lookfeel_f_mpp']				= "מדיה לדף";

	$mgrlang['lookfeel_f_mpp_d']			= "מספר קבצי המדיה להציג בדף.";

	$mgrlang['dsp_f_attach_d']				= "איזו מדיה תוכל להירשם למכירה עם גודל דיגיטלי זה.";

	$mgrlang['dsp_f_profiletype']			= "סוג פרופיל";

	$mgrlang['dsp_f_profiletype_d']			= "בחר האם פרופיל גודל דיגיטלי זה הוא עבור וידאו, תמונה או משהו אחר.";

	$mgrlang['dsp_f_comtype']				= "סוג עמלה";

	$mgrlang['dsp_f_comtype_d']				= "בחר האם לגבות עמלה עבור מוצר זה באחוזים או בערך דולרי.";

	$mgrlang['dsp_new_header']				= "הוסף פרופיל דיגיטלי חדש";

	$mgrlang['dsp_edit_header']				= "ערוך פרופיל דיגיטלי";

	$mgrlang['dsp_new_message']				= "בעזרת הטופס הבא ניתן להוסיף פרופיל דיגיטלי חדש.";

	$mgrlang['dsp_edit_message']			= "ערוך תוכנית זו לחץ על כפתור 'שמור שינויים'.";

	$mgrlang['dsp_f_name']					= "שם פרופיל דיגיטלי";

	$mgrlang['dsp_f_name_d']				= "הכנס את שם פרופיל דיגיטלי זה. לדוגמא 'Large' או 'Web Version'.";

	$mgrlang['dsp_f_code']					= "קוד פריט";

	$mgrlang['dsp_f_code_d']				= "קוד פריט לגודל זה.";

	$mgrlang['dsp_f_price']					= "מחיר";

	$mgrlang['dsp_f_price_d']				= "מחיר פריט זה. זהו מחיר התחלתי אם זה Rights Managed.";

	$mgrlang['dsp_f_credits']				= "קרדיטים";

	$mgrlang['dsp_f_credits_d']				= "מספר הקרדיטים שנצרכים כדי לרכוש גודל זה.";

	$mgrlang['dsp_f_license']				= "רשיון";

	$mgrlang['dsp_f_license_d']				= "סוג רשיון לפרופיל זה.";

	$mgrlang['dsp_f_quantity']				= "כמות זמינה";

	$mgrlang['dsp_f_comval']				= "ערך עמלה";

	$mgrlang['dsp_f_comval_d']				= "ערך דולרי לעמלה עבור פריט זה.";

	$mgrlang['dsp_f_comlevel']				= "רמת עמלה";

	$mgrlang['dsp_f_comlevel_d']			= "אחוז מסה\"כ ההדפסה שהעמלה שלה תשולם ברמת עמלת התורמים.";

	$mgrlang['dsp_f_mmprice']				= "מחיר מינימום/מקסימום";

	$mgrlang['dsp_f_mmprice_d']				= "מחיר מינימלי שתורם יכול להרוויח על פריט זה. השאר ריק או הכנס 0 בשביל כלום.";

	$mgrlang['dsp_f_mmcredits']				= "מינימום/מקסימום קרדיטים";

	$mgrlang['dsp_f_mmcredits_d']			= "מספר מינימלי של קרדיטים שתורם יכול לקבל על פריט זה. השאר ריק או הכנס 0 בשביל כלום.";	

	$mgrlang['dsp_f_force_dis']				= "הכרח תצוגה";

	$mgrlang['dsp_f_force_dis_d']			= "הצג גירסא דיגיטלית זו להורדה, אפילו אם היא גדולה מהמקור.";	

	$mgrlang['dsp_f_cal_rs']				= "חשב גדלים אמיתיים";

	$mgrlang['dsp_f_cal_rs_d']				= "חשב את הערך המשתנה האמיתי, בזמן שינוי מהגודל המקורי לגודל של הפרופיל.";

	$mgrlang['dsp_f_width']					= "רוחב";

	$mgrlang['dsp_f_height']				= "גובה";

	$mgrlang['dsp_f_format']				= "פורמט";

	$mgrlang['dsp_f_hd']					= "HD";

	$mgrlang['dsp_f_running_time']			= "זמן ריצה";

	$mgrlang['dsp_f_fps']					= "FPS";

	$mgrlang['dsp_f_quantity_d']			= "כמות ברירת מחדל של גודל זה שזמין. הכנס 9999 בשביל ללא הגבלה. מאוחר יותר תוכל להגדיר זאת עבור כל קובץ מדיה.";

	$mgrlang['dsp_f_taxable']				= "חייב במס";

	$mgrlang['dsp_f_taxable_d']				= "גבה מס על פריט זה.";

	$mgrlang['dsp_f_groups']				= "קבוצות פרופילים דיגיטליים";

	$mgrlang['dsp_f_groups_d']				= "בחר את קבוצות הפרופילים הדיגיטליים שזה שייך אליהן.";

	$mgrlang['dsp_op_rf']					= "חופשי מתמלוגים";

	$mgrlang['dsp_op_cu']					= "צור קשר";

	$mgrlang['dsp_op_fr']					= "הורדה חופשית";

	$mgrlang['dsp_op_rm']					= "Rights Managed";

	$mgrlang['dsp_unlimited']				= "לא מוגבל";

	$mgrlang['dsp_amount']					= "הגדר כמות";

	

	# PRINTS

	$mgrlang['prints_new_header']			= "הוסף הדפסות חדשות";

	$mgrlang['prints_edit_header']			= "ערוך הדפסות";

	$mgrlang['prints_new_message']			= "בעזרת הטופס הבא תוכל להוסיף הדפסות חדשות.";

	$mgrlang['prints_edit_message']			= "ערוך הדפסה זו לחץ על כפתור 'שמור שינויים'.";

	$mgrlang['prints_f_name']				= "שם הדפסה";

	$mgrlang['prints_f_name_d']				= "הכנס את שם הדפסה זו.";

	$mgrlang['prints_f_price']				= "מחיר הדפסה";

	$mgrlang['prints_f_price_d']			= "מחיר הדפסה זה.";

	$mgrlang['prints_f_my_cost']			= "העלות שלי";

	$mgrlang['prints_f_my_cost_d']			= "העלות שלי לפריט זה. משמש כדי לחשב רווח.";

	$mgrlang['prints_f_credits']			= "קרדיטים";

	$mgrlang['prints_f_credits_d']			= "עלות קרדיטים כדי לרכוש הדפסה זו.";

	$mgrlang['prints_f_perm']				= "הרשאות";

	$mgrlang['prints_f_perm_d']				= "בחר משתמשים, קבוצות משתמשים או תוכניות הרשמה שיכולים לראות פריט זה.";

	$mgrlang['prints_f_taxable']			= "חייב במס";

	$mgrlang['prints_f_taxable_d']			= "האם לחייב במס פריט זה.";	

	$mgrlang['prints_f_multiple']			= "כפולה";

	$mgrlang['prints_f_multiple_d']			= "אפשר ללקוחות לרכוש יותר מפריט אחד.";

	$mgrlang['prints_f_groups']				= "קבוצות הדפסות";

	$mgrlang['prints_f_groups_d']			= "בחר את קבוצות ההדפסה שזה שייך אליהן.";

	$mgrlang['prints_mes_02']				= "חייבת להיות לפחות אפשרות אחת בקבוצה זו.";

	$mgrlang['prints_mes_04']				= "חייבים להיות שמות לכל האפשרויות.";

	$mgrlang['prints_b_add']				= "הוסף פריט";

	$mgrlang['prints_unlimited']			= "לא מוגבל";

	$mgrlang['prints_amount']				= "הגדר כמות";

	$mgrlang['prints_f_desc_d']				= "תיאור לפריט הדפסה זה.";

	$mgrlang['prints_mes_05']				= "תקף רק אם תוכנית ההרשמה של תורם זה מוגדרת כ- 'תורם קובע מחירים'.";

	$mgrlang['prints_f_attach_d']			= "היכן להציע הדפסה זו למכירה. בחירה זו עוקפת בחירות שנעשו בפריטי מדיה אינדיבידואליים.";

	$mgrlang['prints_f_adv_d']				= "פרסם פריט זה בדף ההדפסות המקודמות.";

	

	# PACKAGES

	$mgrlang['packages_tab1']				= "פרטים";

	$mgrlang['packages_tab2']				= "הדפסות";

	$mgrlang['packages_tab3']				= "מוצרים";

	$mgrlang['packages_tab5']				= "קבוצות";

	$mgrlang['packages_new_header']			= "הוסף חבילה חדשה";

	$mgrlang['packages_edit_header']		= "ערוך חבילה";

	$mgrlang['packages_new_message']		= "בעזרת הטופס הבא ניתן להוסיף חבילה חדשה.";

	$mgrlang['packages_edit_message']		= "ערוך חבילה ולחץ על כפתור 'שמור שינויים'.";

	$mgrlang['packages_f_name']				= "שם חבילה";

	$mgrlang['packages_f_name_d']			= "הכנס שם חבילה זו.";

	$mgrlang['packages_f_prints']			= "הדפסות";

	$mgrlang['packages_f_prints_d']			= "פריטי הדפסה כלולים בחבילה זו.";

	$mgrlang['packages_f_prod']				= "מוצרים";

	$mgrlang['packages_f_prod_d']			= "פריטים שכלולים בחבילה זו.";

	$mgrlang['packages_h_order']			= "הזמנה";

	$mgrlang['packages_h_item_code']		= "קוד פריט";

	$mgrlang['packages_h_item_name']		= "שם פריט";

	$mgrlang['packages_h_options']			= "אפשרויות";

	$mgrlang['packages_f_weight']			= "משקל חבילה";

	$mgrlang['packages_f_weight_d']			= "משקל החבילה בגרמים. משמש לחישובי משלוח.";

	$mgrlang['packages_f_price']			= "מחיר חבילה";

	$mgrlang['packages_f_price_d']			= "מחיר החבילה סה\"כ.";

	$mgrlang['packages_f_cost']				= "העלות שלי";

	$mgrlang['packages_f_cost_d']			= "העלות שלך בשביל החבילה. משמש לחישובי רווח.";

	$mgrlang['packages_f_perm']				= "הרשאות";

	$mgrlang['packages_f_perm_d']			= "מי יכול לראות/לרכוש חבילה זו.";

	$mgrlang['packages_f_taxable']			= "חיוב במס";

	$mgrlang['packages_f_taxable_d']		= "האם לחייב במס חבילה זו.";	

	$mgrlang['packages_f_credits']			= "קרדיטים";

	$mgrlang['packages_f_credits_d']		= "עלות קרדיטים לרכישת חבילה זו.";

	$mgrlang['packages_f_labcode']			= "Lab Code";

	$mgrlang['packages_f_labcode_d']		= "Lab code if needed for external printing, shipping, etc.";

	$mgrlang['packages_f_quantity']			= "כמות זמינה";

	$mgrlang['packages_f_quantity_d']		= "כמות חבילות זמינות. השאר ריק עבור ללא הגבלה.";

	$mgrlang['packages_f_groups']			= "קבוצות חבילות";

	$mgrlang['packages_f_groups_d']			= "בחר את קבוצות החבילות שזה שייך אליהן.";

	$mgrlang['packages_f_desc_d']			= "תיאור קצר של חבילה זו.";

	$mgrlang['packages_f_active_d']			= "הגדר מוצר זה כפעיל. מוצרים לא פעילים לא יוצגו באתר.";

	$mgrlang['packages_f_options']			= "אפשר אפשרויות";

	$mgrlang['packages_f_options_d']		= "אפשר לבחור אפשרויות להדפסות ומוצרים בתוך חבילה זו. זה ישנה את המחיר הבסיסי של החבילה.";

	$mgrlang['packages_f_adv_d']			= "פרסם חבילה זו בדף החבילות המקודמות.";	

	$mgrlang['packages_f_options2']			= "אפשרויות";

	$mgrlang['packages_f_colls']			= "אוספים";

	$mgrlang['packages_f_colls_d']			= "אוספים שכלולים בחבילה זו.";

	$mgrlang['packages_f_sub']				= "מינוי";

	$mgrlang['packages_f_sub_d']			= "אתה יכול לכלול מינוי אחד בחבילה זו.";

	$mgrlang['packages_f_attach_d']			= "היכן להציע חבילה זו למכירה. בחירה זו עוקפת בחירות מפריטי מדיה אינדיבידואליים.";

	

	# LANGUAGES

	$mgrlang['languages_tab1']				= "שפת האתר";

	$mgrlang['languages_tab2']				= "שפת פאנל הניהול";

	$mgrlang['languages_f_uselang']			= "שפה";

	$mgrlang['languages_f_uselang_d']		= "בחר שפה/ות שיהיו זמינות באתר. קבע את ברירת המחדל עבור מבקרים חדשים.";

	$mgrlang['languages_f_uselang_d2']		= "בחר שפה עבור איזור הניהול";

	$mgrlang['languages_defaults']			= "כדי לשנות את פורמט תאריך/שעה עבור שפה זו, ערוך את הקובץ";

	$mgrlang['languages_default']			= "ברירת מחדל";

	$mgrlang['languages_lang']				= "פעיל";

	

	# UTILITIES

	$mgrlang['util_f_ugmc']					= "עדכן מוני מדיית גלריה";

	$mgrlang['util_f_ugmc_d']				= "עדכן גלריות במספר הקבצים שהן מכילות.";

	$mgrlang['util_f_srp']					= "צור נקודת שחזור";

	$mgrlang['util_f_srp_d']				= "צור נקודת שחזור להגדרות האתר. אם יש בעיה תוכל לשחזר חזרה לנקודה זו. זה כולל את איזורי 'הגדרות האתר', 'מראה והרגשה' ו- 'התקנת תוכנה' בלבד.";

	$mgrlang['util_f_srp_b']				= "צור";

	$mgrlang['util_f_srp_o']				= "שם (אופציונלי)";	

	$mgrlang['util_f_restore']				= "הגדרות שחזור";

	$mgrlang['util_f_restore_d']			= "שחזר את הגדרות האתר מנקודת שחזור. זה כולל את אזורי 'הגדרות האתר', 'מראה והרגשה' ו- 'התקנת תוכנה'.";

	$mgrlang['util_f_restore_b']			= "שחזר";

	$mgrlang['util_f_cleanup']				= "ניקוי בסיס נתונים";

	$mgrlang['util_f_cleanup_d']			= "הרץ ניקוי על בסיס הנתונים כדי למחוק מידע לא בשימוש.";

	$mgrlang['util_f_cleanup_b']			= "הרץ ניקוי";


	$mgrlang['util_f_cache_b']				= "נקה זיכרון מטמון";

	$mgrlang['util_f_cache_d']				= "נקה זיכרון מטמון דף ותמונה.";

	$mgrlang['util_f_pal']					= "מחק רישומי פעילות";

	$mgrlang['util_f_pal_d']				= "מחק את כל רישומי הפעילות לפני תאריך זה.";

	$mgrlang['util_f_pal_b']				= "מחק";	

	$mgrlang['util_f_budb']					= "בצע גיבוי לבסיס הנתונים";

	$mgrlang['util_f_budb_d']				= "בצע גיבוי מלא לבסיס הנתונים ושמור את הקובץ לתיקיית 'backups' בשרת.";

	$mgrlang['util_f_budb_b']				= "הרץ גיבוי";	

	$mgrlang['util_f_sal']					= "רישומי פעילות במערכת";

	$mgrlang['util_f_sal_d']				= "צפה ברישומי הפעילות של המערכת. כולל כל הפעולות שנעשו מאחורי הקלעים.";	

	$mgrlang['util_f_pdac']					= "הדפס/הורד את רישומי הפעילות";

	$mgrlang['util_f_pdac_d']				= "הדפס או הורד את רישומי הפעילות עבור משתמשים או מנהלים.";

	$mgrlang['util_f_pdac_admins']			= "כל המנהלים";

	$mgrlang['util_f_pdac_mems']			= "כל המשתמשים";

	$mgrlang['util_f_pdac_for']				= "עבור";

	$mgrlang['util_f_pdac_from']			= "מ-";

	$mgrlang['util_f_pdac_to']				= "ל-";	

	$mgrlang['util_f_sal_b']				= "צפה ברישום פעילות";

	$mgrlang['util_mes_01']					= "רישומי פעילות ישנים נמחקו.";

	$mgrlang['util_mes_02']					= "נקודת שחזור נוצרה עבור הגדרות האתר.";

	$mgrlang['util_mes_03']					= "ההגדרות שוחזרו.";

	$mgrlang['util_mes_04']					= "בסיס הנתונים נוקה. רשומות לא בשימוש הוסרו.";

	$mgrlang['util_mes_05']					= "בסיס הנתונים גובה.";

	$mgrlang['util_mes_06']					= "זיכרון מטמון תמונה ודף נוקו.";

	

	# SUPPORT TICKETS

	$mgrlang['tickets_t_summary']			= "סיכום כרטיס";

	$mgrlang['tickets_f_messages']			= "הודעה";

	$mgrlang['tickets_f_files']				= "קובץ";

	$mgrlang['tickets_edit_header']			= "ערוך כרטיס תמיכה";

	$mgrlang['tickets_edit_message']		= "צפה והגב לכרטיס תמיכה זה.";

	$mgrlang['tickets_tab1']				= "פרטים";

	$mgrlang['tickets_f_summary']			= "סיכום";

	$mgrlang['tickets_f_summary_d']			= "סיכום שנכתב ע\"י המשתמש לכרטיס זה.";

	$mgrlang['tickets_f_notify']			= "הודע למשתמש";

	$mgrlang['tickets_f_notify_d']			= "הודע למשתמש במייל שהוא קיבל תגובה לכרטיס התמיכה שלו.";

	$mgrlang['tickets_f_close']				= "סגור כרטיס";	

	$mgrlang['tickets_id']					= "מזהה כרטיס";

	$mgrlang['tickets_opened']				= "כרטיס נפתח";	

	$mgrlang['tickets_updated']				= "כרטיס עודכן";

	$mgrlang['tickets_status']				= "סטטוס כרטיס";	

	$mgrlang['tickets_f_reply']				= "תגובה";

	$mgrlang['tickets_f_reply_d']			= "הוסף תגובה לכרטיס תמיכה זה.";	

	$mgrlang['tickets_f_tickcon']			= "רצף השיחה שבכרטיס";

	$mgrlang['tickets_f_tickcon_d']			= "כל ההודעות שבכרטיס זה. הודעות שמודגשות בכחול נענו על-ידי מנהל.";

	$mgrlang['tickets_mes_id']				= "מזהה הודעה";

	$mgrlang['tickets_no_mes']				= "אין הודעות בכרטיס תמיכה זה";	

	$mgrlang['tickets_f_files']				= "קבצים";

	$mgrlang['tickets_f_files_d']			= "קבצים שצורפו לכרטיס זה.";

	$mgrlang['tickets_file_id']				= "מזהה קובץ";	

	$mgrlang['tickets_filename']			= "שם קובץ";

	$mgrlang['tickets_added']				= "התווסף";

	$mgrlang['tickets_upped_by']			= "הועלה על-ידי";

	$mgrlang['tickets_size']				= "גודל";	

	$mgrlang['tickets_f_status']			= "שנה סטטוס ל-";

	$mgrlang['tickets_f_status_d']			= "הגדר את הסטטוס של הכרטיס.";

	$mgrlang['tickets_attach_file']			= "צרף קובץ";

	

	

	# NAVIGATION

	$mgrlang['nav_home']					= "לוח בקרה";

	

	$mgrlang['nav_content']					= "עורך תוכן";

	$mgrlang['nav_content_d']				= "אנא בחר מהאפשרויות.";

	$mgrlang['subnav_page_content']			= "תוכן דף";

	$mgrlang['subnav_page_content_d']		= "ערוך איזורי דפי תוכן ומקטעי טקסט באתר.";	

	$mgrlang['subnav_news']					= "מאמרי חדשות";

	$mgrlang['subnav_news_d']				= "נהל מאמרי חדשות שמופיעים באתר.";

	$mgrlang['subnav_news_groups']			= "קבוצות של חדשות";

	$mgrlang['subnav_news_groups_d']		= "קבוצות של חדשות משמשות כדי לארגן את החדשות בתוך איזור הניהול.";		

	$mgrlang['subnav_email_content']		= "תוכן אימייל";

	$mgrlang['subnav_email_content_d']		= "ערוך מיילים שנשלחים למבקרים של האתר.";	

	$mgrlang['subnav_agreements']			= "הסכמים";

	$mgrlang['subnav_agreements_d']			= "רשיונות מדיה וטפסי שחרור/הסכמים.";	

	$mgrlang['subnav_support_tickets']		= "כרטיסי תמיכה";

	$mgrlang['subnav_support_tickets_d']	= "קרא והגב לכרטיסי תמיכה מהמשתמשים.";

	

	$mgrlang['nav_library']					= "ספרייה";

	$mgrlang['nav_library_d']				= "אנא בחר מהאפשרויות.";

	$mgrlang['subnav_galleries']			= "גלריות/ארועים";

	$mgrlang['subnav_galleries_d']			= "צור וערוך גלריות או ארועים פרטיים/ציבוריים.";

	$mgrlang['subnav_media']				= "מדיה";

	$mgrlang['subnav_media_d']				= "נהל מדיה שקיימת כרגע באתר.";

	$mgrlang['subnav_asset_groups']			= "קבוצות מדיה";

	$mgrlang['subnav_asset_groups_d']		= "קבוצות מדיה משמשות כדי לארגן את המדיה בתוך איזור הניהול.";

	$mgrlang['subnav_collections']			= "אוספים דיגיטליים";

	$mgrlang['subnav_collections_d']		= "צור ונהל אוספי מדיה.";

	$mgrlang['subnav_collections_groups']	= "קבוצות אוספים";

	$mgrlang['subnav_collections_groups_d']	= "נהל קבוצות אוספים.";

	$mgrlang['subnav_digital_sp']			= "פרופילים דיגיטליים";

	$mgrlang['subnav_digital_sp_d']			= "התקן פרופילים עבור גירסאות דיגיטליות של המדיה שלך.";

	$mgrlang['subnav_prints']				= "הדפסות";

	$mgrlang['subnav_prints_d']				= "צור וערוך פריטי הדפסה.";

	$mgrlang['subnav_packages']				= "חבילות";

	$mgrlang['subnav_packages_d']			= "צור וערוך חבילות של הדפסות, מוצרים ופריטים דיגיטליים.";

	$mgrlang['subnav_package_groups']		= "קבוצות חבילות";

	$mgrlang['subnav_package_groups_d']		= "קבוצות חבילות משמשות כדי לארגן חבילות בתוך איזור הניהול.";

	$mgrlang['subnav_products']				= "מוצרים";

	$mgrlang['subnav_products_d']			= "צור וערוך מוצרים שרשומים באתר.";

	$mgrlang['subnav_product_groups']		= "קבוצות מוצרים";

	$mgrlang['subnav_product_groups_d']		= "קבוצות מוצרים משמשות כדי לארגן את המוצרים בתוך איזור הניהול.";

	$mgrlang['subnav_add_media']			= "הוסף מדיה חדשה";

	$mgrlang['subnav_add_files_d']			= "העלה וייבא מדיה חדשה לאתר.";	

	$mgrlang['subnav_media_types']			= "סוגי מדיה";

	$mgrlang['subnav_media_types_d']		= "צור וערוך שמות עבור סוגי המדיה.";

	$mgrlang['subnav_media_types_groups']	= "קבוצות סוגי מדיה";

	$mgrlang['subnav_media_types_groups_d']	= "קבוצות סוגי מדיה משמשות כדי לארגן את סוגי המדיה.";

	$mgrlang['subnav_folders']				= "תיקיות";

	$mgrlang['subnav_folders_d']			= "צור וערוך תיקיות שבהם מאוחסנת המדיה שלך.";

	$mgrlang['subnav_digital_sp_groups']	= "קבוצות פרופילים דיגיטליים";

	$mgrlang['subnav_digital_sp_groups_d']	= "קבוצות פרופילים דיגיטליים משמשות כדי לארגן את הפרופילים הדיגיטליים בתוך איזור הניהול.";

	$mgrlang['subnav_print_groups']			= "קבוצות של הדפסות";

	$mgrlang['subnav_print_groups_d']		= "קבוצות של הדפסות משמשות כדי לארגן את ההדפסות באיזור הניהול.";

	$mgrlang['subnav_media_queue']			= "מדיה בהמתנה";

	$mgrlang['subnav_media_queue_d']		= "מדיה שהועלתה על-ידי משתמשים, שממתינה לאישור.";

	$mgrlang['subnav_media_comments']		= "הערות מדיה";

	$mgrlang['subnav_media_comments_d']		= "הערות שמבקרים כתבו על מדיה באתר.";

	$mgrlang['subnav_media_ratings']		= "דירוגי מדיה";

	$mgrlang['subnav_media_ratings_d']		= "מדיה באתר שמבקרים דרגו.";

	$mgrlang['subnav_media_tags']			= "תוויות מדיה";

	$mgrlang['subnav_media_tags_d']			= "מדיה באתר שמבקרים הציעו תוויות חדשות בשבילה.";

	

	$mgrlang['nav_sales']					= "מכירות";

	$mgrlang['nav_sales_d']					= "אנא בחר מהאפשרויות.";

	$mgrlang['subnav_orders']				= "הזמנות";

	$mgrlang['subnav_orders_d']				= "צפה בהזמנות ממתינות ושהסתיימו.";

	$mgrlang['subnav_order_groups']			= "קבוצות של הזמנות";

	$mgrlang['subnav_order_groups_d']		= "קבוצות של הזמנות משמשות כדי לארגן את ההזמנות באיזור הניהול.";

	$mgrlang['subnav_pending_orders']		= "הזמנות ממתינות";

	$mgrlang['subnav_pending_orders_d']		= "צפה בהזמנות ממתינות.";	

	$mgrlang['subnav_lightboxes']			= "תיבות אישיות";

	$mgrlang['subnav_lightboxes_d']			= "צפה בתיבות אישיות של משתמשים.";

	$mgrlang['subnav_lightboxes_groups']	= "קבוצות תיבות אישיות";

	$mgrlang['subnav_lightboxes_groups_d']	= "קבוצות של תיבות אישיות משתמשות כדי לארגן את התיבות האישיות שמבקרים יוצרים.";	

	

	$mgrlang['subnav_billings']				= "חיובים";

	$mgrlang['subnav_billings_d']			= "צפה בחיובי משתמשים ממתינים/שהושלמו.";	

	$mgrlang['subnav_quotes']				= "בקשות מחירים";

	$mgrlang['subnav_quotes_d']				= "צפה בבקשות עבור הצעות מחיר.";	

	

	$mgrlang['nav_users']					= "משתמשים";

	$mgrlang['nav_users_d']					= "אנא בחר מהאפשרויות.";

	$mgrlang['subnav_administrators']		= "מנהלים";

	$mgrlang['subnav_administrators_d']		= "נהל מנהלים והרשאות.";	

	$mgrlang['subnav_members']				= "משתמשים רשומים";

	$mgrlang['subnav_members_d']			= "נהל משתמשים רשומים לאתר.";

	$mgrlang['subnav_member_bios']			= "ביוגרפיות";

	$mgrlang['subnav_member_bios_d']		= "ביוגרפיות שמשתמשים כתבו על עצמם.";

	$mgrlang['subnav_member_avatars']		= "דמויות";

	$mgrlang['subnav_member_avatars_d']		= "דמויות שמשתמשים העלו.";

	$mgrlang['subnav_member_groups']		= "קבוצות משתמשים רשומים";

	$mgrlang['subnav_member_groups_d']		= "'קבוצות משתמשים רשומים' משמש כדי לארגן את המשתמשים בתוך איזור הניהול.";

	$mgrlang['subnav_memberships']			= "תוכניות הרשמה";

	$mgrlang['subnav_memberships_d']		= "נהל תוכניות הרשמה.";

	$mgrlang['subnav_membership_groups']	= "קבוצות תוכניות הרשמה";

	$mgrlang['subnav_membership_groups_d']	= "'קבוצות תוכניות הרשמה' משמש כדי לארגן את התוכניות בתוך איזור הניהול.";

	

	$mgrlang['nav_help']					= "עזרה";

	$mgrlang['subnav_bfish']				= "Yahoo Babel Fish";

	$mgrlang['subnav_bfish_d']				= "מתרגם און-ליין מאת Yahoo.";

	$mgrlang['subnav_googletrans']			= "Google Translate";

	$mgrlang['subnav_googletrans_d']		= "מתרגם און-ליין מאת Google.";

	$mgrlang['subnav_passgen']				= "מייצר סיסמאות";

	$mgrlang['subnav_passgen_d']			= "צור סיסמאות עבור האתר ואיזור הניהול.";

	$mgrlang['subnav_wizard']				= "טען אשף התקנה";

	$mgrlang['subnav_wizard_d']				= "אשף התקנה צעד-אחר-צעד.";	

	$mgrlang['subnav_forum']				= "פורום תמיכה";

	$mgrlang['subnav_forum_d']				= "השתתף בדיונים ומצא עזרה עם משתמשים אחרים.";

	$mgrlang['subnav_faqs']					= "דוקומנטציה & מדריכים";

	$mgrlang['subnav_faqs_d']				= "בסיס נתונים של שאלות נפוצות, לעזור לך לנהל ולהתקין את האתר.";

	$mgrlang['subnav_extras']				= "תוספים & תוספות";

	$mgrlang['subnav_extras_d']				= "הוסף תכונות ותוספות לאתר.";

	$mgrlang['subnav_manual']				= "מדריך און-ליין";

	$mgrlang['subnav_manual_d']				= "מדריך אינטרנטי עבור מוצר זה.";

	$mgrlang['subnav_software_upgrade']		= "עדכון תוכנה";

	$mgrlang['subnav_software_upgrade_d']	= "בדוק והחל שדרוגים זמינים והתקן תוספים חדשים.";

	

	$mgrlang['nav_settings']				= "הגדרות";

	$mgrlang['nav_settings_d']				= "אנא בחר מהאפשרויות.";	

	$mgrlang['subnav_payment_options']		= "שירותי סליקת תשלומים";

	$mgrlang['subnav_payment_options_d']	= "שנה הגדרות תשלומים ושירותי סליקה.";	

	$mgrlang['subnav_website_settings']		= "הגדרות האתר";

	$mgrlang['subnav_website_settings_d']	= "התאם את הגדרות האתר.";

	$mgrlang['subnav_look']					= "מראה והרגשה";

	$mgrlang['subnav_look_d']				= "התאם את הגדרות 'מראה והרגשה' של האתר.";	

	$mgrlang['subnav_software_setup']		= "התקנת תוכנה";

	$mgrlang['subnav_software_setup_d']		= "התאם את הגדרות התוכנה כך שתתאים לצרכיך.";	

	$mgrlang['subnav_languages']			= "שפות";

	$mgrlang['subnav_languages_d']			= "בחר את השפה בה יוצג האתר.";	

	$mgrlang['subnav_countries']			= "מדינות";

	$mgrlang['subnav_countries_d']			= "ערוך את רשימת המדינות.";

	$mgrlang['subnav_countries_groups']		= "קבוצות של מדינות";

	$mgrlang['subnav_countries_groups_d']	= "קבוצות של מדינות משמש כדי לארגן את המדינות באיזור הניהול.";	

	$mgrlang['subnav_states']				= "מדינות/פרובינציות";

	$mgrlang['subnav_states_d']				= "ערוך רשימת מדינות ופרובינציות.";

	$mgrlang['subnav_zipcodes']				= "מיקודים";

	$mgrlang['subnav_zipcodes_d']			= "ערוך רשימת מיקודים.";		

	$mgrlang['subnav_shipping']				= "משלוח";

	$mgrlang['subnav_shipping_d']			= "הגדר שיטות משלוח וחישובים.";

	$mgrlang['subnav_shipping_groups']		= "קבוצות של משלוחים";

	$mgrlang['subnav_shipping_groups_d']	= "קבוצות של משלוחים משמש כדי לארגן את שיטות המשלוח באיזור הניהול.";		

	$mgrlang['subnav_taxes']				= "מיסים";

	$mgrlang['subnav_taxes_d']				= "הגדר שיעורי מס/מע\"מ.";

	$mgrlang['subnav_rightsmanaged']		= "תמחור Rights Managed";

	$mgrlang['subnav_rightsmanaged_d']		= "הגדר תוכניות תמחור rights managed.";

	$mgrlang['subnav_credits']				= "חבילות קרדיטים";

	$mgrlang['subnav_credits_d']			= "הגדר מחירים לקניית קרדיטים.";

	$mgrlang['subnav_currencies']			= "מטבעות";

	$mgrlang['subnav_currencies_d']			= "הגדר אילו מטבעות האתר יאפשר לשלם בהם.";	

	$mgrlang['subnav_subscriptions']		= "מינויים";

	$mgrlang['subnav_promotions']			= "קידומי מכירות & קופונים";

	$mgrlang['subnav_subscriptions_d']		= "ערוך/צור קידומי מכירות וקופונים.";	

	$mgrlang['subnav_components']			= "רכיבים";

	$mgrlang['subnav_components_d']			= "הגדר רכיבים להוספה לאתר.";

	$mgrlang['subnav_services']				= "שירותים";

	$mgrlang['subnav_utilities']			= "כלים";

	$mgrlang['subnav_utilities_d']			= "כלי ניהול נוספים עבור האתר.";	

	$mgrlang['subnav_storage']				= "Storage <span class='mtag_grey roundme' style='font-size: 10px; font-weight: normal'>beta</span>";

	$mgrlang['subnav_storage_d']			= "הגדר שרתים נוספים שיכולים לארח מדיה.";

	$mgrlang['subnav_storage_groups']		= "קבוצות אחסון";

	$mgrlang['subnav_storage_groups_d']		= "קבוצות אחסון משמשות כדי לארגן את איזורי האחסון בתוך איזור הניהול.";

	$mgrlang['subnav_folders_groups']		= "קבוצות של תיקיות";

	$mgrlang['subnav_folders_groups_d']		= "קבוצות של תיקיות משמש כדי לארגן את התיקיות באיזור הניהול.";

	$mgrlang['subnav_toolslinks']			= "הקישורים שלי";

	$mgrlang['subnav_toolslinks_d']			= "ערוך 'הקישורים שלי' בתפריט הנפתח כלים וקישורים.";

	

	$mgrlang['nav_reports']					= "דוחות";

	$mgrlang['nav_reports_d']				= "אנא בחר מהאפשרויות.";

	

?>