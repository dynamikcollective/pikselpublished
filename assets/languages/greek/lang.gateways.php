<?php
	# GATEWAYS LANG
	# 4.6.3
	# STRIPE
	$lang['stripe_displayName']			= "Stripe";
	$lang['stripe_publicDescription']	= "Credit Card or Debit Card";
	$lang['stripe_pkey']				= "Publishable Key";
	$lang['stripe_pkey_d']				= "Your publishable key for your stripe account.";
	$lang['stripe_skey']				= "Secret Key";
	$lang['stripe_skey_d']				= "Your secret key for your stripe account.";
	
	# 4.1.4
	# MOLLIE IDEAL
	$lang['mollieideal_displayName']		= "iDeal (Mollie)";
	$lang['mollieideal_f_partnerid']		= "ID Συνεργάτη";
	$lang['mollieideal_f_partnerid_d']		= "Ο iDeal ID Συνεργάτη";
	$lang['mollieideal_f_profilekey']		= "Profile key";
	$lang['mollieideal_f_profilekey_d']		= "Ο Mollie iDeal Profile key.";
	$lang['mollieideal_f_testmode']			= "Τέστ κατάσταση";
	$lang['mollieideal_f_testmode_d']		= "Γύρισε το Mollie iDeal σε κατάσταση τεστ.";
	$lang['mollieideal_instructions']		= "Βεβαιωθείτε ότι η κατάσταση τεστ αντιστοιχεί στις ρυθμίσεις στο Mollie.nl. ";
	$lang['mollieideal_publicDescription']	= "Πληρώστε εύκολα και ασφαλή με το iDeal (διαθέσιμο μόνο στην Ολλανδία).";
	
	# 4.1
	# ONEBIP 
	$lang['onebip_displayName']			= "OneBip"; 
	$lang['onebip_merchantid']			= "Email";	
	$lang['onebip_merchantid_d']		= "Enter the email used for your onebip Account."; 
	$lang['onebip_merchantkey']			= "API Key"; 
	$lang['onebip_merchantkey_d']		= "Enter the API key for your onebip Account.";
	$lang['onebip_testmode']			= "κατάσταση τεστ";
	$lang['onebip_testmode_d']			= "Τοποθετήστε το  onebip payment into testing mode.";
	$lang['onebip_publicDescription']	= "Πληρώστε με το κινητό";
	
	# PAYFAST 
	$lang['payfast_displayName']		= "PayFast"; 
	$lang['payfast_merchantid']			= "Merchant ID";	
	$lang['payfast_merchantid_d']		= "Τοποθετήστε the merchant ID for your PayFast Account."; 
	$lang['payfast_merchantkey']		= "Merchant Key"; 
	$lang['payfast_merchantkey_d']		= "Τοποθετήστε the merchant key for your PayFast Account.";
	$lang['payfast_testmode']			= "κατάσταση τεστ"; 
	$lang['payfast_testmode_d']			= "Τοποθετήστε το PayFast into testing mode.";
	$lang['payfast_publicDescription']	= "Credit Card or Bank Account"; 
	
	# NOCHEX
	$lang['nochex_displayName']			= "Nochex";
	$lang['nochex_f_accountid']			= "Account ID";
	$lang['nochex_f_accountid_d']		= "Your Nochex Account ID.";
	$lang['nochex_f_testmode']			= "κατάσταση τεστ";
	$lang['nochex_f_testmode_d']		= "Τοποθετήστε το  Nochex payments into testing mode.";
	$lang['nochex_publicDescription']	= "Credit or Debit Card";
	
	# WORLDPAY
	$lang['worldpay_displayName']		= "WorldPay";
	$lang['worldpay_f_installid']		= "Installation ID";
	$lang['worldpay_f_installid_d']		= "Installation ID for your WorldPay account.";
	$lang['worldpay_f_testmode']		= "κατάσταση τεστ";
	$lang['worldpay_f_testmode_d']		= "Τοποθετήστε το WorldPay payments into testing mode.";
	$lang['worldpay_publicDescription']= "Πιστωτκή ή χρεωστική κάρτα";
	
	# ROBOKASSA
	$lang['robokassa_displayName']		= "RoboKassa";
	$lang['robokassa_f_merchantid']		= "Merchant ID";
	$lang['robokassa_f_merchantid_d']	= "Merchant ID for your RoboKassa account.";
	$lang['robokassa_f_merchantpass']	= "Merchant Pass";
	$lang['robokassa_f_merchantpass_d']	= "Your RoboKassa merchant pass ID.";
	$lang['robokassa_publicDescription']= "Πιστωτκή ή χρεωστική κάρτα";
	
	# PAYGATE
	$lang['paygate_displayName']		= "PayGate";
	$lang['paygate_f_accountid']		= "PayGate ID";
	$lang['paygate_f_accountid_d']		= "ID for your PayGate account.";
	$lang['paygate_f_accountkey']		= "PayGate Key";
	$lang['paygate_f_accountkey_d']		= "Your PayGate key ID.";
	$lang['paygate_f_testmode']			= "κατάσταση τεστ";
	$lang['paygate_f_testmode_d']		= "Τοποθετήστε το PayGate payments σε κατάσταση τεστ.";	
	$lang['paygate_f_testid']			= "Test PayGate ID";
	$lang['paygate_f_testid_d']			= "Your PayGate test id";
	$lang['paygate_f_testkey']			= "Test PayGate Key";
	$lang['paygate_f_testkey_d']		= "Your PayGate test Key";
	$lang['paygate_publicDescription']	= "Πιστωτκή ή χρεωστική κάρτα";
	
	# PAYSTATION
	$lang['paystation_displayName']		= "PayStation";
	$lang['paystation_f_accountid']		= "PayStation ID";
	$lang['paystation_f_accountid_d']	= "ID for your PayStation account.";
	$lang['paystation_f_testmode']		= "κατάσταση τεστ";
	$lang['paystation_f_testmode_d']	= "Τοποθετήστε το PayStation payments σε κατάσταση τεστ.";	
	$lang['paystation_f_gatewayid']		= "Gateway ID";
	$lang['paystation_f_gatewayid_d']	= "Your PayStation gateway ID.";
	$lang['paystation_publicDescription']= "Πιστωτκή ή χρεωστική κάρτα";
	
	# SKRILL
	$lang['skrill_displayName']			= "Skrill (moneybookers)";
	$lang['skrill_f_email']				= "Email διεύθυνση";
	$lang['skrill_f_email_d']			= "Email διεύθυνση for your Skrill account.";
	$lang['skrill_f_testmode']			= "κατάσταση τεστ";
	$lang['skrill_f_testmode_d']		= "Τοποθετήστε το Skrill payments σε κατάσταση τεστ.";	
	$lang['skrill_f_testemail']			= "Testing Email";
	$lang['skrill_f_testemail_d']		= "Your Skrill testing email διεύθυνση.";
	$lang['skrill_publicDescription']	= "Πιστωτκή ή χρεωστική κάρτα";
	$lang['skrill_f_completeOrder']		= "Κάντε κλικ εδώ για να ολοκληρώσετε την παραγγελία σα!";
	
	# CHRONOPAY
	$lang['chronopay_displayName']		= "ChronoPay";
	$lang['chronopay_f_clientid']		= "Client ID";
	$lang['chronopay_f_clientid_d']		= "Your ChronoPay Client ID.";
	$lang['chronopay_f_siteid']			= "Site ID";
	$lang['chronopay_f_siteid_d']		= "Your ChronoPay Site ID.";
	$lang['chronopay_f_productid']		= "Product ID.";
	$lang['chronopay_f_productid_d']	= "Your ChronoPay Product ID.";
	$lang['chronopay_publicDescription']= "Πιστωτκή ή χρεωστική κάρτα";
	
	# IDEAL
	$lang['ideal_displayName']			= "iDeal (ING)";
	$lang['ideal_f_accountid']			= "Account ID";
	$lang['ideal_f_accountid_d']		= "Your iDeal Account ID.";
	$lang['ideal_f_transkey']			= "Secret Key";
	$lang['ideal_f_transkey_d']			= "Your iDeal Secret Key.";
	$lang['ideal_f_testmode']			= "κατάσταση τεστ";
	$lang['ideal_f_testmode_d']			= "Τοποθετήστε το  iDeal payments σε κατάσταση τεστ.";
	$lang['ideal_publicDescription']	= "Τραπεζικός Λογαριασμός";
	
	# PAYPAL
	$lang['paypal_displayName']			= "PayPal";
	$lang['paypal_f_email']				= "Διεύθυνση Email";
	$lang['paypal_f_email_d']			= "Email διεύθυνση για τον λογαριασμό PayPal σας..";
	$lang['paypal_f_testmode']			= "κατάσταση τεστ";
	$lang['paypal_f_testmode_d']		= "Τοποθετήστε τις πληρωμές PayPal σε κατάσταση τεστ.";	
	$lang['paypal_f_testemail']			= "Testing Email";
	$lang['paypal_f_testemail_d']		= "Your PayPal sandbox testing email address.";
	$lang['paypal_publicDescription']	= "PayPal, Πιστωτκή ή Τραπεζικός λογαριασμός";
	
	# 2CHECKOUT
	$lang['2checkout_displayName']		= "2Checkout.com";
	$lang['2checkout_f_accountid']		= "Account ID";
	$lang['2checkout_f_accountid_d']	= "Your 2Checkout Account ID.";
	$lang['2checkout_f_testmode']		= "κατάσταση τεστ";
	$lang['2checkout_f_testmode_d']		= "Τοποθετήστε το  2Checkout payments σε κατάσταση τεστ.";
	$lang['2checkout_instructions']		= "In 2Checkout.com set direct return to 'Header Redirect (Your URL)' and set approved URL to http://www.YOUR_DOMAIN_NAME.com/assets/gateways/2checkout/ipn.php";
	$lang['2checkout_publicDescription']= "Πιστωτική η US Τραπεζικός λογαριασμός";

	
	# Plug n' Pay
	$lang['plugnpay_displayName']		= "Plug n' Pay";
	$lang['plugnpay_f_accountid']		= "Account ID";
	$lang['plugnpay_f_accountid_d']		= "Your Plug n' Pay Account ID.";
	$lang['plugnpay_publicDescription']	= "";
	
	# AUTHORIZE.NET
	$lang['authorize_displayName']		= "Authorize.net";
	$lang['authorize_f_apiid']			= "API Login ID";
	$lang['authorize_f_apiid_d']		= "Your Authorize.net API Login ID.";
	$lang['authorize_f_transkey']		= "Transaction Key";
	$lang['authorize_f_transkey_d']		= "Your Authorize.net Transaction Key.";
	$lang['authorize_f_testmode']		= " κατάσταση τεστ";
	$lang['authorize_f_testmode_d']		= "Τοποθετήστε το  Authorize.net payments σε κατάσταση τεστ.";
	$lang['authorize_publicDescription']= "";
	$lang['authorize_f_completeOrder'] = "Κάντε κλικ εδώ να ολοκληρώσετε την παραγγελία σας!";
	
	# MYGATE.CO.ZA
	$lang['mygate_displayName']			= "MyGate.co.za";
	$lang['mygate_f_merchantid']		= "Merchant ID";
	$lang['mygate_f_merchantid_d']		= "Your MyGate.co.za Merchant ID.";
	$lang['mygate_f_appid']				= "Application ID";
	$lang['mygate_f_appid_d']			= "Your MyGate.co.za Application ID.";
	$lang['mygate_publicDescription']	= "";
	$lang['mygate_f_testmode']			= "Κατάσταση τεστ";
	$lang['mygate_f_testmode_d']		= "Τοποθετήστε το Mygate payments σε κατάσταση τεστ.";	
	
	# MAIL IN PAYMENT
	$lang['mailin_displayName']			= "Mail με πληρωμή";
	$lang['mailin_f_instructions']		= "Mail με οδηγίες πληρωμής ";
	$lang['mailin_f_instructions_d']	= "Οδηγίες για τους πελάτες για να στείλετε mail με πληρωμές, όπως επιταγές ή εντολές πληρωμής.";
	$lang['mailin_publicDescription']	= "Mail την πληρωμή σας στη διεύθυνση που παρέχονται στην επόμενη σελίδα.";
?>