<?php
	# DANISH
	$calendar = array();
	$calendar['long_month']		= array(1 => "Januar","Februar","Marts","April","Maj","Juni","Juli","August","September","Oktober","November","December");
	$calendar['short_month']	= array(1 => "Jan","Feb","Mar","Apr","Maj","Jun","Jul","Aug","Sep","Okt","Nov","Dec");
	$calendar['full_days']		= array(1 => "S\u00F8ndag","Mandag","Tirsdag","Onsdag","Torsdag","Fredag","L\u00F8rdag");
	$calendar['short_days']		= array(1 => "S\u00F8n","Man","Tir","Ons","Tor","Fre","L\u00F8r");
?>