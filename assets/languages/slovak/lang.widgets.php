<?php
	# 4.4.6
	$wplang['stats_visitors']		= "Približných návštev na tomto mieste";
	
	# 4.1.3
	$wplang['qstats_pending_media']	= "Čaká na prispievateľa";
	
	# 4.0.7	
	$wplang['sitehealth_exif']	= "PHP EXIF Podpora (exif_read_data)";
	
	# GENERAL WIDGET LANGUAGE
	$wplang['widget_close']		= "Zavrieť";
	$wplang['widget_save']		= "Ulož";
	$wplang['load_failed'] 		= "Nahravací panel Chyba!";
	$wplang['widget_title']		= "Názov";
	$wplang['widget_note']		= "Popis";
	$wplang['widget_for']		= "Pre";
	
	# NOTES WIDGET
	$wplang['notes_title']		= "Poznámky";
	$wplang['notes_newnote']	= "Nová poznámka";
	$wplang['notes_postedby']	= "Pridal";
	$wplang['notes_lastupdate']	= "Posledná Aktualizácia";
	$wplang['notes_nonotes']	= "Niesu poznámky na zobrazenie";
	
	# EXTRAS WIDGET
	$wplang['extras_title']		= "PhotoStore Extras";
	
	# KTOOLS ACCOUNT WIDGET
	$wplang['kaccount_title']	= "Ktools Účet";
	$wplang['kaccount_support']	= "Podpora/Aktualizácia zostáva dní";
	$wplang['kaccount_messages']= "Neprečítané správy";
	$wplang['kaccount_affil']	= "Partnerský predaj od posledného prihlásenia";
	
	# KNEWS WIDGET
	$wplang['knews_title']		= "Ktools.net Novinky";
	
	# QUICK STATS WIDGET - NEW AND PENDING
	$wplang['qstats_title']		= "Nové a Čakajúce";
	$wplang['qstats_logmem']	= "Členovia od posledného prihlásenia";
	$wplang['qstats_penmem']	= "Čakajúci členovia";
	$wplang['qstats_logorders']	= "Objednávky od posledného prihlásenia";
	$wplang['qstats_penorders']	= "Čakajúce objednávky";
	$wplang['qstats_logcomm']	= "Komentáre od posledného prihlásenia";
	$wplang['qstats_pencomm']	= "Čakajúce komentáre";
	$wplang['qstats_logtags']	= "Kľúčové slová od posledného prihlásenia";
	$wplang['qstats_pentags']	= "Čakajúce kľúčové slová";
	$wplang['qstats_lograte']	= "Hodnotenia od posledného prihlásenia";
	$wplang['qstats_penrate']	= "Čakajúce hodnotenia";
	$wplang['qstats_penbios']	= "Čakajúci členovia";
	$wplang['qstats_penavatars']= "Čakajúci Avatary";
	$wplang['qstats_pensupport']= "Čakajúci na technickú podporu";
	
	# STATS WIDGET
	$wplang['stats_title']		= "Štatistiky";
	$wplang['stats_op1']		= "Predaje";
	$wplang['stats_op2']		= "Členovia";
	$wplang['stats_op_7days']	= "Posledných 7 Dní";
	$wplang['stats_op_6mon']	= "Posledných 6 Mesiacov";
	$wplang['stats_op_5year']	= "Posledných 5 Rokov";
	$wplang['stats_tdsales']	= "Dnešný predaj";
	$wplang['stats_tdorders']	= "Dnešných objednávok";
	$wplang['stats_sales']		= "Predaje";
	$wplang['stats_orders']		= "Objednávky";	
	$wplang['stats_atsales']	= "Predaje za celý čas";
	$wplang['stats_atorders']	= "Objednávky za celý čas";
	$wplang['stats_tdmems']		= "Nový členovia Dnes";
	$wplang['stats_mems']		= "Nový členovia";
	$wplang['stats_tamems']		= "Celkový počet aktívnych členov";
	$wplang['stats_timems']		= "Celkový počet neaktívnych členov";
	
	
	# SITE HEALTH WIDGET
	$wplang['sitehealth_title']	= "Stránka a Server Stav";
	$wplang['sitehealth_ok']	= "OK";
	$wplang['sitehealth_low']	= "Nízky";
	$wplang['sitehealth_high']	= "VYSOKÝ";
	$wplang['sitehealth_failed']= "ZLYHALA";
	$wplang['sitehealth_unava'] = "Nieje k dispozícii";
	$wplang['sitehealth_off']	= "OFF";
	$wplang['sitehealth_on']	= "ON";
	$wplang['sitehealth_inst']	= "Inštalovaná";
	$wplang['sitehealth_none']	= "Nieje";
	$wplang['sitehealth_exists']= "Existuje";
	$wplang['sitehealth_write']	= "Prepisovaťeľný";
	$wplang['sitehealth_nonwri']= "Nieje Prepisovateľný";
	$wplang['sitehealth_php']	= "PHP Verzia";
	$wplang['sitehealth_gd']	= "GD Knižnica";
	$wplang['sitehealth_mem']	= "PHP Memory Limit";
	$wplang['sitehealth_exe']	= "PHP max_execution_time";
	$wplang['sitehealth_time']	= "PHP max_input_time";
	$wplang['sitehealth_file']	= "PHP upload_max_filesize";
	$wplang['sitehealth_post']	= "PHP post_max_size";
	$wplang['sitehealth_safe']	= "PHP safe_mode";
	$wplang['sitehealth_dbv']	= "Databáza/Overiť verziu produktu";
	$wplang['sitehealth_mysqlv']= "MySQL Verzia";
	$wplang['sitehealth_load']	= "Server Load";
	$wplang['sitehealth_upti']	= "Server beží";
	
	# UPDATE CHECK WIDGET
	$wplang['updater_title']	= "Zistiť aktualizaciu";
	$wplang['updater_newest']	= "Máte najnovšiu verziu";
	$wplang['updater_update']	= "Aktualizácia pripravená";
	$wplang['updater_newestis']	= "Novšia verzia je"; 
	$wplang['updater_getnew']	= "Ak chcete získať novšiu veriu , prihláste sa do vášho <a href='http://www.ktools.net/members/' target='_blank'>Ktools.net konta</a>";
	$wplang['updater_yourv']	= "Používate verziu";
	
	# BLANK WIDGET
	$wplang['blank_title']		= "Prázdny Panel";
	
	# CALENDAR WIDGET
	$wplang['calendar_title']	= "Kalendár";
	
	# TIPS CHECK WIDGET
	//$wplang['tips_title']		= "Tipy";
	//$wplang['tips_next']		= "Nasledujúci Tip";
	//$welcometip[] 				= "Did you know you can use <strong>Ctrl+Shift+S</strong> to open and close the shortcuts menu?"; 
	//$welcometip[] 				= "You can drag these welcome panels to rearrange them the way you would like. Just click on the title and drag. Then drop the panel when it is in the order that you would like."; 
	//$welcometip[] 				= "For quick access to your galleries and many other areas click on the tab with the arrow in the upper left. This will open the shortcuts menu. Click the tab again to close it."; 
	
?>