<?php
	# SLOVAK
	$calendar = array();
	$calendar['long_month']		= array(1 => "Január","Február","Marec","Apríľ","Máj","Jún","Júl","August","September","Október","November","December");
	$calendar['short_month']	= array(1 => "Jan","Feb","Mar","Apr","Máj","Jún","Júl","Aug","Sept","Okt","Nov","Dec");
	$calendar['full_days']		= array(1 => "Nedeľa","Pondelok","Utorok","Streda","Štvrtok","Piatok","Sobota");
	$calendar['short_days']		= array(1 => "Ned","Pon","Uto","Str","Štv","Pia","Sob");
?>