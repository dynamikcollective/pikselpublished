<?php
	
	# 4.7
	$lang['featuredGalleries']		= "Featured Galleries";
	
	# 4.6.3	
	$lang['ccMessage']				= "Please enter your credit card information below.";
	$lang['ccNumber']				= "Card Number";
	$lang['ccCVC']					= "CVC";
	$lang['ccExpiration']			= "Expiration (MM/YYYY)";
	
	
	# 4.6.1
	$lang['passwordLeaveBlank']		= "Deja en blanco para ninguna contraseña";
	$lang['myAlbums']				= "Mis álbumes";
	
	# 4.5
	$lang['dateDownloadUpper']		= "FECHA DESCARGADO";
	$lang['downloadTypeUpper']		= "DESCARGA TIPO";
	$lang['rss']					= "RSS";

	# 4.4.8

	$lang['uploadFile']				= "Cargar Archivo";

	

	# 4.4.7

	$lang['contrSmallFileSize']		= "El archivo es muy pequeño. Debe tener se de por lo menos";

	# plupload

	$lang['plupSelectFiles']		=  "Seleccionar archivos";

	$lang['plupAddFilesToQueue']	=  "Agregar archivos a subir y hacer click en el botón de inicio.";

	$lang['plupFilename']			=  "Archivo";

	$lang['plupStatus']				=  "Estado";

	$lang['plupSize']				=  "Tamaño";

	$lang['plupAddFiles']			=  "Agregar Archivos";

	$lang['plupStartUplaod']		=  "Inciar la subida";

	$lang['plupStopUpload']			=  "Interrumpir la subida";

	$lang['plupStartQueue']			=  "Iniciar carga de información";

	$lang['plupDragFilesHere']		=  "Arrastrar los archivos hasta aquí.";

	

	# 4.4.6

	$lang['batchUploader']			= "Subir de a Lotes";

	$lang['uploader'][1]			= "Basado en Java";

	$lang['uploader'][2]			= "Basado en HTML5/Flash";

	$lang['change']					= "Cambiar";

	

	# 4.4.5

	$lang['moreInfoPlus']				= "[Más info]";

	

	# 4.4.3

	$lang['mediaLabelPropRelease']	= "Cesión de derechos sobre imagen de la Propiedad";

	

	# 4.4.2

	$lang['go']						= "IR";

	

	# 4.4.0

	$lang['share'] 					= "Compartir";

	$lang['bbcode'] 				= "BBCode";

	$lang['html'] 					= "HTML";

	$lang['link']					= "Vínculo";

	$lang['pricingCalculator']		= "Calculadora de Precios";

	$lang['noOptionsAvailable']		= "No hay opciones disponibles";

	$lang['viewCartUpper']			= "VER CARRITO";

		

# 4.3

	$lang['vatIDNumber']			= "ID Fiscal";

	

# 4.2.1

	$lang['captchaError']			= "El captcha ingresado no es correcto. El mensaje no fue enviado.";



# 4.1.7

	$lang['mediaLicenseEU'] 		= "Uso Editorial";

	

# 4.1.6

	$lang['yourBill'] 				= "Su Cuenta";

	$lang['invoiceNumber'] 			= "Número de Factura";

	$lang['paymentThanks'] 			= "Gracias por su compra.";

	$lang['msActive'] 				= "Su cuenta ya está activa.";

	$lang['today'] 					= "Hoy";

	$lang['downloadsRemainingB']	= "Reiniciar descarga";



# 4.1.4

	$lang['link'] 					= "Vínculo";

	$lang['optional'] 				= "Opcional";



# 4.1.3

	$lang['keywordRelevancy'] 		= "Palabra clave";

	$lang['mediaLicenseEX'] 		= "Licencia estendida";



# 4.1.1

	$lang['editMediaDetails'] 		= "Edita detalles";

	$lang['searchResults'] 			= "Resultado de la búsqueda";

	$lang['width'] 					= "Ancho";

	$lang['height'] 				= "Alto";

	$lang['hd'] 					= "HD";

	$lang['mediaProfile'] 			= "Perfil";

	$lang['photo'] 					= "foto";

	$lang['video'] 					= "Video";

	$lang['other'] 					= "Otra";

	$lang['thumbnail'] 				= "Miniatura";

	$lang['videoSample'] 			= "Ver Video";

	$lang['attachFile'] 			= "Adjuntar archivo";

	$lang['fileAttached'] 			= "Archivo adjuntado";

	$lang['attachMessage'] 			= "Seleccione el perfil para adjuntar al archivo.";

	$lang['browse'] 				= "Visualizar";

	$lang['uploadThumb'] 			= "Subir miniatura";

	$lang['uploadVideo'] 			= "Subir muestra de Video";



# 4.1

	$lang['importSuccessMes'] 		= "Importación finalizada!";

	$lang['forgotPassword'] 		= "Olvidó su contraseña?";

	$lang['passwordSent'] 			= "Se le ha enviado un mensaje por correo electrónico, mire en su bandeja de entrada o en la carpeta de correo no deseado.";

	$lang['passwordFailed'] 		= "Disculpe pero el usuario no existe, ingrese nuevamente la dirección de correo electrónico.";

	$lang['photoProfiles']			= "Perfiles Fotográficos";

	$lang['videoProfiles']			= "Perfiles de Video";

	$lang['otherProfiles']			= "Otros perfiles";

	$lang['saving']					= "Guardando";

	$lang['myAccount']				= "Mi cuenta";

	$lang['myGalleries']			= "Mis galerias";

	$lang['noMediaAlbum']			= "No hay archivo que mostrar.";

	$lang['editDetails']			= "Edite detalles";

	$lang['approvalStatus0']		= "Pendiente de aprobación";

	$lang['approvalStatus1']		= "Aprobado";

	$lang['approvalStatus2']		= "No aprobado";

	$lang['noDetailsMes']			= "No se proporcionaron más detalles!";	

	$lang['orphanedMedia']			= "medios huérfanos";

	$lang['lastBatch']				= "Último lote añadido";

	$lang['deleteAlbumMes']			= "Desea borrar este álbum y los archivos que contiene?";

	$lang['mailInMedia']			= "Enviar en un CD/DVD";

	$lang['deleteAlbum']			= "Borrar álbum";

	$lang['editAlbum']				= "Editar álbum";

	$lang['albumName']				= "Nombre del álbum ";

	$lang['makePublic']				= "Hacerlo Público";

	$lang['deleteMedia']			= "Borrar Archivo";

	$lang['deleteMediaMes']			= "¿Borrar los archivos seleccionados?";

	$lang['selectAll']				= "Selecionar todo";

	$lang['selectNone']				= "Deseleccionar";

	$lang['noImportFilesMessage']	= "¡No hay archivos para importar!";

	$lang['selectAlbumMes']			= "Seleccione el álbum que desea añadir el archivo.";

	$lang['selectGalleriesMes']		= "Seleccione la galería donde desea añadir el archivo.";

	$lang['chooseItemsMes']			= "Elija los artículos para vender en la lista siguiente.";

	$lang['ablum']					= "Album";

	$lang['pricing']				= "Precios";

	$lang['px']						= "px";	

	$lang['noSales']				= "No tiene ventas.";

	$lang['itemUpper']				= "ARTICULO";

	$lang['commissionUpper']		= "COMISION";

	$lang['addUpper']				= "AÑADIR";

	$lang['cmDeleteVerify']			= "¿Esta seguro que desea borrar el archivo?";

	$lang['waitingForImport']		= "Esperando archivo para importar";

	$lang['importSelectedUpper']	= "IMPORTAR SELECCIONADAS";

	$lang['addMediaDetails']		= "Añadir detalles de archivo";

	$lang['uploadUpper']			= "SUBIR";

	$lang['noBioMessage']			= "Actualmente no existe historial sobre ese usuario.";

	$lang['collections']			= "Colecciones";

	$lang['uploadMediaUpper']		= "SUBIR NUEVO ARCHIVO";

	$lang['startUpper']				= "INICIO";



# 4.0.9

	$lang['displayName']			= "Mostrar nombre";

	$lang['newAlbum']				= "Nuevo álbum";

	$lang['albums']					= "Albumes";

	$lang['viewAllMedia']			= "VER TODOS LOS ARCHIVOS";

	$lang['signUpNow']				= "REGISTRARSE";



/*Spanish*/

$lang['exactMatch'] = "Exacta";

$lang['orderNumUpper'] = "PEDIDO";

$lang['orderDateUpper'] = "FECHA DE LA ORDEN";

$lang['paymentUpper'] = "PAGO";

$lang['dateRange'] = "Rango de fechas";

$lang['resolution'] = "Resolución";

$lang['continueShopUpper'] = "SEGUIR COMPRANDO";

$lang['votes'] = "votos";

$lang['moreNews'] = "más noticias";

$lang['currentSearch'] = "Búsqueda actual";

$lang['dates'] = "Fechas";

$lang['licenseType'] = "Tipo de licencia";

$lang['searchUpper'] = "BUSCAR";

$lang['from'] = "De";

$lang['to'] = "Para";

$lang['lightboxUpper'] = "PRESELECCIÓN";

$lang['itemsUpper'] = "ARTÍCULOS";

$lang['createdUpper'] = "CREADO";

$lang['na'] = "N/D";

$lang['galSortCDate'] = "Fecha de creación";

$lang['digitalDownloads'] = "Descargas digitales";

$lang['copyright'] = "Copyright © 2012";

$lang['reserved'] = "Todos los derechos reservados.";

$lang['days'] = "Días";

$lang['weeks'] = "Semanas";

$lang['months'] = "Meses";

$lang['years'] = "Años";

$lang['weekly'] = "Semanal";

$lang['monthly'] = "Mensual";

$lang['quarterly'] = "Trimestral";

$lang['semi-annually'] = "Semestral";

$lang['annually'] = "Anualmente";

$lang['guest'] = "Invitado";

$lang['login'] = "Iniciar sesión";

$lang['loginCaps'] = "INICIAR SESION";

$lang['loginMessage'] = "Introduzca su dirección de correo electrónico y contraseña para iniciar sesión.";

$lang['loggedOutMessage'] = "Usted ha sido desconectado.";

$lang['loginFailedMessage'] = "Error de acceso: Su correo electrónico o la contraseña son incorrectos.";

$lang['accountActivated'] = "Tu cuenta ha sido activada.";

$lang['activationEmail'] = "Le hemos enviado un mail de verificación. Por favor, use el enlace que recibirá para activar su cuenta.";

$lang['loginAccountClosed'] = "Esta cuenta está cerrada o inactiva.";

$lang['loginPending'] = "Esta cuenta no está verificada. Debe ser verificada antes de poder iniciar la sesión.";

$lang['yesLogin'] = "Sí, me gustaría iniciar la sesión.";

$lang['noCreateAccount'] = "No, me gustaría crear una cuenta.";

$lang['haveAccountQuestion'] = "¿Tiene usted una cuenta con nosotros?";

$lang['collections'] = "Colecciones digitales";

$lang['featuredCollections'] = "Destacados colecciones digitales";

$lang['similarMedia'] = "Medios similares";

$lang['paypal'] = "PayPal";

$lang['checkMO'] = "Comprobar Transferencia";

$lang['paypalEmail'] = "Correo electrónico de PayPal";

$lang['paid'] = "Pagado";

$lang['processing'] = "Tratamiento";

$lang['unpaid'] = "No pagado";

$lang['refunded'] = "Le devolvemos su dinero";

$lang['failed'] = "Falló";

$lang['cancelled'] = "Cancelado";

$lang['approved'] = "Aprobado";

$lang['incomplete'] = "Incompleto";

$lang['billLater'] = "Pagar luego";

$lang['expired'] = "Expirado";

$lang['unlimited'] = "Ilimitado";

$lang['quantityAvailable'] = "Cantidad disponible";

$lang['shipped'] = "Enviado";

$lang['notShipped'] = "No Enviado";

$lang['backordered'] = "Pedido pendiente";

$lang['taxIncMessage'] = "IVA Incluido";

$lang['addTag'] = "Añadir etiqueta";

$lang['memberTags'] = "Etiquetas miembros";

$lang['comments'] = "Comentarios";

$lang['showMoreComments'] = "Mostrar todos los comentarios";

$lang['noComments'] = "No hay comentarios aprobados.";

$lang['noTags'] = "No hay etiquetas aprobadas.";

$lang['addNewComment'] = "Añadir nuevo comentario";

$lang['commentPosted'] = "Tu comentario ha sido publicado.";

$lang['commentPending'] = "Su nuevo comentario ha sido presentado y será aprobado en breve.";

$lang['commentError'] = "Hubo un error y tu comentario no se presentó.";

$lang['tagPosted'] = "La etiqueta ha sido publicada.";

$lang['tagPending'] = "Su nueva etiqueta ha sido presentada y será aprobada en breve.";

$lang['tagError'] = "Hubo un error, y la etiqueta no se presentó.";

$lang['tagDuplicate'] = "Esta etiqueta ya existe.";

$lang['tagNotAccepted'] = "La etiqueta no fue aceptada por nuestro sistema.";

$lang['preferredLang'] = "Idioma preferido";

$lang['dateTime'] = "Fecha / Hora";

$lang['preferredCurrency'] = "Moneda preferida";

$lang['longDate'] = "Fecha larga";

$lang['shortDate'] = "Fecha corta";

$lang['numbDate'] = "Número Fecha";

$lang['daylightSavings'] = "Horario de verano";

$lang['dateFormat'] = "Formato de fecha";

$lang['timeZone'] = "Franja horario";

$lang['dateDisplay'] = "Mostrar fecha";

$lang['clockFormat'] = "Formato de Reloj";

$lang['numberDateSep'] = "Número Separador de fecha";

$lang['renew'] = "RENOVAR";

$lang['noOrders'] = "No hay pedidos en esta cuenta.";

$lang['noDownloads'] = "No se han hecho descargas en esta cuenta.";

$lang['noFeatured'] = "No hay artículos destacados en esta área.";

$lang['promotions'] = "Promociones";

$lang['noPromotions'] = "No hay cupones o promociones en este momento.";

$lang['autoApply'] = "Se aplica automáticamente en la caja.";

$lang['useCoupon'] = "Utilice el siguiente código en la caja o haga clic en el botón Aplicar para utilizar este cupón / promoción";

$lang['couponApplied'] = "Un cupón o descuento se ha aplicado a su carrito.";

$lang['couponFailed'] = "Cupón de descuento no válido.";

$lang['couponNeedsLogin'] = "Debes estar registrado para utilizar este cupón o descuento.";

$lang['couponMinumumWarn'] = "El subtotal no cumple con el requisito de utilizar este cupón o descuento.";

$lang['couponAlreadyUsed'] = "Sólo puede utilizar este cupón o descuento una vez.";

$lang['couponLoginWarn'] = "Debes estar registrado para utilizar este cupón o descuento.";

$lang['checkout'] = "PEDIDO";

$lang['continue'] = "CONTINUAR";

$lang['shipTo'] = "Enviamos";

$lang['billTo'] = "Facturar a";

$lang['galleries'] = "Galerías";

$lang['chooseGallery'] = "Elegir una galería de abajo para ver su contenido.";

$lang['galleryLogin'] = "Contraseña para galería";

$lang['galleryWrongPass'] = "La contraseña que ha introducido para esta galería era incorrecta.";

$lang['newestMedia'] = "Nuevos Medios";

$lang['popularMedia'] = "Fotografias populares";

$lang['contributors'] = "Colaboradores";

$lang['contUploadNewMedia'] = "Sube los nuevos medios";

$lang['contViewSales'] = "Ver Ventas";

$lang['contPortfolio'] = "Mi carpeta";

$lang['contGalleries'] = "Mis galerías";

$lang['contMedia'] = "Mis medios";

$lang['aboutUs'] = "Acerca de nosotros";

$lang['news'] = "Noticia";

$lang['noNews'] = "No hay noticias en este momento.";

$lang['termsOfUse'] = "Condiciones de uso";

$lang['privacyPolicy'] = "Política de privacidad";

$lang['purchaseAgreement'] = "Acuerdo de compra";

$lang['iAgree'] = "Estoy de acuerdo con la";

$lang['createAccount'] = "Crear una cuenta";

$lang['createAccountMessage'] = "Por favor, rellene el siguiente formulario para crear una nueva cuenta.";

$lang['contactUs'] = "Contacte con nosotros";

$lang['contactMessage'] = "Gracias por contactar con nosotros. Le responderemos en breve.";

$lang['contactError'] = "El formulario de contacto no fue correctamente cumplimentados. Su mensaje no fue enviado.";

$lang['contactIntro'] = "Por favor, use el siguiente formulario para contactar con nosotros. Le responderemos tan pronto como sea posible.";

$lang['contactEmailSubject'] = "Pregunta de Formulario de Contacto";

$lang['contactFromName'] = "Formulario de Contacto";

$lang['prints'] = "Láminas";

$lang['featuredPrints'] = "Impresiones destacadas";

$lang['newBill'] = "Una nueva factura se ha creado para esta membresía. Esta membresía no se activa o renovar hasta que la factura sea pagada. Las cuentas pendientes de las cuotas de afiliación han sido cancelados.";

$lang['accountInfo'] = "Información de la cuenta";

$lang['accountUpdated'] = "La información de su cuenta ha sido actualizada.";

$lang['editAccountInfo'] = "Editar su información de la cuenta";

$lang['editAccountInfoMes'] = "Editar la información de su cuenta y haga clic en el botón Save para guardar los cambios.";

$lang['accountInfoError1'] = "Contraseña y Verificar contraseña no coinciden!";

$lang['accountInfoError5'] = "La contraseña actual es incorrecta!";

$lang['accountInfoError2'] = "Su contraseña debe tener al menos 6 caracteres!";

$lang['accountInfoError12'] = "Esta dirección de correo electrónico ya está en uso!";

$lang['accountInfoError13'] = "Tu email no se ha aceptado!";

$lang['accountInfoError3'] = "Introduzca las palabras de arriba!";

$lang['accountInfoError4'] = "Captcha palabras no son correctos!";

$lang['readAgree'] = "He leído la";

$lang['agreements'] = "Acuerdos";

$lang['poweredBy'] = "Diseñado por";

$lang['captchaAudio'] = "Introduce los números que oyes";

$lang['captchaIncorrect'] = "Incorrecto, por favor vuelve a intentarlo";

$lang['captchaPlayAgain'] = "Reproduce de nuevo";

$lang['captchaDownloadMP3'] = "Descargar en formato MP3";

$lang['captcha'] = "Captcha";

$lang['subHeaderID'] = "Identificación";

$lang['subHeaderSubscript'] = "SUSCRIPCIÓN";

$lang['subHeaderExpires'] = "VENCE";

$lang['subHeaderDPD'] = "Descargas al día / restante";

$lang['subHeaderStatus'] = "ESTADO";

$lang['downloads'] = "Descargas";

$lang['thankRequest'] = "Gracias por su solicitud. Nos pondremos en contacto contigo en breve.";

$lang['pleaseContactForm'] = "Por favor, póngase en contacto con nosotros para el precio de este archivo rellenando los datos del siguiente formulario";

$lang['downWithSubscription'] = "Descargar uso de suscripción";

$lang['noInstantDownload'] = "Este archivo no está disponible para descarga inmediata. Será entregado por el administrador del sitio a través de correo electrónico cuando se comprueve la compra.";

$lang['newTicketsMessage'] = "Tiquets nuevos o actualizados";

$lang['emptyTicket'] = "Esta entrada está vacía.";

$lang['messageID'] = "Codigo de mensaje";

$lang['ticketNoReplies'] = "Esta entrada está cerrada y no puede aceptar nuevas respuestas.";

$lang['ticketUpdated'] = "Esta entrada ha sido actualizado!";

$lang['ticketClosed'] = "Esta entrada ha sido cerrada!";

$lang['closeTicket'] = "Cerrar entrada";

$lang['newTicket'] = "Nuevo Ticket";

$lang['newTicketButton'] = "NUEVO TICKET";

$lang['ticketUnreadMes'] = "Tiene mensajes sin leer";

$lang['ticketUnderAccount'] = "No hay entradas de ayuda previstos en esta cuenta";

$lang['ticketSubmitted'] = "Su solicitud de ayuda ha ingresado a nuestro sistema. Le responderemos a la brevedad.";

$lang['mediaNotElidgiblePack'] = "Este archivo no es compatible con el pack!";

$lang['noBills'] = "No hay pedidos en esta cuenta.";

$lang['lightbox'] = "Preselecciones";

$lang['noLightboxes'] = "Usted no tiene preselecciones.";

$lang['lightboxDeleted'] = "La preselección ha sido eliminada.";

$lang['lbDeleteVerify'] = "¿Seguro que quiere borrar esta preselección?";

$lang['newLightbox'] = "NUEVA PRESELECCION";

$lang['lightboxCreated'] = "Su preselección se ha creado.";

$lang['addToLightbox'] = "Agregar a la preselección";

$lang['createNewLightbox'] = "Crear una preselección nueva";

$lang['editLightbox'] = "Editar esta preselección";

$lang['addNotes'] = "Añadir notas";

$lang['editLightboxItem'] = "Editar el artículo de la preselección";

$lang['removeFromLightbox'] = "QUITAR DE LA PRESELECCION";

$lang['savedChangesMessage'] = "Sus cambios han sido guardados.";

$lang['noSubs'] = "No hay suscripciones cargo a esta cuenta.";

$lang['unpaidBills'] = "facturas pendientes de pago";

$lang['notices'] = "Avisos";

$lang['subscription'] = "Suscripción";

$lang['assignToPackage'] = "Asignar a pack";

$lang['startNewPackage'] = "Comience un nuevo pack";

$lang['packagesInCart'] = "Los pack en su carro";

$lang['id'] = "Identificación";

$lang['summary'] = "Resumen";

$lang['status'] = "Estado";

$lang['lastUpdated'] = "Última actualización";

$lang['opened'] = "Abierto";

$lang['reply'] = "Responder";

$lang['required'] = "Necesario";

$lang['bills'] = "Facturas";

$lang['orders'] = "Órdenes";

$lang['downloadHistory'] = "Historial de descargas";

$lang['supportTickets'] = "Tickets de Soporte";

$lang['order'] = "Orden";

$lang['packages'] = "Packs";

$lang['featuredPackages'] = "Packs destacados";

$lang['products'] = "Productos";

$lang['featuredProducts'] = "Productos destacados";

$lang['subscriptions'] = "Suscripciones";

$lang['featuredSubscriptions'] = "Suscripciones destacados";

$lang['yourCredits'] = "SU <br /> CRÉDITOS";

$lang['featuredCredits'] = "Packs de créditos destacados";

$lang['media'] = "ARCHIVOS";

$lang['mediaNav'] = "Archivos";

$lang['featuredMedia'] = "Archivos destacados";

$lang['featuredItems'] = "Destacados";

$lang['showcasedContributors'] = "Colaboradores exhibidos";

$lang['forum'] = "Foro";

$lang['randomMedia'] = "Archivos al azar";

$lang['language'] = "Idioma";

$lang['priceCreditSep'] = "o";

$lang['viewCollection'] = "Ver la colección";

$lang['loggedInAs'] = "Usted está registrado como";

$lang['editProfile'] = "Editar cuenta";

$lang['noItemCartWarning'] = "Tienes que seleccionar una foto antes de poder agregar este artículo a su carrito!";

$lang['cartTotalListWarning'] = "Estos valores son sólo estimaciones y pueden cambiar ligeramente debido a la fluctuación de la moneda y el redondeo.";

$lang['applyCouponCode'] = "Aplicar código de cupón";

$lang['billMeLater'] = "Facturar mas tarde";

$lang['billMeLaterDescription'] = "Vamos a factura mensualmente sus compras.";

$lang['shippingOptions'] = "Opciones de envío";

$lang['paymentOptions'] = "Opciones de pago";

$lang['chooseCountryFirst'] = "Elige país";

$lang['paymentCancelled'] = "Su pago ha sido cancelado. Si quieres puedes probar el pago de nuevo.";

$lang['paymentDeclined'] = "Su pago ha sido rechazada. Si quieres puedes probar el pago de nuevo.";

$lang['generalInfo'] = "Información personal";

$lang['membership'] = "Afiliación";

$lang['preferences'] = "Preferencias";

$lang['address'] = "Dirección";

$lang['actions'] = "Acciones";

$lang['changePass'] = "Cambiar la contraseña";

$lang['changeAvatar'] = "Cambiar avatar";

$lang['bio'] = "Historia";

$lang['contributorSettings'] = "Configuración de colaborador";

$lang['edit'] = "EDITAR";

$lang['leftToFill'] = "Artículos de izquierda a llenar";

$lang['commissionMethod'] = "Comisión método de pago";

$lang['commission'] = "Comisión";

$lang['signupDate'] = "Miembro desde";

$lang['lastLogin'] = "Último inicio de sesión";

$lang['clientName'] = "Nombre del cliente";

$lang['eventCode'] = "Código del evento";

$lang['eventDate'] = "Fecha del evento";

$lang['eventLocation'] = "Lugar del evento";

$lang['viewPackOptions'] = "Ver el contenido del pack y opciones";

$lang['viewOptions'] = "Ver Opciones";

$lang['remove'] = "QUITAR";

$lang['productShots'] = "Fotos de Producto";

$lang['currentPass'] = "Contraseña actual";

$lang['newPass'] = "Nueva contraseña";

$lang['vNewPass'] = "Verificar la nueva contraseña";

$lang['verifyPass'] = "Verifique su contraseña";

$lang['firstName'] = "Nombre";

$lang['lastName'] = "Apellido";

$lang['location'] = "Ubicación";

$lang['memberSince'] = "Miembro desde";

$lang['portfolio'] = "CARPETA";

$lang['profile'] = "Perfil";

$lang['city'] = "Ciudad";

$lang['state'] = "Estado / Provincia";

$lang['zip'] = "Postal / Código Postal";

$lang['country'] = "País";

$lang['save'] = "GUARDAR";

$lang['add'] = "ADD";

$lang['companyName'] = "Nombre de la empresa";

$lang['accountStatus'] = "Estado de la cuenta";

$lang['website'] = "Sitio web";

$lang['phone'] = "Teléfono";

$lang['email'] = "E-mail";

$lang['name'] = "Nombre";

$lang['submit'] = "ENVIAR";

$lang['message'] = "Mensaje";

$lang['question'] = "Pregunta";

$lang['password'] = "Contraseña";

$lang['members'] = "Miembros";

$lang['visits'] = "Visitas";

$lang['logout'] = "Cerrar sesión";

$lang['lightboxes'] = "Mesas de luz";

$lang['cart'] = "Su carro";

$lang['cartItemAdded'] = "El artículo ha sido añadido a su carrito.";

$lang['includesTax'] = "Incluye IVA / IVA";

$lang['addToCart'] = "Añadir al Carrito";

$lang['items'] = "Artículos";

$lang['item'] = "Artículo";

$lang['qty'] = "Cantidad";

$lang['price'] = "Precio";

$lang['more'] = "Más";

$lang['moreInfo'] = "Más información";

$lang['back'] = "Atrás";

$lang['none'] = "Ninguno";

$lang['details'] = "Detalles";

$lang['options'] = "Opciones";

$lang['page'] = "Página";

$lang['ratingSubmitted'] = "Valoración Enviado";

$lang['noMedia'] = "Esta galería está vacía";

$lang['itemsTotal'] = "total de artículos";

$lang['of'] = "de";

$lang['view'] = "VER";

$lang['apply'] = "APLICA";

$lang['currency'] = "cambiar moneda";

$lang['active'] = "Activo";

$lang['pending'] = "Pendiente";

$lang['freeTrial'] = "Prueba gratuita";

$lang['setupFee'] = "Configuración de Tarifa";

$lang['free'] = "Libre";

$lang['open'] = "Abierto";

$lang['close'] = "Cerrar";

$lang['closed'] = "Cerrado";

$lang['by'] = "Por";

$lang['download'] = "Descargar";

$lang['downloadUpper'] = "DESCARGAR";

$lang['KB'] = "KB";

$lang['MB'] = "MB";

$lang['files'] = "Archivos";

$lang['unknown'] = "Desconocido";

$lang['freeDownload'] = "Descargar Gratis";

$lang['prevDown'] = "Descarga anterior";

$lang['pay'] = "PAGAR";

$lang['purchaseCredits'] = "Comprar créditos";

$lang['purchaseSub'] = "Adquiera una suscripción";

$lang['hour'] = "Hora";

$lang['slash'] = "Tala";

$lang['period'] = "Período";

$lang['dash'] = "Guión";

$lang['gmt'] = "GMT";

$lang['avatar'] = "Avatar";

$lang['delete'] = "ELIMINAR";

$lang['uploadAvatar'] = "Subir Avatar";

$lang['welcome'] = "Bienvenido";

$lang['expires'] = "Expira";

$lang['msExpired'] = "Las siguientes cuentas han caducado";

$lang['newSales'] = "nuevas ventas desde la última vez conectados";

$lang['never'] = "Nunca";

$lang['yes'] = "SI";

$lang['no'] = "NO";

$lang['create'] = "CREAR";

$lang['cancel'] = "CANCELAR";

$lang['notes'] = "Notas";

$lang['update'] = "ACTUALIZACIÓN";

$lang['each'] = "cada";

$lang['enterKeywords'] = "Introduzca palabra/s clave";

$lang['send'] = "ENVIAR";

$lang['emailTo'] = "Enviar por correo electrónico a ";

$lang['yourName'] = "Su Nombre";

$lang['yourEmail'] = "Su correo electrónico";

$lang['emailToFriend'] = "Vínculo de correo electrónico a este archivo";

$lang['emailToFriendSent'] = "Un correo electrónico ha sido enviado! Usted puede enviar otro, o cerrar esta ventana.";

$lang['linkSentBy'] = "Un enlace a una foto o vídeo ha sido enviado a usted por";

$lang['newPackageMessage'] = "Comience un nuevo paquete de abajo o seleccione un paquete similar ya en su cesta";

$lang['warning'] = "¡Advertencia!";

$lang['estimated'] = "Estimado";

$lang['doneUpper'] = "HECHO";

$lang['returnToSiteUpper'] = "VOLVER AL SITIO";

$lang['advancedSearch'] = "Búsqueda Avanzada";

$lang['eventSearch'] = "Buscar Evento";

$lang['AND'] = "Y";

$lang['OR'] = "O";

$lang['NOT'] = "NO";

$lang['noAccess'] = "Usted no tiene permisos para ver esta galeria.";

$lang['siteStats'] = "Estadísticas del sitio";

$lang['membersOnline'] = "Usuarios en línea";

$lang['minutesAgo'] = "minutos";

$lang['seconds'] = "Segundos";

$lang['megabytesAbv'] = "MB";

$lang['downWithSub'] = "Descargar uso de suscripción";

$lang['downloadsRemaining'] = "Descargas restantes";

$lang['requestDownloadSuccess'] = "Hemos recibido su solicitud para el archivo y te contactaremos a la brevedad.";

$lang['mediaLicenseNFS'] = "No para la venta";

$lang['mediaLicenseRF'] = "Libres";

$lang['mediaLicenseRM'] = "Derechos protegidos";

$lang['mediaLicenseFR'] = "Libre";

$lang['mediaLicenseCU'] = "Contacte con nosotros";

$lang['original'] = "Original";

$lang['prevDownloaded'] = "Usted ha descargado este archivo. No hay ningún cargo para descargarlo de nuevo.";

$lang['instantDownload'] = "Descarga inmediata";

$lang['requestDownload'] = "Solicitud de descarga";

$lang['request'] = "SOLICITAR";

$lang['enterEmail'] = "Ingrese su e-mail";

$lang['license'] = "Licencia";

$lang['inches'] = "Pulgadas";

$lang['centimeters'] = "Centímetros";

$lang['dpi'] = "dpi";

$lang['noSimilarMediaMessage'] = "No se encontraron archivos similares.";

$lang['backUpper'] = "VOLVER";

$lang['nextUpper'] = "SIGUIENTE";

$lang['prevUpper'] = "ANTERIOR";

$lang['curGalleryOnly'] = "Solo galería actual";

$lang['sortBy'] = "Ordenado por";

$lang['galSortColor'] = "Color";

$lang['galSortDate'] = "Fecha alta";

$lang['galSortID'] = "Identificación";

$lang['galSortTitle'] = "Título";

$lang['galSortFilename'] = "Nombre de archivo";

$lang['galSortFilesize'] = "Tamaño del archivo";

$lang['galSortSortNum'] = "Ordenar número";

$lang['galSortBatchID'] = "ID del lote";

$lang['galSortFeatured'] = "Destacados";

$lang['galSortWidth'] = "Ancho";

$lang['galSortHeight'] = "Altura";

$lang['galSortViews'] = "Vistas";

$lang['galSortAsc'] = "Ascendente";

$lang['galSortDesc'] = "Descendente";

$lang['mediaIncludedInColl'] = "Este archivo se incluye en las siguientes colecciones.";

$lang['billHeaderInvoice'] = "FACTURA";

$lang['billHeaderDate'] = "FECHA DE FACTURA";

$lang['billHeaderDueDate'] = "FECHA DE VENCIMIENTO";

$lang['billHeaderTotal'] = "Total";

$lang['billHeaderStatus'] = "ESTADO";

$lang['creditsWarning'] = "Usted no tiene suficientes créditos. Por favor, ingresa o añade créditos a su cuenta.";

$lang['subtotalWarning'] = "La compra mínima es:";

$lang['pleaseWait'] = "Por favor, espere mientras procesamos su solicitud!";

$lang['billMailinThanks'] = "Por favor, envíe su cheque o giro postal por la cantidad siguiente a la dirección indicada. Una vez que se reciba el pago se le podrá descargar.";

$lang['mailinRef'] = "Por favor, consulte el siguiente número con su pago";

$lang['subtotal'] = "Total parcial";

$lang['shipping'] = "Envío";

$lang['discounts'] = "Descuentos / cupones";

$lang['total'] = "Total";

$lang['creditsSubtotal'] = "Créditos Subtotal";

$lang['creditsDiscounts'] = "Descuentos de crédito";

$lang['credits'] = "Créditos";

$lang['reviewOrder'] = "Revise su pedido";

$lang['payment'] = "Pago";

$lang['cartNoItems'] = "No tiene artículos en su carrito!";

$lang['enterShipInfo'] = "Por favor, introduzca sus datos de envío y la información de facturación a continuación";

$lang['sameAsShipping'] = "Igual que la dirección de envío";

$lang['differentAddress'] = "Proporcionar una dirección diferente";

$lang['noShipMethod'] = "No existen métodos de envío para enviar estos artículos.";

$lang['choosePaymentMethod'] = "Por favor, elija un método de pago";

$lang['discountCode'] = "Código de Descuento";

$lang['use'] = "Utilizar";

$lang['continueNoAccount'] = "o continuar sin una cuenta";

$lang['yourOrder'] = "Su pedido";

$lang['viewInvoice'] = "Ver factura";

$lang['totalsShown'] = "Los totales se muestra en la";

$lang['downloadExpired'] = "Descarga caducado";

$lang['downloadExpiredMes'] = "Este enlace ha expirado. Por favor, póngase en contacto con nosotros para que se reactive.";

$lang['downloadMax'] = "Ha superado el máximo de descargas";

$lang['downloadMaxMes'] = "Usted ha llegado al número máximo de descargas permitidas para este archivo comprado. Por favor, póngase en contacto con nosotros para tener el enlace de descarga reactivado.";

$lang['downloadNotApproved'] = "El pedido no ha sido aprobado";

$lang['downloadNotApprovedMes'] = "No se puede descargar este archivo hasta que su pedido haya sido aprobado. Por favor, intentalo más tarde.";

$lang['downloadNotAvail'] = "Descarga inmediata no disponible";

$lang['downloadNotAvailMes'] = "Este archivo no está disponible para descarga inmediata. Por favor, use el botón de solicitud de abajo para que este archivo se envie por correo electrónico.";

$lang['orderNumber'] = "Número de pedido";

$lang['orderPlaced'] = "Pedido realizado";

$lang['orderStatus'] = "Estado del pedido";

$lang['paymentStatus'] = "Comprobar el estado del";

$lang['iptc'] = "IPTC";

$lang['iptc_title'] = "Título";

$lang['iptc_description'] = "Descripción";

$lang['iptc_instructions'] = "Instrucciones";

$lang['iptc_date_created'] = "Fecha de creación";

$lang['iptc_author'] = "Autor";

$lang['iptc_city'] = "Ciudad";

$lang['iptc_state'] = "Estado";

$lang['iptc_country'] = "País";

$lang['iptc_job_identifier'] = "Identificador de trabajo";

$lang['iptc_headline'] = "Titular";

$lang['iptc_provider'] = "Línea de crédito";

$lang['iptc_source'] = "Fuente";

$lang['iptc_description_writer'] = "Descripción del escritor";

$lang['iptc_urgency'] = "Urgencia";

$lang['iptc_copyright_notice'] = "Aviso de Copyright";

$lang['exif'] = "EXIF";

$lang['exif_FileName'] = "Nombre de archivo";

$lang['exif_FileDateTime'] = "Archivo Fecha / Hora";

$lang['exif_FileSize'] = "Tamaño del archivo";

$lang['exif_FileType'] = "Tipo de Archivo";

$lang['exif_MimeType'] = "Tipo mudo";

$lang['exif_SectionsFound'] = "Secciones encontradas";

$lang['exif_ImageDescription'] = "Descripción de la imagen";

$lang['exif_Make'] = "Hacer";

$lang['exif_Model'] = "Modelo";

$lang['exif_Orientation'] = "Orientación";

$lang['exif_XResolution'] = "Resolución horizontal";

$lang['exif_YResolution'] = "Resolución vertical";

$lang['exif_ResolutionUnit'] = "Resolución de la Unidad";

$lang['exif_Software'] = "Software";

$lang['exif_DateTime'] = "Fecha / Hora";

$lang['exif_YCbCrPositioning'] = "Posicionamiento YCbCr";

$lang['exif_Exif_IFD_Pointer'] = "Exif IFD Pointer";

$lang['exif_GPS_IFD_Pointer'] = "GPS IFD puntero";

$lang['exif_ExposureTime'] = "Tiempo de exposición";

$lang['exif_FNumber'] = "Apertura Diafragma, F";

$lang['exif_ExposureProgram'] = "La exposición del Programa";

$lang['exif_ISOSpeedRatings'] = "ISO";

$lang['exif_ExifVersion'] = "Exif";

$lang['exif_DateTimeOriginal'] = "Fecha / hora original";

$lang['exif_DateTimeDigitized'] = "Fecha / Hora Digitalizada";

$lang['exif_ComponentsConfiguration'] = "Componentes de configuración";

$lang['exif_ShutterSpeedValue'] = "Velocidad de Obturador";

$lang['exif_ApertureValue'] = "Valor de Apertura";

$lang['exif_MeteringMode'] = "Modo de medición";

$lang['exif_Flash'] = "Flash";

$lang['exif_FocalLength'] = "Longitud focal";

$lang['exif_FlashPixVersion'] = "Versión de Flash Pix";

$lang['exif_ColorSpace'] = "Espacio de color";

$lang['exif_ExifImageWidth'] = "Ancho de imagen Exif";

$lang['exif_ExifImageLength'] = "Exif longitud de la imagen";

$lang['exif_SensingMethod'] = "Método de detección";

$lang['exif_ExposureMode'] = "Modo de exposición";

$lang['exif_WhiteBalance'] = "Balance de blancos";

$lang['exif_SceneCaptureType'] = "Escena tipo de captura";

$lang['exif_Sharpness'] = "Nitidez";

$lang['exif_GPSLatitudeRef'] = "GPS Latitud Ref.";

$lang['exif_GPSLatitude_0'] = "GPSLatitude 0";

$lang['exif_GPSLatitude_1'] = "GPSLatitude 1";

$lang['exif_GPSLatitude_2'] = "GPSLatitude 2";

$lang['exif_GPSLongitudeRef'] = "GPS Ref. Longitud";

$lang['exif_GPSLongitude_0'] = "GPS Longitud 0";

$lang['exif_GPSLongitude_1'] = "GPS Longitud 1";

$lang['exif_GPSLongitude_2'] = "GPS Longitud 2";

$lang['exif_GPSTimeStamp_0'] = "Fecha y hora GPS 0";

$lang['exif_GPSTimeStamp_1'] = "Fecha y hora GPS 1";

$lang['exif_GPSTimeStamp_2'] = "Fecha y hora GPS 2";

$lang['exif_GPSImgDirectionRef'] = "GPS Imgage dirección de referencia";

$lang['exif_GPSImgDirection'] = "GPS Imgage Dirección";

$lang['mediaLabelTitle'] = "Título";

$lang['mediaLabelDesc'] = "Descripción";

$lang['mediaLabelCopyright'] = "Derechos de autor";

$lang['mediaLabelRestrictions'] = "Restricciones de uso";

$lang['mediaLabelRelease'] = "Modelo de lanzamiento";

$lang['mediaLabelKeys'] = "Palabras clave";

$lang['mediaLabelFilename'] = "Nombre de archivo";

$lang['mediaLabelID'] = "Identificación";

$lang['mediaLabelDate'] = "Añadido";

$lang['mediaLabelDateC'] = "Creado";

$lang['mediaLabelDownloads'] = "Descargas";

$lang['mediaLabelViews'] = "Vistas";

$lang['mediaLabelPurchases'] = "Compras";

$lang['mediaLabelColors'] = "Colores";

$lang['mediaLabelPrice'] = "Precio";

$lang['mediaLabelCredits'] = "Créditos";

$lang['mediaLabelOwner'] = "Dueño";

$lang['mediaLabelUnknown'] = "Desconocido";

$lang['mediaLabelResolution'] = "Píxeles";

$lang['mediaLabelFilesize'] = "Tamaño del archivo";

$lang['mediaLabelRunningTime'] = "Duración";

$lang['mediaLabelFPS'] = "FPS";

$lang['mediaLabelFormat'] = "Formato";

$lang['search'] = "Buscar";

$lang['searchEnterKeyMessage'] = "Por favor, introduzca las palabras clave en el campo de búsqueda para comenzar su búsqueda.";

$lang['searchNoResults'] = "Su búsqueda no ha arrojado ningún resultado";

$lang['searchDateRange'] = "Buscar en entorno de fechas";

$lang['searchNarrow'] = "Limitar resultados de la búsqueda";

$lang['searchStart'] = "Comience su búsqueda de abajo!";

$lang['searchAll'] = "Todo";

$lang['searchClear'] = "Limpiar búsqueda";

$lang['searchKeywords'] = "Palabras clave";

$lang['searchTitle'] = "Título";

$lang['searchDescription'] = "Descripción";

$lang['searchFilename'] = "Nombre de archivo";

$lang['searchPortrait'] = "Retrato";

$lang['searchLandscape'] = "Paisaje";

$lang['searchSquare'] = "Plaza";

$lang['searchRoyaltyFree'] = "Libres";

$lang['searchRightsManaged'] = "Derechos Protegidos";

$lang['searchFree'] = "Libre";

$lang['searchContactUs'] = "Contacte con nosotros";

$lang['searchHeaderKeywords'] = "Palabras clave";

$lang['searchHeaderFields'] = "Campos";

$lang['searchHeaderType'] = "Tipo";

$lang['searchHeaderOrientation'] = "Orientación";

$lang['searchHeaderColor'] = "Color";

$lang['searchHeaderGalleries'] = "Galerías";

?>