<?php
	# 4.6.3
	# STRIPE
	$lang['stripe_displayName']			= "Stripe";
	$lang['stripe_publicDescription']	= "Credit Card or Debit Card";
	$lang['stripe_pkey']				= "Publishable Key";
	$lang['stripe_pkey_d']				= "Your publishable key for your stripe account.";
	$lang['stripe_skey']				= "Secret Key";
	$lang['stripe_skey_d']				= "Your secret key for your stripe account.";
	
	# 4.1.4
	# MOLLIE IDEAL
	$lang['mollieideal_displayName']		= "iDeal (Mollie)";
	$lang['mollieideal_f_partnerid']		= "Partner ID";
	$lang['mollieideal_f_partnerid_d']		= "Your iDeal Partner ID.";
	$lang['mollieideal_f_profilekey']		= "Profile key";
	$lang['mollieideal_f_profilekey_d']		= "Your Mollie iDeal Profile key.";
	$lang['mollieideal_f_testmode']			= "Modo Prueva";
	$lang['mollieideal_f_testmode_d']		= "Pasar a modo de prueva la pasarela de pago Mollie iDeal.";
	$lang['mollieideal_instructions']		= "Asegurese que los datos del modo de prueva corresponde a su cuenta Mollie.nl";
	$lang['mollieideal_publicDescription']	= "Pagos seguros usando iDeal (Solo disponible en Holanda).";

	# 4.1
	# ONEBIP 
	$lang['onebip_displayName']		= "OneBip"; 
	$lang['onebip_merchantid']			= "Email";	
	$lang['onebip_merchantid_d']		= "Entre el email usado para tu cuenta onebip. "; 
	$lang['onebip_merchantkey']		= "API Key"; 
	$lang['onebip_merchantkey_d']		= "Entre la API key de su cuenta onebip.";
	$lang['onebip_testmode']			= "Modo Prueva";
	$lang['onebip_testmode_d']			= "Pasar a modo de prueva la pasarela de pago onebip.";
	$lang['onebip_publicDescription']	= "Pay by Mobile";
	
	# PAYFAST 
	$lang['payfast_displayName']		= "PayFast"; 
	$lang['payfast_merchantid']			= "Merchant ID";	
	$lang['payfast_merchantid_d']		= "Enter the merchant ID for your PayFast Account."; 
	$lang['payfast_merchantkey']		= "Merchant Key"; 
	$lang['payfast_merchantkey_d']		= "Enter the merchant key for your PayFast Account.";
	$lang['payfast_testmode']			= "Modo Prueva"; 
	$lang['payfast_testmode_d']			= "Pasar a modo de prueva la pasarela de pago PayFast.";
	$lang['payfast_publicDescription']	= "Credit Card or Bank Account"; 

/*Spanish*/
$lang['nochex_displayName'] = "Nochex";
$lang['nochex_f_accountid'] = "ID de la cuenta";
$lang['nochex_f_accountid_d'] = "Su Nochex ID de la cuenta.";
$lang['nochex_f_testmode'] = "Prueba el modo de";
$lang['nochex_f_testmode_d'] = "Pon tus pagos Nochex en modo de prueba.";
$lang['nochex_publicDescription'] = "De Crédito o Débito";
$lang['worldpay_displayName'] = "WorldPay";
$lang['worldpay_f_installid'] = "ID de instalación";
$lang['worldpay_f_installid_d'] = "ID de instalación de su cuenta WorldPay.";
$lang['worldpay_f_testmode'] = "Prueba el modo de";
$lang['worldpay_f_testmode_d'] = "Pon tus pagos WorldPay en modo de prueba.";
$lang['worldpay_publicDescription'] = "De Crédito o Débito";
$lang['robokassa_displayName'] = "RoboKassa";
$lang['robokassa_f_merchantid'] = "De identificación del comerciante";
$lang['robokassa_f_merchantid_d'] = "De identificación del comerciante para su cuenta RoboKassa.";
$lang['robokassa_f_merchantpass'] = "Mercante Pass";
$lang['robokassa_f_merchantpass_d'] = "Su comerciante RoboKassa pase de identificación.";
$lang['robokassa_publicDescription'] = "De Crédito o Débito";
$lang['paygate_displayName'] = "PayGate";
$lang['paygate_f_accountid'] = "PayGate ID";
$lang['paygate_f_accountid_d'] = "ID de su cuenta PayGate.";
$lang['paygate_f_accountkey'] = "PayGate clave";
$lang['paygate_f_accountkey_d'] = "El identificador de clave PayGate.";
$lang['paygate_f_testmode'] = "Prueba el modo de";
$lang['paygate_f_testmode_d'] = "Pon tus pagos PayGate en modo de prueba.";
$lang['paygate_f_testid'] = "Prueba de Identificación PayGate";
$lang['paygate_f_testid_d'] = "Tu ID de Prueba PayGate";
$lang['paygate_f_testkey'] = "Prueba PayGate clave";
$lang['paygate_f_testkey_d'] = "Su clave de prueba de PayGate";
$lang['paygate_publicDescription'] = "De Crédito o Débito";
$lang['paystation_displayName'] = "Cajero Automático";
$lang['paystation_f_accountid'] = "Paystation ID";
$lang['paystation_f_accountid_d'] = "ID de su cuenta de Cajero Automático.";
$lang['paystation_f_testmode'] = "Prueba el modo de";
$lang['paystation_f_testmode_d'] = "Pon tus pagos Cajero Automático en modo de prueba.";
$lang['paystation_f_gatewayid'] = "Puerta de enlace de ID";
$lang['paystation_f_gatewayid_d'] = "Su Paystation ID puerta de enlace.";
$lang['paystation_publicDescription'] = "De Crédito o Débito";
$lang['skrill_displayName'] = "Skrill (moneybookers)";
$lang['skrill_f_email'] = "Dirección de correo electrónico";
$lang['skrill_f_email_d'] = "Dirección de correo electrónico de su cuenta Skrill.";
$lang['skrill_f_testmode'] = "Prueba el modo de";
$lang['skrill_f_testmode_d'] = "Pon tus pagos Skrill en modo de prueba.";
$lang['skrill_f_testemail'] = "Prueba de E-mail";
$lang['skrill_f_testemail_d'] = "Su prueba Skrill dirección de correo electrónico.";
$lang['skrill_publicDescription'] = "De Crédito o Débito";
$lang['skrill_f_completeOrder'] = "Haz clic aquí para completar su pedido!";
$lang['chronopay_displayName'] = "ChronoPay";
$lang['chronopay_f_clientid'] = "Identificación del Cliente";
$lang['chronopay_f_clientid_d'] = "Su ChronoPay ID de cliente.";
$lang['chronopay_f_siteid'] = "Sitio ID";
$lang['chronopay_f_siteid_d'] = "Su ChronoPay ID del sitio.";
$lang['chronopay_f_productid'] = "ID del producto.";
$lang['chronopay_f_productid_d'] = "Su ChronoPay ID de producto.";
$lang['chronopay_publicDescription'] = "De Crédito o Débito";
$lang['ideal_displayName'] = "iDeal";
$lang['ideal_f_accountid'] = "ID de la cuenta";
$lang['ideal_f_accountid_d'] = "Su número de cuenta iDeal.";
$lang['ideal_f_transkey'] = "Clave Secreta";
$lang['ideal_f_transkey_d'] = "Su clave secreta iDeal.";
$lang['ideal_f_testmode'] = "Prueba el modo de";
$lang['ideal_f_testmode_d'] = "Pon tus pagos ideales en modo de prueba.";
$lang['ideal_publicDescription'] = "Cuenta bancaria";
$lang['paypal_displayName'] = "PayPal";
$lang['paypal_f_email'] = "Dirección de correo electrónico";
$lang['paypal_f_email_d'] = "Dirección de correo electrónico de su cuenta PayPal.";
$lang['paypal_f_testmode'] = "Prueba el modo de";
$lang['paypal_f_testmode_d'] = "Pon tus pagos de PayPal en modo de prueba.";
$lang['paypal_f_testemail'] = "Prueba de E-mail";
$lang['paypal_f_testemail_d'] = "Su caja de arena de pruebas de PayPal dirección de correo electrónico.";
$lang['paypal_publicDescription'] = "PayPal, tarjeta de crédito o cuenta bancaria";
$lang['2checkout_displayName'] = "2Checkout.com";
$lang['2checkout_f_accountid'] = "ID de la cuenta";
$lang['2checkout_f_accountid_d'] = "Tu ID de la cuenta de 2Checkout.";
$lang['2checkout_f_testmode'] = "Prueba el modo de";
$lang['2checkout_f_testmode_d'] = "Pon tus pagos 2Checkout en modo de prueba.";
$lang['2checkout_instructions']		= "In 2Checkout.com set direct return to 'Header Redirect (Your URL)' and set approved URL to http://www.YOUR_DOMAIN_NAME.com/assets/gateways/2checkout/ipn.php";
$lang['2checkout_publicDescription'] = "Tarjeta de crédito o cuenta bancaria de EE.UU.";
$lang['plugnpay_displayName'] = "Pago Plug n &#39;";
$lang['plugnpay_f_accountid'] = "ID de la cuenta";
$lang['plugnpay_f_accountid_d'] = "Su ID Plug n &#39;Pay cuenta.";
$lang['plugnpay_publicDescription'] = "";
$lang['authorize_displayName'] = "Authorize.net";
$lang['authorize_f_apiid'] = "API de usuario ID";
$lang['authorize_f_apiid_d'] = "El API de Authorize.net usuario ID.";
$lang['authorize_f_transkey'] = "Operación clave";
$lang['authorize_f_transkey_d'] = "Su Authorize.net Llave de Transacción.";
$lang['authorize_f_testmode'] = "Prueba el modo de";
$lang['authorize_f_testmode_d'] = "Ponga sus pagos de Authorize.net en modo de prueba.";
$lang['authorize_publicDescription'] = "";
$lang['authorize_f_completeOrder'] = "Haz clic aquí para completar su pedido!";
$lang['mygate_displayName'] = "MyGate.co.za";
$lang['mygate_f_merchantid'] = "De identificación del comerciante";
$lang['mygate_f_merchantid_d'] = "Su MyGate.co.za identificación del comerciante.";
$lang['mygate_f_appid'] = "ID de aplicación";
$lang['mygate_f_appid_d'] = "Su MyGate.co.za ID de Aplicación.";
$lang['mygate_publicDescription'] = "";
$lang['mygate_f_testmode'] = "Prueba el modo de";
$lang['mygate_f_testmode_d'] = "Pon tus pagos mygate en modo de prueba.";
$lang['mailin_displayName'] = "Pago por trasnferencia bancaria"; //Correo en el pago
$lang['mailin_f_instructions'] = "Correo En Intrucciones de pago";
$lang['mailin_f_instructions_d'] = "Las instrucciones para los clientes enviar correo electrónico en los pagos, tales como cheques o giros postales.";
$lang['mailin_publicDescription'] = "Envíe su pago a la dirección proporcionada en la página siguiente.";
?>