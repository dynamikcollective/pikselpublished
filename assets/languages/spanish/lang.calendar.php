<?php
/*Spanish*/
$calendar = array();
$calendar['long_month'] = array(1 => "Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
$calendar['short_month'] = array(1 => "Ene","Feb","Mar","Abr","May","Jun","Jul","Ago","Sep","Oct","Nov","Dic");
$calendar['full_days'] = array(1 => "Domingo","Lunes","Martes","Miércoles","Jueves","Viernes","Sábado");
$calendar['short_days'] = array(1 => "Dom","Lun","Mar","Mie","Jue","Vie","Sab");
?>