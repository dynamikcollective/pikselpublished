<?php
	# POLISH
	$calendar = array();
	$calendar['long_month']		= array(1 => "Styczeń","Luty","Marzec","Kwiecień","Maj","Czerwiec","Lipiec","Sierpień","Wrzesień","Październik","Listopad","Grudzień");
	$calendar['short_month']	= array(1 => "Sty","Lut","Mar","Kwi","Maj","Cze","Lip","Sie","Wrz","Paź","Lis","Gru");
	$calendar['full_days']		= array(1 => "niedziela","poniedziałek","wtorek","środa","czwartek","piątek","sobota");
	$calendar['short_days']		= array(1 => "nie","pon","wto","śro","czw","pią","sob");
?>