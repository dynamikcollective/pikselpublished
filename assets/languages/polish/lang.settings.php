<?php
	# POLISH
	$langset['version'] 			= "4.0.8";		// Version number this language file is translated for
	$langset['active'] 				= 1;			// Allow the language to be selected in the management area
	$langset['translatedBy'] 		= 'Ktools Member - Agnieszka Posavec'; // www.jantarmedia.com // Method or person who translated the language
	$langset['mgmtAreaTrans'] 		= 0;			// Management area also translated
	$langset['lang_charset'] 		= "utf-8";		// The required character set for your language text.	
	$langset['id']					= "polish"; 	// Language ID - MUST MATCH THE DIRECORY NAME EXACTLY - NO SPACES - ALL LOWERCASE
	$langset['locale']				= "pl_PL"; 		// Language Locale
	$langset['xmlLangCode']			= "pl"; 		// Language Locale
	$langset['name']				= "Polski"; 	// Language Name That Gets Displayed
	
	$langset['date_format']			= "EURO"; 		// US (US Date 12/31/2007), EURO (European Date 31/12/2007), INT (World Date 2007/12/31)
	$langset['clock_format']		= "24"; 		// 12 or 24 (hours)
?>