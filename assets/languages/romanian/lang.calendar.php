<?php
	# ROMANIAN
	$calendar = array();
	$calendar['long_month']		= array(1 => "Ianuarie","Februarie","Martie","Aprilie","Mai","Iunie","Iulie","August","Septembrie","Octombrie","Noiembrie","Decembrie");
	$calendar['short_month']	= array(1 => "Ian","Feb","Mar","Apr","Mai","Iun","Iul","Aug","Sep","Oct","Noi","Dec");
	$calendar['full_days']		= array(1 => "Duminică","Luni","Marți","Miercuri","Joi","Vineri","Sâmbătă");
	$calendar['short_days']		= array(1 => "Du","Lu","Ma","Mi","Jo","Vi","Sâ");
?>