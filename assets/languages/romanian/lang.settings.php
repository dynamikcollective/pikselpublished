<?php
	# ROMANIAN	
	$langset['version'] 			= "4.2.2";		// Version number this language file is translated for
	$langset['active'] 				= 1;			// Allow the language to be selected in the management area
	$langset['translatedBy'] 		= 'arpadpap';   // Method or person who translated the language
	$langset['mgmtAreaTrans'] 		= 1;			// Management area also translated
	$langset['lang_charset'] 		= "utf-8";		// The required character set for your language text.	
	$langset['id']					= "romanian"; 	// Language ID - MUST MATCH THE DIRECORY NAME EXACTLY - NO SPACES - ALL LOWERCASE
	$langset['locale']				= "ro_RO"; 		// Language Locale
	$langset['xmlLangCode']			= "ro"; 		// Language Locale
	$langset['name']				= "Română"; 	// Language Name That Gets Displayed

	$langset['date_format']			= "RO"; 		// US (US Date 31/12/2007), EURO (European Date 31/12/2007), INT (World Date 31/12/2007)
	$langset['clock_format']		= "24"; 		// 12 or 24 (hours)
?>