<?php
	/******************************************************************
	*  ROMANIAN
	******************************************************************/
	
	# 4.7
	$lang['featuredGalleries']		= "Featured Galleries";
	
	
	# 4.6.3	
	$lang['ccMessage']				= "Please enter your credit card information below.";
	$lang['ccNumber']				= "Card Number";
	$lang['ccCVC']					= "CVC";
	$lang['ccExpiration']			= "Expiration (MM/YYYY)";
	
	
	# 4.6.1
	$lang['passwordLeaveBlank']		= "Leave blank for no password";
	$lang['myAlbums']				= "My Albums";
	
	# 4.5
	$lang['dateDownloadUpper']		= "DATE DOWNLOADED";
	$lang['downloadTypeUpper']		= "DOWNLOAD TYPE";
	$lang['rss']					= "RSS";
	
	# 4.4.8
	$lang['uploadFile']				= "Upload File";
	
	# 4.4.7
	$lang['contrSmallFileSize']		= "The file size is too small. The file must be at least";
	# plupload
	$lang['plupSelectFiles']		=  "Select files";
	$lang['plupAddFilesToQueue']	=  "Add files to the upload queue and click the start button.";
	$lang['plupFilename']			=  "Filename";
	$lang['plupStatus']				=  "Status";
	$lang['plupSize']				=  "Size";
	$lang['plupAddFiles']			=  "Add files";
	$lang['plupStartUplaod']		=  "Start upload";
	$lang['plupStopUpload']			=  "Stop current upload";
	$lang['plupStartQueue']			=  "Start uploading queue";
	$lang['plupDragFilesHere']		=  "Drag files here.";
	
	# 4.4.6
	$lang['batchUploader']			= "Batch Uploader";
	$lang['uploader'][1]			= "Java Based";
	$lang['uploader'][2]			= "HTML5/Flash Based";
	$lang['change']					= "Change";
	
	# 4.4.5
	$lang['moreInfoPlus']				= "[More Info]";
	
	# 4.4.3
	$lang['mediaLabelPropRelease']	= "Property Release";
	
	# 4.4.2
	$lang['go']						= "GO";
	
	# 4.4.0
	$lang['share'] 					= "Share";
	$lang['bbcode'] 				= "BBCode";
	$lang['html'] 					= "HTML";
	$lang['link']					= "Link";
	$lang['pricingCalculator']		= "Pricing Calculator";
	$lang['noOptionsAvailable']		= "No options available";
	$lang['viewCartUpper']			= "VIEW CART";
	
	# 4.3
	$lang['vatIDNumber']			= "VAT ID Number";
	
	# 4.2.1
	$lang['captchaError']			= "Captcha introdus nu a fost corect. Mesajul dvs. nu a fost trimis.";
	
	# 4.1.7
	$lang['mediaLicenseEU'] 		= "Utilizări în scop editorial";
	
	# 4.1.6
	$lang['yourBill'] 				= "Factura Dvs.";
	$lang['invoiceNumber'] 			= "Numărul facturii";
	$lang['paymentThanks'] 			= "Vă mulțumim pentru plată.";
	$lang['msActive'] 				= "Statutul de membru este acum activ.";
	$lang['today'] 					= "Azi";
	$lang['downloadsRemainingB']	= "Descărcări rămase";
	
	# 4.1.4
	$lang['link'] 					= "Link";
	$lang['optional'] 				= "Opțional";
	
	# 4.1.3
	$lang['keywordRelevancy'] 		= "Relevanța cuvinte cheie";
	$lang['mediaLicenseEX'] 		= "Licență extinsă";
	
	# 4.1.1
	$lang['editMediaDetails'] 		= "Editarea Detaliile Mediei";
	$lang['searchResults'] 			= "Rezultatele căutării";
	$lang['width'] 					= "Lățime";
	$lang['height'] 				= "Înălțime";
	$lang['hd'] 					= "HD";
	$lang['mediaProfile'] 			= "Profil";
	$lang['photo'] 					= "Fhoto";
	$lang['video'] 					= "Video";
	$lang['other'] 					= "Alt";
	$lang['thumbnail'] 				= "Miniatură";
	$lang['videoSample'] 			= "Video de previzualizare";
	$lang['attachFile'] 			= "Atașați fișiere";
	$lang['fileAttached'] 			= "Fișier atașat";
	$lang['attachMessage'] 			= "Vă rugăm să selectați un fișier de atașat pentru acest profil";
	$lang['browse'] 				= "Răsfoiți";
	$lang['uploadThumb'] 			= "Încărcați miniaturi";
	$lang['uploadVideo'] 			= "Încărcați video de previzualizare";
	
	# 4.1
	$lang['importSuccessMes'] 		= "Importat cu succes!";
	$lang['forgotPassword'] 		= "Ați uitat parola?";
	$lang['passwordSent'] 			= "E-mail a fost trimis, vă rugăm să verificați inbox-ul de e-mail, sau directorul de spam pentru mesaj.";
	$lang['passwordFailed'] 		= "Ne pare rău dar, acest membru nu există, vă rugăm să încercați să introduceți din nou adresa dvs. de email.";
	$lang['photoProfiles']			= "Profiluri foto";
	$lang['videoProfiles']			= "Profiluri video";
	$lang['otherProfiles']			= "Alte Profiluri";
	$lang['saving']					= "Salvează";
	$lang['myAccount']				= "Contul meu";
	$lang['myGalleries']			= "Galeriile mele";
	$lang['noMediaAlbum']			= "Nu există nici un mass-media de afișat.";
	$lang['editDetails']			= "Editați detaliile";
	$lang['approvalStatus0']		= "În curs de aprobare";
	$lang['approvalStatus1']		= "Aprobat";
	$lang['approvalStatus2']		= "A eșuat aprobarea";
	$lang['noDetailsMes']			= "Nu sunt mai multe detalii";	
	$lang['orphanedMedia']			= "Orfani media";
	$lang['lastBatch']				= "Ultimul lot Adăugat";
	$lang['deleteAlbumMes']			= "Ștergeți acest album și toate mass-media pe care le conține?";
	$lang['mailInMedia']			= "Mail pe CD/DVD";
	$lang['deleteAlbum']			= "Ștergeți albumul";
	$lang['editAlbum']				= "Editare album";
	$lang['albumName']				= "Nume album";
	$lang['makePublic']				= "A face public";
	$lang['deleteMedia']			= "Ștergeți Media";
	$lang['deleteMediaMes']			= "Ștergeți fișierele selectate?";
	$lang['selectAll']				= "Selectați Toate";
	$lang['selectNone']				= "Deselectați";
	$lang['noImportFilesMessage']	= "Nu există fișiere de importat!";
	$lang['selectAlbumMes']			= "Selectați albumul pe care doriți, pentru a adăuga această mass-media.";
	$lang['selectGalleriesMes']		= "Selectați galeria pe care doriți, să adăugați această mass-media.";
	$lang['chooseItemsMes']			= "Alegeți un produs pentru vânzare din lista de mai jos.";
	$lang['ablum']					= "Album";
	$lang['pricing']				= "Prețuri";
	$lang['px']						= "px";	
	$lang['noSales']				= "Nu aveți nici o vânzare.";
	$lang['itemUpper']				= "ITEM";
	$lang['commissionUpper']		= "COMISIA";
	$lang['addUpper']				= "ADĂUGAȚI";	
	$lang['cmDeleteVerify']			= "Sunteți sigur că doriți să ștergeți acest fișier?";
	$lang['waitingForImport']		= "Fișier în așteptare pentru import.";
	$lang['importSelectedUpper']	= "IMPORT CELE SELECTATE";
	$lang['addMediaDetails']		= "Adăugați detalii pentru fișier.";
	$lang['uploadUpper']			= "ÎNCARCĂ";
	$lang['noBioMessage']			= "În prezent nu există nici o biografie pentru acest membru.";
	$lang['collections']			= "Colecții";
	$lang['uploadMediaUpper']		= "ÎNCĂRCATI UN NOU MEDIA";
	$lang['startUpper']				= "START";
	
	# 4.0.9
	$lang['displayName']			= "Afișarea numelui";
	$lang['newAlbum']				= "Album nou";
	$lang['albums']					= "Albume";
	$lang['viewAllMedia']			= "VIZUALIZAȚI TOATE MEDIILE";
	$lang['signUpNow']				= "INREGISTREAZA-TE ACUM";
	
	# 4.0.8		
	$lang['exactMatch']				= "Potrivire exactă";
	$lang['mediaLabelMediaTypes']	= "Tipuri de media";
	
	# 4.0.6
	$lang['orderNumUpper']			= "NUMĂRUL DE ORDINE";
	$lang['orderDateUpper']			= "DATA DE COMANDĂ";
	$lang['paymentUpper']			= "PLATA";	
	
	# 4.0.5
	$lang['dateRange']				= "Interval de date";
	$lang['resolution']				= "Rezoluție";
	$lang['continueShopUpper']		= "Continuare cumpărături";
	$lang['votes']					= "voturi";
	$lang['moreNews']				= "mai multe știri";
	$lang['currentSearch']			= "Căutarea actuală";
	$lang['dates']					= "Date";
	$lang['licenseType']			= "Tip licență";
	$lang['searchUpper']			= "CAUTĂ";
	$lang['from']					= "De la";
	$lang['to']						= "până la";
	$lang['lightboxUpper']			= "LIGHTBOX";
	$lang['itemsUpper']				= "ELEMENTE";
	$lang['createdUpper']			= "CREATĂ";
	$lang['na']						= "Nu sunt informații"; 
	$lang['galSortCDate']			= "Data creării";
	
	# 4.0.4
	$lang['digitalDownloads']		= "Download-uri digitale";

	$lang['copyright']				= "Copyright &copy; ".date("Y");
	$lang['reserved']				= "Toate drepturile rezervate.";
	
	$lang['days']					= "Zile";
	$lang['weeks']					= "Săptămâni";
	$lang['months']					= "Luni";
	$lang['years']					= "Ani";
	$lang['weekly']					= "Săptămânal";
	$lang['monthly']				= "Lunar";
	$lang['quarterly']				= "Trimestrial";
	$lang['semi-annually']			= "Semi-Anual";
	$lang['annually']				= "Anual";	
	$lang['guest']					= "Vizitator";	
	$lang['login'] 					= "Autentificare";	
	$lang['loginCaps'] 				= "AUTENTIFICARE";
	$lang['loginMessage']			= "Vă rugăm să introduceți jos adresa dvs. de e-mail și parola, pentru autentificare.";
	$lang['loggedOutMessage']		= "Ați fost delogat.";
	$lang['loginFailedMessage']		= "Autentificare eșuată: e-mail sau parola au fost incorecte.";
	$lang['accountActivated']		= "Contul dvs. a fost activat.";
	$lang['activationEmail']		= "Un e-mail de verificare a fost trimisă. Vă rugăm să folositi link-ul primit în e-mail, pentru a activa contul.";
	$lang['loginAccountClosed']		= "Acest cont este închis sau inactiv.";
	$lang['loginPending']			= "Acest cont nu este verificat. Acesta trebuie să fie verificată înainte de a vă puteți autentifica.";
	$lang['yesLogin']				= "Da, aș dori să mă loghez";
	$lang['noCreateAccount']		= "Nu, aș dori să creez un cont.";
	$lang['haveAccountQuestion']	= "Nu aveți un cont la noi?";	
	$lang['collections']			= "Colecții digitale";
	$lang['featuredCollections']	= "Colecțiile digitale recomandate";	
	$lang['similarMedia']			= "Medii asemănătoare";	
	$lang['paypal']					= "PayPal";
	$lang['checkMO']				= "CEC/Ordin de plată";
	$lang['paypalEmail']			= "PayPal Email";	
	$lang['paid']					= "Achitat";
	$lang['processing']				= "Prelucrare";
	$lang['unpaid']					= "Neplătit";
	$lang['refunded']				= "Rambursat";
	$lang['failed']					= "Eșuat";
	$lang['cancelled']				= "Anulat";
	$lang['approved']				= "Aprobat";
	$lang['incomplete']				= "Incomplet";
	$lang['billLater']				= "Factura mai târziu";
	$lang['expired']				= "Expirat";
	$lang['unlimited']				= "Nelimitat";
	$lang['quantityAvailable']		= "Cantitatea disponibilă";	
	$lang['shipped']				= "Expediat";
	$lang['notShipped']				= "Nu este Expediat";
	$lang['backordered']			= "Pe lista de restocare";	
	$lang['taxIncMessage']			= "TVA inclus";	
	$lang['addTag']					= "Adauga taguri";
	$lang['memberTags']				= "Tagurile membrilor";
	$lang['comments']				= "Comentarii";
	$lang['showMoreComments']		= "Arată toate comentariile.";
	$lang['noComments']				= "Nu sunt comentarii aprobate.";
	$lang['noTags']					= "Nu există tag-uri aprobate.";
	$lang['addNewComment']			= "Comentează";
	$lang['commentPosted']			= "Comentariul dvs. a fost publicata.";
	$lang['commentPending']			= "Noul comentariul dvs. a fost trimisa, și va fi aprobat în curând.";
	$lang['commentError']			= "A fost o eroare, iar comentariu Dvs. nu a fost publicata";
	$lang['tagPosted']				= "Tag-ul dvs. a fost publicat.";
	$lang['tagPending']				= "Noul tag a fost trimis la aprobare, va fi controlat și aprobat în curând.";
	$lang['tagError']				= "A fost o eroare, și tag-ul dvs. nu a fost publicat";
	$lang['tagDuplicate']			= "Acest tag deja există.";
	$lang['tagNotAccepted']			= "Tag-ul dvs. nu a fost acceptat de către sistemul nostru.";	
	$lang['preferredLang']			= "Limba preferată";
	$lang['dateTime']				= "Data/Timp";
	$lang['preferredCurrency']		= "Valuta preferată";	
	$lang['longDate']				= "Data lungă";
	$lang['shortDate']				= "Data scurtă";
	$lang['numbDate']				= "Număr Data";	
	$lang['daylightSavings']		= "Ora de vară";
	$lang['dateFormat']				= "Formatul datei";
	$lang['timeZone']				= "Zonă de timp";
	$lang['dateDisplay']			= "Afișează data";
	$lang['clockFormat']			= "Format ceas";
	$lang['numberDateSep']			= "Separator Număr Data";	
	$lang['renew']					= "REÎNNOIRE";	
	$lang['noOrders']				= "Nu există comenzi sub acest cont.";
	$lang['noDownloads']			= "Nu au fost descărcări sub acest cont.";
	$lang['noFeatured'] 			= "Nu exista articole recomandate in acest domeniu.";	
	$lang['promotions']				= "Promoții";
	$lang['noPromotions']			= "Nu există cupoane sau promoții în acest moment.";	
	$lang['autoApply']				= "Aplicat automat la casă.";
	$lang['useCoupon']				= "Folosiți codul de mai jos la casă, sau faceți clic pe butonul -APLICĂ- de a folosi acest cupon";	
	$lang['couponApplied']			= "Un cupon/discount a fost aplicată în coș.";
	$lang['couponFailed']			= "Acest cupon/discount este invalid.";	
	$lang['couponNeedsLogin']		= "Trebuie să fiiți autentificați pentru a folosi acest cupon/discount.";
	$lang['couponMinumumWarn']		= "Subtotalul dumneavoastră nu îndeplinește cerința ca sa utilizați acest cupon/discount.";
	$lang['couponAlreadyUsed']		= "Acest cupon/discount puteți folosi doar o singură dată";
	$lang['couponLoginWarn']		= "Trebuie să fiiți autentificați pentru a folosi acest cupon/discount.";	
	$lang['checkout']				= "PLATĂ";
	$lang['continue']				= "CONTINUĂ";	
	$lang['shipTo']					= "Expediat la";
	$lang['billTo']					= "Facturat pentru";
	//$lang['mailTo']					= "Mail Payment To";	
	$lang['galleries']				= "Galerii";
	$lang['chooseGallery']			= "Alegeți o galerie de mai jos pentru a vedea conținutul lui.";
	$lang['galleryLogin']			= "Vă rugăm să introduceți mai jos parola pentru această galerie, pentru autentificare.";
	$lang['galleryWrongPass']		= "Parola introdusă pentru această galerie a fost incorectă.";	
	$lang['newestMedia']			= "Cele mai noi";
	$lang['popularMedia']			= "Cele mai preferate";	
	$lang['contributors']			= "Colaboratori";
	$lang['contUploadNewMedia']		= "Adăugați un Media nou";
	$lang['contViewSales']			= "Vizualizați vânzările";
	$lang['contPortfolio']			= "Portofoliul meu";
	$lang['contGalleries']			= "Galeriile mele";
	$lang['contMedia']				= "Mediile mele";	
	$lang['aboutUs']				= "Despre noi";	
	$lang['news']					= "Știri";
	$lang['noNews']					= "Nu există știri în acest moment.";	
	$lang['termsOfUse']				= "Termeni și condiții de utilizare";
	$lang['privacyPolicy']			= "Politica de confidenţialitate";
	$lang['purchaseAgreement']		= "Acordul de cumpărare";	
	$lang['iAgree']					= "Sunt de acord cu";	
	$lang['createAccount']			= "Creați un cont";
	$lang['createAccountMessage']	= "Vă rugăm să completați formularul de mai jos pentru a crea un cont nou.";	
	$lang['contactUs']				= "Contactați-ne";
	$lang['contactMessage']			= "Vă mulțumim pentru că nea-ți contactat. Vă vom răspunde în scurt timp.";
	$lang['contactError']			= "Formularul de contact nu a fost completat corect. Mesajul dvs. nu a fost trimis.";
	$lang['contactIntro']			= "Vă rugăm să utilizați formularul de mai jos pentru a ne contacta. Vă vom răspunde în cel mai scurt timp posibil.";
	$lang['contactEmailSubject']	= "Întrebare din Formularul de contact";
	$lang['contactFromName']		= "Formular de contact";	
	$lang['prints']					= "Printuri";
	$lang['featuredPrints']			= "Printuri recomandate";	
	$lang['newBill']				= "O noua factura a fost creata pentru acest membru. Acest membru nu va deveni activă sau nu va fi reînnoita până când factura nu este plătita.";
	$lang['accountInfo']			= "Editați contul";
	$lang['accountUpdated']			= "Informațiile contului dvs. a fost actualizat.";
	$lang['editAccountInfo']		= "Editați informatiile contului dvs.";
	$lang['editAccountInfoMes']		= "Editați informatiile contul dvs. mai jos, și faceți clic pe butonul -Salveaza- pentru a salva modificările.";	
	$lang['accountInfoError1']		= "Parola și parola de verificare nu se potrivesc!";
	$lang['accountInfoError5']		= "Parola actuală introdusă este incorectă!";
	$lang['accountInfoError2']		= "Parola trebuie să fie de cel puțin 6 caractere!";
	$lang['accountInfoError12']		= "Această adresă de e-mail este deja folosit!";
	$lang['accountInfoError13']		= "E-mail nu a fost acceptat!";
	$lang['accountInfoError3']		= "Introduceți cuvintele de mai sus!";
	$lang['accountInfoError4']		= "Cuvintele Captcha au fost incorecte!";
	$lang['readAgree']				= "Am citit";	
	$lang['agreements']				= "Acordurile";	
	$lang['poweredBy']				= "Realizat de";
	$lang['captchaAudio']			= "Introduceți numerele pe care le auziți";
	$lang['captchaIncorrect']		= "Incorect, vă rugăm să încercați din nou";
	$lang['captchaPlayAgain']		= "Redare din nou";
	$lang['captchaDownloadMP3']		= "Descărca în format MP3";
	$lang['captcha']				= "Cod de siguranta";
	$lang['subHeaderID']			= "ID";
	$lang['subHeaderSubscript']		= "ABONAMENT";
	$lang['subHeaderExpires']		= "Expiră";
	$lang['subHeaderDPD']			= "Descărcări pe zi/RĂMASE";
	$lang['subHeaderStatus']		= "STATUTUL";
	$lang['downloads']				= "Descărcări";	
	$lang['thankRequest']			= "Vă mulțumim pentru cererea dumneavoastră. Vă vom contacta în scurt timp.";
	$lang['pleaseContactForm']		= "Vă rugăm să ne contactați pentru oferta de pret privind acest fișier, prin completarea formularului de mai jos";
	$lang['downWithSubscription']	= "Descărcați folosind abonamentul";	
	$lang['noInstantDownload'] 		= "Acest fișier nu este disponibil pentru descărcare instantanee. Acesta va fi livrat de către administratorul site-ului prin e-mail, la cerere sau cumpărare.";
	
	// Tickets
	$lang['newTicketsMessage']		= "/tichete de suport/ noi sau actualizate";
	$lang['emptyTicket']			= "Acest /tichet de suport/ este gol.";
	$lang['messageID']				= "ID-ul mesajului";
	$lang['ticketNoReplies']		= "Acest /tichet de suport/ este închis, și nici nu poate accepta răspunsuri.";
	$lang['ticketUpdated']			= "Acest /tichet de suport/ a fost actualizat!";
	$lang['ticketClosed']			= "Acest /tichet de suport/ a fost închis!";
	$lang['closeTicket']			= "Inchide tichetul";
	$lang['newTicket']				= "Nou /tichet de suport/";
	$lang['newTicketButton']		= "TICKET NOU";
	$lang['ticketUnreadMes']		= "Tichetul conține mesaje necitite / nou.";
	$lang['ticketUnderAccount']		= "Nu exista /tichet de suport/ în cadrul acestei cont";	
	$lang['ticketSubmitted']		= "/Tichetul de suport/ al dumneavoastră a fost trimisa. Noi vom răspunde în scurt timp.";
	
	$lang['mediaNotElidgiblePack']	= "Acest media nu este eligibil pentru a fi atribuită la orice pachet!";
	$lang['noBills']				= "Nu există facturi sub acest cont.";	
	$lang['lightbox']				= "Lightbox";
	$lang['noLightboxes']			= "Nu aveți lightbox-uri.";
	$lang['lightboxDeleted']		= "Lightboxul a fost șters.";
	$lang['lbDeleteVerify']			= "Sunteți sigur că doriți să ștergeți această lightbox?";
	$lang['newLightbox']			= "LIGHTBOX NOU";
	$lang['lightboxCreated']		= "Noul dvs. lightbox a fost creat.";
	$lang['addToLightbox']			= "Adaugă la Lightbox";
	$lang['createNewLightbox']		= "Creați un nou Lightbox";
	$lang['editLightbox']			= "Editați această Lightbox";
	$lang['addNotes']				= "Adăugați observații";
	$lang['editLightboxItem']		= "Editați elementul din Lightbox";
	$lang['removeFromLightbox']		= "STERGE DIN LIGHTBOX";	
	$lang['savedChangesMessage']	= "Modificările dvs. au fost salvate.";	
	$lang['noSubs']					= "Nu există abonamente sub acest cont.";	
	$lang['unpaidBills']			= "facturi neplătite";	
	$lang['notices']				= "Notificări";	
	$lang['subscription']			= "Abonamente";	
	$lang['assignToPackage']		= "Atribuiți la un pachet";
	$lang['startNewPackage']		= "Start un nou pachet";
	$lang['packagesInCart']			= "Pachetele in cosul dvs.";	
	$lang['id']						= "ID";
	$lang['summary']				= "Rezumat";
	$lang['status']					= "Starea";
	$lang['lastUpdated']			= "Ultima modificare";
	$lang['opened']					= "Deschisă";
	$lang['reply']					= "Răspuns";	
	$lang['required']				= "Obligatoriu";	
	$lang['bills']					= "Facturi";
	$lang['orders']					= "Comenzi";
	$lang['downloadHistory']		= "Istoria descărcărilor ";
	$lang['supportTickets']			= "Thikete de suport";
	$lang['order']					= "Comanda";	
	$lang['packages']				= "Pachete";
	$lang['featuredPackages']		= "Pachete recomandate";	
	$lang['products']				= "Produse";
	$lang['featuredProducts']		= "Produse recomandate";	
	$lang['subscriptions']			= "Abonamente";
	$lang['featuredSubscriptions']	= "Abonamente recomandate";	
	$lang['yourCredits']			= "CREDITELE<br />DVS.";
	$lang['featuredCredits']		= "Pachete de credite recomandate";	
	$lang['media'] 					= "MEDIA";
	$lang['mediaNav'] 				= "Media";
	$lang['featuredMedia']			= "Media recomandate";	
	$lang['featuredItems'] 			= "Recomandări";	
	$lang['showcasedContributors'] 	= "Colaboratori prezentate";	
	$lang['galleryLogin']			= "Logare in Galerie";	
	$lang['forum'] 					= "Forum";
	$lang['randomMedia'] 			= "Media aleatoare";
	$lang['language'] 				= "Limbă";	
	$lang['priceCreditSep']			= "sau";	
	$lang['viewCollection']			= "VIZUALIZAȚI COLECȚIA";	
	$lang['loggedInAs']				= "Sunteți logat ca";	
	$lang['editProfile']			= "EDITAȚI CONTUL";	
	$lang['noItemCartWarning']		= "Trebuie să selectați o fotografie înainte de a putea adăuga la coș!";
	$lang['cartTotalListWarning']	= "Aceste valori sunt doar estimări, și se poate schimba ușor ca urmare a fluctuației monedei și a rotunjirii.";
	$lang['applyCouponCode']		= "Aplicați codul Cupon";
	$lang['billMeLater']			= "Facturare mai târziu";
	$lang['billMeLaterDescription']	= "Va vom factura lunar, pentru cumpărăturile dumneavoastră.";
	$lang['shippingOptions']		= "Modalitați de livrare";
	$lang['paymentOptions']			= "Opțiuni de plată";
	$lang['chooseCountryFirst']		= "Întâi alegeți țara";
	$lang['paymentCancelled']		= "Plata dvs. a fost anulată. Dacă doriți, puteți încerca plata din nou."; 
	$lang['paymentDeclined']		= "Plata dvs. a fost refuzată.Dacă doriți, puteți încerca plata din nou."; 		
	$lang['generalInfo']			= "Informații personale";
	$lang['membership']				= "Calitate de membru";
	$lang['preferences']			= "Preferințe";
	$lang['address']				= "Address";
	$lang['actions']				= "Acțiuni";
	$lang['changePass']				= "Schimbare parolă";
	$lang['changeAvatar']			= "Schimbare Avatar";
	$lang['bio']					= "Biografie";
	$lang['contributorSettings']	= "Setări Colaborator";
	$lang['edit']					= "EDITARE";	
	$lang['leftToFill']				= "Elemente ramase";	
	$lang['commissionMethod']		= "Metoda de plată comision"; 
	$lang['commission']				= "Comisia";	
	$lang['signupDate']				= "Membru din";
	$lang['lastLogin']				= "Ultima logare";	
	$lang['clientName']				= "Numele client";
	$lang['eventCode']				= "Codul evenimentului";
	$lang['eventDate']				= "Data evenimentului";
	$lang['eventLocation']			= "Locul evenimentului";	
	$lang['viewPackOptions']		= "Vezi Conținutul pachetului & Opțiuni";
	$lang['viewOptions']			= "Vizualizați opțiunile";	
	$lang['remove']					= "ÎNDEPĂRTAȚI";
	$lang['productShots']			= "Imaginile produsului";
	$lang['currentPass']			= "Parola actuală";
	$lang['newPass']				= "Noua parolă";
	$lang['vNewPass']				= "Verificați Parola nouă";
	$lang['verifyPass']				= "Verificați parola";
	$lang['firstName']				= "Prenume";
	$lang['lastName']				= "Nume";
	$lang['location']				= "Localitatea";
	$lang['memberSince']			= "Membru din";
	$lang['portfolio']				= "PORTFOLIO";
	$lang['profile']				= "PROFIL";
	$lang['address']				= "Adresa";
	$lang['city']					= "Orașul";
	$lang['state']					= "Județul";
	$lang['zip']					= "Codul poștal";
	$lang['country']				= "Țara";
	$lang['save'] 					= "SALVEAZA";
	$lang['add'] 					= "ADAUGĂ";
	$lang['companyName']			= "Numele companiei";
	$lang['accountStatus']			= "Starea contului";
	$lang['website']				= "Site-ul web";
	$lang['phone']					= "Telefon";
	$lang['email'] 					= "Email";
	$lang['name'] 					= "Nume";
	$lang['submit'] 				= "TRIMITE";	
	$lang['message'] 				= "Mesaj";	
	$lang['question']				= "Întrebare";
	$lang['password'] 				= "Parola";	
	$lang['members'] 				= "Membrii";
	$lang['visits'] 				= "Vizitatori";	
	$lang['logout'] 				= "Ieșire";
	$lang['lightboxes']				= "Lightboxuri";
	$lang['cart']					= "Coșul DVS";
	$lang['cartItemAdded']			= "Un articol a fost adăugat în coșul dvs.";
	$lang['includesTax']			= "Include TVA";
	$lang['addToCart']				= "ADAUGĂ LA COȘ";
	$lang['items']					= "Elemente";
	$lang['item']					= "Element";
	$lang['qty']					= "Cantitate";
	$lang['price']					= "Preț";
	$lang['more']					= "Mai mult";
	$lang['moreInfo']				= "Mai multe informații";
	$lang['back']					= "Înapoi";
	$lang['none']					= "Niciuna";
	$lang['details']				= "Detalii";
	$lang['options']				= "Opțiuni";
	$lang['page']					= "Pagina";
	$lang['ratingSubmitted']		= "Evaluarea trimisa";
	$lang['noMedia']				= "Această galerie este gol";
	$lang['itemsTotal']				= "Total produse";
	$lang['of']						= "din";
	$lang['view']					= "ARATĂ";
	$lang['apply']					= "ADAUGĂ";
	$lang['currency']				= "schimbați valuta";
	$lang['active']					= "Activ";
	$lang['pending']				= "În curs";
	$lang['freeTrial']				= "Trial gratuit";
	$lang['setupFee']				= "Taxa de instalare";
	$lang['free']					= "Gratuit";
	$lang['open']					= "Deschide";
	$lang['close']					= "ÎNCHIDE";
	$lang['closed']					= "Închisă";
	$lang['by']						= "De";
	$lang['download']				= "Descărcați";
	$lang['downloadUpper']			= "DESCĂRCAȚI";
	$lang['KB']						= "K";
	$lang['MB']						= "MB";
	$lang['files']					= "Fișiere";
	$lang['unknown']				= "Necunoscut";
	$lang['freeDownload']			= "Descărcați gratuit";
	$lang['prevDown']				= "Descărcarea anterioară";
	$lang['pay']					= "PLATA";
	$lang['purchaseCredits']		= "CUMPĂRAȚI CREDITE";
	$lang['purchaseSub']			= "CUMPĂRAȚI UN ABONAMENT";
	$lang['hour'] 					= "Oră";
	$lang['slash'] 					= "Linie fracţionară";
	$lang['period'] 				= "Perioada";
	$lang['dash'] 					= "Liniuță";
	$lang['gmt'] 					= "GMT";
	$lang['avatar'] 				= "Avatar";
	$lang['delete'] 				= "ȘTERGE";
	$lang['uploadAvatar']			= "ÎNCARCĂ AVATAR";
	$lang['welcome']				= "Bine ați venit";
	$lang['expires']				= "Expiră";
	$lang['msExpired']				= "Statutul de membru a expirat";
	$lang['newSales']				= "noi vânzări de când v-ați conectat ultima dată";
	$lang['never']					= "Niciodată";
	$lang['yes']					= "DA";
	$lang['no']						= "NU";
	$lang['create'] 				= "CREEAZĂ";
	$lang['cancel'] 				= "ANULEAZĂ";
	$lang['notes'] 					= "Observații";
	$lang['update'] 				= "ACTUALIZARE";
	$lang['each'] 					= "fiecare";
	$lang['enterKeywords']			= "Introduceți cuvintele cheie";
	$lang['send']					= "TRIMITE";
	$lang['emailTo']				= "Email pentru";
	$lang['yourName']				= "Numele dvs.";
	$lang['yourEmail']				= "E-mail dvs.";
	$lang['emailToFriend']			= "Trimiteti unui prieten";
	$lang['emailToFriendSent']		= "Un email a fost trimis! Puteți trimite un alt email, sau închideți această fereastră.";
	$lang['linkSentBy']				= "Un link photo/video a fost trimis";
	$lang['newPackageMessage']		= "Începeți un nou pachet de mai jos, sau selectați un pachet similar care este deja in coș";
	$lang['warning']				= "Atenție!";
	$lang['estimated']				= "Estimată";
	$lang['doneUpper']				= "FĂCUT";
	$lang['returnToSiteUpper']		= "REVENI LA SITE";
	$lang['chooseCountryFirst']		= "Alegeți țara întâi";
	$lang['advancedSearch']			= "Căutare avansată";
	$lang['eventSearch']			= "Căutați evenimente";	
	$lang['AND']					= "ȘI";
	$lang['OR']						= "SAU";
	$lang['NOT']					= "NU";
	$lang['noAccess']				= "Nu aveți acces pentru a vedea această zonă.";	
	$lang['siteStats']				= "Statisticile site-ului";
	$lang['membersOnline']			= "Membrii online";
	$lang['minutesAgo']				= "minute în urmă";
	$lang['seconds']				= "Secunde";	
	$lang['megabytesAbv']			= "MB";	
	$lang['downWithSub']			= "Descărcați, folosind abonamentul";
	$lang['downloadsRemaining']		= "Download-uri rămase azi";
	$lang['requestDownloadSuccess']	= "Am primit solicitarea dvs. pentru acest fișier, și o să vă contactăm în scurt timp.";	
	$lang['mediaLicenseNFS']		= "Nu e de vânzare";
	$lang['mediaLicenseRF']			= "Drepturi de autor, gratuit";
	$lang['mediaLicenseRM']			= "Drepturi de autor, cu plată";
	$lang['mediaLicenseFR']			= "Gratuit";
	$lang['mediaLicenseCU']			= "Preț, Contactați-ne";	
	$lang['original']				= "Original";	
	$lang['prevDownloaded']			= "Ați descărcat anterior acest fișier. Nu există nici o taxă să-l descărcați din nou.";
	$lang['instantDownload']		= "Descărcare Instant";
	$lang['requestDownload']		= "CERERE DE DESCĂRCARE";
	$lang['request']				= "CERERE";
	$lang['enterEmail']				= "Introduceți adresa de email";	
	$lang['license']				= "Licență";
	$lang['inches']					= "Inci";
	$lang['centimeters']			= "Centimetri";
	$lang['dpi']					= "dpi";	
	$lang['noSimilarMediaMessage']	= "Nu au fost găsite medii similare.";
	$lang['backUpper']				= "ÎNAPOI";
	$lang['nextUpper']				= "URMĂTORUL";
	$lang['prevUpper']				= "ANTERIOR";
	$lang['curGalleryOnly']			= "Numai Galeria actuală";
	$lang['sortBy']					= "Sortare după";
	$lang['galSortColor']			= "Culoare";
	$lang['galSortDate']			= "Data adăugării";
	$lang['galSortID']				= "ID";
	$lang['galSortTitle']			= "Denumire";
	$lang['galSortFilename']		= "Nume fișier";
	$lang['galSortFilesize']		= "Mărime fișier";
	$lang['galSortSortNum']			= "Sortare după număr";
	$lang['galSortBatchID']			= "Lot ID";
	$lang['galSortFeatured']		= "Recomandate";
	$lang['galSortWidth']			= "Lățime";
	$lang['galSortHeight']			= "Înălțime";
	$lang['galSortViews']			= "Vizualizări";
	$lang['galSortAsc']				= "Crescător";
	$lang['galSortDesc']			= "Descrescător";
	$lang['mediaIncludedInColl']	= "Acest fișier este inclus în următoarele colecții.";	
	$lang['billHeaderInvoice']		= "FACTURA";
	$lang['billHeaderDate']			= "DATA DE FACTURARE";
	$lang['billHeaderDueDate']		= "SCADENȚA";
	$lang['billHeaderTotal']		= "TOTAL";
	$lang['billHeaderStatus']		= "STAREA";
	
	// Cart
	$lang['creditsWarning']			= "Nu aveți suficiente credite pentru a plăti. Conectați-vă sau adăugați credite pentru coș.";
	$lang['subtotalWarning']		= "Valoare minimă a cumpărăturilor este:";
	$lang['pleaseWait']				= "Vă rugăm să așteptați în timp ce noi procesăm solicitarea dvs.!";
	$lang['billMailinThanks']		= "Vă rugăm să trimiteți prin poștă cecul sau ordinul de plată pentru următoarea sumă, la adresa menționată. După ce plata este primită, vom marca factura ca plătit.";
	$lang['mailinRef']				= "Vă rugăm să referiți la următorul număr de plata";
	$lang['subtotal']				= "Subtotalul";
	$lang['shipping']				= "Livrare";
	$lang['discounts']				= "Discounturi/Cupoane";
	$lang['total']					= "Total";
	$lang['creditsSubtotal']		= "Credite subtotal";
	$lang['creditsDiscounts']		= "Credite discounturi";
	$lang['credits']				= "Credite";
	$lang['reviewOrder']			= "Examinați comanda dvs.";
	$lang['payment']				= "Plata";
	$lang['cartNoItems']			= "Nu aveți niciun produs în coș!";
	$lang['enterShipInfo']			= "Vă rugăm să introduceți, informațiile de livrare și facturare mai jos.";
	$lang['sameAsShipping']			= "Același ca adresa de livrare";
	$lang['differentAddress']		= "Oferiți o altă adresă";
	$lang['noShipMethod']			= "Nu există metode de expediere pentru a livra aceste elemente.";	
	$lang['choosePaymentMethod']	= "Vă rugăm să alegeți o metodă de plată";	
	$lang['discountCode']			= "CODUL DE DISCOUNT";
	$lang['use']					= "Utilizați";
	$lang['continueNoAccount']		= "Sau continuați fără cont, mai jos";
	
	
	// Order
	$lang['yourOrder']				= "Comanda Dvs.";
	$lang['viewInvoice']			= "VEZI FACTURA";
	$lang['totalsShown']			= "Totaluri arată în";
	$lang['downloadExpired']		= "Descărcare expirată";
	$lang['downloadExpiredMes']		= "Acest link de download a expirat. Vă rugăm să ne contactați pentru reactivare.";
	$lang['downloadMax']			= "Numărul de Descărcări maximi depășită";
	$lang['downloadMaxMes']			= "Ați atins numărul maxim de descărcări permis pentru acest fișier cumpărat. Vă rugăm să ne contactați pentru a avea link-ul de descărcare reactivat.";
	$lang['downloadNotApproved']	= "Comandă neaprobată";
	$lang['downloadNotApprovedMes']	= "Nu puteți descărca acest fișier până când comanda nu a fost aprobat. Vă rugăm să reveniți în curând.";	
	$lang['downloadNotAvail']		= "Descărcarea Instant nu este disponibil";
	$lang['downloadNotAvailMes']	= "Acest fișier nu este disponibil pentru descărcare instantanee. Vă rugăm să folosiți butonul de cerere, de mai jos ca să vă trimitem pe email linkul de descărcare.";
	$lang['orderNumber']			= "Număr de comandă";
	$lang['orderPlaced']			= "Comandă plasată"; 
	$lang['orderStatus']			= "Starea comenzii";
	$lang['paymentStatus']			= "Starea de plată";
	
	
	// IPTC
	$lang['iptc']					= "IPTC";
	$lang['iptc_title']				= "Titlu";
	$lang['iptc_description']		= "Descriere";
	$lang['iptc_instructions']		= "Instrucțiuni";
	$lang['iptc_date_created']		= "Data creării";
	$lang['iptc_author']			= "Autor";
	$lang['iptc_city']				= "Localitate";
	$lang['iptc_state']				= "Județ";
	$lang['iptc_country']			= "Țara";
	$lang['iptc_job_identifier']	= "Profesia";
	$lang['iptc_headline']			= "Linia Titlu";
	$lang['iptc_provider']			= "Linia de credit"; //Provider
	$lang['iptc_source']			= "Sursa";
	$lang['iptc_description_writer']= "Scriitorul Descrierii";
	$lang['iptc_urgency']			= "Urgență";
	$lang['iptc_copyright_notice']	= "Termeni si condiții Copyright";
	
	// EXIF
	$lang['exif']							= "EXIF";
	$lang['exif_FileName']					= "Nume fișier";
	$lang['exif_FileDateTime']				= "Fișier data/ora";
	$lang['exif_FileSize']					= "Mărime fișier";
	$lang['exif_FileType']					= "Tip fișier";
	$lang['exif_MimeType']					= "Tip media";
	$lang['exif_SectionsFound']				= "Secțiuni găsit";
	$lang['exif_ImageDescription']			= "Descrierea imaginii";
	$lang['exif_Make']						= "Implementare";
	$lang['exif_Model']						= "Model";
	$lang['exif_Orientation']				= "Orientarea";
	$lang['exif_XResolution']				= "Xrezoluție";
	$lang['exif_YResolution']				= "Yrezoluție";
	$lang['exif_ResolutionUnit']			= "Unitatea de rezoluție";
	$lang['exif_Software']					= "Software";
	$lang['exif_DateTime']					= "Data/Ora";
	$lang['exif_YCbCrPositioning']			= "YCbCr Poziționare";
	$lang['exif_Exif_IFD_Pointer']			= "Exif IFD indicator";
	$lang['exif_GPS_IFD_Pointer']			= "GPS IFD indicator";
	$lang['exif_ExposureTime']				= "Timp de expunere";
	$lang['exif_FNumber']					= "FNumăr";
	$lang['exif_ExposureProgram']			= "Programul de expunere";
	$lang['exif_ISOSpeedRatings']			= "Sensibilitate ISO";
	$lang['exif_ExifVersion']				= "Versiunea Exif";
	$lang['exif_DateTimeOriginal']			= "Data/Timp Original";
	$lang['exif_DateTimeDigitized']			= "Data/Timp digitalizat";
	$lang['exif_ComponentsConfiguration']	= "Configurarea componentelor";
	$lang['exif_ShutterSpeedValue']			= "Valoarea timpului de expunere";
	$lang['exif_ApertureValue']				= "Valoarea diafragmei";
	$lang['exif_MeteringMode']				= "Mod de măsurare";
	$lang['exif_Flash']						= "Blițul";
	$lang['exif_FocalLength']				= "Distanța focală";
	$lang['exif_FlashPixVersion']			= "Bliț Pix versiune";
	$lang['exif_ColorSpace']				= "Spațiul de culoare";
	$lang['exif_ExifImageWidth']			= "Exif lățime fotografie";
	$lang['exif_ExifImageLength']			= "Exif lungime fotografie";
	$lang['exif_SensingMethod']				= "Metoda de detectare";
	$lang['exif_ExposureMode']				= "Modul expunerii";
	$lang['exif_WhiteBalance']				= "Balansul de alb";
	$lang['exif_SceneCaptureType']			= "Tipul Capturarea Scenei";
	$lang['exif_Sharpness']					= "Claritate";
	$lang['exif_GPSLatitudeRef']			= "Referință Latitudine GPS";
	$lang['exif_GPSLatitude_0']				= "LatitudineGPS 0";
	$lang['exif_GPSLatitude_1']				= "LatitudineGPS 1";
	$lang['exif_GPSLatitude_2']				= "LatitudineGPS 2";
	$lang['exif_GPSLongitudeRef']			= "Referință Latitudine GPS";
	$lang['exif_GPSLongitude_0']			= "Longitudine GPS 0";
	$lang['exif_GPSLongitude_1']			= "Longitudine GPS 1";
	$lang['exif_GPSLongitude_2']			= "Longitudine GPS 2";
	$lang['exif_GPSTimeStamp_0']			= "Amprenta de timp GPS 0";
	$lang['exif_GPSTimeStamp_1']			= "Amprenta de timp GPS 1";
	$lang['exif_GPSTimeStamp_2']			= "Amprenta de timp GPS 2";
	$lang['exif_GPSImgDirectionRef']		= "GPS Direcția fotografiei de referință";
	$lang['exif_GPSImgDirection']			= "GPS Direcția fotografiei";
	
	// Media Labels
	$lang['mediaLabelTitle']		= "Titlu";
	$lang['mediaLabelDesc']			= "Descriere";
	$lang['mediaLabelCopyright']	= "Drepturi de autor";
	$lang['mediaLabelRestrictions']	= "Restricții de utilizare";
	$lang['mediaLabelRelease']		= "Model eliberare pentru";
	$lang['mediaLabelKeys']			= "Cuvinte cheie";
	$lang['mediaLabelFilename']		= "Numele fișierului";
	$lang['mediaLabelID']			= "ID";
	$lang['mediaLabelDate']			= "Adăugat";
	$lang['mediaLabelDateC']		= "Creat";
	$lang['mediaLabelDownloads']	= "Descărcări";
	$lang['mediaLabelViews']		= "Vizualizări";
	$lang['mediaLabelPurchases']	= "Cumpărări";
	$lang['mediaLabelColors']		= "Culori";
	$lang['mediaLabelPrice']		= "Preț";
	$lang['mediaLabelCredits']		= "Credite";
	$lang['mediaLabelOwner']		= "Proprietar";
	$lang['mediaLabelUnknown']		= "Necunoscut";
	$lang['mediaLabelResolution']	= "Pixeli";
	$lang['mediaLabelFilesize']		= "Mărime";
	$lang['mediaLabelRunningTime']	= "Durată";
	$lang['mediaLabelFPS']			= "FPS";
	$lang['mediaLabelFormat']		= "Formatul";
	
	// Search
	$lang['search']					= "Căutare";
	$lang['searchEnterKeyMessage']	= "Vă rugăm să introduceți cuvintele cheie în câmpul de căutare, pentru a începe căutarea.";
	$lang['searchNoResults']		= "Căutarea dvs. nu a avut nici un rezultat.";
	$lang['searchDateRange']		= "Căutare în Interval de date";
	$lang['searchNarrow']			= "Restrânge rezultatele căutării";
	$lang['searchStart']			= "Începeți căutarea, mai jos!";
	$lang['searchAll']				= "Toate";
	$lang['searchClear']			= "Ștergeți căutarea";
	$lang['searchKeywords']			= "Cuvinte cheie";
	$lang['searchTitle']			= "Titlu";
	$lang['searchDescription']		= "Descriere";
	$lang['searchFilename']			= "Numele fișierului";
	$lang['searchPortrait']			= "Portret";
	$lang['searchLandscape']		= "Peisaj";
	$lang['searchSquare']			= "Pătrat";
	$lang['searchRoyaltyFree']		= "Dreptul de autor, gratuit";
	$lang['searchRightsManaged']	= "Dreptul de autor cu plată";
	$lang['searchFree']				= "Gratuit";
	$lang['searchContactUs']		= "Cu prețul -Contactați-ne";
	$lang['searchHeaderKeywords']	= "Cuvinte cheie";
	$lang['searchHeaderFields']		= "Domenii";
	$lang['searchHeaderType']		= "Tip";
	$lang['searchHeaderOrientation']= "Orientare";
	$lang['searchHeaderColor']		= "Culoare";
	$lang['searchHeaderGalleries']	= "Galerii";	
?>