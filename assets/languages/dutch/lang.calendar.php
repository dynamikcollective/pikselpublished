<?php
	# Dutch
    # CALENDAR LANG
    
	$calendar = array();
	$calendar['long_month']		= array(1 => "Januari","Februari","Maart","April","Mei","Juni","Juli","Augustus","September","Oktober","November","December");
	$calendar['short_month']	= array(1 => "Jan","Feb","Mar","Apr","Mei","Jun","Jul","Aug","Sep","Okt","Nov","Dec");
	$calendar['full_days']		= array(1 => "Zondag","Maandag","Dinsdag","Woensdag","Donderdag","Vrijdag","Zaterdag");
	$calendar['short_days']		= array(1 => "Zon","Maa","Din","Woe","Don","Vri","Zat");
?>