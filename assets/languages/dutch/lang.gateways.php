<?php
	# Dutch
    # GATEWAYS LANG
	# 4.6.3
	# STRIPE
	$lang['stripe_displayName']			= "Stripe";
	$lang['stripe_publicDescription']	= "Credit Card or Debit Card";
	$lang['stripe_pkey']				= "Publishable Key";
	$lang['stripe_pkey_d']				= "Your publishable key for your stripe account.";
	$lang['stripe_skey']				= "Secret Key";
	$lang['stripe_skey_d']				= "Your secret key for your stripe account.";
	
	# 4.1.4
	# MOLLIE IDEAL
	$lang['mollieideal_displayName']		= "iDeal";
	$lang['mollieideal_f_partnerid']		= "Partner ID";
	$lang['mollieideal_f_partnerid_d']		= "Uw iDeal Partner ID."; // "Your iDeal Partner ID.";
	$lang['mollieideal_f_profilekey']		= "Profile key";
	$lang['mollieideal_f_profilekey_d']		= "Uw Mollie iDeal Profile key."; // "Your Mollie iDeal Profile key.";
	$lang['mollieideal_f_testmode']			= "Test Modus"; // "Testing Mode";
	$lang['mollieideal_f_testmode_d']		= "Zet uw Mollie iDeal in test modus."; // "Put your iDeal payments into testing mode.";
	$lang['mollieideal_instructions']		= "Zorg ervoor dat de testmodus overeenkomt met de testmodus-instelling in uw account op Mollie.nl."; // "Make sure the testing mode is the same as the testing mode setting on your Mollie.nl profile.";
	$lang['mollieideal_publicDescription']	= "Reken veilig en vertrouwd af met iDeal."; // "Pay safe and easy using iDeal";
	
	# 4.1
	# ONEBIP 
	$lang['onebip_displayName']		        = "OneBip"; 
	$lang['onebip_merchantid']		    	= "Email";	
	$lang['onebip_merchantid_d']	    	= "Voer uw email adres in van uw onebip account."; //"Enter the email used for your onebip Account."; 
	$lang['onebip_merchantkey']		        = "API Key"; 
	$lang['onebip_merchantkey_d']	    	= "Voer uw API key in van uw onebip account."; //"Enter the API key for your onebip Account.";
	$lang['onebip_testmode']		    	= "Test Modus"; //"Test Mode";
	$lang['onebip_testmode_d']		    	= "Zet uw onebip betaling in de testmodus."; //"Put your onebip payment into testing mode.";
	$lang['onebip_publicDescription']	    = "Betaal per mobiel"; //"Pay by Mobile";
	
	
	# PAYFAST 
	$lang['payfast_displayName']		= "PayFast"; 
	$lang['payfast_merchantid']			= "Merchant ID";	
	$lang['payfast_merchantid_d']		= "Voer uw merchant ID in van uw uw Payfast Account."; //"Enter the merchant ID for your PayFast Account."; 
	$lang['payfast_merchantkey']		= "Merchant Key"; 
	$lang['payfast_merchantkey_d']		= "Voer de merchant key in van uw Payfast account."; //"Enter the merchant key for your PayFast Account.";
	$lang['payfast_testmode']			= "test Modus"; //"Test Mode"; 
	$lang['payfast_testmode_d']			= "Zet uw PayFast in de test modus."; //"Put your PayFast into testing mode.";
	$lang['payfast_publicDescription']	= "Credit kaart of bankrekening"; //"Credit Card or Bank Account"; 
	
	# NOCHEX
	$lang['nochex_displayName']			= "Nochex";
	$lang['nochex_f_accountid']			= "Account ID";
	$lang['nochex_f_accountid_d']		= "Uw Nochex Account ID."; // "Your Nochex Account ID.";
	$lang['nochex_f_testmode']			= "Test Modus"; // "Testing Mode";
	$lang['nochex_f_testmode_d']		= "Zet uw Nochex overmakingen in test modus."; // "Put your Nochex payments into testing mode.";
	$lang['nochex_publicDescription']	= "Credit of Debitcard"; // "Credit or Debit Card";
	
	# WORLDPAY
	$lang['worldpay_displayName']		= "WorldPay";
	$lang['worldpay_f_installid']		= "Installatie ID"; // "Installation ID";
	$lang['worldpay_f_installid_d']		= "Installatie ID voor uw WorldPay account."; // "Installation ID for your WorldPay account.";
	$lang['worldpay_f_testmode']		= "Test Modus"; // "Testing Mode";
	$lang['worldpay_f_testmode_d']		= "Zet uw WorldPay overmakingen in test modus."; // "Put your WorldPay payments into testing mode.";
	$lang['worldpay_publicDescription']	= "Credit of Debitcard"; // "Credit or Debit Card";
	
	# ROBOKASSA
	$lang['robokassa_displayName']		= "RoboKassa";
	$lang['robokassa_f_merchantid']		= "Merchant ID";
	$lang['robokassa_f_merchantid_d']	= "Merchant ID voor uw RoboKassa account."; // "Merchant ID for your RoboKassa account.";
	$lang['robokassa_f_merchantpass']	= "Merchant Pass"; // "Merchant Pass";
	$lang['robokassa_f_merchantpass_d']	= "Uw RoboKassa Merchant Pass ID."; // "Your RoboKassa merchant pass ID.";
	$lang['robokassa_publicDescription']= "Credit of Debitcard"; // "Credit or Debit Card";
	
	# PAYGATE
	$lang['paygate_displayName']		= "PayGate";
	$lang['paygate_f_accountid']		= "PayGate ID";
	$lang['paygate_f_accountid_d']		= "ID voor uw PayGate account."; // "ID for your PayGate account.";
	$lang['paygate_f_accountkey']		= "PayGate Key";
	$lang['paygate_f_accountkey_d']		= "Uw PayGate key ID."; // "Your PayGate key ID.";
	$lang['paygate_f_testmode']			= "Test Modus"; // "Testing Mode";
	$lang['paygate_f_testmode_d']		= "Zet uw PayGate overmakingen in test modus."; // "Put your PayGate payments into testing mode.";	
	$lang['paygate_f_testid']			= "Test PayGate ID";
	$lang['paygate_f_testid_d']			= "Uw PayGate test ID"; // "Your PayGate test id";
	$lang['paygate_f_testkey']			= "Test PayGate Key";
	$lang['paygate_f_testkey_d']		= "Uw PayGate test Key"; // "Your PayGate test Key";
	$lang['paygate_publicDescription']	= "Credit of Debitcard"; // "Credit or Debit Card";
	
	# PAYSTATION
	$lang['paystation_displayName']			= "PayStation";
	$lang['paystation_f_accountid']			= "PayStation ID";
	$lang['paystation_f_accountid_d']		= "ID voor uw PayStation account."; // "ID for your PayStation account.";
	$lang['paystation_f_testmode']			= "Test Modus"; // "Testing Mode";
	$lang['paystation_f_testmode_d']		= "Zet uw PayStation overmakingen in test modus."; // "Put your PayStation payments into testing mode.";	
	$lang['paystation_f_gatewayid']			= "Gateway ID";
	$lang['paystation_f_gatewayid_d']		= "Uw PayStation gateway ID."; // "Your PayStation gateway ID.";
	$lang['paystation_publicDescription']	= "Credit of Debitcard"; // "Credit or Debit Card";
	
	# SKRILL
	$lang['skrill_displayName']			= "Skrill (moneybookers)";
	$lang['skrill_f_email']				= "Email Adres"; // "Email Address";
	$lang['skrill_f_email_d']			= "Email adres voor uw Skrill account."; // "Email address for your Skrill account.";
	$lang['skrill_f_testmode']			= "Test Modus"; // "Testing Mode";
	$lang['skrill_f_testmode_d']		= "Zet uw Skrill overmakingen in test modus."; // "Put your Skrill payments into testing mode.";	
	$lang['skrill_f_testemail']			= "Testen Email"; // "Testing Email";
	$lang['skrill_f_testemail_d']		= "Uw Skrill test email adres."; // Your Skrill testing email address.";
	$lang['skrill_publicDescription']	= "Credit of Debitcard"; // "Credit or Debit Card";
	$lang['skrill_f_completeOrder'] 	= "Klik hier om uw bestelling af te ronden!"; // "Click here to complete your order!";
	
	# CHRONOPAY
	$lang['chronopay_displayName']		= "ChronoPay";
	$lang['chronopay_f_clientid']		= "Client ID";
	$lang['chronopay_f_clientid_d']		= "Uw ChronoPay Client ID."; // "Your ChronoPay Client ID.";
	$lang['chronopay_f_siteid']			= "Site ID";
	$lang['chronopay_f_siteid_d']		= "Uw ChronoPay Site ID."; // "Your ChronoPay Site ID.";
	$lang['chronopay_f_productid']		= "Product ID.";
	$lang['chronopay_f_productid_d']	= "Uw ChronoPay Product ID."; // "Your ChronoPay Product ID.";
	$lang['chronopay_publicDescription']= "Credit of Debitcard"; // "Credit or Debit Card";
	
	# IDEAL
	$lang['ideal_displayName']		= "iDeal";
	$lang['ideal_f_accountid']		= "Account ID";
	$lang['ideal_f_accountid_d']	= "Uw iDeal Account ID."; // "Your iDeal Account ID.";
	$lang['ideal_f_transkey']		= "Secret Key";
	$lang['ideal_f_transkey_d']		= "Uw iDeal Secret Key."; // "Your iDeal Secret Key.";
	$lang['ideal_f_testmode']		= "Test Modus"; // "Testing Mode";
	$lang['ideal_f_testmode_d']		= "Zet uw iDeal overmakingen in test modus."; // "Put your iDeal payments into testing mode.";
	$lang['ideal_publicDescription']= "Bank Rekening"; // "Bank Account";
	
	# PAYPAL
	$lang['paypal_displayName']			= "PayPal";
	$lang['paypal_f_email']				= "Email Adres"; // "Email Address";
	$lang['paypal_f_email_d']			= "Email adres voor uw PayPal account."; // "Email address for your PayPal account.";
	$lang['paypal_f_testmode']			= "Test Modus"; // "Testing Mode";
	$lang['paypal_f_testmode_d']		= "Zet uw PayPal overmakingen in test modus."; // "Put your PayPal payments into testing mode.";	
	$lang['paypal_f_testemail']			= "Test Email"; // "Testing Email";
	$lang['paypal_f_testemail_d']		= "Uw PayPal sandbox test email adres."; // "Your PayPal sandbox testing email address.";
	$lang['paypal_publicDescription']	= "PayPal, Credit Kaart of Bank Rekening"; // "PayPal, Credit Card or Bank Account";
	
	# 2CHECKOUT
	$lang['2checkout_displayName']		= "2Checkout.com";
	$lang['2checkout_f_accountid']		= "Account ID";
	$lang['2checkout_f_accountid_d']	= "Uw 2Checkout Account ID."; // "Your 2Checkout Account ID.";
	$lang['2checkout_f_testmode']		= "Test Modus"; // "Testing Mode";
	$lang['2checkout_f_testmode_d']		= "Zet uw 2Checkout overmakingen in test modus."; // "Put your 2Checkout payments into testing mode.";
	$lang['2checkout_instructions']		= "In 2Checkout.com zet 'direct return' op YES en de 'approved URL' op http://www.YOUR_DOMAIN_NAME.com/gateways/2checkout/ipn.php"; // "In 2Checkout.com set direct return to YES and set approved URL to http://www.YOUR_DOMAIN_NAME.com/gateways/2checkout/ipn.php";
	$lang['2checkout_publicDescription']= "Credit Kaart of US Bank Rekening"; // "Credit Card or US Bank Account";

	
	# Plug n' Pay
	$lang['plugnpay_displayName']		= "Plug n' Pay";
	$lang['plugnpay_f_accountid']		= "Account ID";
	$lang['plugnpay_f_accountid_d']		= "Uw Plug n' Pay Account ID."; // "Your Plug n' Pay Account ID.";
	$lang['plugnpay_publicDescription']	= "";
	
	# AUTHORIZE.NET
	$lang['authorize_displayName']		= "Authorize.net";
	$lang['authorize_f_apiid']			= "API Login ID";
	$lang['authorize_f_apiid_d']		= "Uw Authorize.net API Login ID."; // "Your Authorize.net API Login ID.";
	$lang['authorize_f_transkey']		= "Transaction Key";
	$lang['authorize_f_transkey_d']		= "Uw Authorize.net Transaction Key."; // "Your Authorize.net Transaction Key.";
	$lang['authorize_f_testmode']		= "Test Modus"; // "Testing Mode";
	$lang['authorize_f_testmode_d']		= "Zet uw Authorize.net overmakingen in test modus."; // "Put your Authorize.net payments into testing mode.";
	$lang['authorize_publicDescription']= "";
	$lang['authorize_f_completeOrder'] 	= "Klik hier om uw bestelling af te ronden!"; // "Click here to complete your order!";
	
	# MYGATE.CO.ZA
	$lang['mygate_displayName']			= "MyGate.co.za";
	$lang['mygate_f_merchantid']		= "Merchant ID";
	$lang['mygate_f_merchantid_d']		= "Uw MyGate.co.za Merchant ID."; // "Your MyGate.co.za Merchant ID.";
	$lang['mygate_f_appid']				= "Application ID";
	$lang['mygate_f_appid_d']			= "Uw MyGate.co.za Application ID."; // "Your MyGate.co.za Application ID.";
	$lang['mygate_publicDescription']	= "";
	$lang['mygate_f_testmode']			= "Test Modus"; // "Testing Mode";
	$lang['mygate_f_testmode_d']		= "Zet uw Mygate overmakingen in test modus."; // "Put your Mygate payments into testing mode.";	
	
	# MAIL IN PAYMENT
	$lang['mailin_displayName']			= "Mail In Payment";
	$lang['mailin_f_instructions']		= "Mail In Payment Intructies"; // "Mail In Payment Intructions";
	$lang['mailin_f_instructions_d']	= "Instructies voor klanten om betalingen te versturen via mail, zoals cheques of postwissels."; // "Instructions for customers to send mail in payments such as checks or money orders.";
	$lang['mailin_publicDescription']	= "Mail uw betaling naar het adres voorzien op de volgende pagina."; // "Mail your payment to the address provided on the next page.";
?>