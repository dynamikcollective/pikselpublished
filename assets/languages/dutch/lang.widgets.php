<?php
	# Dutch
    # WIDGETS LANG
    
	# 4.4.6
	$wplang['stats_visitors']		= "Geschatte Site bezoeken";
	
    # 4.1.3
	$wplang['qstats_pending_media']	= "Aanbieder Media in Afwachting"; //"Pending Contributor Media";
	
	# 4.0.7	
	$wplang['sitehealth_exif']	= "PHP EXIF Ondersteuning (exif_read_data)"; //"PHP EXIF Support (exif_read_data)";
	
	# GENERAL WIDGET LANGUAGE
	$wplang['widget_close']		= "Sluit"; // "Close";
	$wplang['widget_save']		= "Opslaan"; // "Save";
	$wplang['load_failed'] 		= "Laden van Configuratiescherm Mislukt!"; // "Loading Panel Failed!";
	$wplang['widget_title']		= "Titel"; // "Title";
	$wplang['widget_note']		= "Opmerking"; // "Note";
	$wplang['widget_for']		= "Voor"; // "For";
	
	# NOTES WIDGET
	$wplang['notes_title']		= "Opmerkingen"; // "Notes";
	$wplang['notes_newnote']	= "Nieuwe Opmerking"; // "New Note";
	$wplang['notes_postedby']	= "Geplaatst Door"; // "Posted By";
	$wplang['notes_lastupdate']	= "Laatst Aangepast"; // "Last Updated";
	$wplang['notes_nonotes']	= "Er zijn geen opmerkingen weer te geven"; // "There are no notes to display";
	
	# EXTRAS WIDGET
	$wplang['extras_title']		= "PhotoStore Extra's"; // "PhotoStore Extras";
	
	# KTOOLS ACCOUNT WIDGET
	$wplang['kaccount_title']	= "Ktools Account"; // "Ktools Account";
	$wplang['kaccount_support']	= "Resterende Dagen Hulpondersteuning/Upgrades"; // "Support/Upgrade Days Remaining";
	$wplang['kaccount_messages']= "Ongelezen Berichten"; // "Unread Messages";
	$wplang['kaccount_affil']	= "Partner Verkopen Sinds Laatste Login"; // "Affiliate Sales Since Last Login";
	
	# KNEWS WIDGET
	$wplang['knews_title']		= "Ktools.net Nieuws"; // "Ktools.net News";
	
	# QUICK STATS WIDGET - NEW AND PENDING
	$wplang['qstats_title']		= "Nieuw & in Afwachting"; // "New & Pending";
	$wplang['qstats_logmem']	= "Leden Sinds Laatste Login"; // "Members Since Last Login";
	$wplang['qstats_penmem']	= "Leden in Afwachting"; // "Pending Members";
	$wplang['qstats_logorders']	= "Bestellingen Sinds Laatste Login"; // "Orders Since Last Login";
	$wplang['qstats_penorders']	= "Bestellingen in Afwachting"; // "Pending Orders";
	$wplang['qstats_logcomm']	= "Reacties Sinds Laatste Login"; // "Comments Since Last Login";
	$wplang['qstats_pencomm']	= "Reacties in Afwachting"; // "Pending Comments";
	$wplang['qstats_logtags']	= "Trefwoorden Sinds Laatste Login"; // "Tags Since Last Login";
	$wplang['qstats_pentags']	= "Trefwoorden in Afwachting"; // "Pending Tags";
	$wplang['qstats_lograte']	= "Beoordelingen Sinds Laatste Login"; // "Ratings Since Last Login";
	$wplang['qstats_penrate']	= "Beoordelingen in Afwachting"; // "Pending Ratings";
	$wplang['qstats_penbios']	= "Bio's van Leden in Afwachting"; // "Pending Member Bios";
	$wplang['qstats_penavatars']= "Avatars in Afwachting"; // "Pending Avatars";
	$wplang['qstats_pensupport']= "Hulpaanvraag Tickets in Afwachting"; // "Pending Support Tickets";
	
	# STATS WIDGET
	$wplang['stats_title']		= "Statistieken"; // "Stats";
	$wplang['stats_op1']		= "Verkopen"; // "Sales";
	$wplang['stats_op2']		= "Leden"; // "Members";
	$wplang['stats_op_7days']	= "Laatste 7 Dagen"; // "Last 7 Days";
	$wplang['stats_op_6mon']	= "Laatste 6 Maanden"; // "Last 6 Months";
	$wplang['stats_op_5year']	= "Laatste 5 Jaar"; // "Last 5 Years";
	$wplang['stats_tdsales']	= "Verkopen van Vandaag"; // "Todays Sales";
	$wplang['stats_tdorders']	= "Bestellingen van Vandaag"; // "Todays Orders";
	$wplang['stats_sales']		= "Verkopen"; // "Sales";
	$wplang['stats_orders']		= "Bestellingen"; // "Orders";	
	$wplang['stats_atsales']	= "Verkopen Aller Tijden"; // "All Time Sales";
	$wplang['stats_atorders']	= "Bestellingen Aller Tijden"; // "All Time Orders";
	$wplang['stats_tdmems']		= "Nieuwe Leden van Vandaag"; // "New Members Today";
	$wplang['stats_mems']		= "Nieuwe Leden"; // "New Members";
	$wplang['stats_tamems']		= "Totaal Aantal Actieve Leden"; // "Total Active Members";
	$wplang['stats_timems']		= "Totaal Aantal Inactieve Leden"; // "Total Inactive Members";
	
	
	# SITE HEALTH WIDGET
	$wplang['sitehealth_title']	= "Site & Server Gezondheid"; // "Site & Server Health";
	$wplang['sitehealth_ok']	= "OK"; // "OK";
	$wplang['sitehealth_low']	= "LAAG"; // "LOW";
	$wplang['sitehealth_high']	= "HOOG"; // "HIGH";
	$wplang['sitehealth_failed']= "MISLUKT"; // "FAILED";
	$wplang['sitehealth_unava'] = "Niet Beschikbaar"; // "Unavailable";
	$wplang['sitehealth_off']	= "AAN"; // "OFF";
	$wplang['sitehealth_on']	= "AF"; // "ON";
	$wplang['sitehealth_inst']	= "Geïnstalleerd"; // "Installed";
	$wplang['sitehealth_none']	= "Geen"; // "None";
	$wplang['sitehealth_exists']= "Bestaat"; // "Exists";
	$wplang['sitehealth_write']	= "Schrijfbaar"; // "Writable";
	$wplang['sitehealth_nonwri']= "Niet Schrijfbaar"; // "Not Writable";
	$wplang['sitehealth_php']	= "PHP Versie"; // "PHP Version";
	$wplang['sitehealth_gd']	= "GD Library";
	$wplang['sitehealth_mem']	= "PHP Memory Limit";
	$wplang['sitehealth_exe']	= "PHP max_execution_time";
	$wplang['sitehealth_time']	= "PHP max_input_time";
	$wplang['sitehealth_file']	= "PHP upload_max_filesize";
	$wplang['sitehealth_post']	= "PHP post_max_size";
	$wplang['sitehealth_safe']	= "PHP safe_mode";
	$wplang['sitehealth_dbv']	= "Database/Produkt Versie Check"; // "Database/Product Version Check";
	$wplang['sitehealth_mysqlv']= "MySQL Versie"; // "MySQL Version";
	$wplang['sitehealth_load']	= "Server Load";
	$wplang['sitehealth_upti']	= "Server Uptime";
	
	# UPDATE CHECK WIDGET
	$wplang['updater_title']	= "Controleer op Updates"; // "Check For Updates";
	$wplang['updater_newest']	= "U heeft de nieuwste versie"; // "You have the newest version";
	$wplang['updater_update']	= "Update Beschikbaar"; // "Update Available";
	$wplang['updater_newestis']	= "De nieuwste versie is"; // "The newest version is"; 
	$wplang['updater_getnew']	= "Om de nieuwste versie te verkrijgen kunt u inloggen op uw <a href='http://www.ktools.net/members/' target='_blank'>Ktools.net account</a>"; // "To get the newest version please login to your <a href='http://www.ktools.net/members/' target='_blank'>Ktools.net account</a>";
	$wplang['updater_yourv']	= "U werkt met versie"; // "You are running version";
	
	# BLANK WIDGET
	$wplang['blank_title']		= "Leeg Configuratiescherm"; // "Blank Panel";
	
	# CALENDAR WIDGET
	$wplang['calendar_title']	= "Kalender"; // "Calendar";
	
	# TIPS CHECK WIDGET
	//$wplang['tips_title']		= "Tips";
	//$wplang['tips_next']		= "Next Tip";
	//$welcometip[] 				= "Did you know you can use <strong>Ctrl+Shift+S</strong> to open and close the shortcuts menu?"; 
	//$welcometip[] 				= "You can drag these welcome panels to rearrange them the way you would like. Just click on the title and drag. Then drop the panel when it is in the order that you would like."; 
	//$welcometip[] 				= "For quick access to your galleries and many other areas click on the tab with the arrow in the upper left. This will open the shortcuts menu. Click the tab again to close it."; 
	
?>