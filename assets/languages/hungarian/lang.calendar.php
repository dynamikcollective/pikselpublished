<?php
	# HUNGARIAN
	$calendar = array();
	$calendar['long_month']		= array(1 => "Január","Február","Március","Április","Május","Június","Július","Augusztus","Szeptember","Október","November","December");
	$calendar['short_month']	= array(1 => "Jan","Feb","Már","Ápr","Máj","Jún","Júl","Aug","Szep","Okt","Nov","Dec");
	$calendar['full_days']		= array(1 => "Vasárnap","Hétfő","Kedd","Szerda","Csütörtök","Péntek","Szombat");
	$calendar['short_days']		= array(1 => "Va","Hé","Ke","Sze","Csü","Pé","Szo");
?>