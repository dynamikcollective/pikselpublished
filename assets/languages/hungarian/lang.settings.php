<?php
	# HUNGARIAN	
	$langset['version'] 			= "4.2.2";		// Version number this language file is translated for
	$langset['active'] 				= 1;			// Allow the language to be selected in the management area
	$langset['translatedBy'] 		= '';			// Method or person who translated the language
	$langset['mgmtAreaTrans'] 		= 1;			// Management area also translated
	$langset['lang_charset'] 		= "utf-8";		// The required character set for your language text.	
	$langset['id']					= "hungarian"; 	// Language ID - MUST MATCH THE DIRECORY NAME EXACTLY - NO SPACES - ALL LOWERCASE
	$langset['locale']				= "hu_HU"; 		// Language Locale
	$langset['xmlLangCode']			= "hu"; 		// Language Locale
	$langset['name']				= "Magyar"; 	// Language Name That Gets Displayed

	$langset['date_format']			= "HU"; 		// US (US Date 2007/12/31), EURO (European Date 2007/12/31), INT (World Date 2007/12/31)
	$langset['clock_format']		= "12"; 		// 12 or 24 (hours)
?>