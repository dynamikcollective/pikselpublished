<?php
	# 4.4.6
	$wplang['stats_visitors']		= "Approximate Site Visits";
	
	# 4.1.3
	$wplang['qstats_pending_media']	= "Материалы, ожидающие авторов";
	
	# 4.0.7	
	$wplang['sitehealth_exif']	= "Поддержка PHP EXIF (exif_read_data)";
	
	# GENERAL WIDGET LANGUAGE
	$wplang['widget_close']		= "Закрыть";
	$wplang['widget_save']		= "Сохранить";
	$wplang['load_failed'] 		= "Загрузка панели не удалась!";
	$wplang['widget_title']		= "Название";
	$wplang['widget_note']		= "Примечание";
	$wplang['widget_for']		= "Для";
	
	# NOTES WIDGET
	$wplang['notes_title']		= "Примечания";
	$wplang['notes_newnote']	= "Новое примечание";
	$wplang['notes_postedby']	= "Опубликовано от";
	$wplang['notes_lastupdate']	= "Последнее обновление";
	$wplang['notes_nonotes']	= "Нет заметок для отображения";
	
	# EXTRAS WIDGET
	$wplang['extras_title']		= "PhotoStore Дополнительно";
	
	# KTOOLS ACCOUNT WIDGET
	$wplang['kaccount_title']	= "Ktools Акаунт";
	$wplang['kaccount_support']	= "Поддержка/Обновление осталось дней";
	$wplang['kaccount_messages']= "Непрочитанные сообщения";
	$wplang['kaccount_affil']	= "Партнерские продажи с последнего входа";
	
	# KNEWS WIDGET
	$wplang['knews_title']		= "Ktools.net Новости";
	
	# QUICK STATS WIDGET - NEW AND PENDING
	$wplang['qstats_title']		= "Новые & Ожидание";
	$wplang['qstats_logmem']	= "Пользователи с последнего входа";
	$wplang['qstats_penmem']	= "Ожидающие пользователи";
	$wplang['qstats_logorders']	= "Заказы с последнего входа";
	$wplang['qstats_penorders']	= "Отложенные заказы";
	$wplang['qstats_logcomm']	= "Комментарии с последнего входа";
	$wplang['qstats_pencomm']	= "Находящиеся на рассмотрении комментарии";
	$wplang['qstats_logtags']	= "Теги с последнего входа";
	$wplang['qstats_pentags']	= "Ожидающие Теги";
	$wplang['qstats_lograte']	= "Рейтинги с последнего входа";
	$wplang['qstats_penrate']	= "Ожидающие рейтинги";
	$wplang['qstats_penbios']	= "Ожидающий участник";
	$wplang['qstats_penavatars']= "Ожидающие аватары";
	$wplang['qstats_pensupport']= "Ожидающие поддержку билеты";
	
	# STATS WIDGET
	$wplang['stats_title']		= "Статистика";
	$wplang['stats_op1']		= "Продажи";
	$wplang['stats_op2']		= "Участники";
	$wplang['stats_op_7days']	= "За последние 7 дней";
	$wplang['stats_op_6mon']	= "Последние 6 месяцев";
	$wplang['stats_op_5year']	= "Последние 5 лет";
	$wplang['stats_tdsales']	= "Сегодняшние продаж";
	$wplang['stats_tdorders']	= "Сегодняшние заказы";
	$wplang['stats_sales']		= "Продажи";
	$wplang['stats_orders']		= "Заказы";	
	$wplang['stats_atsales']	= "Все время продажи";
	$wplang['stats_atorders']	= "Все время заказы";
	$wplang['stats_tdmems']		= "Новых участников за сегодня";
	$wplang['stats_mems']		= "Новые участники";
	$wplang['stats_tamems']		= "Всего активных участников";
	$wplang['stats_timems']		= "Всего неактивных членов";
	
	
	# SITE HEALTH WIDGET
	$wplang['sitehealth_title']	= "Сайт & Исправность сервера";
	$wplang['sitehealth_ok']	= "ХОРОШО";
	$wplang['sitehealth_low']	= "НИЗКИЙ";
	$wplang['sitehealth_high']	= "ВЫСОКИЙ";
	$wplang['sitehealth_failed']= "НЕ УДАЛОСЬ";
	$wplang['sitehealth_unava'] = "Недоступно";
	$wplang['sitehealth_off']	= "ВЫКЛ";
	$wplang['sitehealth_on']	= "ВКЛ";
	$wplang['sitehealth_inst']	= "установлен";
	$wplang['sitehealth_none']	= "Нет";
	$wplang['sitehealth_exists']= "Существует";
	$wplang['sitehealth_write']	= "Записываемые";
	$wplang['sitehealth_nonwri']= "Не записываемые";
	$wplang['sitehealth_php']	= "PHP Версия";
	$wplang['sitehealth_gd']	= "Библиотека GD";
	$wplang['sitehealth_mem']	= "Лимит памяти PHP";
	$wplang['sitehealth_exe']	= "PHP max_execution_time";
	$wplang['sitehealth_time']	= "PHP max_input_time";
	$wplang['sitehealth_file']	= "PHP upload_max_filesize";
	$wplang['sitehealth_post']	= "PHP post_max_size";
	$wplang['sitehealth_safe']	= "PHP safe_mode";
	$wplang['sitehealth_dbv']	= "База данных/Версия продукта Проверить";
	$wplang['sitehealth_mysqlv']= "MySQL Версия";
	$wplang['sitehealth_load']	= "Сервер Загрузка";
	$wplang['sitehealth_upti']	= "Время работы сервера";
	
	# UPDATE CHECK WIDGET
	$wplang['updater_title']	= "Проверить наличие обновлений";
	$wplang['updater_newest']	= "У вас установлена новейшая версия";
	$wplang['updater_update']	= "Доступно обновление";
	$wplang['updater_newestis']	= "Новейшая версия"; 
	$wplang['updater_getnew']	= "Чтобы получить последнюю версию пожалуйста, войдите в свой <a href='http://www.ktools.net/members/' target='_blank'>Ktools.net акаунт</a>";
	$wplang['updater_yourv']	= "Вы используете версию";
	
	# BLANK WIDGET
	$wplang['blank_title']		= "Пустая панель";
	
	# CALENDAR WIDGET
	$wplang['calendar_title']	= "Календарь";
	
	# TIPS CHECK WIDGET
	//$wplang['tips_title']		= "Советы";
	//$wplang['tips_next']		= "Следующий совет";
	//$welcometip[] 				= "Знаете ли вы, что можете использовать <strong> Ctrl + Shift + S </ STRONG>, чтобы открыть и закрыть меню?"; 
	//$welcometip[] 				= "Вы можете перетаскивать эти панели, чтобы изменить их так, как вам хотелось бы. Просто нажмите на название и перетащите. Затем опустите панель, когда он в том месте где хотели."; 
	//$welcometip[] 				= "Для быстрого доступа к вашей галереи и многим другим областям, перейдите на вкладку со стрелками в левом верхнем углу. Это откроет меню. Перейдите на вкладку еще раз, чтобы закрыть её."; 
	
?>