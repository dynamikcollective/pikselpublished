<?php
	# GATEWAYS LANG
	
	# 4.1.4
	# MOLLIE IDEAL
	$lang['mollieideal_displayName']		= "iDeal (Mollie)";
	$lang['mollieideal_f_partnerid']		= "ID партнера";
	$lang['mollieideal_f_partnerid_d']		= "Ваш партнерский ID iDeal.";
	$lang['mollieideal_f_profilekey']		= "Профиль ключа";
	$lang['mollieideal_f_profilekey_d']		= "Ваш профиль ключа Mollie iDeal.";
	$lang['mollieideal_f_testmode']			= "Тестовый режим";
	$lang['mollieideal_f_testmode_d']		= "Вкючить Mollie iDeal в тестовом режиме.";
	$lang['mollieideal_instructions']		= "Убедитесь, что тестовый режим соответствует параметрам вашего счета в Mollie.nl.";
	$lang['mollieideal_publicDescription']	= "Оплата безопасна и проста с помощью iDeal (доступно только в Нидерландах).";
	
	# 4.1
	# ONEBIP 
	$lang['onebip_displayName']			= "OneBip"; 
	$lang['onebip_merchantid']			= "Email";	
	$lang['onebip_merchantid_d']		= "Введите email для использования в вашем счете onebip."; 
	$lang['onebip_merchantkey']			= "API ключ"; 
	$lang['onebip_merchantkey_d']		= "Введите API ключ для вашего счета onebip.";
	$lang['onebip_testmode']			= "Тестовый режим";
	$lang['onebip_testmode_d']			= "Проверте ваш счет onebip в тестовом режиме.";
	$lang['onebip_publicDescription']	= "Мобильная оплата";
	
	# PAYFAST 
	$lang['payfast_displayName']		= "PayFast"; 
	$lang['payfast_merchantid']			= "ID продавца";	
	$lang['payfast_merchantid_d']		= "Введите ID продавца для вашего PayFast счета."; 
	$lang['payfast_merchantkey']		= "Ключ продавца"; 
	$lang['payfast_merchantkey_d']		= "Введите ключ продавца для PayFast счета.";
	$lang['payfast_testmode']			= "Тестовый режим"; 
	$lang['payfast_testmode_d']			= "Проверьте ваш PayFast в тестовом режиме.";
	$lang['payfast_publicDescription']	= "Кредитная карта или банковский счет"; 
	
	# NOCHEX
	$lang['nochex_displayName']			= "Nochex";
	$lang['nochex_f_accountid']			= "ID счета";
	$lang['nochex_f_accountid_d']		= "Ваш ID счета Nochex.";
	$lang['nochex_f_testmode']			= "Тестовый режим";
	$lang['nochex_f_testmode_d']		= "Проверьте ваш Nochex платежи в тестовом режиме.";
	$lang['nochex_publicDescription']	= "Кредитная или дебетовая карта";
	
	# WORLDPAY
	$lang['worldpay_displayName']		= "WorldPay";
	$lang['worldpay_f_installid']		= "Получение ID";
	$lang['worldpay_f_installid_d']		= "Получение ID для вашего счета WorldPay.";
	$lang['worldpay_f_testmode']		= "Тестовый режим";
	$lang['worldpay_f_testmode_d']		= "Проверьте ваш счет WorldPay в тестовом режиме.";
	$lang['worldpay_publicDescription']= "Кредитная или дебетовая карта";
	
	# ROBOKASSA
	$lang['robokassa_displayName']		= "RoboKassa";
	$lang['robokassa_f_merchantid']		= "ID продавца";
	$lang['robokassa_f_merchantid_d']	= "ID продавца для вашего счета RoboKassa.";
	$lang['robokassa_f_merchantpass']	= "Ваш пароль";
	$lang['robokassa_f_merchantpass_d']	= "ID пароль продавца RoboKassa.";
	$lang['robokassa_publicDescription']= "Кредитная или дебетовая карта";
	
	# PAYGATE
	$lang['paygate_displayName']		= "PayGate";
	$lang['paygate_f_accountid']		= "PayGate ID";
	$lang['paygate_f_accountid_d']		= "ID для вашего PayGate счета.";
	$lang['paygate_f_accountkey']		= "PayGate Ключ";
	$lang['paygate_f_accountkey_d']		= "Ваш PayGate ID ключа.";
	$lang['paygate_f_testmode']			= "Тестовый режим";
	$lang['paygate_f_testmode_d']		= "Проверьте ваш PayGate платежи в тестовом режиме.";	
	$lang['paygate_f_testid']			= "Тестирование PayGate ID";
	$lang['paygate_f_testid_d']			= "Ваш PayGate id тест";
	$lang['paygate_f_testkey']			= "Тест PayGate Ключа";
	$lang['paygate_f_testkey_d']		= "Ваш PayGate тест Ключа";
	$lang['paygate_publicDescription']	= "Кредитная или дебетовая карта";
	
	# PAYSTATION
	$lang['paystation_displayName']		= "PayStation";
	$lang['paystation_f_accountid']		= "PayStation ID";
	$lang['paystation_f_accountid_d']	= "ID для вашего PayStation счета.";
	$lang['paystation_f_testmode']		= "Тестовый режим";
	$lang['paystation_f_testmode_d']	= "Проверьте ваши платежи PayStation в тестовом режиме.";	
	$lang['paystation_f_gatewayid']		= "ID шлюза";
	$lang['paystation_f_gatewayid_d']	= "Ваш PayStation ID шлюза.";
	$lang['paystation_publicDescription']= "Кредитная или дебетовая карта";
	
	# SKRILL
	$lang['skrill_displayName']			= "Skrill (moneybookers)";
	$lang['skrill_f_email']				= "Email адрес";
	$lang['skrill_f_email_d']			= "Email адрес для вашего Skrill счета.";
	$lang['skrill_f_testmode']			= "Тестовый режим";
	$lang['skrill_f_testmode_d']		= "Проверьте ваш Skrill платежи в тестовом режиме.";	
	$lang['skrill_f_testemail']			= "Проверка Email";
	$lang['skrill_f_testemail_d']		= "Тестирование вашего Skrill email адреса.";
	$lang['skrill_publicDescription']	= "Кредитная или дебетовая карта";
	$lang['skrill_f_completeOrder']		= "Нажмите здесь, чтобы выполнить Ваш заказ!";
	
	# CHRONOPAY
	$lang['chronopay_displayName']		= "ChronoPay";
	$lang['chronopay_f_clientid']		= "ID клиента";
	$lang['chronopay_f_clientid_d']		= "Ваш клиентский ID ChronoPay.";
	$lang['chronopay_f_siteid']			= "ID сайта";
	$lang['chronopay_f_siteid_d']		= "Ваш ID сайта ChronoPay.";
	$lang['chronopay_f_productid']		= "ID продукта.";
	$lang['chronopay_f_productid_d']	= "Ваш ID продукта ChronoPay.";
	$lang['chronopay_publicDescription']= "Кредитная или дебетовая карта";
	
	# IDEAL
	$lang['ideal_displayName']			= "iDeal (ING)";
	$lang['ideal_f_accountid']			= "Номер счета";
	$lang['ideal_f_accountid_d']		= "Ваш ID счета iDeal.";
	$lang['ideal_f_transkey']			= "Секретный ключ";
	$lang['ideal_f_transkey_d']			= "Ваш секретный ключ Your iDeal.";
	$lang['ideal_f_testmode']			= "Тестовый режим";
	$lang['ideal_f_testmode_d']			= "Проверьте ваши iDeal платежи в тестовом режиме.";
	$lang['ideal_publicDescription']	= "Банковский счет";
	
	# PAYPAL
	$lang['paypal_displayName']			= "PayPal";
	$lang['paypal_f_email']				= "Email адрес";
	$lang['paypal_f_email_d']			= "Email адрес для вашего PayPal счета.";
	$lang['paypal_f_testmode']			= "Тестовый режим";
	$lang['paypal_f_testmode_d']		= "Проверьте ваши PayPal платежи в тестовом режиме.";	
	$lang['paypal_f_testemail']			= "Проверить Email";
	$lang['paypal_f_testemail_d']		= "Проверка вашего email адреса PayPal аккаунта.";
	$lang['paypal_publicDescription']	= "PayPal, Кредитная карта или банковский счет";
	
	# 2CHECKOUT
	$lang['2checkout_displayName']		= "2Checkout.com";
	$lang['2checkout_f_accountid']		= "Номер счета";
	$lang['2checkout_f_accountid_d']	= "Ваш IDсчета 2Checkout.";
	$lang['2checkout_f_testmode']		= "Тестовый режим";
	$lang['2checkout_f_testmode_d']		= "Проверьте ваши платежи 2Checkout в тестовом режиме.";
	$lang['2checkout_instructions']		= "In 2Checkout.com set direct return to 'Header Redirect (Your URL)' and set approved URL to http://www.YOUR_DOMAIN_NAME.com/assets/gateways/2checkout/ipn.php";
	$lang['2checkout_publicDescription']= "Кредитная карта или счет в банке США";

	
	# Plug n' Pay
	$lang['plugnpay_displayName']		= "Plug n' Pay";
	$lang['plugnpay_f_accountid']		= "Номер счета";
	$lang['plugnpay_f_accountid_d']		= "Ваш ID счета Plug n' Pay.";
	$lang['plugnpay_publicDescription']	= "";
	
	# AUTHORIZE.NET
	$lang['authorize_displayName']		= "Authorize.net";
	$lang['authorize_f_apiid']			= "API Логин ID";
	$lang['authorize_f_apiid_d']		= "Ваш API Логин ID Authorize.net.";
	$lang['authorize_f_transkey']		= "Ключ транзакции";
	$lang['authorize_f_transkey_d']		= "Ваш ключ транзакции Authorize.net.";
	$lang['authorize_f_testmode']		= "Тестовый режим";
	$lang['authorize_f_testmode_d']		= "Проверьте в тестовом режиме платежи Authorize.net.";
	$lang['authorize_publicDescription']= "";
	$lang['authorize_f_completeOrder'] = "Нажмите здесь, чтобы выполнить Ваш заказ!";
	
	# MYGATE.CO.ZA
	$lang['mygate_displayName']			= "MyGate.co.za";
	$lang['mygate_f_merchantid']		= "ID Продавца";
	$lang['mygate_f_merchantid_d']		= "Ваш ID продавца MyGate.co.za.";
	$lang['mygate_f_appid']				= "ID приложения";
	$lang['mygate_f_appid_d']			= "Ваш ID приложения MyGate.co.za.";
	$lang['mygate_publicDescription']	= "";
	$lang['mygate_f_testmode']			= "Тестовый режим";
	$lang['mygate_f_testmode_d']		= "Проверьте ваши Mygate платежи в тестовом режиме.";	
	
	# MAIL IN PAYMENT
	$lang['mailin_displayName']			= "Mail In Payment";
	$lang['mailin_f_instructions']		= "Инстроукция Mail In Payment";
	$lang['mailin_f_instructions_d']	= "Инструкция для клиентов, чтобы отправить письмо c платежами, таких как чеки или денежные переводы.";
	$lang['mailin_publicDescription']	= "Почта платежа по адресу, указанному на следующей странице.";
?>