<?php
	# HRVATSKI
	$calendar = array();
	$calendar['long_month']		= array(1 => "Siječanj","Veljača","Ožujak","Travanj","Svibanj","Lipanj","Srpanj","Kolovoz","Rujan","Listopad","Studeni","Prosinac");
	$calendar['short_month']	= array(1 => "Sij","Velj","Ožu","Trav","Svib","Lip","Srp","Kol","Ruj","Lis","Stu","Pro");
	$calendar['full_days']		= array(1 => "Nedjelja","Ponedjeljak","Utorak","Srijeda","Četvrtak","Petak","Subota");
	$calendar['short_days']		= array(1 => "Ned","Pon","Uto","Sri","Čet","Pet","Sub");
?>