<?php
	# 4.4.6
	$wplang['stats_visitors']		= "Approximate Site Visits";
	
	# 4.1.3
	$wplang['qstats_pending_media']	= "Pending Contributor Media";
	
	# 4.0.7
	$wplang['sitehealth_exif'] = "Supporte PHP EXIF  (exif_read_data)";
	
	# GENERAL WIDGET LANGUAGE
	$wplang['widget_close']		= "Zapri";
	$wplang['widget_save']		= "Shrani";
	$wplang['load_failed'] 		= "Napaka pri nalaganju!";
	$wplang['widget_title']		= "Naslov";
	$wplang['widget_note']		= "Opomba";
	$wplang['widget_for']		= "Za";
	
	# NOTES WIDGET
	$wplang['notes_title']		= "Opombe";
	$wplang['notes_newnote']	= "Nova opomba";
	$wplang['notes_postedby']	= "Napisal";
	$wplang['notes_lastupdate']	= "Nazadnje posodobljeno";
	$wplang['notes_nonotes']	= "Ni opomb za prikaz";
	
	# EXTRAS WIDGET
	$wplang['extras_title']		= "PhotoStore Extras";
	
	# KTOOLS ACCOUNT WIDGET
	$wplang['kaccount_title']	= "Ktools Account";
	$wplang['kaccount_support']	= "Support/Upgrade Days Remaining";
	$wplang['kaccount_messages']= "Unread Messages";
	$wplang['kaccount_affil']	= "Affiliate Sales Since Last Login";
	
	# KNEWS WIDGET
	$wplang['knews_title']		= "Ktools.net News";
	
	# QUICK STATS WIDGET - NEW AND PENDING
	$wplang['qstats_title']		= "New & Pending";
	$wplang['qstats_logmem']	= "Members Since Last Login";
	$wplang['qstats_penmem']	= "Pending Members";
	$wplang['qstats_logorders']	= "Orders Since Last Login";
	$wplang['qstats_penorders']	= "Pending Orders";
	$wplang['qstats_logcomm']	= "Comments Since Last Login";
	$wplang['qstats_pencomm']	= "Pending Comments";
	$wplang['qstats_logtags']	= "Tags Since Last Login";
	$wplang['qstats_pentags']	= "Pending Tags";
	$wplang['qstats_lograte']	= "Ratings Since Last Login";
	$wplang['qstats_penrate']	= "Pending Ratings";
	$wplang['qstats_penbios']	= "Pending Member Bios";
	$wplang['qstats_penavatars']= "Pending Avatars";
	$wplang['qstats_pensupport']= "Pending Support Tickets";
	
	# STATS WIDGET
	$wplang['stats_title']		= "Stats";
	$wplang['stats_op1']		= "Sales";
	$wplang['stats_op2']		= "Members";
	$wplang['stats_op_7days']	= "Last 7 Days";
	$wplang['stats_op_6mon']	= "Last 6 Months";
	$wplang['stats_op_5year']	= "Last 5 Years";
	$wplang['stats_tdsales']	= "Todays Sales";
	$wplang['stats_tdorders']	= "Todays Orders";
	$wplang['stats_sales']		= "Sales";
	$wplang['stats_orders']		= "Orders";	
	$wplang['stats_atsales']	= "All Time Sales";
	$wplang['stats_atorders']	= "All Time Orders";
	$wplang['stats_tdmems']		= "New Members Today";
	$wplang['stats_mems']		= "New Members";
	$wplang['stats_tamems']		= "Total Active Members";
	$wplang['stats_timems']		= "Total Inactive Members";
	
	
	# SITE HEALTH WIDGET
	$wplang['sitehealth_title']	= "Site & Server Health";
	$wplang['sitehealth_ok']	= "OK";
	$wplang['sitehealth_low']	= "LOW";
	$wplang['sitehealth_high']	= "HIGH";
	$wplang['sitehealth_failed']= "FAILED";
	$wplang['sitehealth_unava'] = "Unavailable";
	$wplang['sitehealth_off']	= "OFF";
	$wplang['sitehealth_on']	= "ON";
	$wplang['sitehealth_inst']	= "Installed";
	$wplang['sitehealth_none']	= "None";
	$wplang['sitehealth_exists']= "Exists";
	$wplang['sitehealth_write']	= "Writable";
	$wplang['sitehealth_nonwri']= "Not Writable";
	$wplang['sitehealth_php']	= "PHP Version";
	$wplang['sitehealth_gd']	= "GD Library";
	$wplang['sitehealth_mem']	= "PHP Memory Limit";
	$wplang['sitehealth_exe']	= "PHP max_execution_time";
	$wplang['sitehealth_time']	= "PHP max_input_time";
	$wplang['sitehealth_file']	= "PHP upload_max_filesize";
	$wplang['sitehealth_post']	= "PHP post_max_size";
	$wplang['sitehealth_safe']	= "PHP safe_mode";
	$wplang['sitehealth_dbv']	= "Database/Product Version Check";
	$wplang['sitehealth_mysqlv']= "MySQL Version";
	$wplang['sitehealth_load']	= "Server Load";
	$wplang['sitehealth_upti']	= "Server Uptime";
	
	# UPDATE CHECK WIDGET
	$wplang['updater_title']	= "Check For Updates";
	$wplang['updater_newest']	= "You have the newest version";
	$wplang['updater_update']	= "Update Available";
	$wplang['updater_newestis']	= "The newest version is"; 
	$wplang['updater_getnew']	= "To get the newest version please login to your <a href='http://www.ktools.net/members/' target='_blank'>Ktools.net account</a>";
	$wplang['updater_yourv']	= "You are running version";
	
	# BLANK WIDGET
	$wplang['blank_title']		= "Blank Panel";
	
	# CALENDAR WIDGET
	$wplang['calendar_title']	= "Calendar";
	
	# TIPS CHECK WIDGET
	//$wplang['tips_title']		= "Tips";
	//$wplang['tips_next']		= "Next Tip";
	//$welcometip[] 				= "Did you know you can use <strong>Ctrl+Shift+S</strong> to open and close the shortcuts menu?"; 
	//$welcometip[] 				= "You can drag these welcome panels to rearrange them the way you would like. Just click on the title and drag. Then drop the panel when it is in the order that you would like."; 
	//$welcometip[] 				= "For quick access to your galleries and many other areas click on the tab with the arrow in the upper left. This will open the shortcuts menu. Click the tab again to close it."; 
	
?>