<?php
	# SWEDISH	
	$langset['version'] 			= "4.1";		// Version number this language file is translated for
	$langset['active'] 				= 1;			// Allow the language to be selected in the management area
	$langset['translatedBy'] 		= 'Ktools Member - Anders Lindblom';		// Method or person who translated the language
	$langset['mgmtAreaTrans'] 		= 0;			// Management area also translated
	$langset['lang_charset'] 		= "utf-8";		// The required character set for your language text.	
	$langset['id']					= "swedish"; 	// Language ID - MUST MATCH THE DIRECORY NAME EXACTLY - NO SPACES - ALL LOWERCASE
	$langset['locale']				= "sv_SE"; 		// Language Locale
	$langset['xmlLangCode']			= "sv"; 		// Language Locale
	$langset['name']				= "Swedish"; 	// Language Name That Gets Displayed
	
	$langset['time_zome']			= "1";			// -12 to 12
	$langset['daylight_savings']	= 1;			// Adjust date and time for daylight savings
	$langset['date_format']			= "EURO"; 		// US (US Date 12/31/2007), EURO (European Date 31/12/2007), INT (World Date 2007/12/31)
	$langset['clock_format']		= "24"; 		// 12 or 24 (hours)
?>