<?php
	# ENGLISH
	$calendar = array();
	$calendar['long_month']		= array(1 => "January","February","March","April","May","June","July","August","September","October","November","December");
	$calendar['short_month']	= array(1 => "Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sept","Oct","Nov","Dec");
	$calendar['full_days']		= array(1 => "Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday");
	$calendar['short_days']		= array(1 => "Sun","Mon","Tues","Wed","Thur","Fri","Sat");
?>