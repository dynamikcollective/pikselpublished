<?php
	# JAPANESE
	$calendar = array();
	$calendar['long_month']		= array(1 => "1月","2月","3月","4月","5月","6月","7月","8月","9月","10月","11月","12月");
	$calendar['short_month']	= array(1 => "1月","2月","3月","4月","5月","6月","7月","8月","9月","10月","11月","12月");
	$calendar['full_days']		= array(1 => "日曜日","月曜日","火曜日","水曜日","木曜日","金曜日","土曜日");
	$calendar['short_days']		= array(1 => "日曜","月曜","火曜","水曜","木曜","金曜","土曜");
?>