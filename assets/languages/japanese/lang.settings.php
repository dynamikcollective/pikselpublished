<?php
	# ENGLISH	
	$langset['version'] 			= "4.4.3";		// Version number this language file is translated for
	$langset['active'] 				= 0;			// Allow the language to be selected in the management area
	$langset['translatedBy'] 		= '';			// Method or person who translated the language
	$langset['mgmtAreaTrans'] 		= 1;			// Management area also translated
	$langset['lang_charset'] 		= "utf-8";		// The required character set for your language text.	
	$langset['id']					= "japanese"; 	// Language ID - MUST MATCH THE DIRECORY NAME EXACTLY - NO SPACES - ALL LOWERCASE
	$langset['locale']				= "en_JP"; 		// Language Locale
	$langset['xmlLangCode']			= "ja"; 		// Language Locale
	$langset['name']				= "Japanese"; 	// Language Name That Gets Displayed

	$langset['date_format']			= "JP"; 		// US (US Date 12/31/2007), EURO (European Date 31/12/2007), INT (World Date 2007/12/31)
	$langset['clock_format']		= "12"; 		// 12 or 24 (hours)
?>