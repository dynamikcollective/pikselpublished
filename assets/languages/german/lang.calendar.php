<?php
	# DEUTSCH
	$calendar = array();
	$calendar['long_month']		= array(1 => "Januar","Februar","März","April","Mai","Juni","Juli","August","September","Oktober","November","Dezember");
	$calendar['short_month']	= array(1 => "Jan","Feb","Mär","Apr","Mai","Jun","Jul","Aug","Sept","Okt","Nov","Dez");
	$calendar['full_days']		= array(1 => "Sonntag","Montag","Dienstag","Mittwoch","Donnerstag","Freitag","Samstag");
	$calendar['short_days']		= array(1 => "So","Mo","Di","Mi","Do","Fr","Sa");
?>