<?php
	/******************************************************************
	*  Deutsch
	******************************************************************/
	
	# 4.7
	$lang['featuredGalleries']		= "Featured Galleries";
	
	
	# 4.6.3	
	$lang['ccMessage']				= "Please enter your credit card information below.";
	$lang['ccNumber']				= "Card Number";
	$lang['ccCVC']					= "CVC";
	$lang['ccExpiration']			= "Expiration (MM/YYYY)";
	
	
	# 4.6.1
	$lang['passwordLeaveBlank']		= "Leave blank for no password";
	$lang['myAlbums']				= "My Albums";
	
	# 4.5
	$lang['dateDownloadUpper']		= "DATE DOWNLOADED";
	$lang['downloadTypeUpper']		= "DOWNLOAD TYPE";
	$lang['rss']					= "RSS";
	
	# 4.4.8
	$lang['uploadFile']				= "Upload File";
	
	# 4.4.7
	$lang['contrSmallFileSize']		= "The file size is too small. The file must be at least";
	# plupload
	$lang['plupSelectFiles']		=  "Select files";
	$lang['plupAddFilesToQueue']	=  "Add files to the upload queue and click the start button.";
	$lang['plupFilename']			=  "Filename";
	$lang['plupStatus']				=  "Status";
	$lang['plupSize']				=  "Size";
	$lang['plupAddFiles']			=  "Add files";
	$lang['plupStartUplaod']		=  "Start upload";
	$lang['plupStopUpload']			=  "Stop current upload";
	$lang['plupStartQueue']			=  "Start uploading queue";
	$lang['plupDragFilesHere']		=  "Drag files here.";
	
	# 4.4.6
	$lang['batchUploader']			= "Batch Uploader";
	$lang['uploader'][1]			= "Java Based";
	$lang['uploader'][2]			= "HTML5/Flash Based";
	$lang['change']					= "Change";
	
	# 4.4.5
	$lang['moreInfoPlus']			= "[More Info]";
	
	# Custom/ Used für Checkout template, regarding the mandatory Icon by Law Aug 1, 2012
	$lang['checkout1']				= "JETZT KAUFEN";

	# 4.4.3
	$lang['mediaLabelPropRelease']	= "Property Release";

	# 4.4.2
	$lang['go']						= "GO";
	
	# 4.4.0
	$lang['share'] 					= "Share";
	$lang['bbcode'] 				= "BBCode";
	$lang['html'] 					= "HTML";
	$lang['link']					= "Link";
	$lang['pricingCalculator']		= "Pricing Calculator";
	$lang['noOptionsAvailable']		= "No options available";
	$lang['viewCartUpper']			= "WARENKORB ANSEHEN";
	
	# 4.3
	$lang['vatIDNumber']			= "VAT ID Number";
		
	# 4.2.1
	$lang['captchaError']			= "Die Eingabe des Captchacodes war nicht korrekt. Die Nachricht wurde nicht verschickt";
	
	# 4.1.7
	$lang['mediaLicenseEU'] 		= "Editorial Use";
	
	# 4.1.6
	$lang['yourBill'] 				= "Deine Rechnung";
	$lang['invoiceNumber'] 			= "Rechnungsnummer";
	$lang['paymentThanks'] 			= "Vielen Dank f&uuml;r die Zahlung";
	$lang['msActive'] 				= "Deine Mitgliedschaft ist nun aktiv.";
	$lang['today'] 					= "Heute";
	$lang['downloadsRemainingB']	= "&Uuml;brige Downloads";
	
	# 4.1.4
	$lang['link'] 					= "Link";
	$lang['optional'] 				= "Optional";
	
	# 4.1.3
	$lang['keywordRelevancy'] 		= "Schl&uuml;elwort Relevanz";
	$lang['mediaLicenseEX'] 		= "Erweiterte Lizenz";
	
	# 4.1.1
	$lang['editMediaDetails'] 		= "Bearbeite Media Details";
	$lang['searchResults'] 			= "Suchergebnis";
	$lang['width'] 					= "Breite";
	$lang['height'] 				= "H&ouml;he";
	$lang['hd'] 					= "HD";
	$lang['mediaProfile'] 			= "Profil";
	$lang['photo'] 					= "Photo";
	$lang['video'] 					= "Video";
	$lang['other'] 					= "Sonstiges";
	$lang['thumbnail'] 				= "Thumbnail";
	$lang['videoSample'] 			= "Videoausschnitt";
	$lang['attachFile'] 			= "Dateianhang";
	$lang['fileAttached'] 			= "Anhang";
	$lang['attachMessage'] 			= "Bitte w&auml;hlen Sie eine Datei f&uuml;r ihr Profil aus";
	$lang['browse'] 				= "Browse";
	$lang['uploadThumb'] 			= "Upload Thumbnail";
	$lang['uploadVideo'] 			= "Upload Video Preview";
	
	# 4.1
	$lang['importSuccessMes'] 		= "Import erfolgreich!";
	$lang['forgotPassword'] 		= "Passwort vergessen?";
	$lang['passwordSent'] 			= "Email verschickt. Bitte &Uuml;erpr&uuml;fen sie ihre Mailbox inkl. Ihrem Spamordner.";
	$lang['passwordFailed'] 		= "Es tut uns Leid, aber diese Mitgliedschaft ist uns nicht bekannt. Bitte geben sie Ihre Emailadresse erneut ein.";
	$lang['photoProfiles']			= "Photo Profiles";
	$lang['videoProfiles']			= "Video Profiles";
	$lang['otherProfiles']			= "Other Profiles";
	$lang['saving']					= "Saving";
	$lang['myAccount']				= "Mein Konto";
	$lang['myGalleries']			= "Meine Gallerien";
	$lang['noMediaAlbum']			= "There is no media to display.";
	$lang['editDetails']			= "Edit Details";
	$lang['approvalStatus0']		= "Pending Approval";
	$lang['approvalStatus1']		= "Approved";
	$lang['approvalStatus2']		= "Failed Approval";
	$lang['noDetailsMes']			= "No further details were provided!";	
	$lang['orphanedMedia']			= "Orphaned Media";
	$lang['lastBatch']				= "Last Batch Added";
	$lang['deleteAlbumMes']			= "Delete this album and all media it contains?";
	$lang['mailInMedia']			= "Mail on CD/DVD";
	$lang['deleteAlbum']			= "Delete Album";
	$lang['editAlbum']				= "Edit Album";
	$lang['albumName']				= "Album Name";
	$lang['makePublic']				= "Make Public";
	$lang['deleteMedia']			= "Delete Media";
	$lang['deleteMediaMes']			= "Delete the selected media files?";
	$lang['selectAll']				= "Select All";
	$lang['selectNone']				= "Select None";
	$lang['noImportFilesMessage']	= "There are no files to import!";
	$lang['selectAlbumMes']			= "Select the album you wish to add this media to.";
	$lang['selectGalleriesMes']		= "Select the galleries you wish to add this media to.";
	$lang['chooseItemsMes']			= "Choose the items to sell from the list below.";
	$lang['ablum']					= "Album";
	$lang['pricing']				= "Pricing";
	$lang['px']						= "px";	
	$lang['noSales']				= "Du hast keine Verk&auml;fe.";
	$lang['itemUpper']				= "ITEM";
	$lang['commissionUpper']		= "COMMISSION";
	$lang['addUpper']				= "ADD";
	$lang['cmDeleteVerify']			= "Are you sure that you would like to delete this media?";
	$lang['waitingForImport']		= "Media Waiting For Import";
	$lang['importSelectedUpper']	= "IMPORT SELECTED";
	$lang['addMediaDetails']		= "Add Media Details";
	$lang['uploadUpper']			= "UPLOAD";
	$lang['noBioMessage']			= "Currently there is no bio for this member.";
	$lang['collections']			= "Collections";
	$lang['uploadMediaUpper']		= "UPLOAD NEW MEDIA";
	$lang['startUpper']				= "START";
	
	# 4.0.9
	$lang['displayName']			= "Display Name";
	$lang['newAlbum']				= "Neues Album";
	$lang['albums']					= "Alben";
	$lang['viewAllMedia']			= "ZEIGE ALLE MEDIEN";
	$lang['signUpNow']				= "JETZT REGISTRIEREN";
	
	# 4.0.8		
	$lang['exactMatch']				= "Exaktes Ergebnis";
	$lang['mediaLabelMediaTypes']	= "Medientyp";
	
	# 4.0.6
	$lang['orderNumUpper']			= "Bestellnummer";
	$lang['orderDateUpper']			= "Bestelldatum";
	$lang['paymentUpper']			= "Bezahlung";	
	
	# 4.0.5
	$lang['dateRange']				= "Datumsbereich";
	$lang['resolution']				= "Aufl&ouml;sung";
	$lang['continueShopUpper']		= "WEITER EINKAUFEN";
	$lang['votes']					= "Bewertung";
	$lang['moreNews']				= "mehr Blog-Artikel";
	$lang['currentSearch']			= "Erweiterte Suche";
	$lang['dates']					= "Termine";
	$lang['licenseType']			= "Lizenz-Typ";
	$lang['searchUpper']			= "SUCHEN";
	$lang['from']					= "von";
	$lang['to']						= "zu";
	$lang['lightboxUpper']			= "LEUCHTKASTEN";
	$lang['itemsUpper']				= "ARTIKEL";
	$lang['createdUpper']			= "ERSTELLT"; 
	$lang['na']						= "Unzutreffend";  // Needs translation
	$lang['galSortCDate']			= "Erstellt am";  // Needs translation

	# 4.0.4
	$lang['digitalDownloads']		= "Digitale Downloads";
    
	$lang['copyright']				= "Copyright &copy; ".date("Y");
	$lang['reserved']				= "Alle Rechte vorbehalten.";
	
	$lang['days']					= "Tage";
	$lang['weeks']					= "Wochen";
	$lang['months']					= "Monate";
	$lang['years']					= "Jahre";
	$lang['weekly']					= "W&ouml;chentlich";
	$lang['monthly']				= "Monatlich";
	$lang['quarterly']				= "Viertelj&auml;hrlich";
	$lang['semi-annually']			= "Halbj&auml;hrlich";
	$lang['annually']				= "J&auml;hrlich";
	$lang['guest']					= "Gast";
	$lang['login'] 					= "Kundenlogin";	
	$lang['loginCaps'] 				= "ANMELDEN";	
	$lang['loginMessage']			= "Bitte geben Sie Ihre E-Mail-Adresse und Ihr Passwort ein um sich anzumelden.";
	$lang['loggedOutMessage']		= "Sie haben sich abgemeldet";
	$lang['loginFailedMessage']		= "Fehler bei der Anmeldung: E-Mail oder Passwort waren falsch.";
	$lang['accountActivated']		= "Ihr Konto wurde aktiviert.";
	$lang['activationEmail']		= "Eine &Uuml;berpr&uuml;fungs E-Mail wurde an Sie verschickt. Bitte nutzen Sie den darin enthaltenen Link  um Ihr Kundenkonto zu aktivieren.";
	$lang['loginAccountClosed']		= "Dieses Konto ist geschlossen oder inaktiv.";
	$lang['loginPending']			= "Dieses Konto ist nicht verifiziert. Es muss verifiziert werden, bevor Sie sich anmelden k&ouml;nnen.";
	$lang['yesLogin']				= "Ja, Ich habe ein Kundenkonto und m&ouml;chte mich anmelden.";
	$lang['noCreateAccount']		= "Nein, Ich m&ouml;chte ein neues Kundenkonto erstellen.";
	$lang['haveAccountQuestion']	= "Haben Sie schon ein Kundenkonto bei uns?";
	$lang['collections']			= "Digitalsammlungen";
	$lang['featuredCollections']	= "Besondere Digitalsammlungen";
	$lang['similarMedia']			= "&Auml;hnliche Medien";
	$lang['paypal']					= "PayPal";
	$lang['checkMO']				= "&Uuml;berweisung";
	$lang['paypalEmail']			= "PayPal E-mail";
	$lang['paid']					= "Bezahlt";
	$lang['processing']				= "In Bearbeitung";
	$lang['unpaid']					= "Unbezahlt";
	$lang['refunded']				= "Zur&uuml;ckgezahlt";
	$lang['failed']					= "Fehlgeschlagen";
	$lang['cancelled']				= "Abgebrochen";
	$lang['approved']				= "Genehmigt";
	$lang['incomplete']				= "Unvollst&auml;ndig";
	$lang['billLater']				= "Rechnung Sp&uuml;ter";
	$lang['expired']				= "Abgelaufen";
	$lang['unlimited']				= "Uneingeschr&auml;nkt";
	$lang['quantityAvailable']		= "Verf&uuml;gbare Menge";
	$lang['shipped']				= "Versand";
	$lang['notShipped']				= "nicht versendet";
	$lang['backordered']			= "nachbestellt";
	$lang['taxIncMessage']			= "inkl. MwSt.";
	$lang['addTag']					= "Tag hinzuf&uuml;gen";
	$lang['memberTags']				= "Mitglieder Tags";
	$lang['comments']				= "Kommentare";
	$lang['showMoreComments']		= "Zeige alle Kommentare";
	$lang['noComments']				= "Es wurden noch keine Kommentare zugelassen.";
	$lang['noTags']					= "Es gibt keine zugelassenen Tags.";
	$lang['addNewComment']			= "Kommentar hinzuf&uuml;gen";
	$lang['commentPosted']			= "Ihr Kommentar wurde ver&ouml;ffentlicht.";
	$lang['commentPending']			= "Ihr Kommentar wurde versendet und wird in K&uuml;rze genehmigt.";
	$lang['commentError']			= "Es gab einen Fehler und Ihr Kommentar wurde nicht &uuml;bermittelt.";
	$lang['tagPosted']				= "Ihr Tag wurde hinzugef&uuml;gt";
	$lang['tagPending']				= "Ihr neuer Tag wurde versendet und wird in K&uuml;rze genehmigt.";
	$lang['tagError']				= "Es gab einen Fehler und Ihr Tag wurde nicht &uuml;bermittelt.";
	$lang['tagDuplicate']			= "Dieser Tag ist bereits vorhanden.";
	$lang['tagNotAccepted']			= "Ihr Tag wurde von unserem System nicht akzeptiert.";
	$lang['preferredLang']			= "bevorzugte Sprache";
	$lang['dateTime']				= "Datum/Zeit";
	$lang['preferredCurrency']		= "bevorzugte W&auml;hrung";
	$lang['longDate']				= "langes Datum";
	$lang['shortDate']				= "kurzes Datum";
	$lang['numbDate']				= "Nummer Datum";
	$lang['daylightSavings']		= "Sommerzeit";
	$lang['dateFormat']				= "Datumsformat";
	$lang['timeZone']				= "Zeitzone";
	$lang['dateDisplay']			= "Datumsanzeige";
	$lang['clockFormat']			= "Uhrformat";
	$lang['numberDateSep']			= "Nummer Datum Trennzeichen";
	$lang['renew']					= "verl&auml;ngern";
	$lang['noOrders']				= "Es gibt keine Bestellungen f&uuml;r diesem Konto.";
	$lang['noDownloads']			= "Es gibt keine Downloads f&uuml;r diesem Konto.";
	$lang['noFeatured'] 			= "Zur Zeit sind keine Objekte in diesem Bereich.";
	$lang['promotions']				= "Aktionen";
	$lang['noPromotions']			= "Wir haben keine Gutscheine oder Aktionen zu diesem Zeitpunkt.";	
	$lang['autoApply']				= "Automatisch w&auml;hrend der Bestellung angerechnet.";
	$lang['useCoupon']				= "Verwenden Sie den folgenden Code in der Kasse oder klicken Sie auf die Schaltfl&auml;che &Uuml;bernehmen, um diesen Gutschein / Promotion nutzen";
	$lang['couponApplied']			= "Ein Gutschein oder Rabatt wurde Ihrem Einkaufskorb hinzugef&uuml;gt.";
	$lang['couponFailed']			= "Dieser Gutschein oder Rabatt ist ung&uuml;ltig.";	
	$lang['couponNeedsLogin']		= "Sie m&uuml;ssen angemeldet sein, um diesen Gutschein oder Rabatt nutzen zu k&ouml;nnen.";
	$lang['couponMinumumWarn']		= "Ihre Zwischensumme erf&uuml;llt nicht die Anforderung um diesen Gutschein oder Rabatts nutaen zu k&ouml;nnen.";
	$lang['couponAlreadyUsed']		= "Sie k&ouml;nnen diesen Gutschein oder sofort Rabatt nur einmal nutzen.";
	$lang['couponLoginWarn']		= "Sie m&uuml;ssen angemeldet sein, um diesen Gutschein oder Rabatt zu nutzen.";
	$lang['checkout']				= "ZUR KASSE";
	$lang['continue']				= "WEITER";
	$lang['shipTo']					= "Liefern nach";
	$lang['billTo']					= "Rechnung an";
	$lang['mailTo']					= "Vorauskasse";
	$lang['galleries']				= "Kategorien";
	$lang['chooseGallery']			= "W&auml;hlen Sie eine Kategorie unten, um ihre Inhalte anzuzeigen.";
	$lang['galleryLogin']			= "Bitte geben Sie das Kennwort f&uuml;r diese Kategorie unten ein, um sich anzumelden.";
	$lang['galleryWrongPass']		= "Das Passwort, das Sie f&uuml;r diese Kategorie eingegeben haben, war falsch.";
	$lang['newestMedia']			= "Aktuellste Uploads";
	$lang['popularMedia']			= "Beliebteste Medien";
	$lang['contributors']			= "Anbieter";
	$lang['contUploadNewMedia']		= "Laden Sie neue Medien hoch";
	$lang['contViewSales']			= "Verk&auml;ufe anschauen";
	$lang['contPortfolio']			= "Mein Portfolio";
	$lang['contGalleries']			= "Meine Galerien";
	$lang['contMedia']				= "Meine Medien";
	$lang['aboutUs']				= "Profil";
	$lang['news']					= "Blog";
	$lang['noNews']					= "Es gibt keine Blog-Artikel zu diesem Zeitpunkt.";
	$lang['termsOfUse']				= "Nutzungsbedingungen";
	$lang['privacyPolicy']			= "Datenschutzerkl&auml;rung";
	$lang['purchaseAgreement']		= "AGB";	
	$lang['iAgree']					= "Ich akzeptiere die";
	$lang['createAccount']			= "Kundenkonto erstellen";
	$lang['createAccountMessage']	= "Bitte f&uuml;llen Sie die unten aufgef&uuml;hrten Formularfelder aus, um ein neues Kundenkonto zu erstellen.";
	$lang['contactUs']				= "Kontakt";
	$lang['contactMessage']			= "Vielen Dank f&uuml;r Ihre Kontaktaufnahme. Wir werden in K&uuml;rze antworten.";
	$lang['contactError']			= "Das Kontaktformular wurde nicht korrekt ausgef&uuml;llt. Ihre Nachricht konnte nicht gesendet werden.";
	$lang['contactIntro']			= "Bitte benutzen Sie das untenstehende Formular, um uns zu kontaktieren. Wir werden so bald wie m&ouml;glich antworten.";
	$lang['contactEmailSubject']	= "Frage von Kontaktformular";
	$lang['contactFromName']		= "Kontaktformular";
	$lang['prints']					= "Drucke";
	$lang['featuredPrints']			= "Besondere Drucke";
	$lang['newBill']				= "Eine neue Rechnung wurde f&uuml;r diese Mitgliedschaft erstellt. Diese Mitgliedschaft wird erst aktiv, oder zu erneuern sein, bis die Rechnung bezahlt wurde. Noch ausstehende Zahlungen f&uuml;r Mitgliedsbeitr&auml;ge wurden abgesagt.";
	$lang['accountInfo']			= "Kundenkonto Informationen";
	$lang['accountUpdated']			= "Ihre Kundenkonto Informationen wurden Aktualisiert.";
	$lang['editAccountInfo']		= "&Auml;ndern Sie Ihre Kundenkonto Informationen";
	$lang['editAccountInfoMes']		= "Bearbeiten Sie Ihre Kundenkonto Informationen und klicken Sie auf Speichern, um Ihre &Auml;nderungen zu &uuml;bernehmen.";	
	$lang['accountInfoError1']		= "Passwort und Passwortbest&auml;tigung stimmen nicht &uuml;berein!";
	$lang['accountInfoError5']		= "Das aktuell eingegebene Passwort ist nicht korrekt!";
	$lang['accountInfoError2']		= "Ihr Passwort muss mindestens 6 Zeichen lang sein!";
	$lang['accountInfoError12']		= "Diese E-Mail-Adresse wird bereits verwendet!";
	$lang['accountInfoError13']		= "Ihre E-Mail-Adresse wurde nicht akzeptiert!";
	$lang['accountInfoError3']		= "W&ouml;rter oben eingeben!";
	$lang['accountInfoError4']		= "Captcha W&ouml;rter waren falsch!";
	$lang['readAgree']				= "Ich akzeptiere die";
	$lang['agreements']				= "Vereinbarung";
	$lang['poweredBy']				= "Angeboten von";
	$lang['captchaAudio']			= "Geben Sie die Zahlen ein die Sie h&ouml;ren";
	$lang['captchaIncorrect']		= "Falsche Eingabe, bitte versuchen Sie es erneut";
	$lang['captchaPlayAgain']		= "erneut abspielen";
	$lang['captchaDownloadMP3']		= "Download in MP3 format";
	$lang['captcha']				= "Captcha";
	$lang['subHeaderID']			= "ID";
	$lang['subHeaderSubscript']		= "ABONNEMENT";
	$lang['subHeaderExpires']		= "ABGELAUFEN";
	$lang['subHeaderDPD']			= "VERBLEIBENDE DOWNLOADS PRO TAG";
	$lang['subHeaderStatus']		= "STATUS";
	$lang['downloads']				= "Downloads";	
	$lang['thankRequest']			= "Vielen Dank f&uuml;r Ihre Anfrage. Wir werden Sie umgehend kontaktieren";
	$lang['pleaseContactForm']		= "Bitte verwenden Sie das folgende Formular f&uuml;r die Preisanfrage zu dieser Datei, ";
	$lang['downWithSubscription']	= "Download mit Abonnement";
	$lang['noInstantDownload'] 		= "Diese Datei ist nicht f&uuml;r den Sofort-Download erh&auml;ltlich. Er wird vom Seiten-Administrator per E-Mail geliefert werden, wenn gew&uuml;nscht oder gekauft.";
    
	// Tickets
	$lang['newTicketsMessage']		= "neue oder aktualisierte Tickets";
	$lang['emptyTicket']			= "Dieses Ticket ist leer.";
	$lang['messageID']				= "Nachrichten ID";
	$lang['ticketNoReplies']		= "Dieses Ticket wurde geschlossen und neue Antworten werden nicht akzeptiert.";
	$lang['ticketUpdated']			= "Dieses Ticket wurde aktualisiert!";
	$lang['ticketClosed']			= "Dieses Ticket wurde geschlossen!";
	$lang['closeTicket']			= "Geschlossenes Ticket";
	$lang['newTicket']				= "Neues Support Ticket";
	$lang['newTicketButton']		= "NEUES TICKET";
	$lang['ticketUnreadMes']		= "Ticket enth&auml;lt ungelesene / neue Nachrichten";
	$lang['ticketUnderAccount']		= "Es gibt keine Support-Tickets in diesem Konto";	
	$lang['ticketSubmitted']		= "Ihr Support-Ticket wurde versandt. Wir werden in K&uuml;rze antworten.";
	
	$lang['mediaNotElidgiblePack']	= "Dieser Medium ist nicht berechtigt, auf irgendwelche Pakete zugeordnet zu werden!";
	$lang['noBills']				= "Es gibt keine Rechnungen unter diesem Konto.";
	$lang['lightbox']				= "Leuchtkasten";
	$lang['noLightboxes']			= "Sie haben noch keinen Leuchtkasten angelegt.";
	$lang['lightboxDeleted']		= "Ihr Leuchtkasten wurde gel&ouml;scht";
	$lang['lbDeleteVerify']			= "Sind Sie sicher, dass Sie ihren Leuchtkasten l&ouml;schen m&ouml;chten?";
	$lang['newLightbox']			= "NEUEN LEUCHTKASTEN ANLEGEN";
	$lang['lightboxCreated']		= "Ihr neuer Leuchtkasten wurde angelegt";
	$lang['addToLightbox']			= "Zum Leuchtkasten hinzuf&uuml;gen";
	$lang['createNewLightbox']		= "Erstelle Sie einen neuen Leuchtkasten";
	$lang['editLightbox']			= "Bearbeiten Sie diesen Leuchtkasten";
	$lang['addNotes']				= "Anmerkungen hinzuf&uuml;gen";
	$lang['editLightboxItem']		= "Leuchtkasten Artikel bearbeiten";
	$lang['removeFromLightbox']		= "AUS DEM LEUCHTKASTEN ENTFERNEN";
	$lang['savedChangesMessage']	= "Ihre &auml;nderungen wurden gespeichert.";
	$lang['noSubs']					= "Es sind keine Abonnements f&uuml;r diesem Konto vorhanden.";
	$lang['unpaidBills']			= "Unbezahlte Rechnungen";
	$lang['notices']				= "Hinweise";
	$lang['subscription']			= "Abonnement";
	$lang['assignToPackage']		= "zu Paket hinzuf&uuml;gen";
	$lang['startNewPackage']		= "Erstellen Sie ein neues Paket";
	$lang['packagesInCart']			= "Pakete in Ihrem Einkaufskorb ";
	$lang['id']						= "ID";
	$lang['summary']				= "Zusammenfassung";
	$lang['status']					= "Status";
	$lang['lastUpdated']			= "Zuletzt aktualisiert";
	$lang['opened']					= "Er&ouml;ffnet";
	$lang['reply']					= "Antworten";
	$lang['required']				= "Erforderlich";
	$lang['bills']					= "Rechnungen";
	$lang['orders']					= "Bestellungen";
	$lang['downloadHistory']		= "Download History";
	$lang['supportTickets']			= "Supporttickets";
	$lang['order']					= "Bestellen";
	$lang['packages']				= "Pakete";
	$lang['featuredPackages']		= "Besondere Pakete";
	$lang['products']				= "Produkte";
	$lang['featuredProducts']		= "Besonderes Produkte";
	$lang['subscriptions']			= "Abonnements";
	$lang['featuredSubscriptions']	= "Besondere Abonnements";
	$lang['yourCredits']			= "IHRE<br />CREDITS";
	$lang['featuredCredits']		= "Besondere Credits Pakete";
	$lang['media'] 					= "MEDIEN";
	$lang['mediaNav'] 				= "Medien";
	$lang['featuredMedia']			= "Besondere Medien";
	$lang['featuredItems'] 			= "Besonderes";	
	$lang['showcasedContributors'] 	= "Pr&auml;sentierte Anbieter";
	$lang['galleryLogin']			= "Kategorie Login";
	$lang['forum'] 					= "Forum";
	$lang['randomMedia'] 			= "Zuf&auml;llige Medien";
	$lang['language'] 				= "Sprache";
	$lang['priceCreditSep']			= "oder";
	$lang['viewCollection']			= "COLLECTION ANSCHAUEN";
	$lang['loggedInAs']				= "Sie sind angemeldet als";
	$lang['editProfile']			= "KUNDENKONTO BEARBEITEN";
	$lang['noItemCartWarning']		= "Sie m&uuml;ssen ein Foto ausw&auml;hlen, bevor Sie diesen Artikel in den Einkaufskorb legen k&ouml;nnen!";
	$lang['cartTotalListWarning']	= "These values are estimates only and may change slightly due to currency fluctuation and rounding.";
	$lang['applyCouponCode']		= "Gutscheincode anwenden";
	$lang['billMeLater']			= "Auf Rechnung";
	$lang['billMeLaterDescription']	= "Wir Erstellen eine monatliche Rechnung f&uuml;r Ihre Eink&auml;ufe.";
	$lang['shippingOptions']		= "Versand Optionen";
	$lang['paymentOptions']			= "Zahlungsm&ouml;glichkeiten";
	$lang['chooseCountryFirst']		= "Zuerst Land w&auml;hlen";
	$lang['paymentCancelled']		= "Ihre Zahlung wurde storniert. Wenn Sie m&ouml;chten, k&ouml;nnen erneut versuchen zur Kasse zu gehen."; 
	$lang['paymentDeclined']		= "Ihre Zahlung wurde abgelehnt. Wenn Sie m&ouml;chten, k&ouml;nnen erneut versuchen zur Kasse zu gehen."; 
	$lang['generalInfo']			= "Pers&ouml;nliche Informationen";
	$lang['membership']				= "Mitgliedschaft";
	$lang['preferences']			= "Einstellungen";
	$lang['address']				= "Adresse";
	$lang['actions']				= "Aktionen";
	$lang['changePass']				= "Passwort &auml;ndern";
	$lang['changeAvatar']			= "Avatar &auml;ndern";
	$lang['bio']					= "Bio";
	$lang['contributorSettings']	= "Anbieter einstellungen";
	$lang['edit']					= "BEARBEITEN";
	$lang['leftToFill']				= "Artikel links zu f&uuml;llen";
	$lang['commissionMethod']		= "Kommission Zahlungsmethode"; 
	$lang['commission']				= "Kommission";
	$lang['signupDate']				= "Mitglied seit";
	$lang['lastLogin']				= "Letzter Login";
	$lang['clientName']				= "Kunden Name";
	$lang['eventCode']				= "Event Code";
	$lang['eventDate']				= "Event Datum";
	$lang['eventLocation']			= "Event Standort";
	$lang['viewPackOptions']		= "Paketinhalt anzeigen & Optionen";
	$lang['viewOptions']			= "Optionen anschauen";
	$lang['remove']					= "ENTFERNEN";
	$lang['productShots']			= "Produkt Fotos";
	$lang['currentPass']			= "Aktuelles Passwort";
	$lang['newPass']				= "neues Passwort";
	$lang['vNewPass']				= "neues Passwort &uuml;berpr&uuml;fen";
	$lang['verifyPass']				= "Passwort best&auml;tigen";
	$lang['firstName']				= "Vorname";
	$lang['lastName']				= "Nachname";
	$lang['location']				= "Standort";
	$lang['memberSince']			= "Mitglied seit";
	$lang['portfolio']				= "PORTFOLIO";
	$lang['profile']				= "Profil";
	$lang['address']				= "Adresse";
	$lang['city']					= "Stadt";
	$lang['state']					= "Bundesland";
	$lang['zip']					= "Postleitzahl";
	$lang['country']				= "Land";
	$lang['save'] 					= "Speichern";
	$lang['add'] 					= "HINZUF&Uuml;GEN";
	$lang['companyName']			= "Firmenname";
	$lang['accountStatus']			= "Kontostatus";
	$lang['website']				= "Internetpr&auml;senz";
	$lang['phone']					= "Telefon";
	$lang['email'] 					= "Email";
	$lang['name'] 					= "Name";
	$lang['submit'] 				= "SENDEN";	
	$lang['message'] 				= "Nachricht";	
	$lang['question']				= "Ihre Mitteilung";
	$lang['password'] 				= "Passwort";	
	$lang['members'] 				= "Mitglieder";
	$lang['visits'] 				= "Besuche";	
	$lang['logout'] 				= "Ausloggen";
	$lang['lightboxes']				= "Leuchtkasten";
	$lang['cart']					= "Ihr Einkaufskorb";
	$lang['cartItemAdded']			= "Ein Artikel wurde zu Ihrem Einkaufskorb hinzugef&uuml;gt.";
	$lang['includesTax']			= "Inklusive Umsatzsteuer / Mehrwertsteuer";
	$lang['addToCart']				= "IN DEN EINKAUFSKORB";
	$lang['items']					= "Artikel im Einkaufskorb";
	$lang['item']					= "Artikel";
	$lang['qty']					= "Anzahl";
	$lang['price']					= "Preis";
	$lang['more']					= "mehr";
	$lang['moreInfo']				= "mehr Informationen";
	$lang['back']					= "Zur&uuml;ck";
	$lang['none']					= "Keine";
	$lang['details']				= "Details"; // Einzelheiten
	$lang['options']				= "Optionen";
	$lang['page']					= "Seite";
	$lang['ratingSubmitted']		= "Bewertung Verfasst";
	$lang['noMedia']				= "Diese Galerie ist leer";
	$lang['itemsTotal']				= "Artikel insgesamt";
	$lang['of']						= "von";
	$lang['view']					= "ANSCHAUEN";
	$lang['apply']					= "&Uuml;BERNEHMEN";
	$lang['currency']				= "W&auml;hrung &auml;ndern";
	$lang['active']					= "Aktiv";
	$lang['pending']				= "in Bearbeitung";
	$lang['freeTrial']				= "Kostenlose Testversion";
	$lang['setupFee']				= "Einrichtungsgeb&uuml;hr";
	$lang['free']					= "Kostenlos";
	$lang['open']					= "Offen";
	$lang['close']					= "Schlie&szlig;en";
	$lang['closed']					= "Geschlossen";
	$lang['by']						= "Von";
	$lang['download']				= "Downloaden";
	$lang['downloadUpper']			= "DOWNLOADEN";
	$lang['KB']						= "K";
	$lang['MB']						= "MB";
	$lang['files']					= "Dateien";
	$lang['unknown']				= "Unbekannt";
	$lang['freeDownload']			= "Kostenloser Download";
	$lang['prevDown']				= "Vorschau Download";
	$lang['pay']					= "ZAHLEN";
	$lang['purchaseCredits']		= "CREDITS KAUFEN";
	$lang['purchaseSub']			= "ERWERBEN SIE EIN ABONNEMENT";
	$lang['hour'] 					= "Stunde";
	$lang['slash'] 					= "Schr&auml;gstrich";
	$lang['period'] 				= "Punkt";
	$lang['dash'] 					= "Strich";
	$lang['gmt'] 					= "GMT";
	$lang['avatar'] 				= "Avatar";
	$lang['delete'] 				= "L&Ouml;SCHEN";
	$lang['uploadAvatar']			= "Avatar hochladen";
	$lang['welcome']				= "Willkommen";
	$lang['expires']				= "L&auml;uft ab";
	$lang['msExpired']				= "Die folgende Mitgliedschaft ist abgelaufen";
	$lang['newSales']				= "Neue Ums&auml;tze seit dem letzten login";
	$lang['never']					= "niemals";
	$lang['yes']					= "JA";
	$lang['no']						= "NEIN";
	$lang['create'] 				= "ERSTELLEN";
	$lang['cancel'] 				= "ABBRECHEN";
	$lang['notes'] 					= "Hinweise";
	$lang['update'] 				= "Aktualisieren";
	$lang['each'] 					= "jedes";
	$lang['enterKeywords']			= "Suchen mit Keywords";
	$lang['send']					= "SENDEN";
	$lang['emailTo']				= "E-mail zu";
	$lang['yourName']				= "Ihr Name";
	$lang['yourEmail']				= "Ihre E-mail";
	$lang['emailToFriend']			= "E-Mail Link zu diesen Medien";
	$lang['emailToFriendSent']		= "Ihre E-Mail wurde versendet! Sie k&ouml;nnen noch eine E-Mail Senden  oder schlie&szlig;en Sie dieses Fenster.";
	$lang['linkSentBy']				= "Ein Link zu einem Foto oder Video wurde Ihnen geschickt von";
	$lang['newPackageMessage']		= "Starten Sie ein neues Paket unten oder w&auml;hlen Sie ein &auml;hnliches Paket welsches bereits in Ihrem Warenkorb Einkaufskorb vorhanden ist";
	$lang['warning']				= "Warnung!";
	$lang['estimated']				= "Pauschal";
	$lang['doneUpper']				= "FERTIG";
	$lang['returnToSiteUpper']		= "ZUR&Uuml;CK ZUR WEBSEITE";
	$lang['chooseCountryFirst']		= "Bitte w&auml;hlen Sie zuerst ihr Land";
	$lang['advancedSearch']			= "Erweiterte Suche";
	$lang['eventSearch']			= "Eventsuche";	
	$lang['AND']					= "UND";
	$lang['OR']						= "ODER";
	$lang['NOT']					= "NICHT";
    
	$lang['noAccess']				= "Sie haben keinen Zugang zu diesem angezeigten Bereich.";
	$lang['siteStats']				= "Seiten-Statistik";
	$lang['membersOnline']			= "Mitglieder Online";
	$lang['minutesAgo']				= "Minuten vor";
	$lang['seconds']				= "Sekunden";
	$lang['megabytesAbv']			= "MB";
	$lang['downWithSub']			= "Download Using Subscription";
	$lang['downloadsRemaining']		= "Verbleibende Downloads heute";
	$lang['requestDownloadSuccess']	= "Wir haben Ihre Anfrage f&uuml;r diese Datei erhalten und werden uns in K&uuml;rze mit Ihnen in Verbindung setzen.";
	$lang['mediaLicenseNFS']		= "Nicht f&uuml;r den Weiterverkauf bestimmt";
	$lang['mediaLicenseRF']			= "Royalty Free (Lizenzfrei)";
	$lang['mediaLicenseRM']			= "Lizensiert";
	$lang['mediaLicenseFR']			= "Frei";
	$lang['mediaLicenseCU']			= "Kontaktieren Sie uns";
	$lang['original']				= "Original";
	$lang['prevDownloaded']			= "Sie haben zuvor diese Datei heruntergeladen haben. Es gibt kein Guthaben, um sie erneut herunterzuladen.";
	$lang['instantDownload']		= "Sofortdownload";
	$lang['requestDownload']		= "DOWNLOAD Anfordern ";
	$lang['request']				= "ANFORDERN";
	$lang['enterEmail']				= "Geben Sie Ihre Email-Adresse ein";
	$lang['license']				= "Lizenz";
	$lang['inches']					= "Zoll";
	$lang['centimeters']			= "Zentimeter";
	$lang['dpi']					= "dpi";
	$lang['noSimilarMediaMessage']	= "Es wurden keine &auml;hnlichen Medien gefunden.";
	$lang['backUpper']				= "ZUR&Uuml;CK";
	$lang['nextUpper']				= "WEITER";
	$lang['prevUpper']				= "ZUR&Uuml;CK";
	$lang['curGalleryOnly']			= "nur Aktuelle Galerie";
	$lang['sortBy']					= "Sortieren nach";
	$lang['galSortColor']			= "Farbe";
	$lang['galSortDate']			= "aufgenommen am";
	$lang['galSortID']				= "Bildnummer";
	$lang['galSortTitle']			= "Title";
	$lang['galSortFilename']		= "Dateiname";
	$lang['galSortFilesize']		= "Dateigr&ouml;&szlig;e";
	$lang['galSortSortNum']			= "Sortiernummer";
	$lang['galSortBatchID']			= "Batch ID";
	$lang['galSortFeatured']		= "Besonderes";
	$lang['galSortWidth']			= "Breite";
	$lang['galSortHeight']			= "H&ouml;he";
	$lang['galSortViews']			= "Ansichten";
	$lang['galSortAsc']				= "aufsteigend";
	$lang['galSortDesc']			= "absteigend";
	$lang['mediaIncludedInColl']	= "Diese Medien-Datei ist in folgender Sammlungen vertreten.";
	$lang['billHeaderInvoice']		= "RECHNUNG";
	$lang['billHeaderDate']			= "RECHNUNGSDATUM";
	$lang['billHeaderDueDate']		= "F&Auml;LLIGKEITS Datum";
	$lang['billHeaderTotal']		= "Gesamtbetrag";
	$lang['billHeaderStatus']		= "STATUS";
	
	// Cart
	$lang['creditsWarning']			= "Sie verf&uuml;gen nicht &uuml;ber gen&uuml;gend Credits um zur Kasse zu gehen. Bitte Anmelden oder f&uuml;gen Sie Credits zu Ihrem Einkaufskorb hinzu.";
	$lang['subtotalWarning']		= "Der Mindestbestellwert, um Kasse zu gehen ist:";
	$lang['pleaseWait']				= "Bitte warten Ihre Anfrage wird bearbeitet!";
	$lang['orderMailinThanks']		= "Danke. Wir haben Ihre Bestellung erhalten. Bitte schicken Sie Ihre &Uuml;berweisung oder eine Zahlungsanweisung f&uuml;r den folgenden Betrag an die aufgef&uuml;hrte Rechnungsadresse. Sobald die Zahlung bei uns eingegangen ist, werden wir Ihre Bestellung genehmigen.";
	$lang['billMailinThanks']		= "Bitte schicken Sie Ihre &Uuml;berweisung oder eine Zahlungsanweisung f&uuml;r den folgenden Betrag an die aufgef&uuml;hrte Rechnungsadresse. Sobald Ihre Zahlung bei uns eingegangen ist, markieren wir Ihre Rechnung als bezahlt.";
	$lang['mailinRef']				= "Bitte verweisen Sie auf die folgende Nummer mit Ihrer Zahlung";
	$lang['subtotal']				= "Zwischensumme";
	$lang['shipping']				= "Versand";
	$lang['discounts']				= "Rabatte / Gutscheine";
	$lang['total']					= "Gesamt";
	$lang['creditsSubtotal']		= "Credits Zwischensumme";
	$lang['creditsDiscounts']		= "Credit Rabatte";
	$lang['credits']				= "Credits";
	$lang['reviewOrder']			= "Zusammenfassung";
	$lang['payment']				= "Zahlung";
	$lang['cartNoItems']			= "Sie haben noch keine Artikel in Ihrem Einkaufskorb!";
	$lang['enterShipInfo']			= "Bitte geben Sie Ihre Versand-und Rechnungs Informationen unten ein";
	$lang['sameAsShipping']			= "Gleich wie Lieferadresse";
	$lang['differentAddress']		= "Geben Sie eine andere Adresse an";
	$lang['noShipMethod']			= "Keine Versand-Methoden existieren, um diese Artikel zu versendet.";	
	$lang['choosePaymentMethod']	= "Bitte W&auml;hlen Sie eine Zahlungsmethode";	
	$lang['discountCode']			= "GUTSCHEIN CODE";
	$lang['use']					= "Nutzen";
	$lang['continueNoAccount']		= "oder ohne Kundenkonto weiter unten";
	
	
	// Order
	$lang['yourOrder']				= "Ihre Bestellung";
	$lang['viewInvoice']			= "RECHNUNG ANSEHEN";
	$lang['totalsShown']			= "Summen gezeigt in";
	$lang['downloadExpired']		= "Download Abgelaufen";
	$lang['downloadExpiredMes']		= "Dieser Download-Link ist abgelaufen. Bitte kontaktieren Sie uns, um ihn wieder zu aktiviert.";
	$lang['downloadMax']			= "Maximale Downloads &uuml;berschritten";
	$lang['downloadMaxMes']			= "Sie haben die maximale Anzahl der Downloads f&uuml;r diese gekaufte Datei bereits erreicht. Bitte kontaktieren Sie uns, damit der Download-Link reaktiviert werden kann.";
	$lang['downloadNotApproved']	= "Bestellung nicht genehmigt";
	$lang['downloadNotApprovedMes']	= "Sie k&ouml;nnen diese Datei nicht herunterladen, bis Ihre Bestellung best&auml;tigt wurde. Bitte schauen Sie zu einem sp&auml;teren Zeitpunkt nochmal nach.";	
	$lang['downloadNotAvail']		= "Sofort-Download nicht verf&uuml;gbar";
	$lang['downloadNotAvailMes']	= "Diese Datei ist nicht verf&uuml;gbar f&uuml;r den Sofort-Download. Bitte benutzen Sie den Anfordern Button unten, um diese Datei an Sie per E-Mail zu schicken.";
	$lang['orderNumber']			= "Bestellnummer";
	$lang['orderPlaced']			= "Bestellung erstellt"; 
	$lang['orderStatus']			= "Status der Bestellung";
	$lang['paymentStatus']			= "Zahlungsstatus";
	
	
	// IPTC
	$lang['iptc']					= "IPTC";
	$lang['iptc_title']				= "Title";
	$lang['iptc_description']		= "Beschreibung";
	$lang['iptc_instructions']		= "Anleitung";
	$lang['iptc_date_created']		= "Erstellungsdatum";
	$lang['iptc_author']			= "Autor";
	$lang['iptc_city']				= "Stadt";
	$lang['iptc_state']				= "Bundesland";
	$lang['iptc_country']			= "Land";
	$lang['iptc_job_identifier']	= "Anleitung";
	$lang['iptc_headline']			= "Job-ID";
	$lang['iptc_provider']			= "Provider";
	$lang['iptc_source']			= "Quelle";
	$lang['iptc_description_writer']= "Beschreibungs Autor";
	$lang['iptc_urgency']			= "Dringlichkeit";
	$lang['iptc_copyright_notice']	= "Copyright-Notiz";
    
	// EXIF
	$lang['exif']							= "EXIF";
	$lang['exif_FileName']					= "Dateiname";
	$lang['exif_FileDateTime']				= "Datei Datum / Zeit";
	$lang['exif_FileSize']					= "Dateigr&ouml;&szlig;e";
	$lang['exif_FileType']					= "Dateityp";
	$lang['exif_MimeType']					= "Mime Typ";
	$lang['exif_SectionsFound']				= "Sections Found";
	$lang['exif_ImageDescription']			= "Bildbeschreibung";
	$lang['exif_Make']						= "Erstellt";
	$lang['exif_Model']						= "Modell";
	$lang['exif_Orientation']				= "Orientierung";
	$lang['exif_XResolution']				= "XAufl&ouml;sung";
	$lang['exif_YResolution']				= "YAufl&ouml;sung";
	$lang['exif_ResolutionUnit']			= "Aufl&ouml;sungs-Einheit";
	$lang['exif_Software']					= "Software";
	$lang['exif_DateTime']					= "Datum / Zeit";
	$lang['exif_YCbCrPositioning']			= "YCbCr Positioning";
	$lang['exif_Exif_IFD_Pointer']			= "Exif IFD Pointer";
	$lang['exif_GPS_IFD_Pointer']			= "GPS IFD Pointer";
	$lang['exif_ExposureTime']				= "Belichtungszeit";
	$lang['exif_FNumber']					= "FNummer";
	$lang['exif_ExposureProgram']			= "Belichtungsprogramm";
	$lang['exif_ISOSpeedRatings']			= "ISO Empfindlichkeit";
	$lang['exif_ExifVersion']				= "Exif Version";
	$lang['exif_DateTimeOriginal']			= "Urspr&uuml;ngliches Datum / Zeit";
	$lang['exif_DateTimeDigitized']			= "Datum / Zeit der Digitalisierung";
	$lang['exif_ComponentsConfiguration']	= "Komponenten-Konfiguration";
	$lang['exif_ShutterSpeedValue']			= "Verschlusszeit";
	$lang['exif_ApertureValue']				= "Blendenwert";
	$lang['exif_MeteringMode']				= "Messmodus";
	$lang['exif_Flash']						= "Blitz";
	$lang['exif_FocalLength']				= "Brennweite";
	$lang['exif_FlashPixVersion']			= "Flash Pix Version";
	$lang['exif_ColorSpace']				= "Farbraum";
	$lang['exif_ExifImageWidth']			= "Exif Bild Breite";
	$lang['exif_ExifImageLength']			= "Exif Bild L&auml;nge";
	$lang['exif_SensingMethod']				= "Abtastmethode";
	$lang['exif_ExposureMode']				= "Belichtungsmodus";
	$lang['exif_WhiteBalance']				= "Wei&szlig;abgleich";
	$lang['exif_SceneCaptureType']			= "Szenenaufnahmetyp";
	$lang['exif_Sharpness']					= "Sch&auml;rfe";
	$lang['exif_GPSLatitudeRef']			= "GPS Latitude Ref";
	$lang['exif_GPSLatitude_0']				= "GPSLatitude 0";
	$lang['exif_GPSLatitude_1']				= "GPSLatitude 1";
	$lang['exif_GPSLatitude_2']				= "GPSLatitude 2";
	$lang['exif_GPSLongitudeRef']			= "GPS Longitude Ref";
	$lang['exif_GPSLongitude_0']			= "GPS Longitude 0";
	$lang['exif_GPSLongitude_1']			= "GPS Longitude 1";
	$lang['exif_GPSLongitude_2']			= "GPS Longitude 2";
	$lang['exif_GPSTimeStamp_0']			= "GPS Timestamp 0";
	$lang['exif_GPSTimeStamp_1']			= "GPS Timestamp 1";
	$lang['exif_GPSTimeStamp_2']			= "GPS Timestamp 2";
	$lang['exif_GPSImgDirectionRef']		= "GPS Bild Richtungs Referenz";
	$lang['exif_GPSImgDirection']			= "GPS Bild Richtung";
	
 	// Media Labels
	$lang['mediaLabelTitle']		= "Titel";
	$lang['mediaLabelDesc']			= "Beschreibung";
	$lang['mediaLabelCopyright']	= "Copyright";
	$lang['mediaLabelRestrictions']	= "Nutzungsbeschr&auml;nkungen";
	$lang['mediaLabelRelease']		= "Model Release";
	$lang['mediaLabelKeys']			= "Schl&#252;sselw&#246;rter";
	$lang['mediaLabelFilename']		= "Dateiname";
	$lang['mediaLabelID']			= "Bildnummer";
	$lang['mediaLabelDate']			= "Hinzugef&uuml;gt";
	$lang['mediaLabelDateC']		= "Erstellt";
	$lang['mediaLabelDownloads']	= "Downloads";
	$lang['mediaLabelViews']		= "Ansichten";
	$lang['mediaLabelPurchases']	= "K&auml;ufe";
	$lang['mediaLabelColors']		= "Farben";
	$lang['mediaLabelPrice']		= "Preis";
	$lang['mediaLabelCredits']		= "Credits";
	$lang['mediaLabelOwner']		= "Autor";
	$lang['mediaLabelUnknown']		= "unbekannt";
	$lang['mediaLabelResolution']	= "Pixels";
	$lang['mediaLabelFilesize']		= "Dateigr&ouml;&szlig;e";
	$lang['mediaLabelRunningTime']	= "Laufzeit";
	$lang['mediaLabelFPS']			= "FPS";
	$lang['mediaLabelFormat']		= "Format";
	
	// Search
	$lang['search']					= "Suche";
	$lang['searchEnterKeyMessage']	= "Bitte geben Sie Suchbegriffe in das Suchfeld ein, um Ihre Suche zu starten.";
	$lang['searchNoResults']		= "Ihre Suche brachte keine Ergebnisse";
	$lang['searchDateRange']		= "Suchen Sie im Datumsbereich";
	$lang['searchNarrow']			= "Suchresultate eingrenzen";
	$lang['searchStart']			= "Beginnen Sie Ihre Suche unten!";
	$lang['searchAll']				= "Alle";
	$lang['searchClear']			= "Suche l&ouml;schen";
	$lang['searchKeywords']			= "Keywords";
	$lang['searchTitle']			= "Titel";
	$lang['searchDescription']		= "Beschreibung";
	$lang['searchFilename']			= "Dateiname";
	$lang['searchPortrait']			= "Portr&auml;t";
	$lang['searchLandscape']		= "Landschaft";
	$lang['searchSquare']			= "Quadrat";
	$lang['searchRoyaltyFree']		= "Royalty Free";
	$lang['searchRightsManaged']	= "Rights Managed";
	$lang['searchFree']				= "Kostenlos";
	$lang['searchContactUs']		= "Kontakt";
	$lang['searchHeaderKeywords']	= "Schl&#252;sselw&#246;rter";
	$lang['searchHeaderFields']		= "Felder";
	$lang['searchHeaderType']		= "Typ";
	$lang['searchHeaderOrientation']= "Orientierung";
	$lang['searchHeaderColor']		= "Farbe";
	$lang['searchHeaderGalleries']	= "Galerien";	
    
?>