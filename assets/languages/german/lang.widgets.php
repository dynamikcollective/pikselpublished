<?php
	# 4.4.6
	$wplang['stats_visitors']		= "Approximate Site Visits";
	
	# 4.1.3
	$wplang['qstats_pending_media']	= "Pending Contributor Media";
	
	# 4.0.7	
	$wplang['sitehealth_exif']	= "PHP EXIF Support (exif_read_data)";
	
	# GENERAL WIDGET LANGUAGE
	$wplang['widget_close']		= "Schlie&szlig;en";
	$wplang['widget_save']		= "Speichern";
	$wplang['load_failed'] 		= "Laden der Tafel Panel fehlgeschlagen!";
	$wplang['widget_title']		= "Titel";
	$wplang['widget_note']		= "Notiz";
	$wplang['widget_for']		= "F&uuml;r";
	
	# NOTES WIDGET
	$wplang['notes_title']		= "Notizen";
	$wplang['notes_newnote']	= "neue Notiz";
	$wplang['notes_postedby']	= "erstellt von";
	$wplang['notes_lastupdate']	= "zuletzt aktualisiert";
	$wplang['notes_nonotes']	= "Es gibt keine Notizen zum Anzeigen";
	
	# EXTRAS WIDGET
	$wplang['extras_title']		= "PhotoStore Extras";
	
	# KTOOLS ACCOUNT WIDGET
	$wplang['kaccount_title']	= "Ktools Konto";
	$wplang['kaccount_support']	= "Support/Update Verbleibende Tage";
	$wplang['kaccount_messages']= "Ungelesene Nachrichten";
	$wplang['kaccount_affil']	= "Affiliate Verk&auml;ufe seit dem letztem Login";
	
	# KNEWS WIDGET
	$wplang['knews_title']		= "Ktools.net Neukkeiten";
	
	# QUICK STATS WIDGET - NEW AND PENDING
	$wplang['qstats_title']		= "Neu & Ausstehend";
	$wplang['qstats_logmem']	= "Neue Mitglieder seit letztem Login";
	$wplang['qstats_penmem']	= "Ausstehende Mitglieder";
	$wplang['qstats_logorders']	= "Neue Bestellungen seit letztem Login";
	$wplang['qstats_penorders']	= "Ausstehende Bestellungen";
	$wplang['qstats_logcomm']	= "Neue Kommentare seit letztem Login";
	$wplang['qstats_pencomm']	= "Ausstehende Kommentare";
	$wplang['qstats_logtags']	= "Neue Tags seit letztem Login";
	$wplang['qstats_pentags']	= "Ausstehende Tags";
	$wplang['qstats_lograte']	= "Neue Bewertungen seit letztem Login";
	$wplang['qstats_penrate']	= "Ausstehende Bewertungen";
	$wplang['qstats_penbios']	= "Ausstehende Mitglied Bios";
	$wplang['qstats_penavatars']= "Ausstehende Avatars";
	$wplang['qstats_pensupport']= "Ausstehende Support Tickets";
	
	# STATS WIDGET
	$wplang['stats_title']		= "Statistik";
	$wplang['stats_op1']		= "Verk&auml;ufe";
	$wplang['stats_op2']		= "Mitglieder";
	$wplang['stats_op_7days']	= "Letzten 7 Tage";
	$wplang['stats_op_6mon']	= "Letzte 6 Monate";
	$wplang['stats_op_5year']	= "Letzte 5 Jahre";
	$wplang['stats_tdsales']	= "heutige Verk&auml;ufe";
	$wplang['stats_tdorders']	= "heutige Bestellungen";
	$wplang['stats_sales']		= "Verk&auml;ufe";
	$wplang['stats_orders']		= "Bestellungen";	
	$wplang['stats_atsales']	= "gesamt Verk&auml;ufe";
	$wplang['stats_atorders']	= "gesamt Bestellungen";
	$wplang['stats_tdmems']		= "neue Mitglieder heute";
	$wplang['stats_mems']		= "neue Mitglieder";
	$wplang['stats_tamems']		= "aktive Mitglieder insgesamt";
	$wplang['stats_timems']		= "inaktive Mitglieder insgesamt";
	
	
	# SITE HEALTH WIDGET
	$wplang['sitehealth_title']	= "Seiten & Server Auslastung";
	$wplang['sitehealth_ok']	= "OK";
	$wplang['sitehealth_low']	= "GERING";
	$wplang['sitehealth_high']	= "HOCH";
	$wplang['sitehealth_failed']= "FEHLGESCHLAGEN";
	$wplang['sitehealth_unava'] = "nicht verf&uuml;gbar";
	$wplang['sitehealth_off']	= "AUS";
	$wplang['sitehealth_on']	= "AN";
	$wplang['sitehealth_inst']	= "Installiert";
	$wplang['sitehealth_none']	= "Keine";
	$wplang['sitehealth_exists']= "Existierende";
	$wplang['sitehealth_write']	= "Beschreibbar";
	$wplang['sitehealth_nonwri']= "Nicht Beschreibbar";
	$wplang['sitehealth_php']	= "PHP Version";
	$wplang['sitehealth_gd']	= "GD Library";
	$wplang['sitehealth_mem']	= "PHP Speicher Limit";
	$wplang['sitehealth_exe']	= "PHP max_execution_time";
	$wplang['sitehealth_time']	= "PHP max_input_time";
	$wplang['sitehealth_file']	= "PHP upload_max_filesize";
	$wplang['sitehealth_post']	= "PHP post_max_size";
	$wplang['sitehealth_safe']	= "PHP safe_mode";
	$wplang['sitehealth_dbv']	= "Database/Product Version Check";
	$wplang['sitehealth_mysqlv']= "MySQL Version";
	$wplang['sitehealth_load']	= "Server Auslastung";
	$wplang['sitehealth_upti']	= "Server Zeit aeit letzten boot";
	
	# UPDATE CHECK WIDGET
	$wplang['updater_title']	= "Auf Updates pr&uuml;fen";
	$wplang['updater_newest']	= "Sie haben die neueste Version";
	$wplang['updater_update']	= "Update verf&uuml;gbar";
	$wplang['updater_newestis']	= "Die neueste Version ist"; 
	$wplang['updater_getnew']	= "Um die neueste Version zu bekommen loggen Sie sich bitte mi ihrem <a href='http://www.ktools.net/members/' target='_blank'>Ktools.net account</a> ein";
	$wplang['updater_yourv']	= "Sie nutzen Version";
	
	# BLANK WIDGET
	$wplang['blank_title']		= "Leere Tafel";
	
	# CALENDAR WIDGET
	$wplang['calendar_title']	= "Kalender";
	
	# TIPS CHECK WIDGET
	$wplang['tips_title']		= "Tipps";
	$wplang['tips_next']		= "N�chster Tip";
	$welcometip[] 				= "Wussten Sie schon, dass Sie <strong> Strg + Umschalt + S </ strong> verwenden k&ouml;nnen um das Men&uuml; zu &Ouml;ffnen und zu Schlie&szlig;en."; 
	$welcometip[] 				= "Sie k&ouml;nnen die Willkommen Paletten per Drag & Drop neu anordnen wo immer Sie m&ouml;chten. Einfach auf den Titel klicken und ziehen. Dann lassen Sie die Palette wieder los, wenn die Palette in der Reihenfolge angeordnet ist die Sie wollten."; 
	$welcometip[] 				= "F&uuml;r schnellen Zugriff auf Ihre Kategorien / Events und viele anderen Bereichen klicken auf die Registerkarte mit dem Pfeil in oben links. Es &ouml;ffnet sich das Shortcuts Men�. Klicken Sie auf die Registerkarte diese wieder zu schlie&szlig;en."; 
	
?>