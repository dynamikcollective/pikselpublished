<?php
	# GATEWAYS LANG
	# 4.6.3
	# STRIPE
	$lang['stripe_displayName']			= "Stripe";
	$lang['stripe_publicDescription']	= "Credit Card or Debit Card";
	$lang['stripe_pkey']				= "Publishable Key";
	$lang['stripe_pkey_d']				= "Your publishable key for your stripe account.";
	$lang['stripe_skey']				= "Secret Key";
	$lang['stripe_skey_d']				= "Your secret key for your stripe account.";
	
	# 4.1.4
	# MOLLIE IDEAL
	$lang['mollieideal_displayName']		= "iDeal (Mollie)";
	$lang['mollieideal_f_partnerid']		= "Partner ID";
	$lang['mollieideal_f_partnerid_d']		= "Your iDeal Partner ID.";
	$lang['mollieideal_f_profilekey']		= "Profile key";
	$lang['mollieideal_f_profilekey_d']		= "Your Mollie iDeal Profile key.";
	$lang['mollieideal_f_testmode']			= "Test Mode";
	$lang['mollieideal_f_testmode_d']		= "Turn your Mollie iDeal into testing mode.";
	$lang['mollieideal_instructions']		= "Make sure the testmode corresponds to the settings in your Mollie.nl account.";
	$lang['mollieideal_publicDescription']	= "Pay safe and easy using iDeal (only available in the Netherlands).";
	
	# 4.1
	# ONEBIP 
	$lang['onebip_displayName']		= "OneBip"; 
	$lang['onebip_merchantid']			= "Email";	
	$lang['onebip_merchantid_d']		= "Enter the email used for your onebip Account."; 
	$lang['onebip_merchantkey']		= "API Key"; 
	$lang['onebip_merchantkey_d']		= "Enter the API key for your onebip Account.";
	$lang['onebip_testmode']			= "Test Mode";
	$lang['onebip_testmode_d']			= "Put your onebip payment into testing mode.";
	$lang['onebip_publicDescription']	= "Pay by Mobile";
	
	# PAYFAST 
	$lang['payfast_displayName']		= "PayFast"; 
	$lang['payfast_merchantid']			= "Merchant ID";	
	$lang['payfast_merchantid_d']		= "Enter the merchant ID for your PayFast Account."; 
	$lang['payfast_merchantkey']		= "Merchant Key"; 
	$lang['payfast_merchantkey_d']		= "Enter the merchant key for your PayFast Account.";
	$lang['payfast_testmode']			= "Test Mode"; 
	$lang['payfast_testmode_d']			= "Put your PayFast into testing mode.";
	$lang['payfast_publicDescription']	= "Credit Card or Bank Account"; 
	
	# NOCHEX
	$lang['nochex_displayName']		= "Nochex";
	$lang['nochex_f_accountid']		= "Account ID";
	$lang['nochex_f_accountid_d']	= "Ihre Nochex Account ID.";
	$lang['nochex_f_testmode']		= "Test Modus";
	$lang['nochex_f_testmode_d']		= "Setzen Sie Ihre Nochex Konto in den Testmodus.";
	$lang['nochex_publicDescription']= "Kredit-oder Kundenkarte";
	
	# WORLDPAY
	$lang['worldpay_displayName']		= "WorldPay";
	$lang['worldpay_f_installid']		= "Installation ID";
	$lang['worldpay_f_installid_d']	= "Installation ID f&uuml;r Ihr WorldPay Konto.";
	$lang['worldpay_f_testmode']			= "Test Modus";
	$lang['worldpay_f_testmode_d']		= "Setzen Sie Ihre WorldPay Konto in den Testmodus.";
	$lang['worldpay_publicDescription']= "Kredit-oder Kundenkarte";
	
	# ROBOKASSA
	$lang['robokassa_displayName']		= "RoboKassa";
	$lang['robokassa_f_merchantid']		= "H&auml;ndler ID";
	$lang['robokassa_f_merchantid_d']	= "H&auml;ndler ID f&uuml;r Ihr RoboKassa Konto.";
	$lang['robokassa_f_merchantpass']			= "H&auml;ndler Pass";
	$lang['robokassa_f_merchantpass_d']		= "Ihre RoboKassa H&auml;ndler Pass ID.";
	$lang['robokassa_publicDescription']= "Kredit-oder Kundenkarte";
	
	# PAYGATE
	$lang['paygate_displayName']		= "PayGate";
	$lang['paygate_f_accountid']		= "PayGate ID";
	$lang['paygate_f_accountid_d']	= "ID f&uuml;r Ihr PayGate Konto.";
	$lang['paygate_f_accountkey']			= "PayGate Key";
	$lang['paygate_f_accountkey_d']		= "Ihre PayGate key ID.";
	$lang['paygate_f_testmode']			= "Test Modus";
	$lang['paygate_f_testmode_d']		= "Setzen Sie Ihr PayGate Konto in den Testmodus.";	
	$lang['paygate_f_testid']				= "Test PayGate ID";
	$lang['paygate_f_testid_d']			= "Ihre PayGate test id";
	$lang['paygate_f_testkey']			= "PayGate Key Testen";
	$lang['paygate_f_testkey_d']		= "Ihr PayGate test Key";
	$lang['paygate_publicDescription']= "Kredit-oder Kundenkarte";
	
	# PAYSTATION
	$lang['paystation_displayName']		= "PayStation";
	$lang['paystation_f_accountid']		= "PayStation ID";
	$lang['paystation_f_accountid_d']	= "ID f&uuml;r Ihr PayStation Konto.";
	$lang['paystation_f_testmode']			= "Test Modus";
	$lang['paystation_f_testmode_d']		= "Setzen Sie Ihr PayStation Konto in den Testmodus.";	
	$lang['paystation_f_gatewayid']			= "Gateway ID";
	$lang['paystation_f_gatewayid_d']		= "Ihre PayStation gateway ID.";
	$lang['paystation_publicDescription']= "Kredit-oder Kundenkarte";
	
	# SKRILL
	$lang['skrill_displayName']		= "Skrill (moneybookers)";
	$lang['skrill_f_email']		= "E-Mail-Adresse";
	$lang['skrill_f_email_d']	= "E-Mail-Adresse f&uuml;r Ihr Skrill Konto.";
	$lang['skrill_f_testmode']			= "Test Modus";
	$lang['skrill_f_testmode_d']		= "Setzen Sie Ihr Skrill Konto in den Testmodus.";	
	$lang['skrill_f_testemail']			= "Test E-mail";
	$lang['skrill_f_testemail_d']		= "Ihre Skrill Test E-Mail-Adresse.";
	$lang['skrill_publicDescription']= "Kredit-oder Kundenkarte";
	$lang['skrill_f_completeOrder'] = "Klicken Sie hier, um Ihre Bestellung abzuschlie&szlig;en!";
	
	# CHRONOPAY
	$lang['chronopay_displayName']		= "ChronoPay";
	$lang['chronopay_f_clientid']		= "Client ID";
	$lang['chronopay_f_clientid_d']	= "Ihre ChronoPay Client ID.";
	$lang['chronopay_f_siteid']		= "Site ID";
	$lang['chronopay_f_siteid_d']		= "Ihre ChronoPay Site ID.";
	$lang['chronopay_f_productid']		= "Produkt ID.";
	$lang['chronopay_f_productid_d']		= "Ihre ChronoPay Produkt ID.";
	$lang['chronopay_publicDescription']= "Kredit-oder Kundenkarte";
	
	# IDEAL
	$lang['ideal_displayName']		= "iDeal";
	$lang['ideal_f_accountid']		= "Account ID";
	$lang['ideal_f_accountid_d']	= "Ihr iDeal Konto ID.";
	$lang['ideal_f_transkey']		= "Secret Key";
	$lang['ideal_f_transkey_d']		= "Ihr iDeal Secret Key.";
	$lang['ideal_f_testmode']		= "Test Modus";
	$lang['ideal_f_testmode_d']		= "Setzen Sie Ihr iDeal Konto in den Testmodus.";
	$lang['ideal_publicDescription']= "Bankkonto";
	
	# PAYPAL
	$lang['paypal_displayName']			= "PayPal";
	$lang['paypal_f_email']				= "E-mail Addresse";
	$lang['paypal_f_email_d']			= "E-mail Addresse f&uuml;r Ihr PayPal Konto.";
	$lang['paypal_f_testmode']			= "Test Modus";
	$lang['paypal_f_testmode_d']		= "Setzen Sie Ihr PayPal Konto in den Testmodus.";	
	$lang['paypal_f_testemail']			= "Test E-mail";
	$lang['paypal_f_testemail_d']		= "Ihre PayPal sandbox Test E-mail Addresse.";
	$lang['paypal_publicDescription']	= "PayPal, Kreditkarte oder Bankkonto";
	
	# 2CHECKOUT
	$lang['2checkout_displayName']		= "2Checkout.com";
	$lang['2checkout_f_accountid']		= "Account ID";
	$lang['2checkout_f_accountid_d']	= "Ihre 2Checkout Account ID.";
	$lang['2checkout_f_testmode']		= "Test Modus";
	$lang['2checkout_f_testmode_d']		= "Setzen Sie Ihr 2Checkout Konto in den Testmodus.";
	$lang['2checkout_instructions']		= "In 2Checkout.com set direct return to 'Header Redirect (Your URL)' and set approved URL to http://www.YOUR_DOMAIN_NAME.com/assets/gateways/2checkout/ipn.php";
	$lang['2checkout_publicDescription']= "Kreditkarte oder or US Bankkonto";

	
	# Plug n' Pay
	$lang['plugnpay_displayName']		= "Plug n' Pay";
	$lang['plugnpay_f_accountid']		= "Konto ID";
	$lang['plugnpay_f_accountid_d']		= "Ihre Plug n' Pay Account ID.";
	$lang['plugnpay_publicDescription']	= "";
	
	# AUTHORIZE.NET
	$lang['authorize_displayName']		= "Authorize.net";
	$lang['authorize_f_apiid']			= "API Login ID";
	$lang['authorize_f_apiid_d']		= "Ihre Authorize.net API Login ID.";
	$lang['authorize_f_transkey']		= "Transaktion Schl&uuml;ssel";
	$lang['authorize_f_transkey_d']		= "Ihr Authorize.net Transaktion Schl&uuml;ssel.";
	$lang['authorize_f_testmode']		= "Test Modus";
	$lang['authorize_f_testmode_d']		= "Setzen Sie Ihr Authorize.net Konto in den Testmodus.";
	$lang['authorize_publicDescription']= "";
	$lang['authorize_f_completeOrder'] = "Klicken Sie hier, um Ihre Bestellung abzuschlie&szlig;en!";
	
	# MYGATE.CO.ZA
	$lang['mygate_displayName']			= "MyGate.co.za";
	$lang['mygate_f_merchantid']		= "H&auml;ndler ID";
	$lang['mygate_f_merchantid_d']		= "Ihre MyGate.co.za H&auml;ndler ID.";
	$lang['mygate_f_appid']				= "Application ID";
	$lang['mygate_f_appid_d']			= "Ihre MyGate.co.za Application ID.";
	$lang['mygate_publicDescription']	= "";
	$lang['mygate_f_testmode']			= "Test Modus";
	$lang['mygate_f_testmode_d']		= "Setzen Sie Ihr Mygate Konto in den Testmodus.";	
	
	# MAIL IN PAYMENT
	$lang['mailin_displayName']			= "E-Mail In Zahlung";
	$lang['mailin_f_instructions']		= "E-Mail In Zahlung Anleitung";
	$lang['mailin_f_instructions_d']	= "Anleitungen f&uuml;r Kunden, um E-Mail In Zahlung zu schicken, wie Schecks oder Zahlungsanweisungen.";
	$lang['mailin_publicDescription']	= "Mailen Sie Ihre Zahlung an die Adresse, die auf der n&auml;chsten Seite zur Verf&uuml;gung gestellt wird.";
?>