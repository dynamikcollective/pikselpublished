<?php
	
	# 4.7.3
	$mgrlang['del_linked_file']					= "Deliver Linked File";
	$mgrlang['file_linked']						= "File Linked";
	$mgrlang['other_digital_sizes']				= "Other Digital Sizes";
	$mgrlang['media_f_el']						= "External Link";
	$mgrlang['media_f_el_d']					= "Link to file if not stored locally.";
	
	# 4.7
	$mgrlang['webset_f_fotomoto']				= "Fotomoto Store ID";
	$mgrlang['webset_f_fotomoto_d']				= "To enable Fotomoto support enter your store ID.";
	# plupload
	$mgrlang['plupSelectFiles']					=  "Select files";
	$mgrlang['plupAddFilesToQueue']				=  "Add files to the upload queue and click the start button.";
	$mgrlang['plupFilename']					=  "Filename";
	$mgrlang['plupStatus']						=  "Status";
	$mgrlang['plupSize']						=  "Size";
	$mgrlang['plupAddFiles']					=  "Add files";
	$mgrlang['plupStartUplaod']					=  "Start upload";
	$mgrlang['plupStopUpload']					=  "Stop current upload";
	$mgrlang['plupStartQueue']					=  "Start uploading queue";
	$mgrlang['plupDragFilesHere']				=  "Drag files here.";
	$mgrlang['feature_gallery']					=  "Feature Gallery On Home Page";
	$mgrlang['feature_gallery_d']				=  "Feature this gallery on the home page.";
	
	# 4.6.1
	$mgrlang['image_zoom_warning']				= "The Image Zoom feature prevents sample photos on the details page from scaling for mobile devices.";
	
	# 4.6
	$mgrlang['webset_f_facebook_link']			= "Link To Facebook";
	$mgrlang['webset_f_facebook_link_d']		= "Add a link to your Facebook page.";
	$mgrlang['webset_f_twitter_link']			= "Link To Twitter";
	$mgrlang['webset_f_twitter_link_d']			= "Add a link to your Twitter page.";
	$mgrlang['webset_social_set']				= "Social";
	
	# 4.4.6
	$mgrlang['mem_del_bill']				= "Are you sure you would like to delete this bill?";
	
	# 4.4.5
	$mgrlang['setup_f_pubuploader']			= "Default Public Batch Uploader";
	$mgrlang['setup_f_pubuploader_d']		= "Default batch uploader for members.";
	$mgrlang['setup_f_uploader_op4']		= "Plupload (HTML5, Flash, Gears, Silverlight, HTML4)";
	
	# 4.4.4
	$mgrlang['gen_code_link_d']						= "HTML code to insert into your template for a link to this agreement.";
	$mgrlang['gen_email_template']					= "Email Template";
	$mgrlang['util_f_sql'] 							= "SQL Execute";
	$mgrlang['util_f_sql_d'] 						= "Insert a query to execute. WARNING! Do not use this unless you know what you are doing!";
	$mgrlang['util_f_exec']							= "Execute";
	$mgrlang['util_mes_07']							= "Your query ran successfully.";
	$mgrlang['util_mes_08']							= "Your query failed to run.";
	
	# 4.4.3
	$mgrlang['media_f_pr']							= "Property Release";
	$mgrlang['media_f_pr_d']						= "Does this media have a property release form.";
	$mgrlang['landf_f_gallerySorting']				= "Sorting Gallery List";
	$mgrlang['landf_f_gallerySorting_d']			= "Set how you wish for your galleries to be sorted in a list. (IMPORTANT! you will not see the change right away)";
	$mgrlang['landf_f_gallerySorting_ascending']	= "Ascending A-Z";
	$mgrlang['landf_f_gallerySorting_descending']	= "Descending Z-A";
	$mgrlang['landf_f_gallerySortBy_name']			= "Gallery Name";
	$mgrlang['landf_f_gallerySortBy_sortnumber']	= "Gallery Sort Number";
	$mgrlang['landf_f_gallerySortBy_created']		= "Date Created";
	$mgrlang['landf_f_gallerySortBy_edited']		= "Date Edited";
	$mgrlang['landf_f_gallerySortBy_activedate']	= "Active Date";
	$mgrlang['landf_f_gallerySortBy_expiredate']	= "Expire Date";
	$mgrlang['landf_f_gallerySortBy_eventdate']		= "Event Date";
	$mgrlang['landf_f_gallerySortBy_eventlocation']	= "Event Location";
	$mgrlang['landf_f_gallerySortBy_eventcode']		= "Event Code";
	$mgrlang['cp_linked']							= "Link";
	$mgrlang['cp_linked_d']							= "Specify a link to where users who try to view this content will be redirected to. Example (http://www.--domain--.com).";
	
	# 4.4.2
	$mgrlang['landf_f_tagCloudSort_keyword']			= "Alpha-Numeric";
	$mgrlang['landf_f_tagCloudSort_default']			= "Popularity";
	$mgrlang['landf_f_tagCloudSort_d']					= "Use the drop down box to set how you would like the tag cloud keywords sorted.";
	$mgrlang['landf_f_tagCloudSort']					= "Tag Cloud Sort";
	$mgrlang['landf_f_tagCloudOn_d']					= "Show the tag cloud on the advanced search or no results returned during search.";
	$mgrlang['landf_f_tagCloudOn']						= "Tag Could";
	$mgrlang['gen_dig_sub_dl']							= "Digital Subscription Download";
	
	# 4.4.0
	$mgrlang['approve_media']				= "Approve Media";
	$mgrlang['rm_pricing']					= "Rights Managed Pricing";
	$mgrlang['rm_selections']				= "Rights Managed Pricing Selections";
	$mgrlang['landf_f_share'] 				= "Share";
	$mgrlang['landf_f_share_d'] 			= "Display the share media area on the details page where visitors can fetch BBcode, HTML, etc.";
	$mgrlang['landf_f_contri_link'] 		= "Display Contributors Page Link";
	$mgrlang['landf_f_contri_link_d'] 		= "Show the contributor page link in the top menu area.";	
	$mgrlang['rm_price_mod'] 				= "Price Modification";
	$mgrlang['rm_price_mod_d'] 				= "How the price will be modified when selecting this option.";
	$mgrlang['rm_price_d'] 					= "The price that is added, subtracted or multiplied to the running total.";
	$mgrlang['rm_credits_d'] 				= "The credits that are added, subtracted or multiplied to the running total.";
	$mgrlang['rm_option_name'] 				= "Option Name";
	$mgrlang['rm_option_name_d'] 			= "Name of this option.";
	$mgrlang['rm_option_group'] 			= "Option Group";
	$mgrlang['rm_option_group_d'] 			= "Select the option group that this is an option for.";
	$mgrlang['rm_message_1'] 				= "You need to create option groups before you create options.";
	$mgrlang['rm_message_2'] 				= "This RM scheme has no option groups. Click 'New Option Group' to start.";	
	$mgrlang['rm_og_name'] 					= "Option Group Name";
	$mgrlang['rm_og_name_d'] 				= "The name of this option group.";	
	$mgrlang['rm_link_to'] 					= "Link To";
	$mgrlang['rm_link_to_d'] 				= "Select the options that this group is linked to. This group (dropdown) will then be shown when that option is selected.";	
	$mgrlang['gen_base_price'] 				= "Base Price";
	$mgrlang['gen_base_credits'] 			= "Base Credits";	
	$mgrlang['rm_custom_bc'] 				= "Set custom base cost";
	$mgrlang['rm_custom_mp'] 				= "Use media's price/credits as base cost";		
	$mgrlang['rm_cps'] 						= "Configure Pricing Scheme";
	$mgrlang['gen_pricing_scheme']			= "Pricing Scheme";
	$mgrlang['gen_new_op_grp']				= "New Option Group";
	$mgrlang['gen_new_option']				= "New Option";
	$mgrlang['gen_edit_op_grp']				= "Edit Option Group";
	$mgrlang['gen_edit_option']				= "Edit Option";
	$mgrlang['webset_f_minicart']			= "Mini Cart";
	$mgrlang['webset_f_minicart_d']			= "Dynamic add to cart feature that will keep your customer on the current page when adding an item to the cart.";

	# 4.3
	$mgrlang['mem_mes_10']					= "No bills exist under this account.";
	$mgrlang['taxvat_id']					= "TAX/VAT ID";
	$mgrlang['landf_f_license']				= "Display License";
	$mgrlang['landf_f_license_d']			= "Display the license type for digital media. Unchecking this will hide the license type everywhere on the public site.";
	$mgrlang['webset_f_taxid']				= "Request VAT ID Number";
	$mgrlang['webset_f_taxid_d']			= "Request a customers VAT ID number when checking out.";	
	$mgrlang['webset_f_onotes']				= "Customer Notes";
	$mgrlang['webset_f_onotes_d']			= "Allow customers to add notes to their order.";
	$mgrlang['order_onotes_d']				= "Notes the customer entered with their order.";	
	$mgrlang['subnav_licenses']				= "Licenses";
	$mgrlang['subnav_licenses_d']			= "Create and edit license types for your media.";	
	$mgrlang['licenses_new_header']			= "Add License";
	$mgrlang['licenses_edit_header']		= "Edit License";
	$mgrlang['licenses_new_message']		= "With the following form you can add a new license type.";
	$mgrlang['licenses_edit_message']		= "Edit this license type and click the Save Changes button.";
	$mgrlang['licenses_tab1']				= "Details";
	$mgrlang['licenses_tab2']				= "Groups";
	$mgrlang['licenses_f_name']				= "License Name";
	$mgrlang['licenses_f_name_d']			= "The name of the license type. Example: Royalty Free.";	
	$mgrlang['licenses_f_groups']			= "License Groups";
	$mgrlang['licenses_f_groups_d']			= "Select the groups that this license type belongs to.";
	$mgrlang['licenses_f_description_d']	= "A short description of this license type.";	
	$mgrlang['licenses_f_handle_d']			= "Select how this license should be hanlded on the site.";
	$mgrlang['gen_reg_license']				= "Regular License";

	# 4.2.3
	$mgrlang['mem_mes_10']					= "No bills exist under this account.";
	$mgrlang['taxvat_id']					= "TAX/VAT ID";
	$mgrlang['landf_f_license']				= "Display License";
	$mgrlang['landf_f_license_d']			= "Display the license type for digital media. Unchecking this will hide the license type everywhere on the public site.";
	$mgrlang['webset_f_taxid']				= "Request VAT ID Number";
	$mgrlang['webset_f_taxid_d']			= "Request a customers VAT ID number when checking out.";	
	$mgrlang['webset_f_onotes']				= "Customer Notes";
	$mgrlang['webset_f_onotes_d']			= "Allow customers to add notes to their order.";
	$mgrlang['order_onotes_d']				= "Notes the customer entered with their order.";	
	$mgrlang['subnav_licenses']				= "Licenses";
	$mgrlang['subnav_licenses_d']			= "Create and edit license types for your media.";	
	$mgrlang['licenses_new_header']			= "Add License";
	$mgrlang['licenses_edit_header']		= "Edit License";
	$mgrlang['licenses_new_message']		= "With the following form you can add a new license type.";
	$mgrlang['licenses_edit_message']		= "Edit this license type and click the Save Changes button.";
	$mgrlang['licenses_tab1']				= "Details";
	$mgrlang['licenses_tab2']				= "Groups";
	$mgrlang['licenses_f_name']				= "License Name";
	$mgrlang['licenses_f_name_d']			= "The name of the license type. Example: Royalty Free.";	
	$mgrlang['licenses_f_groups']			= "License Groups";
	$mgrlang['licenses_f_groups_d']			= "Select the groups that this license type belongs to.";
	$mgrlang['licenses_f_description_d']	= "A short description of this license type.";
	
	# 4.2.2
	$mgrlang['tax_dit']						= "Digital Item Taxes";
	$mgrlang['tax_dit_d']					= "(Media, Credits, Subscriptions, Memberships)";
	$mgrlang['tax_pit']						= "Physical Product Taxes";
	$mgrlang['tax_pit_d']					= "(Prints, Products, Packages)";	
	$mgrlang['tax_f_taxms_d']				= "Apply tax to memberships";
	$mgrlang['tax_f_taxprints_d']			= "Apply tax to prints and products.";
	$mgrlang['tax_f_taxshipping_d']			= "Apply tax to shipping costs.";
	$mgrlang['tax_f_taxdigital_d']			= "Apply tax to digital media downloads.";	
	$mgrlang['tax_f_taxsubs_d']				= "Apply tax to digital subscriptions.";
	$mgrlang['tax_f_taxcredits_d']			= "Apply tax to digital credits.";
	$mgrlang['country_f_tax_ms_d']			= "Apply tax when a customer purchases memberships in this country.";
	$mgrlang['states_f_tax_ms_d']			= "Apply tax when a customer purchases memberships in this state/province.";
	$mgrlang['zipcodes_f_tax_ms_d']			= "Apply tax when a customer purchases memberships in this zip/postal code.";
	$mgrlang['landf_watermark'] 			= "Watermarks";
	$mgrlang['landf_f_uploadwatermark'] 	= "Upload Watermark";
	$mgrlang['landf_f_deletewatermark'] 	= "Delete Watermarks";
	$mgrlang['landf_f_uploadwatermark_d'] 	= "The watermark must be a PNG format, anything else will not work!";
	$mgrlang['landf_f_deletewatermark_d'] 	= "Put a check in the boxes below for the watermarks you wish to delete, then click save button.";
	
	# 4.2.1
	$mgrlang['contact_captcha']				= "Contact Form Captcha";
	$mgrlang['contact_captcha_d']			= "Turn on captcha for the contact us form on your store front so that visitors have to enter a unique code when signing up. This will preven spambots from flooding your contact form.";
	$mgrlang['digital_downloads_upper']		= "DIGITAL DOWNLOADS";
	$mgrlang['products_upper']				= "PRODUCTS";

	# 4.1.7

	$mgrlang['dsp_op_eu']					= "Editorial Use";

	$mgrlang['mem_f_msexpires']				= "Expiration";

	$mgrlang['mem_f_msexpires_d']			= "When the current membership plan ends for this member. Once ended they will be placed on the basic default membership";

	$mgrlang['add_selected']				= "Add Selected";

	$mgrlang['remove_selected']				= "Remove Selected";

	

	# 4.1.6

	$mgrlang['batch_edit']					= "集體修改";

	$mgrlang['saved']						= "儲存";

	$mgrlang['subscription_f_dtotal']		= "總下載數目";

	$mgrlang['subscription_f_dtotal_d']		= "可讓訂閱會員下載的數量.讓其空白若是無限制性.";

	$mgrlang['gen_t_dlrem_caps']			= "總下載數目/剩下";

	$mgrlang['no_limit']					= "無限制性";

	

	# 4.1.4

	$mgrlang['pay_commission']				= "支付傭金";

	

	# 4.1.3

	$mgrlang['landf_f_watermark_d']			= "應用浮水印的功能在網頁上的照片.將使用所設置的相同的浮水印進行預覽.";

	$mgrlang['mem_del_sub']					= "你確定你想刪除此訂閱?";

	$mgrlang['dsp_op_ex']					= "擴展許可證";

	

	# 4.1.2

	$mgrlang['contributors_media']			= "貢獻者媒體";

	

	# 4.1

	$mgrlang['dp_watermark']				= "浮水印";

	$mgrlang['dp_watermark_d']				= "選擇一個浮水印顯示在這個版本時下載, 請設置為none，如果你不希望有任何浮水印.";

	$mgrlang['order_item_id'] 				= "身分證";

	$mgrlang['order_item_file'] 			= "檔案";

	$mgrlang['order_item_ofile'] 			= "原始檔案";

	$mgrlang['order_item_afile'] 			= "附屬檔案";

	$mgrlang['geo_location_area']			= "地理位置區";

	$mgrlang['geo_on_off']					= "地理位置區 開/關";

	$mgrlang['geo_on_off_d']				= "打開或關閉的地理定位功能.";

	$mgrlang['geo_width']					= "地圖寬度";

	$mgrlang['geo_width_d'] 				= "你想顯示地圖的寬度.";

	$mgrlang['geo_height'] 					= "地圖高度";

	$mgrlang['geo_height_d'] 				= "你想顯示地圖的高度.";

	$mgrlang['geo_zoom'] 					= "地圖縮放";

	$mgrlang['geo_zoom_d'] 					= "設置地圖的縮放級別, 數字越大，接近地面水平.";

	$mgrlang['geo_pin_color'] 				= "Pin顏色";

	$mgrlang['geo_pin_color_d'] 			= "置了PIN碼或標記的顏色 (例子: 黑色, 白色, 紅色, 等等..).";

	$mgrlang['geo_map_type'] 				= "地圖類型";

	$mgrlang['geo_map_type_d']				= "設置地圖顯示.";

	$mgrlang['geo_map_type_roadmap'] 		= "路線圖";

	$mgrlang['geo_map_type_satellite']		 = "衛星";

	$mgrlang['geo_map_type_terrain']		= "領域";

	$mgrlang['geo_map_type_hybrid']			= "混合(路地圖+衛星)";

	$mgrlang['zoom_location_area']			= "圖片放大區";

	$mgrlang['zoom_on_off']					= "圖片放大/關閉";

	$mgrlang['zoom_on_off_d']				= "打開或關閉圖像縮放功能.";

	$mgrlang['zoom_lens_size']				= "圖片放大鏡頭尺寸";

	$mgrlang['zoom_lens_size_d'] 			= "輸入的變焦透鏡的尺寸為.";

	$mgrlang['zoom_border_size'] 			= "圖片放大邊框大小";

	$mgrlang['zoom_border_size_d'] 			= "輸入邊框大小的變焦鏡頭.";

	$mgrlang['zoom_border_color'] 			= "圖片放大邊框顏色";

	$mgrlang['zoom_border_color_d'] 		= "輸入圖像縮放邊框的顏色.";

	$mgrlang['gen_del_manually']			= "提供手動";

	$mgrlang['gen_del_orig']				= "提供原始檔";

	$mgrlang['gen_create_auto_mes']			= "自動創建時，僅適用於原上傳的檔是一個JPG.您的主機可能有記憶體限制，以防止自動創建的大幅照片.如果您遇到記憶體問題，你將需要你的主機增加你的記憶體限制.";

	$mgrlang['dsp_f_delivery'] 				= "送貨方法";

	$mgrlang['dsp_f_delivery_d'] 			= "選擇這個檔將被交付給客戶.";

	$mgrlang['setup_measurement'] 			= "計量單位t";

	$mgrlang['setup_measurement_d'] 		= "選擇將要顯示的計量單位.";

	$mgrlang['setup_measurement_i'] 		= "公分";

	$mgrlang['setup_measurement_c'] 		= "公尺";

	$mgrlang['fileupload_title'] 			= "上傳文件";

	$mgrlang['fileupload_details'] 			= "上傳檔，可以使用在上面的內容區域.";

	$mgrlang['fileupload_source'] 			= "來源: (把檢查框，刪除專案)";

	$mgrlang['approvalStatus0']				= "待批准";

	$mgrlang['approvalStatus1']				= "已批准";

	$mgrlang['approvalStatus2']				= "失敗審批";

	$mgrlang['media_f_astatus']				= "審批狀態";

	$mgrlang['media_f_astatus_d']			= "此媒體的審批狀態.";

	$mgrlang['media_f_astatus_mes']			= "消息/原因";

	$mgrlang['subnav_contr_sales']			= "貢獻者銷售";

	$mgrlang['subnav_contr_sales_d']		= "欠供款的銷售和傭金.";

	$mgrlang['mem_tab17']					= "銷售";

	$mgrlang['no_sales']					= "沒有銷售記錄";

	$mgrlang['mem_f_profviews']				= "資料查看";

	$mgrlang['mem_f_profviews_d']			= "該會員的公眾資料已被瀏覽的次數.";	

	$mgrlang['webset_f_cos']				= "委員會會員計畫下載";

	$mgrlang['webset_f_cos_d']				= "每下載一個客戶在訂閱的貢獻者委員會.";

	$mgrlang['one_download']				= "1下載";

	$mgrlang['gen_pay']						= "付";

	$mgrlang['mark_as_paid']				= "標記列作繳足";

	$mgrlang['mark_as_unpaid']				= "標記列作為還未繳足";

	$mgrlang['no_member']					= "會員不存在!";

	$mgrlang['send_cmo']					= "寄支票/匯票";

	$mgrlang['pay_paypal']					= "通過PayPal支付";

	$mgrlang['com_pay_from']				= "金支付";

	$mgrlang['commission_display']			= "顯示傭金狀態";

	$mgrlang['contributor_sale']			= "貢獻者的銷售e";

	$mgrlang['contributor_sale_d']			= "貢獻者的銷售和傭金的詳細資訊.";

	$mgrlang['contrsales_f_ps_d']			= "該貢獻者傭金支付的狀態.";

	$mgrlang['contrsales_f_on_d']			= "這發生在訂購傭金.";

	$mgrlang['contrsales_f_com']			= "傭金";

	$mgrlang['contrsales_f_com_d']			= "在此出售賺取的傭金的金額.";	

	$mgrlang['contrsales_f_sd']				= "銷售日期";

	$mgrlang['contrsales_f_sd_d']			= "最初發生的日期銷售.";

	$mgrlang['contrsales_f_dp']				= "日期標示為有支付";

	$mgrlang['contrsales_f_dp_d']			= "這個被標記為支付日期.";	

	$mgrlang['contrsales_f_mem_d']			= "提供者擁有被出售的媒體.";	

	$mgrlang['contrsales_f_io']				= "訂購商品";

	$mgrlang['contrsales_f_io_d']			= "項目被勒令賺取傭金.";	

	$mgrlang['media_f_albums']				= "相冊";

	$mgrlang['media_f_albums_d']			= "員的相冊，這種媒體將顯示在.";

		

	

	# 4.0.9

	$mgrlang['mem_f_disname']				= "顯示名字";

	$mgrlang['mem_f_disname_d']				= "顯示名字在任何公開範圍.如果空白將使用真實名字";	

	$mgrlang['mem_f_showcase']				= "展示投稿人";

	$mgrlang['mem_f_showcase_d']			= "允許會員成為展示投稿人";

	$mgrlang['membership_f_album']			= "創建相簿";

	$mgrlang['membership_f_album_d']		= "允許會員在會員籍中創建他們的相簿在自己的帳號裡";	

	$mgrlang['webset_featured_media'] 		= "特色媒體區域";

	$mgrlang['gen_milliseconds'] 			= "千分之一秒";

	$mgrlang['landf_f_hpf_width']			= "寬度";

	$mgrlang['landf_f_hpf_width_d']			= "該網頁的寬度特色照片";	

	$mgrlang['landf_f_hpf_crop']			= "裁剪致";

	$mgrlang['landf_f_hpf_crop_d']			= "裁剪主頁相片到這一高度.";	

	$mgrlang['landf_f_hpf_fadespeed']		= "速度漸弱";

	$mgrlang['landf_f_hpf_fadespeed_d']		= "以毫秒為單位的時間，它需要一個圖片淡入或淡出";	

	$mgrlang['landf_f_hpf_inverval']		= "間隔";

	$mgrlang['landf_f_hpf_inverval_d']		= "時間以毫秒為單位顯示照片，然後移動到下一個";

	$mgrlang['landf_f_hpf_ddelay']			= "詳細資訊延遲";

	$mgrlang['landf_f_hpf_ddelay_d']		= "時間以毫秒為單位的詳細資訊被延遲彈出窗之前的顯示";	

	$mgrlang['landf_f_hpf_ddis']			= "詳細資訊顯示時間";

	$mgrlang['landf_f_hpf_ddis_d']			= "時間以毫秒為單位的詳細資訊彈出窗顯示之前隱藏起來";

	

	# 4.0.8

	$mgrlang['media_filemissing']			= "檔案遺失";

	

	# 4.0.7

	$mgrlang['setup_f_iptcutf']				= "UTF8 IPTC編碼";

	$mgrlang['setup_f_iptcutf_d']			= "從您的照片使用UTF8編碼讀取IPTC資訊.如果您被特殊字元困擾，請嘗試停用此功能";

	

	# 4.0.6

	$mgrlang['files_processed']				= "檔案處理";

	$mgrlang['elapsed']						= "流逝";

	$mgrlang['remaining']					= "提醒";

	$mgrlang['createBillScratch']				= "從頭開始創建一個帳單";

	$mgrlang['createBillLater']				= "創建一個會員的帳單";

	$mgrlang['landf_f_tospage']				= "允許使用條款頁面";

	$mgrlang['landf_f_pppage']				= "啟用隱私權政策";

	$mgrlang['landf_f_papage']				= "啟用採購協議";

	

	# 4.0.5

	$mgrlang['gen_imp_media']				= "選擇要傳送的檔或";

	$mgrlang['gen_files2']					= "檔案";

	$mgrlang['gen_folders']					= "資料夾";

	$mgrlang['gen_importmes']				= "選擇您想在此批傳送的檔，或點擊“上傳新媒體”按鈕來添加新的檔";	

	$mgrlang['all_media']					= "所有媒體";

	$mgrlang['orph_media']					= "單獨的媒體";

	$mgrlang['last_batch']					= "最後一批傳送";

	$mgrlang['added_today']					= "添加今天";

	$mgrlang['added_week']					= "添加最後一周";

	$mgrlang['added_month']					= "添加最後一個月";

	$mgrlang['featured_media']				= "特色媒體";

	$mgrlang['create_group']				= "創建群體";

	$mgrlang['gen_other']					= "其他";

	$mgrlang['gen_photo']					= "照片";

	$mgrlang['gen_video']					= "影片";

	$mgrlang['choose_thumb_mes']				= "點擊上傳按鈕選擇檔的縮略圖";

	$mgrlang['min_size_mes']				= "建議尺寸最少像素 800x600";

	$mgrlang['add_range']					= "添加範圍";

	$mgrlang['gen_sharpen']					= "清楚";

	$mgrlang['gen_na']						= "沒有";

	$mgrlang['landf_f_login']				= "展示登入/創建帳號";//需要翻譯""Show Login/Create Account";  // Needs translation

	$mgrlang['landf_f_login_d']				= "展示登入和創建帳號連接或者按鈕";//需要翻譯Show the login and create account links or buttons.";  // Needs translation

	

	# 4.0.4

	$mgrlang['gen_import_done_mes']			= "您的媒體已經傳送到您的網站";

	$mgrlang['gen_import_done_mes2']		= "請檢查任何紅色公告";

	$mgrlang['gen_manage_media']			= "管理我的媒體";

	

	# 4.0.1;

	$mgrlang['galleries_f_sort_d']			= "預設圖庫編排";

	$mgrlang['recommended']					= "建議";

	

		

	# COPYRIGHT

	$mgrlang['powered_by']					= "技術支援";

	$mgrlang['copyright']					= "版權所有©;2012 Ktools.net LLC,保留所有權利.";//版權所有必須保持，不能被刪除";



	# LOGIN AREA

	$mgrlang['login_title'] 				= "登入";

	$mgrlang['login_box_header']			= "管理範圍";

	$mgrlang['login_username']				= "使用名字";

	$mgrlang['login_password']				= "密碼";

	$mgrlang['login_recover_email_body']	= "以下是管理範圍登入資料";

	$mgrlang['login_mes_01']				= "這郵址沒有在資料庫";

	$mgrlang['login_mes_02']				= "您的登入資料將會電郵給您";

	$mgrlang['login_mes_03']				= "恢復您的登入資訊";

	$mgrlang['login_email_address']			= "電子信箱";

	$mgrlang['login_mes_04']				= "若你在5分鐘內沒有收到您的登入資料,請檢查您的垃圾過濾阻擋設定及確定沒被擋出";

	$mgrlang['login_back'] 					= "回去登入頁面";

	$mgrlang['login_mes_05']				= "無法登入.請再嘗試";

	$mgrlang['login_mes_06']				= "您已經登出";

	$mgrlang['login_mes_07']				= "您必須先登錄才能查看此頁面";

	$mgrlang['login_mes_08']				= "您的會話已超時";

	$mgrlang['login_mes_09']				= "請登入";

	$mgrlang['login_remember']				= "記得我";

	$mgrlang['login_recover']				= "恢復登入資訊";

	$mgrlang['login_enter']					= "登入";

	$mgrlang['login_send']					= "傳送";

	$mgrlang['login_check']					= "檢查使用名字/密碼...";

	$mgrlang['login_loggedin']				= "登入";

	$mgrlang['login_loggedout']				= "登出";

	$mgrlang['login_tab1']					= "登入";

	$mgrlang['login_tab2']					= "恢復密碼";

	$mgrlang['login_echeck']				= "檢查電子郵件地址...";

	

	# GENERAL AREAS

	$mgrlang['support_ticket']				= "支援編號"; 

	$mgrlang['ticket']						= "編號"; 

	$mgrlang['email']						= "電子信箱";  

	$mgrlang['start']						= "開始"; 

	$mgrlang['enc_decry']					= "譯成密碼/解碼";

	$mgrlang['start_importing']				= "開始傳送";

	$mgrlang['start_upload']				= "開始上載";

	$mgrlang['gen_preview']					= "預覽";

	$mgrlang['gen_attach_file']				= "附上檔案";

	$mgrlang['gen_each']					= "每個";

	$mgrlang['gen_downloads']				= "下載";

	$mgrlang['gen_dl_caps']					= "下載";

	$mgrlang['gen_bills']					= "票據";

	$mgrlang['gen_lu_caps']					= "最後更新";

	$mgrlang['gen_notice']					= "公告";

	$mgrlang['gen_language']				= "語言";

	$mgrlang['gen_tips']					= "提示";

	$mgrlang['gen_debug']					= "調試面板";

	$mgrlang['gen_pageload']				= "頁面載入中";

	$mgrlang['gen_seconds']					= "秒";

	$mgrlang['gen_off']						= "關閉";

	$mgrlang['gen_server']					= "服務";

	$mgrlang['change_batch']				= "您可以更改正在使用的批次上傳下";

	$mgrlang['gen_fileex']					= "接受檔案延長";

	$mgrlang['gen_cud']						= "當前使用者詳細資訊";

	$mgrlang['gen_addon']					= "安裝後附加組件";

	$mgrlang['gen_settings']				= "裝配方法"; 

	$mgrlang['gen_curpage']					= "現在頁面"; 

	$mgrlang['gen_logout']					= "登出"; 

	$mgrlang['gen_logged_as']				= "登錄的"; 

	$mgrlang['gen_open_pub']				= "公共網頁"; 

	$mgrlang['gen_edit_pub']				= "編輯方式"; 

	$mgrlang['gen_help']					= "幫助和支援"; 

	$mgrlang['gen_mgr_title']				= "管理範圍"; 

	$mgrlang['gen_info']					= "展示有關更多資料"; 

	$mgrlang['gen_demo_mode']				= "該軟體在示範模式下運行。某些功能可能會被禁用"; 

	$mgrlang['gen_b_search']				= "搜尋"; 

	$mgrlang['gen_search']					= "搜尋"; 

	$mgrlang['gen_search_results']			= "搜尋結果"; 

	$mgrlang['gen_b_cancel']				= "取消"; 

	$mgrlang['gen_b_save']					= "儲存"; 

	$mgrlang['gen_b_help']					= "幫助"; 

	$mgrlang['gen_b_update']				= "更新"; 

	$mgrlang['gen_b_clear_sort']			= "清除所有分類"; 

	$mgrlang['gen_add_new']					= "增加新"; 

	$mgrlang['gen_sel_all']					= "選擇全部"; 

	$mgrlang['gen_desel_all']				= "取消全部選擇"; 

	$mgrlang['gen_print_this']				= "這一頁而已"; 

	$mgrlang['gen_print_all']				= "全部頁面"; 

	$mgrlang['gen_b_print']					= "列印"; 

	$mgrlang['gen_print']					= "列印"; 

	$mgrlang['gen_prints']					= "列印"; 

	$mgrlang['gen_subs']					= "訂購"; 

	$mgrlang['gen_prod']					= "產品"; 

	$mgrlang['gen_prods']					= "產品"; 

	$mgrlang['gen_pack']					= "配套"; 

	$mgrlang['gen_packs']					= "配套"; 

	$mgrlang['gen_digital']					= "數碼";  

	$mgrlang['gen_coll']					= "收集";  

	$mgrlang['gen_colls']					= "收集";  

	$mgrlang['gen_media_types']				= "媒體類型";   

	$mgrlang['gen_sub']						= "訂購";  

	$mgrlang['gen_b_prepare']				= "準備";  

	$mgrlang['gen_delete_sel']				= "刪除選擇";  

	$mgrlang['gen_edit']					= "編輯這專案"; 

	$mgrlang['gen_assets']					= "查看媒體"; 

	$mgrlang['gen_default']					= "預設"; 

	$mgrlang['gen_short_view']				= "瀏覽"; 

	$mgrlang['gen_short_upload']			= "上傳"; 

	$mgrlang['gen_short_edit']				= "編輯"; 

	$mgrlang['gen_thumbnail']				= "縮略圖"; 

	$mgrlang['gen_vidsample']				= "影片樣本"; 	

	$mgrlang['gen_short_delete']			= "刪除"; 

	$mgrlang['gen_activate']				= "啟動"; 

	$mgrlang['gen_deactivate']				= "無效"; 

	$mgrlang['gen_upload']					= "上傳到這個圖庫"; 

	$mgrlang['gen_delete']					= "刪除這項目"; 

	$mgrlang['gen_b_cancel2']				= "取消"; 

	$mgrlang['gen_b_next']					= "下一頁"; 

	$mgrlang['gen_b_previous']				= "前一頁"; 

	$mgrlang['gen_b_purge_al']				= "清除之前"; 

	$mgrlang['gen_b_print']					= "列印"; 

	$mgrlang['gen_b_approve']				= "批准"; 

	$mgrlang['gen_b_approved']				= "批准"; 

	$mgrlang['gen_b_napproved']				= "不批准"; 

	$mgrlang['gen_b_display']				= "顯示";

	$mgrlang['gen_b_download']				= "下載 (CVS)"; 

	$mgrlang['gen_add_lang']				= "增加語言"; 

	$mgrlang['gen_sys_name']				= "系統"; 

	$mgrlang['gen_loading']					= "裝載..."; 

	$mgrlang['gen_addonerror']				= "沒有安裝這個附加元件"; 

	$mgrlang['gen_mes_locked']				= "上鎖"; 

	$mgrlang['gen_shortcuts']				= "文庫快速鍵"; 

	$mgrlang['gen_details']					= "細節"; 

	$mgrlang['gen_missing']					= "遺失"; 

	$mgrlang['gen_langfiles']				= "語言檔案"; 

	$mgrlang['gen_alerts']					= "通知"; 

	$mgrlang['gen_noalerts']				= "在這個時候有沒有通知"; 

	$mgrlang['gen_dbbackup']				= "您的資料庫有備份"; 

	$mgrlang['gen_dbbackup2']				= "資料庫備份"; 

	$mgrlang['gen_langerr1']				= "語言目錄安裝失敗"; 

	$mgrlang['gen_langerr2']				= "語言目錄或裝配檔案遺失"; 

	$mgrlang['gen_saving']					= "儲存"; 

	$mgrlang['gen_mylinks']					= "我的連接"; 

	$mgrlang['gen_editlinks']				= "編輯我的連接..."; 

	$mgrlang['gen_meswin']					= "留言視窗"; 

	$mgrlang['gen_t_none']					= "沒有"; 

	$mgrlang['gen_t_all']					= "所有"; 

	$mgrlang['gen_t_sub_caps']				= "訂購"; 

	$mgrlang['gen_t_exp_caps']				= "過期"; 

	$mgrlang['gen_t_dpdr_caps']				= "每天下載量/剩餘"; 

	$mgrlang['gen_t_show_all']				= "展示全部"; 

	$mgrlang['gen_t_show_sel']				= "展示選擇"; 

	$mgrlang['gen_t_ae_grps']				= "編輯群體"; 

	$mgrlang['gen_t_select']				= "選擇"; 

	$mgrlang['gen_t_actions']				= "行動"; 

	$mgrlang['gen_empty']					= "沒有專案顯示.在上面按一下添加新按鈕添加一個專案"; 

	$mgrlang['gen_empty_short']				= "沒有專案顯示."; 

	$mgrlang['gen_b_new']					= "創建新的"; 

	$mgrlang['gen_b_ed']					= "編輯"; 

	$mgrlang['gen_b_sa']					= "選擇全部"; 

	$mgrlang['gen_b_sn']					= "取消選取"; 

	$mgrlang['gen_b_remove_sel']			= "除去選擇"; 

	$mgrlang['gen_refresh']					= "恢復"; 

	$mgrlang['gen_b_grps']					= "群體"; 

	$mgrlang['gen_group']					= "群體"; 

	$mgrlang['gen_qty']						= "數量"; 

	$mgrlang['gen_b_panels']				= "小工具"; 

	$mgrlang['gen_b_dup']					= "重複"; 

	$mgrlang['gen_b_grps_alt']				= "群體"; 

	$mgrlang['gen_b_back']					= "回去"; 

	$mgrlang['gen_b_back_alt']				= "回到"; 

	$mgrlang['gen_b_del']					= "刪除"; 

	$mgrlang['gen_b_headers']				= "資料"; 

	$mgrlang['gen_b_filters']				= "過濾"; 

	$mgrlang['gen_b_memberships']			= "會員資格"; 

	$mgrlang['gen_b_sav']					= "儲存"; 

	$mgrlang['gen_b_submit']				= "提交"; 

	$mgrlang['gen_b_close']					= "關"; 

	$mgrlang['mes_to_mem']					= "給會員訊息"; 

	$mgrlang['gen_b_upload']				= "上傳"; 

	$mgrlang['gen_b_assign']				= "分配"; 

	$mgrlang['gen_b_unassign']				= "取消分配"; 

	$mgrlang['gen_b_try_again']				= "再嘗試"; 

	$mgrlang['gen_mes_delsuc']				= "成功刪除專案!"; 

	$mgrlang['gen_mes_dupsuc']				= "產品已成功複製!";

	$mgrlang['gen_mes_noitem']				= "沒有項目選擇刪除!";

	$mgrlang['gen_mes_noitem2']				= "沒有項目刪除因為你不能刪除預設的記錄!"; 

	$mgrlang['gen_mes_newsave']				= "您已經增加新項目!"; 

	$mgrlang['gen_mes_changesave']			= "您的更改已經儲存!"; 

	$mgrlang['gen_mes_sortclear']			= "所有排序已被清除!"; 

	$mgrlang['gen_pleaseenter']				= "請進入"; 

	$mgrlang['gen_pleaseselect']			= "請選擇"; 

	$mgrlang['gen_beforesave']				= "儲存之前"; 

	$mgrlang['gen_passgen']					= "密碼生產器";

	$mgrlang['gen_page']					= "頁面"; 

	$mgrlang['gen_of']						= "共有"; 

	$mgrlang['gen_items']					= "總項目"; 

	$mgrlang['gen_showing']					= "展示"; 

	$mgrlang['gen_group_mult']				= "群體重複專案"; 

	$mgrlang['gen_perpage']					= "每一頁"; 

	$mgrlang['gen_error_reported']			= "錯誤報告:"; 

	$mgrlang['gen_error_01']				= "以下的庫路徑不存在。請更新您的庫的路徑以 設定>軟體安裝:"; 

	$mgrlang['gen_error_02']				= "以下的庫路徑必須是可寫的使用照片庫存:";

	$mgrlang['gen_error_03']				= "這軟體沒有啟動系列號碼."; 

	$mgrlang['gen_error_04']				= "這軟體不能正確啟動和/或者有無效的關鍵.請聯絡<a href='http://www.ktools.net/' target='_blank'>Ktools.net</a>關於更多資料詳情.";

	$mgrlang['gen_error_05']				= "不能在資料庫選擇目錄.確定資料庫裝載正確.";

	$mgrlang['gen_error_06']				= "這資產目錄一定要有寫許可."; 

	$mgrlang['gen_error_07']				= "這裝置資料夾還存在.關於保障理由這將會刪除."; 

	$mgrlang['gen_error_07b']				= "這照片庫存.zip裝置檔案還存在.關於保障理由這將會刪除."; 

	$mgrlang['gen_error_08']				=  "這資產目錄沒有存在."; 

	$mgrlang['gen_error_09']				= "必須安裝PHP 4.2或更高版本."; 

	$mgrlang['gen_error_10']				= "GD陳列室 2 沒有安裝. "; 

	$mgrlang['gen_error_11']				= "該軟體遇到了一個嚴重的錯誤:". 

	$mgrlang['gen_error_12']				= "這圖庫編輯範圍需要改名()功能在PHP."; 

	$mgrlang['gen_error_13']				= "這圖庫編輯範圍需要mkdir()功能在PHP."; 

	$mgrlang['gen_error_14']				= "這圖庫編輯範圍需要rmdir()功能在PHP."; 

	$mgrlang['gen_error_15']				= "這個軟體需要真正方式()功能在PHP."; 

	$mgrlang['gen_error_16']				= "這個軟體需要開dir()功能在PHP."; 

	$mgrlang['gen_error_17']				= "這個軟體需要讀dir()功能在PHP."; 

	$mgrlang['gen_error_18']				= "這個進來目錄不存在;"; 

	$mgrlang['gen_error_19']				= "這個進來目錄沒有可以以文字表達的"; 

	$mgrlang['gen_error_20']				= "這個主機/ 服器沒有足夠的記憶體來創建這個照片的預覽。這個過程需要約"; 

	$mgrlang['gen_error_21']				= "這個軟體沒有經正式鑒定的服務."; 

	$mgrlang['gen_error_22']				= "您不被允許進入這個範圍."; 

	$mgrlang['gen_error_23']				= "伺服器記憶體不足以運作這照片. 請修改尺寸然後再上載";

	$mgrlang['gen_error_24']				= "發生錯誤"; 

	$mgrlang['gen_error_25']				= "檔案尺寸太大.最大尺寸"; 

	$mgrlang['gen_error_26']				= "未檢測到的Adobe Flash Player.使用此功能，必須安裝."; 

	$mgrlang['gen_error_27']				= "頭像目錄不存在或不可寫。這是解決之前，您將無法上傳或編輯頭像."; 

	$mgrlang['gen_error_28']				= "抱歉.您的伺服器上的PHP,無法載入視窗小部件，因為allow_url_fopen選項是關閉的."; 

	$mgrlang['gen_error_29']				= "已作出修訂，要求您更新您的圖庫媒體數量.";

	$mgrlang['gen_error_29b']				= "現在點擊這裡."; 

	$mgrlang['gen_error_29c']				= "您的圖片庫媒體數量已被更新。現在，您可以關閉此視窗."; 

	$mgrlang['gen_search_start']			= "開始鍵入搜尋."; 

	$mgrlang['gen_hidden']					= "隱藏示範方式"; 

	$mgrlang['gen_disabled']				= "示範模式下禁用此功能."; 

	$mgrlang['gen_suredelete']				= "您確定要刪除這個項目?"; 

	$mgrlang['gen_suredeact']				= "您確定要使這個項目無效?"; 

	$mgrlang['gen_mes_leaverow']			= "您必須離開至少一行."; 

	$mgrlang['gen_wb_everyone']				= "每一個";

	$mgrlang['gen_wb_limited']				= "有限的"; 

	$mgrlang['gen_wb_specific']				= "只有特別的會員群體,會員或會員資格";

	$mgrlang['gen_wb_members']				= "會員"; 

	$mgrlang['gen_mem_gals']				= "會員圖庫"; 

	$mgrlang['gen_wb_groups']				= "會員群體"; 

	$mgrlang['gen_wb_memberships']			= "會員籍"; 

	$mgrlang['gen_item_for']				= "這個項目可利用給"; 

	$mgrlang['gen_medianame_photos']		= "照片"; 

	$mgrlang['gen_medianame_videos']		= "影片"; 

	$mgrlang['gen_medianame_music']			= "音樂"; 

	$mgrlang['gen_medianame_files']			= "檔案"; 

	$mgrlang['gen_medianame_media']			= "媒體"; 

	$mgrlang['gen_media_caps']				= "媒體"; 

	$mgrlang['gen_dd_caps']					= "下載日期"; 

	$mgrlang['gen_ds_caps']					= "下載尺寸"; 

	$mgrlang['gen_dat_caps']				= "下載使用樣式"; 

	$mgrlang['gen_gmes_1']					= "選擇您想顯示在下面的清單中的群組."; 

	$mgrlang['gen_gmes_2']					= "目前，您有沒有創建的群組。按一下“編輯群組”按鈕來管理這些群組."; 

	$mgrlang['gen_gmes_3']					= "這必須至少兩個活躍專案使用整理特寫!"; 

	$mgrlang['gen_h_dd']					= "移動和點來重新排列訂購下列的專案"; 

	$mgrlang['gen_to']						= "致"; 

	$mgrlang['gen_from']					= "來自"; 

	$mgrlang['gen_tfg']						= "下一個群組"; 

	$mgrlang['gen_tfr']						= "下一個地區"; 

	$mgrlang['gen_clear_groups']			= "清除所有以前分配的群組"; 

	$mgrlang['gen_au_itg']					= "分配到群組"; 

	$mgrlang['gen_sort']					= "排序"; 

	$mgrlang['gen_actions']					= "操作"; 

	$mgrlang['gen_related_actions']			= "相關選項"; 

	$mgrlang['gen_width']					= "寬度"; 

	$mgrlang['gen_height']					= "高度"; 

	$mgrlang['gen_wb_noneavail']			= "無可用"; 

	$mgrlang['gen_block_ip']				= "IP 已封鎖"; 

	$mgrlang['gen_block_ip2']				= "若要解除IP封鎖"; 

	$mgrlang['gen_block_domain']			= "網域封鎖的"; 

	$mgrlang['gen_block_domain2']			= "解除封鎖這個網域去"; 

	$mgrlang['gen_block_email']				= "電子信箱地址封鎖"; 

	$mgrlang['gen_block_email2']			= "要解鎖這個電子信箱地址";

	$mgrlang['gen_active']					= "設置活動";

	$mgrlang['gen_inactive']				= "設置為無效";

	$mgrlang['gen_approved']				= "設置批准";

	$mgrlang['gen_unapproved']				= "設置未經批准的" ;

	$mgrlang['gen_selitems']				= "所選項目" ;

	$mgrlang['gen_allitems']				= "所有項目" ;

	$mgrlang['gen_example']					= "例子";

	$mgrlang['gen_or']						= "或者";

	$mgrlang['gen_chr_period']				= "期間";

	$mgrlang['gen_chr_comma']				= "逗號";

	$mgrlang['gen_chr_dash']				= "-";

	$mgrlang['gen_chr_equals']				= "等於";

	$mgrlang['gen_chr_slash']				= "/"; 

	$mgrlang['gen_chr_semicolon']			= "分號";;

	$mgrlang['gen_chr_colon']				= "冒號";

	$mgrlang['gen_chr_apostrophe']			= "撇號"; 

	$mgrlang['gen_copy']					= "複製"; 

	$mgrlang['gen_togroups']				= "分配/取消分配給群體"; 

	$mgrlang['gen_toregions']				= "分配/取消分配給各區域";   

	$mgrlang['gen_tostatus']				= "設置有效/無效"; 

	$mgrlang['gen_toapproved']				= "認可/未經批准的設定";

	$mgrlang['gen_toregions']				= "指定的地區";;

	$mgrlang['gen_files']					= "檔案";;

	$mgrlang['gen_total']					= "總額";;

	$mgrlang['gen_mb']						= "MB";

	$mgrlang['gen_kb']						= "KB";

	$mgrlang['gen_gb']						= "GB";

	$mgrlang['gen_ilg']						= "包括當地的圖庫"; 

	$mgrlang['gen_contrap']					= "提供者分配價格";       

	$mgrlang['gen_adminap']					= "使用系統管理的價格";  

	$mgrlang['gen_adminap2']				= "管理員分配價格"; 

	$mgrlang['gen_base_price']				= "原本"; 

	$mgrlang['gen_base_photoprice']			= "媒體價格";    

	$mgrlang['gen_base_photocredits']		= "媒體積分"; 

	$mgrlang['gen_base_credits']			= "原本"; 

	$mgrlang['gen_closed']					= "關閉"; 

	$mgrlang['gen_open']					= "開啟";

	$mgrlang['gen_pending']					= "未審核"; 

	$mgrlang['gen_processing']				= "處理"; 

	$mgrlang['gen_refunded']				= "退還"; 

	$mgrlang['gen_paid']					= "繳款";

	$mgrlang['gen_visitor']					= "訪客";

	$mgrlang['get_never']					= "從來沒有";

	$mgrlang['get_set_date']				= "設置日期";

	$mgrlang['get_direct_link']				= "直接連結"; 	

	$mgrlang['gen_active']					= "​​活躍"; 

	$mgrlang['gen_inactive']				= "暫無業務"; 

	$mgrlang['gen_active_d']				= "該專案積極。無效項目將不會出現在公開網站上."; 

	$mgrlang['gen_approved']				= "批准"; 

	$mgrlang['gen_failed']					= "失敗"; 

	$mgrlang['gen_cancelled']				= "取消";

	$mgrlang['gen_bill']					= "等下付款"; 

	$mgrlang['gen_incomplete']				= "不完整";        

	$mgrlang['gen_none']					= "無"; 

	$mgrlang['gen_unpaid']					= "未付款"; 

	$mgrlang['gen_shipped']					= "運送"; 

	$mgrlang['gen_pshipped']				= "部分運送"; 

	$mgrlang['gen_backordered']				= "缺貨"; 

	$mgrlang['gen_shipnone']				= "無"; 

	$mgrlang['gen_shipparent']				= "使用“訂單運輸狀態"; 

	$mgrlang['gen_notshipped']				= "沒有運輸"; 

	$mgrlang['gen_notap']					= "無"; 

	$mgrlang['gen_reset']					= "重設"; 

	$mgrlang['gen_remove']					= "解除"; 

	$mgrlang['gen_f_discount']				= "折扣價"; 

	$mgrlang['gen_f_discount_d']			= "價格為每個項目後的第一次。留為空白，以保持相同的價格作為第一項."; 

	$mgrlang['gen_f_discountc']				= "折扣積分";

	$mgrlang['gen_f_discountc_d']			= "每個項目後的第一個學分。留為空白，以保持相同學分的第一個項目."; 

	$mgrlang['gen_f_weight']				= "產品重量"; 

	$mgrlang['gen_f_weight_d']				= "此產品重量。如果需要的話，用於計算運費."; 

	$mgrlang['gen_f_lab_code']				= "實驗室代碼"; 

	$mgrlang['gen_f_lab_code_d']			= "一個項目的代碼列印實驗室可以用它來識別本產品."; 

	$mgrlang['gen_f_lab']					= "通知欄";

	$mgrlang['gen_f_lab_d']					= "如果任何訂單都放在這個專案的訂單電子郵件的副本發送到選定的實驗室."; 

	$mgrlang['gen_f_perm']					= "許可權"; 

	$mgrlang['gen_f_perm_d']				= "選擇會員，會員組及會員籍可以看到這個項目."; 

	$mgrlang['gen_f_itemcode']				= "產品編號"; 

	$mgrlang['gen_f_itemcode_d']			= "輸入一個可選的項目供內部使用的代碼."; 

	$mgrlang['gen_t_sortorder']				= "訂購"; 

	$mgrlang['gen_t_subject']				= "主題"; 

	$mgrlang['gen_t_id']					= "編號"; 

	$mgrlang['gen_t_lb_name']				= "我的最愛名稱"; 

	$mgrlang['gen_t_name']					= "姓名"; 

	$mgrlang['gen_t_active']				= "活躍"; 

	$mgrlang['gen_t_status']				= "狀態"; 

	$mgrlang['gen_t_submitted']				= "提交"; 

	$mgrlang['gen_t_member']				= "會員"; 

	$mgrlang['gen_member']					= "會員"; 

	$mgrlang['gen_t_media']					= "媒體"; 

	$mgrlang['gen_t_quantity']				= "數量"; 

	$mgrlang['gen_t_contributor']			= "提供者"; 

	$mgrlang['gen_t_commission']			= "傭金"; 

	$mgrlang['gen_t_downexp']				= "下載失效"; 

	$mgrlang['gen_t_ship_status']			= "送貨狀態";

	$mgrlang['gen_t_cost']					= "成本"; 

	$mgrlang['gen_t_type']					= "類型"; 

	$mgrlang['gen_t_item']					= "項目"; 

	$mgrlang['gen_t_updated']				= "更新"; 

	$mgrlang['gen_t_date']					= "日期"; 

	$mgrlang['gen_t_customer']				= "客戶"; 

	$mgrlang['gen_t_title']					= "標題"; 

	$mgrlang['gen_t_code']					= "代碼"; 

	$mgrlang['gen_t_remaining']				= "剩餘"; 

	$mgrlang['gen_t_region']				= "區域"; 

	$mgrlang['gen_t_gname']					= "群體名稱"; 

	$mgrlang['gen_t_homepage']				= "首頁"; 

	$mgrlang['gen_group_new_header']		= "添加新群體"; 

	$mgrlang['gen_group_edit_header']		= "編輯群體"; 

	$mgrlang['gen_group_new_message']		= "以面的表格，您可以添加一個新的群體.在管理範圍,群組會説明您組織日期."; 

	$mgrlang['gen_group_f_name']			= "群體名稱"; 

	$mgrlang['gen_group_f_name_d']			= "此組的名稱."; 

	$mgrlang['gen_group_f_flag']			= "圖示";

	$mgrlang['gen_group_f_flag_d']			= "這群組圖示來識別在管理領域."; 

	$mgrlang['gen_each']					= "ea.";

	$mgrlang['gen_b_exit_grps']				= "退出群組"; 

	$mgrlang['gen_b_exit_msps']				= "退出會員籍"; 

	$mgrlang['gen_groups']					= "群體"; 

	$mgrlang['gen_new_group']				= "添加新群體"; 

	$mgrlang['gen_edit_group']				= "編輯群體"; 

	$mgrlang['gen_group_list']				= "群體列表"; 

	$mgrlang['gen_empty_groups']			= "在這範圍裡沒有群體.按一下“添加新組”選項中添加一組."; 

	$mgrlang['gen_group_header']			= "在這群體中顯示專案"; 

	$mgrlang['gen_opgroup']					= "選項群組"; 

	$mgrlang['gen_option']					= "選項"; 

	$mgrlang['gen_empty_options']			= "此選項群組中沒有選項.點擊新建選項“選項卡中添加的選項."; 

	$mgrlang['gen_newoption']				= "新選項"; 

	$mgrlang['gen_editoption']				= "編輯選項"; 

	$mgrlang['gen_listoption']				= "選項清單"; 

	$mgrlang['gen_option_f_name']				= "選項名稱"; 

	$mgrlang['gen_option_f_name_d']				= "此選項的名稱"; 

	$mgrlang['gen_price']					= "價格"; 

	$mgrlang['gen_option_f_price_d']			= "從原來的物品售價增加或減少當你選擇這選項. 請留下空白若沒有修改";

	$mgrlang['gen_credits']					= "積分"; 

	$mgrlang['gen_option_f_credits_d']			= "選擇此選項時，積分成本從原來的專案貸款價格來增加或減少.留空不調整."; 

	$mgrlang['gen_icon']					= "圖示"; ;

	$mgrlang['gen_no_orders']				= "沒有訂單顯示"; 

	$mgrlang['gen_add_ship']				= "附加運送"; 

	$mgrlang['gen_add_ship_d']				= "您想添加任何額外的運費.這將增加出貨後計算，但區域的百分比和稅收前."; 

	

	$mgrlang['gen_option_type1']				= "拉下"; 

	$mgrlang['gen_option_type2']				= "核取方塊"; 

	$mgrlang['gen_option_type3']				= "選項按鈕"; 

	$mgrlang['gen_option_h_sort']				= "分類"; 

	$mgrlang['gen_option_h_name']				= "選項群組名稱"; 

	$mgrlang['gen_option_h_type']				= "類型"; 

	$mgrlang['gen_option_h_active']				= "活躍"; 

	$mgrlang['gen_option_h_required']			= "需要"; 

	$mgrlang['gen_tab_prod_shots']				= "產品照片"; 

	$mgrlang['gen_tab_pricing']				= "定價"; 

	$mgrlang['gen_tab_attach']				= "附屬";

	$mgrlang['gen_tab_options']				= "選項";

	$mgrlang['gen_tab_groups']				= "群體"; 

	$mgrlang['gen_tab_discounts']				= "折扣"; 

	$mgrlang['gen_tab_details']				= "細節"; 

	$mgrlang['gen_tab_options']				= "選項"; 

	$mgrlang['gen_tab_parent']				= "家長"; 

	$mgrlang['gen_tab_event_details']			= "活動詳情"; 

	$mgrlang['gen_tab_permissions']				= "許可權"; 

	$mgrlang['gen_tab_advanced']				= "進階"; 

	$mgrlang['gen_tab_shipping']				= "航運";

	$mgrlang['gen_tab_contributors']			= "提供者"; 

	$mgrlang['gen_tab_galleries']				= "圖庫"; 

	$mgrlang['gen_bill_item']				= "帳單項目"; 

	$mgrlang['gen_dig_ver']					= "數位版本"; 

	$mgrlang['gen_del_logo']				= "您確定您想刪除這個標誌?"; 

	$mgrlang['gen_expired']					= "期滿"; 

	$mgrlang['gen_not_active']				= "尚未活躍"; 

	$mgrlang['gen_publish']					= "發佈"; 

	$mgrlang['gen_featurepage']				= "此功能的頁面關閉根據 設置 > 外觀和感覺 > 頁."; 

	$mgrlang['gen_hpfeaturearea']				= "此功能的網頁區域關閉根據 設置 > 外觀和感覺 > 首頁."; 

	$mgrlang['gen_import']					= "輸入批次";  

	$mgrlang['gen_leave_blank_mes']				= "預設為空白"; 

	$mgrlang['leave_blank_quan']				= "預設為空白或多無限量的數量"; 

	$mgrlang['gen_unlimited']				= "無限"; 

	$mgrlang['gen_nfs']					= "非賣品"; 

	$mgrlang['gen_rm']					= "版權管理"; 

	$mgrlang['gen_free']					= "免費"; 

	$mgrlang['gen_cu']					= "聯絡我們"; 

	$mgrlang['quick_create_gal']				= "快速創建新的圖庫"; 

	$mgrlang['not_enough_mem']				= "未創建的縮略圖.您的伺服器沒有足夠的記憶體來創建該尺寸的​​圖像的縮略圖.您可以手動添加媒體庫>媒體中的縮略圖。如果你想你的縮略圖自動創建，請聯繫您的託管公司，並讓他們提高你的PHP的記憶體限制."; 

	$mgrlang['no_auto_thumbs']				= "該檔案類型不能自動創建縮略圖."; 

	$mgrlang['manual_add_thumb']				= "您可以手動添加這個檔媒體庫>媒體的縮略圖"; 

	$mgrlang['no_folder_import']				= "伺服器上的資料夾不存在.輸入過程失敗.";

	$mgrlang['folder_not_writable']				= "資料夾是不可寫.輸入過程失敗.";

	$mgrlang['error_moving_file']				= "失敗.無法移動檔至陳列室目錄.";

	$mgrlang['file_too_large']				= "檔案過大，無法移動到外部位置.跳過文件.";

	$mgrlang['error_move_ext']				= "無法移動檔案至儲存位置"; 

	$mgrlang['no_curl']					= "CURL擴展沒有被載入."; 

	$mgrlang['failed_as3_move']				= "無法傳輸檔案到AmazonS3."; 

	$mgrlang['failed_cf_move']				= "無法傳輸檔案CloudFiles。驗證失敗.";  

	$mgrlang['failed_icon_copy']				= "無法複製圖示庫目錄."; 

	$mgrlang['failed_thumb_copy']				= "無法複製縮略圖庫目錄."; 

	$mgrlang['failed_sample_copy']				= "無法複製樣本庫目錄."; 

	$mgrlang['gen_local']					= "本地";  

	$mgrlang['folder_exists']				= "此資料夾已經存在，在不同的存儲位置";   

	$mgrlang['folder_exists2']				= "請更改存儲位置，以符合或創建一個新的資料夾.";  

	$mgrlang['rmps']					= "版權管理價格計畫"; 

	$mgrlang['original_prof_set']				= "留為空白，使用原來的設定檔設置."; 

	$mgrlang['remove_customization']			= "刪除自訂"; 

	$mgrlang['quan_available']				= "現有數量"; 

	$mgrlang['no_preview']					= "預覽中不能創建."; 

	$mgrlang['select_files_import']				= "您可以繼續之前您必須選擇檔輸入.";  

	$mgrlang['gen_error_occ']				= "發生錯誤!"; 

	$mgrlang['starting_import']				= "開始輸入流程"; 

	$mgrlang['gen_stop']					= "停止"; 

	$mgrlang['add_more_to_batch']				= "增加更多的該批次"; 

	$mgrlang['import_new_batch']				= "輸入新批次"; 

	$mgrlang['files_uploaded']				= "您的檔案已被上傳.您可以將它們輸入到您的網站.";  

	$mgrlang['upload_new_media']				= "上載新媒體"; 

	$mgrlang['gen_continue']				= "繼續"; 

	$mgrlang['choose_save_loc']				= "選擇保存的檔案"; 

	$mgrlang['import_folder']				= "媒體輸入至以下資料夾中."; 

	$mgrlang['create_folder']				= "創建新資料夾"; 

	$mgrlang['enc_folder']					= "加密資料夾"; 

	$mgrlang['enc_folder_d']				= "加密資料夾的名稱和它包含的檔"; 

	$mgrlang['ext_store_limit']				= "他們必須將將檔存儲在以外的本機伺服器";

	$mgrlang['large_files_skipped']				= "更大的文件將被跳過"; 

	$mgrlang['local_lib']					= "本地庫";	

	$mgrlang['f_clips']					= "剪輯"; 

	$mgrlang['f_clips_d']					= "使用這些代碼在您的文字和他們各自的實際值將被替換為";	

	$mgrlang['f_content']					= "內容"; 

	$mgrlang['f_content_d']					= "此網頁上或在這些內容塊的內容將出現.";

	$mgrlang['gen_desc_caps']				= "說明"; 

	$mgrlang['gen_cost_caps']				= "成本";   

	$mgrlang['gen_order_num_caps']				= "訂單號碼";

	$mgrlang['gen_order_date_caps']				= "訂單日期"; 

	$mgrlang['gen_order_date']				= "訂單日期"; 

	$mgrlang['gen_in_bill_caps']				= "包括在帳單"; 

	$mgrlang['error']					= "錯誤"; 

	$mgrlang['gen_invoice_number']				= "發票號碼"; 

	$mgrlang['gen_due_date']				= "截止日期"; 

	$mgrlang['gen_due_date_caps']				= "截止日期";

	$mgrlang['gen_subtotal']				= "小計";

	$mgrlang['gen_total']					= "總數";

	$mgrlang['gen_total_caps']				= "總數"; 

	$mgrlang['gen_tax']					= "稅收"; 

	$mgrlang['gen_shipping']				= "航運"; 

	$mgrlang['gen_discounts']				= "折扣"; 

	$mgrlang['gen_cred_discounts']				= "積分折扣"; 

	$mgrlang['gen_cred_total']				= "總積分"; 

	$mgrlang['gen_payment_status']				= "付款狀態"; 

	$mgrlang['gen_payment_status_caps']			= "付款狀態"; 

	$mgrlang['please_choose_mem']				= "請選擇會員"; 

	$mgrlang['display_data']				= "顯示這個清單中的資料"; 

	$mgrlang['customer']					= "客戶"; 

	$mgrlang['gen_invoice_date']				= "發票日期";

	$mgrlang['gen_payment_date']				= "付款日期";

	$mgrlang['gen_pd_caps']					= "付款日期";

	$mgrlang['gen_shipping_status']				= "運送狀態";

	$mgrlang['gen_exit_search']				= "退出搜索"; 

	$mgrlang['gen_notes']					= "筆記"; 

	$mgrlang['gen_notes_d']					= "筆記只供內部使用，而不會公開顯示";  

	$mgrlang['gen_description']				= "說明";  

	$mgrlang['gen_description_d']				= "這個項目的說明"; 

	$mgrlang['gen_no_notes']				= "此產品沒有筆記"; 

	$mgrlang['max_prod_shots']				= "您已經達到了最大數量的產品照片，您可以附加到這個項目.";    	

	$mgrlang['tab_pricing']					= "定價"; 

	$mgrlang['tab_contents']				= "內容"; 

	$mgrlang['tab_prod_shots']				= "產品照片"; 

	$mgrlang['tab_details']					= "細節"; 

	$mgrlang['tab_groups']					= "群體"; 

	$mgrlang['tab_advanced']				= "進階"; 

	$mgrlang['tab_advertise']				= "廣告"; 

	$mgrlang['gen_quantity']				= "數量"; 

	$mgrlang['shipping_methods']				= "運送方法"; 

	$mgrlang['shipping_methods_d']				= "這一區域的送貨方式."; 

	$mgrlang['shipping_adj']				= "運費調整"; 

	$mgrlang['shipping_adj_d']				= "收費後運到該國的運輸總百分比。預設值是100％。超過100％，增加了成本，在100％減去成本";

	$mgrlang['gen_taxable']					= "應納稅"; 

	$mgrlang['gen_taxable_d']				= "該項目應課稅"; 

	$mgrlang['all_galleries']				= "所有陳列室"; 

	$mgrlang['percentage']					= "百分比"; 

	$mgrlang['dollar_value']				= "美元的價值"; 

	$mgrlang['min']						= "最小"; 

	$mgrlang['max']						= "最大"; 

	$mgrlang['attachment']					= "附件"; 

	$mgrlang['attachment_mwsg']				= "在選定的圖庫附加媒體"; 

	$mgrlang['galleries']					= "陳列室"; 

	$mgrlang['galleries_attach']				= "選擇畫廊有顯示此專案，然後選擇下面的附件類型";

	$mgrlang['body']					= "主題"; 

	$mgrlang['body_email_d']				= "此電子郵件的主體或內容."; 	

	$mgrlang['enc_name']					= "已加密名稱"; 	

	$mgrlang['gen_active_date']				= "活動日期"; 

	$mgrlang['gen_expire_date']				= "到期日期"; 

	$mgrlang['gen_owner']					= "所有者"; 

	$mgrlang['gen_more']					= "更多"; 

	$mgrlang['allow_contr']					= "允許提供者"; 

	$mgrlang['access']					= "訪問"; 

	$mgrlang['gen_now']					= "現在"; 

	$mgrlang['gen_never']					= "從來沒有"; 

	$mgrlang['max_icons']					= "您已經達到了最大數量的圖示，您可以附加到這個項目";  

	$mgrlang['exit_members']				= "退出會員"; 

	$mgrlang['changes_saved']				= "已儲存您的更改";  

	$mgrlang['gen_size']					= "尺寸";

	$mgrlang['gen_quality']					= "品質"; 

	$mgrlang['gen_watermark']				= "浮水印"; 

	$mgrlang['gen_top']					= "頂端"; 

	$mgrlang['gen_bottom']					= "底端"; 

	$mgrlang['gen_over']					= "以上"; 

	$mgrlang['gen_hidden']					= "隱藏"; 

	$mgrlang['gen_unknown']					= "未知"; 

	$mgrlang['gen_feature']					= "特點"; 

	$mgrlang['gen_unfeature']				= "非特點"; 	

	$mgrlang['gen_set_galicon']				= "設置圖庫圖示"; 

	$mgrlang['gen_unset_galicon']				= "未設置圖庫圖示"; 	

	$mgrlang['gen_gallery']					= "圖庫"; 	

	$mgrlang['add_media']					= "添加媒體"; 	

	$mgrlang['active_inactive']				= "活躍/不活躍"; 	

	$mgrlang['gen_featured']				= "精選"; 

	$mgrlang['set_gal_icon']				= "設置為圖庫圖示"; 

	$mgrlang['edit_details']				= "編輯詳細資訊"; 

	$mgrlang['not_available']				= "選項不再有效"; 

	$mgrlang['digital_collections']			= "數位收藏"; 

	$mgrlang['gen_code']					= "代碼"; 

	$mgrlang['gen_code_d']					= "到您的範本中顯示的代碼把這個內容塊.";

	$mgrlang['gen_content_name']			= "內容區域名稱"; 

	$mgrlang['gen_content_name_d']			= "這內容區域的名稱.在某些情況下，公眾網站，也可以用來作為標題。";  

	$mgrlang['gen_content_type']			= "內容類別型"; 

	$mgrlang['gen_content_type_d']			= "選擇如果這是一個阻止內容或一個完整的頁面"; 

	$mgrlang['gen_content_block']			= "內容阻止"; 

	$mgrlang['gen_content_page']			= "新頁面"; 

	$mgrlang['gen_contr_sell']				= "貢獻者銷售"; 

	$mgrlang['gen_contr_sell_d']			= "允許出售此項目的貢獻者"; 	

	$mgrlang['gen_com_type']				= "傭金類型"; 

	$mgrlang['gen_com_type_d']				= "選擇這個項目的百分比或金額的傭金";  

	$mgrlang['gen_com_value']				= "傭金值"; 

	$mgrlang['gen_com_value_d']				= "這個專案的傭金美元值"; 

	$mgrlang['gen_com_level']				= "傭金水準"; 

	$mgrlang['gen_com_value_d']				= "列印總的百分比傭金將支付其中的貢獻傭金水準";  	

	$mgrlang['gen_mmprice']					= "最低/最高價格"; 

	$mgrlang['gen_mmprice_d']				= "最低價格可以使這個項目的貢獻。請留空或輸入0表示沒有.";

	$mgrlang['gen_mmcredits']				= "最小/最大積分"; 

	$mgrlang['gen_mmcredits_d']				= "最低儲錢，投稿人可以做這個項目。請留空或輸入0表示沒有.";

	$mgrlang['attach_sel_gal']				= "附加到選定陳列室中"; 

	$mgrlang['attach_media_in_gal']			= "附加到媒體在選定的陳列室"; 

	$mgrlang['attach_both']					= "在這些陳列室附加到陳列室和媒體"; 

	$mgrlang['gen_orders']					= "訂單";

	$mgrlang['gen_dis_in_all']				= "顯示所有";

	$mgrlang['gen_days']					= "每天";

	$mgrlang['gen_weeks']					= "每週"; 

	$mgrlang['gen_months']					= "每月";  

	$mgrlang['gen_years']					= "每年"; 

	$mgrlang['gen_adv_hp']					= "刊登廣告的首頁"; 

	$mgrlang['gen_adv_hp_d']				= "廣告這資料在您的網站主頁上.";  

	$mgrlang['gen_adv_fp']					= "廣告在推薦頁";  

	$mgrlang['gen_originals']				= "原稿";  

	$mgrlang['gen_not_enough_mem']			= "沒有足夠的記憶體來為這個圖像創建一個圖示";  	

	$mgrlang['manually_approve']			= "手動批准";  

	$mgrlang['auto_approve']				= "自動批准";  

	$mgrlang['one_credit']					= "1 積分";  

	$mgrlang['mem_selector']				= "會員選擇";   

	$mgrlang['add_ship_mod']				= "增加運費模組";

	$mgrlang['add_sub']					= "新增訂閱";  

	$mgrlang['select_sub_mes']				= "選擇訂閱添加到該會員的帳戶，並點擊“保存更改.";    

	$mgrlang['clear_regions']				= "清除所有先前指定的區域";  

	$mgrlang['batch_upload']				= "批量上傳"; 

	$mgrlang['upload_photos']				= "上傳照片";  

	$mgrlang['gen_message']					= "信息";  

	$mgrlang['create_new_ticket']			= "創建新票證";

	$mgrlang['gen_items2']					= "項目";  

	$mgrlang['bill_date_caps']				= "帳單日期";  

	$mgrlang['int_notes']					= "內部筆錄";  

	$mgrlang['int_notes_d']					= "筆記只供內部使用，而不是向公眾展示";   	

	$mgrlang['edit_prof']					= "編輯個人資料";  

	$mgrlang['flag']					= "國旗";  

	

	$mgrlang['photographer_caps']			= "攝影師"; 

	$mgrlang['email_caps']					= "電子郵件";  

	$mgrlang['compname_caps']				= "公司名稱";  

	$mgrlang['website_caps']				= "網站";  

	$mgrlang['signup_caps']					= "註冊";  

	$mgrlang['lastlogin_caps']				= "最後登入";  

	$mgrlang['membership_caps']				= "會員籍";  

	$mgrlang['credits_caps']				= "積分";  

	$mgrlang['groups_caps']					= "群體";  

	$mgrlang['notes_caps']					= "筆記";  

	$mgrlang['address_caps']				= "地址";  

	$mgrlang['create_automat']				= "自動創建";  

	

	

	

	

	

	# WEBSITE SETTINGS

	$mgrlang['webset_tab1']					= "一般設置";  

	$mgrlang['webset_tab2']					= "商店/購物車"; 

	$mgrlang['webset_tab3']					= "聯繫方式";  

	$mgrlang['webset_tab4']					= "通知";  

	$mgrlang['webset_tab5']					= "搜索設置";  

	$mgrlang['webset_tab9']					= "會員";  

	$mgrlang['webset_tab7']					= "進階設定"; 

	$mgrlang['webset_tab8']					= "註冊";  

	$mgrlang['webset_tab10']				= "發票設置";  

	$mgrlang['webset_f_status']				= "網站狀態";  

	$mgrlang['webset_f_status_d']			= "打開或關閉您的網站.關閉時，您可以有一個資訊顯示給訪客.";  

	$mgrlang['webset_on']					= "打";

	$mgrlang['webset_off']					= "關閉";

	$mgrlang['webset_message']				= "信息"; 

	$mgrlang['webset_f_title']				= "網站標題"; 

	$mgrlang['webset_f_title_d']			= "您的網站的標題"; 

	$mgrlang['webset_f_meta_desc']			= "網站說明";  

	$mgrlang['webset_f_meta_desc_d']		= "為您的網站的說明meta標籤";  

	$mgrlang['webset_f_meta_keys']			= "網站關鍵字";  

	$mgrlang['webset_f_meta_keys_d']		= "關鍵字meta標籤在您的網站";  

	$mgrlang['webset_f_use_gpi']			= "使用媒體資訊的Meta標籤";  

	$mgrlang['webset_f_use_gpi_d']			= "更換網站畫廊/圖片/媒體資訊的圖庫或媒體詳細資訊頁面的meta標籤";   

	$mgrlang['webset_f_signup_req']			= "註冊要求";  

	$mgrlang['webset_f_signup_req_d']		= "從您的網站購買之前,要求客戶創建一個帳戶.";  

	$mgrlang['webset_h_rating_system']		= "評級系統";

	$mgrlang['webset_f_rating_system']		= "評級系統";

	$mgrlang['webset_f_rating_system_d']		= "打開照片評級系統，並允許成員評級";

	$mgrlang['webset_f_rating_system_lr']		= "訪客評價";  

	$mgrlang['webset_f_rating_system_lr_d']		= "允許訪客評級系統，並允許登入人士評級";

	$mgrlang['webset_h_comment_system']		= "評論系統";  

	$mgrlang['webset_f_comment_system']		= "評論系統";  

	$mgrlang['webset_f_comment_system_d']		= "打開的評論系統，並允許會員發表評論，照片和其他媒體的.";   

	$mgrlang['webset_f_comment_system_lr']		= "訪客評論";  

	$mgrlang['webset_f_comment_system_lr_d']	= "允許訪客對照片留言";

	$mgrlang['webset_h_tagging_system']		= "社區標記";  

	$mgrlang['webset_f_tagging_system']		= "社區標記";  

	$mgrlang['webset_f_tagging_system_d']		= "允許被添加到您的照片和其他媒體的標籤/關鍵字";    

	$mgrlang['webset_f_tagging_system_lr']		= "訪客標記"; 

	$mgrlang['webset_f_tagging_system_lr_d']	= "允許客人將標記添加到照片和其他媒體的帳戶";

	$mgrlang['webset_f_request_photo']		= "媒體要求";  

	$mgrlang['webset_f_request_photo_d']		= "允許訪客(未登入)要求您的照片或其他媒體";    

	$mgrlang['webset_f_email_friend']		= "轉寄給朋友";  

	$mgrlang['webset_f_email_friend_d']		= "允許訪客電子郵件中的連結檔案詳細資料頁面給朋友.";   

	$mgrlang['webset_f_news']				= "新聞頁面";  

	$mgrlang['webset_f_news_d']				= "在您的網站上打開新聞頁面.";  

	$mgrlang['webset_f_search']				= "搜尋";  

	$mgrlang['webset_f_search_d']			= "打開搜索功能，讓訪客來搜索照片及其他媒體.";    

	$mgrlang['webset_f_esearch']			= "活動搜索";  

	$mgrlang['webset_f_esearch_d']			= "顯示活動搜索區域"; 

	$mgrlang['webset_f_contact']			= "聯繫頁"; 

	$mgrlang['webset_f_contact_d']			= "打開聯繫頁面，並允許訪問者使用的聯繫方式與您取得聯繫";   	

	$mgrlang['webset_f_watch_lists']		= "觀察名單"; 

	$mgrlang['webset_f_watch_lists_d']		= "允許會員創建監視清單觀看新照片陳列室";  

	$mgrlang['webset_h_lightbox']			= "我的最愛"; 

	$mgrlang['webset_f_lightbox']			= "我的最愛"; 

	$mgrlang['webset_f_lightbox_d']			= "允許會員創建我的最愛"; 

	$mgrlang['webset_f_glightbox']			= "訪客我的最愛"; 

	$mgrlang['webset_f_glightbox_d']		= "允許訪客在沒有會員帳號下創建我的最愛"; 	

	$mgrlang['webset_f_cart']				= "購物車"; 

	$mgrlang['webset_f_cart_d']				= "購物車和結帳系統類型"; 

	$mgrlang['webset_f_credits']			= "基於積分的購物車";

	$mgrlang['webset_f_credits_d']			= "允許會員購買積分，將用於購買媒體和/或產品.";   

	$mgrlang['webset_f_delete_carts']		= "刪除被遺棄的購物車"; 

	$mgrlang['webset_f_delete_carts_d']		= "當你想有被遺棄的購物車從資料庫中刪除";

	$mgrlang['webset_f_download_expire']		= "下載過期"; 

	$mgrlang['webset_f_download_expire_d']		= "下載連結有效期在購買後這一段時間."; 

	$mgrlang['webset_f_dl_attempts']		= "嘗試下載"; 

	$mgrlang['webset_f_dl_attempts_d']		= "，使用者可以下載它們訂單的次數.";

	$mgrlang['webset_f_dl_unlimited']		= "無限"; 

	$mgrlang['webset_time_never']			= "從來沒有"; 

	$mgrlang['webset_time_1d']				= "一天之後"; 

	$mgrlang['webset_time_3d']				= "三天之後"; 

	$mgrlang['webset_time_5d']				= "五天之後"; 

	$mgrlang['webset_time_1w']				= "一周後"; 

	$mgrlang['webset_time_2w']				= "兩周後"; 

	$mgrlang['webset_time_1m']				= "一個月後";  

	$mgrlang['webset_time_2m']				= "兩個月後";  

	$mgrlang['webset_time_3m']				= "三個月後"; 

	$mgrlang['webset_time_6m']				= "六個月後"; 

	$mgrlang['webset_time_1y']				= "一年後"; 

	$mgrlang['webset_f_dle']				= "下載連結擴展"; 

	$mgrlang['webset_f_dle_d']				= "允許會員活躍的下載連結通過電子郵件驗證他們的購買."; 

	$mgrlang['webset_f_default_price']			= "預定價格";

	$mgrlang['webset_f_default_price_d']		= "這個價格如果沒有被指定為一個項目價格."; 

	$mgrlang['webset_f_default_credits']		= "預定積分"; 

	$mgrlang['webset_f_default_credits_d']		= "如果指定的儲值不計學分，使這一數額的儲值";

	$mgrlang['webset_f_auto_orders']		= "自動批准訂單"; 

	$mgrlang['webset_f_auto_orders_d']		= "批准訂單時，會自動支付.取消選擇手動批准訂單."; 

	$mgrlang['webset_f_min_total']			= "最小小計"; 

	$mgrlang['webset_f_min_total_d']		= "訂單小計，必須至少這個價格（未扣除任何稅款或運輸）之前有人可以結帳."; 

	$mgrlang['webset_f_support_email']		= "支援的電子郵件"; 

	$mgrlang['webset_f_support_email_d']		= "任何支援相關的電子郵件會去這個電子郵寄地址."; 

	$mgrlang['webset_f_sales_email']		= "銷售電子郵件"; 

	$mgrlang['webset_f_sales_email_d']		= "任何銷售相關的電子郵件會去這個電子郵寄地址."; 

	$mgrlang['webset_f_address']			= "營業地址";

	$mgrlang['webset_f_address_d']			= "適用於聯繫資訊，然後發送支票/匯票支付."; 

	$mgrlang['webset_f_business_country']		= "商業的國家"; 

	$mgrlang['webset_f_business_country_d']		= "生意的國家所在地.";  	

	$mgrlang['webset_f_business_name']		= "商業名稱"; 

	$mgrlang['webset_f_business_name_d']		= "商業名稱或多或網站."; 

	$mgrlang['webset_f_business_city']		= "商業城市"; 

	$mgrlang['webset_f_business_city_d']		= "城市所在地"; 

	$mgrlang['webset_f_business_state']		= "商業縣或多或州屬"; 

	$mgrlang['webset_f_business_state_d']		= "您的公司是總部設在的州或省";

	$mgrlang['webset_f_business_zip']		= "商業郵區"; 

	$mgrlang['webset_f_business_zip_d']		= "您的商業郵遞區號."; 	

	$mgrlang['webset_f_notify_sale']		= "新訂單"; 

	$mgrlang['webset_f_notify_sale_d']		= "當下訂單時,請通知我.這郵件會寄送到銷售電子信箱."; 

	$mgrlang['webset_f_notify_account']		= "新帳戶"; 

	$mgrlang['webset_f_notify_account_d']		= "當一個新的會員創建帳戶，通過電子郵件通知我.這些郵件會去支援電子郵件地址."; 

	$mgrlang['webset_f_notify_profile']		= "會員資料更新"; 

	$mgrlang['webset_f_notify_profile_d']		= "通過電子郵件通知我，當一個成員更改他們的個人資料.這些郵件會去支援電子郵件地址."; 

	$mgrlang['webset_f_notify_rating']		= "新評級"; 

	$mgrlang['webset_f_notify_rating_d']		= "當有人標價照片或其他媒體時，通過電子郵件通知我.這些郵件會去支援電子郵件地址."; 

	$mgrlang['webset_f_notify_comment']		= "新評論"; 

	$mgrlang['webset_f_notify_comment_d']		= "當有人在照片或其他媒體發表評論,通過電子郵件通知我.這些郵件會去支援電子郵件地址."; 

	$mgrlang['webset_f_notify_lightbox']		= "新我的最愛"; 

	$mgrlang['webset_f_notify_lightbox_d']		= "當有人創建新的我的最愛,通過電子郵件通知我.這些郵件會去支援電子郵件地址."; 

	$mgrlang['webset_f_grp_login']			= "登入群組"; 

	$mgrlang['webset_f_grp_login_d']		= "僅當他們是登入狀態,自動加入下列群組."; 

	$mgrlang['webset_f_grp_signup']			= "全球註冊用戶組"; 

	$mgrlang['webset_f_grp_signup_d']		= "當他們註冊一個帳戶時,無論他們選擇哪個會員計畫,會員自動加入下列群組."; 

	$mgrlang['webset_f_forum_link']			= "連結到論壇"; 

	$mgrlang['webset_f_forum_link_d']		= "連結到外部論壇或公告板."; 	

	$mgrlang['webset_f_member_avatars']		= "會員頭像"; 

	$mgrlang['webset_f_member_avatars_d']		= "允許會員上傳的頭像，代表自己的標誌或照片."; 	

	$mgrlang['webset_f_avatar_feature1']		= "頭像批准"; 

	$mgrlang['webset_f_avatar_feature1_d']		= "被公開展示前,讓您能夠查看和批准的頭像."; 

	$mgrlang['webset_f_avatar_feature2']		= "最大頭像尺寸"; 

	$mgrlang['webset_f_avatar_feature2_d']		= "可以上傳頭像的最大寬度或高度."; 

	$mgrlang['webset_f_avatar_feature3']		= "頭像檔案類型"; 

	$mgrlang['webset_f_avatar_feature3_d']		= "會員可以上傳的頭像檔案類型."; 

	$mgrlang['webset_f_avatar_feature4']		= "最大頭像檔案尺寸"; 

	$mgrlang['webset_f_avatar_feature4_d']		= "可以由會員上傳的最大頭像檔案尺寸."; 		

	$mgrlang['webset_h_contr']			= "提供者"; 

	$mgrlang['webset_f_contr_portfolios']		= "提供者簡介"; 

	$mgrlang['webset_f_contr_portfolios_d']		= "允許貢獻者組合，他們的最佳作品，並添加自己的簡介";

	$mgrlang['webset_f_contr_showcase']		= "展示者";

	$mgrlang['webset_f_contr_showcase_d']		= "已被設置為展示他們的會員帳戶中展示的貢獻者.";	

	$mgrlang['webset_f_contr_showcase1']		= "多少"; 

	$mgrlang['webset_f_contr_showcase1_d']		= "你想展示多少貢獻數量";

	$mgrlang['webset_f_contr_showcase3']		= "選擇提供者"; 

	$mgrlang['webset_f_contr_showcase3_d']		= "貢獻者應當如何可以選擇將展示";

	$mgrlang['webset_f_contr_showcase3op1']		= "隨機 (任何提供者)"; 

	$mgrlang['webset_f_contr_showcase3op2']		= "隨機(只有那些媒體銷售)"; 

	$mgrlang['webset_f_contr_showcase3op3']		= "隨機 (只有那些擁有資料)";

	$mgrlang['webset_f_contr_showcase3op4']		= "隨機 (只有那些擁有資料者與媒體者)";

	$mgrlang['webset_f_contr_showcase3op5']		= "我將手動選擇"; 	

	$mgrlang['webset_f_contr_metatags']		= "在META標籤中使用會員資訊"; 

	$mgrlang['webset_f_contr_metatags_d']		= "使用會員資訊,會員的個人資料頁供meta標籤."; 

	$mgrlang['webset_f_contr_cd']			= "貢獻者 CD/DVD";

	$mgrlang['webset_f_contr_cd_d']			= "允許貢獻者給客戶發送照片和其他媒體上的CD / DVD，而不是即時下載";

	$mgrlang['webset_f_contr_samples']		= "僅允許提供者樣本"; 

	$mgrlang['webset_f_contr_samples_d']		= "允許貢獻者只上傳自己的媒體樣本大小，並在購買時提供原件";

	$mgrlang['webset_f_contr_cd2']			= "提交CD / DVD"; 

	$mgrlang['webset_f_contr_cd2_d']		= "允許貢獻者CD / DVD，而不是把他們的照片和媒體上傳到網站";

	$mgrlang['webset_f_contr_cd2_mes']		= "提交CD / DVD信息"; 

	$mgrlang['webset_f_contr_cd2_mes_d']		= "要顯示的訊息發送CD / DVD媒體上的貢獻";

	$mgrlang['webset_f_com_calc']			= "提供者委員會"; 

	$mgrlang['webset_f_com_calc_d']			= "提供者委員會是根據每個銷售的總數量算出利潤";

	$mgrlang['webset_com_calc_op1']			= "利潤"; 

	$mgrlang['webset_com_calc_op2']			= "總數"; 

	$mgrlang['webset_f_notify_contrup']		= "投稿上傳"; 

	$mgrlang['webset_f_notify_contrup_d']		= "當一個會員上傳新媒體時,通過電子郵件通知我.這些郵件會去支援電子郵寄地址."; 

	$mgrlang['webset_f_poe']				= "列印實驗室聯繫"; 

	$mgrlang['webset_f_poe_d']				= "列印實驗室的姓名和電子郵寄地址.讓您的某些列印/發送訂單電子郵件，列印實驗室或自動聯絡人."; 

	$mgrlang['webset_b_lab']				= "添加實驗室名稱"; 

	$mgrlang['webset_lab_name']				= "姓名";

	$mgrlang['webset_lab_email']			= "電子郵件"; 

	$mgrlang['webset_f_invoice_prefix']		= "發票號碼首碼"; 

	$mgrlang['webset_f_invoice_prefix_d']		= "代碼放在發票號碼前面."; 

	$mgrlang['webset_f_invoice_next']		= "下一個發票號碼"; 

	$mgrlang['webset_f_invoice_next_d']		= "發票/訂單會採取一個序號."; 

	$mgrlang['webset_f_invoice_suffix']		= "發票編號尾碼"; 

	$mgrlang['webset_f_invoice_suffix_d']		= "代碼放在發票號碼後面."; 

	$mgrlang['webset_f_invoice_preview']		= "發票號碼預覽"; 

	$mgrlang['webset_mes_invoice_warn']		= "您的下一個發票號碼可能是低於先前給出的發票號碼."; 

	$mgrlang['webset_mes_cart_warn']		= "如果購物車的啟用，您必須選擇至少1個購物車類型."; 

	$mgrlang['webset_f_compay']				= "傭金支付"; 

	$mgrlang['webset_f_compay_d']			= "可供會員選擇的接收任何類型的支付傭金."; 

	$mgrlang['webset_f_compay_op1']			= "PayPal";

	$mgrlang['webset_f_compay_op2']			= "支票/匯票"; 

	$mgrlang['webset_f_compay_op3']			= "其他"; 

	$mgrlang['webset_f_compay_op3b']		= "標籤"; 

	$mgrlang['webset_f_contr_dvp']			= "原始版本定價"; 

	$mgrlang['webset_f_contr_dvp_d']		= "誰設定的價格在原有的數字版本貢獻者上傳";

	$mgrlang['webset_f_contr_col']			= "採集價格"; 

	$mgrlang['webset_f_contr_col_d']		= "設置價格的貢獻者創建的集合";

	$mgrlang['webset_rss_news']				= "新 RSS"; 

	$mgrlang['webset_rss_news_d']			= "允許訪問者訂閱RSS提要的網站新聞."; 

	$mgrlang['webset_rss_galleries']		= "圖庫 RSS"; 

	$mgrlang['webset_rss_galleries_d']		= "允許訪問者訂閱RSS提要陳列室."; 

	$mgrlang['webset_rss_fmedia']			= "多功能的多媒體RSS"; 

	$mgrlang['webset_rss_fmedia_d']			= "允許訪問者訂閱RSS提要的功能媒體."; 

	$mgrlang['webset_rss_search']			= "搜尋 RSS"; 

	$mgrlang['webset_rss_search_d']			= "允許訪問者訂閱RSS提要的搜索."; 

	$mgrlang['webset_rss_newest']			= "新項目RSS"; 

	$mgrlang['webset_rss_newest_d']			= "允許訪問者訂閱RSS提要的最新照片/檔案."; 

	$mgrlang['webset_rss_popular']			= "熱門產品RSS"; 

	$mgrlang['webset_rss_popular_d']		= "允許訪問者訂閱RSS提要最流行的照片/檔案."; 

	$mgrlang['webset_rss_records']			= "記錄"; 

	$mgrlang['webset_rss_records_d']		= "記錄的數目包括每個RSS提要."; 

	$mgrlang['webset_ticket_sys']			= "支援系統"; 

	$mgrlang['webset_f_enable_ts']			= "支援系統"; 

	$mgrlang['webset_f_enable_ts_d']		= "打開您網站的會員的支援票系統";

	$mgrlang['webset_f_coc']				= "傭金的積分"; 

	$mgrlang['webset_f_coc_d']				= "貢獻者傭金每學分，如果那是由客戶使用信用支付";

	$mgrlang['webset_pur_wcred']			= "允許用積分購買"; 	

	$mgrlang['webset_f_ont']				= "訂單號碼類型";

	$mgrlang['webset_f_ont_d']				= "選擇順序編號或隨機的順序編號"; 	

	$mgrlang['webset_use_seq']				= "使用順序編號"; 

	$mgrlang['webset_use_rand']				= "使用隨機的順序編號"; 

	$mgrlang['webset_f_non']				= "下一個訂單號碼"; 

	$mgrlang['webset_f_non_d']				= "在下訂單的數量";	

	$mgrlang['webset_f_puragree']			= "購買協議"; 

	$mgrlang['webset_f_puragree_d']			= "要求客戶同意購買協定前檢查."; 	

	$mgrlang['webset_f_credsys']			= "積分系統";	

	$mgrlang['webset_f_skipship']			= "跳過運費"; 

	$mgrlang['webset_f_skipship_d']			= "跳過任何運費支付專案與積分."; 	

	$mgrlang['webset_f_mships']				= "會員籍"; 

	$mgrlang['webset_f_mships_d']			= "顯示可用的會員資格創建帳戶形式."; 	

	$mgrlang['webset_h_info']				= "信息"; 

	$mgrlang['webset_h_disabled']			= "免填"; 

	$mgrlang['webset_h_request']			= "選填"; 

	$mgrlang['webset_h_require']			= "必填"; 

	$mgrlang['webset_h_info']				= "信息"; 

	$mgrlang['webset_signup_argee']			= "註冊協議"; 	

	$mgrlang['webset_cur_based']			= "以貨幣"; 

	$mgrlang['webset_cred_based']			= "積分制式";

	$mgrlang['webset_both']					= "二者"; 

	$mgrlang['webset_enabled']				= "啟用"; 

	$mgrlang['webset_enable_subs']			= "啟用訂閱"; 

	$mgrlang['webset_f_new_tags']			= "新標記"; 

	$mgrlang['webset_f_new_tags_d']			= "有人張貼一個新的標籤時,通過電子郵件通知我.這些郵件會去支援電子郵寄地址."; 	

	$mgrlang['webset_f_fileds']				= "欄位"; 

	$mgrlang['webset_f_fileds_d']			= "允許訪問者選擇要搜索的領域，縮小他們的搜索."; 	

	$mgrlang['webset_f_types']				= "類型"; 

	$mgrlang['webset_f_types_d']			= "讓訪客來縮小他們的搜索的媒體類型."; 	

	$mgrlang['webset_f_orien']				= "方向"; 

	$mgrlang['webset_f_orien_d']			= "允許訪客來縮小他們的搜索方向."; 	

	$mgrlang['webset_f_color']				= "顏色"; 

	$mgrlang['webset_f_color_d']			= "允許訪客縮小他們的搜索顏色."; 	

	$mgrlang['webset_f_dates']				= "日期"; 

	$mgrlang['webset_f_dates_d']			= "允許訪客縮小他們的搜索，按日期範圍."; 	

	$mgrlang['webset_f_lictype']			= "授權類型"; 

	$mgrlang['webset_f_lictype_d']			= "允許訪客以縮小搜索範圍的授權類型."; 	

	$mgrlang['webset_f_galleries']			= "陳列室"; 

	$mgrlang['webset_f_galleries_d']		= "允許訪問者在搜尋網頁面上縮小他們的搜索陳列室."; 	

	$mgrlang['webset_f_regfield']			= "註冊領域"; 

	$mgrlang['webset_f_regfield_d']			= "你喜歡什麼樣的資料將捕捉到一個使用者登錄時?";

	

	

	# MEDIA

	$mgrlang['media_f_grp_d']				= "將媒體添加到以下群組."; 

	$mgrlang['media_f_keeporg']				= "保持原稿"; 

	$mgrlang['media_f_keeporg_d']			= "選擇保留或放棄原件.購買時,如果你不保持原件您將必須將它們上傳."; 

	$mgrlang['media_det_for_batch']			= "分批詳細內容"; 

	$mgrlang['media_f_sn']					= "排序號碼";  

	$mgrlang['media_f_sn_d']				= "手動排序號碼分配."; 

	$mgrlang['media_f_mr']					= "型號"; 

	$mgrlang['media_f_mr_d']				= "這個檔有一個模型形式發佈"; 

	$mgrlang['media_f_copyright']			= "版權所有"; 

	$mgrlang['media_f_copyright_d']			= "此檔案的版權資訊."; 	

	$mgrlang['media_f_usageres']			= "使用限制"; 

	$mgrlang['media_f_usageres_d']			= "在這個檔案的​​任何使用限制."; 

	$mgrlang['media_f_groups']				= "群組"; 

	$mgrlang['media_f_dc_d']				= "最新的照片，影片或檔案創建."; 

	$mgrlang['media_f_da_d']				= "最新的照片，影片或檔案添加到陳列室中."; 

	$mgrlang['media_f_mt_d']				= "選擇適合這些媒體檔案的媒體類型."; 

	$mgrlang['media_save_first']			= "保存第一次看到的檔案名稱和路徑."; 

	$mgrlang['media_mem_media']				= "會員的媒體"; 

	$mgrlang['media_exit_mem']				= "退出會員"; 	

	$mgrlang['media_f_title']				= "標題"; 

	$mgrlang['media_f_title_d']				= "此媒體檔案的標題."; 	

	$mgrlang['media_f_titleimp_d']			= "分配一個名稱您想要添加到您要導入的所有媒體在這個批次."; 		

	$mgrlang['media_f_description']			= "說明"; 

	$mgrlang['media_f_description_d']		= "此媒體檔案的說明."; 

	$mgrlang['media_f_descriptionimp_d']		= "分配的描述您想要添加到您要導入的所有媒體在這個批次."; 

	$mgrlang['media_f_keywords']			= "關鍵字"; 

	$mgrlang['media_f_keywords_d']			= "關鍵字分配給該媒體."; 

	$mgrlang['media_f_keywordsimp_d']		= "指定關鍵字，您想添加到您要導入的所有媒體在這個批次."; 

	$mgrlang['media_f_galls']				= "陳列室/活動"; 

	$mgrlang['media_f_galls_d']				= "分配圖庫或事件，媒體將顯示在這個批次."; 

	$mgrlang['media_f_format']				= "格式/檔案類型."; 

	$mgrlang['media_f_format_d']			= "這個檔案的​​格式或檔案類型."; 

	$mgrlang['media_f_width']				= "寬度"; 

	$mgrlang['media_f_width_d']				= "這個檔案的​​寬度."; 

	$mgrlang['media_f_height']				= "高度"; 

	$mgrlang['media_f_height_d']			= "這個檔案的​​高度."; 

	$mgrlang['media_f_hd']					= "高清"; 

	$mgrlang['media_f_hd_d']				= "檢查如果這是一個高清影片."; 

	$mgrlang['media_f_fps']					= "FPS"; 

	$mgrlang['media_f_fps_d']				= "該影片幀每秒."; 

	$mgrlang['media_f_runtime']				= "執行時間/長度"; 

	$mgrlang['media_f_runtime_d']			= "此影片的長度."; 	

	$mgrlang['media_f_orgcopy']				= "正本"; 

	$mgrlang['media_f_orgcopy_d']			= "選擇如何應列出這些檔案的原始版本."; 

	$mgrlang['media_f_hidden']				= "隱藏（不提供下載）"; 

	$mgrlang['media_f_rmps']				= "版權管理價格計畫"; 

	$mgrlang['media_f_price']				= "價格"; 

	$mgrlang['media_f_price_d']				= "這個項目的原始版本的價格."; 

	$mgrlang['media_f_credits']				= "積分"; 

	$mgrlang['media_f_credits_d']			= "這個項目的原始版本積分數額."; 

	$mgrlang['media_click_upbut']			= "點擊上傳按鈕，選擇要附加的檔案"; 	

	$mgrlang['media_f_digsizes']			= "數位尺寸"; 

	$mgrlang['media_f_digsizes_d']			= "額外的變化，你想賣這個項目。您可以自訂這些設定檔，添加特定的此項目的定價和許可";

	$mgrlang['media_no_mem']				= "'自動化創建' 無法給予因為記憶體不足的原因! 你的主機是你的記憶體使用限制使用 PHP memory_limit 功能. 請與他們聯繫以放寬限制";

	$mgrlang['media_f_print_d']				= "列出這些版畫與媒體在此批銷售.所有組將隨時添加任何列印";

	$mgrlang['media_f_prod_d']				= "列出這些產品的銷售，在此批媒體";

	$mgrlang['media_f_pack_d']				= "選擇的配套，將顯示此批中的照片.";  

	$mgrlang['media_size_limit']			= "其中一些引入檔案已超過規定限制及無法以存儲.檔案限制是.";    

	$mgrlang['media_preimport']				= "您可以在引入檔案前增加相關資料與價錢,然後在準備就序按以下引入按鍵."; 

	$mgrlang['media_customize']				= "定制"; 

	$mgrlang['media_customized']			= "定制"; 

	$mgrlang['media_fileattached']			= "檔案附加"; 

	$mgrlang['media_pd_customized']			= "定價或詳細資料已定制"; 

	$mgrlang['media_f_coll']				= "添加此媒體到以下收集.只有從個人媒體“的類型”創建收集將在這裡列出.";  

	

	# LOOK & FEEL	

	$mgrlang['landf_f_detpage']				= "詳細資訊頁"; 

	$mgrlang['landf_f_detpage_d']			= "顯示以下專案的詳情頁面.";  	

	$mgrlang['landf_f_crop_thumb']			= "裁剪縮略圖"; 

	$mgrlang['landf_f_crop_thumb_d']		= "裁剪的縮略圖，使橫向和縱向的的圖像顯示在相同大小.";  

	$mgrlang['landf_f_crop_ro']				= "裁剪翻轉"; 

	$mgrlang['landf_f_crop_ro_d']			= "裁剪翻轉,橫向和縱向的的圖像顯示在相同大小."; 	

	$mgrlang['landf_f_vid_skin']			= "影片控制範本"; 

	$mgrlang['landf_f_vid_skin_d']			= "範本使用的影片播放機控制."; 	

	$mgrlang['landf_f_vid_ap']				= "自動播放影片"; 

	$mgrlang['landf_f_vid_ap_d']			= "自動開始播放媒體上的詳細資訊頁面的範例影片.在iOS，Android或移動瀏覽器可能無法正常運作."; 	

	$mgrlang['landf_f_vid_loop']			= "重複影片"; 

	$mgrlang['landf_f_vid_loop_d']			= "結束時，自動重複的影片."; 	

	$mgrlang['landf_f_vid_bgcolor']			= "影片播放機的背景顏色"; 

	$mgrlang['landf_f_vid_bgcolor_d']		= "影片播放機的影片還沒有被載入時的背景顏色. 無法用於 iOS, Android 或移動瀏覽器";	

	$mgrlang['landf_f_vid_rosize']			= "影片返回大小"; 

	$mgrlang['landf_f_vid_rosize_d']		= "在返回播放的影片的大小."; 		

	$mgrlang['landf_f_vid_prsize']			= "影片預覽大小"; 

	$mgrlang['landf_f_vid_prsize_d']		= "在媒體上詳細資訊頁面的預覽影片的大小."; 	

	$mgrlang['landf_f_vid_autors']			= "自動調整"; 

	$mgrlang['landf_f_vid_autors_d']		= "在影片開始播放時，自動調整大小相符的影片播放機.在iOS，Android或移動瀏覽器能無法正常運作.";   

	$mgrlang['landf_f_vid_cont']			= "影片控制"; 

	$mgrlang['landf_f_vid_cont_d']			= "媒體的詳細資訊頁面上的控制項位置的影片."; 

	$mgrlang['landf_f_vid_prwm']			= "預覽影片浮水印"; 

	$mgrlang['landf_f_vid_prwm_d']			= "在媒體詳細資訊頁面上要使用的浮水印範例影片."; 

	$mgrlang['landf_f_vid_rowm']			= "翻轉影片浮水印"; 

	$mgrlang['landf_f_vid_rowm_d']			= "要使用的浮水印範例影片在翻動快顯視窗.";

	$mgrlang['landf_f_vid_wmpos']			= "浮水印的位置"; 

	$mgrlang['landf_f_vid_wmpos_d']			= "範本使用的影片播放機控制."; 

	$mgrlang['landf_f_vid_wmpos1']			= "左下方"; 

	$mgrlang['landf_f_vid_wmpos2']			= "右下方"; 

	$mgrlang['landf_f_vid_wmpos3']			= "左上方";  

	$mgrlang['landf_f_vid_wmpos4']			= "右上方"; 

	$mgrlang['landf_crop_height']			= "裁剪高度"; 

	$mgrlang['landf_preimg_01']				= "花"; 

	$mgrlang['landf_preimg_02']				= "實心藍"; 

	$mgrlang['landf_preimg_03']				= "海龜照片"; 

	$mgrlang['landf_preimg_04']				= "花"; 

	$mgrlang['landf_preimg_05']				= "剪掉";

	$mgrlang['landf_preimg_06']				= "泳池和球";

	$mgrlang['landf_preimg_07']				= "狗";

	$mgrlang['landf_preimg_08']				= "分岔";

	$mgrlang['landf_preimg_09']				= "蜜蜂 & 花瓣";

	$mgrlang['landf_olderver']				= "表示這次的主題是較舊版本的軟體，並可能與此版本不相容.";   

	$mgrlang['mediadet_mem_fname']			= "會員名字"; 

	$mgrlang['mediadet_mem_email']			= "會員電子郵件"; 

	$mgrlang['mediadet_mem_id']				= "會員編號"; 

	$mgrlang['mediadet_mem_lname']			= "會員姓名"; 

	$mgrlang['mediadet_id']					= "編號"; 

	$mgrlang['mediadet_title']				= "標題"; 

	$mgrlang['mediadet_filename']			= "檔案名稱"; 

	$mgrlang['mediadet_owner']				= "擁有者"; 

	$mgrlang['mediadet_priceorg']			= "價格(原創)"; 

	$mgrlang['mediadet_creditsorg']			= "積分(原創)"; 

	$mgrlang['mediadet_dateadded']			= "上傳日期"; 

	$mgrlang['mediadet_datecreated']		= "製作日期"; 

	$mgrlang['mediadet_description']		= "說明"; 

	$mgrlang['mediadet_foldfile']			= "檔/檔案名稱"; 

	$mgrlang['mediadet_views']				= "瀏覽次數"; 

	$mgrlang['mediadet_keywords']			= "關鍵字"; 

	$mgrlang['mediadet_colors']				= "顏色"; 

	$mgrlang['mediadet_copyright']			= "版權"; 

	$mgrlang['mediadet_usage_rest']			= "使用限制"; 

	$mgrlang['mediadet_mod_release']		= "發佈的模型"; 

	$mgrlang['mediadet_res']				= "解析度（原創）"; 

	$mgrlang['mediadet_filesize']			= "檔案大小(原創)"; 

	$mgrlang['mediadet_rating']				= "評級"; 

	$mgrlang['mediadet_add_lb']				= "添加到我的最愛"; 

	$mgrlang['mediadet_add_pack']			= "添加到配套"; 

	$mgrlang['mediadet_email']				= "電子郵件"; 

	$mgrlang['mediadet_rating']				= "評級"; 	

	$mgrlang['landf_f_aupage']				= "啟用關於我們頁面"; 

	$mgrlang['landf_f_fppage']				= "啟用特色促銷頁面"; 

	$mgrlang['landf_f_fppage_d']			= "顯示在促銷頁面其中列出了優惠券及促銷.";  	

	$mgrlang['landf_f_nmpage']				= "啟用最新的媒體頁面"; 

	$mgrlang['landf_f_nmpage_d']			= "顯示最新的媒體區，將顯示最新的媒體已添加.";  	

	$mgrlang['landf_f_fmpage']				= "啟用多功能的多媒體頁面"; 

	$mgrlang['landf_f_fmpage_d']			= "顯示功能的媒體網站上的網頁."; 		

	$mgrlang['landf_f_pmpage']				= "啟用熱門媒體"; 

	$mgrlang['landf_f_pmpage_d']			= "顯示流行的媒體領域，這將顯示在您的網站上最流行的媒體檔案."; 		

	$mgrlang['landf_f_fprintspage']			= "啟用特色列印頁面"; 

	$mgrlang['landf_f_fprintspage_d']		= "顯示特色列印的頁面，其中會顯示任何標記為特色的列印."; 	

	$mgrlang['landf_f_fprodspage']			= "啟用精選產品頁面"; 

	$mgrlang['landf_f_fprodspage_d']		= "顯示特色產品頁會顯示任何標記為特色的產品."; 	

	$mgrlang['landf_f_fpackspage']			= "啟用精選配套頁面"; 

	$mgrlang['landf_f_fpackspage_d']		= "顯示“特色配套”頁面這將顯示任何標記為特色的配套.";  

	$mgrlang['landf_f_fcollspage']			= "啟用特色收藏頁面"; 

	$mgrlang['landf_f_fcollspage_d']		= "顯示“特色收藏”頁面這將顯示任何標記為特色的收藏."; 	

	$mgrlang['landf_f_fsubspage']			= "啟用特色訂閱頁面."; 

	$mgrlang['landf_f_fsubspage_d']			= "顯示“特色訂閱”頁面這將顯示任何標記為特色的訂閱.";  	

	$mgrlang['landf_f_fcredpage']			= "啟用特色積分頁面"; 

	$mgrlang['landf_f_fcredpage_d']			= "顯示“特色積分”頁面這將顯示任何標記為特色的積分.";   

	$mgrlang['landf_f_medcount']			= "媒體計數"; 

	$mgrlang['landf_f_medcount_d']			= "顯示每家畫廊的媒體檔案數量。"; 	

	$mgrlang['landf_f_stats']				= "顯示統計資料"; 

	$mgrlang['landf_f_stats_d']				= "公眾網站上顯示網站統計資料."; 	

	$mgrlang['landf_f_memonline']			= "線上會員"; 

	$mgrlang['landf_f_memonline_d']			= "在公眾網站顯示會員線上."; 

	$mgrlang['landf_f_relatedmed']			= "相關媒體"; 

	$mgrlang['landf_f_relatedmed_d']		= "顯示相關的媒體區當查看詳細資訊頁面時.";   	

	$mgrlang['landf_f_iptc']				= "國際報業電信委員會"; 

	$mgrlang['landf_f_iptc_d']				= "顯示國際報業電信委員會資訊在媒體上詳細資訊頁面（如果存在）.";   

	$mgrlang['landf_f_exif']				= "EXIF";

	$mgrlang['landf_f_exif_d']				= "顯示EXIF資訊媒體上的詳細資訊頁面，如果存在."; 	

	$mgrlang['landf_f_socnet']				= "社交網路代碼"; 

	$mgrlang['landf_f_socnet_d']			= "複製和貼上任何社交HTML/ Javascript代碼在這裡.";  

	$mgrlang['landf_f_logo']				= "標誌"; 

	$mgrlang['landf_f_logo_d']				= "在您的網站上上載一個標誌使用,電子郵件和發票.PNG，GIF或JPEG而已."; 

	$mgrlang['landf_f_newsarea']			= "啟用新聞專區"; 

	$mgrlang['landf_f_featuremed']			= "啟用功能的媒體區"; 

	$mgrlang['landf_f_newmed']				= "啟用新媒體區"; 

	$mgrlang['landf_f_newmed_d']			= "在主頁上的新媒體顯示一個小的選擇."; 

	$mgrlang['landf_f_newmed2']				= "有多少新的媒體專案應顯示的主頁上."; 	

	$mgrlang['gen_how_many']				= "多少"; 	

	$mgrlang['landf_f_popmed']				= "啟用大眾媒體區"; 

	$mgrlang['landf_f_popmed_d']			= "在網頁上流行的媒體顯示一個小的選擇."; 

	$mgrlang['landf_f_popmed2']				= "有多少流行的媒體專案應顯示的主頁上."; 

	$mgrlang['landf_f_randmed']				= "首頁隨機媒體.";         

	$mgrlang['landf_f_randmed_d']			= "在網頁上隨機的媒體顯示一個小的選擇."; 

	$mgrlang['landf_f_randmed2']			= "有多少隨機媒體專案應顯示的主頁上."; 	

	$mgrlang['landf_f_featpromo']			= "啟用特色促銷區"; 

	$mgrlang['landf_f_featprint']			= "啟用特色列印區"; 

	$mgrlang['landf_f_featprod']			= "啟用特色產品區"; 

	$mgrlang['landf_f_featpacks']			= "啟用特色配套區"; 

	$mgrlang['landf_f_featcolls']			= "啟用特色收藏區"; 

	$mgrlang['landf_f_featsubs']			= "啟用特色訂閱區"; 

	$mgrlang['landf_f_featcred']			= "啟用特色積分區"; 

	$mgrlang['landf_f_galthumbsize']		= "圖庫縮略圖大小"; 

	$mgrlang['landf_f_galthumbsize_d']		= "選擇圖庫縮略圖的大小.您的縮略圖設置的品質和銳利設置將是相同的.";    	

	$mgrlang['landf_f_cropgalthumb']		= "裁剪圖庫圖示"; 

	$mgrlang['landf_f_cropgalthumb_d']		= "裁剪圖庫圖示，使橫向和縱向的圖像顯示在相同大小.";	

	$mgrlang['landf_f_galperpage']			= "每頁圖庫"; 

	$mgrlang['landf_f_galperpage_d']		= "裁剪圖庫縮略圖圖示，使橫向和縱向的圖像顯示在相同大小.";  	

	$mgrlang['landf_f_galdsort']			= "預設圖庫分類"; 

	$mgrlang['landf_f_galdsort_d']			= "預設值分類圖庫."; 

	$mgrlang['landf_theme']					= "主題"; 

	$mgrlang['landf_thumbnails']			= "縮略圖"; 	

	$mgrlang['landf_rollovers']				= "翻轉"; 	

	$mgrlang['landf_previews']				= "預覽"; 

	$mgrlang['landf_homepage']				= "首頁"; 	

	$mgrlang['landf_other']					= "其他"; 

	$mgrlang['landf_videos']				= "影片"; 

	$mgrlang['landf_pages']					= "頁"; 

	$mgrlang['landf_galleries']				= "陳列室"; 

	$mgrlang['landf_logo']					= "標誌"; ;

	$mgrlang['landf_preimg']				= "預覽圖像"; 	

	$mgrlang['landf_f_details']				= "詳細資料"; 

	$mgrlang['landf_f_details_d']			= "在縮略圖圖示顯示以下詳細資訊."; 

	$mgrlang['landf_f_detro_d']				= "下面的照片翻轉視窗中顯示以下詳細資訊.";  

	$mgrlang['landf_f_actrollover']			= "啟用翻轉"; 

	$mgrlang['landf_f_actrollover_d']		= "滾動縮略圖時，顯示快顯視窗"; 

	

	# AGREEMENTS

	$mgrlang['agree_f_name']				= "執照/協定名稱"; 

	$mgrlang['agree_f_name_d']				= "執照或協定的名稱.在某些情況下，公眾網站，也可以用來作為標題."; 

	$mgrlang['agree_f_content_d']			= "在此許可證或協定的內容將出現在."; 

	

	# PUBLIC AREAS

	$mgrlang['pubLightbox']					= "我的最愛"; 

	$mgrlang['pubRating']					= "媒體評級"; 

	$mgrlang['pubUpdateMembership']				= "更新會員籍"; 

	$mgrlang['pubUpdate']					= "更新"; 

	$mgrlang['pubAccountInfo']				= "帳戶資料"; 

	$mgrlang['pubBio']					= "簡歷"; 

	$mgrlang['pubAddress']					= "地址"; 

	$mgrlang['pubPassword']					= "密碼"; 

	$mgrlang['pubDateTime']					= "日期/時間"; 

	$mgrlang['pubAvatar']					= "頭像"; 

	$mgrlang['pubDelete']					= "刪除"; 

	$mgrlang['pubNew']					= "新"; 

	$mgrlang['pubCreate']					= "創建"; 

	$mgrlang['pubAddItem']					= "添加項目"; 

	$mgrlang['pubMedia']					= "媒體"; 

	$mgrlang['pubRemoveItem']				= "刪除項目"; 

	$mgrlang['pubLogin']					= "登入"; 

	$mgrlang['pubLoggedOut']				= "登出"; 

	$mgrlang['pubLoggedIn']					= "登入"; 

	$mgrlang['pubNewComment']				= "新評論"; 

	$mgrlang['pubNewTag']					= "新標籤"; 

	

	# WELCOME PAGE

	$mgrlang['welcome_wel']					= "歡迎"; 

	$mgrlang['welcome_last_logged']			= "您最後一次登入是"; 

	$mgrlang['welcome_page_title']			= "控制台"; 

	$mgrlang['welcome_ftv_wizard']			= "這似乎是您的第一次登錄到管理區域.您想啟動安裝嚮導.";   

	$mgrlang['welcome_launch_wiz']			= "您想啟動安裝嚮導?"; 

	

	# ADMINISTRATORS PAGE

	$mgrlang['admin_b_new']					= "添加新的"; 

	$mgrlang['admin_b_sa']					= "選擇所有"; 

	$mgrlang['admin_b_sn']					= "沒有選擇"; 

	$mgrlang['admin_b_del']					= "刪除"; 

	$mgrlang['admin_mes_01']				= "已添加一個新的管理員."; 

	$mgrlang['admin_mes_02']				= "您的更改已保存."; 

	$mgrlang['admin_mes_03']				= "成功刪除管理員帳戶."; 

	$mgrlang['admin_mes_04']				= "成功刪除管理員."; 

	$mgrlang['admin_mes_05']				= "沒有管理員被選定為刪除."; 

	$mgrlang['admin_mes_06']				= "只有超級管理員可以刪除管理員.";  

	$mgrlang['admin_mes_07']				= "只有超級管理員可以添加新的管理員."; 

	$mgrlang['admin_mes_08']				= "如果你不是一個超級管理員，您只能編輯自己的個人資料.";  

	$mgrlang['admin_admin']					= "管理員"; 

	$mgrlang['admin_superadmin']			= "超級管理員"; 

	$mgrlang['admin_last_login']			= "最後登入"; 

	$mgrlang['admin_never']					= "從來沒有"; 

	$mgrlang['admin_username']				= "使用姓名"; 

	$mgrlang['admin_password']				= "密碼"; 

	$mgrlang['admin_email']					= "電子郵件"; 

	$mgrlang['admin_mes_09']				= "請輸入"; 

	$mgrlang['admin_mes_10']				= "儲存之前"; 

	$mgrlang['admin_edit_header']			= "編輯管理員"; 

	$mgrlang['admin_edit_message']			= "編輯管理員，然後點擊“保存更改”按鈕."; 

	$mgrlang['admin_new_header']			= "添加新的管理員"; 

	$mgrlang['admin_new_message']			= "下面的表格，你可以添加一個新的管理員為您的網站";

	$mgrlang['admin_tab1']					= "管理員詳細資訊"; 

	$mgrlang['admin_tab2']					= "許可權"; 

	$mgrlang['admin_tab3']					= "活動日誌"; 

	$mgrlang['admin_f_username']			= "用戶名"; 

	$mgrlang['admin_f_username_d']			= "最多17個字母"; 

	$mgrlang['admin_f_password']			= "密碼"; 

	$mgrlang['admin_f_password_d']			= "最多50個字母"; 

	$mgrlang['admin_hidden']				= "隱藏在示範模式"; 

	$mgrlang['admin_f_email']				= "電子郵件"; 

	$mgrlang['admin_f_email_d']				= "最多250個字母"; 

	$mgrlang['admin_f_active']				= "活躍"; 

	$mgrlang['admin_f_active_d']			= "使該帳戶活躍"; 

	$mgrlang['admin_f_perm']				= "許可權"; 

	$mgrlang['admin_f_perm_d']				= "顯示畫廊旁邊的媒體檔案數量，你有機會獲得這個管理員許可權管理."; 

	$mgrlang['admin_f_all']					= "所有"; 

	$mgrlang['admin_f_none']				= "沒有"; 

	$mgrlang['admin_unencrypt']				= "解密"; 

	$mgrlang['admin_mes_11']				= "您輸入的電子郵寄地址無效."; 

	$mgrlang['admin_mes_12']				= "用戶名包含非法字元."; 

	$mgrlang['admin_mes_13']				= "密碼不能包含空格."; 

	$mgrlang['admin_mes_14']				= "此用戶名已被使用.請選擇其他.";  

	$mgrlang['admin_al_none']				= "沒有活動記錄!"; 

	

	# DISCOUNTS BOX

	$mgrlang['discounts_buy']				= "購買"; 

	$mgrlang['discounts_save']				= "或更多，並保存"; 

	

	# GALLERIES

	$mgrlang['galleries_new_header']		= "添加新圖庫"; 

	$mgrlang['galleries_edit_header']		= "編輯圖庫"; 

	$mgrlang['galleries_new_message']		= "通過下面的表格，您可以到您的的網站添加一個圖庫."; 

	$mgrlang['galleries_edit_message']		= "編輯圖庫然後點擊“保存更改”按鈕."; 

	$mgrlang['galleries_f_name']			= "圖庫或活動名稱"; 

	$mgrlang['galleries_f_name_d']			= "最多100個字母"; 

	$mgrlang['galleries_f_description']		= "說明"; 

	$mgrlang['galleries_f_description_d']		= "這個圖庫的說明.這將顯示在公共網站上在這個圖庫."; 

	$mgrlang['galleries_f_parent']			= "家長庫";  

	$mgrlang['galleries_f_parent_d']		= "家長庫"; 

	$mgrlang['galleries_f_dirpath']			= "目錄路徑"; 

	$mgrlang['galleries_f_dirpath_d']		= "這個畫廊的路徑在伺服器上"; 	

	$mgrlang['galleries_none']				= "沒有"; 

	$mgrlang['galleries_mes_01']			= "已創建您的新圖庫."; 

	$mgrlang['galleries_mes_02']			= "您的更改已保存."; 

	$mgrlang['galleries_mes_03']			= "創建失敗:"; 

	$mgrlang['galleries_mes_04']			= "刪除失敗:"; 

	$mgrlang['galleries_mes_05']			= "移動失敗:"; 

	$mgrlang['galleries_mes_06']			= "目錄不存在:"; 

	$mgrlang['galleries_mes_07']			= "目錄已經存在:"; 

	$mgrlang['galleries_nogal']				= "您目前沒有陳列室創建."; 

	$mgrlang['galleries_b_list']			= "列表"; 

	$mgrlang['galleries_b_boxes']			= "盒子"; 

	$mgrlang['galleries_f_icon_d']			= "選擇一個圖示來代表這家圖庫."; 

	$mgrlang['galleries_f_active_d']		= "這個圖庫的日期應該成為活躍."; 

	$mgrlang['galleries_f_expire_d']		= "這個圖庫的日期應該過期，不再可用."; 

	$mgrlang['galleries_f_dlink_d']			= "直接連結到這個圖庫"; 

	$mgrlang['galleries_f_owner_d']			= "這個圖庫是屬於誰的."; 

	$mgrlang['galleries_f_contr_d']			= "允許成員貢獻于這個畫廊.只允許擁有有效會員資格能夠這樣做.";  

	$mgrlang['galleries_f_client']			= "客戶名稱"; 

	$mgrlang['galleries_f_client_d']		= "本次活動的客戶的名稱是"; 

	$mgrlang['galleries_f_eventid']			= "活動代碼或編號"; 

	$mgrlang['galleries_f_eventid_d']		= "你的內部代碼或這事件ID"; 

	$mgrlang['galleries_f_eventdate']		= "活動日期"; 

	$mgrlang['galleries_f_eventdate_d']		= "這個事件發生的日期。此事件用於用戶端搜索.";  	

	$mgrlang['galleries_f_eventloc']		= "活動地點"; 

	$mgrlang['galleries_f_eventloc_d']		= "您想在這裡，您可以輸入活動的城市，國家，位址或任何其他地點的資訊.";   	

	$mgrlang['galleries_f_access']			= "活動地點"; 

	$mgrlang['galleries_f_access_d']		= "選擇誰可以訪問這個圖庫."; 	

	$mgrlang['galleries_f_password']		= "密碼"; 

	$mgrlang['galleries_f_password_d']		= "要使用密碼保護，這個圖庫/活動輸入密碼.對於沒有密碼留空不填."; 

	$mgrlang['galleries_f_assets']			= "資產"; 

	$mgrlang['galleries_f_assets_d']		= "在這種情況下，資產/畫廊數目。包括照片，影片和檔."; 

	$mgrlang['galleries_f_views']			= "查看"; 

	$mgrlang['galleries_f_views_d']			= "在此事件/畫廊次資產已被觀看。清除畫廊美景清除計數，這畫廊和所有的孩子畫廊."; 

	

	$mgrlang['sort_default']				= "使用預定"; 

	$mgrlang['sort_added']					= "上傳日期"; 

	$mgrlang['sort_id']					= "編號"; 

	$mgrlang['sort_title']					= "標題"; 

	$mgrlang['sort_filename']				= "檔案名稱"; 

	$mgrlang['sort_filesize']				= "檔案大小"; 

	$mgrlang['sort_width']					= "寬"; 

	$mgrlang['sort_height']					= "高"; 

	$mgrlang['sort_snumber']				= "排序號碼"; 

	$mgrlang['sort_bid']					= "代碼"; 

	$mgrlang['sort_featured']				= "特色"; 

	$mgrlang['sort_views']					= "查看"; 

	$mgrlang['sort_asce']					= "順序";         

	$mgrlang['sort_desc']					= "逆序"; 

	$mgrlang['galleries_no_mem']			= "現今並未有任何會員選擇."; 

	

	$mgrlang['galleries_f_ded']				= "顯示活動詳細資訊"; 

	$mgrlang['galleries_f_ded_d']			= "在大眾圖庫頁面顯示活動詳細資訊."; 	

	

	

	

	

	

	# NEWS

	$mgrlang['news_new_header']				= "添加新新聞專案"; 

	$mgrlang['news_edit_header']			= "編輯新聞專案"; 

	$mgrlang['news_new_message']			= "以下表格，您可以添加一個新的新聞項目到您的網站."; 

	$mgrlang['news_edit_message']			= "編輯這則新聞專案並按一下“保存更改”按鈕."; 

	$mgrlang['news_tab1']					= "新聞詳細"; 

	$mgrlang['news_tab2']					= "群體"; 

	$mgrlang['news_f_date']					= "發佈日期/時間"; 

	$mgrlang['news_f_date_d']				= "發佈日期/時間於這新聞 (年年年年/月月/日日 分分:秒秒鐘).新聞中心專案將不會顯示在此日期之前，"; 

	$mgrlang['news_f_title']				= "標題"; 

	$mgrlang['news_f_title_d']				= "最多100個字元"; 

	$mgrlang['news_f_short']				= "簡單介紹"; 

	$mgrlang['news_f_short_d']				= "這是預覽說明."; 

	$mgrlang['news_f_article']				= "文章"; 

	$mgrlang['news_f_acticle_d']			= "完整的新聞文章"; 

	$mgrlang['news_f_active']				= "​​活躍"; 

	$mgrlang['news_f_active_d']				= "這則新聞在網站上顯示."; 

	$mgrlang['news_f_homepage']				= "首頁";

	$mgrlang['news_f_homepage_d']			= "在網頁上顯示這則新聞專案."; 

	$mgrlang['news_title']					= "標題"; 

	$mgrlang['news_al_delete']				= "刪除新聞專案"; 

	$mgrlang['news_f_groups']				= "新聞群組"; 

	$mgrlang['news_f_groups_d']				= "這屬於選擇新聞群組的新聞."; 

	$mgrlang['news_f_expire_d']				= "得知此消息後項目的日期應該過期，無法再顯現."; 

	

	

	# PAGE CONTENT AREAS

	$mgrlang['pc_new_header']				= "添加新的內容"; 

	$mgrlang['pc_edit_header']				= "編輯頁面內容"; 

	$mgrlang['pc_new_message']				= "以下表格，您可以添加新的內容到您的網站和網頁."; 

	$mgrlang['pc_edit_message']				= "編輯的內容，然後點擊“保存更改”按鈕."; 

	$mgrlang['pc_content']					= "一般內容區"; 

	$mgrlang['pc_custom_page']				= "您的內容網頁"; 

	$mgrlang['pc_custom_block']				= "你的內容塊"; 

	

	# EMAIL CONTENT AREAS

	$mgrlang['ec_new_header']				= "添加新電子郵件"; 

	$mgrlang['ec_edit_header']				= "編輯電子郵件內容"; 

	$mgrlang['ec_new_message']				= "通過以下表格，您可以添加一個新的電子郵件消息."; 

	$mgrlang['ec_edit_message']				= "編輯此電子郵件並點擊“保存更改”按鈕."; 

	$mgrlang['ec_public']					= "使用者的電子郵件"; 

	$mgrlang['ec_admin']					= "管理員的電子郵件"; 

	$mgrlang['ec_custom']					= "您的電子郵件"; 

	$mgrlang['ec_f_emailsub']				= "電子郵件主題"; 

	$mgrlang['ec_f_emailsub_d']				= "一個主題的電子郵件資訊."; 

	

	# AGREEMENTS AREAS

	$mgrlang['agree_new_header']			= "添加新的執照或協議"; 

	$mgrlang['agree_edit_header']			= "編輯執照或協議"; 

	$mgrlang['agree_new_message']			= "通過以下表格，您可以添加一個新的執照或協議"; 

	$mgrlang['agree_edit_message']			= "編輯本執照或協議，然後按一下“保存更改”按鈕."; 

	$mgrlang['agree_general']				= "一般執照及協定"; 

	$mgrlang['agree_custom']				= "您的執照及協議"; 

	

	# SOFTWARE SETUP

	$mgrlang['setup_tab1']					= "一般設定"; 

	$mgrlang['setup_tab2']					= "日期及時間"; 

	$mgrlang['setup_tab3']					= "進階"; 

	$mgrlang['setup_tab4']					= "拷貝"; 

	$mgrlang['setup_tab5']					= "號碼"; 

	$mgrlang['setup_tab6']					= "關於"; 

	$mgrlang['setup_tab7']					= "影像處理中"; 

	$mgrlang['setup_tab8']					= "郵件設置"; 

	$mgrlang['setup_tab9']					= "安全"; 

	$mgrlang['setup_tab10']					= "系統安全"; 

	$mgrlang['setup_mes_01']				= "時間調整"; 

	$mgrlang['setup_hours']					= "時間"; 

	$mgrlang['setup_gmt']					= "世界標準時間"; 

	$mgrlang['setup_timezone_01']			= "Enitwetok, Kwajalien";

	$mgrlang['setup_timezone_02']			= "中途島，薩摩亞群島"; 

	$mgrlang['setup_timezone_03']			= "夏威夷"; 

	$mgrlang['setup_timezone_04']			= "阿拉斯加州"; 

	$mgrlang['setup_timezone_05']			= "太平洋時間(美國;加拿大)"; 

	$mgrlang['setup_timezone_06']			= "山地時間(美國;加拿大)"; 

	$mgrlang['setup_timezone_07']			= "美國中部時間(美國;加拿大),墨西哥城"; 

	$mgrlang['setup_timezone_08']			= "美國東部時間（美國和加拿大），波哥大，利馬"; 

	$mgrlang['setup_timezone_09']			= "大西洋時間（加拿大），卡拉卡斯，拉巴斯"; 

	$mgrlang['setup_timezone_10']			= "紐芬蘭"; 

	$mgrlang['setup_timezone_11']			= "巴西，布宜諾賽勒斯，福克蘭群島"; 

	$mgrlang['setup_timezone_12']			= "中大西洋，阿森松島，聖赫勒拿島"; 

	$mgrlang['setup_timezone_13']			= "亞速爾群島，佛德角群島";  

	$mgrlang['setup_timezone_14']			= "卡薩布蘭卡，倫敦，都柏林，里斯本，蒙羅維亞"; 

	$mgrlang['setup_timezone_15']			= "布魯塞爾，哥本哈根，馬德里，巴黎"; 

	$mgrlang['setup_timezone_16']			= "加里寧格勒，南非";             

	$mgrlang['setup_timezone_17']			= "巴格達，利雅德，莫斯科，奈洛比"; 

	$mgrlang['setup_timezone_18']			= "德黑蘭"; 

	$mgrlang['setup_timezone_19']			= "阿布達比，馬斯喀特，巴庫，第比利斯"; 

	$mgrlang['setup_timezone_20']			= "喀布爾"; ; 

	$mgrlang['setup_timezone_21']			= "葉卡特琳堡，卡拉奇，塔什干"; 

	$mgrlang['setup_timezone_22']			= "孟買，加爾各答，馬德拉斯，新德里"; 

	$mgrlang['setup_timezone_23']			= "阿拉木圖，科隆巴"; 

	$mgrlang['setup_timezone_24']			= "曼谷，河內，雅加達"; 

	$mgrlang['setup_timezone_25']			= "香港，帕斯，新加坡，臺北"; 

	$mgrlang['setup_timezone_26']			= "大阪，札幌，首爾，東京，亞庫次克";

	$mgrlang['setup_timezone_27']			= "阿德萊德，達爾文"; ;

	$mgrlang['setup_timezone_28']			= "墨爾本，悉尼，巴布亞新磯內亞";  

	$mgrlang['setup_timezone_29']			= "馬加丹，所羅門群島，新赫里多尼亞"; 

	$mgrlang['setup_timezone_30']			= "奧克蘭，斐濟，馬紹爾群島"; 

	$mgrlang['setup_f_iptc']				= "讀取IPTC信息"; 

	$mgrlang['setup_f_iptc_d']				= "在導入時，從照片中讀取IPTC資訊."; 

	$mgrlang['setup_f_exif']				= "讀取EXIF信息"; 

	$mgrlang['setup_f_exif_d']				= "在導入時，從照片中讀取EXIF資訊."; 

	$mgrlang['setup_f_url']					= "完整的URL"; 

	$mgrlang['setup_f_url_d']				= "沒有結尾的斜線(/)"; 	

	$mgrlang['setup_f_idn']					= "傳入目錄路徑"; 

	$mgrlang['setup_f_idn_d']				= "在您的伺服器上的傳入目錄的路徑。必須具有寫許可權."; 

	$mgrlang['setup_f_lp']					= "圖庫路徑"; 

	$mgrlang['setup_f_lp_d']				= "您的伺服器上的圖庫目錄的路徑。必須具有寫許可權.";  

	$mgrlang['setup_f_dv']					= "刪除驗證"; 

	$mgrlang['setup_f_dv_d']				= "每次都詢問驗證管理器中刪除記錄之前."; 

	$mgrlang['setup_f_alerts']				= "彈出警告";

	$mgrlang['setup_f_alerts_d']			= "顯示彈出警報管理區的新會員，新訂單，備份等.";  

	$mgrlang['setup_f_aka']					= "允許支援訪問"; 

	$mgrlang['setup_f_aka_d']				= "允許 Ktools.net 進入只為支持用途而已. 我們將只能登錄，當你請求援助.";

	$mgrlang['setup_f_is']					= "資訊分享"; 

	$mgrlang['setup_f_is_d']				= "匿名份額的統計資訊與Ktools.net."; 	

	$mgrlang['setup_f_admin_ac']			= "管理活動"; 

	$mgrlang['setup_f_admin_ac_d']			= "記錄管理員的所有活動."; 

	$mgrlang['setup_f_mem_ac']				= "會員活動"; 

	$mgrlang['setup_f_mem_ac_d']			= "記錄所有活動會員."; 

	$mgrlang['setup_f_ceditor']				= "內容編輯"; 

	$mgrlang['setup_f_ceditor_d']			= "在文件區域中選擇一個內容編輯器使用."; 

	$mgrlang['setup_f_ceditor_op1']			= "InnovaEditor";

	$mgrlang['setup_f_ceditor_op2']			= "TinyMCE";

	$mgrlang['setup_f_ceditor_op0']			= "無"; 

	$mgrlang['setup_f_uploader']			= "批量上傳"; 

	$mgrlang['setup_f_uploader_d']			= "選擇批次上載您想使用時將介質添加到您的網站."; 

	$mgrlang['setup_f_uploader_op1']		= "Java的上傳（推薦）"; 

	$mgrlang['setup_f_uploader_op2']		= "Flash上傳"; 

	$mgrlang['setup_f_uploader_op3']		= "HTML上傳"; 

	$mgrlang['setup_f_weight']				= "重量單位符號"; 

	$mgrlang['setup_f_weight_d']			= "標籤，將顯示的重量單位旁邊。例如磅，公斤，磅或公斤."; 

	$mgrlang['setup_f_tz']					= "時區"; 

	$mgrlang['setup_f_tz_d']				= "選擇您所在的時區."; 

	$mgrlang['setup_f_dtb']					= "日期和時間預覽"; 

	$mgrlang['setup_f_dtb_d']				= "預覽時間的調整."; 

	$mgrlang['setup_f_ds']					= "夏令時"; 

	$mgrlang['setup_f_ds_d']				= "自動調整夏令時."; 

	$mgrlang['setup_f_df']					= "日期格式"; 

	$mgrlang['setup_f_df_d']				= "美國，歐洲或國際/世界時間."; 

	$mgrlang['setup_f_dd']					= "日期顯示"; 

	$mgrlang['setup_f_dd_d']				= "您想怎麼顯示日期."; 

	$mgrlang['setup_f_dd1_d']				= "完整日期"; 

	$mgrlang['setup_f_dd2_d']				= "簡短日期"; 

	$mgrlang['setup_f_dd3_d']				= "號碼日期"; 

	$mgrlang['setup_f_df1_d']				= "月月/日日/年年年年"; 

	$mgrlang['setup_f_df2_d']				= "日日/月月/年年年年"; 

	$mgrlang['setup_f_df3_d']				= "年年年年/月月/日日"; 

	$mgrlang['setup_f_cf']					= "時間格式"; 

	$mgrlang['setup_f_cf_d']				= "12或24小時格式."; 

	$mgrlang['setup_f_cf1_d']				= "12小時"; 

	$mgrlang['setup_f_cf2_d']				= "24小時"; 

	$mgrlang['setup_f_nds']					= "號碼日期分隔"; 

	$mgrlang['setup_f_nds_d']				= "顯示數位格式的日期時,與此符號的分開的月，日和年."; 

	$mgrlang['setup_f_lfo']					= "語言檔案覆蓋"; 

	$mgrlang['setup_f_lfo_d']				= "覆蓋所選擇的語言在公共網站上設置的日期和時間格式。為每種語言在lang.settings.php文件."; 

	$mgrlang['setup_f_mo']					= "會員覆蓋"; 

	$mgrlang['setup_f_mo_d']				= "在他們的帳戶，允許會員自己設定的日期和時間選項."; 

	$mgrlang['setup_f_mwr']					= "SEO連結"; 

	$mgrlang['setup_f_mwr_d']				= "按一下以啟用mod_rewrite模組支援對搜尋引擎友好的URL. 需要合格 .htaccess file.";	

	$mgrlang['setup_f_amfolders']				= "自動管理資料夾"; 

	$mgrlang['setup_f_amfolders_d']				= "自動為我管理資料夾。禁用時，您將能夠管理自己的資料夾";	

	$mgrlang['setup_f_debug']				= "管理器調試面板"; 

	$mgrlang['setup_f_debug_d']				= "按Ctrl + Shift+ D打開面板處於活動狀態時."; 

	$mgrlang['setup_f_demo']				= "示範模式"; 

	$mgrlang['setup_f_demo_d']				= "允許示範訪問管理區."; 	

	$mgrlang['setup_f_purge_ac']				= "清除活動日誌"; 

	$mgrlang['setup_f_purge_ac_d']				= "在此之後的時間內，刪除舊的活動日誌."; 

	$mgrlang['setup_time_never']				= "從來沒有"; 

	$mgrlang['setup_time_1d']				= "一天之後"; 

	$mgrlang['setup_time_3d']				= "三天之後"; 

	$mgrlang['setup_time_5d']				= "五天之後"; 

	$mgrlang['setup_time_1w']				= "一周之後"; 

	$mgrlang['setup_time_2w']				= "兩周之後"; 

	$mgrlang['setup_time_1m']				= "一個月之後"; 

	$mgrlang['setup_time_2m']				= "兩個月之後"; 

	$mgrlang['setup_time_3m']				= "三個月之後"; 

	$mgrlang['setup_time_6m']				= "六個月之後"; 

	$mgrlang['setup_time_1y']				= "一年之後"; 

	$mgrlang['setup_f_captcha']				= "驗證碼"; 

	$mgrlang['setup_f_captcha_d']				= "讓遊客進入一個獨特的代碼簽名時，打開驗證碼新註冊";

	$mgrlang['setup_f_email_conf']				= "需要電子郵件確認"; 

	$mgrlang['setup_f_email_conf_d']			= "要求確認他們的標誌之前，通過電子郵件的帳戶生效"; 

	$mgrlang['setup_f_drc']					= "禁用右鍵點擊"; 

	$mgrlang['setup_f_drc_d']				= "嘗試防止訪客使用在您的網站上點擊滑鼠右鍵.還禁用IE的圖像工具列.";  

	$mgrlang['setup_f_dcp']					= "禁用複製和張貼"; 

	$mgrlang['setup_f_dcp_d']				= "嘗試防止訪客使用複製和張貼的照片."; 

	$mgrlang['setup_f_dpri']				= "禁用列印"; 

	$mgrlang['setup_f_dpri_d']				= "嘗試防止訪客從您的網站列印頁面."; 

	$mgrlang['setup_f_dpl']					= "禁用連結"; 

	$mgrlang['setup_f_dpl_d']				= "嘗試防止從外部網站連結縮略圖和樣品照片."; 

	$mgrlang['setup_f_dec_separator']			= "十進位分隔符號"; 

	$mgrlang['setup_f_dec_separator_d']			= "要使用的字元的十進位分隔符號."; 

	$mgrlang['setup_f_thou_separator']			= "千位元分隔符號的字元"; 

	$mgrlang['setup_f_thou_separator_d']			= "使用千位元分隔符號的字元."; 

	$mgrlang['setup_f_thou_sep_none']			= "無"; 

	$mgrlang['setup_f_thou_sep_space']			= "空間"; 

	$mgrlang['setup_f_num_after']				= "小數位數"; 

	$mgrlang['setup_f_num_after_d']				= "數小數點後的小數";

	$mgrlang['setup_f_neg_format']				= "負數格式"; 

	$mgrlang['setup_f_neg_format_d']			= "使用的格式為負數."; 

	$mgrlang['setup_f_lang_override']			= "語言檔覆蓋"; 

	$mgrlang['setup_f_lang_override_d']			= "在公共網站上，覆蓋數位格式設置與所選擇的語言檔中的設置."; 

	$mgrlang['setup_f_mem_override']			= "會員覆蓋"; 

	$mgrlang['setup_f_mem_override_d']			= "允許會員在其帳戶中設定自己的數位格式選項.";  

	$mgrlang['setup_f_num_preview']				= "號碼預覽"; 

	$mgrlang['setup_f_num_pos']				= "正面"; 

	$mgrlang['setup_f_num_neg']				= "負面"; 		

	$mgrlang['setup_f_blockip']				= "阻擋IP地址"; 

	$mgrlang['setup_f_blockip_d']			= "阻擋這些IP位址查看您的網站.每行一個.";  

	$mgrlang['setup_f_blockreferrer']		= "阻擋網域"; 

	$mgrlang['setup_f_blockreferrer_d']		= "阻擋訪客來自這些領域的.例如:google.com.不包括www.每行一個.";   

	$mgrlang['setup_f_blockemail']			= "阻擋電子郵件郵址"; 

	$mgrlang['setup_f_blockemail_d']		= "阻擋座訪問者的電子郵寄地址包含下列簽署. 例如: someone@somewhere.com or @yahoo.com.";

	$mgrlang['setup_f_blockwords']			= "過濾字眼"; 

	$mgrlang['setup_f_blockwords_d']		= "以下的輸入內容會被過濾掉.";

	$mgrlang['setup_f_imageproc']			= "影像處理器"; 

	$mgrlang['setup_f_imageproc_d']			= "應使用哪個影像處理器."; 

	$mgrlang['setup_f_mailproc']			= "郵件類型"; 

	$mgrlang['setup_f_mailproc_d']			= "選擇伺服器的過程中您想用來發送電子郵件."; 

	$mgrlang['setup_f_copy_messages']		= "複製電子郵件消息"; 

	$mgrlang['setup_f_copy_messages_d']		= "複製所有被發送的電子郵件，通過該系統會員資訊區域中.";  

	$mgrlang['setup_phpmail']				= "PHP Mail()";

	$mgrlang['setup_smtp']					= "SMTP";

	$mgrlang['setup_f_smtp_host']			= "SMTP Host";

	$mgrlang['setup_f_smtp_host_d']			= "SMTP server host.";

	$mgrlang['setup_f_smtp_port']			= "SMTP 埠";

	$mgrlang['setup_te_sent']			= "測試電子郵件發送"; 

	$mgrlang['setup_f_smtp_port_d']			= "此伺服器的埠號. 通常 25.";	

	$mgrlang['setup_testemailat_mes']		= "您可以測試您的e-mail發送測試電子郵件設定以下的設置 > 實用工具 > 發送測試電子郵件";

	$mgrlang['setup_f_test_email']			= "發送測試電子郵件"; 

	$mgrlang['setup_f_test_email_d']		= "發送測試電子郵件支援電子郵寄地址設置“下的定義 > 網站設置 > 聯繫方式";	

	$mgrlang['setup_f_smtp_username']		= "SMTP用戶名"; 

	$mgrlang['setup_f_smtp_username_d']		= "SMTP伺服器的用戶名."; 

	$mgrlang['setup_f_smtp_password']		= "SMTP密碼"; 

	$mgrlang['setup_f_smtp_password_d']		= "SMTP伺服器的密碼."; 

	$mgrlang['setup_gd']					= "GD Library";

	$mgrlang['setup_imagemagick']			= "ImageMagick";

	$mgrlang['setup_imagemagickerr']		= "沒有安裝";

	$mgrlang['setup_f_flexpricing']			= "高級定價"; 

	$mgrlang['setup_f_flexpricing_d']		= "開啟高級定價選項.點擊閱讀更多."; 

	$mgrlang['setup_f_lbu']					= "最後資料庫備份"; 

	$mgrlang['setup_f_lbu_d']				= "當最後一個資料庫備份執行的時間."; 

	$mgrlang['setup_f_srp']					= "設定還原點"; 

	$mgrlang['setup_f_srp_d']				= "自動創建一個還原點每次保存任何設置. 包括 “網站設置”，“外觀和感覺”和“軟體設置”區域. 否則復原點只能手動“設置”>“實用程式”下的";

	$mgrlang['setup_f_nbu']					= "下一個資料庫備份"; 

	$mgrlang['setup_f_nbu_d']				= "下一次資料庫備份運行的時間";

	$mgrlang['setup_f_dbbackup']			= "資料庫備份相隔時間"; 

	$mgrlang['setup_f_dbbackup_d']			= "設置在其中的時間間隔自動備份您的網站伺服器上的資料庫.";  

	$mgrlang['setup_f_dbbackup_o1']			= "一天一次"; 

	$mgrlang['setup_f_dbbackup_o2']			= "一星期一次"; 

	$mgrlang['setup_f_dbbackup_o3']			= "一個月一次"; 

	$mgrlang['setup_f_dbbackup_o4']			= "從沒"; 

	$mgrlang['setup_f_cron']				= "Cron Job 路徑";

	$mgrlang['setup_f_cron_d']				= "到cron作業檔的路徑在伺服器上設置可選的cron作業";

	$mgrlang['setup_f_statshtml']			= "統計HTML"; 

	$mgrlang['setup_f_statshtml_d']			= "輸入HTML關閉網站的統計計數器."; 

	$mgrlang['setup_f_savedbackup']			= "下載資料庫備份"; 

	$mgrlang['setup_f_savedbackup_d']		= "下載保存的資料庫備份.選擇日期的備份，按一下下載.";  

	$mgrlang['setup_f_savedbackup_b']		= "下載"; 

	$mgrlang['setup_f_nobackup']			= "沒有備份發現"; 		

	$mgrlang['setup_f_serial']			= "序號"; 

	$mgrlang['setup_f_serial_d']			= "需要序號來安裝."; 

	$mgrlang['setup_f_actkey']			= "啟用金鑰"; 

	$mgrlang['setup_f_actkey_d']			= "此安裝啟用金鑰."; 

	$mgrlang['setup_f_valkey']			= "驗證金鑰"; 

	$mgrlang['setup_f_valkey_d']			= "此安裝的驗證金鑰."; 

	$mgrlang['setup_f_addons']			= "附加組件"; 

	$mgrlang['setup_f_addons_d']			= "安裝後附加組件."; 	

	$mgrlang['setup_f_version']			= "版本資訊"; 

	$mgrlang['setup_f_version_d']			= "目前已安裝的產品名稱和版本."; 

	$mgrlang['setup_f_servinfo']			= "伺服器資訊"; 

	$mgrlang['setup_f_servinfo_d']			= "顯示此伺服器的phpinfo（）."; 

	$mgrlang['setup_f_opensi']				= "打開伺服器資訊"; 

	$mgrlang['setup_f_licagree']			= "軟體使用授權協定"; 

	$mgrlang['setup_f_licagree_d']			= "顯示該軟體的授權合約."; 

	$mgrlang['setup_f_api']					= "API密碼"; 

	$mgrlang['setup_f_api_d']				= "使用指令稿API時需要的密碼."; 

	$mgrlang['setup_f_openla']				= "開放授權合約"; 

	$mgrlang['setup_mes_01']				= "您網站的您輸入的URL不匹配當前的URL."; 

	$mgrlang['setup_mes_02']				= "您已輸入的URL您的網站不匹配當前的URL."; 

	$mgrlang['setup_mes_03']				= "傳入您所輸入的路徑是不可寫."; 

	$mgrlang['setup_mes_05']				= "圖庫路徑您已經進入不存在於伺服器."; 

	$mgrlang['setup_mes_06']				= "圖庫路徑您輸入的是不可寫的."; 

	$mgrlang['setup_mes_07']				= "銳化選項需要在伺服器上安裝PHP 5或更高版本.";  

	$mgrlang['setup_tip_01']				= "要手動創建一個還原點的設置'Settings > Utilities'.然後在創建還原點，點擊“創建”."; 	

	$mgrlang['setup_image_caching']			= "圖像暫存"; 

	$mgrlang['setup_image_caching_d']		= "暫存的圖像，使他們更快.此設置可以改變在tweak.php文件."; 	

	$mgrlang['setup_ic_time']				= "圖像暫存時間"; 

	$mgrlang['setup_ic_time_d']				= "期限時間，以秒暫存圖像，然後再裝入一個新的副本.此設置可以改變在tweak.php文件.";  

	$mgrlang['setup_seconds']				= "秒"; 	

	$mgrlang['setup_en_cache']				= "啟用頁面暫存"; 

	$mgrlang['setup_en_cache_d']			= "暫存的網頁，使他們更快地載入.不影響照片"; 	

	$mgrlang['setup_pc_period']				= "頁面暫存期"; 

	$mgrlang['setup_pc_period_d']			= "在幾秒鐘的時間來暫存頁面期限."; 	

	$mgrlang['setup_customizer']			= "定制";

	$mgrlang['setup_customizer_d']			= "價格和其他選項自訂列印，產品和每個媒體檔的數碼版本."; 	

	$mgrlang['setup_htaccess']				= "此功能需要一個有效的.htaccess檔中的目錄安裝在那裡的PhotoStore."; 	

	$mgrlang['setup_f_autoenc']				= "自動資料夾加密"; 

	$mgrlang['setup_f_autoenc_d']			= "自動默認為資料夾加密，而無需以檢查框."; 

	$mgrlang['setup_f_libsize']				= "圖庫"; 

	$mgrlang['setup_f_libsize_d']			= "您的媒體庫大約大小."; 

	$mgrlang['setup_f_sfiletype']			= "支援的檔案類型"; 

	$mgrlang['setup_f_sfiletype_d']			= "目前支持的檔案類型."; 	

	

	# STORAGE

	$mgrlang['storage_t_alias']				= "別號"; 

	$mgrlang['storage_new_header']			= "添加新的儲存設定檔"; 

	$mgrlang['storage_edit_header']			= "編輯儲存設定檔"; 

	$mgrlang['storage_new_message']			= "通過以下表格，您可以添加新的儲存設定檔."; 

	$mgrlang['storage_edit_message']		= "編輯本存儲設定檔，然後點擊“保存更改”按鈕."; 	

	$mgrlang['storage_tab1']				= "詳細資訊"; 

	$mgrlang['storage_tab2']				= "群組"; 

	$mgrlang['storage_tab3']				= "進階"; 

	$mgrlang['storage_f_alias']				= "別號"; 

	$mgrlang['storage_f_alias_d']			= "此存儲設定檔的名稱."; 

	$mgrlang['storage_f_groups']			= "儲存組"; 

	$mgrlang['storage_f_groups_d']			= "這種方法屬於選擇儲存組."; 

	$mgrlang['storage_f_st']				= "儲存類型"; 

	$mgrlang['storage_f_st_d']				= "此檔案的存儲類型."; 	

	$mgrlang['storage_f_stop1']				= "伺服器目錄"; 

	$mgrlang['storage_f_stop2']				= "FTP";

	$mgrlang['storage_f_stop3']				= "Amazon S3";

	$mgrlang['storage_f_stop4']				= "Rackspace Cloud Files";

	$mgrlang['storage_f_active']			= "活躍"; 

	$mgrlang['storage_f_active_d']			= "這個儲存設定檔設置為活躍."; 	

	$mgrlang['storage_f_ftphost']			= "主機"; 

	$mgrlang['storage_f_ftphost_d']			= "FTP主機或IP位址."; 

	$mgrlang['storage_f_ftpport']			= "Port";

	$mgrlang['storage_f_ftpport_d']			= "FTP port number to use";

	$mgrlang['storage_f_ftpuser']			= "用戶名"; 

	$mgrlang['storage_f_ftpuser_d']			= "FTP帳戶的用戶名."; 

	$mgrlang['storage_f_ftppass']			= "密碼"; 

	$mgrlang['storage_f_ftppass_d']			= "FTP帳戶的密碼."; 

	$mgrlang['storage_f_ftppath']			= "路徑"; 

	$mgrlang['storage_f_ftppoath_d']		= "在FTP伺服器上的目錄路徑."; 

	$mgrlang['storage_f_localpath']			= "絕對路徑"; 

	$mgrlang['storage_f_localpath_d']		= "儲存位置的絕對路徑."; 

	$mgrlang['storage_b_test']				= "測試"; 

	$mgrlang['storage_b_checking']			= "檢查..."; 

	$mgrlang['storage_f_media']				= "媒體"; 

	$mgrlang['storage_f_media_d']			= "在這個儲存位置的媒體檔數約";

	$mgrlang['storage_f_space']				= "磁碟空間"; 

	$mgrlang['storage_f_space_d']			= "近似磁碟空間使用的媒體在這個存儲位置";	

	$mgrlang['storage_f_as3key']			= "進入鍵";	

	$mgrlang['storage_f_as3key_d']			= "您的Amazon S3帳戶的進入";

	$mgrlang['storage_f_as3skey']			= "秘密鑰匙"; 

	$mgrlang['storage_f_as3skey_d']			= "您的秘密鑰匙從Amazon S3帳戶";	

	$mgrlang['storage_f_cf_username']		= "用戶名稱"; 	

	$mgrlang['storage_f_cf_username_d']		= "您的用戶名Rackspace的雲端檔檔案";

	$mgrlang['storage_f_cf_apikey']			= "API金鑰"; 	

	$mgrlang['storage_f_cf_apikey_d']		= "你的API金鑰為Rackspace的雲端檔檔案";	

	$mgrlang['storage_mes_ftp1']			= "FTP連接成功."; 

	$mgrlang['storage_mes_ftp2']			= "FTP連接失敗。主機或連接埠是不正確的."; 

	$mgrlang['storage_mes_ftp3']			= "FTP連接失敗。用戶名或密碼不正確."; 

	$mgrlang['storage_mes_ftp4']			= "FTP連接失敗。路徑是不正確的."; 

	$mgrlang['storage_mes_ftp5']			= "無法使FTP連接."; 

	$mgrlang['storage_mes_ftp6a']			= "FTP功能不支援此伺服器所"; 

	$mgrlang['storage_mes_ftp6b']			= "此伺服器不支援的功能，有必要使用PHP的FTP連接。加入PHP的FTP支援，請聯繫您的主機.";  

	$mgrlang['storage_mes_local1']			= "看來你已經進入了一個相對路徑.請輸入唯一的絕對路徑.";

	$mgrlang['storage_mes_local2']			= "測試成功."; 

	$mgrlang['storage_mes_local3']			= "測試失敗。此目錄是不可寫."; 

	$mgrlang['storage_mes_local4']			= "該目錄不存在."; 

	$mgrlang['storage_mes_as3_1']			= "連接到Amazon S3是成功的."; 

	$mgrlang['storage_mes_as3_2']			= "連接到Amazon S3失敗."; 

	$mgrlang['storage_mes_as3_3a']			= "Amazon S3此伺服器不支援的功能"; 

	$mgrlang['storage_mes_as3_3b']			= "此伺服器不支援的功能是必要的，以使Amazon S3的連接使用PHP。請聯繫您的主機添加CURL的支援PHP.";     

	$mgrlang['storage_mes_cf_1']			= "Rackspace雲端檔的連接是成功的";

	$mgrlang['storage_mes_cf_2']			= "Rackspace雲端檔的連接是失敗的";

	$mgrlang['storage_mes_cf_3a']			= "Rackspace雲端檔通過此伺服器功能是不支援的";

	$mgrlang['storage_mes_cf_3b']			= "此伺服器不支援的功能是必要的，使Rackspace雲檔連接使用PHP.請聯繫您的主機添加捲曲支援PHP.";

	

	# FOLDERS

	$mgrlang['folders_t_enc']				= "加密的"; 

	$mgrlang['folders_tab1']				= "詳細資訊"; 

	$mgrlang['folders_tab2']				= "群組"; 

	$mgrlang['folders_tab3']				= "進階"; 

	$mgrlang['folders_f_name']				= "資料夾名稱"; 

	$mgrlang['folders_f_name_d']			= "在您的圖庫資料夾的名稱。字母，數位，底線和虛線。最多40個字元.";  

	$mgrlang['folders_f_storage']			= "儲存"; 

	$mgrlang['folders_f_storage_d']			= "將創建此資料夾的位置."; 

	$mgrlang['folders_f_groups']			= "資料夾組"; 

	$mgrlang['folders_f_groups_d']			= "選擇的資料夾，這個資料夾屬於."; 

	$mgrlang['folders_f_media']				= "媒體"; 

	$mgrlang['folders_f_media_d']			= "在此資料夾中的媒體檔大概數量."; 

	$mgrlang['folders_f_notes']				= "筆錄"; 

	$mgrlang['folders_f_notes_d']			= "此資料夾內部使用的選購注意事項."; 

	$mgrlang['folders_f_space']				= "磁碟空間"; 

	$mgrlang['folders_f_space_d']			= "大概在此資料夾中的媒體使用的磁碟空間."; 	

	$mgrlang['folders_new_header']			= "添加新資料夾"; 

	$mgrlang['folders_edit_header']			= "編輯資料夾"; 

	$mgrlang['folders_new_message']			= "通過以下表格，您可以添加一個新的資料夾."; 

	$mgrlang['folders_edit_message']		= "編輯這個資料夾，然後點擊“保存更改”按鈕."; 

	$mgrlang['folders_f_sp']				= "儲存路徑"; 

	$mgrlang['folders_f_sp_d']				= "到這個資料夾中儲存位置的路徑."; 

	$mgrlang['folders_auto_manage_mes']		= "自動為您管理您的資料夾.要更改此設置的訪問-設置>軟體設置>進階"; 

	

	# TAXES/VAT

	$mgrlang['tax_mes_01']					= "稅/增值稅率包含的字元不是數位."; 

	$mgrlang['tax_mes_02']					= "稅/增值稅的價值不能超過99％"; 

	$mgrlang['tax_mes_03']					= "使用預設的稅/增值稅值"; 

	$mgrlang['tax_mes_04']					= "稅/增值稅的值必須大於1％"; 

	$mgrlang['tax_mes_05']					= "稅/增值稅的預設值中包含的字元不是數位."; 

	$mgrlang['tax_mes_06']					= "稅/增值稅的價值不能超過99％"; 

	$mgrlang['tax_mes_07']					= "稅/增值稅的值必須大於1％"; 

	$mgrlang['tax_tab1']					= "詳細資訊"; 

	$mgrlang['tax_tab2']					= "設置"; 

	$mgrlang['tax_tab3']					= "進階"; 

	$mgrlang['tax_f_tdi']					= "收取稅金的數位產品"; 

	$mgrlang['tax_f_tdi_d']					= "檢查啟用。這將覆蓋任何區域設置."; 

	$mgrlang['tax_f_tp']					= "應用稅務列印/產品"; 

	$mgrlang['tax_f_tp_d']					= "檢查啟用。這將覆蓋任何區域設置."; 

	$mgrlang['tax_f_taxa']					= "稅收名稱"; 

	$mgrlang['tax_f_taxa_d']				= "稅務值A的例子：稅，增值稅，消費稅，PST的顯示名稱。留空給不是稅務值A."; 

	$mgrlang['tax_f_taxb']					= "稅務B名稱"; 

	$mgrlang['tax_f_taxb_d']				= "顯示名稱的稅務值B.無稅B留空."; 

	$mgrlang['tax_f_taxc']					= "稅務C名稱"; 

	$mgrlang['tax_f_taxc_d']				= "顯示名稱的稅務值C.無稅C留空."; 	

	$mgrlang['tax_f_dtr']					= "預設值稅/增值稅率"; 

	$mgrlang['tax_f_dtr_d']					= "如果沒有稅/增值稅率設定為一個地區，這些值將被用.";  

	$mgrlang['tax_f_dtr_a']					= "稅務值A"; 

	$mgrlang['tax_f_dtr_b']					= "稅務值B"; 

	$mgrlang['tax_f_dtr_c']					= "稅務值C"; 

	$mgrlang['tax_f_inc']					= "價格包括稅款"; 

	$mgrlang['tax_f_inc_d']					= "給客戶的稅務顯示價格已經包括."; 

	$mgrlang['tax_f_taxtype']				= "稅務設置"; 

	$mgrlang['tax_f_taxtype_d']				= "選擇適用相同的稅款，向每個人徵收稅金基於區域";

	$mgrlang['tax_f_taxa_default']			= "稅務值A"; 

	$mgrlang['tax_f_taxa_default_d']		= "所得稅額收取稅務A."; 

	$mgrlang['tax_f_taxb_default']			= "稅務值B"; 

	$mgrlang['tax_f_taxb_default_d']		= "所得稅額收取稅務B."; 

	$mgrlang['tax_f_taxc_default']			= "稅務值C"; 

	$mgrlang['tax_f_taxc_default_d']		= "所得稅額收取稅務C."; 

	$mgrlang['tax_f_tbr_d']					= "此選項允許您設置您的商店，讓您可以指定由國家，州/省，郵遞區號/郵遞區號稅。保存後啟用此選項的稅，然後被分配在這些領域.";   

	$mgrlang['tax_f_optout']				= "選擇退出"; 

	$mgrlang['tax_f_optout_d']				= "允許會員退出繳納稅款（不推薦）."; 

	$mgrlang['tax_f_stm']					= "銷售稅消息"; 

	$mgrlang['tax_f_stm_d']					= "您可能須依法提供您的稅務號碼或代碼。使用此欄位提供資訊有關的稅務類型和您的稅務號碼或代碼.";  	

	$mgrlang['tax_globally']				= "稅在全球範圍內"; 

	$mgrlang['tax_by_region']				= "稅區"; 

	$mgrlang['tax_f_taxmem']				= "稅會員資格"; 	

	

	# SOFTWARE UPGRADE

	$mgrlang['softup_mes_01']				= "這不是一個有效的安裝檔."; 

	$mgrlang['softup_mes_02']				= "升級完成！您應該看到新的版本編號."; 

	$mgrlang['softup_tab1']					= "詳細"; 

	$mgrlang['softup_f_replace']			= "替代檔案"; 

	$mgrlang['softup_f_replace_d']			= "資料庫已被更新。以下檔已經改變，將需要更換。要完成升級替換在以下列表的每個檔的​​zip檔與新的。完成後，點擊完成，您將被帶回升級頁面.";   

	$mgrlang['softup_f_current']			= "安裝的版本"; 

	$mgrlang['softup_f_current_d']			= "目前的版本，您已經安裝了."; 

	$mgrlang['softup_f_check']				= "最新可用版本"; 

	$mgrlang['softup_f_check_d']			= "最新版本的軟體可用的"; 

	$mgrlang['softup_f_upfile']				= "選擇升級或添加的檔"; 

	$mgrlang['softup_f_upfile_d']			= "處理選擇一個upgrade.php文件。瀏覽檔的“瀏覽”按鈕，然後按一下“程式升級檔”.";   

	$mgrlang['softup_b_process']			= "過程升級檔案"; 

	$mgrlang['softup_b_check_now']			= "現在檢查"; 

	$mgrlang['softup_error_01']				= "升級未成功完成."; 

	

	# COUNTRIES

	$mgrlang['countries_t_name']			= "國家名稱"; 

	$mgrlang['country_f_title']				= "國家名稱"; 

	$mgrlang['country_f_title_d']			= "國家的名稱"; 

	$mgrlang['country_tab1']				= "詳細資訊"; 

	$mgrlang['country_tab2']				= "航運"; 

	$mgrlang['country_tab3']				= "稅"; 

	$mgrlang['country_tab4']				= "進階"; 

	$mgrlang['country_tab5']				= "群組"; 

	$mgrlang['country_new_header']			= "增加新的國家"; 

	$mgrlang['country_new_message']			= "通過以下表格，您可以添加一個國家."; 

	$mgrlang['country_edit_message']		= "編輯這個國家，然後點擊“保存更改”按鈕."; 

	$mgrlang['country_f_active']			= "活躍"; 

	$mgrlang['country_f_active_d']			= "在列表中，設置這個國家的活躍."; 

	$mgrlang['country_f_code2']				= "國家代碼（2）"; 

	$mgrlang['country_f_code2_d']			= "2字元的國家代碼."; 

	$mgrlang['country_f_code3']				= "國家代碼（3）"; 

	$mgrlang['country_f_code3_d']			= "3字元的國家代碼."; 

	$mgrlang['country_f_numcode']			= "數字代碼"; 

	$mgrlang['country_f_numcode_d']			= "ISO 3166-1分配給該國的數字代碼."; 	

	$mgrlang['country_f_longitude']			= "經度";  

	$mgrlang['country_f_longitude_d']		= "國家的經度."; 

	$mgrlang['country_f_latitude']			= "緯度"; 

	$mgrlang['country_f_latitude_d']		= "國家的緯度";

	$mgrlang['country_f_region']			= "地區";

	$mgrlang['country_f_region_d']			= "個國家所在的地區.如果沒有區域留空或輸入無."; 	

	$mgrlang['country_f_tax_a']				= "A 稅"; 

	$mgrlang['country_f_tax_a_d']			= "在這個國家到為稅項一個充電所得稅額";

	$mgrlang['country_f_tax_b']				= "B 稅"; 

	$mgrlang['country_f_tax_b_d']			= "在這個國家到為稅項一個充電所得稅額";

	$mgrlang['country_f_tax_c']				= "C 稅"; 

	$mgrlang['country_f_tax_c_d']			= "在這個國家到為稅項一個充電所得稅額";	

	$mgrlang['country_f_tax_prints']		= "稅收列印服務和產品"; 

	$mgrlang['country_f_tax_prints_d']		= "使用稅控列印服務和產品，包括配套在這個國家."; 

	$mgrlang['country_f_tax_digital']		= "稅收數位下載"; 

	$mgrlang['country_f_tax_digital_d']		= "應用在這個國家的數位下載稅."; 

	$mgrlang['country_f_tax_subs']			= "稅務訂閱"; 

	$mgrlang['country_f_tax_subs_d']		= "應用在這個國家的付費訂閱稅."; 

	$mgrlang['country_f_tax_shipping']		= "稅務運費"; 

	$mgrlang['country_f_tax_shipping_d']		= "在這個國家的運輸成本稅的命令，";

	$mgrlang['country_f_tax_credits']		= "稅務積分"; 

	$mgrlang['country_f_tax_credits_d']		= "當客戶購買學分在這個國家所申請的稅收";

	$mgrlang['country_f_groups']			= "國家群組"; 

	$mgrlang['country_f_groups_d']			= "選擇這個國家屬於國家組."; 

	

	# STATES

	$mgrlang['states_t_name']				= "州/省名"; 

	$mgrlang['states_country']				= "國家"; 

	$mgrlang['states_edit_countries']		= "更改國家"; 	

	$mgrlang['states_f_title']				= "州/省名"; 

	$mgrlang['states_f_title_d']			= "州/省名稱"; 

	$mgrlang['states_tab1']					= "詳細資訊"; 

	$mgrlang['states_tab2']					= "航運"; 

	$mgrlang['states_tab3']					= "稅"; 

	$mgrlang['states_tab4']					= "進階"; 

	$mgrlang['states_new_header']			= "添加新的州/省"; 

	$mgrlang['states_edit_header']			= " 編輯州/省資訊"; 

	$mgrlang['states_new_message']			= "通過以下表格，您可以添加一個州/省.";  

	$mgrlang['states_edit_message']			= "編輯此州/省，然後點擊“保存更改”按鈕.";  

	$mgrlang['states_f_country']			= "國家"; 

	$mgrlang['states_f_country_d']			= "這個國家的州/省是."; 

	$mgrlang['states_f_active']				= "活躍"; 

	$mgrlang['states_f_active_d']			= "列表中的設置該州/省."; 

	$mgrlang['states_f_scode']				= "州/省代號"; 

	$mgrlang['states_f_scode_d']			= "州/省的縮寫或代碼."; 

	$mgrlang['states_f_tax_a']				= "稅A"; 

	$mgrlang['states_f_tax_a_d']			= "所得稅額收取稅A一個在該州/省";

	$mgrlang['states_f_tax_b']				= "稅B"; 

	$mgrlang['states_f_tax_b_d']			= "所得稅額收取稅B一個在該州/省";

	$mgrlang['states_f_tax_c']				= "稅C"; 

	$mgrlang['states_f_tax_c_d']			= "所得稅額收取稅C一個在該州/省";	

	$mgrlang['states_f_tax_prints']			= "稅務列印服務和產品"; 

	$mgrlang['states_f_tax_prints_d']		= "使用稅控列印服務和產品，包括包在這個州/省";

	$mgrlang['states_f_tax_digital']		= "稅務數碼下載"; 

	$mgrlang['states_f_tax_digital_d']		= "數字下載在這個州/省稅";

	$mgrlang['states_f_tax_subs']			= "稅務訂閱"; 

	$mgrlang['states_f_tax_subs_d']			= "付費訂閱，在該州/省稅";

	$mgrlang['states_f_tax_shipping']		= "稅務運費"; 

	$mgrlang['states_f_tax_shipping_d']		= "以訂單的運輸成本稅，在這個州/省";

	$mgrlang['states_f_tax_credits']		= "稅務學分";

	$mgrlang['states_f_tax_credits_d']		= "申請稅時，當客戶購買學分在這個州/省";

	$mgrlang['states_f_tax_mems']			= "稅務會員籍"; 

	$mgrlang['states_f_tax_mems_d']			= "申請稅時，客戶購買會員資格在這個州/省";

	

	# ZIPCODES

	$mgrlang['zipcodes_t_name']				= "郵遞區號"; 

	$mgrlang['zipcodes_region']				= "區域"; 

	$mgrlang['zipcodes_edit_countries']		= "更改國家"; 

	$mgrlang['zipcodes_edit_states']		= "編輯狀態"; 

	$mgrlang['zipcodes_tab1']				= "詳細資訊"; 

	$mgrlang['zipcodes_tab2']				= "貨運"; 

	$mgrlang['zipcodes_tab3']				= "稅務"; 

	$mgrlang['zipcodes_tab4']				= "進階"; 

	$mgrlang['zipcodes_new_header']			= "添加新的郵遞區號"; 

	$mgrlang['zipcodes_edit_header']		= "編輯郵遞區號"; 

	$mgrlang['zipcodes_new_message']		= "通過以下表格，您可以添加一個郵遞區號.";  

	$mgrlang['zipcodes_edit_message']		= "編輯這郵遞區號，然後點擊“保存更改”按鈕.";  

	$mgrlang['zipcodes_f_region']			= "區域"; 

	$mgrlang['zipcodes_f_region_d']			= "該地區郵政屬於.";

	$mgrlang['zipcodes_f_zipcode']			= "郵遞區號"; 

	$mgrlang['zipcodes_f_active_d']			= "xxxx"; 

	$mgrlang['zipcodes_f_active']			= "活躍"; 

	$mgrlang['zipcodes_f_active_d']			= "在列表中，設置郵遞區號為活動."; 

	$mgrlang['zipcodes_f_tax_a']			= "稅A"; 

	$mgrlang['zipcodes_f_tax_a_d']			= "所得稅額收取稅A在這郵遞區號."; 

	$mgrlang['zipcodes_f_tax_b']			= "稅B"; 

	$mgrlang['zipcodes_f_tax_b_d']			= "所得稅額收取稅B在這郵遞區號."; 

	$mgrlang['zipcodes_f_tax_c']			= "稅C"; 

	$mgrlang['zipcodes_f_tax_c_d']			= "所得稅額收取稅C在這郵遞區號."; 	

	$mgrlang['zipcodes_f_tax_prints']		= "稅收列印服務和產品"; 

	$mgrlang['zipcodes_f_tax_prints_d']		= "使用稅控列印服務和產品，包括在這個郵政代碼的配套."; 

	$mgrlang['zipcodes_f_tax_digital']		= "稅務數位下載"; 

	$mgrlang['zipcodes_f_tax_digital_d']		= "應用稅到這個郵政代碼的數位下載"; 

	$mgrlang['zipcodes_f_tax_subs']			= "稅務訂閱";

	$mgrlang['zipcodes_f_tax_subs_d']		= "以支付訂閱此郵遞區號的稅";

	$mgrlang['zipcodes_f_tax_shipping']		= "稅務運費"; 

	$mgrlang['zipcodes_f_tax_shipping_d']		= "以訂單的運輸成本，這郵遞區號的稅";

	$mgrlang['zipcodes_f_tax_credits']		= "稅務積分";

	$mgrlang['zipcodes_f_tax_credits_d']		= "申請稅時，客戶購買積分的郵遞區號";

		

	# SHIPPING

	$mgrlang['shipping_new_header']			= "添加新的運輸方法"; 

	$mgrlang['shipping_edit_header']		= "編輯運輸方法"; 

	$mgrlang['shipping_new_message']		= "以下表格，您可以添加一個新的運輸方式到您的網站.";   

	$mgrlang['shipping_edit_message']		= "編輯此運輸方法，然後按一下“保存更改”按.";  

	$mgrlang['shipping_tab1']				= "詳細資訊"; 

	$mgrlang['shipping_tab2']				= "定價"; 

	$mgrlang['shipping_tab3']				= "群組"; 

	$mgrlang['shipping_tab4']				= "地區"; 

	$mgrlang['shipping_tab5']				= "進階"; 

	$mgrlang['shipping_f_title']			= "運輸方式名稱"; 

	$mgrlang['shipping_f_title_d']			= "這種運輸方式的名稱.例如:2日運輸"; 

	$mgrlang['shipping_f_desc']				= "描述"; 

	$mgrlang['shipping_f_desc_d']			= "這種運輸方式的簡短說明."; 

	$mgrlang['shipping_f_active']			= "活躍"; 

	$mgrlang['shipping_f_active_d']			= "這種運輸方式設置為活動."; 	

	$mgrlang['shipping_f_ship_days']		= "運輸天"; 

	$mgrlang['shipping_f_ship_days_d']		= "收到物品後，被運往範圍內所需的天. 此設置是另加的.";

	$mgrlang['shipping_f_ship_days1']		= "天數"; 

	$mgrlang['shipping_f_taxable']			= "應納稅"; 

	$mgrlang['shipping_f_taxable_d']		= "檢查如果這種運輸方式應納稅."; 

	$mgrlang['shipping_f_ship_notes']		= "內部運輸注意事項"; 

	$mgrlang['shipping_f_ship_notes_d']		= "注意，以説明您記住這個運輸方式的任何細節。不公開."; 	

	$mgrlang['shipping_f_module']			= "運費模組"; 

	$mgrlang['shipping_f_module_d']			= "使用預定義的運輸公司和他們的計算，或者創建自己的."; 

	$mgrlang['shipping_f_module_op1']		= "習慣"; 

	$mgrlang['shipping_f_calc']				= "計算運輸"; 

	$mgrlang['shipping_f_calc_d']			= "訂單小計，數量或重量計算運費."; 	

	$mgrlang['shipping_f_calc_op1']			= "重量"; 

	$mgrlang['shipping_f_calc_op2']			= "小計"; 

	$mgrlang['shipping_f_calc_op3']			= "定額費用"; 

	$mgrlang['shipping_f_calc_op4']			= "數量"; 

	$mgrlang['shipping_f_ccalc']			= "成本計算"; 

	$mgrlang['shipping_f_ccalc_d']			= "成本是指在固定的金額或百分比."; 

	$mgrlang['shipping_f_ccalc_op1']		= "固定數額"; 

	$mgrlang['shipping_f_ccalc_op2']		= "百分比小計"; 

	$mgrlang['shipping_f_calc_from']		= "從"; 

	$mgrlang['shipping_f_calc_to']			= "致"; 

	$mgrlang['shipping_f_calc_cost']		= "成本"; 

	$mgrlang['shipping_f_calc_inf']			= "和最多"; 

	$mgrlang['shipping_f_ranges']			= "範圍"; 

	$mgrlang['shipping_f_ranges_d']			= "輸入增加範圍的運輸成本";

	$mgrlang['shipping_f_flatrate']			= "定額費用價格"; 

	$mgrlang['shipping_f_flatrate_d']		= "輸入統一稅率為這種運輸方式的價格或百分比";

	$mgrlang['shipping_f_groups']			= "運輸群組"; 

	$mgrlang['shipping_f_groups_d']			= "選擇海運的運輸方式";

	$mgrlang['shipping_f_regions']			= "運輸地區"; 

	$mgrlang['shipping_f_regions_d']		= "選擇的地區，這種運輸方法可用於";

	$mgrlang['shipping_f_regions_op1']		= "世界各地的"; 

	$mgrlang['shipping_f_regions_op2']		= "選擇地區"; 

	$mgrlang['shipping_f_regions_op1_d']		= "這是適用於所有國家的運輸方式."; 

	$mgrlang['shipping_f_regions_op2_d']		= "這是僅適用於以下地區選擇送貨方式."; 	

	$mgrlang['shipping_mes_01']				= "從範圍值是空的或不是一個數字."; 

	$mgrlang['shipping_mes_02']				= "範圍值是空的或不是一個數字."; 

	$mgrlang['shipping_mes_03']				= "這個範圍內的價格是空的或不是一個數字."; 

	$mgrlang['shipping_mes_04']				= "圍不能小於或等於從範圍."; 

	$mgrlang['shipping_mes_05']				= "請進入該單位的運費價格."; 

	$mgrlang['shipping_mes_06']				= "請輸入一個成本的最後一行."; 

	

	# CURRENCIES

	$mgrlang['currencies_t_defaultcur']		= "主要"; 

	$mgrlang['currencies_t_er']				= "匯率"; 

	$mgrlang['currencies_wb_upt']			= "更新貨幣匯率"; 

	$mgrlang['currencies_wb_upd']			= "要更新您的當前匯率通過獲取他們從雅虎或谷歌，請點擊更新按鈕。只有活躍的貨幣進行更新.";  

	$mgrlang['currencies_dd_ur']			= "更新價格"; 

	$mgrlang['currencies_mes1']				= "匯率更新使用"; 

	$mgrlang['currencies_mes2']				= "更新完成"; 

	$mgrlang['currencies_mes3']				= "更新中..."; 

	$mgrlang['currencies_error1']			= "更新失敗 - 貨幣不支持通過谷歌或雅虎"; 

	$mgrlang['currencies_error2']			= "更新失敗 "; 

	$mgrlang['currencies_error3']			= "您無法重新啟動的主要貨幣."; 

	$mgrlang['currencies_tab1']				= "詳細資訊"; 

	$mgrlang['currencies_tab2']				= "匯率"; 

	$mgrlang['currencies_new_header']		= "添加新貨幣"; 

	$mgrlang['currencies_edit_header']		= "編輯貨幣資訊"; 

	$mgrlang['currencies_new_message']		= "通過以下表格，您可以添加一個貨幣."; 

	$mgrlang['currencies_edit_message']		= "編輯貨幣，然後點擊“保存更改”按鈕."; 

	$mgrlang['currencies_f_name']			= "貨幣名稱"; 

	$mgrlang['currencies_f_name_d']			= "貨幣名稱要添加."; 

	$mgrlang['currencies_f_code']			= "貨幣代碼"; 

	$mgrlang['currencies_f_code_d']			= "您要添加的貨幣的3個數位代碼."; 

	$mgrlang['currencies_f_active']			= "活躍"; 

	$mgrlang['currencies_f_active_d']		= "設置該貨幣活躍。有一個以上的貨幣處於活動狀態，讓您的客戶選擇從清單中活躍的貨幣.";

	$mgrlang['currencies_f_defaultcur']		= "預設";

	$mgrlang['currencies_f_defaultcur_d']		= "您運行您的網站，將此設置為預設的或主要貨幣."; 

	$mgrlang['currencies_f_den']			= "定義或符號";

	$mgrlang['currencies_f_den_d']			= "貨幣定義的符號時，顯示價格";

	$mgrlang['currencies_f_dec_sep']		= "小數分隔符號"; 

	$mgrlang['currencies_f_dec_sep_d']		= "要使用的字元的十進位分隔符號."; 

	$mgrlang['currencies_f_thou_sep']		= "千位分隔符號"; 

	$mgrlang['currencies_f_thou_sep_d']		= "使用千位元分隔符號的字元."; 

	$mgrlang['currencies_f_thou_sep_none']		= "沒有"; 

	$mgrlang['currencies_f_thou_sep_space']		= "空間"; 

	$mgrlang['currencies_f_num_after']		= "小數位數"; 

	$mgrlang['currencies_f_num_after_d']		= "數小數點後的小數."; 

	$mgrlang['currencies_f_neg_format']		= "負值貨幣格式"; 

	$mgrlang['currencies_f_neg_format_d']		= "使用的格式為負值的貨幣."; 

	$mgrlang['currencies_f_pos_format']		= "正值貨幣格式"; 

	$mgrlang['currencies_f_pos_format_d']		= "使用的格式為正值貨幣."; 

	$mgrlang['currencies_f_num_neg']		= "負值"; 

	$mgrlang['currencies_f_num_pos']		= "正值"; 

	$mgrlang['currencies_f_num_preview']		= "貨幣預覽"; 

	$mgrlang['currencies_f_er_preview']		= "匯兌預覽"; 

	$mgrlang['currencies_f_er']			= "匯率"; 

	$mgrlang['currencies_f_er_d']			= "從主要的或默認貨幣這個貨幣的匯率";

	$mgrlang['currencies_f_last_update']		= "最後更新"; 

	$mgrlang['currencies_f_last_update_d']		= "該貨幣記錄的最後一次更新."; 

	$mgrlang['currencies_f_never_update']		= "從來沒有"; 

	$mgrlang['currencies_f_er_ffg']			= "從谷歌獲取比率"; 

	$mgrlang['currencies_f_er_ffy']			= "從雅虎獲取比率"; 

	$mgrlang['currencies_f_er_fetching']		= "取回"; 

	$mgrlang['currencies_f_auto_update']		= "最後更新"; 

	$mgrlang['currencies_f_auto_update_d']		= "每天自動抓取當前價格。需要管理器登錄或cron作業設置.";  

	$mgrlang['currencies_f_er_updater']		= "更新使用"; 

	$mgrlang['currencies_f_er_updater_d']		= "在批次處理或自動更新使用以下方法來發現比率.";  

	$mgrlang['currencies_updated_24hours']		= "匯率在過去的24小時內更新."; 

	

	# MEDIA TYPES

	$mgrlang['media_type_new_header']		= "添加新媒體類型"; 

	$mgrlang['media_type_edit_header']		= "編輯媒體類型"; 

	$mgrlang['media_type_new_message']		= "通過以下形式，您可以添加一個新的媒體類型."; 

	$mgrlang['media_type_edit_message']		= "編輯此媒體類型，然後按一下“保存更改”按鈕."; 

	$mgrlang['media_type_tab1']				= "詳細資訊"; 

	$mgrlang['media_type_tab2']				= "群組"; 

	$mgrlang['media_type_f_name']			= "媒體類型名稱"; 

	$mgrlang['media_type_f_name_d']			= "這個名字的媒體類型。例：插圖."; 	

	$mgrlang['media_type_f_groups']			= "媒體類型群組"; 

	$mgrlang['media_type_f_groups_d']		= "選擇組，這種媒體類型屬於."; 

	$mgrlang['media_type_f_active_d']		= "設置及啟動該媒體類型的.暫無媒體類型名稱將不會顯示在網站上.分配給這些媒體類型的媒體，無論此設置將顯示.";

	

	# MEDIA COMMENTS

	$mgrlang['media_comments_t_media']		= "媒體"; 

	$mgrlang['media_comments_t_member']		= "會員"; 

	$mgrlang['media_comments_t_comment']		= "評論"; 	

	$mgrlang['media_comments_edit_header']		= "編輯媒體評論"; 

	$mgrlang['media_comments_edit_message']		= "編輯評論訪問者所張貼，然後點擊“保存更改”按鈕.";  

	$mgrlang['media_comments_tab1']			= "詳細內容"; 

	$mgrlang['media_comments_f_com']		= "評論"; 

	$mgrlang['media_comments_f_com_d']		= "有關上述媒體發佈的訪問者張貼評論."; 

	$mgrlang['media_comments_f_postd']		= "提交日期/時間"; 

	$mgrlang['media_comments_f_postd_d']		= "提交評論時的日期和時間."; 

	$mgrlang['media_comments_f_status']		= "狀態"; 

	$mgrlang['media_comments_f_status_d']		= "這個評論的狀態。只有經過批准的意見在公共網站上顯示.";   

	$mgrlang['media_comments_f_mem']		= "會員"; 

	$mgrlang['media_comments_f_mem_d']		= "會員提交評論.如果有人沒有一個帳戶或沒有登錄的訪客.";   

	$mgrlang['media_comments_f_med']		= "媒體"; 

	$mgrlang['media_comments_f_med_d']		= "媒體評論，這留給."; 

	

	# MEDIA RATINGS

	$mgrlang['media_ratings_t_rating']		= "評級"; 

	$mgrlang['media_ratings_edit_header']		= "編輯媒體評級"; 

	$mgrlang['media_ratings_edit_message']		= "編輯該評級訪問者來自，然後點擊“保存更改”按鈕.";  

	$mgrlang['media_ratings_f_rating']		= "評級"; 

	$mgrlang['media_ratings_f_rating_d']		= "提交的訪客或會員的評級."; 

	$mgrlang['media_ratings_tab1']			= "詳細資訊"; 

	$mgrlang['media_ratings_f_postd']		= "提交日期/時間"; 

	$mgrlang['media_ratings_f_postd_d']		= "評級被提交時的日期和時間."; 

	$mgrlang['media_ratings_f_status']		= "狀態"; 

	$mgrlang['media_ratings_f_status_d']		= "這個等級的狀態。只有經過批准的評分的公共網站上顯示.";    

	$mgrlang['media_ratings_f_mem']			= "會員"; 

	$mgrlang['media_ratings_f_mem_d']		= "會員提交了該評級.如果有人沒有一個帳戶或沒有登錄的訪客.";    

	$mgrlang['media_ratings_f_med']			= "媒體"; 

	$mgrlang['media_ratings_f_med_d']		= "媒體評論，這留給";

	

	# MEDIA TAGS

	$mgrlang['media_tags_t_tag']			= "標籤"; 

	$mgrlang['media_tags_edit_header']		= "編輯媒體標籤"; 

	$mgrlang['media_tags_edit_message']		= "編輯訪問者來自這個標籤，然後點擊“保存更改”按鈕.";  

	$mgrlang['media_tags_tab1']			= "詳細資訊"; 

	$mgrlang['media_tags_f_tag']			= "標籤"; 

	$mgrlang['media_tags_f_tag_d']			= "上面貼標籤資訊來自訪客的媒體."; 

	$mgrlang['media_tags_f_postd']			= "提交日期/時間"; 

	$mgrlang['media_tags_f_postd_d']		= "標籤被提交時的日期和時間."; 

	$mgrlang['media_tags_f_status']			= "狀態"; 

	$mgrlang['media_tags_f_status_d']		= "這個標籤的狀態。只有經過批准的標籤將顯示在公共網站.";  

	$mgrlang['media_tags_f_mem']			= "會員"; 

	$mgrlang['media_tags_f_mem_d']			= "會員提交這個標籤.如果有人沒有一個帳戶或沒有登錄的訪客.";    

	$mgrlang['media_tags_f_med']			= "媒體"; 

	$mgrlang['media_tags_f_med_d']			= "媒體離開這個標記";

	

	# BILLINGS

	$mgrlang['billings_new_header']			= "創建新帳單"; 

	$mgrlang['billings_edit_header']		= "編輯帳單"; 

	$mgrlang['billings_new_message']		= "使用以下表格來創建一個新帳單的會員."; 

	$mgrlang['billings_edit_message']		= "編輯帳單，然後點擊“保存更改”按鈕."; 

	$mgrlang['billings_f_method']			= "方法"; 

	$mgrlang['billings_f_method_d']			= "您想如何創造這個帳單"; 

	$mgrlang['billings_f_member']			= "給會員"; 

	$mgrlang['billings_no_bill_later']		= "有沒有條例草案在這個時候我後來的訂單";

	$mgrlang['billings_bill_date']			= "帳單日期"; 

	$mgrlang['billings_display']			= "與這些狀態顯示帳單"; 

	$mgrlang['billings_invnum_bill']		= "本條例草案的發票編號";

	$mgrlang['billings_for_mem']			= "給會員"; 

	$mgrlang['billings_f_items_d']			= "這個草案中所包含的項目";

	$mgrlang['billings_f_orders']			= "訂單";

	$mgrlang['billings_f_orders_d']			= "給我包含在這個草案的結帳單";

	$mgrlang['billings_f_status']			= "狀態"; 

	$mgrlang['billings_f_status_d']			= "本條例草案的付款狀態";

	$mgrlang['billings_f_bill_date_d']		= "帳單的創建日期."; 

	$mgrlang['billings_f_due_date']			= "截止日期"; 

	$mgrlang['billings_f_due_date_d']		= "結帳單的到期日";

	$mgrlang['billings_f_groups']			= "帳單組"; 

	

	# PAYMENT GATEWAYS

	$mgrlang['pmntgate_edit_header']		= "使用在您的網站上設置的支付閘道."; 	

	$mgrlang['pmntgate_actgate']			= "啟動閘道"; 

	

	# MEMBERSHIPS

	$mgrlang['membership']					= "會員籍"; 

	$mgrlang['membership_tab1']				= "計畫詳情"; 

	$mgrlang['membership_tab2']				= "定價"; 

	$mgrlang['membership_tab3']				= "統計資料";

	$mgrlang['membership_tab4']				= "固打制";

	$mgrlang['membership_tab5']				= "選項";

	$mgrlang['membership_tab6']				= "群組"; 

	$mgrlang['membership_t_sortorder']		= "訂購"; 

	$mgrlang['membership_new_header']		= "添加新會員籍計畫."; 

	$mgrlang['membership_edit_header']		= "編輯會員籍計畫"; 

	$mgrlang['membership_new_message']		= "使用以下表格來創建一個新的會員計畫."; 

	$mgrlang['membership_edit_message']		= "編輯本會員計畫，然後點擊“保存更改”按鈕."; 

	$mgrlang['membership_f_name']			= "計畫名稱"; 

	$mgrlang['membership_f_name_d']			= "會員籍計畫名稱."; 

	$mgrlang['membership_f_groups']			= "群組計畫"; 

	$mgrlang['membership_f_groups_d']		= "選擇的會員團體，這個計畫屬於."; 

	$mgrlang['membership_f_flag']			= "圖示"; 

	$mgrlang['membership_f_flag_d']			= "圖示來識別本會員在管理領域."; 

	$mgrlang['membership_f_directlink']		= "直接連結"; 

	$mgrlang['membership_f_directlink_d']		= "直接連結到本會員註冊."; 

	$mgrlang['membership_f_description']		= "描述"; 

	$mgrlang['membership_f_description_d']		= "本會員計畫的說明."; 

	$mgrlang['membership_default']			= "預設會員"; 

	$mgrlang['membership_f_active']			= "活躍"; 

	$mgrlang['membership_f_active_d']		= "這個會員計畫設置活躍或不活躍。處於非活躍狀態區塊新成員簽署了這個計畫，但不影響目前的會員.";  

	$mgrlang['membership_f_ms_type']		= "會員類型";

	$mgrlang['membership_f_ms_type_d']		= "這是選擇成員的類型. 免費, 一次性或定期.";

	$mgrlang['membership_f_ms_type_op1']		= "免費"; ;

	$mgrlang['membership_f_ms_type_op2']		= "同一時間"; 

	$mgrlang['membership_f_ms_type_op3']		= "經常性"; 

	$mgrlang['membership_f_trial']			= "試用期"; 

	$mgrlang['membership_f_trial_d']		= "允許此會員試用期."; 

	$mgrlang['membership_f_op1']			= "無"; 

	$mgrlang['membership_f_op2']			= "試用期"; 

	$mgrlang['membership_f_op3']			= "每天"; 

	$mgrlang['membership_f_op4']			= "每週"; 

	$mgrlang['membership_f_op5']			= "每月"; 

	$mgrlang['membership_f_op6']			= "每年"; 

	$mgrlang['membership_f_cost']			= "成本";

	$mgrlang['membership_f_cost_d']			= "成本訂閱本會員。保留為空白或輸入0，沒有安裝費."; 

	$mgrlang['membership_f_cost_h1']		= "價格"; 

	$mgrlang['membership_f_cost_h2']		= "期間"; 

	$mgrlang['membership_f_cost_op1']		= "每週"; 

	$mgrlang['membership_f_cost_op2']		= "每月"; 

	$mgrlang['membership_f_cost_op3']		= "每季的"; 

	$mgrlang['membership_f_cost_op4']		= "半年"; 

	$mgrlang['membership_f_cost_op5']		= "每年"; 

	$mgrlang['membership_f_ms_mems']		= "會員籍會員"; 

	$mgrlang['membership_f_ms_mems_d']		= "此會員計畫"; 

	$mgrlang['membership_f_showmem']		= "顯示會員"; 

	$mgrlang['membership_f_bio']			= "傳記"; 

	$mgrlang['membership_f_bio_d']			= "允許對自己的成員在此添加一個簡短的傳記."; 

	$mgrlang['membership_manual']			= "手動批准"; 

	$mgrlang['membership_auto']			= "自動批准"; 

	$mgrlang['membership_f_avatar']			= "頭像";  

	$mgrlang['membership_f_avatar_d']		= "允許在此會員上傳頭像代表自己。然後，選擇是否需要由管理員批准的頭像顯示在您的網站上.";    

	$mgrlang['membership_f_lightboxes']		= "我的最愛"; 

	$mgrlang['membership_f_lightboxes_d']		= "允許該會員計畫的成員可以創建我的最愛."; 

	$mgrlang['membership_f_comments']		= "評論"; 

	$mgrlang['membership_f_comments_d']		= "允許該會員計畫的會員要發表評論，照片和其他媒體上。然後選擇評論需要管理員批准之前，它會顯示在您的網站上.";   

	$mgrlang['membership_f_rating']			= "媒體評價"; 

	$mgrlang['membership_f_rating_d']		= "此會員計畫，允許會員利率照片和其他媒體。如果評估需要由管理員批准之前，它會顯示在您的網站上然後選擇.";  

	$mgrlang['membership_f_tagging_d']		= "允許該會員計畫的會員添加標籤的照片和其他媒體。然後選擇是否顯示在您的網站上之前，需要由管理員批准的標籤.";    

	$mgrlang['membership_f_memgroups']		= "會員群組"; 

	$mgrlang['membership_f_memgroups_d']		= "當登錄到本會員自動成為這些群體的一部分.";   

	$mgrlang['membership_f_coupons']		= "優惠券"; 

	$mgrlang['membership_f_coupons_d']		= "這些優惠券給會員或折扣優惠訂閱此會員計畫.";  

	$mgrlang['membership_f_galsuggest']		= "允許圖庫的建議"; 

	$mgrlang['membership_f_galsuggest_d']		= "允許會員屬於該會員計畫建議無以復加的圖庫.";  

	$mgrlang['membership_f_medrequest']		= "請求圖片/媒體"; 

	$mgrlang['membership_f_medrequest_d']		= "允許屬於該會員計畫到無以復加的要求照片或其他媒體.";  

	$mgrlang['membership_f_featured']		= "投稿者名單"; 

	$mgrlang['membership_f_featured_d']		= "列出會員作為貢獻者任何貢獻者頁上的這個會員計畫.";  

	$mgrlang['membership_f_auploads']		= "允許上傳"; 

	$mgrlang['membership_f_auploads_d']		= "允許會員計畫會員在此上傳照片和其他媒體.";  

	$mgrlang['membership_f_diskspace']		= "磁碟空間"; 

	$mgrlang['membership_f_diskspace_d']		= "磁碟空間量，讓這個會員使用他們的照片.";  

	$mgrlang['membership_f_admingal']		= "管理員圖庫"; 

	$mgrlang['membership_f_admingal_d']		= "允許該會員計畫的會員上傳到您所創建的圖庫.";  

	$mgrlang['membership_f_allowedit']		= "允許編輯"; 

	$mgrlang['membership_f_allowedit_d']		= "允許此會員計畫編輯自己的照片或媒體詳細。然後選擇的編輯應該由管理員批准之前公佈在網站上.";      

	$mgrlang['membership_f_allowdel']		= "允許刪除"; 

	$mgrlang['membership_f_allowdel_d']		= "允許該會員計畫的成員刪除自己的照片或媒體。然後選擇管理員應該檢討的照片或媒體刪除.";  

	$mgrlang['membership_f_acfilesizes']		= "接受檔大小"; 

	$mgrlang['membership_f_acfilesizes_d']		= "照片和其他媒體，會員可以上傳的檔大小.";   

	$mgrlang['membership_f_min']			= "最小"; 

	$mgrlang['membership_f_mb']			= "mb";

	$mgrlang['membership_f_px']			= "px";

	$mgrlang['membership_f_max']			= "最大"; 

	$mgrlang['membership_f_res']			= "接受的解析度"; 

	$mgrlang['membership_f_res_d']			= "解析度的照片，會員可以上傳."; 

	$mgrlang['membership_f_aft']			= "接受的檔案類型"; 

	$mgrlang['membership_f_aft_d']			= "此會員計畫的會員可以上傳的檔案類型.";  

	$mgrlang['membership_f_portfolio']		= "Portfolio作品";

	$mgrlang['membership_f_portfolio_d']		= "許此會員計畫建立一個投資組合，以展示自己的作品";

	$mgrlang['membership_f_allowsell']		= "允許銷售"; 

	$mgrlang['membership_f_allowsell_d']		= "允許此會員計畫出售照片和其他媒體."; 

	$mgrlang['membership_f_commission']		= "傭金級別"; 

	$mgrlang['membership_f_commission_d']		= "此會員計畫會員賺取的傭金的百分比級別.";  

	$mgrlang['membership_f_commission_op1']		= "沒有"; 

	$mgrlang['membership_f_commission_op2']		= "固定費用"; 

	$mgrlang['membership_f_commission_op3']		= "百分比"; 

	$mgrlang['membership_f_aprod']			= "產品"; 

	$mgrlang['membership_f_aprod_d']		= "允許會員計畫會員在此選擇產品出售.";  

	$mgrlang['membership_f_sdf']			= "銷售原本的數位版"; 

	$mgrlang['membership_f_sdf_d']			= "允許此會員計畫的會員出售他們的原本的數位檔.";  

	$mgrlang['membership_f_aprints']		= "列印"; 

	$mgrlang['membership_f_aprints_d']		= "允許會員計畫會員在此選擇列印出售.";  

	$mgrlang['membership_f_asizes']			= "數位檔案"; 

	$mgrlang['membership_f_asizes_d']		= "允許該會員計畫會員選擇從數位檔案銷售.";  

	$mgrlang['membership_f_collect']		= "收藏創建"; 

	$mgrlang['membership_f_collect_d']		= "允許會員計畫會員在此創建集合他們的照片和其他媒體出售.";   

	$mgrlang['membership_f_setupfee']		= "安裝費"; 

	$mgrlang['membership_f_approval']		= "批准"; 

	$mgrlang['membership_f_approval_d']		= "選擇您想批准的照片，媒體，其設置和產品選擇時的貢獻者上傳一些新的東西.這也適用於他們之後的變化.";   

	$mgrlang['membership_f_iis']			= "包括在搜尋"; 

	$mgrlang['membership_f_iis_d']			= "任何媒體上傳由該會員計畫的會員包括訪問者搜尋.";  	

	$mgrlang['membership_f_pricing']		= "定價"; 

	$mgrlang['membership_f_pricing_d']		= "選擇誰轉讓人指定的價格?";  

	

	

	

	# TOOLS LINKS

	$mgrlang['tl_new_header']				= "添加新連結"; 

	$mgrlang['tl_edit_header']				= "編輯連結";   

	$mgrlang['tl_new_message']				= "通過以下表格，您可以添加一個新的連結到你的工具和連結下拉清單.";  

	$mgrlang['tl_edit_message']				= "編輯這個連結，並點擊“保存更改”按鈕."; 

	$mgrlang['tl_tab1']					= "連結詳細資訊"; 

	$mgrlang['tl_f_name']					= "連結名稱"; 

	$mgrlang['tl_f_name_d']					= "連結名稱顯示在下拉清單中的工具與連結";

	$mgrlang['tl_f_url']					= "連結"; 

	$mgrlang['tl_f_url_d']					= "你希望連結到網站或網頁. 包括 http:// 如果它是一個外部的連結.";

	$mgrlang['tl_f_owner']					= "每一個"; 

	$mgrlang['tl_f_owner_d']				= "顯示該連結予所有管理員. 如果沒勾選，它只會顯示在此帳戶登錄時，";	

	$mgrlang['tl_f_target']					= "目標視窗"; 

	$mgrlang['tl_f_target_d']				= "你想要的連結，打開的視窗";

	$mgrlang['tl_newwindow']				= "新窗口"; 

	$mgrlang['tl_thiswindow']				= "此窗口"; 



	# MEMBERS

	$mgrlang['members_new_header']				= "添加新會員"; 

	$mgrlang['members_edit_header']				= "編輯會員"; 

	$mgrlang['members_new_message']				= "通過以下表格，您可以添加一個新成員."; 

	$mgrlang['members_edit_message']			= "編輯會員，然後按一下“保存更改”按鈕."; 

	$mgrlang['mem_tab1']					= "總則";

	$mgrlang['mem_tab2']					= "會員籍"; 

	$mgrlang['mem_tab3']					= "訂購"; 

	$mgrlang['mem_tab4']					= "我的最愛"; 

	$mgrlang['mem_tab5']					= "評級"; 

	$mgrlang['mem_tab6']					= "門票";

	$mgrlang['mem_tab7']					= "行情"; 

	$mgrlang['mem_tab8']					= "上載"; 

	$mgrlang['mem_tab9']					= "活動日誌"; 

	$mgrlang['mem_tab10']					= "群組"; 

	$mgrlang['mem_tab11']					= "進階"; 

	$mgrlang['mem_tab12']					= "統計資料"; 

	$mgrlang['mem_tab13']					= "評論"; 

	$mgrlang['mem_tab14']					= "地址"; 

	$mgrlang['mem_tab15']					= "標記"; 

	$mgrlang['mem_tab16']					= "傳記"; 

	$mgrlang['mem_f_groups']				= "會員群組"; 

	$mgrlang['mem_f_groups_d']				= "選擇該會員所屬的群組。會員可以屬於無限組.";   

	$mgrlang['mem_never']					= "從來沒有"; 

	$mgrlang['mem_last_login']				= "最後登入"; 

	$mgrlang['mem_member_num']				= "會員編號"; 

	$mgrlang['mem_signup_date']				= "註冊日期"; 

	$mgrlang['mem_unique_id']				= "獨特的編號"; 

	$mgrlang['mem_f_signupdate']				= "註冊日期/時間"; 

	$mgrlang['mem_f_signupdate_d']				= "會員創建(GMT)日期和時間.";  

	$mgrlang['mem_f_fname']					= "名字"; 

	$mgrlang['mem_f_fname_d']				= "會員的名字"; 

	$mgrlang['mem_f_lname']					= "姓名"; 

	$mgrlang['mem_name']					= "名稱"; 

	$mgrlang['mem_f_lname_d']				= "會員姓名"; 

	$mgrlang['mem_f_email']					= "電子郵件"; 

	$mgrlang['mem_f_email_d']				= "會員的電子郵件"; 

	$mgrlang['mem_f_email_error']				= "這電子郵寄地址已經可以使用."; 

	$mgrlang['mem_f_password']				= "密碼"; 

	$mgrlang['mem_f_password_d']				= "會員的密碼"; 

	$mgrlang['mem_f_company_name']				= "公司名稱"; 

	$mgrlang['mem_f_company_name_d']			= "會員的公司名稱."; 

	$mgrlang['mem_f_website']				= "網站"; 

	$mgrlang['mem_f_website_d']				= "會員的網站 URL."; 

	$mgrlang['mem_f_address']				= "地址"; 

	$mgrlang['mem_f_address_d']				= "會員的地址."; 

	$mgrlang['mem_f_city']					= "城市"; 

	$mgrlang['mem_f_city_d']				= "會員的城市."; 

	$mgrlang['mem_f_state']					= "州/省"; 

	$mgrlang['mem_f_state_d']				= "會員的州/省."; 

	$mgrlang['mem_f_country']				= "國家"; 

	$mgrlang['mem_f_country_d']				= "會員的國家"; 

	$mgrlang['mem_f_zip']					= "郵編/郵遞區號"; 

	$mgrlang['mem_f_zip_d']					= "會員的郵遞區號或郵編."; 

	$mgrlang['mem_f_status']				= "狀態"; 

	$mgrlang['mem_f_status_d']				= "設置為活躍的會員。不設置為活躍的會員將無法登錄."; 

	$mgrlang['mem_f_phone']					= "電話號碼"; 

	$mgrlang['mem_f_phone_d']				= "會員的電話號碼."; 

	$mgrlang['mem_f_notes']					= "筆記"; 

	$mgrlang['mem_f_notes_d']				= "有關此會員的注意事項。只在管理區中看得到."; 

	$mgrlang['mem_f_membership']				= "會員籍"; 

	$mgrlang['mem_f_membership_d']				= "會員籍是啟動的.會員可以在同一時間只有一個會員計畫.";

	$mgrlang['mem_f_email_pass']				= "電子郵件密碼"; 

	$mgrlang['mem_block_list']				= "添加到阻止列表";

	$mgrlang['mem_f_visit']					= "訪問網站"; 

	$mgrlang['mem_country_warn']				= "這個國家有沒有分配給它的州/省. 您可以加入在這些下面:";

	$mgrlang['mem_choose_country']				= "請選擇一個國家!";

	$mgrlang['mem_f_signup_ip']				= "註冊IP地址";

	$mgrlang['mem_f_signup_ip_d']				= "IP位址時記錄的成員簽署了";

	$mgrlang['mem_f_login_ip']				= "最後登入IP位址";

	$mgrlang['mem_f_login_ip_d']				= "IP位址時記錄的成員最近一次登錄是在";

	$mgrlang['mem_f_referrer']				= "引薦";

	$mgrlang['mem_f_referrer_d']				= "引薦網站時記錄的成員簽署了";

	$mgrlang['mem_f_avatar']				= "頭像"; 

	$mgrlang['mem_f_avatar_d']				= "會員頭像或標誌."; 

	$mgrlang['mem_del_avatar']				= "你確定你想刪除此頭像?";

	$mgrlang['mem_unknown']					= "沒有/未知"; 

	$mgrlang['mem_f_paypal']				= "PayPal電子郵寄地址"; 

	$mgrlang['mem_f_paypal_d']				= "會員PayPal電子郵寄地址。用於發送支付傭金."; 

	$mgrlang['mem_f_compref']				= "傭金支付方法"; 

	$mgrlang['mem_f_compref_d']				= "成員偏好如何，他們想支付的傭金支付.";

	$mgrlang['mem_f_paypal_email']				= "PayPal電子郵寄地址"; 

	$mgrlang['mem_mes_01']					= "不收取傭金的付款選項打開. 為了把這些啟動";

	$mgrlang['mem_mes_02']					= "此會員任何媒體沒有評分."; 

	$mgrlang['mem_mes_03']					= "此會員並沒有標記任何媒體.";

	$mgrlang['mem_mes_04']					= "此會員並沒有在任何媒體上發表評論."; 

	$mgrlang['mem_mes_05']					= "不支持的門票已經開此帳戶下";

	$mgrlang['mem_orig']					= "原本"; 

	$mgrlang['mem_mes_06']					= "在此帳戶下沒有下載."; 

	$mgrlang['mem_mes_07']					= "在此帳戶下沒有訂閱."; 

	$mgrlang['mem_mes_08']					= "沒有訂單已放置在該帳戶下."; 

	$mgrlang['mem_mes_09']					= "我的最愛已在此帳戶下創建."; 

	$mgrlang['mem_new_ticket']				= "新門票";

	$mgrlang['mem_quotes']					= "行情"; 

	$mgrlang['mem_comlevel']				= "傭金級別"; 

	$mgrlang['mem_comlevelms']				= "使用會員傭金級別"; 

	$mgrlang['mem_comlevelcus']				= "使用自訂傭金級別"; 

	$mgrlang['mem_f_billlater']				= "允許之後開帳單給我"; 

	$mgrlang['mem_f_billlater_d']				= "讓您的會員選擇登出時才收結帳單";

	$mgrlang['mem_f_crdits_d']				= "該會員的積分數.";

	$mgrlang['mem_add_sub']					= "新增訂閱"; 

	$mgrlang['mem_f_bio_updated']				= "傳記更新"; 

	$mgrlang['mem_f_bio_updated_d']				= "BIO日期然後更新該會員";	

	$mgrlang['mem_f_bio']					= "傳記"; 

	$mgrlang['mem_f_bio_d']					= "傳記進入該會員"; 

	$mgrlang['mem_f_status']				= "狀態"; 

	$mgrlang['mem_f_status_d']				= "選中此核取方塊以顯示在網站上批准這種BIO";

	$mgrlang['mem_new_bill']				= "新帳單"; 

	$mgrlang['mem_show_only']				= "只顯示這些會員計畫";

	$mgrlang['mem_edit_ms']					= "編輯會員籍"; 

	$mgrlang['mem_table_headers']				= "這些表HEADER";

	$mgrlang['mem_f_ftm']					= "功能該會員";

	$mgrlang['mem_f_ftm_d']					= "允許這個成員是功能上的網頁和貢獻者頁面";

	

	# DOWNLOAD HISTORY

	$mgrlang['mem_download_unknown']		= "未知"; 

	$mgrlang['mem_download_free']			= "免費下載"; 

	$mgrlang['mem_download_sub']			= "訂購"; 

	$mgrlang['mem_download_order']			= "訂單"; 

	$mgrlang['mem_download_credits']		= "積分"; 

	$mgrlang['mem_download_prev']			= "之前下載"; 

	

	

	# ORDERS

	$mgrlang['order_tab1']					= "訂單詳情"; 

	$mgrlang['order_tab2']					= "項目"; 

	$mgrlang['order_tab3']					= "發票詳情"; 

	$mgrlang['order_tab4']					= "交易"; 

	$mgrlang['order_t_ordernum']				= "訂單號碼";

	$mgrlang['order_t_payment']				= "付款"; 

	$mgrlang['order_t_shipping']				= "運送狀態"; 

	$mgrlang['order_t_status']				= "訂購狀態"; 

	$mgrlang['order_t_total']				= "總數"; 

	$mgrlang['order_t_invoicenum']				= "發票號碼"; 

	$mgrlang['order_new_header']				= "添加新訂單"; 

	$mgrlang['order_edit_header']				= "查看訂購";  

	$mgrlang['order_new_message']				= "通過以下表格，您可以添加一個新的訂單."; 

	$mgrlang['order_edit_message']				= "查看這個命令的詳細資訊，若根據需要編輯它";

	$mgrlang['order_f_invoice']				= "發票編號"; 

	$mgrlang['order_f_invoice_d']				= "發票號碼分配給這個訂單";

	$mgrlang['order_f_ordernum']				= "訂單編號"; 

	$mgrlang['order_f_ordernum_d']				= "唯一的訂單號分配給這個訂單";

	$mgrlang['order_reset_dls']				= "重新設定下載"; 

	$mgrlang['order_reset_exp']				= "重新設定截止"; 

	$mgrlang['order_del_oi']				= "刪除訂單物品"; 

	$mgrlang['order_update_ss']				= "更新物品貨運狀況"; 

	$mgrlang['order_track_num']				= "追蹤號碼"; 

	$mgrlang['order_dis_statuses']				= "顯示訂單的狀態"; 

	$mgrlang['order_dis_data']				= "顯示這個表中的資料";

	$mgrlang['order_f_status']				= "訂單";"訂單狀態";

	$mgrlang['order_f_status_d']				= "目前此訂單的狀態."; 

	$mgrlang['order_f_datet']				= "訂單日期/時間"; 

	$mgrlang['order_f_datet_d']				= "這個訂單是日期和時間 (GMT).";

	$mgrlang['order_f_customer']				= "顧客"; 

	$mgrlang['order_f_adminnotes']				= "管理員注意事項"; 

	$mgrlang['order_f_adminnotes_d']			= "該訂單內部講義";	

	$mgrlang['order_f_memnotes']				= "注釋若要會員";

	$mgrlang['order_f_memnotes_d']				= "這些說明將被張貼在該訂單的訂單詳細資訊頁面.";   	

	$mgrlang['order_f_ogroups']				= "訂單組";

	$mgrlang['order_f_paystat']				= "付款狀態"; 

	$mgrlang['order_f_paystat_d']				= "此訂單的付款狀況。建議你不船舶訂單不批准的付款狀態";	

	$mgrlang['order_f_paydt']				= "付款日期/時間"; 

	$mgrlang['order_f_paydt_d']				= "此發票支付的日期和時間(GMT)."; 

	$mgrlang['order_f_coupuse']				= "優惠券使用"; 

	$mgrlang['order_f_coupuse_d']				= "使用優惠券購買的編號."; 

	$mgrlang['order_f_curuse']				= "使用貨幣"; 

	$mgrlang['order_f_curuse_d']				= "由顧客選擇的貨幣，在結帳時的編號.";   

	$mgrlang['order_f_pubord']				= "公眾訂單詳細資訊頁面連結"; 

	$mgrlang['order_f_pubilin']				= "公共發票連結"; 

	$mgrlang['order_f_oshipstat']				= "訂單發貨狀態"; 

	$mgrlang['order_f_oshipstat_d']				= "在這整個訂單的整體狀態航運."; 

	$mgrlang['order_f_shipto']				= "送貨地址"; 

	$mgrlang['order_f_shipto_d']				= "該位址的運費由客戶提供."; 

	$mgrlang['order_f_shipmeth']				= "貨運方式"; 

	$mgrlang['order_f_shipmeth_d']				= "運輸方式由客戶選擇."; 

	$mgrlang['order_f_pback']				= "回傳變數"; 

	$mgrlang['order_f_pback_d']				= "從支付閘道的變數被送回";

	

	# PRODUCTS

	$mgrlang['products_new_header']			= "增加新產品"; 

	$mgrlang['products_edit_header']		= "更改產品"; 

	$mgrlang['products_new_message']		= "通過以下表格，您可以添加一個新的產品.";  

	$mgrlang['products_edit_message']		= "編輯本產品，然後點擊“保存更改”按鈕.";  

	$mgrlang['products_f_name']				= "產品名字"; 

	$mgrlang['products_f_name_d']			= "輸入這個產品的名稱."; 

	$mgrlang['products_f_price']			= "產品價格"; 

	$mgrlang['products_f_price_d']			= "產品價格"; 

	$mgrlang['products_f_my_cost']			= "我的價格"; 

	$mgrlang['products_f_my_cost_d']		= "我此項目的成本.用於計算利潤.";  

	$mgrlang['products_f_credits']			= "積分";  

	$mgrlang['products_f_credits_d']		= "積分成本購買此產品."; 

	$mgrlang['products_f_perm']				= "許可權"; 

	$mgrlang['products_f_perm_d']			= "選擇會員，會員組會員可以看到這個項目.";   

	$mgrlang['products_f_taxable']			= "應納稅"; 

	$mgrlang['products_f_taxable_d']		= "如果這個項目徵稅."; 	

	$mgrlang['products_f_multiple']			= "倍數"; 

	$mgrlang['products_f_multiple_d']		= "讓客戶購買這些專案超過1項."; 

	$mgrlang['products_f_discount']			= "折扣"; 

	$mgrlang['products_f_discount_d']		= "價格為每個項目後的第一次。留為空白，以保持相同的價格作為第一項.";   

	$mgrlang['products_f_groups']			= "產品組"; 

	$mgrlang['products_f_groups_d']			= "這屬於選擇產品組.";  

	$mgrlang['products_f_top']				= "產品類型"; 

	$mgrlang['products_f_groups_d']			= "選擇如果這是一個媒體為基礎的產品或一個獨立的產品.";  

	$mgrlang['products_f_medbased']			= "基於媒體的產品"; 

	$mgrlang['products_f_standalone']		= "獨立的產品"; 

	$mgrlang['products_f_adv_d']			= "廣告產品的特色產品頁面."; 

	$mgrlang['products_f_attach']			= "附件"; 

	$mgrlang['products_f_attach_d']			= "在哪裡銷售這款產品的報價。此選項將覆蓋從個人媒體選擇.";  

	

	# COLLECTIONS

	$mgrlang['collections_new_header']		= "添加新收集"; 

	$mgrlang['collections_edit_header']		= "編輯收集"; 

	$mgrlang['collections_new_message']		= "以下表格，您可以添加一個新的收集."; 

	$mgrlang['collections_edit_message']		= "編輯此收集，然後按一下“保存更改”按鈕."; 

	$mgrlang['collections_f_name']			= "收集名稱"; 

	$mgrlang['collections_f_name_d']		= "輸入此收集的的名稱."; 

	$mgrlang['collections_f_price']			= "收集的價格"; 

	$mgrlang['collections_f_price_d']		= "收集的價格."; 

	$mgrlang['collections_f_credits']		= "積分"; 

	$mgrlang['collections_f_credits_d']		= "積分成本來購買本收集."; 

	$mgrlang['collections_f_weight']		= "產品重量"; 

	$mgrlang['collections_f_weight_d']		= "此產品重量.如果需要的話，用於計算運費.";  

	$mgrlang['collections_f_perm']			= "許可權"; 

	$mgrlang['collections_f_perm_d']		= "選擇會員，會員組會員可以看到這個項目.";  

	$mgrlang['collections_f_taxable']		= "應納稅"; 

	$mgrlang['collections_f_taxable_d']		= "應否這個項目徵稅."; 

	$mgrlang['collections_f_groups']		= "收集群組"; 

	$mgrlang['collections_f_groups_d']		= "這屬於選擇收集群組.";  

	$mgrlang['collections_f_active_d']		= "設置活躍此收藏集的。將不會顯示在網站上暫無收藏.";   

	$mgrlang['collections_f_toc']			= "收集的類型"; 

	$mgrlang['collections_f_toc_d']			= "從現有的畫廊或創建此收集，它使個別媒體可以被添加到收集後";    

	$mgrlang['collections_cfg']			= "從圖庫的創建珍藏"; 

	$mgrlang['collections_cfim']			= "從個人媒體創建珍藏";  

	$mgrlang['collections_media_added']		= "媒體可以從這個集合中移除的媒體庫>媒體或媒體導入到庫中.";     

	$mgrlang['collections_choose_gal']		= "選擇在這個集合中包括哪些圖庫。在這些圖庫將包括所有的媒體珍藏中，無論任何圖庫許可權或設置。同時，任何未來的媒體加入到這個珍藏或圖庫內，這將是提供給任何人誰曾購買此珍藏.";          

	$mgrlang['collections_adv_d']			= "廣告這個珍藏的功能珍藏頁.";   

	

	

	# SUBSCRIPTIONS

	$mgrlang['subscription_new_header']		= "添加訂閱"; 

	$mgrlang['subscription_edit_header']		= "編輯訂閱"; 

	$mgrlang['subscription_new_message']		= "通過以下表格，您可以添加新的訂閱."; 

	$mgrlang['subscription_edit_message']		= "編輯訂閱，然後點擊“保存更改”按鈕."; 

	$mgrlang['subscription_tab1']			= "詳細資訊"; 

	$mgrlang['subscription_tab2']			= "群組"; 

	$mgrlang['subscription_f_name']			= "訂閱名稱"; 

	$mgrlang['subscription_f_name_d']		= "訂閱名稱.";  	

	$mgrlang['subscription_f_groups']		= "訂閱群組"; 

	$mgrlang['subscription_f_groups_d']		= "選擇訂閱所屬的群體.";  

	$mgrlang['subscription_f_durvalue']		= "為期"; 

	$mgrlang['subscription_f_durvalue_d']		= "本次認購的時間長度是一次購買";	

	$mgrlang['subscription_f_price']		= "價格"; 

	$mgrlang['subscription_f_price_d']		= "訂閱價格."; 	

	$mgrlang['subscription_f_credits']		= "積分"; 

	$mgrlang['subscription_f_credits_d']		= "訂閱價格."; 	

	$mgrlang['subscription_f_dpd']			= "今日下載"; 

	$mgrlang['subscription_f_dpd_d']		= "下載會員的數量與此訂閱，每天可以下載。讓空白的無限";

	$mgrlang['subscription_adv_fp_d']		= "認購特色的訂閱頁面做廣告.";   

	

	$mgrlang['subscription_f_downi']		= "下載物品"; 

	$mgrlang['subscription_f_downi_d']		= "可以下載本次認購的項目."; 

	

	# CREDITS

	$mgrlang['credits_new_header']			= "添加積分配套"; 

	$mgrlang['credits_edit_header']			= "編輯積分配套"; 

	$mgrlang['credits_new_message']			= "通過以下表格，您可以添加一個新的積分配套."; 

	$mgrlang['credits_edit_message']		= "編輯此積分配套，然後按一下“保存更改”按鈕."; 

	$mgrlang['credits_tab1']				= "詳細資訊"; 

	$mgrlang['credits_tab2']				= "群組"; 

	$mgrlang['credits_f_name']				= "積分配套名稱"; 

	$mgrlang['credits_f_name_d']			= "積分配套名稱."; 	

	$mgrlang['credits_f_groups']			= "積分配套群組"; 

	$mgrlang['credits_f_groups_d']			= "選擇組，這積分配套屬於."; 

	$mgrlang['credits_f_amount_d']			= "這數目積分包括這配套."; 	

	$mgrlang['credits_f_price']			= "價格"; 

	$mgrlang['credits_f_price_d']			= "積分的價格."; 

	$mgrlang['credits_f_advertise_d']		= "廣告這個功能學分的學分頁.";

	

	# PROMOTIONS

	$mgrlang['promotions_new_header']		= "增加促銷或固本"; 

	$mgrlang['promotions_edit_header']		= "更改促銷或固本"; 

	$mgrlang['promotions_new_message']		= "以下表格中，您可以創建新的促銷或優惠券.";  

	$mgrlang['promotions_edit_message']		= "編輯促銷或優惠券，然後點擊“保存更改”按鈕.";  

	$mgrlang['promotions_tab1']				= "詳細資訊"; 

	$mgrlang['promotions_tab2']				= "群組"; 	

	$mgrlang['promotions_f_name']			= "促銷/優惠券名稱"; 

	$mgrlang['promotions_f_name_d']			= "促銷或優惠券的名稱."; 

	$mgrlang['promotions_f_code']			= "促銷或固本號碼"; 

	$mgrlang['promotions_f_code_d']			= "輸入一個唯一的代碼，此優惠券或促銷活動.這將是客戶輸入的代碼當檢查出.不能有空格.在保存時，將全部轉換為大寫.";     

	$mgrlang['promotions_f_groups']			= "促銷/優惠券組"; 

	$mgrlang['promotions_f_groups_d']		= "選擇這個項目所屬的群.";  

	$mgrlang['promotions_lowestprice']		= "在顧客的購物車中價格最低的項目將成為免費項目.";  

	$mgrlang['promotions_buy']				= "買"; 

	$mgrlang['promotions_get']				= "得到"; 

	$mgrlang['promotions_free']				= "免費"; 

	$mgrlang['promotions_f_minpur']			= "最低購買"; 

	$mgrlang['promotions_f_minpur_d']		= "設定最低採購數額之前，此優惠券可應用於.不包括稅金和運費。沒有最低留空.";    	

	$mgrlang['promotions_f_quanre']			= "剩餘數量"; 

	$mgrlang['promotions_f_oneuse']			= "每位會員的一個使用"; 

	$mgrlang['promotions_f_oneuse_d']		= "這將限制中的成員只有一次使用優惠券.客人將無法使用此優惠券，而無需創建一個帳戶或登錄.";    

	$mgrlang['promotions_f_autoap']			= "自動應用"; 

	$mgrlang['promotions_f_autoap_d']		= "此優惠將被自動應用，而無需將其添加在一個促銷代碼的連結或類型.";    

	$mgrlang['promotions_f_type']			= "促銷/優惠券類型"; 

	$mgrlang['promotions_f_type_d']			= "您想這是選擇促銷或優惠券的類型."; 	

	$mgrlang['promotions_type_op1']			= "百分比關"; 

	$mgrlang['promotions_type_op2']			= "金額關閉";

	$mgrlang['promotions_type_op3']			= "無稅"; 

	$mgrlang['promotions_type_op4']			= "免費送貨"; 

	$mgrlang['promotions_type_op5']			= "大批折扣"; 

	$mgrlang['promotions_digitalfile']		= "數位檔案";  

	$mgrlang['promotions_directlink']		= "直接連接"; 

	$mgrlang['promotions_directlink_d']		= "在公共網站連結優惠券."; 

	$mgrlang['promotions_advoncart']		= "廣告在購物車頁面"; 

	$mgrlang['promotions_advoncart_d']		= "在購物車頁面顯示此促銷/優惠券.";  	

	$mgrlang['promotions_advonpp']			= "廣告在促銷頁"; 

	$mgrlang['promotions_advonpp_d']		= "顯示此促銷/優惠券的促銷活動頁面."; 

	

	

	# RSS FEEDS

	$mgrlang['rss_type1']					= "最新的"; 

	$mgrlang['rss_type2']					= "最流行的"; 

	$mgrlang['rss_type3']					= "隨機"; 

	$mgrlang['rss_type4']					= "網站的新聞專案"; 

	$mgrlang['rss_type1b']					= "最新的媒體"; 

	$mgrlang['rss_type2b']					= "最流行的媒體"; 

	$mgrlang['rss_type3b']					= "隨機媒體"; 

	$mgrlang['rss_type4b']					= "網站新聞"; 	

	$mgrlang['rss_mes_01']					= "媒體從"; 

	$mgrlang['rss_mes_02']					= "RSS內容成功刪除."; 

	$mgrlang['rss_mes_03']					= "RSS內容成功刪除."; 

	$mgrlang['rss_mes_04']					= "沒有RSS被選定為刪除."; 

	$mgrlang['rss_f_photos']				= "項目數量"; 

	$mgrlang['rss_f_photos_d']				= "顯示專案的RSS;提要數量."; 

	$mgrlang['rss_f_type']					= "RSS提要類型"; 

	$mgrlang['rss_f_type_d']				= "顯示RSS提要類型."; 	

	$mgrlang['rss_new_header']				= "添加RSS提要"; 

	$mgrlang['rss_edit_header']				= "編輯RSS提要"; 

	$mgrlang['rss_new_message']				= "下面的表格，您可以添加新的RSS提要到您的網站.";  

	$mgrlang['rss_edit_message']				= "編輯這個RSS提要，然後點擊“保存更改”按鈕."; 

	$mgrlang['rss_tab1']					= "提要詳情"; 

	$mgrlang['rss_f_category']				= "圖庫"; 

	$mgrlang['rss_f_category_d']				= "媒體庫顯示從."; 

	$mgrlang['rss_mes_05']					= "您已添加新的RSS提要."; 

	$mgrlang['rss_mes_06']					= "您的更改已保存."; 

	$mgrlang['rss_views']					= "意見"; 

	$mgrlang['rss_reset']					= "重新設定"; 

	$mgrlang['rss_allgal']					= "所有公共圖庫"; 	

	

	# SERVERS

	$mgrlang['servers_mes_02']				= "成功刪除伺服器設定檔."; 

	$mgrlang['servers_mes_03']				= "沒有選擇要刪除的伺服器設定檔."; 

	$mgrlang['servers_new_header']				= "添加新的檔案類型簡介"; 

	$mgrlang['servers_edit_header']				= "編輯伺服器設定檔"; 

	$mgrlang['servers_new_message']				= "下面的表格，您可以添加一個新的伺服器設定檔到您的網站.";  

	$mgrlang['servers_edit_message']			= "編輯此伺服器設定檔，然後點擊“保存更改”按鈕."; 

	$mgrlang['servers_tab1']				= "伺服器詳情"; 

	$mgrlang['servers_f_name']				= "伺服器名稱"; 

	$mgrlang['servers_f_name_d']				= "命名您想參考伺服器.例如Server1或MyServer.com.";  

	$mgrlang['servers_f_url']				= "完整的URL";"Full URL";

	$mgrlang['servers_f_url_d']				= "完整的URL伺服器.例如http://www.myserver.com or http://www.myserver.com/path.Full URL for the server.沒有多餘的(/).";   

	$mgrlang['servers_b_test']				= "測試"; 

	$mgrlang['servers_f_pass']				= "密碼關鍵字"; 

	$mgrlang['servers_f_pass_d']				= "被傳遞到遠端伺服器進行驗證的密碼.6-10個字元，不能有空格.";    

	$mgrlang['servers_mes_04']				= "請輸入一個有效的URL"; 

	$mgrlang['servers_mes_05']				= "已添加新的伺服器設定檔."; 

	$mgrlang['servers_mes_06']				= "您的更改已保存."; 

	

	# DIGITAL PROFILES

	$mgrlang['lookfeel_f_mpp']				= "每頁媒體"; 

	$mgrlang['lookfeel_f_mpp_d']				= "每頁顯示媒體檔的數量."; 

	$mgrlang['dsp_f_attach_d']				= "什麼媒體將這個數字的大小可以上市銷售.";

	$mgrlang['dsp_f_profiletype']				= "簡介類型"; 

	$mgrlang['dsp_f_profiletype_d']				= "選擇這個數字的大小設定檔是否是為影片，照片或其它.";

	$mgrlang['dsp_f_comtype']				= "傭金類型"; 

	$mgrlang['dsp_f_comtype_d']				= "選擇這個項目的百分比或金額的傭金.";  

	$mgrlang['dsp_new_header']				= "新增數位簡介"; 

	$mgrlang['dsp_edit_header']				= "編輯數位簡介"; 

	$mgrlang['dsp_new_message']				= "通過以下表格，您可以添加一個新的數位的檔案.";  

	$mgrlang['dsp_edit_message']				= "編輯這個方案，然後按一下“保存更改”按鈕."; 

	$mgrlang['dsp_f_name']					= "數位檔案名稱"; 

	$mgrlang['dsp_f_name_d']				= "輸入這個數位的的檔案檔的名稱.例如“大”或“網頁版”.";   

	$mgrlang['dsp_f_code']					= "項目代碼"; 

	$mgrlang['dsp_f_code_d']				= "這專案代碼大小"; 

	$mgrlang['dsp_f_price']					= "價格"; 

	$mgrlang['dsp_f_price_d']				= "這個項目的價格.這是起始價格，如果它是版權管理.";  

	$mgrlang['dsp_f_credits']				= "積分"; 

	$mgrlang['dsp_f_credits_d']				= "需要購買這種規模的學分數目.";

	$mgrlang['dsp_f_license']				= "執照"; 

	$mgrlang['dsp_f_license_d']				= "此設定檔的執照類型."; 

	$mgrlang['dsp_f_quantity']				= "可用數量"; 

	$mgrlang['dsp_f_comval']				= "傭金值"; 

	$mgrlang['dsp_f_comval_d']				= "這個專案的傭金美元值."; 

	$mgrlang['dsp_f_comlevel']				= "傭金級別"; 

	$mgrlang['dsp_f_comlevel_d']				= "列印總量的百分比的傭金將支付的貢獻者傭金水準";

	$mgrlang['dsp_f_mmprice']				= "最低/最高價格"; 

	$mgrlang['dsp_f_mmprice_d']				= "最低價格可以使這個項目的貢獻。請留空或輸入0表示沒有";

	$mgrlang['dsp_f_mmcredits']				= "最低/最高積分"; 

	$mgrlang['dsp_f_mmcredits_d']				= "最低積分，投稿人可以做這個項目。請留空或輸入0表示沒有";	

	$mgrlang['dsp_f_force_dis']				= "強制顯示"; 

	$mgrlang['dsp_f_force_dis_d']				= "顯示這個數位即使是較大的比原來的版本下載";	

	$mgrlang['dsp_f_cal_rs']				= "計算出實際尺寸"; 

	$mgrlang['dsp_f_cal_rs_d']				= "計算的實際規模大小，縮放範圍從原來的更新值時";

	$mgrlang['dsp_f_width']					= "寬";   

	$mgrlang['dsp_f_height']				= "高"; 

	$mgrlang['dsp_f_format']				= "格式"; 

	$mgrlang['dsp_f_hd']					= "高清"; 

	$mgrlang['dsp_f_running_time']				= "運作時間";;

	$mgrlang['dsp_f_fps']					= "FPS"; 

	$mgrlang['dsp_f_quantity_d']				= "預設提供這種規模的數量。輸入9999無限。您以後還可以設置每個媒體檔.";

	$mgrlang['dsp_f_taxable']				= "應納稅"; 

	$mgrlang['dsp_f_taxable_d']				= "這個項目的收稅."; 

	$mgrlang['dsp_f_groups']				= "數字簡介群組s";

	$mgrlang['dsp_f_groups_d']				= "選擇數位設定檔組，這屬於";

	$mgrlang['dsp_op_rf']					= "豁免";

	$mgrlang['dsp_op_cu']					= "聯絡我們"; 

	$mgrlang['dsp_op_fr']					= "免費下載"; 

	$mgrlang['dsp_op_rm']					= "版權管理"; 

	$mgrlang['dsp_unlimited']				= "無限"; 

	$mgrlang['dsp_amount']					= "設定數額"; 

	

	# PRINTS

	$mgrlang['prints_new_header']			= "添加新列印"; 

	$mgrlang['prints_edit_header']			= "編輯列印"; 

	$mgrlang['prints_new_message']			= "以下表格，您可以添加一個新的列印."; 

	$mgrlang['prints_edit_message']			= "編輯列印，然後點擊“保存更改”按鈕."; 

	$mgrlang['prints_f_name']				= "列印名稱"; 

	$mgrlang['prints_f_name_d']				= "輸入列印名稱."; 

	$mgrlang['prints_f_price']				= "列印價格"; 

	$mgrlang['prints_f_price_d']			= "列印項目的價格."; 

	$mgrlang['prints_f_my_cost']			= "我的成本"; 

	$mgrlang['prints_f_my_cost_d']			= "我此項目的成本.用於計算利潤.";  

	$mgrlang['prints_f_credits']			= "積分"; 

	$mgrlang['prints_f_credits_d']			= "它的成本積分購買此列印";

	$mgrlang['prints_f_perm']				= "許可權"; 

	$mgrlang['prints_f_perm_d']				= "選擇會員，會員組會員可以看到這個項目.";   

	$mgrlang['prints_f_taxable']			= "應納稅"; 

	$mgrlang['prints_f_taxable_d']			= "如果這個項目納稅."; 	

	$mgrlang['prints_f_multiple']			= "多重"; 

	$mgrlang['prints_f_multiple_d']			= "讓客戶購買這些專案超過一個.";  

	$mgrlang['prints_f_groups']				= "列印群組"; 

	$mgrlang['prints_f_groups_d']			= "這屬於選擇列印組."; 

	$mgrlang['prints_mes_02']				= "您必須至少有一個群組.";  

	$mgrlang['prints_mes_04']				= "所有選項的名稱必須分配."; 

	$mgrlang['prints_b_add']				= "添加項目"; 

	$mgrlang['prints_unlimited']			= "無限"; 

	$mgrlang['prints_amount']				= "設定數額"; 

	$mgrlang['prints_f_desc_d']				= "該列印項目的說明."; 

	$mgrlang['prints_mes_05']				= "只有有效的，如果設置為成員的貢獻者 '提供者分配價格'.";

	$mgrlang['prints_f_attach_d']				= "提供列印銷售. 此選項將覆蓋從個人媒體選擇.";

	$mgrlang['prints_f_adv_d']				= "此列印功能列印頁面做廣告";

	

	# PACKAGES

	$mgrlang['packages_tab1']				= "詳細"; 

	$mgrlang['packages_tab2']				= "列印"; 

	$mgrlang['packages_tab3']				= "產品"; 

	$mgrlang['packages_tab5']				= "組別"; 

	$mgrlang['packages_new_header']			= "增加新配套"; 

	$mgrlang['packages_edit_header']		= "修改配套"; 

	$mgrlang['packages_new_message']		= "通過以下表格，您可以添加一個新的配套.";  

	$mgrlang['packages_edit_message']		= "編輯這個配套,然後按一下“保存更改”按鈕.";  

	$mgrlang['packages_f_name']				= "配套名稱"; 

	$mgrlang['packages_f_name_d']			= "輸入配套名稱."; 

	$mgrlang['packages_f_prints']			= "列印"; 

	$mgrlang['packages_f_prints_d']			= "列印項目包含這個配套."; 

	$mgrlang['packages_f_prod']				= "產品"; 

	$mgrlang['packages_f_prod_d']			= "產品包含這個配套."; 

	$mgrlang['packages_h_order']			= "訂購"; 

	$mgrlang['packages_h_item_code']		= "項目代碼"; 

	$mgrlang['packages_h_item_name']		= "項目名稱"; 

	$mgrlang['packages_h_options']			= "選項"; 

	$mgrlang['packages_f_weight']			= "配套重量"; 

	$mgrlang['packages_f_weight_d']			= "這個包克重量。. 用於裝運計算的.";

	$mgrlang['packages_f_price']			= "配套價格"; 

	$mgrlang['packages_f_price_d']			= "這個配套的總價格."; 

	$mgrlang['packages_f_cost']				= "我的成本"; 

	$mgrlang['packages_f_cost_d']			= "您的這個配套的成本。用於計算利潤.";   

	$mgrlang['packages_f_perm']				= "許可權"; 

	$mgrlang['packages_f_perm_d']			= "誰可以看到/購買此配套.";  

	$mgrlang['packages_f_taxable']			= "應納稅"; 

	$mgrlang['packages_f_taxable_d']		= "如果這個配套被徵稅."; 	

	$mgrlang['packages_f_credits']			= "積分"; 

	$mgrlang['packages_f_credits_d']		= "積分花費購買這個配套."; 

	$mgrlang['packages_f_labcode']			= "實驗室代碼"; 

	$mgrlang['packages_f_labcode_d']		= "實驗室的代碼，如果需要外部印刷，運輸, 其它.";

	$mgrlang['packages_f_quantity']			= "可用數量"; 

	$mgrlang['packages_f_quantity_d']		= "這些包件數量. 讓空白以設為無限.";

	$mgrlang['packages_f_groups']			= "配套組"; 

	$mgrlang['packages_f_groups_d']			= "這屬於選擇配套組.";  

	$mgrlang['packages_f_desc_d']			= "這個配套的簡短說明."; 

	$mgrlang['packages_f_active_d']			= "設定這產品活躍。暫無產品將不會顯示在網站上.";   

	$mgrlang['packages_f_options']			= "允許選項"; 

	$mgrlang['packages_f_options_d']		= "允許選項，可以選擇列印服務和產品於這配套.這將調整基本配套的價格.";     

	$mgrlang['packages_f_adv_d']			= "配套在“特色配套”頁面上做廣告.";  	

	$mgrlang['packages_f_options2']			= "選擇"; 

	$mgrlang['packages_f_colls']			= "收集"; 

	$mgrlang['packages_f_colls_d']			= "收集包括這個配套."; 

	$mgrlang['packages_f_sub']				= "訂購"; 

	$mgrlang['packages_f_sub_d']			= "您可以選擇一個訂閱包括在此配套.";  

	$mgrlang['packages_f_attach_d']			= "提供這個包銷售的. 此選項將覆蓋從個人媒體選擇.";

	

	# LANGUAGES

	$mgrlang['languages_tab1']				= "大眾語言"; 

	$mgrlang['languages_tab2']				= "語言管理"; 

	$mgrlang['languages_f_uselang']			= "語言"; 

	$mgrlang['languages_f_uselang_d']		= "選擇語言的公共網站上. 設置預設為第一次參觀.";

	$mgrlang['languages_f_uselang_d2']		= "選擇一種語言區域";

	$mgrlang['languages_defaults']			= "此語言要更改預設的日期和時間格式編輯";

	$mgrlang['languages_default']			= "預定"; 

	$mgrlang['languages_lang']				= "活躍"; 

	

	# UTILITIES

	$mgrlang['util_f_ugmc']					= "更新圖庫媒體統計"; 

	$mgrlang['util_f_ugmc_d']				= "更新圖庫的它們所包含的媒體檔的數量.";  

	$mgrlang['util_f_srp']					= "創建復原點"; 

	$mgrlang['util_f_srp_d']				= "為您的網站設置一個復原點.如果出現錯誤，您可以設置恢復到這一點.這包括“網站設置”，“外觀及感覺”和“軟體設置”區域而已.";      

	$mgrlang['util_f_srp_b']				= "創建"; 

	$mgrlang['util_f_srp_o']				= "名稱(可選)"; 

	$mgrlang['util_f_restore']				= "恢復設置"; 

	$mgrlang['util_f_restore_d']			= "從一個復原點還原您的網站設.這包括“網站設置”，“外觀及感覺”和“軟體設置”區域而已.";    

	$mgrlang['util_f_restore_b']			= "恢復"; 

	$mgrlang['util_f_cleanup']				= "資料庫清理"; 

	$mgrlang['util_f_cleanup_d']			= "在資料庫上執行一個清理和清除未使用的資料.";  

	$mgrlang['util_f_cleanup_b']			= "執行清理"; 

	$mgrlang['util_f_cache_b']				= "清除暫存"; 

	$mgrlang['util_f_cache_d']				= "清除頁面和圖像暫存."; 

	$mgrlang['util_f_pal']					= "清除活動日誌"; 

	$mgrlang['util_f_pal_d']				= "在此日期之前刪除所有活動日誌";  

	$mgrlang['util_f_pal_b']				= "清除"; 

	$mgrlang['util_f_budb']					= "執行資料庫備份"; 

	$mgrlang['util_f_budb_d']				= "執行完整資料庫備份，然後將檔保存在伺服器上的“備份”目錄.";     

	$mgrlang['util_f_budb_b']				= "執行備份"; 

	$mgrlang['util_f_sal']					= "系統活動日誌"; 

	$mgrlang['util_f_sal_d']				= "查看系統的活動日誌.包括所有的行動做幕後.";    	

	$mgrlang['util_f_pdac']					= "列印/下載活動日誌"; 

	$mgrlang['util_f_pdac_d']				= "列印或下載活動日誌管理員.";   

	$mgrlang['util_f_pdac_admins']				= "所有管理員"; 

	$mgrlang['util_f_pdac_mems']				= "所有會員"; 

	$mgrlang['util_f_pdac_for']				= "給予"; 

	$mgrlang['util_f_pdac_from']				= "來自"; 

	$mgrlang['util_f_pdac_to']				= "至";  	

	$mgrlang['util_f_sal_b']				= "查看活動記錄"; 

	$mgrlang['util_mes_01']					= "舊的活動日誌已被刪除."; 

	$mgrlang['util_mes_02']					= "復原點已創建為您的網站設置.";  

	$mgrlang['util_mes_03']					= "您的設置已恢復."; 

	$mgrlang['util_mes_04']					= "您的資料庫已經被清理。未使用的記錄已被刪除.";   

	$mgrlang['util_mes_05']					= "您的資料庫已做備份."; 

	$mgrlang['util_mes_06']					= "圖像和頁面暫存被清除."; 

	

	# SUPPORT TICKETS

	$mgrlang['tickets_t_summary']			= "TICK;ET SUMMARY";

	$mgrlang['tickets_f_messages']			= "訊息"; 

	$mgrlang['tickets_f_files']			= "檔案"; 

	$mgrlang['tickets_edit_header']			= "編輯支援票務";

	$mgrlang['tickets_edit_message']		= "查看和回應支持票.";

	$mgrlang['tickets_tab1']			= "詳情"; 

	$mgrlang['tickets_f_summary']			= "總結"; 

	$mgrlang['tickets_f_summary_d']			= "會員輸入的支持票摘要";

	$mgrlang['tickets_f_notify']			= "通知會員"; 

	$mgrlang['tickets_f_notify_d']			= "通過電子郵件通知各會員，他的支持票已回答";

	$mgrlang['tickets_f_close']				= "關閉票務";	

	$mgrlang['tickets_id']					= "票務ID";

	$mgrlang['tickets_opened']				= "開啟票務";	

	$mgrlang['tickets_updated']				= "更新票務";

	$mgrlang['tickets_status']				= "票務狀況";	

	$mgrlang['tickets_f_reply']				= "回復"; 

	$mgrlang['tickets_f_reply_d']			= "回復此支持票";	

	$mgrlang['tickets_f_tickcon']			= "票務對話";

	$mgrlang['tickets_f_tickcon_d']			= "這張票中的所有消息. 以藍色突出顯示的消息回應來自管理員.";

	$mgrlang['tickets_mes_id']				= "信息編號"; 

	$mgrlang['tickets_no_mes']				= "沒有這種支援票的消息";	

	$mgrlang['tickets_f_files']				= "檔案"; 

	$mgrlang['tickets_f_files_d']				= "此門票的文件";

	$mgrlang['tickets_file_id']				= "檔案編號"; 	

	$mgrlang['tickets_filename']				= "檔案名稱"; 

	$mgrlang['tickets_added']				= "附加的"; 

	$mgrlang['tickets_upped_by']				= "上載者"; 

	$mgrlang['tickets_size']				= "尺寸"; 

	$mgrlang['tickets_f_status']				= "將狀態設置為";  

	$mgrlang['tickets_f_status_d']				= "將狀態設置為此票證."; 

	$mgrlang['tickets_attach_file']				= "附加檔案"; 

	

	

	# NAVIGATION

	$mgrlang['nav_home']					= "儀表板";

	

	$mgrlang['nav_content']					= "內容編輯"; 

	$mgrlang['nav_content_d']				= "請從以下選項中進行選擇."; 

	$mgrlang['subnav_page_content']			= "網頁內容"; 

	$mgrlang['subnav_page_content_d']		= "編輯頁面內容區域和文字區塊在您的網站";	

	$mgrlang['subnav_news']					= "新聞文章"; 

	$mgrlang['subnav_news_d']				= "管理出現在您的網站上的新聞文章."; 

	$mgrlang['subnav_news_groups']			= "新聞群組"; 

	$mgrlang['subnav_news_groups_d']		= "新聞群組用於組織您的新聞管理區域內.";   		

	$mgrlang['subnav_email_content']		= "電子郵件內容"; 

	$mgrlang['subnav_email_content_d']		= "編輯電子郵件發送給您的訪客."; 	

	$mgrlang['subnav_agreements']			= "執照及協議"; 

	$mgrlang['subnav_agreements_d']			= "媒體執照及模式的發佈形式/協定.";  	

	$mgrlang['subnav_support_tickets']		= "支持門票";

	$mgrlang['subnav_support_tickets_d']		= "閱讀並回應您的會員支持票";

	

	$mgrlang['nav_library']					= "文庫"; 

	$mgrlang['nav_library_d']				= "請從以下選項中進行選擇.";  

	$mgrlang['subnav_galleries']			= "陳列室/活動"; 

	$mgrlang['subnav_galleries_d']			= "創建和編輯公共和私人陳列室或活動."; 

	$mgrlang['subnav_media']				= "媒體"; 

	$mgrlang['subnav_media_d']				= "管理目前在您的網站上的媒體.";  

	$mgrlang['subnav_asset_groups']			= "媒體群組"; 

	$mgrlang['subnav_asset_groups_d']		= "媒體組用於管理區域內組織媒體.";  

	$mgrlang['subnav_collections']			= "數位收藏"; 

	$mgrlang['subnav_collections_d']		= "創建和管理媒體的收藏."; 

	$mgrlang['subnav_collections_groups']		= "收藏群體"; 

	$mgrlang['subnav_collections_groups_d']		= "管理收藏群體."; 

	$mgrlang['subnav_digital_sp']			= "數位檔案"; 

	$mgrlang['subnav_digital_sp_d']			= "您的媒體數位化版本的安裝設定檔.";  

	$mgrlang['subnav_prints']				= "列印"; 

	$mgrlang['subnav_prints_d']				= "創建和編輯現有的列印專案."; 

	$mgrlang['subnav_packages']				= "配套"; 

	$mgrlang['subnav_packages_d']			= "創建和編輯列印，產品及數位專案配套"; 

	$mgrlang['subnav_package_groups']		= "配套組"; 

	$mgrlang['subnav_package_groups_d']		= "配套組用於組織您的配套管理區域內.";   

	$mgrlang['subnav_products']				= "產品"; 

	$mgrlang['subnav_products_d']			= "創建和編輯您的網站上列出的產品."; 

	$mgrlang['subnav_product_groups']		= "產品組"; 

	$mgrlang['subnav_product_groups_d']		= "在管理領域，產品組用於組織您的產品.";   

	$mgrlang['subnav_add_media']			= "添加新媒體"; 

	$mgrlang['subnav_add_files_d']			= "新媒體上傳和輸入到您的網站."; 	

	$mgrlang['subnav_media_types']			= "媒體類型"; 

	$mgrlang['subnav_media_types_d']		= "創建和編輯媒體類型的名稱."; 

	$mgrlang['subnav_media_types_groups']		= "媒體類型組"; 

	$mgrlang['subnav_media_types_groups_d']		= "媒體類型組用於組織您的媒體類型."; 

	$mgrlang['subnav_folders']				= "資料夾"; 

	$mgrlang['subnav_folders_d']			= "創建和編輯您的媒體儲存的資料夾."; 

	$mgrlang['subnav_digital_sp_groups']		= "數位設定檔組"; 

	$mgrlang['subnav_digital_sp_groups_d']		= "數位設定檔組用於組織你的數位檔案管理區域內.";   

	$mgrlang['subnav_print_groups']			= "列印組"; 

	$mgrlang['subnav_print_groups_d']		= "列印組用於組織您的列印管理區域內.";   

	$mgrlang['subnav_media_queue']			= "媒體排序"; 

	$mgrlang['subnav_media_queue_d']		= "等待批准貢獻者上載媒體."; 

	$mgrlang['subnav_media_comments']		= "媒體評論"; 

	$mgrlang['subnav_media_comments_d']		= "您的訪客在您的網站上張貼有關媒體的評論.";  

	$mgrlang['subnav_media_ratings']		= "媒體評級"; 

	$mgrlang['subnav_media_ratings_d']		= "媒體在您的網站上訪客都留下了評級.";  

	$mgrlang['subnav_media_tags']			= "媒體標籤"; 

	$mgrlang['subnav_media_tags_d']			= "媒體在您的網站上，訪客提出了新的標籤.";  

	

	$mgrlang['nav_sales']					= "銷售"; 

	$mgrlang['nav_sales_d']					= "請從以下選項中進行選擇."; 

	$mgrlang['subnav_orders']				= "訂單"; 

	$mgrlang['subnav_orders_d']				= "查看完成和待處理的訂單."; 

	$mgrlang['subnav_order_groups']			= "訂購組"; 

	$mgrlang['subnav_order_groups_d']		= "訂購組是在管理領域，用於組織您的訂單.";  

	$mgrlang['subnav_pending_orders']		= "待處理的訂單"; 

	$mgrlang['subnav_pending_orders_d']		= "查看待處理的訂單."; 

	$mgrlang['subnav_lightboxes']			= "我的最愛";   

	$mgrlang['subnav_lightboxes_d']			= "查看會員的我的最愛."; 

	$mgrlang['subnav_lightboxes_groups']		= "我的最愛組"; 

	$mgrlang['subnav_lightboxes_groups_d']		= "我的最愛組用於組織訪客做的我的最愛.";   	

	

	$mgrlang['subnav_billings']				= "帳單"; 

	$mgrlang['subnav_billings_d']			= "查看會員的申請中和已完成的帳單."; 	

	$mgrlang['subnav_quotes']				= "報價要求"; 

	$mgrlang['subnav_quotes_d']				= "查看報價要求."; 	

	

	$mgrlang['nav_users']					= "用戶"; 

	$mgrlang['nav_users_d']					= "請從以下選項中進行選擇."; 

	$mgrlang['subnav_administrators']		= "管理員"; 

	$mgrlang['subnav_administrators_d']		= "管理管理員和他們的許可權."; 

	$mgrlang['subnav_members']				= "會員"; 

	$mgrlang['subnav_members_d']			= "管理您的網站."; 

	$mgrlang['subnav_member_bios']			= "使用者資料"; 

	$mgrlang['subnav_member_bios_d']		= "BIOS成員提交關於自己";

	$mgrlang['subnav_member_avatars']		= "頭像"; 

	$mgrlang['subnav_member_avatars_d']		= "頭像已上傳."; 

	$mgrlang['subnav_member_groups']		= "會員群組"; 

	$mgrlang['subnav_member_groups_d']		= "會員組用於組織會員管理區域內.";   

	$mgrlang['subnav_memberships']			= "會員配套"; 

	$mgrlang['subnav_memberships_d']		= "管理會員配套."; 

	$mgrlang['subnav_membership_groups']		= "會員群組"; 

	$mgrlang['subnav_membership_groups_d']		= "會員組用於組織管理區域內的會員資格.";   

	

	$mgrlang['nav_help']					= "幫助"; 

	$mgrlang['subnav_bfish']				= "Bing.com翻譯";

	$mgrlang['subnav_bfish_d']				= "Bing.com翻譯器."; 

	$mgrlang['subnav_googletrans']			= "谷歌翻譯"; 

	$mgrlang['subnav_googletrans_d']		= "谷歌線上翻譯器."; 

	$mgrlang['subnav_passgen']				= "密碼生成器"; 

	$mgrlang['subnav_passgen_d']			= "產生您的網站與管理區域的密碼."; 

	$mgrlang['subnav_wizard']				= "啟動安裝嚮導"; 

	$mgrlang['subnav_wizard_d']				= "一步一步引導安裝嚮導."; 	

	$mgrlang['subnav_forum']				= "支援論壇"; 

	$mgrlang['subnav_forum_d']				= "討論並找到其他用戶的幫助."; 

	$mgrlang['subnav_faqs']					= "文檔和教程"; 

	$mgrlang['subnav_faqs_d']				= "資料庫的常見問題，可以幫助您管理和設置您的網站.";  

	$mgrlang['subnav_extras']				= "添加組件及附加"; 

	$mgrlang['subnav_extras_d']				= "您的網站添加額外的功能和特點."; 

	$mgrlang['subnav_manual']				= "使用手冊"; 

	$mgrlang['subnav_manual_d']				= "此產品基於網路的手冊."; 

	$mgrlang['subnav_software_upgrade']		= "軟體更新"; 

	$mgrlang['subnav_software_upgrade_d']		= "檢查應用可用的升級和安裝新的附加元件.";  

	

	$mgrlang['nav_settings']				= "設定"; 

	$mgrlang['nav_settings_d']				= "請從以下選項中進行選擇."; 	

	$mgrlang['subnav_payment_options']		= "付款設定"; 

	$mgrlang['subnav_payment_options_d']		= "設置付款設定和閘道."; 	

	$mgrlang['subnav_website_settings']		= "網站設定"; 

	$mgrlang['subnav_website_settings_d']		= "為您的網站配置設定."; 

	$mgrlang['subnav_look']					= "外觀和感覺"; 

	$mgrlang['subnav_look_d']				= "為您的網站設定設置外觀和感覺."; 	

	$mgrlang['subnav_software_setup']		= "軟體設置"; 

	$mgrlang['subnav_software_setup_d']		= "設置和調整軟體設置，以滿足您的需求.";  	

	$mgrlang['subnav_languages']			= "語言"; 

	$mgrlang['subnav_languages_d']			= "選擇的語言將顯示在您的網站."; 	

	$mgrlang['subnav_countries']			= "國家"; 

	$mgrlang['subnav_countries_d']			= "編輯的國家名單."; 

	$mgrlang['subnav_countries_groups']		= "國家群組"; 

	$mgrlang['subnav_countries_groups_d']		= "國家組用於組織管理區域內的國家.";	

	$mgrlang['subnav_states']				= "州/省"; 

	$mgrlang['subnav_states_d']				= "國家和省的編輯列表"; 

	$mgrlang['subnav_zipcodes']				= "郵編/郵遞區號"; 

	$mgrlang['subnav_zipcodes_d']			= "編輯列表中的郵編/郵遞區號."; 		

	$mgrlang['subnav_shipping']				= "運輸"; 

	$mgrlang['subnav_shipping_d']			= "安裝運輸方式和計算"; 

	$mgrlang['subnav_shipping_groups']		= "組別"; 

	$mgrlang['subnav_shipping_groups_d']		= "貨運組別用於組織管理區域內部的運輸方式";   		

	$mgrlang['subnav_taxes']				= "稅務"; 

	$mgrlang['subnav_taxes_d']				= "置稅/增值稅率."; 

	$mgrlang['subnav_rightsmanaged']		= "版權管理定價"; 

	$mgrlang['subnav_rightsmanaged_d']		= "設置許可權管理的定價方案."; 

	$mgrlang['subnav_credits']				= "積分配套"; 

	$mgrlang['subnav_credits_d']			= "設置價格購買積分."; 

	$mgrlang['subnav_currencies']			= "貨幣"; 

	$mgrlang['subnav_currencies_d']			= "您的網站將允許設置貨幣."; 	

	$mgrlang['subnav_subscriptions']		= "訂購"; 

	$mgrlang['subnav_promotions']			= "促銷和優惠券."; 

	$mgrlang['subnav_subscriptions_d']		= "編輯/創建促銷和優惠券";	

	$mgrlang['subnav_components']			= "組件";

	$mgrlang['subnav_components_d']			= "設置部件添加到您的網站."; 

	$mgrlang['subnav_services']			= "服務"; 

	$mgrlang['subnav_utilities']			= "實用工具."; 

	$mgrlang['subnav_utilities_d']			= "給您網站的其他管理實用工具."; 	

	$mgrlang['subnav_storage']			= "儲存Storage<span class='mtag_grey roundme' style='font-size: 10px; font-weight: normal'>beta</span>";" <span class='mtag_grey roundme' style='font-size: 10px; font-weight: normal'>beta</span>";

	$mgrlang['subnav_storage_d']			= "安裝其他的伺服器可以承載媒體."; 

	$mgrlang['subnav_storage_groups']		= "儲存組別"; 

	$mgrlang['subnav_storage_groups_d']		= "儲存組用於組織儲存領域內部的管理區."; 

	$mgrlang['subnav_folders_groups']		= "資料夾組"; 

	$mgrlang['subnav_folders_groups_d']		= "資料夾組是用於整理您的資料夾內部管理區."; 

	$mgrlang['subnav_toolslinks']			= "我的連結";

	$mgrlang['subnav_toolslinks_d']			= "根據工具編輯“我的連結”和連結下拉式功能表。"; 

	

	$mgrlang['nav_reports']					= "報告"; 

	$mgrlang['nav_reports_d']				= "請選擇以下"; 

	

?>