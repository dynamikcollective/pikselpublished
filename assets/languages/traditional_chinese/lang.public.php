<?php

	/******************************************************************

	*  TRADITIONAL CHINESE

	******************************************************************/
	
	# 4.7
	$lang['featuredGalleries']		= "Featured Galleries";
	
	# 4.6.3	
	$lang['ccMessage']				= "Please enter your credit card information below.";
	$lang['ccNumber']				= "Card Number";
	$lang['ccCVC']					= "CVC";
	$lang['ccExpiration']			= "Expiration (MM/YYYY)";
	
	
	# 4.6.1
	$lang['passwordLeaveBlank']		= "Leave blank for no password";
	$lang['myAlbums']				= "My Albums";
	
	# 4.5
	$lang['dateDownloadUpper']		= "DATE DOWNLOADED";
	$lang['downloadTypeUpper']		= "DOWNLOAD TYPE";
	$lang['rss']					= "RSS";
	
	# 4.4.8
	$lang['uploadFile']				= "Upload File";
	
	# 4.4.7
	$lang['contrSmallFileSize']		= "The file size is too small. The file must be at least";
	# plupload
	$lang['plupSelectFiles']		=  "Select files";
	$lang['plupAddFilesToQueue']	=  "Add files to the upload queue and click the start button.";
	$lang['plupFilename']			=  "Filename";
	$lang['plupStatus']				=  "Status";
	$lang['plupSize']				=  "Size";
	$lang['plupAddFiles']			=  "Add files";
	$lang['plupStartUplaod']		=  "Start upload";
	$lang['plupStopUpload']			=  "Stop current upload";
	$lang['plupStartQueue']			=  "Start uploading queue";
	$lang['plupDragFilesHere']		=  "Drag files here.";
	
	# 4.4.6
	$lang['batchUploader']			= "Batch Uploader";
	$lang['uploader'][1]			= "Java Based";
	$lang['uploader'][2]			= "HTML5/Flash Based";
	$lang['change']					= "Change";
	
	# 4.4.5
	$lang['moreInfoPlus']				= "[More Info]";
	
	# 4.4.3
	$lang['mediaLabelPropRelease']	= "Property Release";
	
	# 4.4.2
	$lang['go']						= "GO";
	
	# 4.4.0
	$lang['share'] 					= "Share";
	$lang['bbcode'] 				= "BBCode";
	$lang['html'] 					= "HTML";
	$lang['link']					= "Link";
	$lang['pricingCalculator']		= "Pricing Calculator";
	$lang['noOptionsAvailable']		= "No options available";
	$lang['viewCartUpper']			= "VIEW CART";
		
	# 4.3
	$lang['vatIDNumber']			= "VAT ID Number";
		
	# 4.2.3
	$lang['vatIDNumber']			= "VAT ID Number";
	
	# 4.2.1
	$lang['captchaError']			= "The captcha entered was not correct. Your message was not sent.";
	

	# 4.1.7

	$lang['mediaLicenseEU'] 		= "Editorial Use";

	

	# 4.1.6

	$lang['yourBill'] 				= "你的帳單";

	$lang['invoiceNumber'] 			= "發貨單號碼";

	$lang['paymentThanks'] 			= "謝謝你的付款.";

	$lang['msActive'] 				= "你的會員資格經已生效.";

	$lang['today'] 					= "今天";

	$lang['downloadsRemainingB']	= "剩餘的下載數量";

	

	# 4.1.4

	$lang['link'] 					= "連結";

	$lang['optional'] 				= "可選擇性";

	

	# 4.1.3

	$lang['keywordRelevancy'] 		= "關鍵字關聯性";

	$lang['mediaLicenseEX'] 		= "擴展授予許可";

	

	# 4.1.1

	$lang['editMediaDetails'] 		= "編輯媒體詳細";

	$lang['searchResults'] 			= "搜索結果";

	$lang['width'] 					= "寬度";

	$lang['height'] 				= "高度";

	$lang['hd'] 					= "高清";

	$lang['mediaProfile'] 			= "總則";

	$lang['photo'] 					= "照片";

	$lang['video'] 					= "影片";

	$lang['other'] 					= "其他";

	$lang['thumbnail'] 				= "縮略圖";

	$lang['videoSample'] 			= "影片樣品";

	$lang['attachFile'] 			= "附加檔案";

	$lang['fileAttached'] 			= "附加檔";

	$lang['attachMessage'] 			= "請選擇此設定檔要附加的檔";

	$lang['browse'] 				= "瀏覽";

	$lang['uploadThumb'] 			= "上傳的縮圖";

	$lang['uploadVideo'] 			= "上傳影片預覽";

	

	# 4.1

	$lang['importSuccessMes'] 		= "導入成功!";

	$lang['forgotPassword'] 		= "忘記密碼?";

	$lang['passwordSent'] 			= "電子郵件已發送, 檢查您的電子郵件收件箱或垃圾郵件資料夾的郵件.";

	$lang['passwordFailed'] 		= "很抱歉，該成員不存在, 請嘗試重新輸入您的電子郵件.";

	$lang['photoProfiles']			= "照片簡介";

	$lang['videoProfiles']			= "影片簡介";

	$lang['otherProfiles']			= "其他簡介";

	$lang['saving']					= "儲存";

	$lang['myAccount']				= "我的帳戶";

	$lang['myGalleries']			= "我的畫廊";

	$lang['noMediaAlbum']			= "沒有媒體來顯示.";

	$lang['editDetails']			= "編輯詳細資訊";

	$lang['approvalStatus0']		= "待批准";

	$lang['approvalStatus1']		= "已批准";

	$lang['approvalStatus2']		= "審批不批准";

	$lang['noDetailsMes']			= "沒有提供進一步的細節!";	

	$lang['orphanedMedia']			= "孤立的媒體";

	$lang['lastBatch']				= "最後一批加入";

	$lang['deleteAlbumMes']			= "刪除這張專輯和它包含的所有媒體?";

	$lang['mailInMedia']			= "CD / DVD上的郵件";

	$lang['deleteAlbum']			= "刪除相冊";

	$lang['editAlbum']				= "編輯相簿";

	$lang['albumName']				= "相簿名稱";

	$lang['makePublic']				= "公開示眾";

	$lang['deleteMedia']			= "刪除媒體";

	$lang['deleteMediaMes']			= "刪除所選的媒體檔?";

	$lang['selectAll']				= "全選";

	$lang['selectNone']				= "選擇None（無）";

	$lang['noImportFilesMessage']	= "沒有要導入的檔!";

	$lang['selectAlbumMes']			= "選擇你要添加此媒體到的相簿.";

	$lang['selectGalleriesMes']		= "選擇你想添加此媒體到畫廊";

	$lang['chooseItemsMes']			= "從下面的列表中選擇項目出售";

	$lang['ablum']					= "相簿";

	$lang['pricing']				= "定價";

	$lang['px']						= "像素";	

	$lang['noSales']				= "你還沒有銷售.";

	$lang['itemUpper']				= "項目";

	$lang['commissionUpper']		= "傭金";

	$lang['addUpper']				= "增加";	

	$lang['cmDeleteVerify']			= "你確定是否想刪除此媒體?";

	$lang['waitingForImport']		= "等待媒體導入";

	$lang['importSelectedUpper']	= "導入選定的";

	$lang['addMediaDetails']		= "增加媒體詳細";

	$lang['uploadUpper']			= "上傳";

	$lang['noBioMessage']			= "目前還沒有此會員的個人簡歷";

	$lang['collections']			= "選集";

	$lang['uploadMediaUpper']		= "上傳新媒體";

	$lang['startUpper']				= "開始";

	

	# 4.0.9

	$lang['displayName']			= "顯示名字";

	$lang['newAlbum']				= "新相簿";

	$lang['albums']					= "相簿";

	$lang['viewAllMedia']			= "觀看全部媒體";

	$lang['signUpNow']				= "即刻現在註冊";

	

	# 4.0.8		

	$lang['exactMatch']				= "完全對比";

	$lang['mediaLabelMediaTypes']	= "檔案格式";

	

	# 4.0.6

	$lang['orderNumUpper']			= "訂單號碼";

	$lang['orderDateUpper']			= "訂單日期";

	$lang['paymentUpper']			= "付款";	

	

	# 4.0.5

	$lang['dateRange']				= "日期範圍";

	$lang['resolution']				= "解析度";

	$lang['continueShopUpper']		= "繼續購物";

	$lang['votes']					= "投票";

	$lang['moreNews']				= "更多新資詢";

	$lang['currentSearch']			= "搜尋";

	$lang['dates']					= "日期";

	$lang['licenseType']			= "執照種類";

	$lang['searchUpper']			= "搜尋";

	$lang['from']					= "從";

	$lang['to']						= "致";

	$lang['lightboxUpper']			= "我的最愛";

	$lang['itemsUpper']				= "項目";

	$lang['createdUpper']			= "編輯";

	$lang['na']						= "無"; 

	$lang['galSortCDate']			= "創造日期編輯日期";

	

	# 4.0.4

	$lang['digitalDownloads']		= "數位下載";



	$lang['copyright']				= "版權所有; ".date("Y");

	$lang['reserved']				= "版權所有  保留所有權利";

	

	$lang['days']					= "天數";

	$lang['weeks']					= "星期";

	$lang['months']					= "月份";

	$lang['years']					= "年份";

	$lang['weekly']					= "每週";

	$lang['monthly']				= "每月";

	$lang['quarterly']				= "季度";

	$lang['semi-annually']			= "半年度";

	$lang['annually']				= "年度"; 	

	$lang['guest']					= "訪客";	

	$lang['login'] 					= "登入";	

	$lang['loginCaps'] 				= "登入";

	$lang['loginMessage']			= "請輸入您的信箱和密碼以登入";

	$lang['loggedOutMessage']		= "你已從系統登出";

	$lang['loginFailedMessage']		= "登入失敗:您所輸入的信箱或密碼不對";

	$lang['accountActivated']		= "您的帳戶已經啟動";

	$lang['activationEmail']		= "確認郵件已經寄出.請用所收到的連接來啟動你的帳戶";

	$lang['loginAccountClosed']		= "你的帳戶已經關閉或不活躍";

	$lang['loginPending']			= "這帳戶還沒被確認.你務必得在確認後方才登入";

	$lang['yesLogin']				= "是的,我要登入";

	$lang['noCreateAccount']		= "不是,我要創建一個帳戶";

	$lang['haveAccountQuestion']	= "你有我們的帳戶嗎?";	

	$lang['collections']			= "數位收集";

	$lang['featuredCollections']	= "特色數位收集";	

	$lang['similarMedia']			= "類似圖片";	

	$lang['paypal']					= "PayPal";

	$lang['checkMO']				= "支票/匯票";

	$lang['paypalEmail']			= "PayPal Email";	

	$lang['paid']					= "已經付款";

	$lang['processing']				= "處理中";

	$lang['unpaid']					= "未付款"; 

	$lang['refunded']				= "退款"; 

	$lang['failed']					= "失敗"; 

	$lang['cancelled']				= "取消";

	$lang['approved']				= "已批准"; 

	$lang['incomplete']				= "不完整"; 

	$lang['billLater']				= "遲些付款";

	$lang['expired']				= "過期";

	$lang['unlimited']				= "無限";

	$lang['quantityAvailable']		= "適用數量範圍";	

	$lang['shipped']				= "寄送出";

	$lang['notShipped']				= "尚未寄出";

	$lang['backordered']			= "以往訂單"; 

	$lang['taxIncMessage']			= "已包括稅務"; 	

	$lang['addTag']					= "增加標籤"; 

	$lang['memberTags']				= "會員標籤"; 

	$lang['comments']				= "意見簿"; 

	$lang['showMoreComments']		= "顯示全意見"; 

	$lang['noComments']				= "現今沒有批准意見."; 

	$lang['noTags']					= "現今沒有批准標籤."; 

	$lang['addNewComment']			= "增加新意見"; 

	$lang['commentPosted']			= "你的意見已被貼出."; 

	$lang['commentPending']			= "你的新意見已呈交及會在短時間被批准.";   

	$lang['commentError']			= "你的意見出現錯誤所以無法成功呈交.";  

	$lang['tagPosted']				= "你的新標籤已被貼出.:"; 

	$lang['tagPending']				= "你的新標籤已呈交及會在短時間被批准.";  

	$lang['tagError']				= "你的標籤出現錯誤所以無法成功呈交.";   

	$lang['tagDuplicate']			= "這標籤已經出現."; 

	$lang['tagNotAccepted']			= "你的新標籤不被我們的系統接受."; 	

	$lang['preferredLang']			= "首選語言"; 

	$lang['dateTime']				= "日期/時間"; 

	$lang['preferredCurrency']		= " 首選貨幣"; 	

	$lang['longDate']				= "時間長格式"; 

	$lang['shortDate']				= "時間短格式"; 

	$lang['numbDate']				= "時間數目"; 	

	$lang['daylightSavings']		= "夏令時"; 

	$lang['dateFormat']				= "日期格式"; 

	$lang['timeZone']				= "區域時間"; 

	$lang['dateDisplay']			= "顯示日期"; 

	$lang['clockFormat']			= "時鐘格式"; 

	$lang['numberDateSep']			= "時間數目分隔點"; 	

	$lang['renew']					= "更新"; 	

	$lang['noOrders']				= "現在你的帳戶沒有發現任何訂單.";   

	$lang['noDownloads']			= "現在你的帳戶沒有發現任何下載.";  

	$lang['noFeatured'] 			= "現在你的範圍沒有發現任何特色項目."; 	

	$lang['promotions']				= "促銷"; 

	$lang['noPromotions']			= "現在你的範圍沒有發現任何優惠及促銷.";   	

	$lang['autoApply']				= "自動化登出."; 

	$lang['useCoupon']				= "請用以下代號編碼登出或按鍵使用優惠/促銷";    	

	$lang['couponApplied']			= "您的優惠/折扣已在購物車出現.";  

	$lang['couponFailed']			= "這是無效的優惠券或折扣."; 	

	$lang['couponNeedsLogin']		= "您必須先登錄才能使用此優惠券或折扣.";  

	$lang['couponMinumumWarn']		= "你的小計不符合要求使用此優惠券或折扣.";   

	$lang['couponAlreadyUsed']		= "您只可以使用一次此優惠券或折扣.";  

	$lang['couponLoginWarn']		= "您必須先登錄才能使用此優惠券或折扣.";  

	$lang['checkout']				= "結帳"; 

	$lang['continue']				= "繼續"; 	

	$lang['shipTo']					= "寄至"; 

	$lang['billTo']					= "Bill To";

	//$lang['mailTo']				= "郵件支付"; 	

	$lang['galleries']				= "圖庫"; 

	$lang['chooseGallery']			= "選擇一個圖庫，以查看它的內容.";  

	$lang['galleryLogin']			= "請輸入下面這個圖庫的密碼登錄.";   

	$lang['galleryWrongPass']		= "您輸入的密碼為這個圖庫是不正確的.";  

	$lang['newestMedia']			= "最新的媒體"; 

	$lang['popularMedia']			= "熱門媒體"; 	

	$lang['contributors']			= "提供者"; 

	$lang['contUploadNewMedia']		= "增加新媒體"; 

	$lang['contViewSales']			= "查看銷售";  

	$lang['contPortfolio']			= "我的作品"; 

	$lang['contGalleries']			= "我的陳列室"; 

	$lang['contMedia']				= "我的媒體"; 	

	$lang['aboutUs']				= "關於我們"; 	

	$lang['news']					= "信息"; 

	$lang['noNews']					= "在這個時候沒有資訊文章."; 	

	$lang['termsOfUse']				= "使用條款"; 

	$lang['privacyPolicy']			= "隱私權政策"; 

	$lang['purchaseAgreement']		= "購買協議";   

	$lang['iAgree']					= "我同意"; 	

	$lang['createAccount']			= "創建一個帳戶"; 

	$lang['createAccountMessage']	= "請填寫下面的表格，創建一個新的帳戶.";  	

	$lang['contactUs']				= "聯絡我們"; 

	$lang['contactMessage']			= "感謝您與我們聯繫。稍後我們將作出反應.";  

	$lang['contactError']			= "聯繫方式中沒有被正確地填寫。您的郵件沒有發送.";   

	$lang['contactIntro']			= "請使用下面的表格與我們聯繫。我們會儘快回應.";   

	$lang['contactEmailSubject']	= "從聯繫表出現的問題";

	$lang['contactFromName']		= "聯繫方式"; 	

	$lang['prints']					= "列印"; 

	$lang['featuredPrints']			= "特色列印"; 	

	$lang['newBill']				= "這個成員已經有了一項新的帳單. 此成員將不會被啟動或更新，直至該法案支付. 任何未付款的會員費已被取消.";

	$lang['accountInfo']			= "帳戶資料"; 

	$lang['accountUpdated']			= "您的帳號資訊已更新."; 

	$lang['editAccountInfo']		= "編輯您的帳號資訊"; 

	$lang['editAccountInfoMes']		= "編輯您的帳號資訊，然後按一下“保存”按鈕，保存所做的更改.";    	

	$lang['accountInfoError1']		= "密碼和確認密碼不相同!"; 

	$lang['accountInfoError5']		= "目前您輸入的密碼不正確!"; 

	$lang['accountInfoError2']		= "您的密碼必須至少6個字元!"; 

	$lang['accountInfoError12']		= "此電子郵寄地址已在使用中!"; 

	$lang['accountInfoError13']		= "您的電子郵件不被接受!"; 

	$lang['accountInfoError3']		= "請輸入以上的字!"; 

	$lang['accountInfoError4']		= "不正確的驗證碼!"; 

	$lang['readAgree']				= "我已閱讀"; 	

	$lang['agreements']				= "協議"; 	

	$lang['poweredBy']				= "技術支援"; 

	$lang['captchaAudio']			= "請輸入您聽到的字碼"; 

	$lang['captchaIncorrect']		= "不正確，請再試一次"; 

	$lang['captchaPlayAgain']		= "再播放"; 

	$lang['captchaDownloadMP3']		= "下載至MP3格式"; 

	$lang['captcha']				= "驗證碼"; 

	$lang['subHeaderID']			= "編號";  

	$lang['subHeaderSubscript']		= "訂購"; 

	$lang['subHeaderExpires']		= "逾期"; 

	$lang['subHeaderDPD']			= "每天的下載量/剩餘"; 

	$lang['subHeaderStatus']		= "狀況"; 

	$lang['downloads']				= "下載"; 	

	$lang['thankRequest']			= "感謝您的要求。我們將儘快與您聯繫.";   

	$lang['pleaseContactForm']		= "請填妥以下表格來聯絡我們以知這檔案的價錢.";   

	$lang['downWithSubscription']	= "下載使用訂購"; 	

	$lang['noInstantDownload'] 		= "此檔不能立即下載。該計畫將通過網站管理員的要求時，通過電子郵件方法購買.";     

	



// Tickets

	$lang['newTicketsMessage']		= "最新或更新的"; 

	$lang['emptyTicket']			= "這張票是空的.";

	$lang['messageID']				= "留言編號"; 

	$lang['ticketNoReplies']		= "此票證關閉，不能接受任何新的回復.";

	$lang['ticketUpdated']			= "此票已更新!";

	$lang['ticketClosed']			= "此票已關閉!";

	$lang['closeTicket']			= "關閉支援票";

	$lang['newTicket']				= "新的支援票";

	$lang['newTicketButton']		= "新的支援票按鍵";

	$lang['ticketUnreadMes']		= "未讀/新郵件";

	$lang['ticketUnderAccount']		= "在此帳戶下有沒有支援票";	

	$lang['ticketSubmitted']		= "您的支援票已提交。稍後我們將及時回復.";

	

	$lang['mediaNotElidgiblePack']	= "該媒體是沒有資格被分配到任何配套!"; 

	$lang['noBills']				= "有沒有在該帳戶下的票據.";	

	$lang['lightbox']				= "我的最愛"; 

	$lang['noLightboxes']			= "您沒有我的最愛."; 

	$lang['lightboxDeleted']		= "我的最愛已被刪除."; 

	$lang['lbDeleteVerify']			= "您確定您想刪除這個我的最愛?"; 

	$lang['newLightbox']			= "新的我的最愛"; 

	$lang['lightboxCreated']		= "已創建您的新我的最愛."; 

	$lang['addToLightbox']			= "添加到我的最愛"; 

	$lang['createNewLightbox']		= "創建一個新的我的最愛"; 

	$lang['editLightbox']			= "編輯我的最愛"; 

	$lang['addNotes']				= "增加筆記";

	$lang['editLightboxItem']		= "編輯我的最愛產品"; 

	$lang['removeFromLightbox']		= "從我的最愛中刪除"; 	

	$lang['savedChangesMessage']	= "您的更改已保存."; 	

	$lang['noSubs']					= "在此帳戶下有沒有訂閱."; 	

	$lang['unpaidBills']			= "未付帳單"; 	

	$lang['notices']				= "通告";	

	$lang['subscription']			= "訂購"; 	

	$lang['assignToPackage']		= "指定的配套"; 

	$lang['startNewPackage']		= "開始新配套"; 

	$lang['packagesInCart']			= "配套已在購物車中"; 	

	$lang['id']						= "編號"; 

	$lang['summary']				= "總結"; 

	$lang['status']					= "狀態"; 

	$lang['lastUpdated']			= "最後更新"; 

	$lang['opened']					= "開設"; 

	$lang['reply']					= "回復"; 

	$lang['required']				= "要求"; 	

	$lang['bills']					= "帳單"; 

	$lang['orders']					= "訂單";

	$lang['downloadHistory']		= "下載歷史"; 

	$lang['supportTickets']			= "支援"; 

	$lang['order']					= "訂購"; 

	$lang['packages']				= "配套"; 

	$lang['featuredPackages']		= "特色配套"; 	

	$lang['products']				= "產品"; 

	$lang['featuredProducts']		= "特色產品"; 	

	$lang['subscriptions']			= "訂購"; 

	$lang['featuredSubscriptions']	= "特色訂購"; 	

	$lang['yourCredits']			= "你的數額"; 

	$lang['featuredCredits']		= "特色積分配套"; 	

	$lang['media'] 					= "媒體"; 

	$lang['mediaNav'] 				= "媒體"; 

	$lang['featuredMedia']			= "特色媒體"; 	

	$lang['featuredItems'] 			= "特色"; 	

	$lang['showcasedContributors'] 	= "展出的提供者";	

	$lang['galleryLogin']			= "圖庫登入 ";	

	$lang['forum'] 					= "論壇 ";

	$lang['randomMedia'] 			= "隨機媒體";

	$lang['language'] 				= "語言 ";	

	$lang['priceCreditSep']			= "或者";	

	$lang['viewCollection']			= "查看收集 ";	

	$lang['loggedInAs']				= "您登錄為 ";	

	$lang['editProfile']			= "編輯帳戶 ";	

	$lang['noItemCartWarning']		= "您必須選擇一個照片，然後才能將該產品添加到您的購物車 !";

	$lang['cartTotalListWarning']	= "這些僅是一個估計值，可能會稍微改變，由於匯率波動，並四捨五入.";

	$lang['applyCouponCode']		= "申請優惠券代碼 ";

	$lang['billMeLater']			= "之後給我帳單 ";

	$lang['billMeLaterDescription']	= "我們會向您發每月您所購買的物品帳單.";

	$lang['shippingOptions']		= "運送選項";

	$lang['paymentOptions']			= "付款方式";

	$lang['chooseCountryFirst']		= "先選擇國家";

	$lang['paymentCancelled']		= "您的付款已被取消。如果您想要繼續，您可以嘗試再次檢查.";    

	$lang['paymentDeclined']		= "您的付款已被拒絕。如果您想要繼續，您可以嘗試再次檢查.";   		

	$lang['generalInfo']			= "個人資訊"; 

	$lang['membership']				= "會員籍"; 

	$lang['preferences']			= "首選項"; 

	$lang['address']				= "地址"; 

	$lang['actions']				= "行動措施"; 

	$lang['changePass']				= "更換密碼"; 

	$lang['changeAvatar']			= "更換頭像"; 

	$lang['bio']					= "傳記"; 

	$lang['contributorSettings']		= "貢獻者設定"; 

	$lang['edit']					= "編輯"; 

	$lang['leftToFill']				= "留下的物品，以填補";	

	$lang['commissionMethod']		= "傭金支付方法"; 

	$lang['commission']				= "傭金"; 	

	$lang['signupDate']				= "會員期限自"; 

	$lang['lastLogin']				= "最後登入"; 	

	$lang['clientName']				= "顧客姓名"; 

	$lang['eventCode']				= "活動代碼"; 

	$lang['eventDate']				= "活動日期"; 

	$lang['eventLocation']			= "活動地點";  	

	$lang['viewPackOptions']		= "查看配套的內容和選項"; 

	$lang['viewOptions']			= "查看選項"; 	

	$lang['remove']					= "清除"; 

	$lang['productShots']			= "產品照片"; 

	$lang['currentPass']			= "目前的密碼"; 

	$lang['newPass']				= "新密碼"; 

	$lang['vNewPass']				= "確認新密碼"; 

	$lang['verifyPass']				= "確認密碼"; 

	$lang['firstName']				= "名字"; 

	$lang['lastName']				= "姓"; 

	$lang['location']				= "地點"; 

	$lang['memberSince']			= "會員期限自"; 

	$lang['portfolio']				= "PORTFOLIO";

	$lang['profile']				= "簡介"; 

	$lang['address']				= "地址"; 

	$lang['city']					= "城市"; 

	$lang['state']					= "州/省"; 

	$lang['zip']					= "郵編/郵遞區號"; 

	$lang['country']				= "國家"; 

	$lang['save'] 					= "保存"; 

	$lang['add'] 					= "添加"; 

	$lang['companyName']			= "公司名稱"; 

	$lang['accountStatus']			= "帳戶狀態"; 

	$lang['website']				= "網站"; 

	$lang['phone']					= "電話";  

	$lang['email'] 					= "信箱"; 

	$lang['name'] 					= "姓名"; 

	$lang['submit'] 				= "提交"; 	

	$lang['message'] 				= "留言"; 	

	$lang['question']				= "問題"; 

	$lang['password'] 				= "密碼"; 	

	$lang['members'] 				= "會員"; 

	$lang['visits'] 				= "訪問"; 	

	$lang['logout'] 				= "登出"; 

	$lang['lightboxes']				= "我的最愛"; 

	$lang['cart']					= "您的購物車"; 

	$lang['cartItemAdded']			= "目前已被您添加專案的購物車."; 

	$lang['includesTax']			= "包括政府稅/增值稅"; 

	$lang['addToCart']				= "添加到購物車"; 

	$lang['items']					= "項目"; 

	$lang['item']					= "項目"; 

	$lang['qty']					= "數量"; 

	$lang['price']					= "價格"; 

	$lang['more']					= "更多"; 

	$lang['moreInfo']				= "更多資訊"; 

	$lang['back']					= "回去"; 

	$lang['none']					= "無"; 

	$lang['details']				= "細節"; 

	$lang['options']				= "選項"; 

	$lang['page']					= "頁面"; 

	$lang['ratingSubmitted']		= "評價提交"; 

	$lang['noMedia']				= "這個圖庫是空的"; 

	$lang['itemsTotal']				= "總項目"; 

	$lang['of']						= "離開"; 

	$lang['view']					= "查看"; 

	$lang['apply']					= "申請"; 

	$lang['currency']				= "兌換貨幣"; 

	$lang['active']					= "活躍";

	$lang['pending']				= "未審核";

	$lang['freeTrial']				= "免費試用";

	$lang['setupFee']				= "安裝費";

	$lang['free']					= "免費";

	$lang['open']					= "開";

	$lang['close']					= "關閉";

	$lang['closed']					= "關閉";

	$lang['by']						= "By";

	$lang['download']				= "下載";

	$lang['downloadUpper']			= "下載";

	$lang['KB']						= "K";

	$lang['MB']						= "MB";

	$lang['files']					= "檔案"; 

	$lang['unknown']				= "不知道"; 

	$lang['freeDownload']			= "免費下載"; 

	$lang['prevDown']				= "之前下載"; 

	$lang['pay']					= "付款"; 

	$lang['purchaseCredits']		= "購買積分"; 

	$lang['purchaseSub']			= "購買訂閱"; 

	$lang['hour'] 					= "小時"; 

	$lang['slash'] 					= "削減"; 

	$lang['period'] 				= "期間"; 

	$lang['dash'] 					= "Dash";

	$lang['gmt'] 					= "GMT";

	$lang['avatar'] 				= "頭像"; 

	$lang['delete'] 				= "刪除"; 

	$lang['uploadAvatar']			= "上傳頭像"; 

	$lang['welcome']				= "歡迎"; 

	$lang['expires']				= "過期"; 

	$lang['msExpired']				= "以下的會員已過期"; 

	$lang['newSales']				= "自您最近一次登錄的新銷售"; 

	$lang['never']					= "從來沒有";  

	$lang['yes']					= "是的"; 

	$lang['no']						= "不是"; 

	$lang['create'] 				= "創建"; 

	$lang['cancel'] 				= "取消"; 

	$lang['notes'] 					= "筆記"; 

	$lang['update'] 				= "更新"; 

	$lang['each'] 					= "各個"; 

	$lang['enterKeywords']			= "請輸入關鍵字"; 

	$lang['send']					= "發送"; 

	$lang['emailTo']				= "發郵件給"; 

	$lang['yourName']				= "您的姓名"; 

	$lang['yourEmail']				= "您的電子郵件地址"; 

	$lang['emailToFriend']			= "電子郵件連結到這個媒體"; 

	$lang['emailToFriendSent']		= "電子郵件已發送!您可以發送或關閉此視窗.";  

	$lang['linkSentBy']				= "照片或影片的連結已發送到您的.";  

	$lang['newPackageMessage']		= "以下開始一個新配套或選擇一個類似配套已經在您的購物車裡.";    

	$lang['warning']				= "警告!"; 

	$lang['estimated']				= "估計"; 

	$lang['doneUpper']				= "完成"; 

	$lang['returnToSiteUpper']		= "返回網站"; 

	$lang['chooseCountryFirst']		= "選擇國家"; 

	$lang['advancedSearch']			= "進階搜尋"; 

	$lang['eventSearch']			= "搜尋活動"; 	

	$lang['AND']					= "和"; 

	$lang['OR']						= "或"; 

	$lang['NOT']					= "不是"; 

	$lang['noAccess']				= "您沒有進入查看這範圍."; 	

	$lang['siteStats']				= "網站數據"; 

	$lang['membersOnline']			= "線上會員"; 

	$lang['minutesAgo']				= "分鐘前"; 

	$lang['seconds']				= "分秒前";	

	$lang['megabytesAbv']			= "MB";	

	$lang['downWithSub']			= "下載使用認購"; 

	$lang['downloadsRemaining']		= "下載剩餘的今天";  

	$lang['requestDownloadSuccess']	= "我們已經收到您的檔請求，並會儘快與您聯繫.";    	

	$lang['mediaLicenseNFS']		= "非賣品"; 

	$lang['mediaLicenseRF']			= "版稅免費"; 

	$lang['mediaLicenseRM']			= "版權管理"; 

	$lang['mediaLicenseFR']			= "免費"; 

	$lang['mediaLicenseCU']			= "聯絡我們"; 

	$lang['original']				= "原本"; 	

	$lang['prevDownloaded']			= "您先前已經下載了這個檔.有免費下載.";  

	$lang['instantDownload']		= "立即下載"; 

	$lang['requestDownload']		= "要求下載"; 

	$lang['request']				= "要求"; 

	$lang['enterEmail']				= "輸入您的信箱位址"; 	

	$lang['license']				= "執照"; 

	$lang['inches']					= "英寸"; 

	$lang['centimeters']			= "公分"; 

	$lang['dpi']					= "dpi";	

	$lang['noSimilarMediaMessage']	= "沒有發現類似的媒體."; 

	$lang['backUpper']				= "後面"; 

	$lang['nextUpper']				= "下一個"; 

	$lang['prevUpper']				= "上一頁"; 

	$lang['curGalleryOnly']			= "僅當前圖庫"; 

	$lang['sortBy']					= "排序方式";

	$lang['galSortColor']			= "顏色"; 

	$lang['galSortDate']			= "上傳日期"; 

	$lang['galSortID']				= "編號"; 

	$lang['galSortTitle']			= "標題"; 

	$lang['galSortFilename']		= "檔案名稱"; 

	$lang['galSortFilesize']		= "檔案尺寸"; 

	$lang['galSortSortNum']			= "排序號碼"; 

	$lang['galSortBatchID']			= "分批編號"; 

	$lang['galSortFeatured']		= "精選"; 

	$lang['galSortWidth']			= "寬度"; 

	$lang['galSortHeight']			= "高度"; 

	$lang['galSortViews']			= "瀏覽次數"; 

	$lang['galSortAsc']				= "由大到小";  

	$lang['galSortDesc']			= "由小到大"; 

	$lang['mediaIncludedInColl']	= "此媒體檔案包含在以下收集."; 	

	$lang['billHeaderInvoice']		= "發票"; 

	$lang['billHeaderDate']			= "帳單日期"; 

	$lang['billHeaderDueDate']		= "截止日期"; 

	$lang['billHeaderTotal']		= "總"; 

	$lang['billHeaderStatus']		= "狀態";

	

	// Cart

	$lang['creditsWarning']			= "您沒有足夠的積分結算.請登錄或添加積分到您的購物車.";   

	$lang['subtotalWarning']		= "以結帳的最低購買:"; 

	$lang['pleaseWait']				= "請稍候，我們處理您的請求!"; 

	$lang['billMailinThanks']		= "請郵寄支票或匯票數額至下列出的位址.一旦收到付款，我們將標記您的帳單支付.";      

	$lang['mailinRef']				= "請參考下面的號碼與您的付款.";  

	$lang['subtotal']				= "小計"; 

	$lang['shipping']				= "航運"; 

	$lang['discounts']				= "折扣/優惠券"; 

	$lang['total']					= "總"; 

	$lang['creditsSubtotal']		= "積分小計"; 

	$lang['creditsDiscounts']		= "信用卡折扣"; 

	$lang['credits']				= "積分"; 

	$lang['reviewOrder']			= "查看您的訂單"; 

	$lang['payment']				= "付款"; 

	$lang['cartNoItems']			= "您的購物車中沒有商品!"; 

	$lang['enterShipInfo']			= "以下請輸入您的送貨和帳單資訊"; 

	$lang['sameAsShipping']			= "送貨位址相同"; 

	$lang['differentAddress']		= "提供一個不同的位址"; 

	$lang['noShipMethod']			= "沒有運輸方式存在至運送這些物品."; 	

	$lang['choosePaymentMethod']	= "請選擇付款方式"; 	

	$lang['discountCode']			= "折扣代碼"; 

	$lang['use']					= "使用"; 

	$lang['continueNoAccount']		= "或沒有帳戶繼續";

	

	

	// Order

	$lang['yourOrder']				= "您的訂單"; 

	$lang['viewInvoice']			= "查看發票"; 

	$lang['totalsShown']			= "總計所示"; 

	$lang['downloadExpired']		= "下載過期"; 

	$lang['downloadExpiredMes']		= "本次下載連結已經過期.請聯繫我們重新啟動.";   

	$lang['downloadMax']			= "超出最大下載量"; 

	$lang['downloadMaxMes']			= "你已經達到了最大數量允許此購買檔的下載. 以下是載連結啟動，請聯繫我們.";

	$lang['downloadNotApproved']	= "訂單未獲批准"; 

	$lang['downloadNotApprovedMes']	= "您的訂單未被批准之前，您無法下載檔案.請稍後再試.";   

	$lang['downloadNotAvail']		= "不提供即時下載"; 

	$lang['downloadNotAvailMes']	= "此檔不能即時下載.請使用“請求”按鈕，以下有這個檔通過電子郵件發送給您.";    

	$lang['orderNumber']			= "訂單號碼"; 

	$lang['orderPlaced']			= "下訂單"; 

	$lang['orderStatus']			= "訂單狀態"; 

	$lang['paymentStatus']			= "付款狀態"; 

	

	

	// IPTC

	$lang['iptc']					= "IPTC";

	$lang['iptc_title']				= "標題"; 

	$lang['iptc_description']		= "說明"; 

	$lang['iptc_instructions']		= "說明"; 

	$lang['iptc_date_created']		= "創建日期"; 

	$lang['iptc_author']			= "作者"; 

	$lang['iptc_city']				= "城市"; 

	$lang['iptc_state']				= "州"; 

	$lang['iptc_country']			= "國家"; 

	$lang['iptc_job_identifier']	= "作業識別字"; 

	$lang['iptc_headline']			= "標題"; 

	$lang['iptc_provider']			= "Creditline"; //Provider

	$lang['iptc_source']			= "來源"; 

	$lang['iptc_description_writer']= "介紹作家"; 

	$lang['iptc_urgency']			= "急"; 

	$lang['iptc_copyright_notice']	= "版權聲明"; 

	

	// EXIF

	$lang['exif']							= "EXIF";

	$lang['exif_FileName']					= "檔案名稱"; 

	$lang['exif_FileDateTime']				= "檔的日期/時間"; 

	$lang['exif_FileSize']					= "檔案大小"; 

	$lang['exif_FileType']					= "檔案類型"; 

	$lang['exif_MimeType']					= "模仿類型"; 

	$lang['exif_SectionsFound']				= "Sections Found";

	$lang['exif_ImageDescription']			= "圖片說明"; 

	$lang['exif_Make']						= "製作"; 

	$lang['exif_Model']						= "模型"; 

	$lang['exif_Orientation']				= "方向"; 

	$lang['exif_XResolution']				= "X解析度"; 

	$lang['exif_YResolution']				= "Y解析度"; 

	$lang['exif_ResolutionUnit']			= "解析度單位"; 

	$lang['exif_Software']					= "軟體"; 

	$lang['exif_DateTime']					= "日期/時間"; 

	$lang['exif_YCbCrPositioning']			= "YCbCr定位";  

	$lang['exif_Exif_IFD_Pointer']			= "Exif IFD Pointer";

	$lang['exif_GPS_IFD_Pointer']			= "GPS IFD Pointer";

	$lang['exif_ExposureTime']				= "曝光時間"; 

	$lang['exif_FNumber']					= "F號碼"; 

	$lang['exif_ExposureProgram']			= "曝光程式";  

	$lang['exif_ISOSpeedRatings']			= "國際標準組織感光度評級"; 

	$lang['exif_ExifVersion']				= "Exif 版本"; 

	$lang['exif_DateTimeOriginal']			= "日期/時間原件"; 

	$lang['exif_DateTimeDigitized']			= "日期/時間 數位化"; 

	$lang['exif_ComponentsConfiguration']	= "元件配置"; 

	$lang['exif_ShutterSpeedValue']			= "快門速度值"; 

	$lang['exif_ApertureValue']				= "光圈值"; 

	$lang['exif_MeteringMode']				= "測光模式"; 

	$lang['exif_Flash']						= "閃光燈"; 

	$lang['exif_FocalLength']				= "焦距"; 

	$lang['exif_FlashPixVersion']			= "Flash pix版本"; 

	$lang['exif_ColorSpace']				= "色彩空間"; 

	$lang['exif_ExifImageWidth']			= "Exif圖像寬度"; 

	$lang['exif_ExifImageLength']			= "Exif圖像長度"; 

	$lang['exif_SensingMethod']				= "檢測方法";  

	$lang['exif_ExposureMode']				= "曝光模式"; 

	$lang['exif_WhiteBalance']				= "白平衡";  

	$lang['exif_SceneCaptureType']			= "場景拍攝類型";  

	$lang['exif_Sharpness']					= "清晰度";  

	$lang['exif_GPSLatitudeRef']			= "GPS範圍參考";  

	$lang['exif_GPSLatitude_0']				= "GPS範圍 0";  

	$lang['exif_GPSLatitude_1']				= "GPS範圍 1"; 

	$lang['exif_GPSLatitude_2']				= "GPS範圍 2"; 

	$lang['exif_GPSLongitudeRef']			= "GPS經度參考"; 

	$lang['exif_GPSLongitude_0']			= "GPS經度 0";  

	$lang['exif_GPSLongitude_1']			= "GPS經度 1";  

	$lang['exif_GPSLongitude_2']			= "GPS經度 2";  

	$lang['exif_GPSTimeStamp_0']			= "GPS時間戳記 0";  

	$lang['exif_GPSTimeStamp_1']			= "GPS時間戳記 1";  

	$lang['exif_GPSTimeStamp_2']			= "GPS時間戳記 2";  

	$lang['exif_GPSImgDirectionRef']		= "GPS圖像方向參考";  

	$lang['exif_GPSImgDirection']			= "GPS圖像方向";  

	

	// Media Labels

	$lang['mediaLabelTitle']		= "標題"; 

	$lang['mediaLabelDesc']			= "介紹"; 

	$lang['mediaLabelCopyright']	= "版權"; 

	$lang['mediaLabelRestrictions']	= "使用限制"; 

	$lang['mediaLabelRelease']		= "流傳模型";

	$lang['mediaLabelKeys']			= "關鍵字"; 

	$lang['mediaLabelFilename']		= "檔案名稱"; 

	$lang['mediaLabelID']			= "編號"; 

	$lang['mediaLabelDate']			= "附加的"; 

	$lang['mediaLabelDateC']		= "創建"; 

	$lang['mediaLabelDownloads']	= "下載"; 

	$lang['mediaLabelViews']		= "瀏覽次數"; 

	$lang['mediaLabelPurchases']	= "購買"; 

	$lang['mediaLabelColors']		= "顏色"; 

	$lang['mediaLabelPrice']		= "價格"; 

	$lang['mediaLabelCredits']		= "積分"; 

	$lang['mediaLabelOwner']		= "擁有者"; 

	$lang['mediaLabelUnknown']		= "未知"; 

	$lang['mediaLabelResolution']	= "像素"; 

	$lang['mediaLabelFilesize']		= "檔案大小"; 

	$lang['mediaLabelRunningTime']	= "執行時間"; 

	$lang['mediaLabelFPS']			= "FPS";

	$lang['mediaLabelFormat']		= "格式"; 

	

	// Search

	$lang['search']					= "搜尋"; 

	$lang['searchEnterKeyMessage']	= "請在搜索欄位中輸入關鍵字，開始搜索.";  

	$lang['searchNoResults']		= "您的搜索沒有任何結果."; 

	$lang['searchDateRange']		= "在指定日期範圍搜尋."; 

	$lang['searchNarrow']			= "縮小搜尋結果."; 

	$lang['searchStart']			= "以下開始您的搜尋!"; 

	$lang['searchAll']				= "所有"; 

	$lang['searchClear']			= "清除搜尋"; 

	$lang['searchKeywords']			= "關鍵字"; 

	$lang['searchTitle']			= "標題"; 

	$lang['searchDescription']		= "說名"; 

	$lang['searchFilename']			= "檔案名稱"; 

	$lang['searchPortrait']			= "直"; 

	$lang['searchLandscape']		= "橫"; 

	$lang['searchSquare']			= "方形"; 

	$lang['searchRoyaltyFree']		= "免版稅"; 

	$lang['searchRightsManaged']	= "版權管理"; 

	$lang['searchFree']				= "免費"; 

	$lang['searchContactUs']		= "聯絡我們"; 

	$lang['searchHeaderKeywords']	= "關鍵字"; 

	$lang['searchHeaderFields']		= "欄位"; 

	$lang['searchHeaderType']		= "種類"; 

	$lang['searchHeaderOrientation']= "方向"; 

	$lang['searchHeaderColor']		= "顏色"; 

	$lang['searchHeaderGalleries']	= "畫廊"; 	

?>