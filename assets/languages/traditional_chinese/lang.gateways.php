<?php

	# GATEWAYS LANG

	# 4.6.3
	# STRIPE
	$lang['stripe_displayName']			= "Stripe";
	$lang['stripe_publicDescription']	= "Credit Card or Debit Card";
	$lang['stripe_pkey']				= "Publishable Key";
	$lang['stripe_pkey_d']				= "Your publishable key for your stripe account.";
	$lang['stripe_skey']				= "Secret Key";
	$lang['stripe_skey_d']				= "Your secret key for your stripe account.";
	

	# 4.1.4

	# MOLLIE IDEAL

	$lang['mollieideal_displayName']		= "iDeal (Mollie)";

	$lang['mollieideal_f_partnerid']		= "夥伴編號";

	$lang['mollieideal_f_partnerid_d']		= "你的iDeal 夥伴ID.";

	$lang['mollieideal_f_profilekey']		= "簡介關鍵";

	$lang['mollieideal_f_profilekey_d']		= "你的 Mollie iDeal 理想的關鍵.";

	$lang['mollieideal_f_testmode']			= "測試模式";

	$lang['mollieideal_f_testmode_d']		= "轉成你的Mollie iDeal測試模式.";

	$lang['mollieideal_instructions']		= "確保測試模式對應的設置在你的 Mollie.nl帳戶.";

	$lang['mollieideal_publicDescription']	= "支付安全和方便使用 iDeal (僅在荷蘭).";

	

	# 4.1

	# ONEBIP 

	$lang['onebip_displayName']			= "OneBip"; 

	$lang['onebip_merchantid']			= "電子郵件";	

	$lang['onebip_merchantid_d']		= "輸入為您的onebip帳戶所使用的電子郵件."; 

	$lang['onebip_merchantkey']			= "API金鑰"; 

	$lang['onebip_merchantkey_d']		= "輸入為您的onebip帳戶的API金鑰.";

	$lang['onebip_testmode']			= "測試模式";

	$lang['onebip_testmode_d']			= "進入測試模式，用你的onebip支付.";

	$lang['onebip_publicDescription']	= "流動付款";

	

	# PAYFAST 

	$lang['payfast_displayName']		= "快速付款"; 

	$lang['payfast_merchantid']		= "編號";	

	$lang['payfast_merchantid_d']		= "輸入編號"; 

	$lang['payfast_merchantkey']		= "編號"; 

	$lang['payfast_merchantkey_d']		= "將您的NOCHEX付款,進入測試模式";

	$lang['payfast_testmode']		= "測試模式"; 

	$lang['payfast_testmode_d']		= "將您的快速付款進入測試模式";

	$lang['payfast_publicDescription']	= "信用卡或銀行"; 

	

	# NOCHEX

	$lang['nochex_displayName']		= "Nochex";

	$lang['nochex_f_accountid']		= "帳戶編號";

	$lang['nochex_f_accountid_d']		= "您的NOCHEX的帳戶編號";

	$lang['nochex_f_testmode']		= "測試模式";

	$lang['nochex_f_testmode_d']		= "將您的NOCHEX付款,進入測試模式";

	$lang['nochex_publicDescription']	= "信用卡或借記卡";

	

	# WORLDPAY

	$lang['worldpay_displayName']		= "WorldPay";

	$lang['worldpay_f_installid']		= "安裝編號";

	$lang['worldpay_f_installid_d']		= "為您的WorldPay帳戶安裝編號";

	$lang['worldpay_f_testmode']		= "測試模式";

	$lang['worldpay_f_testmode_d']		= "將您的WorldPay付款,進入測試模式";

	$lang['worldpay_publicDescription']	= "信用卡或借記卡";

	

	# ROBOKASSA

	$lang['robokassa_displayName']		= "RoboKassa";

	$lang['robokassa_f_merchantid']		= "商家編號";

	$lang['robokassa_f_merchantid_d']	= "商家編號為您的RoboKassa的帳戶.";

	$lang['robokassa_f_merchantpass']	= "商戶編號商戶通";

	$lang['robokassa_f_merchantpass_d']	= "商家編號為您的RoboKassa商人通ID.";

	$lang['robokassa_publicDescription']= "信用卡或借記卡";

	

	# PAYGATE

	$lang['paygate_displayName']		= "PayGate";

	$lang['paygate_f_accountid']		= "PayGate 編號";

	$lang['paygate_f_accountid_d']		= "D為您關閘的帳戶.";

	$lang['paygate_f_accountkey']		= "PayGate 重點";

	$lang['paygate_f_accountkey_d']		= "您的PayGate編號.";

	$lang['paygate_f_testmode']			= "測試模式";

	$lang['paygate_f_testmode_d']		= "將您的的PayGate支付到測試模式.";	

	$lang['paygate_f_testid']			= "測試你的PayGateID";

	$lang['paygate_f_testid_d']			= "你的測試關閘ID";

	$lang['paygate_f_testkey']			= "測試PayGate關鍵";

	$lang['paygate_f_testkey_d']		= "你的PayGate測試重點";

	$lang['paygate_publicDescription']	= "信用卡或借記卡";

	

	# PAYSTATION

	$lang['paystation_displayName']		= "PayStation";

	$lang['paystation_f_accountid']		= "PayStation 編號";

	$lang['paystation_f_accountid_d']	= "您的PayStation 編號.";

	$lang['paystation_f_testmode']		= "測試模式";

	$lang['paystation_f_testmode_d']	= "進入測試模式，你的PayStation.";	

	$lang['paystation_f_gatewayid']		= "閘道編號";

	$lang['paystation_f_gatewayid_d']	= "你的PayStation 閘道編號.";

	$lang['paystation_publicDescription']= "信用卡或借記卡";

	

	# SKRILL

	$lang['skrill_displayName']			= "Skrill (moneybookers)";

	$lang['skrill_f_email']				= "電子信箱";

	$lang['skrill_f_email_d']			= "您的Skrill帳戶的電子郵件地址.";

	$lang['skrill_f_testmode']			= "測試模式";

	$lang['skrill_f_testmode_d']		= "進入測試模式，你的Skrill";	

	$lang['skrill_f_testemail']			= "測試電子郵件";

	$lang['skrill_f_testemail_d']		= "你的Skrill測試電子郵件地址.";

	$lang['skrill_publicDescription']	= "信用卡或借記卡";

	$lang['skrill_f_completeOrder']		= "點擊這裡填寫您的訂單!";

	

	# CHRONOPAY

	$lang['chronopay_displayName']		= "ChronoPay";

	$lang['chronopay_f_clientid']		= "用戶端ID";

	$lang['chronopay_f_clientid_d']		= "你的ChronoPay用戶端編號.";

	$lang['chronopay_f_siteid']			= "網站ID";

	$lang['chronopay_f_siteid_d']		= "你的ChronoPay網站編號.";

	$lang['chronopay_f_productid']		= "產品編號";

	$lang['chronopay_f_productid_d']	= "你的ChronoPay產品編號.";

	$lang['chronopay_publicDescription']= "信用卡或借記卡";

	

	# IDEAL

	$lang['ideal_displayName']			= "iDeal (ING)";

	$lang['ideal_f_accountid']			= "帳戶編號";

	$lang['ideal_f_accountid_d']		= "你的iDeal帳戶編號.";

	$lang['ideal_f_transkey']			= "秘密鑰匙";

	$lang['ideal_f_transkey_d']			= "你的iDeal秘密鑰匙.";

	$lang['ideal_f_testmode']			= "測試模式";

	$lang['ideal_f_testmode_d']			= "把你的iDeal付款測試模式.";

	$lang['ideal_publicDescription']	= "銀行戶頭";

	

	# PAYPAL

	$lang['paypal_displayName']			= "PayPal";

	$lang['paypal_f_email']				= "電子郵件";

	$lang['paypal_f_email_d']			= "您的PayPal帳戶的電子郵寄地址.";

	$lang['paypal_f_testmode']			= "測試模式";

	$lang['paypal_f_testmode_d']		= "將您的Paypal支付進入測試模式.";	

	$lang['paypal_f_testemail']			= "測試用電子信箱";

	$lang['paypal_f_testemail_d']		= "您的PayPal測試環境使用電子郵寄地址.";

	$lang['paypal_publicDescription']	= "PayPal, 信用卡或銀行帳戶";

	

	# 2CHECKOUT

	$lang['2checkout_displayName']		= "2Checkout.com";

	$lang['2checkout_f_accountid']		= "帳戶編號";

	$lang['2checkout_f_accountid_d']	= "你的2Checkout帳戶編號.";

	$lang['2checkout_f_testmode']		= "測試模式";

	$lang['2checkout_f_testmode_d']		= "進入測試模式，把你的2Checkout支付.";

	$lang['2checkout_instructions']		= "In 2Checkout.com set direct return to 'Header Redirect (Your URL)' and set approved URL to http://www.YOUR_DOMAIN_NAME.com/assets/gateways/2checkout/ipn.php";

	$lang['2checkout_publicDescription']= "信用卡或美國銀行帳戶";



	

	# Plug n' Pay

	$lang['plugnpay_displayName']		= "隨插即支付";

	$lang['plugnpay_f_accountid']		= "戶口編號";

	$lang['plugnpay_f_accountid_d']		= "你的外掛程式與付費帳戶編號.";

	$lang['plugnpay_publicDescription']	= "";

	

	# AUTHORIZE.NET

	$lang['authorize_displayName']		= "Authorize.net";

	$lang['authorize_f_apiid']			= "API 登錄編號";

	$lang['authorize_f_apiid_d']		= "你的Authorize.net API 登錄編號.";

	$lang['authorize_f_transkey']		= "交易鍵";

	$lang['authorize_f_transkey_d']		= "你的Authorize.net交易鍵.";

	$lang['authorize_f_testmode']		= "測試模式";

	$lang['authorize_f_testmode_d']		= "進入測試模式，用你的Authorize.net支付.";

	$lang['authorize_publicDescription']= "";

	$lang['authorize_f_completeOrder'] = "點擊這裡填寫您的訂單!";

	

	# MYGATE.CO.ZA

	$lang['mygate_displayName']			= "MyGate.co.za";

	$lang['mygate_f_merchantid']		= "商家編號";

	$lang['mygate_f_merchantid_d']		= "你的MyGate.co.za 商家編號.";

	$lang['mygate_f_appid']				= "申請ID";

	$lang['mygate_f_appid_d']			= "你的MyGate.co.za 應用程式編號.";

	$lang['mygate_publicDescription']	= "公眾說明";

	$lang['mygate_f_testmode']			= "測試模式";

	$lang['mygate_f_testmode_d']		= "進入測試模式，你的Mygate付款.";	

	

	# MAIL IN PAYMENT

	$lang['mailin_displayName']			= "在付款郵件";

	$lang['mailin_f_instructions']		= "郵件付款說明";

	$lang['mailin_f_instructions_d']	= "說明客戶發送郵件，如支票或匯票付款。";

	$lang['mailin_publicDescription']	= "郵寄您的付款給下頁所提供的地址.";

?>
