<?php

	# 4.4.6
	$wplang['stats_visitors']		= "Approximate Site Visits";

	# 4.0.7	

	$wplang['sitehealth_exif']	= "PHP EXIF支援 (exif_read_data) PHP EXIF Support (exif_read_data)";

	

	# GENERAL WIDGET LANGUAGE

	$wplang['widget_close']		= "關";

	$wplang['widget_save']		= "儲存";

	$wplang['load_failed'] 		= "裝載板失敗!";

	$wplang['widget_title']		= "標題";

	$wplang['widget_note']		= "記錄";

	$wplang['widget_for']		= "為";

	

	# NOTES WIDGET

	$wplang['notes_title']		= "記錄";

	$wplang['notes_newnote']	= "新記錄"; 

	$wplang['notes_postedby']	= "貼出由"; 

	$wplang['notes_lastupdate']	= "最後更新"; 

	$wplang['notes_nonotes']	= "這裡沒有任何的記錄顯示"; 

	

	# EXTRAS WIDGET

	$wplang['extras_title']		= "額外的照片儲存庫"; 

	

	# KTOOLS ACCOUNT WIDGET

	$wplang['kaccount_title']	= "K工具 帳戶"; 

	$wplang['kaccount_support']	= "支援/升級 每天提醒"; 

	$wplang['kaccount_messages']	= "未讀留言"; 

	$wplang['kaccount_affil']	= "加上自上次登錄的銷售"; 

	

	# KNEWS WIDGET

	$wplang['knews_title']		= "Ktools.net 消息";

	

	# QUICK STATS WIDGET - NEW AND PENDING

	$wplang['qstats_title']		= "新和未審核."; 

	$wplang['qstats_logmem']	= "自上次登錄的會員"; 

	$wplang['qstats_penmem']	= "未審核會員"; 

	$wplang['qstats_logorders']	= "自上次登錄的訂單"; 

	$wplang['qstats_penorders']	= "未審核訂單"; 

	$wplang['qstats_logcomm']	= "自上次登錄的意見"; 

	$wplang['qstats_pencomm']	= "未審核的意見"; 

	$wplang['qstats_logtags']	= "自上次登錄的標籤"; 

	$wplang['qstats_pentags']	= "未審核標籤"; 

	$wplang['qstats_lograte']	= "自上次登錄的評級"; 

	$wplang['qstats_penrate']	= "未審核評級"; 

	$wplang['qstats_penbios']	= "未審核的會員動態時報"; 

	$wplang['qstats_penavatars']	= "未審核的頭像"; 

	$wplang['qstats_pensupport']	= "待支持門票";

	

	# STATS WIDGET

	$wplang['stats_title']		= "統計資料"; 

	$wplang['stats_op1']		= "銷售"; 

	$wplang['stats_op2']		= "會員"; 

	$wplang['stats_op_7days']	= "最近7天"; 

	$wplang['stats_op_6mon']	= "最近6個月"; 

	$wplang['stats_op_5year']	= "最近5年"; 

	$wplang['stats_tdsales']	= "總銷售"; 

	$wplang['stats_tdorders']	= "總訂單"; 

	$wplang['stats_sales']		= "銷售"; 

	$wplang['stats_orders']		= "訂購"; 

	$wplang['stats_atsales']	= "全天銷售"; 

	$wplang['stats_atorders']	= "全天訂單"; 

	$wplang['stats_tdmems']		= "今天新會員"; 

	$wplang['stats_mems']		= "新會員"; 

	$wplang['stats_tamems']		= "總活躍會員"; 

	$wplang['stats_timems']		= "總不活躍會員"; 

	

	

	# SITE HEALTH WIDGET

	$wplang['sitehealth_title']	= "網站伺服器健康"; 

	$wplang['sitehealth_ok']	= "OK";

	$wplang['sitehealth_low']	= "低"; 

	$wplang['sitehealth_high']	= "高"; 

	$wplang['sitehealth_failed']	= "失敗"; 

	$wplang['sitehealth_unava'] 	= "不可用"; 

	$wplang['sitehealth_off']	= "關"; 

	$wplang['sitehealth_on']	= "開"; 

	$wplang['sitehealth_inst']	= "安裝"; 

	$wplang['sitehealth_none']	= "無"; 

	$wplang['sitehealth_exists']	= "存在"; 

	$wplang['sitehealth_write']	= "可寫";

	$wplang['sitehealth_nonwri']	= "不可寫";

	$wplang['sitehealth_php']	= "PHP 版本";  

	$wplang['sitehealth_gd']	= "GD 圖書館";  

	$wplang['sitehealth_mem']	= "PHP記憶體限制"; 

	$wplang['sitehealth_exe']	= "PHP 最大_執行_時間";  

	$wplang['sitehealth_time']	= "PHP 最大_輸入_時間";  

	$wplang['sitehealth_file']	= "PHP 上載_最大_檔案尺寸"; ;

	$wplang['sitehealth_post']	= "PHP post_max_size";

	$wplang['sitehealth_safe']	= "PHP安全_模式"; 

	$wplang['sitehealth_dbv']	= "資料庫/產品版本檢查"; 

	$wplang['sitehealth_mysqlv']	= "MySQL 版本";  

	$wplang['sitehealth_load']	= "伺服器負載"; 

	$wplang['sitehealth_upti']	= "伺服器正常執行時間"; 



	# UPDATE CHECK WIDGET

	$wplang['updater_title']	= "檢查更新"; 

	$wplang['updater_newest']	= "你有新版本"; 

	$wplang['updater_update']	= "更新可用"; 

	$wplang['updater_newestis']	= "新版本是"; 

	$wplang['updater_getnew']	= "最新版本請登入 <a href='http://www.ktools.net/members/' target='_blank'>Ktools.net account</a>";

	$wplang['updater_yourv']	= "您正在執行的版本"; 

	

	# BLANK WIDGET

	$wplang['blank_title']		= "空白方格"; 

	

	# CALENDAR WIDGET

	$wplang['calendar_title']	= "日曆"; 

	

	# TIPS CHECK WIDGET

	//$wplang['tips_title']		= "提示"; 

	//$wplang['tips_next']		= "下一個提示"; 

	//$welcometip[] 				= "您可以使用<strong>Ctrl+Shift+S</strong>來開及關的捷徑程式命名表?";   

	//$welcometip[] 				= "您可以將這些的歡迎面板的重新安排他們的位置，你想。只要點擊標題，然後拖動。然後刪除該面板的順序."; 

	//$welcometip[] 				= "為了快速訪問到您的陳列室和許多其他領域，按一下該選項卡的左上角的箭頭。這將打開快顯功能表。再次按一下該選項卡關閉.";    

	

?>
