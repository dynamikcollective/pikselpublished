<?php
	/******************************************************************
	*  CATALÀ
	******************************************************************/
	
	# 4.7
	$lang['featuredGalleries']		= "Featured Galleries";
	
	# 4.6.3	
	$lang['ccMessage']				= "Please enter your credit card information below.";
	$lang['ccNumber']				= "Card Number";
	$lang['ccCVC']					= "CVC";
	$lang['ccExpiration']			= "Expiration (MM/YYYY)";
	
	# 4.6.1
	$lang['passwordLeaveBlank']		= "Deixa en blanc per a cap contrasenya";
	$lang['myAlbums']				= "Els meus Albums";
	
	# 4.5
	$lang['dateDownloadUpper']		= "DATA DESCARREGAT";
	$lang['downloadTypeUpper']		= "DESCÀRREGA TIPUS";
	$lang['rss']					= "RSS";
	
	# 4.4.8
	$lang['uploadFile']				= "Carrega arxiu";
	
	# 4.4.7
	$lang['contrSmallFileSize']		= "La mida del fitxer és massa petit. L'arxiu ha de ser d'almenys";
	# plupload
	$lang['plupSelectFiles']		=  "Selecciona arxius";
	$lang['plupAddFilesToQueue']	=  "Afegir fitxers a la cua de càrrega i feu clic al botó d'inici.";
	$lang['plupFilename']			=  "Nom d'arxiu";
	$lang['plupStatus']				=  "Estat";
	$lang['plupSize']				=  "Tamany";
	$lang['plupAddFiles']			=  "Afegir arxius";
	$lang['plupStartUplaod']		=  "Iniciar càrrega d'arxius";
	$lang['plupStopUpload']			=  "Aturar la càrrega actual";
	$lang['plupStartQueue']			=  "Iniciar càrrega d'arxius";
	$lang['plupDragFilesHere']		=  "Arrossegar arxius aquí.";
	
	# 4.4.6
	$lang['batchUploader']			= "Carregador de lots";
	$lang['uploader'][1]			= "Basat enJava";
	$lang['uploader'][2]			= "Basat en HTML5/Flash";
	$lang['change']					= "Canvi";
	
	# 4.4.5
	$lang['moreInfoPlus']				= "[Més informació]";
	
	# 4.4.3
	$lang['mediaLabelPropRelease']	= "Autorització de Drets";
	
	# 4.4.2
	$lang['go']						= "ANAR";
	
	# 4.4.0
	$lang['share'] 					= "Compartir";
	$lang['bbcode'] 				= "BBCode";
	$lang['html'] 					= "HTML";
	$lang['link']					= "Enllaç";
	$lang['pricingCalculator']		= "Calculadora de preus";
	$lang['noOptionsAvailable']		= "No hi ha opcions disponibles";
	$lang['viewCartUpper']			= "VEURE CARRET";
		
	# 4.3
	$lang['vatIDNumber']			= "Número d'identificació fiscal (NIF)";
	
	# 4.2.1
	$lang['captchaError']			= "El captcha introduit no era correcte. El seu missatge no s'ha enviat.";

	# 4.1.7
	$lang['mediaLicenseEU'] 		= "Us Editorial";
	
	# 4.1.6
	$lang['yourBill'] 				= "la seva factura";
	$lang['invoiceNumber'] 			= "Número de factura";
	$lang['paymentThanks'] 			= "Gràcies per la seva compra.";
	$lang['msActive'] 				= "La seva compte ja està activa.";
	$lang['today'] 					= "Avui";
	$lang['downloadsRemainingB']	= "Reiniciar descarrega";

# 4.1.4
	$lang['link'] 					= "Enllaç";
	$lang['optional'] 				= "Opcional";

# 4.1.3
	$lang['keywordRelevancy'] 		= "Paraula clau";
	$lang['mediaLicenseEX'] 		= "Licència extessa";

# 4.1.1
	$lang['editMediaDetails'] 		= "Edita detalls";
	$lang['searchResults'] 			= "Resultat de la búsqueda";
	$lang['width'] 					= "Ample";
	$lang['height'] 				= "Alt";
	$lang['hd'] 					= "HD";
	$lang['mediaProfile'] 			= "Perfil";
	$lang['photo'] 					= "foto";
	$lang['video'] 					= "Video";
	$lang['other'] 					= "Altre";
	$lang['thumbnail'] 				= "Miniatura";
	$lang['videoSample'] 			= "Mostra Video";
	$lang['attachFile'] 			= "Adjuntar arxiu";
	$lang['fileAttached'] 			= "Arxiu adjuntat";
	$lang['attachMessage'] 			= "Seleccioni el perfil per adjuntar l'arxiu.";
	$lang['browse'] 				= "Visualitzar";
	$lang['uploadThumb'] 			= "Pujar miniatura";
	$lang['uploadVideo'] 			= "Pujar mostra de Video";

# 4.1
	$lang['importSuccessMes'] 		= "Importació finalitzada!";
	$lang['forgotPassword'] 		= "Ha perdut la contrasenya?";
	$lang['passwordSent'] 			= "Se li ha enviat un email, miri en su safata d'entrada o la carpeta de correu no desitjat.";
	$lang['passwordFailed'] 		= "Disculpi però el membro no existeig, si us plau entri el email de nou.";
	$lang['photoProfiles']			= "Perfils Fotogràfics";
	$lang['videoProfiles']			= "Perfils de Video";
	$lang['otherProfiles']			= "Altres perfils";
	$lang['saving']					= "Guardan";
	$lang['myAccount']				= "La meva compte";
	$lang['myGalleries']			= "Les meves galeries";
	$lang['noMediaAlbum']			= "No hi ha arxiu que ensenyar.";
	$lang['editDetails']			= "Editi detalls";
	$lang['approvalStatus0']		= "Pendent d'aprovació";
	$lang['approvalStatus1']		= "Aprovat";
	$lang['approvalStatus2']		= "Error en l'aprovació";
	$lang['noDetailsMes']			= "No s'han proporcionat més detalls!";	
	$lang['orphanedMedia']			= "medis horfes";
	$lang['lastBatch']				= "Últim lot afegit";
	
	
	$lang['deleteAlbumMes']			= "¿Desitja borrar aquest àlbum i els arxius que conté?";
	$lang['mailInMedia']			= "Mail on CD/DVD";
	$lang['deleteAlbum']			= "Borrar àlbum";
	$lang['editAlbum']				= "Editar àlbum";
	$lang['albumName']				= "Nom de l'àlbum ";
	$lang['makePublic']				= "Fer Públic";
	$lang['deleteMedia']			= "Borrar Arxiu";
	$lang['deleteMediaMes']			= "¿Borrar els arxius seleccionats?";
	$lang['selectAll']				= "Selecionar tot";
	$lang['selectNone']				= "Deseleccionar";
	$lang['noImportFilesMessage']	= "¡No hi ha arxius per importar!";
	$lang['selectAlbumMes']			= "Seleccioni l'àlbum al que desitja afegir l'arxiu.";
	$lang['selectGalleriesMes']		= "Seleccioni la galeria que desitja afegir l'arxiu.";
	$lang['chooseItemsMes']			= "Seleccioni els articles per vendre en la seguent llista.";
	$lang['ablum']					= "Àlbum";
	$lang['pricing']				= "Preus";
	$lang['px']						= "px";	
	$lang['noSales']				= "No te vendes.";
	$lang['itemUpper']				= "ARTICLE";
	$lang['commissionUpper']		= "COMISIÓ";
	$lang['addUpper']				= "AFEGIR";
	$lang['cmDeleteVerify']			= "¿Esta segur que desitja borrar l'arxiu?";
	$lang['waitingForImport']		= "Esperant arxiu per importar";
	$lang['importSelectedUpper']	= "IMPORTAR SELECCIONADES";
	$lang['addMediaDetails']		= "Afegir detalls d'arxiu";
	$lang['uploadUpper']			= "PUJAR";
	$lang['noBioMessage']			= "Actualment no existeig historial sobre aquest membre.";
	$lang['collections']			= "Coleccions";
	$lang['uploadMediaUpper']		= "PUJAR NOU ARXIU";
	$lang['startUpper']				= "INICI";

# 4.0.9
	$lang['displayName']			= "Mostrar nom";
	$lang['newAlbum']				= "Nou àlbum";
	$lang['albums']					= "Àlbums";
	$lang['viewAllMedia']			= "VEURE TOTS ELS ARXIUS";
	$lang['signUpNow']				= "REGISTRAR-SE";

/*Spanish*/
$lang['exactMatch'] = "Exacta";
$lang['orderNumUpper']			= "Numero de Compra";
$lang['orderDateUpper']			= "Data de Compra";
$lang['paymentUpper']			= "Pagament";	
$lang['dateRange']				= "Rang de Data";
$lang['resolution']				= "Resolució";
$lang['continueShopUpper']		= "CONTINUI COMPRAN";
$lang['votes']					= "vots";
$lang['moreNews']				= "Més Notícies";
$lang['currentSearch']			= "Búsqueda actual";
$lang['dates']					= "Datas";
$lang['licenseType']			= "Tipus de llicència";
$lang['searchUpper']			= "BUSCAR";
$lang['from']					= "De";
$lang['to']						= "Per";
$lang['lightboxUpper']			= "PRESELECCIÓ";
$lang['itemsUpper']				= "ARTICLES";
$lang['createdUpper']			= "CREAT";
$lang['na']						= "N/A"; 
$lang['galSortCDate']			= "Creat el";	
	$lang['digitalDownloads']		= "Descarregues d'arxius";

	$lang['copyright']				= "Copyright&copy; 2012";
	$lang['reserved']				= "Tots els drets reservats.";
	
	$lang['days']					= "Dies";
	$lang['weeks']					= "Setmanes";
	$lang['months']					= "Mesos";
	$lang['years']					= "Anys";
	$lang['weekly']					= "Setmanal";
	$lang['monthly']				= "Mensual";
	$lang['quarterly']				= "Cuatrimestre";
	$lang['semi-annually']			= "Semestral";
	$lang['annually']				= "Anual";
	$lang['guest']					= "Convidat";
	$lang['login'] 					= "Iniciar sessió";	
	$lang['loginCaps'] 				= "INICIAR SESSIÓ";	
	$lang['loginMessage']			= "Si us plau; ingresi el seu mail i contrasenya per ingresar a la compte.";
	$lang['loggedOutMessage']		= "Fí de la sessió.";
	$lang['loginFailedMessage']		= "Error d'inici de sessió: El seu mail o Contrasenya és incorrecta.";
	$lang['accountActivated']		= "La seva compte a estat activada.";
	$lang['activationEmail']		= "Se li ha enviat un mail de verificació. Si us plau, cliqui sobre el link que rebra al email per activar la seva compte.";
	$lang['loginAccountClosed']		= "Aquesta compte està tancada o inactiva.";
	$lang['loginPending']			= "Compte no verificada. Heu de verificar el compte avans d'iniciar la sessió.";
	$lang['yesLogin']				= "Si, m'agradaria iniciar la sessió.";
	$lang['noCreateAccount']		= "No, M'agradaria crear un compte nou.";
	$lang['haveAccountQuestion']	= "Te un compte amb nosaltres?";

	$lang['collections']			= "Coleccions digitals";
	$lang['featuredCollections']	= "Coleccions digitals destacades";
	
	$lang['similarMedia']			= "Medis similars";
	
	$lang['paypal']					= "PayPal";
	$lang['checkMO']				= "Ordre de Chec/efectivo";
	$lang['paypalEmail']			= "Mail de Paypal";
	
	$lang['paid']					= "Pagament";
	$lang['processing']				= "Processant";
	$lang['unpaid']					= "Inpagament";
	$lang['refunded']				= "Tornat";
	$lang['failed']					= "Ha fallat";
	$lang['cancelled']				= "Cancel·lat";
	$lang['approved']				= "Aprovat";
	$lang['incomplete']				= "Incomplet";
	$lang['billLater']				= "Cobrar despres";
	$lang['expired']				= "Vençut";
	$lang['unlimited']				= "Il·limitat";
	$lang['quantityAvailable']		= "Quantitat disponible";
	
	$lang['shipped']				= "Enviat";
	$lang['notShipped']				= "No enviat";
	$lang['backordered']			= "Contraentrega";
	
	$lang['taxIncMessage']			= "Incloent impostos";
	
	$lang['addTag']					= "Agregar etiqueta";
	$lang['memberTags']				= "Etiqueta de membre";
	$lang['comments']				= "Comentaris";
	$lang['showMoreComments']		= "Mostrar tots els comentaris";
	$lang['noComments']				= "No hi ha comentaris aprovats.";
	$lang['noTags']					= "No hi ha etiquetes aprovades.";
	$lang['addNewComment']			= "Agregar nou comentari";
	$lang['commentPosted']			= "El seu comentari fou publicat.";
	$lang['commentPending']			= "El seu nou comentari fou rebut i aviat serà aprovat.";
	$lang['commentError']			= "Hi ha agut un error i el seu comentari no fou enviat.";
	$lang['tagPosted']				= "La seva etiqueta fou publicada.";
	$lang['tagPending']				= "La seva nova etiqueta fou rebuda i aviat serà aprobada.";
	$lang['tagError']				= "Hi ha agut un error i la seva etiqueta no fou enviada.";
	$lang['tagDuplicate']			= "Aquesta etiqueta ya existeig.";
	$lang['tagNotAccepted']			= "La seva etiqueta no fou aceptada pel nostre sistema.";
	
	$lang['preferredLang']			= "Idioma preferit";
	$lang['dateTime']				= "Data/Hora";
	$lang['preferredCurrency']		= "Moneda preferida";
	
	$lang['longDate']				= "Data llarga";
	$lang['shortDate']				= "Data curta";
	$lang['numbDate']				= "Data numèrica";
	
	$lang['daylightSavings']		= "Horari d'estiu";
	$lang['dateFormat']				= "Format de data";
	$lang['timeZone']				= "Franja horària";
	$lang['dateDisplay']			= "Mostrar data";
	$lang['clockFormat']			= "Format del rellotge";
	$lang['numberDateSep']			= "Separador de número data";
	
	$lang['renew']					= "RENOVAR";
	
	$lang['noOrders']				= "No hi ha compres realitzades amb aquesta compte.";
	$lang['noDownloads']			= "No s'han fet descarreges amb aquesta compte.";
	$lang['noFeatured'] 			= "No hi han articles destacats en aquest àrea.";
	
	$lang['promotions']				= "Promocions";
	$lang['noPromotions']			= "No hi ha cupons o promocions en aquest moment.";	
	$lang['autoApply']				= "S'aplica automàticament a la compra.";
	$lang['useCoupon']				= "Utilitci el sigüent codi en la compra o faci clic al butó aplicar per utilitzar aquest cupó / promoció.";
	
	$lang['couponApplied']			= "Un cupó o desconte s'ha aplicat a la seva compra.";
	$lang['couponFailed']			= "Aquest cupó o descuento no és valid.";	
	$lang['couponNeedsLogin']		= "Has d'iniciar sessió per utilitzar aquest cupó o desconte.";
	$lang['couponMinumumWarn']		= "El subtotal no cumpleig amb el requisit per utilizar aquest cupó o desconte.";
	$lang['couponAlreadyUsed']		= "Nomes pot utilitzar aquest cupó o desconte una vegada.";
	$lang['couponLoginWarn']		= "Has d'iniciar sessió per utilitzar aquest cupó o desconte.";
	
	$lang['checkout']				= "COMPRAR";
	$lang['continue']				= "CONTINUAR";
	
	$lang['shipTo']					= "Enviar a";
	$lang['billTo']					= "Cobrarli a";
	$lang['mailTo']					= "Enviar pagament a";
	
$lang['galleries']				= "Galeries";
	$lang['chooseGallery']			= "Escollir una galeria de sota per veure el seu contingut.";
	$lang['galleryLogin']			= "Si us plau, introdueixi la contrasenya de la galería de sota per iniciar sessió.";
	$lang['galleryWrongPass']		= "La contrasenya que ha introduito per aquesta galería era incorrecta.";
	
	$lang['newestMedia']			= "Nou contingut";

	$lang['popularMedia']			= "Contingut popular";
	
	$lang['contributors']			= "Colaboradors";
	$lang['contUploadNewMedia']		= "Pujar contingut nou";
	$lang['contViewSales']			= "Veure vendes";
	$lang['contPortfolio']			= "El meu Porfolio";
	$lang['contGalleries']			= "Les meves galeries";
	$lang['contMedia']				= "Els meus continguts";
	
	$lang['aboutUs']				= "Sobre nosaltres";
	
	$lang['news']					= "Notícies";
	$lang['noNews']					= "No hi ha cap notícia en aquest moment.";
	
	$lang['termsOfUse']				= "Termes d'Us";
	$lang['privacyPolicy']			= "Política de privacitat";
	$lang['purchaseAgreement']		= "Acord de compra";	
	$lang['iAgree']					= "Acepto les";
	
	$lang['createAccount']			= "Crear un compte";
	$lang['createAccountMessage']	= "Si us plau, completi el següent formulari per crear la seva compte.";
	
	$lang['contactUs']				= "Contacte";
	$lang['contactMessage']			= "Gràcies per contactar amb nosaltres. L'atendrem el mes aviat possible.";
	$lang['contactError']			= "El formulari de contacte no fou omplert correctament. El seu missatje no fou enviat.";
	$lang['contactIntro']			= "Si us plau, utilitzi aquest formulari per contactar amb nosaltres. Li respondrem el més aviat posible.";
	$lang['contactEmailSubject']	= "Pregunta per Formulari de Contacte";
	$lang['contactFromName']		= "formulari de Contacte";
	
	$lang['prints']					= "Impresions";
	$lang['featuredPrints']			= "Impresions Destacades";
	
	$lang['newBill']				= "Una nova Factura s'ha creat per aquesta compta d'usuari. Aquesta Compta no s'activarà o renovarà fins que la factura es pagui. Les Factures anteriors pendents han estat cancelades.";
	
	$lang['accountInfo']			= "Informació de la Compta";
	$lang['accountUpdated']			= "L'informació de la seva compta fou actualitzada.";
	$lang['editAccountInfo']		= "Editar la seva compte d'usuari";
	$lang['editAccountInfoMes']		= "Editi l'informació de la seva compta i faci clic al butó per guardar els canvis.";	
	$lang['accountInfoError1']		= "Les Contrasenyes no coincideixen!";
	$lang['accountInfoError5']		= "La Contrasenya que heu ingresat és Incorrecta!";
	$lang['accountInfoError2']		= "La seva contrasenya ha de tenir al menys 6 caracters!";
	$lang['accountInfoError12']		= "Aquest mail ya fou registrat al nostre web!";
	$lang['accountInfoError13']		= "El seu mail no fou aceptat!";
	$lang['accountInfoError3']		= "Introdueixi les palaules de sobre";
	$lang['accountInfoError4']		= "Les paraules Captcha ingresades son Incorrectes!";
	$lang['readAgree']				= "He lleguit la";
	
$lang['agreements']				= "Acords";
	
	$lang['poweredBy']				= "Powered By";
	$lang['captchaAudio']			= "Introdueig els números que escoltes";
	$lang['captchaIncorrect']		= "Incorrecte, torna a provar-ho, si us plau";
	$lang['captchaPlayAgain']		= "Escoltar de Nou";
	$lang['captchaDownloadMP3']		= "Descarregar en Format MP3";
	$lang['captcha']				= "Captcha";
	
	$lang['subHeaderID']			= "IDENTITAT";
	$lang['subHeaderSubscript']		= "SUSCRIPCIO";
	$lang['subHeaderExpires']		= "VENÇ";
	$lang['subHeaderDPD']			= "DESCARREGUES PER DIA RESTANTS";
	$lang['subHeaderStatus']		= "ESTAT";

	$lang['downloads']				= "Descarregues";	
	$lang['thankRequest']			= "Gràcies per la seva sol·licitud. En breu ens posarem en contacte.";
	$lang['pleaseContactForm']		= "Si us plau, posi's  en contacte amb nosaltres per coneixer el preu d'aquest arxiu completant les dades del seguent formulari";
	$lang['downWithSubscription']	= "Descarregar fent servir suscripció";
	
$lang['noInstantDownload'] 		= "Aquest arxiu no está disponible per descàrrega immediata. Sera enviat per l'administrador del web a través de correu electronic quan en completi la compra.";
// Tickets
	$lang['newTicketsMessage']		= "Tickets nous o actualitzats";
	$lang['emptyTicket']			= "Aquest ticket és vuit.";
	$lang['messageID']				= "ID del Missatge";
	$lang['ticketNoReplies']		= "Aquest ticket esta tancat i no pot acceptar noves respostes.";
	$lang['ticketUpdated']			= "Aquest ticket fou Actualitzat!";
	$lang['ticketClosed']			= "Aquest ticket esta tancat!";
	$lang['closeTicket']			= "Tancar Ticket";
	$lang['newTicket']				= "Nou Ticket de Suport";
	$lang['newTicketButton']		= "TICKET NOU";
	$lang['ticketUnreadMes']		= "Aquest Ticket conte missatges nous o sense lleguir";
	$lang['ticketUnderAccount']		= "No hi ha tickets de Suport per aquest compta.";	
	$lang['ticketSubmitted']		= "El seu Ticket de suport fou enviat. Li respondrem l'avans possible.";
	
	$lang['mediaNotElidgiblePack']	= "Aquest contingut no es pot escollir per agregar-lo a un Pack!.";
	$lang['noBills']				= "No hi ha Factures per aquest compte.";
	
	$lang['lightbox']				= "preselecció";
	$lang['noLightboxes']			= "No te preseleccions.";
	$lang['lightboxDeleted']		= "La preselecció fou Borrada.";
	$lang['lbDeleteVerify']			= "Esta Segur que vol BORRAR la preselecció?";
	$lang['newLightbox']			= "NOVA PRESELECCIÓ";
	$lang['lightboxCreated']		= "S'ha creat una preselecció.";
	$lang['addToLightbox']			= "Agregar a la preselecció";
	$lang['createNewLightbox']		= "Crear Nova preselecció";
	$lang['editLightbox']			= "Editar aquesta preselecció";
	$lang['addNotes']				= "Crear Notes";
	$lang['editLightboxItem']		= "Editar articles de la preselecció";
	$lang['removeFromLightbox']		= "TREURE DE LA PRESELECCIÓ";
	
	$lang['savedChangesMessage']	= "Els seus canvis han estat Guardats.";
	
	$lang['noSubs']					= "No hi ha suscripcions per aquest compta.";
	
	$lang['unpaidBills']			= "Factures Impagades";
	
$lang['notices']				= "Notificacions";
	
	$lang['subscription']			= "Suscripció";
	
	$lang['assignToPackage']		= "Asignar a un Pack";
	$lang['startNewPackage']		= "Crear Nou Pack";
	$lang['packagesInCart']			= "Packs en el seu Carret";
	
	$lang['id']						= "ID";
	$lang['summary']				= "Resum";
	$lang['status']					= "Estat";
	$lang['lastUpdated']			= "Darrera Actualizació";
	$lang['opened']					= "Obert";
	$lang['reply']					= "Respondre";
	
	$lang['required']				= "Obligatori";
	
	$lang['bills']					= "Factures";
	$lang['orders']					= "Compres";
	$lang['downloadHistory']		= "Historial de Descarreges";
	$lang['supportTickets']			= "Tickets de Suport";
	$lang['order']					= "Compra";
	
	
	$lang['packages']				= "Packs";
	$lang['featuredPackages']		= "Packs Destacats";
	
	$lang['products']				= "Productes";
	$lang['featuredProducts']		= "Productes Destacats";
	
	$lang['subscriptions']			= "Suscripcions";
	$lang['featuredSubscriptions']	= "Suscripcions Destacats";
	
	$lang['yourCredits']			= "ELS TEUS<br />CREDITS";
	$lang['featuredCredits']		= "Packs de Credits Destacats";
	
	$lang['media'] 					= "CONTINGUTS";
	$lang['mediaNav'] 				= "Contingut";
	$lang['featuredMedia']			= "Contingut Destacat";
	
	
	$lang['featuredItems'] 			= "Destacat";	
	$lang['showcasedContributors'] 	= "Colaboradors exhibits";
	
	
	$lang['galleryLogin']			= "Entrar a Galeria";
	
	$lang['forum'] 					= "Foros";

	$lang['randomMedia'] 			= "Contingut Aleatori";
	$lang['language'] 				= "idioma";
	
	$lang['priceCreditSep']			= "o";
	
	$lang['viewCollection']			= "Veure Colecció";
	
	$lang['loggedInAs']				= "Conectat com";
	
	$lang['editProfile']			= "EDITAR COMPTA";
	
$lang['noItemCartWarning']		= "Has de seleccionar alguna foto avans de poder agregar aquest article al seu carret!";
	$lang['cartTotalListWarning']	= "Aquest valors son estimacions i pot variar lleugerament degut a la fluctuació de la moneda.";
	$lang['applyCouponCode']		= "Utilitzar Cupo de descompte";
	$lang['billMeLater']			= "Cobrarme despres";
	$lang['billMeLaterDescription']	= "Li anem a facturar mensualment per les seves compres.";
	$lang['shippingOptions']		= "Opcions d'enviament";
	$lang['paymentOptions']			= "Opcions de pagament";
	$lang['chooseCountryFirst']		= "Esculli el Païs Primer";
	$lang['paymentCancelled']		= "El seu pagament ha estat cancel·lat. Si vol pot provar a fer la compra de nou."; 
	$lang['paymentDeclined']		= "El seu pagament ha estat refusat.Si vol pot provar a fer la compra de nou."; 
		
	$lang['generalInfo']			= "Informacio Personal";
	$lang['membership']				= "Membresia";
	$lang['preferences']			= "Preferencies";
	$lang['address']				= "Adreça";
	$lang['actions']				= "Accions";
	$lang['changePass']				= "Canviar Contrasenya";
	$lang['changeAvatar']			= "Canviar Avatar";
	$lang['bio']					= "Bio";
	$lang['contributorSettings']	= "Configuracio de Colaborador";
	$lang['edit']					= "EDITAR";
	
	$lang['leftToFill']				= "Articles restants a omplir";
	
	$lang['commissionMethod']		= "Forma de pagament de les comissions"; 
	$lang['commission']				= "Comissio";
	
	$lang['signupDate']				= "Membre desde";
	$lang['lastLogin']				= "Darrera Sesio";
$lang['clientName']				= "Nom del Client";
	$lang['eventCode']				= "Codi de l'event";
	$lang['eventDate']				= "Data de l'event";
	$lang['eventLocation']			= "Lloc de l'event";
	
	$lang['viewPackOptions']		= "Veure el contingut del pack i opcions";
	$lang['viewOptions']			= "Veure Opcions";
	
	$lang['remove']					= "BORRAR";
	$lang['productShots']			= "Imatges del Producte";
	$lang['currentPass']			= "Contrasenya actual";
	$lang['newPass']				= "Contrasenya nova";
	$lang['vNewPass']				= "Verifiqui la nova contrasenya";
	$lang['verifyPass']				= "Verifiqui Contrasenya";
	$lang['firstName']				= "Nom";
	$lang['lastName']				= "Cognom";
	$lang['location']				= "Ubicacio";
	$lang['memberSince']			= "Membre desde";
	$lang['portfolio']				= "PORFOLIO";
	$lang['profile']				= "Perfil";
	$lang['address']				= "Adreça";
	$lang['city']					= "Ciutat";
	$lang['state']					= "Estat/Provincia";
	$lang['zip']					= "Codi postal";
	$lang['country']				= "Païs";
	$lang['save'] 					= "GUARDAR";
	$lang['add'] 					= "AGREGAR";
	$lang['companyName']			= "Companyia";
	$lang['accountStatus']			= "Estat de la Compte";
	$lang['website']				= "Pagina Web";
	$lang['phone']					= "Telefon";
	$lang['email'] 					= "Email";
	$lang['name'] 					= "Nom";
	$lang['submit'] 				= "ENVIAR";	
	$lang['message'] 				= "Missatge";	
	$lang['question']				= "Consulta";
	$lang['password'] 				= "Contrasenya";	
	$lang['members'] 				= "Membres";
	$lang['visits'] 				= "Visites";	
	$lang['logout'] 				= "Sortir";
	$lang['lightboxes']				= "preseleccions";
	$lang['cart']					= "Carret";
	$lang['cartItemAdded']			= "L'article s'ha afegit al seu carret.";
	$lang['includesTax']			= "Inclou impostos TAX/VAT/IVA";
	$lang['addToCart']				= "AFEGIR AL CARRET";
	$lang['items']					= "Articles";
	$lang['item']					= "Article";
	$lang['qty']					= "Quantitat";
	$lang['price']					= "Preu";
	
	$lang['more']					= "Mes";
	$lang['moreInfo']				= "Mes Info";
	$lang['back']					= "Tornar";
	$lang['none']					= "Cap";
	$lang['details']				= "Detalls";
	$lang['options']				= "Opcions";
	$lang['page']					= "Pagina";
	$lang['ratingSubmitted']		= "Valoracio enviada";
	$lang['noMedia']				= "Aquesta galeria esta vuida";
	$lang['itemsTotal']				= "Total d'articles";
	$lang['of']						= "de";
	$lang['view']					= "VEURE";
	$lang['apply']					= "APLICAR";
	$lang['currency']				= "Canviar Moneda";
	$lang['active']					= "Activar";
	$lang['pending']				= "Pendient";
	$lang['freeTrial']				= "Prova Gratis";
	$lang['setupFee']				= "Cost de la Configuracio";
	$lang['free']					= "Gratis";
	$lang['open']					= "Obrir";
	$lang['close']					= "Tancar";
	$lang['closed']					= "Tancat";
	$lang['by']						= "per";
	$lang['download']				= "Descarregar";
	$lang['downloadUpper']			= "DESCARREGAR";
	$lang['KB']						= "K";
	$lang['MB']						= "MB";
	$lang['files']					= "Archius";
	$lang['unknown']				= "Desconegut";
	$lang['freeDownload']			= "Descàrrega Gratis";
	$lang['prevDown']				= "Descàrrega Previa";
	$lang['pay']					= "PAGAR";
	$lang['purchaseCredits']		= "COMPRAR CREDITS";
	$lang['purchaseSub']			= "COMPRAR UNA SUSCRIPCIO";
	$lang['hour'] 					= "Hora";
	$lang['slash'] 					= "Slash";
	$lang['period'] 				= "Periode";
	$lang['dash'] 					= "Dash";
	$lang['gmt'] 					= "GMT";
	$lang['avatar'] 				= "Avatar";
	$lang['delete'] 				= "BORRAR";
	$lang['uploadAvatar']			= "Pujar Avatar";
	$lang['welcome']				= "Benvingut";
	$lang['expires']				= "Vence";
	$lang['msExpired']				= "La Seguent membresia a vençut";
	$lang['newSales']				= "Noves vendes desde el seu darrer inici de sesio.";
	$lang['never']					= "Mai";
	$lang['yes']					= "SI";
	$lang['no']						= "NO";
	$lang['create'] 				= "CREAR";
	$lang['cancel'] 				= "CANCEL·LAR";
	$lang['notes'] 					= "Notes";
	$lang['update'] 				= "ACTUALITZAR";
	$lang['each'] 					= "Cada";
	$lang['enterKeywords']			= "Ingresi Paraula(es) Clau";
	$lang['send']					= "ENVIAR";
	$lang['emailTo']				= "Email a";
	$lang['yourName']				= "El seu nom";
	$lang['yourEmail']				= "El seu mail";
	$lang['emailToFriend']			= "Enviar per mail l'Enllaç a aquest contingut";
	$lang['emailToFriendSent']		= "S'ha enviat un correu electronic! Voste pot enviar unaltre o tancat aquesta finestra.";
	$lang['linkSentBy']				= "L'enllaç a una foto o video se li ha enviat per";
	$lang['newPackageMessage']		= "Inicii un nou pack o seleccioni un pack similar del seu carret";
	$lang['warning']				= "Advertencia!";
	$lang['estimated']				= "Estimat";
	$lang['doneUpper']				= "LLEST";
	$lang['returnToSiteUpper']		= "TORNAR A LA PLANA WEB";
	$lang['chooseCountryFirst']		= "Primer esculli el Pais";
	$lang['advancedSearch']			= "Búsqueda Avançada";
	$lang['eventSearch']			= "Búsqueda d'event";	
	$lang['AND']					= "I";
	$lang['OR']						= "O";
	$lang['NOT']					= "NO";
	$lang['noAccess']				= "Vosté no te permisos per veure aquesta seccio.";
	$lang['siteStats']				= "Estadistiques del Web";
	$lang['membersOnline']			= "Usuaris Online";

	$lang['minutesAgo']				= "Minuts enrera";

	
	$lang['seconds']				= "Segons";
	
	$lang['megabytesAbv']			= "MB";
	
	$lang['downWithSub']			= "Descarregar utilitzant Suscripció";
	$lang['downloadsRemaining']		= "descarregues restants avui";
	$lang['requestDownloadSuccess']	= "Hem rebut la seva sol·licitud per aquest arxiu i contactarem amb voste el mes aviat possible.";
	
	$lang['mediaLicenseNFS']		= "No per la Venda";
	$lang['mediaLicenseRF']			= "Lliure de drets";
	$lang['mediaLicenseRM']			= "Drets protegits";
	$lang['mediaLicenseFR']			= "Gratis";
	$lang['mediaLicenseCU']			= "Contacti'ns";
	
	$lang['original']				= "Original";
	
	$lang['prevDownloaded']			= "Voste ha descarregat anteriorment aquest arxiu. No hi ha cap cost per descarregar-lo de nou.";
	$lang['instantDownload']		= "descàrrega Immediata";
	$lang['requestDownload']		= "SOL·LICITAR DESCÀRREGA";
	$lang['request']				= "SOL·LICITAR";
	$lang['enterEmail']				= "Ingressi el seu mail";
	
	$lang['license']				= "Llicència";
	$lang['inches']					= "Polzades";
	$lang['centimeters']			= "Centimetres";
	$lang['dpi']					= "DPI";
$lang['noSimilarMediaMessage']	= "No s'han trobat continguts similars";
	$lang['backUpper']				= "TORNAR";
	$lang['nextUpper']				= "SEGUENT";
	$lang['prevUpper']				= "PREVI";
	$lang['curGalleryOnly']			= "Nomes galeria actual";
	$lang['sortBy']					= "Ordenar per";
	$lang['galSortColor']			= "Color";
	$lang['galSortDate']			= "Agregada el";
	$lang['galSortID']				= "ID";
	$lang['galSortTitle']			= "Titol";
	$lang['galSortFilename']		= "Nom d'arxiu";
	$lang['galSortFilesize']		= "Tamany de l'arxiu";
	$lang['galSortSortNum']			= "Ordenar per Numero";
	$lang['galSortBatchID']			= "ID de Lot";
	$lang['galSortFeatured']		= "Destacades";
	$lang['galSortWidth']			= "Ample";
	$lang['galSortHeight']			= "Llarg";
	$lang['galSortViews']			= "Vistes";
	$lang['galSortAsc']				= "Ascendent";
	$lang['galSortDesc']			= "Descendent";
	$lang['mediaIncludedInColl']	= "Aquest Contingut esta inclos a les seguents col·leccions.";
	
	$lang['billHeaderInvoice']		= "FACTURA";
	$lang['billHeaderDate']			= "DATA DE FACTURA";
	$lang['billHeaderDueDate']		= "DATA DE VENCIMIENT";
	$lang['billHeaderTotal']		= "TOTAL";
	$lang['billHeaderStatus']		= "ESTAT";

// Cart
	$lang['creditsWarning']			= "Voste no te suficients crédits per fer la compra. Si us plau, Inicii la sessio o afegeixi crédits al seu carret.";
	$lang['subtotalWarning']		= "La compra minima és:";
	$lang['pleaseWait']				= "Si us plau, esperi mentre procesem la seva solicitut!";
	$lang['orderMailinThanks']		= "Gràcies. Hem rebut la seva comanda. Si us plau, Faci la seva tranferencia o ingres per la seguent quantitat al compte indicat. Un cop es rebi el pagament, s'aprovarà la seva Compra.";
	$lang['billMailinThanks']		= "Si us plau, envíe la seva tranferencia o ingres por la seguent quantitat al compte indicat.  Un cop es rebi el pagament, la seva factura es marcara com pagada.";
	$lang['mailinRef']				= "Si us plau faci referencia al seguent Numero quan realitci el seu pagament";
	$lang['subtotal']				= "Subtotal";
	$lang['shipping']				= "Enviament";
	$lang['discounts']				= "Descomptes/Cupons";
	$lang['total']					= "Total";
	$lang['creditsSubtotal']		= "SubTotal crédits";
	$lang['creditsDiscounts']		= "Descompte crédits";
	$lang['credits']				= "crédits";
	$lang['reviewOrder']			= "Revisar la seva Compra";
	$lang['payment']				= "Pagament";
	$lang['cartNoItems']			= "No te articles en el seu carret!";
	$lang['enterShipInfo']			= "Si us plau, introdueixi les seves dades d'enviament i l'informacio de facturacio a continuacio";
	$lang['sameAsShipping']			= "Igual que l'adreça d'enviament";
	$lang['differentAddress']		= "Proporcionar una adreça diferent";
	$lang['noShipMethod']			= "No existeixen metodes d'enviament per enviar aquests articles.";	
	$lang['choosePaymentMethod']	= "Si us plau, escolli un metode de pagament";	
	$lang['discountCode']			= "CODI DE DESCOMPTE";
	$lang['use']					= "Utilitzar";
	$lang['continueNoAccount']		= "o continuar sense una compte d'usuari a continuació";
	
// Order
	$lang['yourOrder']				= "la seva Compra";
	$lang['viewInvoice']			= "VEURE FACTURA";
	$lang['totalsShown']			= "Mostrar Totals ";
	$lang['downloadExpired']		= "La descàrrega ha finalitzat";
	$lang['downloadExpiredMes']		= "Aquest enllaç ha finalitzat. Si us plau, posi's en contacte amb nosaltres per que li reactivem.";
	$lang['downloadMax']			= "Superada quantitat Maxima de descarregues";
	$lang['downloadMaxMes']			= "Voste ha arribat al numero maxim de descarregues permeses per aquest arxiu. Si us plau, posi's en contacte amb nosaltres per tenir l'enllaç de descàrrega reactivat.";
	$lang['downloadNotApproved']	= "Compra No Aprovada";
	$lang['downloadNotApprovedMes']	= "No es pot descarregar aquest arxiu fins que la seva compra s'hagi aprovat. Si us plau, provi-ho mes tard.";	
	$lang['downloadNotAvail']		= "descàrrega inmediata no disponible";
	$lang['downloadNotAvailMes']	= "Aquest arxiu no está disponible per descàrrega inmediata. Si us plau, utilitzi el boto de sol·licitud de sota per que aquest arxiu li sigui enviat per correu electronic.";
	$lang['orderNumber']			= "Numero de Compra";
	$lang['orderPlaced']			= "Compra Realitzada"; 
	$lang['orderStatus']			= "Estat de La Compra";
	$lang['paymentStatus']			= "Estat del Pagament";
	
	// IPTC
	$lang['iptc']					= "IPTC";
	$lang['iptc_title']				= "Titol";
	$lang['iptc_description']		= "Descripcio";
	$lang['iptc_instructions']		= "Instruccions";
	$lang['iptc_date_created']		= "Data de Creacio";
	$lang['iptc_author']			= "Autor";
	$lang['iptc_city']				= "Ciutat";
	$lang['iptc_state']				= "Estat/Provincia";
	$lang['iptc_country']			= "Pais";
	$lang['iptc_job_identifier']	= "Instruccions";
	$lang['iptc_headline']			= "Identificador de tasca";
	$lang['iptc_provider']			= "Proveidor";
	$lang['iptc_source']			= "Font";
	$lang['iptc_description_writer']= "Descripcio del Escritor";
	$lang['iptc_urgency']			= "Urgencia";
	$lang['iptc_copyright_notice']	= "Avis de Copyright";
	
$lang['exif']							= "EXIF";
	$lang['exif_FileName']					= "Nom d'arxiu";
	$lang['exif_FileDateTime']				= "Data/Hora de l'arxiu";
	$lang['exif_FileSize']					= "Tamany d'arxiu";
	$lang['exif_FileType']					= "Tipus d'arxiu";
	$lang['exif_MimeType']					= "Tractament";
	$lang['exif_SectionsFound']				= "Seccions trobades";
	$lang['exif_ImageDescription']			= "Descripcio de l'imatge";
	$lang['exif_Make']						= "Fet a";
	$lang['exif_Model']						= "Model";
	$lang['exif_Orientation']				= "Orientacio";
	$lang['exif_XResolution']				= "Resolucio X";
	$lang['exif_YResolution']				= "Resolucio Y";
	$lang['exif_ResolutionUnit']			= "Resolutio Unit";
	$lang['exif_Software']					= "Software";
	$lang['exif_DateTime']					= "Data/Hora";
	$lang['exif_YCbCrPositioning']			= "Posicionament YCbCr";
	$lang['exif_Exif_IFD_Pointer']			= "Exif IFD Pointer";
	$lang['exif_GPS_IFD_Pointer']			= "GPS IFD Pointer";
	$lang['exif_ExposureTime']				= "Temps d'exposicio";
	$lang['exif_FNumber']					= "Diafragma";
	$lang['exif_ExposureProgram']			= "Programa d'exposicio";
	$lang['exif_ISOSpeedRatings']			= "ISO";
	$lang['exif_ExifVersion']				= "Versio Exif";
	$lang['exif_DateTimeOriginal']			= "Data/Hora de l'Original";
	$lang['exif_DateTimeDigitized']			= "Data/Hora Digitalitzada";
	$lang['exif_ComponentsConfiguration']	= "Configuracio de Components";
	$lang['exif_ShutterSpeedValue']			= "Velocitat de l'obturador";
	$lang['exif_ApertureValue']				= "Diafragma";
	$lang['exif_MeteringMode']				= "Modus de Medicio";
	$lang['exif_Flash']						= "Flash";
	$lang['exif_FocalLength']				= "Lent Focal";
	$lang['exif_FlashPixVersion']			= "Versio Flash Pix";
	$lang['exif_ColorSpace']				= "Espai de Color";
	$lang['exif_ExifImageWidth']			= "Exif Ample d'imatge";
	$lang['exif_ExifImageLength']			= "Exif Llarg d'imatge";
	$lang['exif_SensingMethod']				= "Metode de deteccio";
	$lang['exif_ExposureMode']				= "Modus d'exposicio";
	$lang['exif_WhiteBalance']				= "Balanç de blancs";
	$lang['exif_SceneCaptureType']			= "Tipus de Captura d'escena";
	$lang['exif_Sharpness']					= "Nitidesa";
	$lang['exif_GPSLatitudeRef']			= "Latitud Ref GPS";
	$lang['exif_GPSLatitude_0']				= "Latitud GPS 0";
	$lang['exif_GPSLatitude_1']				= "Latitud GPS 1";
	$lang['exif_GPSLatitude_2']				= "Latitud GPS 2";
	$lang['exif_GPSLongitudeRef']			= "Longitud Ref GPS";
	$lang['exif_GPSLongitude_0']			= "Longitud GPS 0";
	$lang['exif_GPSLongitude_1']			= "Longitud GPS 1";
	$lang['exif_GPSLongitude_2']			= "Longitud GPS 2";
	$lang['exif_GPSTimeStamp_0']			= "GPS Timestamp 0";
	$lang['exif_GPSTimeStamp_1']			= "GPS Timestamp 1";
	$lang['exif_GPSTimeStamp_2']			= "GPS Timestamp 2";
	$lang['exif_GPSImgDirectionRef']		= "GPS Imgage Direction Reference";
	$lang['exif_GPSImgDirection']			= "GPS Imgage Direction";
	
	$lang['mediaLabelTitle']		= "Titòl";
	$lang['mediaLabelDesc']			= "Descripció";
	$lang['mediaLabelCopyright']	= "Copyright";
	$lang['mediaLabelRestrictions']	= "Restriccions d'ús";
	$lang['mediaLabelRelease']		= "Autorizacio del model";
	$lang['mediaLabelKeys']			= "Paraules clau";
	$lang['mediaLabelFilename']		= "Nom de l'arxiu";
	$lang['mediaLabelID']			= "ID";
	$lang['mediaLabelDate']			= "Agregada";
	$lang['mediaLabelDateC']		= "Creat";
	$lang['mediaLabelDownloads']	= "descarregues";
	$lang['mediaLabelViews']		= "Vistes";
	$lang['mediaLabelPurchases']	= "Compres";
	$lang['mediaLabelColors']		= "Colors";
	$lang['mediaLabelPrice']		= "Preu";
	$lang['mediaLabelCredits']		= "credits";
	$lang['mediaLabelOwner']		= "Propietari";
	$lang['mediaLabelUnknown']		= "Desconegut";
	$lang['mediaLabelResolution']	= "Pixels";
	$lang['mediaLabelFilesize']		= "Tamany de l'arxiu";
	$lang['mediaLabelRunningTime']	= "Temps de Duració";
	$lang['mediaLabelFPS']			= "FPS";
	$lang['mediaLabelFormat']		= "Format";
// Search
	$lang['search']					= "Buscar";
	$lang['searchEnterKeyMessage']	= "Si us plau, introdueixi les paraules clau per comenzar la seva búsqueda.";
	$lang['searchNoResults']		= "la seva búsqueda no ha trobat coincidencies";
	$lang['searchDateRange']		= "Buscar en rang de dates";
	$lang['searchNarrow']			= "Acotar resultats de búsqueda";
	$lang['searchStart']			= "Comenci la seva búsqueda sota";
	$lang['searchAll']				= "Tots";
	$lang['searchClear']			= "Borrar búsqueda";
	$lang['searchKeywords']			= "Paraules clau";
	$lang['searchTitle']			= "Titol";
	$lang['searchDescription']		= "Descripcio";
	$lang['searchFilename']			= "Nom d'arxiu";
	$lang['searchPortrait']			= "Retrat";
	$lang['searchLandscape']		= "Paisatge";
	$lang['searchSquare']			= "Plaça";
	$lang['searchRoyaltyFree']		= "Lliure de drets";
	$lang['searchRightsManaged']	= "Drets protegits";
	$lang['searchFree']				= "Gratis";
	$lang['searchContactUs']		= "Contacti'ns";
	$lang['searchHeaderKeywords']	= "Paraules clau";
	$lang['searchHeaderFields']		= "camps";
	$lang['searchHeaderType']		= "Tipus";
	$lang['searchHeaderOrientation']= "Orientacio";
	$lang['searchHeaderColor']		= "Color";
	$lang['searchHeaderGalleries']	= "Galeries";	
	
?>