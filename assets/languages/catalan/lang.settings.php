<?php
/*Català*/
$langset['version'] = "4.6.1";
$langset['active'] = "1";
$langset['translatedBy'] = "Ktools Member - Francesc Casals Corral";
$langset['mgmtAreaTrans'] = "1";
$langset['lang_charset'] = "utf-8";
$langset['id'] = "catalan";
$langset['locale'] = "cat";
$langset['xmlLangCode'] = "cat";
$langset['name'] = "Catala";
$langset['date_format'] = "US";
$langset['clock_format'] = "12";
?>