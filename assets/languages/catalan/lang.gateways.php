<?php
	# 4.6.3
	# STRIPE
	$lang['stripe_displayName']			= "Stripe";
	$lang['stripe_publicDescription']	= "Credit Card or Debit Card";
	$lang['stripe_pkey']				= "Publishable Key";
	$lang['stripe_pkey_d']				= "Your publishable key for your stripe account.";
	$lang['stripe_skey']				= "Secret Key";
	$lang['stripe_skey_d']				= "Your secret key for your stripe account.";
	
# 4.1.4
	# MOLLIE IDEAL
	$lang['mollieideal_displayName']		= "iDeal (Mollie)";
	$lang['mollieideal_f_partnerid']		= "Partner ID";
	$lang['mollieideal_f_partnerid_d']		= "Your iDeal Partner ID.";
	$lang['mollieideal_f_profilekey']		= "Profile key";
	$lang['mollieideal_f_profilekey_d']		= "Your Mollie iDeal Profile key.";
	$lang['mollieideal_f_testmode']			= "Modo Prueva";
	$lang['mollieideal_f_testmode_d']		= "Pasar a modus de prova la pasarela de pagament Mollie iDeal.";
	$lang['mollieideal_instructions']		= "Assegureu-vos que les dades del mode de prova correspon al seu compte Mollie.nl";
	$lang['mollieideal_publicDescription']	= "Pagaments segurs fent servir iDeal (Només disponible a Holanda).";

	# 4.1
	# ONEBIP 
	$lang['onebip_displayName']		= "OneBip"; 
	$lang['onebip_merchantid']			= "Email";	
	$lang['onebip_merchantid_d']		= "Entri al email usat pel teu compte onebip. "; 
	$lang['onebip_merchantkey']		= "API Key"; 
	$lang['onebip_merchantkey_d']		= "Entri la API key de la seva compte onebip.";
	$lang['onebip_testmode']			= "Modus prova";
	$lang['onebip_testmode_d']			= "Pasar a modus de prova la pasarel·la de pagament onebip.";
	$lang['onebip_publicDescription']	= "Pay by Mobile";
	
	# PAYFAST 
	$lang['payfast_displayName']		= "PayFast"; 
	$lang['payfast_merchantid']			= "Merchant ID";	
	$lang['payfast_merchantid_d']		= "Enter the merchant ID for your PayFast Account."; 
	$lang['payfast_merchantkey']		= "Merchant Key"; 
	$lang['payfast_merchantkey_d']		= "Enter the merchant key for your PayFast Account.";
	$lang['payfast_testmode']			= "Modo Prueva"; 
	$lang['payfast_testmode_d']			= "Pasar a modus de prova la pasarel·la de pagament PayFast.";
	$lang['payfast_publicDescription']	= "Credit Card or Bank Account"; 

/*Spanish*/
$lang['nochex_displayName'] = "Nochex";
$lang['nochex_f_accountid'] = "ID del compte";
$lang['nochex_f_accountid_d'] = "Su Nochex ID de la cuenta.";
$lang['nochex_f_testmode'] = "Prova el modus de";
$lang['nochex_f_testmode_d'] = "Posa el teus pagaments Nochex a modus de prova.";
$lang['nochex_publicDescription'] = "De Crédit o Débit";
$lang['worldpay_displayName'] = "WorldPay";
$lang['worldpay_f_installid'] = "ID d'instalació";
$lang['worldpay_f_installid_d'] = "ID d'instalació del seu compte WorldPay.";
$lang['worldpay_f_testmode'] = "Prova el modus de";
$lang['worldpay_f_testmode_d'] = "Posa els teus pagaments WorldPay en modus de prova.";
$lang['worldpay_publicDescription'] = "De Crédit o Débit";
$lang['robokassa_displayName'] = "RoboKassa";
$lang['robokassa_f_merchantid'] = "D'identificació del comerciant";
$lang['robokassa_f_merchantid_d'] = "D'identificació del comerciant per la seva compte RoboKassa.";
$lang['robokassa_f_merchantpass'] = "Mercante Contrasenya";
$lang['robokassa_f_merchantpass_d'] = "El seu comerciant RoboKassa pase d'identificació.";
$lang['robokassa_publicDescription'] = "De Crédit o Débit";
$lang['paygate_displayName'] = "PayGate";
$lang['paygate_f_accountid'] = "PayGate ID";
$lang['paygate_f_accountid_d'] = "ID de la seva compte PayGate.";
$lang['paygate_f_accountkey'] = "PayGate clau";
$lang['paygate_f_accountkey_d'] = "L'identificador de clau PayGate.";
$lang['paygate_f_testmode'] = "Prova el modus de";
$lang['paygate_f_testmode_d'] = "Posa els teus pagaments PayGate en modus de prueba.";
$lang['paygate_f_testid'] = "Prova d'identificació PayGate";
$lang['paygate_f_testid_d'] = "Tu ID de Prova PayGate";
$lang['paygate_f_testkey'] = "Prova PayGate clau";
$lang['paygate_f_testkey_d'] = "La seva clau de prova de PayGate";
$lang['paygate_publicDescription'] = "De Crédit o Débit";
$lang['paystation_displayName'] = "Caixer automàtic";
$lang['paystation_f_accountid'] = "Paystation ID";
$lang['paystation_f_accountid_d'] = "ID del seu compte de caixer automàtic.";
$lang['paystation_f_testmode'] = "Prova el modus de";
$lang['paystation_f_testmode_d'] = "Posa els teus pagaments caixer automàtic en modus de prova.";
$lang['paystation_f_gatewayid'] = "Porta d'enllaç de ID";
$lang['paystation_f_gatewayid_d'] = "El seu Paystation ID porta d'enllaç.";
$lang['paystation_publicDescription'] = "De Crèdit o Dèbit";
$lang['skrill_displayName'] = "Skrill (moneybookers)";
$lang['skrill_f_email'] = "Direcció de correu electrònic";
$lang['skrill_f_email_d'] = "Direcció de correu electrònic del seu compte Skrill.";
$lang['skrill_f_testmode'] = "Prova el modus de";
$lang['skrill_f_testmode_d'] = "Posa els teus pagaments Skrill en modus de prova.";
$lang['skrill_f_testemail'] = "Prova de E-mail";
$lang['skrill_f_testemail_d'] = "La seva prova Skrill direcció de correu electrònic.";
$lang['skrill_publicDescription'] = "De Crèdit o Dèbit";
$lang['skrill_f_completeOrder'] = "Fes clic aquí per completar la seva comanda!";
$lang['chronopay_displayName'] = "ChronoPay";
$lang['chronopay_f_clientid'] = "Identificació de client";
$lang['chronopay_f_clientid_d'] = "La seva ChronoPay ID de client.";
$lang['chronopay_f_siteid'] = "ID del lloc";
$lang['chronopay_f_siteid_d'] = "La seva ChronoPay ID del seu lloc.";
$lang['chronopay_f_productid'] = "ID del producte.";
$lang['chronopay_f_productid_d'] = "El seu ChronoPay ID de producte.";
$lang['chronopay_publicDescription'] = "De Crèdit o Dèbit";
$lang['ideal_displayName'] = "iDeal";
$lang['ideal_f_accountid'] = "ID del compte";
$lang['ideal_f_accountid_d'] = "El seu número de compte iDeal.";
$lang['ideal_f_transkey'] = "Clau secreta";
$lang['ideal_f_transkey_d'] = "La seva clau secreta iDeal.";
$lang['ideal_f_testmode'] = "Prueba el modo de";
$lang['ideal_f_testmode_d'] = "Posa els teus pagaments ideals en modus de prova.";
$lang['ideal_publicDescription'] = "Compte bancària";
$lang['paypal_displayName'] = "PayPal";
$lang['paypal_f_email'] = "Direcció de correu electrònic";
$lang['paypal_f_email_d'] = "Direcció de correu electrònic del seu compte PayPal.";
$lang['paypal_f_testmode'] = "Prova el modus de";
$lang['paypal_f_testmode_d'] = "Posa els teus pagaments de PayPal en modus de prova.";
$lang['paypal_f_testemail'] = "Prova d' E-mail";
$lang['paypal_f_testemail_d'] = "La seva caixa de proves de PayPal direcció de correu electrònic.";
$lang['paypal_publicDescription'] = "PayPal, targeta de crèdit o compte bancària";
$lang['2checkout_displayName'] = "2Checkout.com";
$lang['2checkout_f_accountid'] = "ID de la compte";
$lang['2checkout_f_accountid_d'] = "La teva ID de la compte de 2Checkout.";
$lang['2checkout_f_testmode'] = "Prova el modus de";
$lang['2checkout_f_testmode_d'] = "Posa els teus pagaments 2Checkout en modus de prova.";
$lang['2checkout_instructions']		= "In 2Checkout.com set direct return to 'Header Redirect (Your URL)' and set approved URL to http://www.YOUR_DOMAIN_NAME.com/assets/gateways/2checkout/ipn.php";
$lang['2checkout_publicDescription'] = "Targeta de crèdit o compte bancària de EE.UU.";
$lang['plugnpay_displayName'] = "Pagament Plug n &#39;";
$lang['plugnpay_f_accountid'] = "ID del compte";
$lang['plugnpay_f_accountid_d'] = "El seu ID Plug n &#39;Pay compte.";
$lang['plugnpay_publicDescription'] = "";
$lang['authorize_displayName'] = "Authorize.net";
$lang['authorize_f_apiid'] = "API de usuari ID";
$lang['authorize_f_apiid_d'] = "El API de Authorize.net usuari ID.";
$lang['authorize_f_transkey'] = "Operació clau";
$lang['authorize_f_transkey_d'] = "La seva Authorize.net clau de Transacció.";
$lang['authorize_f_testmode'] = "Prova el modus de";
$lang['authorize_f_testmode_d'] = "Posi els seus pagaments de Authorize.net en modus de prova.";
$lang['authorize_publicDescription'] = "";
$lang['authorize_f_completeOrder'] = "Faci clic aquí per completar la seva comanda!";
$lang['mygate_displayName'] = "MyGate.co.za";
$lang['mygate_f_merchantid'] = "D'identificació del comerciant";
$lang['mygate_f_merchantid_d'] = "La seva MyGate.co.za identificació del comerciant.";
$lang['mygate_f_appid'] = "ID de aplicació";
$lang['mygate_f_appid_d'] = "La seva MyGate.co.za ID d'aplicació.";
$lang['mygate_publicDescription'] = "";
$lang['mygate_f_testmode'] = "Prova el modus de";
$lang['mygate_f_testmode_d'] = "Posa els teus pagaments mygate en modus de prova.";
$lang['mailin_displayName'] = "Correu en el pagament";
$lang['mailin_f_instructions'] = "Correu en intruccions de pagament";
$lang['mailin_f_instructions_d'] = "Les instruccions pels clients enviar correu electrònic en els pagaments, tals com xecs o transferències.";
$lang['mailin_publicDescription'] = "Envíi el seu pagament a l'adreça proporcionada en la seguent pàgina.";
?>