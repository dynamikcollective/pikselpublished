<?php
	# SPANISH
	$calendar = array();
	$calendar['long_month']		= array(1 => "Gener","Febrer","Març","Abril","Maig","Juny","Juliol","Agost","Setembre","Octubre","Novembre","Decembre");
	$calendar['short_month']	= array(1 => "Gen","Feb","Mar","Abr","Mai","Jun","Jul","Ago","Set","Oct","Nov","Dic");
	$calendar['full_days']		= array(1 => "Diumenge","Dilluns","Dimarts","Dimecres","Dijous","Divendres","Dissabte");
	$calendar['short_days']		= array(1 => "Diu","Dil","Dim","Dic","Dij","Div","Dis");
?>