<?php
	# 4.1.3
	$wplang['qstats_pending_media']	= "Médias des contributeurs en attente";
	
	# 4.0.7	
	$wplang['sitehealth_exif']	= "Support PHP EXIF (exif_read_data)";
	
	# GENERAL WIDGET LANGUAGE
	$wplang['widget_close']		= "Fermer";
	$wplang['widget_save']		= "Sauver";
	$wplang['load_failed'] 		= "Echec de chargement du panneau !";
	$wplang['widget_title']		= "Titre";
	$wplang['widget_note']		= "Notification";
	$wplang['widget_for']		= "De";
	
	# NOTES WIDGET
	$wplang['notes_title']		= "Notification";
	$wplang['notes_newnote']	= "Nouvelle notification";
	$wplang['notes_postedby']	= "Posté par";
	$wplang['notes_lastupdate']	= "Dernière mise à jour";
	$wplang['notes_nonotes']	= "Il n'y a pas de notification à afficher";
	
	# EXTRAS WIDGET
	$wplang['extras_title']		= "PhotoStore Extras";
	
	# KTOOLS ACCOUNT WIDGET
	$wplang['kaccount_title']	= "Compte Ktools";
	$wplang['kaccount_support']	= "Support/Upgrade jours restants";
	$wplang['kaccount_messages']= "Messages non lus";
	$wplang['kaccount_affil']	= "Vente d'Affiliation depuis la dernière session";
	
	# KNEWS WIDGET
	$wplang['knews_title']		= "Actualités Ktools.net ";
	
	# QUICK STATS WIDGET - NEW AND PENDING
	$wplang['qstats_title']		= "Nouveau & en attente";
	$wplang['qstats_logmem']	= "Membres depuis la dernière session";
	$wplang['qstats_penmem']	= "Membres en attente";
	$wplang['qstats_logorders']	= "Commandes depuis la dernière session";
	$wplang['qstats_penorders']	= "Commandes en attente";
	$wplang['qstats_logcomm']	= "Commentaires depuis la dernière session";
	$wplang['qstats_pencomm']	= "Commentaires en attente";
	$wplang['qstats_logtags']	= "Tags depuis la dernière session";
	$wplang['qstats_pentags']	= "Tags en attente";
	$wplang['qstats_lograte']	= "Evaluation depuis la dernière session";
	$wplang['qstats_penrate']	= "Evaluation en attente";
	$wplang['qstats_penbios']	= "Bio membres en attente";
	$wplang['qstats_penavatars']= "Avatars en attente";
	$wplang['qstats_pensupport']= "Billet du support en attente";
	
	# STATS WIDGET
	$wplang['stats_title']		= "Stats";
	$wplang['stats_op1']		= "Ventes";
	$wplang['stats_op2']		= "Membres";
	$wplang['stats_op_7days']	= "Les 7 derniers jours";
	$wplang['stats_op_6mon']	= "Les 6 derniers mois";
	$wplang['stats_op_5year']	= "Les 5 dernières années";
	$wplang['stats_tdsales']	= "Ventes du jour";
	$wplang['stats_tdorders']	= "Commandes du jour";
	$wplang['stats_sales']		= "Ventes";
	$wplang['stats_orders']		= "Commandes";	
	$wplang['stats_atsales']	= "Toutes les ventes";
	$wplang['stats_atorders']	= "Toutes les commandes";
	$wplang['stats_tdmems']		= "Nouveaux membres du jour";
	$wplang['stats_mems']		= "Nouveaux membres";
	$wplang['stats_tamems']		= "Total des membres actifs";
	$wplang['stats_timems']		= "Total des membres inactifs";
	
	
	# SITE HEALTH WIDGET
	$wplang['sitehealth_title']	= "Santé du site & du serveur";
	$wplang['sitehealth_ok']	= "OK";
	$wplang['sitehealth_low']	= "BAS";
	$wplang['sitehealth_high']	= "HAUT";
	$wplang['sitehealth_failed']= "ECHEC";
	$wplang['sitehealth_unava'] = "Non disponible";
	$wplang['sitehealth_off']	= "OFF";
	$wplang['sitehealth_on']	= "ON";
	$wplang['sitehealth_inst']	= "Installé";
	$wplang['sitehealth_none']	= "Rien";
	$wplang['sitehealth_exists']= "Existe";
	$wplang['sitehealth_write']	= "Inscriptible";
	$wplang['sitehealth_nonwri']= "Non inscriptible";
	$wplang['sitehealth_php']	= "Version de PHP";
	$wplang['sitehealth_gd']	= "Librairie GD";
	$wplang['sitehealth_mem']	= "Limite Mémoire du PHP";
	$wplang['sitehealth_exe']	= "PHP max_execution_time";
	$wplang['sitehealth_time']	= "PHP max_input_time";
	$wplang['sitehealth_file']	= "PHP upload_max_filesize";
	$wplang['sitehealth_post']	= "PHP post_max_size";
	$wplang['sitehealth_safe']	= "PHP safe_mode";
	$wplang['sitehealth_dbv']	= "Vérification de la Base de données / Version du produi";
	$wplang['sitehealth_mysqlv']= "Version MySQL";
	$wplang['sitehealth_load']	= "Chargement du serveur";
	$wplang['sitehealth_upti']	= "Disponibilité des serveurs";
	
	# UPDATE CHECK WIDGET
	$wplang['updater_title']	= "Vérifier les mises à jour";
	$wplang['updater_newest']	= "Vous avez la nouvelle version";
	$wplang['updater_update']	= "Mise à jour disponible";
	$wplang['updater_newestis']	= "La nouvelle version est "; 
	$wplang['updater_getnew']	= "Pour obtenir la nouvelle version, veuillez vous connecter à <a href='http://www.ktools.net/members/' target='_blank'> votre compte Ktools.net</a>";
	$wplang['updater_yourv']	= "Vous utilisez la version ";
	
	# BLANK WIDGET
	$wplang['blank_title']		= "Panneau vide";
	
	# CALENDAR WIDGET
	$wplang['calendar_title']	= "Calendrier";
	
	# TIPS CHECK WIDGET
	//$wplang['tips_title']		= "Tips";
	//$wplang['tips_next']		= "Prochain tips";
	//$welcometip[] 				= "Savez-vous que vous pouvez utiliser <strong>Ctrl+Shift+S</strong> pour ouvrir ou fermer les raccourcis de menu ?"; 
	//$welcometip[] 				= "You can drag these welcome panels to rearrange them the way you would like. Just click on the title and drag. Then drop the panel when it is in the order that you would like."; 
	//$welcometip[] 				= "For quick access to your galleries and many other areas click on the tab with the arrow in the upper left. This will open the shortcuts menu. Click the tab again to close it."; 
	
?>