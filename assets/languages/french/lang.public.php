<?php
	/******************************************************************
	*  FRANCAIS
	******************************************************************/
	
	
	# 4.7
	$lang['featuredGalleries']		= "Galeries à l'honneur";
	
	
	# 4.6.3	
	$lang['ccMessage']				= "SVP ! Ajouter vos informations de carte de crédits ci-dessous.";
	$lang['ccNumber']				= "Numéro de carte";
	$lang['ccCVC']					= "Code de vérification";
	$lang['ccExpiration']			= "Date d'expiration (MM/AAAA)";
	
	
	# 4.6.1
	$lang['passwordLeaveBlank']		= "Laisser vide pour ne pas mettre de mot de passe";
	$lang['myAlbums']				= "Mes albums";
	
	# 4.5
	$lang['dateDownloadUpper']		= "DATE DU TELECHARGEMENT";
	$lang['downloadTypeUpper']		= "TYPE DE TELECHARGEMENT";
	$lang['rss']					= "RSS";
	
	# 4.4.8
	$lang['uploadFile']				= "Envoyer un fichier";
	
	# 4.4.7
	$lang['contrSmallFileSize']		= "la taille du fichier est trop petite. Le fichier doit faire au moins.";
	# plupload
	$lang['plupSelectFiles']		=  "Sélectionner les fichiers";
	$lang['plupAddFilesToQueue']	=  "Ajouter les fichiers dans la file de téléchargement et cliquer sur le bouton 'démarrer'.";
	$lang['plupFilename']			=  "Nom du fichier";
	$lang['plupStatus']				=  "Statut";
	$lang['plupSize']				=  "Taille";
	$lang['plupAddFiles']			=  "Ajouter fichiers";
	$lang['plupStartUplaod']		=  "Démarrer l'envoi";
	$lang['plupStopUpload']			=  "Stopper l'envoi en cours";
	$lang['plupStartQueue']			=  "Démarrage de la queue d'envoi";
	$lang['plupDragFilesHere']		=  "Déposer vos fichiers ici.";
	
	# 4.4.6
	$lang['batchUploader']			= "Envoi groupé";
	$lang['uploader'][1]			= "Basé sur JAVA";
	$lang['uploader'][2]			= "Basé sur HTML5/Flash";
	$lang['change']					= "Changer";
	
	# 4.4.5
	$lang['moreInfoPlus']				= "[Plus d'informations]";
	
	# 4.4.3
	$lang['mediaLabelPropRelease']	= "Droit de propriété";
	
	# 4.4.2
	$lang['go']						= "GO";
	
	# 4.4.0
	$lang['share'] 					= "Partager";
	$lang['bbcode'] 				= "BBCode";
	$lang['html'] 					= "HTML";
	$lang['link']					= "Lien";
	$lang['pricingCalculator']		= "Calculateur de prix";
	$lang['noOptionsAvailable']		= "Pas d'options disponible";
	$lang['viewCartUpper']			= "VOIR LE PANIER";
		
	# 4.3
	$lang['vatIDNumber']			= "ID de la TVA";
	
	# 4.2.1
	$lang['captchaError']			= "Le code de vérification captcha est incorrect. Votre message n'a pas été envoyé.";

	# 4.1.7
	$lang['mediaLicenseEU'] 		= "Usage éditorial";
	
	# 4.1.6
	$lang['yourBill'] 				= "Votre facture";
	$lang['invoiceNumber'] 			= "Facture numéro";
	$lang['paymentThanks'] 			= "Merci de votre paiement.";
	$lang['msActive'] 				= "Votre abonnement est maintenant activé.";
	$lang['today'] 					= "Aujourd'hui";
	$lang['downloadsRemainingB']	= "Téléchargement restant";

	# 4.1.4
	$lang['link'] 					= "Lien";
	$lang['optional'] 				= "Optionel";

	# 4.1.3
	$lang['keywordRelevancy'] 		= "Pertinence des mots-clés";
	$lang['mediaLicenseEX'] 		= "Licence étendue";

	# 4.1.1
	$lang['editMediaDetails'] 		= "Editer le détail du média";
	$lang['searchResults'] 			= "Résultat de la recherche";
	$lang['width'] 					= "Largeur";
	$lang['height'] 				= "Hauteur";
	$lang['hd'] 					= "HD";
	$lang['mediaProfile'] 			= "Profil";
	$lang['photo'] 					= "Photo";
	$lang['video'] 					= "Vidéo";
	$lang['other'] 					= "Autre";
	$lang['thumbnail'] 				= "Vignette";
	$lang['videoSample'] 			= "Vidéo exemple";
	$lang['attachFile'] 			= "Attacher un fichier";
	$lang['fileAttached'] 			= "Fichier Attaché";
	$lang['attachMessage'] 			= "SVP ! Choisir un fichier à attacher à ce profil";
	$lang['browse'] 				= "Parcourir";
	$lang['uploadThumb'] 			= "Envoyer une vignette";
	$lang['uploadVideo'] 			= "Envoyer un aperçu vidéo";
	
	# 4.1
	$lang['importSuccessMes'] 		= "Importé avec succès!";
	$lang['forgotPassword'] 		= "Mot de passe oublié ?";
	$lang['passwordSent'] 			= "Email envoyé, SVP, vérifier votre boite email et votre dossier de SPAM .";
	$lang['passwordFailed'] 		= "Désolé mais ce membre n'existe pas, SVP, essayer une autre adresse email.";
	$lang['photoProfiles']			= "Profils des photos";
	$lang['videoProfiles']			= "Profils des vidéos";
	$lang['otherProfiles']			= "Autres Profils";
	$lang['saving']					= "Sauvegarde";
	$lang['myAccount']				= "Mon compte";
	$lang['myGalleries']			= "Mes galeries";
	$lang['noMediaAlbum']			= "Il n'y a pas de média à afficher.";
	$lang['editDetails']			= "Editer les détails";
	$lang['approvalStatus0']		= "Approbation en cours";
	$lang['approvalStatus1']		= "Approuvé";
	$lang['approvalStatus2']		= "Echec de l'approbation";
	$lang['noDetailsMes']			= "Aucun détail n'a été fourni!";	
	$lang['orphanedMedia']			= "Médias orphelins";
	$lang['lastBatch']				= "Dernier lot ajouté";
	$lang['deleteAlbumMes']			= "Supprimer cet album et tous les médias qu'il contient ?";
	$lang['mailInMedia']			= "Envoyé sur CD/DVD";
	$lang['deleteAlbum']			= "Supprimer l'Album";
	$lang['editAlbum']				= "Editer l'Album";
	$lang['albumName']				= "Nom de l'album";
	$lang['makePublic']				= "Rendre publique";
	$lang['deleteMedia']			= "Supprimer média";
	$lang['deleteMediaMes']			= "Supprimer les fichiers médias sélectionnés ?";
	$lang['selectAll']				= "Tout sélectionner";
	$lang['selectNone']				= "Tout désélectionner";
	$lang['noImportFilesMessage']	= "Il n'y a pas de fichiers à importer !";
	$lang['selectAlbumMes']			= "Sélectionner l'album dans lequel vous voulez ajouter le média.";
	$lang['selectGalleriesMes']		= "Sélectionner les galeries dans lesquelles vous voulez ajouter les médias.";
	$lang['chooseItemsMes']			= "Choisisser les articles à vendre dans la liste ci-dessous.";
	$lang['ablum']					= "Album";
	$lang['pricing']				= "Tarification";
	$lang['px']						= "px";	
	$lang['noSales']				= "Vous n'avez aucune vente.";
	$lang['itemUpper']				= "ARTICLE";
	$lang['commissionUpper']		= "COMMISSION";
	$lang['addUpper']				= "AJOUTER";
	$lang['cmDeleteVerify']			= "Etes vous certain de vouloir supprimer ce média?";
	$lang['waitingForImport']		= "Média en attente d'import";
	$lang['importSelectedUpper']	= "IMPORT SELECTIONNES";
	$lang['addMediaDetails']		= "Ajouter un détail de média";
	$lang['uploadUpper']			= "ENVOI";
	$lang['noBioMessage']			= "Il n'y a actuellement aucune biographie pour ce membre.";
	$lang['collections']			= "Collections";
	$lang['uploadMediaUpper']		= "ENVOYER UN NOUVEAU MEDIA";
	$lang['startUpper']				= "COMMENCER";

	# 4.0.9
	$lang['displayName']			= "Afficher le nom";
	$lang['newAlbum']				= "Nouvel Album";
	$lang['albums']					= "Albums";
	$lang['viewAllMedia']			= "VOIR TOUS LES MEDIAS";
	$lang['signUpNow']				= "Souscrire maintenant";

	# 4.0.8		
	$lang['exactMatch']				= "Correspondance exacte";
	$lang['mediaLabelMediaTypes']	= "Types de média";

	# 4.0.6
	$lang['orderNumUpper']			= "NUMERO DE COMMANDE";
	$lang['orderDateUpper']			= "DATE DE LA COMMANDE";
	$lang['paymentUpper']			= "PAIEMENT";	
	
	# 4.0.5
	$lang['dateRange']				= "Plage de date";
	$lang['resolution']				= "Résolution";
	$lang['continueShopUpper']		= "CONTINUER VOS ACHATS";
	$lang['votes']					= "votes";
	$lang['moreNews']				= "plus d'actualités";
	$lang['currentSearch']			= "Recherche actuelle";
	$lang['dates']					= "Dates";
	$lang['licenseType']			= "Type de licence";
	$lang['searchUpper']			= "RECHERCHE";
	$lang['from']					= "De";
	$lang['to']						= "Vers";
	$lang['lightboxUpper']			= "VISIONNEUSE";
	$lang['itemsUpper']				= "ARTICLES";
	$lang['createdUpper']			= "CREE";
	$lang['na']						= "N/A"; 
	$lang['galSortCDate']			= "Date de création";	
	
	# 4.0.4
	$lang['digitalDownloads']		= "Téléchargements Numériques";

	$lang['copyright']				= "Copyright &copy; ".date("Y");
	$lang['reserved']				= "Tous droits réservés.";
	
	$lang['days']					= "Jours";
	$lang['weeks']					= "Semaines";
	$lang['months']					= "Mois";
	$lang['years']					= "Années";
	$lang['weekly']					= "Hebdomadaire";
	$lang['monthly']				= "Mensuel";
	$lang['quarterly']				= "Trimestriel";
	$lang['semi-annually']			= "Semestriel";
	$lang['annually']				= "Annuel";
	$lang['guest']					= "Invité";
	$lang['login'] 					= "Connexion";	
	$lang['loginCaps'] 				= "CONNEXION";	
	$lang['loginMessage']			= "Veuillez mettre votre adresse email et votre mot de passe ci-dessous pour vous connecter.";
	$lang['loggedOutMessage']		= "Vous avez été déconnecté.";
	$lang['loginFailedMessage']		= "Echec de connexion. Votre email ou votre mot de passe sont incorrect.";
	$lang['accountActivated']		= "Votre compte a été activé.";
	$lang['activationEmail']		= "Un email de vérification vous a été envoyé. Utiliser le lien dans votre email pour activer votre compte.";
	$lang['loginAccountClosed']		= "Ce compte est inactif ou fermé.";
	$lang['loginPending']			= "Ce compte n'a pas été vérifié, vous devez l'activer avant de vous connecter.";
	$lang['yesLogin']				= "Oui, je souhaite me connecter.";
	$lang['noCreateAccount']		= "Non, je souhaite créer un compte.";
	$lang['haveAccountQuestion']	= "Avez-vous un compte chez nous ?";
	$lang['collections']			= "Collections Numériques";
	$lang['featuredCollections']	= "Collections numériques à l'honneur";
	$lang['similarMedia']			= "Médias similaires";
	$lang['paypal']					= "PayPal";
	$lang['checkMO']				= "Chèque / Mandat";
	$lang['paypalEmail']			= "Email PayPal";
	$lang['paid']					= "Payée";
	$lang['processing']				= "En cours";
	$lang['unpaid']					= "Impayée";
	$lang['refunded']				= "Remboursée";
	$lang['failed']					= "Echouée";
	$lang['cancelled']				= "Annulée";
	$lang['approved']				= "Approuvée";
	$lang['incomplete']				= "Incomplète";
	$lang['billLater']				= "Payer plus tard";
	$lang['expired']				= "Expirée";
	$lang['unlimited']				= "Illimité";
	$lang['quantityAvailable']		= "Quantité disponible";
	$lang['shipped']				= "Expédiée";
	$lang['notShipped']				= "Non expédiée";
	$lang['backordered']			= "en rupture de stock";
	$lang['taxIncMessage']			= "Taxes incluses";
	$lang['addTag']					= "Ajouter un Tag";
	$lang['memberTags']				= "Tags des membres";
	$lang['comments']				= "Commentaires";
	$lang['showMoreComments']		= "Afficher tous les commentaires";
	$lang['noComments']				= "Il n'y a pas de commentaires approuvés.";
	$lang['noTags']					= "Il n'y a pas de Tags approuvés.";
	$lang['addNewComment']			= "Ajoute un nouveau commentaire";
	$lang['commentPosted']			= "Votre commentaire a été posté.";
	$lang['commentPending']			= "Votre nouveau commentaire a été posté et est en attente de validation.";
	$lang['commentError']			= "Il y a eu une erreur et votre commentaire n'a pas été envoyé.";
	$lang['tagPosted']				= "Votre Tag a bien été posté.";
	$lang['tagPending']				= "Votre Tag a été envoyé et est en attente de validation.";
	$lang['tagError']				= "Il y a eu une erreur et votre Tag n'a pas été posté.";
	$lang['tagDuplicate']			= "Ce Tag existe déjà.";
	$lang['tagNotAccepted']			= "Votre Tag n'a pas été accepté par notre système.";
	$lang['preferredLang']			= "Langage préféré";
	$lang['dateTime']				= "Date/Heure";
	$lang['preferredCurrency']		= "Devise préférée";
	$lang['longDate']				= "Date longue";
	$lang['shortDate']				= "Date courte";
	$lang['numbDate']				= "Date numérique";
	$lang['daylightSavings']		= "Horaire d'été";
	$lang['dateFormat']				= "Format de date";
	$lang['timeZone']				= "Fuseau horaire";
	$lang['dateDisplay']			= "Affichage de la date";
	$lang['clockFormat']			= "Format d'horloge";
	$lang['numberDateSep']			= "Séparateur de date ";
	$lang['renew']					= "RENOUVELER";
	$lang['noOrders']				= "Il n'y a aucune commande pour ce compte.";
	$lang['noDownloads']			= "Il n'y a pas eu de téléchargement pour ce compte.";
	$lang['noFeatured'] 			= "Il n'y a pas d'articles à l'honneur pour cette zone.";
	$lang['promotions']				= "Promotions";
	$lang['noPromotions']			= "Il n'y a pas de coupons ou promotion en ce moment";	
	$lang['autoApply']				= "Appliqué automatiquement à la commande.";
	$lang['useCoupon']				= "Utiliser le code suivant dans votre commande et cliquer sur le bouton 'Appliquer' pour utiliser ce coupon/promotion.";
	$lang['couponApplied']			= "Un coupon ou une remise a été appliqué à votre panier.";
	$lang['couponFailed']			= "Ce coupon ou cette remise n'est pas valide.";	
	$lang['couponNeedsLogin']		= "Vous devez être connecté pour utiliser ce coupon ou cette remise.";
	$lang['couponMinumumWarn']		= "Votre sous-total ne remplit pas les exigences requises pour utiliser ce coupon ou cette remise.";
	$lang['couponAlreadyUsed']		= "Vous ne pouvez utiliser ce coupon et cette remise qu'une seule fois.";
	$lang['couponLoginWarn']		= "Vous devez être connecté pour utiliser ce coupon ou cette remise";
	$lang['checkout']				= "COMMANDER";
	$lang['continue']				= "CONTINUER";
	$lang['shipTo']					= "Expédié à ";
	$lang['billTo']					= "Facturé à ";
	//$lang['mailTo']					= "Paiement par courrier à ";
	$lang['galleries']				= "Galeries";
	$lang['chooseGallery']			= "Choisissez une galerie ci-dessous pour en afficher son contenu.";
	$lang['galleryLogin']			= "SVP ! Veuillez saisir un mot de passe pour afficher le contenu de cette galerie.";
	$lang['galleryWrongPass']		= "Le mot de passe pour cette galerie n'est pas correct.";
	$lang['newestMedia']			= "Nouveau média";
	$lang['popularMedia']			= "Média populaire";
	$lang['contributors']			= "Contribuer";
	$lang['contUploadNewMedia']		= "Envoyer un nouveau média";
	$lang['contViewSales']			= "Voir les ventes";
	$lang['contPortfolio']			= "Mon Portfolio";
	$lang['contGalleries']			= "Mes galeries";
	$lang['contMedia']				= "Mes Médias";
	$lang['aboutUs']				= "A propos";
	$lang['news']					= "Actualités";
	$lang['noNews']					= "Il n'y a pas d'articles d'actualités pour l'instant.";
	$lang['termsOfUse']				= "Conditions d'utilisation";
	$lang['privacyPolicy']			= "Politique de confidentialité";
	$lang['purchaseAgreement']		= "Accord de vente";	
	$lang['iAgree']					= "Je donne mon autorisation pour ";
	$lang['createAccount']			= "Créer un compte";
	$lang['createAccountMessage']	= "SVP ! Veuillez remplir le formulaire ci-dessous pour créér un compte.";
	$lang['contactUs']				= "Nous contacter";
	$lang['contactMessage']			= "Merci de nous avoir contacté. Nous allons vous répondre sous peu.";
	$lang['contactError']			= "Le formulaire de contact n'a pas été correctement renseigné. Votre message n'a pas été envoyé.";
	$lang['contactIntro']			= "SVP ! Utiliser le formulaire ci-dessous pour nous contacter. Nous vous répondrons aussi vite que possible.";
	$lang['contactEmailSubject']	= "Question depuis le formulaire de contact";
	$lang['contactFromName']		= "formulaire de contact";
	$lang['prints']					= "Tirage photos";
	$lang['featuredPrints']			= "Tirage à l'honneur";
	$lang['newBill']				= "Une nouvelle facture a été créée pour cet abonnement. Elle ne sera pas activée ou renouvelée tant que la facture ne sera pas payée. Tous les frais d'abonnement en attente seront annulés.";
	$lang['accountInfo']			= "Information de compte";
	$lang['accountUpdated']			= "Vos informations de compte ont été mis à jour";
	$lang['editAccountInfo']		= "Editer vos informations de compte";
	$lang['editAccountInfoMes']		= "Editer vos informations de compte ci-dessous et cliquer sur le bouton 'Appliquer' ou 'valider' pour sauvegarder vos modifications.";	
	$lang['accountInfoError1']		= "Le mot de passe et la vérification de mot de passe ne correspondent pas !";
	$lang['accountInfoError5']		= "Le mot de passe tapé est incorrect!";
	$lang['accountInfoError2']		= "Votre mot de passe doit contenir au moins 6 charactères!";
	$lang['accountInfoError12']		= "Cette adresse mail existe déjà!";
	$lang['accountInfoError13']		= "Votre adresse email n'a pas été acceptée!";
	$lang['accountInfoError3']		= "Taper les mots ci-dessous!";
	$lang['accountInfoError4']		= "Le code de vérification CAPTCHA est erroné!";
	$lang['readAgree']				= "J'ai lu les ";
	$lang['agreements']				= "Accords";
	$lang['poweredBy']				= "Généré par";
	$lang['captchaAudio']			= "Entrer les numéros que vous entendez";
	$lang['captchaIncorrect']		= "Incorrect ! Essayer à nouveau";
	$lang['captchaPlayAgain']		= "Jouer une nouvelle fois";
	$lang['captchaDownloadMP3']		= "Télécharger au format MP3";
	$lang['captcha']				= "Captcha";
	$lang['subHeaderID']			= "ID";
	$lang['subHeaderSubscript']		= "SOUSCRIPTION";
	$lang['subHeaderExpires']		= "EXPIRE";
	$lang['subHeaderDPD']			= "TELECHARGEMENTS PAR JOURS/RESTANTS";
	$lang['subHeaderStatus']		= "STATUT";
	$lang['downloads']				= "Téléchargements";	
	$lang['thankRequest']			= "Merci de votre demande. Nous vous contacterons sous peu.";
	$lang['pleaseContactForm']		= "SVP ! Veuillez nous contacter pour connaitre le prix de ce fichier en remplissant le formulaire ci-dessous.";
	$lang['downWithSubscription']	= "Télécharger en utilisant la souscription";
	$lang['noInstantDownload'] 		= "Ce fichier n'est pas disponible pour un téléchargement instantané. Il sera livré par l'administrateur du site par e-mail dès la demande ou l'achat.";


	// Tickets
	$lang['newTicketsMessage']		= "Nouveaux billets mis à jour";
	$lang['emptyTicket']			= "Ce billet est vide.";
	$lang['messageID']				= "ID Message";
	$lang['ticketNoReplies']		= "Ce billet est fermé et ne peut accepter de nouvelles réponses.";
	$lang['ticketUpdated']			= "Ce billet a été mis à jour!";
	$lang['ticketClosed']			= "Ce billet a été fermé !";
	$lang['closeTicket']			= "Billet fermé";
	$lang['newTicket']				= "Nouveau billet de support";
	$lang['newTicketButton']		= "NOUVEAU BILLET";
	$lang['ticketUnreadMes']		= "Le billet contient des nouveaux messages non lus";
	$lang['ticketUnderAccount']		= "Il n'y a pas de billets de support pour ce compte";	
	$lang['ticketSubmitted']		= "Votre billet de support a été envoyé, nous y répondrons sous peu.";
	$lang['mediaNotElidgiblePack']	= "Ce média n'est pas éligible pour étre attribué à tous les packages!";
	$lang['noBills']				= "Il n'y a pas de factures pour ce compte";
	$lang['lightbox']				= "Visionneuse";
	$lang['noLightboxes']			= "Vous n'avez pas de visionneuse.";
	$lang['lightboxDeleted']		= "La visionneuse a été supprimée.";
	$lang['lbDeleteVerify']			= "Etes-vous certain de vouloir supprimer cette visionneuse ?";
	$lang['newLightbox']			= "NOUVELLE VISIONNEUSE";
	$lang['lightboxCreated']		= "Votre nouvelle visionneuse a été créée.";
	$lang['addToLightbox']			= "Ajouter à la visionneuse";
	$lang['createNewLightbox']		= "Créer une nouvelle visionneuse";
	$lang['editLightbox']			= "Editer la visionneuse";
	$lang['addNotes']				= "Ajouter des notes";
	$lang['editLightboxItem']		= "Editer les articles de la visionneuse";
	$lang['removeFromLightbox']		= "RETIRER DE LA VISIONNEUSE";
	$lang['savedChangesMessage']	= "Vos changements ont été sauvegardé.";
	$lang['noSubs']					= "Il n'y a pas de souscription pour ce compte.";
	$lang['unpaidBills']			= "Factures impayées";
	$lang['notices']				= "Avis";
	$lang['subscription']			= "Souscription";
	$lang['assignToPackage']		= "Assigner à un package";
	$lang['startNewPackage']		= "Débuter un nouveau package";
	$lang['packagesInCart']			= "Packages dans votre panier";
	$lang['id']						= "ID";
	$lang['summary']				= "Résumé";
	$lang['status']					= "Statut";
	$lang['lastUpdated']			= "Dernière mise à jour";
	$lang['opened']					= "Ouvert";
	$lang['reply']					= "Repondre";
	$lang['required']				= "Requis";
	$lang['bills']					= "Factures";
	$lang['orders']					= "Commandes";
	$lang['downloadHistory']		= "Historique de téléchargements";
	$lang['supportTickets']			= "Billets du support";
	$lang['order']					= "Commande";
	$lang['packages']				= "Packages";
	$lang['featuredPackages']		= "Package à l'honneur";
	$lang['products']				= "Produits";
	$lang['featuredProducts']		= "Produits à l'honneur";
	$lang['subscriptions']			= "Souscriptions";
	$lang['featuredSubscriptions']	= "Souscriptions à l'honneur";
	$lang['yourCredits']			= "VOS<br />CREDITS";
	$lang['featuredCredits']		= "Packages de crédits à l'honneur";
	$lang['media'] 					= "MEDIA";
	$lang['mediaNav'] 				= "Media";
	$lang['featuredMedia']			= "Media à l'honneur";
	$lang['featuredItems'] 			= "En vedette";	
	$lang['showcasedContributors'] 	= "Participants affichés";
	$lang['galleryLogin']			= "Login de galerie";
	$lang['forum'] 					= "Forum";
	$lang['randomMedia'] 			= "Média aléatoire";
	$lang['language'] 				= "Langue";
	$lang['priceCreditSep']			= "ou";
	$lang['viewCollection']			= "VUE DE COLLECTION";
	$lang['loggedInAs']				= "Vous êtes connecté en tant que";
	$lang['editProfile']			= "EDITEZ LE COMPTE";
	$lang['noItemCartWarning']		= "Vous devez sélectionner une photo avant d'ajouter cet article à votre panier !";
	$lang['cartTotalListWarning']	= "Ces valeurs sont des estimations et peuvent varier légèrement en raison des fluctuations monétaires.";
	$lang['applyCouponCode']		= "Appliquer le code du coupon";
	$lang['billMeLater']			= "Me facturer plus tard";
	$lang['billMeLaterDescription']	= "Nous vous facturerons mensuellement pour vos achats.";
	$lang['shippingOptions']		= "Options d'expéditions";
	$lang['paymentOptions']			= "Options de paiements";
	$lang['chooseCountryFirst']		= "Choisissez votre pays en premier";
	$lang['paymentCancelled']		= "Votre paiement a été annulé. Vous pouvez ré-essayer une commande."; 
	$lang['paymentDeclined']		= "Votre paiement a été rejeté. Vous pouvez ré-essayer une commande."; 
	$lang['generalInfo']			= "Information personnelle";
	$lang['membership']				= "Abonnement";
	$lang['preferences']			= "Préférences";
	$lang['address']				= "Adresse";
	$lang['actions']				= "Actions";
	$lang['changePass']				= "Changer le mot de passe";
	$lang['changeAvatar']			= "Changer votre Avatar";
	$lang['bio']					= "Bio";
	$lang['contributorSettings']	= "Paramètres de contribution";
	$lang['edit']					= "EDITER";
	$lang['leftToFill']				= "Eléments à remplir";
	$lang['commissionMethod']		= "Méthode de paiement des commissions"; 
	$lang['commission']				= "Commission";
	$lang['signupDate']				= "Membre depuis";
	$lang['lastLogin']				= "Dernière connexion";
	$lang['clientName']				= "Nom du client";
	$lang['eventCode']				= "Code de l'Evènement";
	$lang['eventDate']				= "Date de l'Evènement";
	$lang['eventLocation']			= "Lieu de l'Evènement";
	$lang['viewPackOptions']		= "Afficher les contenus & options des packages";
	$lang['viewOptions']			= "Afficher les options";
	$lang['remove']					= "ENLEVER";
	$lang['productShots']			= "Vues";
	$lang['currentPass']			= "Mot de passe actuel";
	$lang['newPass']				= "Nouveau mot de passe";
	$lang['vNewPass']				= "Verifier le nouveau mot de passe";
	$lang['verifyPass']				= "Verifier le mot de passe";
	$lang['firstName']				= "Prénom";
	$lang['lastName']				= "Nom de famille";
	$lang['location']				= "Lieu";
	$lang['memberSince']			= "Membre depuis";
	$lang['portfolio']				= "PORTFOLIO";
	$lang['profile']				= "Profil";
	$lang['address']				= "Adresse";
	$lang['city']					= "Ville";
	$lang['state']					= "Etats/Province";
	$lang['zip']					= "ZIP/Code postal";
	$lang['country']				= "Pays";
	$lang['save'] 					= "SAUVER";
	$lang['add'] 					= "AJOUTER";
	$lang['companyName']			= "Nom de l'entreprise";
	$lang['accountStatus']			= "Statut du compte";
	$lang['website']				= "Site internet";
	$lang['phone']					= "Téléphone";
	$lang['email'] 					= "Email";
	$lang['name'] 					= "Nom";
	$lang['submit'] 				= "ENVOYER";	
	$lang['message'] 				= "Message";	
	$lang['question']				= "Question";
	$lang['password'] 				= "Mot de passe";	
	$lang['members'] 				= "Membres";
	$lang['visits'] 				= "Visites";	
	$lang['logout'] 				= "Deconnexion";
	$lang['lightboxes']				= "Visionneuse";
	$lang['cart']					= "Votre panier";
	$lang['cartItemAdded']			= "Un article a été ajouté à votre panier.";
	$lang['includesTax']			= "TAXE/TVA comprises";
	$lang['addToCart']				= "AJOUTER AU PANIER";
	$lang['items']					= "Articles";
	$lang['item']					= "Article";
	$lang['qty']					= "Qté";
	$lang['price']					= "Prix";
	$lang['more']					= "Plus";
	$lang['moreInfo']				= "Plus d'info";
	$lang['back']					= "Retour";
	$lang['none']					= "Aucun";
	$lang['details']				= "Détails";
	$lang['options']				= "Options";
	$lang['page']					= "Page";
	$lang['ratingSubmitted']		= "Evaluation envoyée";
	$lang['noMedia']				= "Cette galerie est vide";
	$lang['itemsTotal']				= "Total des articles";
	$lang['of']						= "de";
	$lang['view']					= "AFFICHER";
	$lang['apply']					= "APPLIQUER";
	$lang['currency']				= "Changer la devise";
	$lang['active']					= "Actif";
	$lang['pending']				= "En attente";
	$lang['freeTrial']				= "Essai gratuit";
	$lang['setupFee']				= "Frais de mise en service";
	$lang['free']					= "Gratuit";
	$lang['open']					= "Ouvert";
	$lang['close']					= "Fermer";
	$lang['closed']					= "Fermé";
	$lang['by']						= "de";
	$lang['download']				= "Télécharger";
	$lang['downloadUpper']			= "TELECHARGER";
	$lang['KB']						= "Ko";
	$lang['MB']						= "Mo";
	$lang['files']					= "Fichiers";
	$lang['unknown']				= "Inconnu";
	$lang['freeDownload']			= "Téléchargement gratuit";
	$lang['prevDown']				= "Téléchargement précédent";
	$lang['pay']					= "PAYER";
	$lang['purchaseCredits']		= "ACHETER DES CREDITS";
	$lang['purchaseSub']			= "ACHETER UNE SOUSCRIPTION";
	$lang['hour'] 					= "Heure";
	$lang['slash'] 					= "Barre oblique";
	$lang['period'] 				= "Point";
	$lang['dash'] 					= "Tiret";
	$lang['gmt'] 					= "GMT";
	$lang['avatar'] 				= "Avatar";
	$lang['delete'] 				= "SUPPRIMER";
	$lang['uploadAvatar']			= "Envoyer un Avatar";
	$lang['welcome']				= "Bienvenue";
	$lang['expires']				= "Expire";
	$lang['msExpired']				= "L'abonnement suivant est expiré";
	$lang['newSales']				= "Nouvelles ventes depuis votre dernière connexion";
	$lang['never']					= "Jamais";
	$lang['yes']					= "OUI";
	$lang['no']						= "NON";
	$lang['create'] 				= "CREER";
	$lang['cancel'] 				= "ANNULER";
	$lang['notes'] 					= "Notifications";
	$lang['update'] 				= "METTRE A JOUR";
	$lang['each'] 					= "chaque";
	$lang['enterKeywords']			= "Ajouter des mots-clés";
	$lang['send']					= "ENVOYER";
	$lang['emailTo']				= "Email à ";
	$lang['yourName']				= "Votre nom";
	$lang['yourEmail']				= "Votre Email";
	$lang['emailToFriend']			= "Lien d'adresse email pour ce média";
	$lang['emailToFriendSent']		= "Un email a été envoyé ! Vous pouvez en envoyer un nouveau ou fermer cette fenêtre.";
	$lang['linkSentBy']				= "Un lien vers une photo ou une vidéo vient de vous être envoyé.";
	$lang['newPackageMessage']		= "Débuter un nouveau package ci-dessous ou choisissez un package similaire existant dans votre panier";
	$lang['warning']				= "Attention !";
	$lang['estimated']				= "Estimé";
	$lang['doneUpper']				= "FAIT";
	$lang['returnToSiteUpper']		= "RETOUR AU SITE";
	$lang['chooseCountryFirst']		= "Choisissez un pays en premier";
	$lang['advancedSearch']			= "Recherche avancée";
	$lang['eventSearch']			= "Recherche d'Evènements";	
	$lang['AND']					= "ET";
	$lang['OR']						= "OU";
	$lang['NOT']					= "SANS";
	$lang['noAccess']				= "Vous n'avez pas accès à cette zone.";
	$lang['siteStats']				= "Stats du site";
	$lang['membersOnline']			= "Membres en ligne";
	$lang['minutesAgo']				= "minutes avant";
	$lang['seconds']				= "Secondes";
	$lang['megabytesAbv']			= "Mo";
	$lang['downWithSub']			= "Télécharger en utilisant la souscription";
	$lang['downloadsRemaining']		= "Téléchargements restants Aujourd'hui";
	$lang['requestDownloadSuccess']	= "Nous avons reçu votre demande pour ce fichier, nous vous contacterons sous peu.";
	$lang['mediaLicenseNFS']		= "Pas à vendre";
	$lang['mediaLicenseRF']			= "Royalty offert";
	$lang['mediaLicenseRM']			= "Droits gérés";
	$lang['mediaLicenseFR']			= "Gratuit";
	$lang['mediaLicenseCU']			= "Nous contacter";
	$lang['original']				= "Original";
	$lang['prevDownloaded']			= "Vous avez déjà téléchargé ce fichier, Vous ne serez pas facturé pour le télécharger à nouveau.";
	$lang['instantDownload']		= "Téléchargement instantanné";
	$lang['requestDownload']		= "DEMANDE DE TELECHARGEMENT";
	$lang['request']				= "DEMANDE";
	$lang['enterEmail']				= "Mettre votre adresse email";
	$lang['license']				= "Licence";
	$lang['inches']					= "Pouces";
	$lang['centimeters']			= "Centimètres";
	$lang['dpi']					= "dpi";
	$lang['noSimilarMediaMessage']	= "Aucun médias similaires trouvés.";
	$lang['backUpper']				= "RETOUR";
	$lang['nextUpper']				= "SUIVANT";
	$lang['prevUpper']				= "PRECEDENT";
	$lang['curGalleryOnly']			= "Galerie courante uniquement";
	$lang['sortBy']					= "Classé par";
	$lang['galSortColor']			= "Couleur";
	$lang['galSortDate']			= "Date d'ajout";
	$lang['galSortID']				= "ID";
	$lang['galSortTitle']			= "Titre";
	$lang['galSortFilename']		= "Nom de fichier";
	$lang['galSortFilesize']		= "Taille de fichier";
	$lang['galSortSortNum']			= "Numéro";
	$lang['galSortBatchID']			= "ID du lot";
	$lang['galSortFeatured']		= "En vedette";
	$lang['galSortWidth']			= "Largeur";
	$lang['galSortHeight']			= "Hauteur";
	$lang['galSortViews']			= "Vues";
	$lang['galSortAsc']				= "Croissant";
	$lang['galSortDesc']			= "Décroissant";
	$lang['mediaIncludedInColl']	= "Ce média est inclus dans la collection suivante.";
	$lang['billHeaderInvoice']		= "FACTURE";
	$lang['billHeaderDate']			= "DATE DE FACTURATION";
	$lang['billHeaderDueDate']		= "DATE LIMITE";
	$lang['billHeaderTotal']		= "TOTAL";
	$lang['billHeaderStatus']		= "STATUT";
	
	
	// Cart
	$lang['creditsWarning']			= "Vous n'avez pas assez de crédits pour commander, SVP ! Connectez-vous et ajoutez des crédits à votre panier .";
	$lang['subtotalWarning']		= "Le montant minimum de commande est de :";
	$lang['pleaseWait']				= "SVP! Patientez le temps que nous procédons à votre requête !";
	$lang['orderMailinThanks']		= "Merci ! Nous avons reçu votre commande ! Envoyez nous votre chèque ou votre mandat du montant qui suit à l'adressse listée. A la réception de votre mandat ou chèque nous procèderons à votre commande.";
	$lang['billMailinThanks']		= "Envoyez nous votre chèque ou votre mandat du montant qui suit à l'adressse listée. Une fois le paiement réceptionné, nous marquerons votre commande comme 'Payée'.";
	$lang['mailinRef']				= "SVP! Indiquer le numéro qui suit avec votre paiement";
	$lang['subtotal']				= "Sous-total";
	$lang['shipping']				= "Expédition";
	$lang['discounts']				= "Remises/Coupons";
	$lang['total']					= "Total";
	$lang['creditsSubtotal']		= "Crédits Sous-total";
	$lang['creditsDiscounts']		= "Crédit de Remise";
	$lang['credits']				= "Crédits";
	$lang['reviewOrder']			= "Vérifiez votre commande";
	$lang['payment']				= "Paiement";
	$lang['cartNoItems']			= "Vous n'avez pas d'articles dans votre panier!";
	$lang['enterShipInfo']			= "SVP ! Veuillez mettre vos informations de facturation et d'expédition ci-dessous";
	$lang['sameAsShipping']			= "Identique à l'adresse d'expédition";
	$lang['differentAddress']		= "Donner une adresse différente";
	$lang['noShipMethod']			= "Aucun moyen d'expédition n'existe pour ces articles.";	
	$lang['choosePaymentMethod']	= "SVP ! Choisisser un moyen de paiement";	
	$lang['discountCode']			= "CODE DE REMISE";
	$lang['use']					= "Utiliser";
	$lang['continueNoAccount']		= "ou continuer sans compte ci-dessous";
		
		
	// Order
	$lang['yourOrder']				= "Votre commande";
	$lang['viewInvoice']			= "AFFICHER LA FACTURE";
	$lang['totalsShown']			= "Totaux indiqués dans";
	$lang['downloadExpired']		= "Téléchargements expirés";
	$lang['downloadExpiredMes']		= "Ce lien de téléchargement a expiré. Veuillez nous contacter pour le ré-activer.";
	$lang['downloadMax']			= "Téléchargements maximum autorisés dépassés";
	$lang['downloadMaxMes']			= "Vous avez atteint le nombre maximum de téléchargements autorisés pour cet article. Veuillez nous contacter pour le ré-activer.";
	$lang['downloadNotApproved']	= "Commande non approuvée";
	$lang['downloadNotApprovedMes']	= "Vous ne pouvez télécharger cet article tant que votre commande n'a pas été approuvée. Ré-essayez plus tard.";	
	$lang['downloadNotAvail']		= "Téléchargements immédiats non disponible";
	$lang['downloadNotAvailMes']	= "Ce fichier n'est pas disponible pour un téléchargement immédiat. Veuillez utiliser le bouton ci-dessous pour nous envoyer une demande, un lien vous sera envoyé par email.";
	$lang['orderNumber']			= "Numéro de commande";
	$lang['orderPlaced']			= "Commande placée"; 
	$lang['orderStatus']			= "Statut de la commande";
	$lang['paymentStatus']			= "Statut du paiement";
	
	
	// IPTC
	$lang['iptc']					= "IPTC";
	$lang['iptc_title']				= "Titre";
	$lang['iptc_description']		= "Description";
	$lang['iptc_instructions']		= "Instructions";
	$lang['iptc_date_created']		= "Date de création";
	$lang['iptc_author']			= "Auteur";
	$lang['iptc_city']				= "Ville";
	$lang['iptc_state']				= "Etat";
	$lang['iptc_country']			= "Pays";
	$lang['iptc_job_identifier']	= "Instructions";
	$lang['iptc_headline']			= "Identificateur de tâches";
	$lang['iptc_provider']			= "Fournisseur";
	$lang['iptc_source']			= "Source";
	$lang['iptc_description_writer']= "Description de l'auteur";
	$lang['iptc_urgency']			= "Urgence";
	$lang['iptc_copyright_notice']	= "Note de copyright";
	
	
	// EXIF
	$lang['exif']							= "EXIF";
	$lang['exif_FileName']					= "Nom du fichier";
	$lang['exif_FileDateTime']				= "Date/Heure du fichier";
	$lang['exif_FileSize']					= "Taille du fichier";
	$lang['exif_FileType']					= "Type de fichier";
	$lang['exif_MimeType']					= "Type MIME";
	$lang['exif_SectionsFound']				= "Sections trouvées";
	$lang['exif_ImageDescription']			= "Description de l'image";
	$lang['exif_Make']						= "Appareil";
	$lang['exif_Model']						= "Modèle";
	$lang['exif_Orientation']				= "Orientation";
	$lang['exif_XResolution']				= "XResolution";
	$lang['exif_YResolution']				= "YResolution";
	$lang['exif_ResolutionUnit']			= "Resolution unité";
	$lang['exif_Software']					= "Logiciel";
	$lang['exif_DateTime']					= "Date/Heure";
	$lang['exif_YCbCrPositioning']			= "YCbCr Positioning";
	$lang['exif_Exif_IFD_Pointer']			= "Exif IFD Pointer";
	$lang['exif_GPS_IFD_Pointer']			= "GPS IFD Pointer";
	$lang['exif_ExposureTime']				= "Temps d'exposition";
	$lang['exif_FNumber']					= "Nombre f";
	$lang['exif_ExposureProgram']			= "Programme d'exposition";
	$lang['exif_ISOSpeedRatings']			= "ISO";
	$lang['exif_ExifVersion']				= "Version EXIF";
	$lang['exif_DateTimeOriginal']			= "Date/Heure original";
	$lang['exif_DateTimeDigitized']			= "Date/Heure digitale";
	$lang['exif_ComponentsConfiguration']	= "Configuration des composants";
	$lang['exif_ShutterSpeedValue']			= "Vitesse";
	$lang['exif_ApertureValue']				= "Valeur d'ouverture";
	$lang['exif_MeteringMode']				= "Mode mètre";
	$lang['exif_Flash']						= "Flash";
	$lang['exif_FocalLength']				= "Longueur focale";
	$lang['exif_FlashPixVersion']			= "Flash Pix Version";
	$lang['exif_ColorSpace']				= "Espace colorimétrique";
	$lang['exif_ExifImageWidth']			= "Exif Largeur d'image";
	$lang['exif_ExifImageLength']			= "Exif Longueur d'image";
	$lang['exif_SensingMethod']				= "Méthode de détection";
	$lang['exif_ExposureMode']				= "Mode d'exposition";
	$lang['exif_WhiteBalance']				= "Balance des blancs";
	$lang['exif_SceneCaptureType']			= "Type de capture de scène";
	$lang['exif_Sharpness']					= "Netteté";
	$lang['exif_GPSLatitudeRef']			= "GPS Latitude Ref";
	$lang['exif_GPSLatitude_0']				= "GPSLatitude 0";
	$lang['exif_GPSLatitude_1']				= "GPSLatitude 1";
	$lang['exif_GPSLatitude_2']				= "GPSLatitude 2";
	$lang['exif_GPSLongitudeRef']			= "GPS Longitude Ref";
	$lang['exif_GPSLongitude_0']			= "GPS Longitude 0";
	$lang['exif_GPSLongitude_1']			= "GPS Longitude 1";
	$lang['exif_GPSLongitude_2']			= "GPS Longitude 2";
	$lang['exif_GPSTimeStamp_0']			= "GPS Timestamp 0";
	$lang['exif_GPSTimeStamp_1']			= "GPS Timestamp 1";
	$lang['exif_GPSTimeStamp_2']			= "GPS Timestamp 2";
	$lang['exif_GPSImgDirectionRef']		= "GPS Image Direction Reference";
	$lang['exif_GPSImgDirection']			= "GPS Image Direction";


	// Media Labels	
	$lang['mediaLabelTitle']		= "Titre";
	$lang['mediaLabelDesc']			= "Description";
	$lang['mediaLabelCopyright']	= "Copyright";
	$lang['mediaLabelRestrictions']	= "Restrictions d'usage";
	$lang['mediaLabelRelease']		= "Autorisation du modèle";
	$lang['mediaLabelKeys']			= "Mots-clés";
	$lang['mediaLabelFilename']		= "Nom du fichier";
	$lang['mediaLabelID']			= "ID";
	$lang['mediaLabelDate']			= "Ajouté";
	$lang['mediaLabelDateC']		= "Créé";
	$lang['mediaLabelDownloads']	= "Téléchargement";
	$lang['mediaLabelViews']		= "Vues";
	$lang['mediaLabelPurchases']	= "Achetés";
	$lang['mediaLabelColors']		= "Couleurs";
	$lang['mediaLabelPrice']		= "Prix";
	$lang['mediaLabelCredits']		= "Crédits";
	$lang['mediaLabelOwner']		= "Propriétaire";
	$lang['mediaLabelUnknown']		= "Unconnu";
	$lang['mediaLabelResolution']	= "Pixels";
	$lang['mediaLabelFilesize']		= "Taille du fichier";
	$lang['mediaLabelRunningTime']	= "Durée";
	$lang['mediaLabelFPS']			= "FPS";
	$lang['mediaLabelFormat']		= "Format";

	
	// Search
	$lang['search']					= "Recherche";
	$lang['searchEnterKeyMessage']	= "SVP ! Veuillez soumettre vos mots-clés pour la recherce. SVP !";
	$lang['searchNoResults']		= "Votre recherche n'a retourné aucun résultats";
	$lang['searchDateRange']		= "Recherche dans une plage de date";
	$lang['searchNarrow']			= "Affiner la recherche";
	$lang['searchStart']			= "Commencez votre recherche ci-dessous!";
	$lang['searchAll']				= "Tous";
	$lang['searchClear']			= "Effacer la recherche";
	$lang['searchKeywords']			= "Mots-clés";
	$lang['searchTitle']			= "Titre";
	$lang['searchDescription']		= "Description";
	$lang['searchFilename']			= "Nom du fichier";
	$lang['searchPortrait']			= "Portrait";
	$lang['searchLandscape']		= "Paysage";
	$lang['searchSquare']			= "Carré";
	$lang['searchRoyaltyFree']		= "Royalty offert";
	$lang['searchRightsManaged']	= "Droits gérés";
	$lang['searchFree']				= "Gratuit";
	$lang['searchContactUs']		= "Nous-contacter";
	$lang['searchHeaderKeywords']	= "Mots-clés";
	$lang['searchHeaderFields']		= "Champs";
	$lang['searchHeaderType']		= "Type";
	$lang['searchHeaderOrientation']= "Orientation";
	$lang['searchHeaderColor']		= "Couleur";
	$lang['searchHeaderGalleries']	= "Galeries";
	
?>
