<?php
	# GATEWAYS LANG
	# 4.6.3
	# STRIPE
	$lang['stripe_displayName']			= "Stripe";
	$lang['stripe_publicDescription']	= "Carte de crédits ou carte de débit";
	$lang['stripe_pkey']				= "Clé de partage";
	$lang['stripe_pkey_d']				= "Votre clé de partage pour votre compte Stripe.";
	$lang['stripe_skey']				= "Clé secrète";
	$lang['stripe_skey_d']				= "Votre clé secrète de votre compte Stripe.";
	
	# 4.1.4
	# MOLLIE IDEAL
	$lang['mollieideal_displayName']		= "iDeal (Mollie)";
	$lang['mollieideal_f_partnerid']		= "ID Partenaire";
	$lang['mollieideal_f_partnerid_d']		= "Votre ID partenaire de iDeal.";
	$lang['mollieideal_f_profilekey']		= "La clé du profile";
	$lang['mollieideal_f_profilekey_d']		= "Votre clé de profile Mollie iDeal.";
	$lang['mollieideal_f_testmode']			= "Mode de test";
	$lang['mollieideal_f_testmode_d']		= "Mettre votre Mollie iDeal en mode de test.";
	$lang['mollieideal_instructions']		= "Soyez certain que le mode de test corresponds aux paramètres de votre compte Mollie.nl.";
	$lang['mollieideal_publicDescription']	= "Paiement sécurisé et facile avec iDeal (Uniquement dans les Pays-Bas).";
	
	# 4.1
	# ONEBIP 
	$lang['onebip_displayName']			= "OneBip"; 
	$lang['onebip_merchantid']			= "Email";	
	$lang['onebip_merchantid_d']		= "Mettre l'email de votre compte onebip."; 
	$lang['onebip_merchantkey']			= "Clé API"; 
	$lang['onebip_merchantkey_d']		= "Mettre la clé API de votre compte onebip.";
	$lang['onebip_testmode']			= "Mode de test";
	$lang['onebip_testmode_d']			= "Mettre votre onebip en mode test.";
	$lang['onebip_publicDescription']	= "Payer par mobile";
	
	# PAYFAST 
	$lang['payfast_displayName']		= "PayFast"; 
	$lang['payfast_merchantid']			= "ID Marchand";	
	$lang['payfast_merchantid_d']		= "Mettre l'ID marchand pour votre compte PayFast."; 
	$lang['payfast_merchantkey']		= "Clé marchand"; 
	$lang['payfast_merchantkey_d']		= "Mettre votre clé marchand pour votre compte PayFast.";
	$lang['payfast_testmode']			= "Mode de test"; 
	$lang['payfast_testmode_d']			= "Mettre votre PayFast en mode test.";
	$lang['payfast_publicDescription']	= "Carte de crédit ou compte bancaire"; 
	
	# NOCHEX
	$lang['nochex_displayName']			= "Nochex";
	$lang['nochex_f_accountid']			= "ID de compte";
	$lang['nochex_f_accountid_d']		= "Votre ID de compte Nochex.";
	$lang['nochex_f_testmode']			= "Mode de test";
	$lang['nochex_f_testmode_d']		= "Mettre votre compte Nochex en mode de test.";
	$lang['nochex_publicDescription']	= "Carte de crédit ou de débit ";
	
	# WORLDPAY
	$lang['worldpay_displayName']			= "WorldPay";
	$lang['worldpay_f_installid']			= "ID d'installation";
	$lang['worldpay_f_installid_d']			= "ID d'installation pour votre compte WorldPay.";
	$lang['worldpay_f_testmode']			= "Mode de test";
	$lang['worldpay_f_testmode_d']			= "Placer votre système de paiement WorldPay en mode de test.";
	$lang['worldpay_publicDescription']		= "Carte de crédit ou de débit";
	
	# ROBOKASSA
	$lang['robokassa_displayName']			= "RoboKassa";
	$lang['robokassa_f_merchantid']			= "ID Marchand";
	$lang['robokassa_f_merchantid_d']		= "ID marchand pour votre compte RoboKassa.";
	$lang['robokassa_f_merchantpass']		= "Passe marchand";
	$lang['robokassa_f_merchantpass_d']		= "Votre passe ID marchand RoboKass.";
	$lang['robokassa_publicDescription'] 	= "Carte de crédit ou de débit";
	
	# PAYGATE
	$lang['paygate_displayName']			= "PayGate";
	$lang['paygate_f_accountid']			= "PayGate ID";
	$lang['paygate_f_accountid_d']			= "ID pour votre compte PayGate.";
	$lang['paygate_f_accountkey']			= "Clé PayGate";
	$lang['paygate_f_accountkey_d']			= "Votre clé ID pour PayGate.";
	$lang['paygate_f_testmode']				= "Mode de test";
	$lang['paygate_f_testmode_d']			= "Placer votre système de paiement PayGate en mode de test.";	
	$lang['paygate_f_testid']				= "ID de test PayGate";
	$lang['paygate_f_testid_d']				= "ID de test PayGate";
	$lang['paygate_f_testkey']				= "Clé de test PayGate";
	$lang['paygate_f_testkey_d']			= "Votre clé de test PayGate";
	$lang['paygate_publicDescription']		= "Carte de crédit ou de débit";
	
	# PAYSTATION
	$lang['paystation_displayName']			= "PayStation";
	$lang['paystation_f_accountid']			= "PayStation ID";
	$lang['paystation_f_accountid_d']		= "ID de votre compte PayStation.";
	$lang['paystation_f_testmode']			= "Mode de test";
	$lang['paystation_f_testmode_d']		= "Placer votre système de paiement PayStation en mode de test.";	
	$lang['paystation_f_gatewayid']			= "ID de la passerelle";
	$lang['paystation_f_gatewayid_d']		= "Votre ID de passerelle PayStation.";
	$lang['paystation_publicDescription']	= "Carte de crédit ou de débit";
	
	# SKRILL
	$lang['skrill_displayName']				= "Skrill (moneybookers)";
	$lang['skrill_f_email']					= "Adresse email";
	$lang['skrill_f_email_d']				= "Adresse email de votre compte Skrill.";
	$lang['skrill_f_testmode']				= "Mode de test";
	$lang['skrill_f_testmode_d']			= "Placez votre système de paiement Skrill en mode de test.";	
	$lang['skrill_f_testemail']				= "Email de test";
	$lang['skrill_f_testemail_d']			= "Votre adresse email de test Skrill.";
	$lang['skrill_publicDescription']		= "Carte de crédit ou de débit";
	$lang['skrill_f_completeOrder'] 		= "Cliquer pour terminer votre commande!";
	
	# CHRONOPAY
	$lang['chronopay_displayName']			= "ChronoPay";
	$lang['chronopay_f_clientid']			= "Client ID";
	$lang['chronopay_f_clientid_d']			= "Votre ID client de ChronoPay.";
	$lang['chronopay_f_siteid']				= "ID du site";
	$lang['chronopay_f_siteid_d']			= "Votre ID de site de ChronoPay.";
	$lang['chronopay_f_productid']			= "ID Produit.";
	$lang['chronopay_f_productid_d']		= "Votre ID de produit de ChronoPay.";
	$lang['chronopay_publicDescription']	= "Carte de crédit ou de débit";
	
	# IDEAL
	$lang['ideal_displayName']				= "iDeal";
	$lang['ideal_f_accountid']				= "ID de Compte";
	$lang['ideal_f_accountid_d']			= "Votre ID de compte iDeal.";
	$lang['ideal_f_transkey']				= "Clé secrète";
	$lang['ideal_f_transkey_d']				= "Votre clé secrète iDeal.";
	$lang['ideal_f_testmode']				= "Mode de test";
	$lang['ideal_f_testmode_d']				= "Placer votre système de paiement iDeal en mode de test.";
	$lang['ideal_publicDescription']		= "Compte bancaire";
	
	# PAYPAL
	$lang['paypal_displayName']				= "PayPal";
	$lang['paypal_f_email']					= "Adresse email";
	$lang['paypal_f_email_d']				= "Adresse email de votre compte PAYPAL.";
	$lang['paypal_f_testmode']				= "Mode de test";
	$lang['paypal_f_testmode_d']			= "Placez votre système de paiement Paypal en mode de test.";	
	$lang['paypal_f_testemail']				= "Email de test";
	$lang['paypal_f_testemail_d']			= "Votre adresse email du SANDBOX de PayPal.";
	$lang['paypal_publicDescription']		= "PayPal, Carte de crédit ou Compte bancaire";
	
	# 2CHECKOUT
	$lang['2checkout_displayName']			= "2Checkout.com";
	$lang['2checkout_f_accountid']			= "ID compte";
	$lang['2checkout_f_accountid_d']		= "Votre ID de compte 2Checkout.";
	$lang['2checkout_f_testmode']			= "Mode de test";
	$lang['2checkout_f_testmode_d']			= "Placez votre système de paiement 2Checkout en mode de test.";
	$lang['2checkout_instructions']			= "Dans 2Checkout.com placer le retour direct sur OUI et placer l'URL d'approbation sur http://www.VOTRE NOM DE DOMAINE.com/gateways/2checkout/ipn.php";
	$lang['2checkout_publicDescription']	= "PayPal, Carte de crédit ou Compte bancaire";

	
	# Plug n' Pay
	$lang['plugnpay_displayName']			= "Plug n' Pay";
	$lang['plugnpay_f_accountid']			= "ID de compte";
	$lang['plugnpay_f_accountid_d']			= "Votre ID de compte Plug n' Pay.";
	$lang['plugnpay_publicDescription']		= "";
	
	# AUTHORIZE.NET
	$lang['authorize_displayName']			= "Authorize.net";
	$lang['authorize_f_apiid']				= "API Login ID";
	$lang['authorize_f_apiid_d']			= "Votre ID de connexion à l'API Authorize.net";
	$lang['authorize_f_transkey']			= "Clé de transaction";
	$lang['authorize_f_transkey_d']			= "Votre clé de transaction Authorize.net.";
	$lang['authorize_f_testmode']			= "Mode de test";
	$lang['authorize_f_testmode_d']			= "Placer votre système de paiement Authorize.net en mode de test.";
	$lang['authorize_publicDescription']	= "";
	$lang['authorize_f_completeOrder'] 		= "Cliquer ici pour terminer votre commande!";
	
	# MYGATE.CO.ZA
	$lang['mygate_displayName']				= "MyGate.co.za";
	$lang['mygate_f_merchantid']			= "ID marchand";
	$lang['mygate_f_merchantid_d']			= "Votre ID marchand MyGate.co.za";
	$lang['mygate_f_appid']					= "Application ID";
	$lang['mygate_f_appid_d']				= "Votre application ID MyGate.co.za.";
	$lang['mygate_publicDescription']		= "";
	$lang['mygate_f_testmode']				= "Mode de test";
	$lang['mygate_f_testmode_d']			= "Placer vote système de paiement MyGate en mode de test.";	
	
	# MAIL IN PAYMENT
	$lang['mailin_displayName']				= "Paiement par virement bancaire ou chèque";
	$lang['mailin_f_instructions']			= "Instruction pour le paiement par virement bancaire ou chèque";
	$lang['mailin_f_instructions_d']		= "Instructions pour envoyer un virement bancaire ou un chèque.";
	$lang['mailin_publicDescription']		= "Envoyer votre paiement à l'adresse qui figure sur la page suivante.";
?>