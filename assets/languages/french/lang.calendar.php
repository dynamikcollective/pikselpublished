<?php
	# FRANCAIS rev 4.7.0
	$calendar = array();
	$calendar['long_month']		= array(1 => "Janvier","Février","Mars","Avril","Mai","Juin","Juillet","Août","Septembre","Octobre","Novembre","Décembre");
	$calendar['short_month']	= array(1 => "Jan","Fev","Mar","Avr","Mai","Juin","Juil","Aout","Sept","Oct","Nov","Dec");
	$calendar['full_days']		= array(1 => "Dimanche","Lundi","Mardi","Mercredi","Jeudi","Vendredi","Samedi");
	$calendar['short_days']		= array(1 => "Dim","Lun","Mar","Mer","Jeu","Ven","Sam");
?>