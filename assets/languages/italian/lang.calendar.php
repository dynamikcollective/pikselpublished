<?php
	# ITALIAN
	$calendar = array();
	$calendar['long_month']		= array(1 => "Gennaio","Febbraio","Marzo","Aprile","Maggio","Giugno","Luglio","Agosto","Settembre","Ottobre","Novembre","Dicembre");
	$calendar['short_month']	= array(1 => "Gen","Feb","Mar","Apr","Mag","Giu","Lug","Ago","Set","Ott","Nov","Dic");
	$calendar['full_days']		= array(1 => "Domenica","Lunedi'","Martedi'","Mercoledi'","Giovedi'","Venerdi'","Sabato");
	$calendar['short_days']		= array(1 => "Dom","Lun","Mar","Mer","Gio","Ven","Sab");
?>