<?php
	# GATEWAYS LANG
	# 4.6.3
	# STRIPE
	$lang['stripe_displayName']			= "Stripe";
	$lang['stripe_publicDescription']	= "Carta di Credito o Carta di Debito";
	$lang['stripe_pkey']				= "Chiave pubblica";
	$lang['stripe_pkey_d']				= "La chiave pubblica per tuo account.";
	$lang['stripe_skey']				= "Chiave Segreta";
	$lang['stripe_skey_d']				= "Chiave segreta per il Tuo account.";

	# 4.1.4
	# MOLLIE IDEAL
	$lang['mollieideal_displayName']		= "iDeal (Mollie)";
	$lang['mollieideal_f_partnerid']		= "Partner ID";
	$lang['mollieideal_f_partnerid_d']		= "Il tuo ID Partner di iDeal.";
	$lang['mollieideal_f_profilekey']		= "Chiave Profilo";
	$lang['mollieideal_f_profilekey_d']		= "Il tuo Profilo key Mollie iDeal.";
	$lang['mollieideal_f_testmode']			= "Modo prova";
	$lang['mollieideal_f_testmode_d']		= "Regola il tuo Mollie iDeal in modo prova.";
	$lang['mollieideal_instructions']		= "Assicurati che il modo prova corrisponda ai parametri del tuo conto Mollie.nl.";
	$lang['mollieideal_publicDescription']	= "Pagamenti facili e sicuri con iDeal (Disponibile soltanto in Olanda).";

	# 4.1
	# ONEBIP 
	$lang['onebip_displayName']			= "OneBip"; 
	$lang['onebip_merchantid']			= "Email";	
	$lang['onebip_merchantid_d']		= "Inserisci l'email del tuo conto onebip."; 
	$lang['onebip_merchantkey']			= "API Key"; 
	$lang['onebip_merchantkey_d']		= "Inserisci l'API key del tuo conto onebip.";
	$lang['onebip_testmode']			= "Modo prova";
	$lang['onebip_testmode_d']			= "Regola il tuo pagamento onebip in modo prova.";
	$lang['onebip_publicDescription']	= "Paga via Mobile";

	# PAYFAST 
	$lang['payfast_displayName']		= "PayFast"; 
	$lang['payfast_merchantid']			= "Merchant ID";	
	$lang['payfast_merchantid_d']		= "Inserisci il Merchant ID del tuo conto PayFast."; 
	$lang['payfast_merchantkey']		= "Merchant Key"; 
	$lang['payfast_merchantkey_d']		= "Inserisci la Merchant Key del tuo conto PayFast.";
	$lang['payfast_testmode']			= "Modo prova"; 
	$lang['payfast_testmode_d']			= "Regola il tuo PayFast in modo prova.";
	$lang['payfast_publicDescription']	= "Carta di Credito o Conto Bancario"; 

	# NOCHEX
	$lang['nochex_displayName']			= "Nochex";
	$lang['nochex_f_accountid']			= "Account ID";
	$lang['nochex_f_accountid_d']		= "Il tuo Account ID Nochex.";
	$lang['nochex_f_testmode']			= "Modo prova";
	$lang['nochex_f_testmode_d']		= "Regola i tuoi pagamenti Nochex in modo prova.";
	$lang['nochex_publicDescription']	= "Carta di Credito o di Debito";

	# WORLDPAY
	$lang['worldpay_displayName']		= "WorldPay";
	$lang['worldpay_f_installid']		= "Installation ID";
	$lang['worldpay_f_installid_d']		= "Installation ID del tuo conto WorldPay.";
	$lang['worldpay_f_testmode']		= "Modo prova";
	$lang['worldpay_f_testmode_d']		= "Regola i tuoi pagamenti WorldPay in modo prova.";
	$lang['worldpay_publicDescription']= "Carta di Credito o di Debito";

	# ROBOKASSA
	$lang['robokassa_displayName']		= "RoboKassa";
	$lang['robokassa_f_merchantid']		= "Merchant ID";
	$lang['robokassa_f_merchantid_d']	= "Merchant ID del tuo conto RoboKassa.";
	$lang['robokassa_f_merchantpass']	= "Merchant Pass";
	$lang['robokassa_f_merchantpass_d']	= "Il tuo Merchant Pass ID RoboKassa.";
	$lang['robokassa_publicDescription']= "Carta di Credito o di Debito";

	# PAYGATE
	$lang['paygate_displayName']		= "PayGate";
	$lang['paygate_f_accountid']		= "PayGate ID";
	$lang['paygate_f_accountid_d']		= "ID del tuo conto PayGate.";
	$lang['paygate_f_accountkey']		= "PayGate Key";
	$lang['paygate_f_accountkey_d']		= "Key ID del tuo PayGate.";
	$lang['paygate_f_testmode']			= "Modo prova";
	$lang['paygate_f_testmode_d']		= "Regola i tuoi pagamenti PayGate in modo prova.";	
	$lang['paygate_f_testid']			= "PayGate ID prova";
	$lang['paygate_f_testid_d']			= "Il tuo ID prova PayGate";
	$lang['paygate_f_testkey']			= "PayGate Key prova";
	$lang['paygate_f_testkey_d']		= "La tua PayGate Key prova";
	$lang['paygate_publicDescription']	= "Carta di Credito o di Debito";

	# PAYSTATION
	$lang['paystation_displayName']		= "PayStation";
	$lang['paystation_f_accountid']		= "PayStation ID";
	$lang['paystation_f_accountid_d']	= "ID del tuo conto PayStation.";
	$lang['paystation_f_testmode']		= "Modo prova";
	$lang['paystation_f_testmode_d']	= "Regola i tuoi pagamenti PayStation in modo prova.";	
	$lang['paystation_f_gatewayid']		= "Gateway ID";
	$lang['paystation_f_gatewayid_d']	= "Il tuo Gateway ID PayStation.";
	$lang['paystation_publicDescription']= "Carta di Credito o di Debito";

	# SKRILL
	$lang['skrill_displayName']			= "Skrill (moneybookers)";
	$lang['skrill_f_email']				= "Indirizzo email";
	$lang['skrill_f_email_d']			= "Indirizzo email del tuo conto Skrill.";
	$lang['skrill_f_testmode']			= "Modo prova";
	$lang['skrill_f_testmode_d']		= "Regola i tuoi pagamenti Skrill in modo prova.";	
	$lang['skrill_f_testemail']			= "Prova email";
	$lang['skrill_f_testemail_d']		= "Il tuo indirizzo email di prova Skrill.";
	$lang['skrill_publicDescription']	= "Carta di Credito o di Debito";
	$lang['skrill_f_completeOrder']		= "Clicca qui per completare il tuo ordine!";

	# CHRONOPAY
	$lang['chronopay_displayName']		= "ChronoPay";
	$lang['chronopay_f_clientid']		= "Client ID";
	$lang['chronopay_f_clientid_d']		= "Il tuo Client ID ChronoPay.";
	$lang['chronopay_f_siteid']			= "ID del sito";
	$lang['chronopay_f_siteid_d']		= "Il tuo ID ChronoPay del sito.";
	$lang['chronopay_f_productid']		= "ID prodotto.";
	$lang['chronopay_f_productid_d']	= "Il tuo ID Cronopay del prodotto.";
	$lang['chronopay_publicDescription']= "Carta di Credito o di Debito";

	# IDEAL
	$lang['ideal_displayName']			= "iDeal (ING)";
	$lang['ideal_f_accountid']			= "Account ID";
	$lang['ideal_f_accountid_d']		= "Il tuo Account ID iDeal.";
	$lang['ideal_f_transkey']			= "Chiave Segreta";
	$lang['ideal_f_transkey_d']			= "La tua Chiave Segreta iDeal.";
	$lang['ideal_f_testmode']			= "Modo prova";
	$lang['ideal_f_testmode_d']			= "Regola i tuoi pagamenti iDeal in modo prova.";
	$lang['ideal_publicDescription']	= "Conto bancario";

	# PAYPAL
	$lang['paypal_displayName']			= "PayPal";
	$lang['paypal_f_email']				= "Indirizzo email";
	$lang['paypal_f_email_d']			= "Indirizzo email del tuo conto Paypal.";
	$lang['paypal_f_testmode']			= "Modo prova";
	$lang['paypal_f_testmode_d']		= "Regola i tuoi pagamenti PayPal in modo prova.";	
	$lang['paypal_f_testemail']			= "Email di prova";
	$lang['paypal_f_testemail_d']		= "Il tuo indirizzo email di prova Paypal Sandbox.";
	$lang['paypal_publicDescription']	= "PayPal, Carta di Credito o Conto bancario";

	# 2CHECKOUT
	$lang['2checkout_displayName']		= "2Checkout.com";
	$lang['2checkout_f_accountid']		= "Account ID";
	$lang['2checkout_f_accountid_d']	= "Il tuo Account ID 2Checkout.";
	$lang['2checkout_f_testmode']		= "Modo prova";
	$lang['2checkout_f_testmode_d']		= "Regola i tuoi pagamenti 2Checkout in modo prova.";
	$lang['2checkout_instructions']		= "In 2Checkout.com set direct return to 'Header Redirect (Your URL)' and set approved URL to http://www.YOUR_DOMAIN_NAME.com/assets/gateways/2checkout/ipn.php";
	$lang['2checkout_publicDescription']= "Carta di Credito o Conto Bancario USA";

	
	# Plug n' Pay
	$lang['plugnpay_displayName']		= "Plug n' Pay";
	$lang['plugnpay_f_accountid']		= "Account ID";
	$lang['plugnpay_f_accountid_d']		= "Il tuo Account ID Plug n' Pay .";
	$lang['plugnpay_publicDescription']	= "";

	# AUTHORIZE.NET
	$lang['authorize_displayName']		= "Authorize.net";
	$lang['authorize_f_apiid']			= "API Login ID";
	$lang['authorize_f_apiid_d']		= "Il tuo Login ID all'API Authorize.net.";
	$lang['authorize_f_transkey']		= "Chiave Operativa";
	$lang['authorize_f_transkey_d']		= "La tu Chiave Operativa Authorize.net.";
	$lang['authorize_f_testmode']		= "Modo prova";
	$lang['authorize_f_testmode_d']		= "Regola i tuoi pagamenti Authorize.net in modo prova.";
	$lang['authorize_publicDescription']= "";
	$lang['authorize_f_completeOrder'] = "Clicca qui per completare il tuo ordine!";

	# MYGATE.CO.ZA
	$lang['mygate_displayName']			= "MyGate.co.za";
	$lang['mygate_f_merchantid']		= "Merchant ID";
	$lang['mygate_f_merchantid_d']		= "Il tuo Merchant IDyGate.co.za.";
	$lang['mygate_f_appid']				= "Application ID";
	$lang['mygate_f_appid_d']			= "Il tuo Application ID MyGate.co.za.";
	$lang['mygate_publicDescription']	= "";
	$lang['mygate_f_testmode']			= "Modo prova";
	$lang['mygate_f_testmode_d']		= "Regola i tuoi pagamenti MyGate in modo prova.";	

	# MAIL IN PAYMENT
	$lang['mailin_displayName']			= "Pagamento via email";
	$lang['mailin_f_instructions']		= "Istruzioni per il pagamento via email";
	$lang['mailin_f_instructions_d']	= "Istruzioni per i clienti per l'invio di richieste di pagamento come assegni o bonifici.";
	$lang['mailin_publicDescription']	= "Invia il tuo pagamento all'indirizzo fornito nella pagine seguente.";
?>