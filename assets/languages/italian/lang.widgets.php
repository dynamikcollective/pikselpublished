<?php
	# 4.4.6
	$wplang['stats_visitors']		= "Visitatori del sito";
	
	# 4.1.3
	$wplang['qstats_pending_media']	= "Media di un collaboratore in attesa";
	
	# 4.0.7	
	$wplang['sitehealth_exif']	= "Supporto PHP EXIF (exif_read_data)";
	
	# GENERAL WIDGET LANGUAGE
	$wplang['widget_close']		= "Chiuso";
	$wplang['widget_save']		= "Salva";
	$wplang['load_failed'] 		= "Errore pannello di caricamento!";
	$wplang['widget_title']		= "Titolo";
	$wplang['widget_note']		= "Nota";
	$wplang['widget_for']		= "Per";
	
	# NOTES WIDGET
	$wplang['notes_title']		= "Note";
	$wplang['notes_newnote']	= "Nuova nota";
	$wplang['notes_postedby']	= "Pubblicato da";
	$wplang['notes_lastupdate']	= "Ultimo aggiornamento";
	$wplang['notes_nonotes']	= "Non ci sono note da mostrare";
	
	# EXTRAS WIDGET
	$wplang['extras_title']		= "PhotoStore Extra";
	
	# KTOOLS ACCOUNT WIDGET
	$wplang['kaccount_title']	= "Account Ktools";
	$wplang['kaccount_support']	= "Supporto/aggiornamento giorni rimanenti";
	$wplang['kaccount_messages']= "Messaggi non letti";
	$wplang['kaccount_affil']	= "Vendite in abbonamento dall'ultimo accesso";
	
	# KNEWS WIDGET
	$wplang['knews_title']		= "Notizie Ktools.net";
	
	# QUICK STATS WIDGET - NEW AND PENDING
	$wplang['qstats_title']		= "Nuovi e in attesa";
	$wplang['qstats_logmem']	= "Utenti dall'ultimo accesso";
	$wplang['qstats_penmem']	= "Utenti in attesa";
	$wplang['qstats_logorders']	= "Ordini dall'ultimo accesso";
	$wplang['qstats_penorders']	= "Ordini in attesa";
	$wplang['qstats_logcomm']	= "Commenti dall'ultimo accesso";
	$wplang['qstats_pencomm']	= "Commenti in attesa";
	$wplang['qstats_logtags']	= "Tag dall'ultimo accesso";
	$wplang['qstats_pentags']	= "Tag in attesa";
	$wplang['qstats_lograte']	= "Valutazioni dall'ultimo accesso";
	$wplang['qstats_penrate']	= "Valutazioni in attesa";
	$wplang['qstats_penbios']	= "Note biografiche utenti in attesa";
	$wplang['qstats_penavatars']= "Avatar in attesa";
	$wplang['qstats_pensupport']= "Ticket di supporto in attesa";
	
	# STATS WIDGET
	$wplang['stats_title']		= "Statistiche";
	$wplang['stats_op1']		= "Vendite";
	$wplang['stats_op2']		= "Utenti";
	$wplang['stats_op_7days']	= "Ultimi 7 giorni";
	$wplang['stats_op_6mon']	= "Ultimi 6 mesi";
	$wplang['stats_op_5year']	= "Ultimi 5 anni";
	$wplang['stats_tdsales']	= "Vendite oggi";
	$wplang['stats_tdorders']	= "Ordini oggi";
	$wplang['stats_sales']		= "Vendite";
	$wplang['stats_orders']		= "Ordini";	
	$wplang['stats_atsales']	= "Vendite complessive dall'inizio";
	$wplang['stats_atorders']	= "Ordini complessivi dall'inizio";
	$wplang['stats_tdmems']		= "Nuovi utenti oggi";
	$wplang['stats_mems']		= "Nuovi utenti";
	$wplang['stats_tamems']		= "Totale utenti attivi";
	$wplang['stats_timems']		= "Totale utenti inattivi";
	
	
	# SITE HEALTH WIDGET
	$wplang['sitehealth_title']	= "Stato di manutenzione del server";
	$wplang['sitehealth_ok']	= "OK";
	$wplang['sitehealth_low']	= "BASSO";
	$wplang['sitehealth_high']	= "ALTO";
	$wplang['sitehealth_failed']= "FALLITO";
	$wplang['sitehealth_unava'] = "Non disponibile";
	$wplang['sitehealth_off']	= "OFF";
	$wplang['sitehealth_on']	= "ON";
	$wplang['sitehealth_inst']	= "Installato";
	$wplang['sitehealth_none']	= "Nessuno";
	$wplang['sitehealth_exists']= "Esistente";
	$wplang['sitehealth_write']	= "Scrivibile";
	$wplang['sitehealth_nonwri']= "Non scrivibile";
	$wplang['sitehealth_php']	= "Versione PHP";
	$wplang['sitehealth_gd']	= "Libreria GD";
	$wplang['sitehealth_mem']	= "Limite memoria PHP";
	$wplang['sitehealth_exe']	= "PHP max_execution_time";
	$wplang['sitehealth_time']	= "PHP max_input_time";
	$wplang['sitehealth_file']	= "PHP upload_max_filesize";
	$wplang['sitehealth_post']	= "PHP post_max_size";
	$wplang['sitehealth_safe']	= "PHP safe_mode";
	$wplang['sitehealth_dbv']	= "Verifica versione prodotto/database";
	$wplang['sitehealth_mysqlv']= "Versione MySQL";
	$wplang['sitehealth_load']	= "Caricamento server";
	$wplang['sitehealth_upti']	= "Autonomia server";
	
	# UPDATE CHECK WIDGET
	$wplang['updater_title']	= "Verifica aggiornamenti";
	$wplang['updater_newest']	= "La versione installata e' la piu' recente";
	$wplang['updater_update']	= "Aggiornamento disponibile";
	$wplang['updater_newestis']	= "La versione piu' recente e'"; 
	$wplang['updater_getnew']	= "Per avere la versione piu' recente accedi al tuo <a href='http://www.ktools.net/members/' target='_blank'>Ktools.net account</a>";
	$wplang['updater_yourv']	= "Stai utilizzando la versione";
	
	# BLANK WIDGET
	$wplang['blank_title']		= "Pannello vuoto";
	
	# CALENDAR WIDGET
	$wplang['calendar_title']	= "Calendario";
	
	# TIPS CHECK WIDGET
	//$wplang['tips_title']		= "Tips";
	//$wplang['tips_next']		= "Next Tip";
	//$welcometip[] 				= "Did you know you can use <strong>Ctrl+Shift+S</strong> to open and close the shortcuts menu?"; 
	//$welcometip[] 				= "You can drag these welcome panels to rearrange them the way you would like. Just click on the title and drag. Then drop the panel when it is in the order that you would like."; 
	//$welcometip[] 				= "For quick access to your galleries and many other areas click on the tab with the arrow in the upper left. This will open the shortcuts menu. Click the tab again to close it."; 
	
?>