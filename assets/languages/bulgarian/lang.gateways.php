<?php
	# GATEWAYS LANG
	# 4.6.3
	# STRIPE
	$lang['stripe_displayName']			= "Stripe";
	$lang['stripe_publicDescription']	= "Credit Card or Debit Card";
	$lang['stripe_pkey']				= "Publishable Key";
	$lang['stripe_pkey_d']				= "Your publishable key for your stripe account.";
	$lang['stripe_skey']				= "Secret Key";
	$lang['stripe_skey_d']				= "Your secret key for your stripe account.";
	
	# 4.1.4
	# MOLLIE IDEAL
	$lang['mollieideal_displayName']		= "iDeal (Mollie)";
	$lang['mollieideal_f_partnerid']		= "ID на партньора";
	$lang['mollieideal_f_partnerid_d']		= "Вашият iDeal партньорски ID.";
	$lang['mollieideal_f_profilekey']		= "Ключ на профила";
	$lang['mollieideal_f_profilekey_d']		= "Вашият Mollie iDeal ключ на профила.";
	$lang['mollieideal_f_testmode']			= "Тестови режим";
	$lang['mollieideal_f_testmode_d']		= "Превключете Вашия Mollie iDeal в тестови режим.";
	$lang['mollieideal_instructions']		= "Уверете се, че тестовият режим съответства на параметрите на Вашата Mollie.nl сметка.";
	$lang['mollieideal_publicDescription']	= "Безопасно и бързо плащане с помощта на iDeal (Достъпно само в Холандия).";
	
	# 4.1
	# ONEBIP 
	$lang['onebip_displayName']			= "OneBip"; 
	$lang['onebip_merchantid']			= "Електронна поща";	
	$lang['onebip_merchantid_d']		= "Въведете електронна поща, за Вашата onebip сметка."; 
	$lang['onebip_merchantkey']			= "API ключ"; 
	$lang['onebip_merchantkey_d']		= "Въведете API ключ за Вашата onebip сметка.";
	$lang['onebip_testmode']			= "Тестови режим";
	$lang['onebip_testmode_d']			= "Превключете Вашата onebip сметка в тестови режим.";
	$lang['onebip_publicDescription']	= "Мобилно плащане";
	
	# PAYFAST 
	$lang['payfast_displayName']		= "PayFast"; 
	$lang['payfast_merchantid']			= "ID на продавача";	
	$lang['payfast_merchantid_d']		= "Въведете ID на продавача за Вашата PayFast сметка."; 
	$lang['payfast_merchantkey']		= "Ключ на продавача"; 
	$lang['payfast_merchantkey_d']		= "Въведете ключ на продавача за Вашата PayFast сметка.";
	$lang['payfast_testmode']			= "Тестови режим"; 
	$lang['payfast_testmode_d']			= "Превключете Вашата PayFast сметка в тестови режим.";
	$lang['payfast_publicDescription']	= "Кредитна карта или банкова сметка"; 
	
	# NOCHEX
	$lang['nochex_displayName']			= "Nochex";
	$lang['nochex_f_accountid']			= "Номер на сметката";
	$lang['nochex_f_accountid_d']		= "Номер на Вашата Nochex сметка.";
	$lang['nochex_f_testmode']			= "Тестови режим";
	$lang['nochex_f_testmode_d']		= "Превключете Вашата Nochex сметка в тестови режим.";
	$lang['nochex_publicDescription']	= "Кредитна или дебитна карта";
	
	# WORLDPAY
	$lang['worldpay_displayName']		= "WorldPay";
	$lang['worldpay_f_installid']		= "Получаване на ID";
	$lang['worldpay_f_installid_d']		= "Получаване на ID за Вашата WorldPay сметка.";
	$lang['worldpay_f_testmode']		= "Тестови режим";
	$lang['worldpay_f_testmode_d']		= "Превключете Вашата WorldPay сметка в тестови режим.";
	$lang['worldpay_publicDescription']= "Кредитна или дебитна карта";
	
	# ROBOKASSA
	$lang['robokassa_displayName']		= "RoboKassa";
	$lang['robokassa_f_merchantid']		= "ID на продавача";
	$lang['robokassa_f_merchantid_d']	= "ID на продавача за Вашата RoboKassa сметка.";
	$lang['robokassa_f_merchantpass']	= "Вашата парола";
	$lang['robokassa_f_merchantpass_d']	= "ID парола на продавача в RoboKassa.";
	$lang['robokassa_publicDescription']= "Кредитна или дебитна карта";
	
	# PAYGATE
	$lang['paygate_displayName']		= "PayGate";
	$lang['paygate_f_accountid']		= "PayGate ID";
	$lang['paygate_f_accountid_d']		= "ID за Вашата PayGate сметка.";
	$lang['paygate_f_accountkey']		= "PayGate ключ";
	$lang['paygate_f_accountkey_d']		= "Вашият PayGate ID ключ.";
	$lang['paygate_f_testmode']			= "Тестови режим";
	$lang['paygate_f_testmode_d']		= "Превключете вашата PayGate сметка в тестови режим.";	
	$lang['paygate_f_testid']			= "Тестване на PayGate ID";
	$lang['paygate_f_testid_d']			= "Вашият PayGate тестови id";
	$lang['paygate_f_testkey']			= "Тест на PayGate ключ";
	$lang['paygate_f_testkey_d']		= "Вашият PayGate тестови ключ";
	$lang['paygate_publicDescription']	= "Кредитна или дебитна карта";
	
	# PAYSTATION
	$lang['paystation_displayName']		= "PayStation";
	$lang['paystation_f_accountid']		= "PayStation ID";
	$lang['paystation_f_accountid_d']	= "ID за Вашата PayStation сметка.";
	$lang['paystation_f_testmode']		= "Тестови режим";
	$lang['paystation_f_testmode_d']	= "Превключете Вашата PayStation сметка в тестови режим.";	
	$lang['paystation_f_gatewayid']		= "Gateway ID";
	$lang['paystation_f_gatewayid_d']	= "Вашият PayStation gateway ID.";
	$lang['paystation_publicDescription']= "Кредитна или дебитна карта";
	
	# SKRILL
	$lang['skrill_displayName']			= "Skrill (moneybookers)";
	$lang['skrill_f_email']				= "Адрес на електронна поща";
	$lang['skrill_f_email_d']			= "Адрес на електронната поща за Вашата сметка в Skrill.";
	$lang['skrill_f_testmode']			= "Тестови режим";
	$lang['skrill_f_testmode_d']		= "Превключете Вашата Skrill сметка в тестови режим.";	
	$lang['skrill_f_testemail']			= "Проверка на електронната поща";
	$lang['skrill_f_testemail_d']		= "Тестване на адреса на електронната поща в Skrill.";
	$lang['skrill_publicDescription']	= "Кредитна или дебитна карта";
	$lang['skrill_f_completeOrder']		= "Натиснете тук, за да приключите поръчката!";
	
	# CHRONOPAY
	$lang['chronopay_displayName']		= "ChronoPay";
	$lang['chronopay_f_clientid']		= "Номер на клиента";
	$lang['chronopay_f_clientid_d']		= "Вашият клиентски номер в ChronoPay.";
	$lang['chronopay_f_siteid']			= "Номер на сайт";
	$lang['chronopay_f_siteid_d']		= "Вашият номер на сайт в ChronoPay.";
	$lang['chronopay_f_productid']		= "Продукт номер.";
	$lang['chronopay_f_productid_d']	= "Вашият номер на продукт в ChronoPay.";
	$lang['chronopay_publicDescription']= "Кредитна или дебитна карта";
	
	# IDEAL
	$lang['ideal_displayName']			= "iDeal (ING)";
	$lang['ideal_f_accountid']			= "Номер на сметката";
	$lang['ideal_f_accountid_d']		= "Вашият номер на сметка в iDeal.";
	$lang['ideal_f_transkey']			= "Секретен ключ";
	$lang['ideal_f_transkey_d']			= "Вашият секретен ключ в iDeal.";
	$lang['ideal_f_testmode']			= "Тестови режим";
	$lang['ideal_f_testmode_d']			= "Превключете Вашата iDeal сметка в тестови режим.";
	$lang['ideal_publicDescription']	= "Банкова сметка";
	
	# PAYPAL
	$lang['paypal_displayName']			= "PayPal";
	$lang['paypal_f_email']				= "Адрес на електронна поща";
	$lang['paypal_f_email_d']			= "Адрес на електронната поща за Вашата сметка в PayPal.";
	$lang['paypal_f_testmode']			= "Тестови режим";
	$lang['paypal_f_testmode_d']		= "Превключете Вашата PayPal сметка в тестови режим.";	
	$lang['paypal_f_testemail']			= "Тест на електронна поща";
	$lang['paypal_f_testemail_d']		= "Проверка на Вашата електронна поща в PayPal.";
	$lang['paypal_publicDescription']	= "PayPal, кредитна карта или банкова сметка";
	
	# 2CHECKOUT
	$lang['2checkout_displayName']		= "2Checkout.com";
	$lang['2checkout_f_accountid']		= "Номер на сметка";
	$lang['2checkout_f_accountid_d']	= "Номер на Вашата 2Checkout сметка.";
	$lang['2checkout_f_testmode']		= "Тестови режим";
	$lang['2checkout_f_testmode_d']		= "Превключете Вашата 2Checkout сметка в тестови режим.";
	$lang['2checkout_instructions']		= "In 2Checkout.com set direct return to 'Header Redirect (Your URL)' and set approved URL to http://www.YOUR_DOMAIN_NAME.com/assets/gateways/2checkout/ipn.php";
	$lang['2checkout_publicDescription']= "Кредитна карта или сметка в банка в САЩ";

	
	# Plug n' Pay
	$lang['plugnpay_displayName']		= "Plug n' Pay";
	$lang['plugnpay_f_accountid']		= "Номер на сметка";
	$lang['plugnpay_f_accountid_d']		= "Номер на Вашата Plug n' Pay сметка.";
	$lang['plugnpay_publicDescription']	= "";
	
	# AUTHORIZE.NET
	$lang['authorize_displayName']		= "Authorize.net";
	$lang['authorize_f_apiid']			= "API логин номер";
	$lang['authorize_f_apiid_d']		= "Вашият API логин номер в Authorize.net.";
	$lang['authorize_f_transkey']		= "Ключ за трансакция";
	$lang['authorize_f_transkey_d']		= "Вашият ключ за трансакция в Authorize.net.";
	$lang['authorize_f_testmode']		= "Тестови режим";
	$lang['authorize_f_testmode_d']		= "Превключете Вашата Authorize.net сметка в тестови режим.";
	$lang['authorize_publicDescription']= "";
	$lang['authorize_f_completeOrder'] = "Натиснете тук, за да приключите поръчката!";
	
	# MYGATE.CO.ZA
	$lang['mygate_displayName']			= "MyGate.co.za";
	$lang['mygate_f_merchantid']		= "Номер на продавача";
	$lang['mygate_f_merchantid_d']		= "Вашият номер на продавача в MyGate.co.za.";
	$lang['mygate_f_appid']				= "Номер на приложение";
	$lang['mygate_f_appid_d']			= "Вашият номер на приложение в MyGate.co.za.";
	$lang['mygate_publicDescription']	= "";
	$lang['mygate_f_testmode']			= "Тестови режим";
	$lang['mygate_f_testmode_d']		= "Превключете Вашата Mygate сметка в тестови режим.";	
	
	# MAIL IN PAYMENT
	$lang['mailin_displayName']			= "Плащане по електронна поща";
	$lang['mailin_f_instructions']		= "Инструкции за плащане по електронна поща";
	$lang['mailin_f_instructions_d']	= "Инструкции за клиенти, които изпращат плащания от типа на чекове или платежни нареждания по електронната поща.";
	$lang['mailin_publicDescription']	= "Изпратете Вашето плащане на адреса, показан на следващата страница.";
?>