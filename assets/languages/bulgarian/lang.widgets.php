<?php
	# 4.4.6
	$wplang['stats_visitors']		= "Approximate Site Visits";
	
	# 4.1.3
	$wplang['qstats_pending_media']	= "Чакащи авторски материали";
	
	# 4.0.7	
	$wplang['sitehealth_exif']	= "PHP EXIF поддръжка (exif_read_data)";
	
	# GENERAL WIDGET LANGUAGE
	$wplang['widget_close']		= "Затвори";
	$wplang['widget_save']		= "Запиши";
	$wplang['load_failed'] 		= "зареждането на панела не беше успешно!";
	$wplang['widget_title']		= "заглавие";
	$wplang['widget_note']		= "Бележка";
	$wplang['widget_for']		= "За";
	
	# NOTES WIDGET
	$wplang['notes_title']		= "Бележки";
	$wplang['notes_newnote']	= "Нова бележка";
	$wplang['notes_postedby']	= "Публикувано от";
	$wplang['notes_lastupdate']	= "Последно обновяване";
	$wplang['notes_nonotes']	= "Няма бележки, които да бъдат показани";
	
	# EXTRAS WIDGET
	$wplang['extras_title']		= "Екстри от PhotoStore";
	
	# KTOOLS ACCOUNT WIDGET
	$wplang['kaccount_title']	= "Екстри в Ktools";
	$wplang['kaccount_support']	= "Оставащи дни за поддръжка/обновяване";
	$wplang['kaccount_messages']= "Непрочетени съобщения";
	$wplang['kaccount_affil']	= "Партньорски продажби от последното влизане";
	
	# KNEWS WIDGET
	$wplang['knews_title']		= "Новини от Ktools.net";
	
	# QUICK STATS WIDGET - NEW AND PENDING
	$wplang['qstats_title']		= "Нови и чакащи";
	$wplang['qstats_logmem']	= "Участници след последното влизане";
	$wplang['qstats_penmem']	= "Чакащи потребители";
	$wplang['qstats_logorders']	= "Поръчки след последното влизане";
	$wplang['qstats_penorders']	= "Чакащи поръчки";
	$wplang['qstats_logcomm']	= "Коментари след последното влизане";
	$wplang['qstats_pencomm']	= "чакащи коментари";
	$wplang['qstats_logtags']	= "Тагове след последното влизане";
	$wplang['qstats_pentags']	= "Чакащи тагове";
	$wplang['qstats_lograte']	= "Рейтинги след последното влизане";
	$wplang['qstats_penrate']	= "Чакащи рейтинги";
	$wplang['qstats_penbios']	= "Чакащи потребителски данни";
	$wplang['qstats_penavatars']= "Чакащи аватари";
	$wplang['qstats_pensupport']= "Чакащи билети за поддръжка";
	
	# STATS WIDGET
	$wplang['stats_title']		= "Статистики";
	$wplang['stats_op1']		= "Продажби";
	$wplang['stats_op2']		= "Потребители";
	$wplang['stats_op_7days']	= "За последните 7 дена";
	$wplang['stats_op_6mon']	= "За последните 6 месеца";
	$wplang['stats_op_5year']	= "За последните 5 години";
	$wplang['stats_tdsales']	= "Продажби от последния ден";
	$wplang['stats_tdorders']	= "Поръчки от последния ден";
	$wplang['stats_sales']		= "Продажби";
	$wplang['stats_orders']		= "Поръчки";	
	$wplang['stats_atsales']	= "Продажби за цялото време";
	$wplang['stats_atorders']	= "Поръчки за цялото време";
	$wplang['stats_tdmems']		= "Нови потребители за днес";
	$wplang['stats_mems']		= "Нови потребители";
	$wplang['stats_tamems']		= "Общ брой активни потребители";
	$wplang['stats_timems']		= "Общ брой неактивни потребители";
	
	
	# SITE HEALTH WIDGET
	$wplang['sitehealth_title']	= "Състояние на сайта и сървъра";
	$wplang['sitehealth_ok']	= "ДОБРО";
	$wplang['sitehealth_low']	= "НИСКО";
	$wplang['sitehealth_high']	= "ВИСОКО";
	$wplang['sitehealth_failed']= "НЕУСПЕШНО";
	$wplang['sitehealth_unava'] = "Недостъпно";
	$wplang['sitehealth_off']	= "ИЗКЛ";
	$wplang['sitehealth_on']	= "ВКЛ";
	$wplang['sitehealth_inst']	= "Инсталиран";
	$wplang['sitehealth_none']	= "Няма";
	$wplang['sitehealth_exists']= "Съществува";
	$wplang['sitehealth_write']	= "Подлижи на запис";
	$wplang['sitehealth_nonwri']= "Не подлежи на запис";
	$wplang['sitehealth_php']	= "PHP версия";
	$wplang['sitehealth_gd']	= "GD библиотека";
	$wplang['sitehealth_mem']	= "Ограничение на PHP паметта";
	$wplang['sitehealth_exe']	= "PHP max_execution_time";
	$wplang['sitehealth_time']	= "PHP max_input_time";
	$wplang['sitehealth_file']	= "PHP upload_max_filesize";
	$wplang['sitehealth_post']	= "PHP post_max_size";
	$wplang['sitehealth_safe']	= "PHP safe_mode";
	$wplang['sitehealth_dbv']	= "Проверка на базата данни/продукта";
	$wplang['sitehealth_mysqlv']= "MySQL версия";
	$wplang['sitehealth_load']	= "Зареждане на сървъра";
	$wplang['sitehealth_upti']	= "Време на работа на сървъра";
	
	# UPDATE CHECK WIDGET
	$wplang['updater_title']	= "Потърси обновления";
	$wplang['updater_newest']	= "Вие имате най-новата версия";
	$wplang['updater_update']	= "Достъпни са обновления";
	$wplang['updater_newestis']	= "Най-новата версия е"; 
	$wplang['updater_getnew']	= "За да свалите актуалната версия, моля, влезте в своя <a href='http://www.ktools.net/members/' target='_blank'>Ktools.net акаунт</a>";
	$wplang['updater_yourv']	= "Вие използвате версия";
	
	# BLANK WIDGET
	$wplang['blank_title']		= "Празен панел";
	
	# CALENDAR WIDGET
	$wplang['calendar_title']	= "Календар";
	
	# TIPS CHECK WIDGET
	//$wplang['tips_title']		= "Съвети";
	//$wplang['tips_next']		= "Следващ съвет";
	//$welcometip[] 				= "Знаете ли, че можете да използвате <strong>Ctrl+Shift+S</strong>, за да отворите и затворите дадено меню?"; 
	//$welcometip[] 				= "Можете да провлачите тези панели, за да ги преподредите по желания от Вас начин. За целта кликнете върху заглавието и преместете. След това пуснете панела на предпочитаното от Вас място."; 
	//$welcometip[] 				= "За бърз достъп до галерии и други менюта трябва да кликнете върху стрелката в горния ляв ъгъл. Така ще отворите менюто с опции. За да го затворите, кликнете още веднъж на същото място."; 
	
?>