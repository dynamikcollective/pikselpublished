<?php
	###################################################################
	####	REGISTER LANGUAGE FILES                                ####
	####	Copyright 2003-2007 Ktools.net. All Rights Reserved    ####
	####	http://www.ktools.net                                  ####
	####	Created: 4-26-2007                                     ####
	####	Modified: 4-26-2007                                    #### 
	###################################################################
	
	// CHECK FOR THESE REQUIRED LANGUAGE FILES - REGISTER LANGUAGES
		// COULD MAKE THIS DYNAMIC BY CREATING THE ARRAY FROM THE ENGLISH FOLDER
		
	# MANAGER FILES	
	$regmgrfiles = array(
						"lang.calendar.php",
						"lang.manager.php",
						"lang.settings.php",
						"lang.gateways.php"
					);
					
					// lang.manager401.php - for 4.0.1 additions
					
	# PUBLIC FILES
	$regpubfiles = array(
						"lang.calendar.php",
						"lang.public.php",
						"lang.settings.php",
						"lang.gateways.php"
					);
?>
